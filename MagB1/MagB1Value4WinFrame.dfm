inherited frameMagB1Value4Win: TframeMagB1Value4Win
  AutoSize = True
  object GroupBox1: TTntGroupBox
    Left = 136
    Top = 56
    Width = 345
    Height = 41
    BiDiMode = bdLeftToRight
    Caption = 'GroupBox1'
    Color = 14670037
    ParentBiDiMode = False
    ParentColor = False
    TabOrder = 0
    object Label2: TTntLabel
      Left = 16
      Top = 16
      Width = 50
      Height = 13
      BiDiMode = bdLeftToRight
      Caption = 'Min Value:'
      ParentBiDiMode = False
    end
    object lblMin1: TTntLabel
      Left = 96
      Top = 17
      Width = 6
      Height = 13
      BiDiMode = bdLeftToRight
      Caption = '0'
      ParentBiDiMode = False
    end
    object Label3: TTntLabel
      Left = 200
      Top = 16
      Width = 53
      Height = 13
      BiDiMode = bdLeftToRight
      Caption = 'Max Value:'
      ParentBiDiMode = False
    end
    object lblMax1: TTntLabel
      Left = 280
      Top = 17
      Width = 6
      Height = 13
      BiDiMode = bdLeftToRight
      Caption = '0'
      ParentBiDiMode = False
    end
  end
  object Panel4: TTntPanel
    Left = 8
    Top = 112
    Width = 289
    Height = 112
    BevelOuter = bvLowered
    Color = 14670037
    TabOrder = 1
  end
  object Panel5: TTntPanel
    Left = 304
    Top = 112
    Width = 289
    Height = 112
    BevelOuter = bvLowered
    Color = 14670037
    TabOrder = 2
  end
  object GroupBox2: TTntGroupBox
    Left = 136
    Top = 240
    Width = 345
    Height = 41
    BiDiMode = bdLeftToRight
    Caption = 'GroupBox2'
    Color = 14670037
    ParentBiDiMode = False
    ParentColor = False
    TabOrder = 3
    object Label4: TTntLabel
      Left = 16
      Top = 16
      Width = 50
      Height = 13
      BiDiMode = bdLeftToRight
      Caption = 'Min Value:'
      ParentBiDiMode = False
    end
    object lblMin2: TTntLabel
      Left = 96
      Top = 16
      Width = 6
      Height = 13
      BiDiMode = bdLeftToRight
      Caption = '0'
      ParentBiDiMode = False
    end
    object Label5: TTntLabel
      Left = 196
      Top = 16
      Width = 53
      Height = 13
      BiDiMode = bdLeftToRight
      Caption = 'Max Value:'
      ParentBiDiMode = False
    end
    object lblMax2: TTntLabel
      Left = 283
      Top = 16
      Width = 6
      Height = 13
      BiDiMode = bdLeftToRight
      Caption = '0'
      ParentBiDiMode = False
    end
  end
  object Panel6: TTntPanel
    Left = 16
    Top = 296
    Width = 289
    Height = 112
    BevelOuter = bvLowered
    Color = 14670037
    TabOrder = 4
  end
  object Panel7: TTntPanel
    Left = 312
    Top = 296
    Width = 289
    Height = 112
    BevelOuter = bvLowered
    Color = 14670037
    TabOrder = 5
  end
  object btnWrite1: TTntButton
    Left = 272
    Top = 424
    Width = 73
    Height = 25
    Caption = 'Write all'
    TabOrder = 6
    OnClick = btnWrite1Click
  end
end
