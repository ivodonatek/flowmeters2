unit MagB1StatRpt;

interface

uses Windows, SysUtils, Messages, Classes, Graphics, Controls,
  StdCtrls, ExtCtrls, Forms, Quickrpt, QRCtrls, MagB1MyCommunicationClass,
  FunctionsUnit;

type
  TqrMagB1Stat = class(TQuickRep)
    DetailBand1: TQRBand;
    TitleBand1: TQRBand;
    qrlTitle: TQRLabel;
    ColumnHeaderBand1: TQRBand;
    qrlDateTitle: TQRLabel;
    qrlTimeTitle: TQRLabel;
    qrlTotalPlusTitle: TQRLabel;
    qrlTotalMinusTitle: TQRLabel;
    qrlTotalTitle: TQRLabel;
    qrlTotalPlusVolumeTitle: TQRLabel;
    qrlTotalMinusVolumeTitle: TQRLabel;
    qrlTotalVolumeTitle: TQRLabel;
    qrlErrorCodeTitle: TQRLabel;
    qrlDate: TQRLabel;
    qrlTime: TQRLabel;
    qrlTotalPlus: TQRLabel;
    qrlTotalMinus: TQRLabel;
    qrlTotal: TQRLabel;
    qrlTotalPlusVolume: TQRLabel;
    qrlTotalMinusVolume: TQRLabel;
    qrlTotalVolume: TQRLabel;
    qrlErrorCode: TQRLabel;
    procedure QuickRepNeedData(Sender: TObject; var MoreData: Boolean);
    procedure QuickRepBeforePrint(Sender: TCustomQuickRep;
      var PrintReport: Boolean);
  private
    LocalLanguage: TLanguage;
    FIndex: Integer;
    FDataB1ListRef: TDataB1List;
  public
    constructor CreateRep(AOwner : TComponent; ADataB1ListRef: TDataB1List; ADemo: Boolean);
    procedure TranslateReport;
  end;

implementation

uses MagB1FunctionsUnit, MagB1DataClass, MagB1TranslationUnit;

{$R *.DFM}

procedure TqrMagB1Stat.QuickRepNeedData(Sender: TObject;
  var MoreData: Boolean);
begin
  if (FIndex < FDataB1ListRef.Count) then begin
    qrlDate.Caption := FormatStatDate(FDataB1ListRef[FIndex].DateTime);
    qrlTime.Caption := FormatStatTime(FDataB1ListRef[FIndex].DateTime);
    qrlTotalPlus.Caption := Format('%.2f', [FDataB1ListRef[FIndex].TotalPlus]);
    qrlTotalMinus.Caption := Format('%.2f', [FDataB1ListRef[FIndex].TotalMinus]);
    qrlTotal.Caption := Format('%.2f', [FDataB1ListRef[FIndex].TotalPlus - FDataB1ListRef[FIndex].TotalMinus]);
    if (FDataB1ListRef[FIndex].TotalVolumeDef) then begin
      qrlTotalPlusVolume.Caption := Format('%.2f', [FDataB1ListRef[FIndex].TotalPlusVolume]);
      qrlTotalMinusVolume.Caption := Format('%.2f', [FDataB1ListRef[FIndex].TotalMinusVolume]);
      qrlTotalVolume.Caption := Format('%.2f', [FDataB1ListRef[FIndex].TotalPlusVolume - FDataB1ListRef[FIndex].TotalMinusVolume]);
    end else begin
      qrlTotalPlusVolume.Caption := '-';
      qrlTotalMinusVolume.Caption := '-';
      qrlTotalVolume.Caption := '-';
    end;
    qrlErrorCode.Caption := CardinalToBinaryStr16(FDataB1ListRef[FIndex].ErrorCode);
  end;
  MoreData := (FIndex < FDataB1ListRef.Count);
  Inc(FIndex);
end;

procedure TqrMagB1Stat.QuickRepBeforePrint(Sender: TCustomQuickRep;
  var PrintReport: Boolean);
begin
  FIndex := 0;
end;

constructor TqrMagB1Stat.CreateRep(AOwner: TComponent; ADataB1ListRef: TDataB1List; ADemo: Boolean);
begin
  inherited Create(AOwner);
  TranslateReport;
  FDataB1ListRef := ADataB1ListRef;
  if ADemo then begin
    qrlTitle.Caption := qrlTitle.Caption + ' (DEMO)';
  end;
end;

procedure TqrMagB1Stat.TranslateReport;
var
  lang: TLanguage;
begin
  lang:= LocalLanguage;
  if lang = langArabic then lang:= langEnglish;

  qrlTitle.Caption := GetTranslationTextToLanguage(lang, 'DATALOGGER', qrlTitle.Caption);
  qrlDateTitle.Caption := GetTranslationTextToLanguage(lang, 'DATE', qrlDateTitle.Caption);
  qrlTimeTitle.Caption := GetTranslationTextToLanguage(lang, 'TIME', qrlTimeTitle.Caption);
  qrlTotalPlusTitle.Caption := GetTranslationTextToLanguage(lang, 'STAT_TOTAL_PLUS', qrlTotalPlusTitle.Caption);
  qrlTotalMinusTitle.Caption := GetTranslationTextToLanguage(lang, 'STAT_TOTAL_MINUS', qrlTotalMinusTitle.Caption);
  qrlTotalTitle.Caption := GetTranslationTextToLanguage(lang, 'STAT_TOTAL', qrlTotalTitle.Caption);
  qrlTotalPlusVolumeTitle.Caption := GetTranslationTextToLanguage(lang, 'STAT_TOTAL_PLUS_VOL', qrlTotalPlusVolumeTitle.Caption);
  qrlTotalMinusVolumeTitle.Caption := GetTranslationTextToLanguage(lang, 'STAT_TOTAL_MINUS_VOL', qrlTotalMinusVolumeTitle.Caption);
  qrlTotalVolumeTitle.Caption := GetTranslationTextToLanguage(lang, 'STAT_TOTAL_VOL', qrlTotalVolumeTitle.Caption);
  qrlErrorCodeTitle.Caption := GetTranslationTextToLanguage(lang, 'STAT_ERROR_CODE', qrlErrorCodeTitle.Caption);
end;

end.
