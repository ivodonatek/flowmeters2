unit MagB1ValueInfoFrame;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, StdCtrls,
  MagB1MyCommunicationClass, NodeClass, ExtCtrls, MagB1FunctionsUnit,
  MagB1ValueHeadFrame, TntStdCtrls, FunctionsUnit;

type
  TframeMagB1ValueInfo = class(TframeMagB1Head)
    Panel1: TPanel;
    lblValue: TLabel;
  private
    { Private declarations }
  protected
    function ConvertUnits: Boolean; override;
  public
    { Public declarations }
    constructor CreateFrame(AOwner: TComponent; MyCommunication : TMyCommunication ; EditNode: TNode;
      Language: TLanguage; LanguageIndex: Integer); override;
    function GetValueText(Value:Integer): String;
  end;

var
  frameMagB1ValueInfo: TframeMagB1ValueInfo;

implementation

uses MagB1GlobalUtilsClass, MagB1TranslationUnit, TntDialogs;

{$R *.DFM}

function TframeMagB1ValueInfo.ConvertUnits: Boolean;
begin
  Result := True;
end;

constructor TframeMagB1ValueInfo.CreateFrame(AOwner: TComponent; MyCommunication: TMyCommunication; EditNode: TNode;
  Language: TLanguage; LanguageIndex: Integer);
var ReadOK:Boolean;
  FourByte:T4Byte;
  LastCursor : TCursor;
  Value:real;
  Decimal, i:integer;
begin
  inherited;
  LastCursor := Screen.Cursor;
  try
    if Assigned(WaitFrm) then
    	WaitFrm.Show;
    Screen.Cursor:=crHourGlass;
    repeat
      Application.ProcessMessages;
      ReadOK:=ReadValue(FEditNode.ModbusAdr-1,FourByte);
    until(ReadOK)or(WideMessageDlg(GetTranslationText('READ_ERROR', STR_READ_ERROR),mtError, [mbRetry, mbAbort], 0) = mrAbort);
    if ReadOK then
    begin
      case FEditNode.Units of
      1..5: Value:=Flow_Conversion( integer(FourByte), FlowUnit);
      6..8: Value:=Volume_Conversion( integer(FourByte), VolumeUnit);
      else
        Value:= integer(FourByte);
//      	lblValue.Caption:=GetValueText(FourByte);
      end;
      Decimal:=1;
      for I:=1 to FEditNode.Decimal do
      	Decimal:=Decimal*10;
      lblValue.Caption:=Format('%.*f',[FEditNode.Decimal,Value/Decimal]);
//      lblValue.Caption:=Format('%.*f',[FEditNode.Digits,Value/Power(10,FEditNode.Decimal)]);

    end
    else
      lblValue.Caption:='Read Error';
    if Assigned(WaitFrm) then
    	WaitFrm.gWait.AddProgress(10);
    Application.ProcessMessages;
  finally
    Screen.Cursor:=LastCursor;
    if Assigned(WaitFrm) then
    	WaitFrm.Hide;
  end;
end;


function TframeMagB1ValueInfo.GetValueText(Value:integer): String;
var I : Integer;
    Decimal : Integer;
begin
  Decimal:=1;
  for I:=1 to FEditNode.Decimal do begin
    Decimal:=Decimal*10;
  end;
//  I:=Integer(FourByte);
	I:=Value;
  Result:=Format('%.*f',[FEditNode.Decimal,I/Decimal]);
end;

end.
