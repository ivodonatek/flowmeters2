inherited frameMagB1ValueBool: TframeMagB1ValueBool
  object lblStatus: TTntLabel
    Left = 32
    Top = 56
    Width = 30
    Height = 13
    BiDiMode = bdLeftToRight
    Caption = 'Status'
    ParentBiDiMode = False
    Transparent = True
  end
  object Panel1: TPanel
    Left = 24
    Top = 80
    Width = 185
    Height = 25
    BevelOuter = bvLowered
    Color = 14670037
    TabOrder = 0
    object lblValue: TTntLabel
      Left = 8
      Top = 5
      Width = 46
      Height = 13
      BiDiMode = bdLeftToRight
      Caption = 'Unknown'
      ParentBiDiMode = False
    end
  end
  object btnWrite: TTntButton
    Left = 136
    Top = 48
    Width = 73
    Height = 25
    BiDiMode = bdLeftToRight
    Caption = 'Yes'
    ParentBiDiMode = False
    TabOrder = 1
    OnClick = btnWriteClick
  end
end
