unit MagB1TranslationUnit;

interface

uses WideIniFile, FunctionsUnit;

const
  LANG_FILE_NAME = 'MagB1\MagB1.dic';

var
  GlobalLanguage: TLanguage = langEnglish;
  GlobalLanguageSection: String = 'EN';
  GlobalLanguageVersionName: WideString = 'English version';
  LanguageIniFile: TWideIniFile = nil;

function GetTranslationText(ACode: WideString; ADefValue: WideString = ''): WideString;
function GetTranslationTextToLanguage(ALanguage: TLanguage; ACode: WideString; ADefValue: WideString = ''): WideString;

implementation

uses Forms, SysUtils;

function GetTranslationText(ACode: WideString; ADefValue: WideString = ''): WideString;
begin
  Result := ADefValue;
  if Assigned(LanguageIniFile) then begin
    Result := LanguageIniFile.ReadString(GlobalLanguageSection, ACode, ADefValue);
  end;
end;

function GetTranslationTextToLanguage(ALanguage: TLanguage; ACode: WideString; ADefValue: WideString = ''): WideString;
begin
  Result := ADefValue;
  if Assigned(LanguageIniFile) then begin
    Result := LanguageIniFile.ReadString(GetLanguageSection(ALanguage), ACode, ADefValue);
  end;
end;

initialization
LanguageIniFile := TWideIniFile.Create(IncludeTrailingBackslash(ExtractFilePath(Application.ExeName)) + LANG_FILE_NAME);

finalization
FreeAndNil(LanguageIniFile);

end.
