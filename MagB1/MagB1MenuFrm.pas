unit MagB1MenuFrm;

interface

uses
  Windows,  filectrl, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, ComCtrls, StdCtrls, NodeClass, MenuNodeClass, MagB1MyCommunicationClass, Spin, Buttons,
  ImgList,IniFiles, MagB1DataClass, TeEngine, TeeFunci, Series, TeeProcs, Chart,
  PBSpinEdit, PBNumEdit, PBSuperSpin, MagB1ValueInfoFrame, MagB1HeadFrm, MagB1ValueEditFrame,
  MagB1Value4WinFrame, MagB1ValueBoolFrame, MagB1ValueISelFrame, MagB1ValuePaswFrame,
  MagB1FunctionsUnit, mxCalendar,WaitFrm, MagB1PassFrm, Grids, AdomCore_4_3,
  TntComCtrls, TntExtCtrls, TntStdCtrls, TntForms, TntDialogs, WideIniFile;

const
  BACKGROUND_FILE_NAME = 'MagB1\Background.jpg';
  ERROR_TEXTS_FILE_NAME = 'MagB1\MagB1_Error.dat';
  CHART_TOLERANCE = 1.2; // 20 %
  GRAPH_ITEMS_COUNT = 100;
  ERROR_CODE_BYTES_COUNT = 16;
  ZERO_FLOW_NAME = 'ZERO_FLOW';
  ZERO_FLOW_CONST_NAME = 'ZERO_FLOW_CONSTANT';
  CALIB_VALUE_NAMES : Array[1..3] of String = ('CALIBRATION_POINT_ONE','CALIBRATION_POINT_TWO','CALIBRATION_POINT_THREE');
  MEASURE_VALUE_NAMES : Array[1..3] of String = ('MEASURED_POINT_ONE','MEASURED_POINT_TWO','MEASURED_POINT_THREE');
  WM_NEED_RESET = WM_USER + 1;

  // xml tagy
  ROOT_TAG_NAME = 'Settings';
  SW_TAG_NAME = 'SW';
  FW_TAG_NAME = 'FW';
  SER_NO_TAG_NAME = 'SerNo';
  USER_SETTINGS_TAG_NAME = 'UserSettings';
  SETTING_TAG_NAME = 'Setting';
  TYPE_TAG_NAME = 'Type';
  MODBUS_ADR_TAG_NAME = 'ModbusAdr';
  MODBUS_ADR1_TAG_NAME = 'ModbusAdr1';
  MODBUS_ADR2_TAG_NAME = 'ModbusAdr2';
  MODBUS_ADR3_TAG_NAME = 'ModbusAdr3';
  MODBUS_ADR4_TAG_NAME = 'ModbusAdr4';
  VALUE_TAG_NAME = 'Value';
  VALUE1_TAG_NAME = 'Value1';
  VALUE2_TAG_NAME = 'Value2';
  VALUE3_TAG_NAME = 'Value3';
  VALUE4_TAG_NAME = 'Value4';

type
  TErrorShapeArray = array[1 .. ERROR_CODE_BYTES_COUNT] of TShape;
  TErrorLabelArray = array[1 .. ERROR_CODE_BYTES_COUNT] of TTntLabel;
  TIntValues4 = array[1..4] of Integer;
  TformMagB1Menu = class(TformMagB1Head)
    tvMenu: TTntTreeView;
    pcMenu: TTntPageControl;
    tsEdit: TTntTabSheet;
    imgPozadi: TTntImage;
    pnlMenu: TTntPanel;
    OpenDialog2: TTntOpenDialog;
    SaveDialog2: TTntSaveDialog;
    btnMenuBackSave: TTntButton;
    btnMenuBackLoad: TTntButton;
    Splitter1: TSplitter;
    tsTime: TTntTabSheet;
    imgBack1: TTntImage;
    lblNowInMemTime: TTntLabel;
    Panel1: TTntPanel;
    lblTime: TTntLabel;
    Image13: TTntImage;
    Image14: TTntImage;
    Image16: TTntImage;
    Image17: TTntImage;
    SpeedButton1: TSpeedButton;
    SpeedButton2: TSpeedButton;
    SpeedButton15: TSpeedButton;
    SpeedButton16: TSpeedButton;
    Panel4: TTntPanel;
    lblTimeMem: TTntLabel;
    btnReadTime: TTntButton;
    btnWriteTime: TTntButton;
    tsDate: TTntTabSheet;
    imgBack2: TTntImage;
    lblNowInMemDate: TTntLabel;
    Panel3: TTntPanel;
    lblDateMem: TTntLabel;
    btnReadDate: TTntButton;
    btnGoToday: TTntButton;
    btnWriteDate: TTntButton;
    tsOnline: TTntTabSheet;
    imgBack3: TTntImage;
    Panel5: TTntPanel;
    lblFlow: TTntLabel;
    lblTotal: TTntLabel;
    lblTotalPlus: TTntLabel;
    lblOnlineFlow: TTntLabel;
    lblOnlineTotal: TTntLabel;
    lblOnlineTotalPlus: TTntLabel;
    OnlineChart: TChart;
    tsCalibration: TTntTabSheet;
    imgBack4: TTntImage;
    Panel2: TTntPanel;
    CalibrationStatusLabel: TTntLabel;
    lblUnit: TTntLabel;
    lblCalibrationData: TTntLabel;
    cbFlowUnit: TTntComboBox;
    CalSpinEdit1: TPBSuperSpin;
    CalSpinEdit2: TPBSuperSpin;
    CalSpinEdit3: TPBSuperSpin;
    btnWriteCalData1: TTntButton;
    btnWriteCalData2: TTntButton;
    btnWriteCalData3: TTntButton;
    btnAutomaticZeroConst: TTntButton;
    Panel6: TTntPanel;
    MeasurmentStatusLabel: TTntLabel;
    lblMeasurementData: TTntLabel;
    lblError: TTntLabel;
    MeasSpinEdit1: TPBSuperSpin;
    MeasSpinEdit2: TPBSuperSpin;
    MeasSpinEdit3: TPBSuperSpin;
    btnWriteMeasData1: TTntButton;
    btnWriteMeasData2: TTntButton;
    btnWriteMeasData3: TTntButton;
    ErrorSpinEdit1: TPBSuperSpin;
    ErrorSpinEdit2: TPBSuperSpin;
    ErrorSpinEdit3: TPBSuperSpin;
    btnMeasurementCalculate: TTntButton;
    Panel8: TTntPanel;
    btnReadAll: TTntButton;
    btnWriteAll: TTntButton;
    btnOpenDataFile: TTntButton;
    btnSaveDataFile: TTntButton;
    OnlineTimer: TTimer;
    SaveDialog1: TTntSaveDialog;
    OpenDialog1: TTntOpenDialog;
    lblTotalMinus: TTntLabel;
    lblOnlineTotalMinus: TTntLabel;
    lblAuxPlus: TTntLabel;
    lblOnlineAuxPlus: TTntLabel;
    lblErrorCode: TTntLabel;
    lblOnlineErrorCode: TTntLabel;
    cmbOnlineFlowUnit: TTntComboBox;
    cmbOnlineTotalUnit: TTntComboBox;
    Series2: TFastLineSeries;
    tsUpdateFirmware: TTntTabSheet;
    imgBack5: TTntImage;
    Panel9: TTntPanel;
    lblUpdateFirmwareFile: TTntLabel;
    btnUpdateFWFileSelect: TTntButton;
    btnUpdateFirmware: TTntButton;
    progUpdateFirmware: TProgressBar;
    lblUpdateFirmwareStatus: TTntLabel;
    edtUpdateFirmwareFile: TTntEdit;
    lblUpdateFirmwareComment: TTntLabel;
    btnManualZeroConst: TTntButton;
    ZeroConstSpinEdit: TSpinEdit;
    Panel10: TTntPanel;
    shpRealTimeError1: TShape;
    shpRealTimeError2: TShape;
    shpRealTimeError3: TShape;
    shpRealTimeError4: TShape;
    shpRealTimeError5: TShape;
    shpRealTimeError6: TShape;
    shpRealTimeError7: TShape;
    shpRealTimeError8: TShape;
    shpRealTimeError9: TShape;
    shpRealTimeError10: TShape;
    shpRealTimeError11: TShape;
    shpRealTimeError12: TShape;
    shpRealTimeError13: TShape;
    shpRealTimeError14: TShape;
    shpRealTimeError15: TShape;
    shpRealTimeError16: TShape;
    lblRealTimeError1: TTntLabel;
    lblRealTimeError5: TTntLabel;
    lblRealTimeError9: TTntLabel;
    lblRealTimeError13: TTntLabel;
    lblRealTimeError2: TTntLabel;
    lblRealTimeError6: TTntLabel;
    lblRealTimeError10: TTntLabel;
    lblRealTimeError14: TTntLabel;
    lblRealTimeError3: TTntLabel;
    lblRealTimeError4: TTntLabel;
    lblRealTimeError7: TTntLabel;
    lblRealTimeError8: TTntLabel;
    lblRealTimeError11: TTntLabel;
    lblRealTimeError12: TTntLabel;
    lblRealTimeError15: TTntLabel;
    lblRealTimeError16: TTntLabel;
    lblErrorTable: TTntLabel;
    Panel11: TTntPanel;
    lblFlow2: TTntLabel;
    lblTotal2: TTntLabel;
    lblTotalPlus2: TTntLabel;
    lblOnlineFlow2: TTntLabel;
    lblOnlineTotal2: TTntLabel;
    lblOnlineTotalPlus2: TTntLabel;
    lblTotalMinus2: TTntLabel;
    lblOnlineTotalMinus2: TTntLabel;
    lblAuxPlus2: TTntLabel;
    lblOnlineAuxPlus2: TTntLabel;
    lblErrorCode2: TTntLabel;
    lblOnlineErrorCode2: TTntLabel;
    cmbOnlineFlowUnit2: TTntComboBox;
    cmbOnlineTotalUnit2: TTntComboBox;
    OnlineChart2: TChart;
    FastLineSeries1: TFastLineSeries;
    panelCalendar: TPanel;
    mxCalendar1: TmxCalendar;
    procedure FormDestroy(Sender: TObject);
    procedure tvMenuChange(Sender: TObject; Node: TTreeNode);
    procedure tvMenuExpanding(Sender: TObject; Node: TTreeNode;
      var AllowExpansion: Boolean);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure btnMenuBackSaveClick(Sender: TObject);
    procedure btnMenuBackLoadClick(Sender: TObject);
    Procedure IncTime( Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure btnReadTimeClick(Sender: TObject);
    procedure btnWriteTimeClick(Sender: TObject);
    procedure btnReadDateClick(Sender: TObject);
    procedure btnWriteDateClick(Sender: TObject);
    procedure btnGoTodayClick(Sender: TObject);
    procedure OnlineTimerTimer(Sender: TObject);
    procedure btnWriteCalDataClick(Sender: TObject);
    procedure btnAutomaticZeroConstClick(Sender: TObject);
    procedure btnWriteMeasDataClick(Sender: TObject);
    procedure btnMeasurementCalculateClick(Sender: TObject);
    procedure btnReadAllClick(Sender: TObject);
    procedure btnWriteAllClick(Sender: TObject);
    procedure btnOpenDataFileClick(Sender: TObject);
    procedure btnSaveDataFileClick(Sender: TObject);
    procedure pcMenuChanging(Sender: TObject; var AllowChange: Boolean);
    procedure pcMenuChange(Sender: TObject);
    procedure btnUpdateFWFileSelectClick(Sender: TObject);
    procedure btnUpdateFirmwareClick(Sender: TObject);
    procedure edtUpdateFirmwareFileChange(Sender: TObject);
    procedure btnManualZeroConstClick(Sender: TObject);
  private
    { Private declarations }
    EventsEnabled: Boolean;
    NeedReset: Boolean;
    MenuNodes : TMenuNodes;
    Nodes : TNodes;
    MyCommunication : TMyCommunication;
    {---}
    EditFrame : Tframe;
    OnlineErr:integer;
    time:TmyTime;
    OnlinePocitadlo:Integer;
    TabIndexPred: word;
    Busy:Boolean;
    MinChartValue, MaxChartValue: Double;
    MinChartCnt, MaxChartCnt: Integer;
    ErrorShapeArray: TErrorShapeArray;
    ErrorLabelArray: TErrorLabelArray;
    procedure LoadFromIniFile;
    procedure SaveToIniFile;
    function CreateXmlElement(const DomDocument: TDomDocument; const Node: TNode; const TagName: string; const Values: TIntValues4): TDomElement;
    procedure ParseXmlElement(const DomElement: TDomElement; var Node: TNode; var Values: TIntValues4);
    function GetNode(ModbusAdr: Integer) : TNode;
  public
    { Public declarations }
    procedure WMNeedReset(var Message: TMessage); message WM_NEED_RESET;
    procedure Init(AMyCommunication : TMyCommunication; AfirmwareNO:integer; AProtokolType:TProtokolType);
    procedure TranslateForm;
    procedure EnableEvents;
  end;

var
  //formMagB1Menu: TformMagB1Menu;
  Secure: array [1..4] of Boolean;

const
  ONLINE_FLOW_UNITS = 5;
  ONLINE_FLOW_UNIT_TEXTS: array [0 .. ONLINE_FLOW_UNITS - 1] of String = ('m3/h', 'UKG/min', 'USG/min' , 'l/min', 'l/s');
  ONLINE_FLOW_UNIT_COEFS: array [0 .. ONLINE_FLOW_UNITS - 1] of Double = (1.0, 3.6661541383, 4.4028675393, 16.66666666666, 0.27777777778);
  ONLINE_TOTAL_UNITS = 4;
  ONLINE_TOTAL_UNIT_TEXTS: array[0 .. ONLINE_TOTAL_UNITS - 1] of String = ('m3', 'UKG', 'USG', 'l');
  ONLINE_TOTAL_UNIT_COEFS: array[0 .. ONLINE_TOTAL_UNITS - 1] of Double = (1.0, 219.9692483, 264.17205236, 1000.0);

implementation

uses
  MagX1Exception, Math, MagB1GlobalUtilsClass, MagB1PassForm, MagB1ValueIBinFrame,
  MagB1ValueIHexFrame, MagB1TranslationUnit, XModem, ShellApi, ZipMstr,
  MagB1ValueIHDTFrame, Main, FunctionsUnit;

{$R *.DFM}

procedure TformMagB1Menu.FormCreate(Sender: TObject);
var I: Integer;
    SL: TStringList;
begin
  inherited;

  EventsEnabled:= False;
  
  TranslateForm;
  NeedReset := False;

  // Error Labels
  ErrorLabelArray[1] := lblRealTimeError1;
  ErrorLabelArray[2] := lblRealTimeError2;
  ErrorLabelArray[3] := lblRealTimeError3;
  ErrorLabelArray[4] := lblRealTimeError4;
  ErrorLabelArray[5] := lblRealTimeError5;
  ErrorLabelArray[6] := lblRealTimeError6;
  ErrorLabelArray[7] := lblRealTimeError7;
  ErrorLabelArray[8] := lblRealTimeError8;
  ErrorLabelArray[9] := lblRealTimeError9;
  ErrorLabelArray[10] := lblRealTimeError10;
  ErrorLabelArray[11] := lblRealTimeError11;
  ErrorLabelArray[12] := lblRealTimeError12;
  ErrorLabelArray[13] := lblRealTimeError13;
  ErrorLabelArray[14] := lblRealTimeError14;
  ErrorLabelArray[15] := lblRealTimeError15;
  ErrorLabelArray[16] := lblRealTimeError16;

  // Error Shapes
  ErrorShapeArray[1] := shpRealTimeError1;
  ErrorShapeArray[2] := shpRealTimeError2;
  ErrorShapeArray[3] := shpRealTimeError3;
  ErrorShapeArray[4] := shpRealTimeError4;
  ErrorShapeArray[5] := shpRealTimeError5;
  ErrorShapeArray[6] := shpRealTimeError6;
  ErrorShapeArray[7] := shpRealTimeError7;
  ErrorShapeArray[8] := shpRealTimeError8;
  ErrorShapeArray[9] := shpRealTimeError9;
  ErrorShapeArray[10] := shpRealTimeError10;
  ErrorShapeArray[11] := shpRealTimeError11;
  ErrorShapeArray[12] := shpRealTimeError12;
  ErrorShapeArray[13] := shpRealTimeError13;
  ErrorShapeArray[14] := shpRealTimeError14;
  ErrorShapeArray[15] := shpRealTimeError15;
  ErrorShapeArray[16] := shpRealTimeError16;

  try
    SL := TStringList.Create();
    try
      SL.LoadFromFile(IncludeTrailingBackslash(ExtractFilePath(Application.ExeName)) + ERROR_TEXTS_FILE_NAME);
      for I := 1 to ERROR_CODE_BYTES_COUNT do begin
        if (I <= SL.Count) then begin
          ErrorLabelArray[I].Caption := SL[I-1];
        end;
      end;
    finally
      SL.Free;
    end;
  except
    on E: Exception do begin
      MessageDlg(Format('Loading ErrorCodes from file "%s" failed!'#13#10'%s [%s]', [IncludeTrailingBackslash(ExtractFilePath(Application.ExeName)) + ERROR_TEXTS_FILE_NAME, E.Message, E.ClassName]), mtError, [mbOK], 0);
    end;
  end;

  pcMenu.ActivePageIndex:=0;

  for I := 1 to 4 do Secure[I] := False;
  CalibrationStatusLabel.Caption := '';
  MeasurmentStatusLabel.Caption := '';

  // naplneni tabulky jednotek kalibrace
  cbFlowUnit.Items.Clear;
  for I := Low(FlowUnitList) to High(FlowUnitList) do begin
    cbFlowUnit.Items.Add(FlowUnitList[I]);
  end;
  cbFlowUnit.ItemIndex := Ord(m3_h);

  //vymaz online graf a nastav 100 nul
  OnlineChart.SeriesList[0].Clear;
  OnlineChart2.SeriesList[0].Clear;
  for I := 0 to GRAPH_ITEMS_COUNT - 1 do begin
    OnlineChart.SeriesList[0].AddXY(I, 0);
    OnlineChart2.SeriesList[0].AddXY(I, 0);
  end;

  MinChartValue := 0;
  MaxChartValue := 0;
  MinChartCnt := 0;
  MaxChartCnt := 0;
  cmbOnlineFlowUnit.Items.Clear;
  cmbOnlineFlowUnit2.Items.Clear;
  for I := 0 to ONLINE_FLOW_UNITS do begin
    cmbOnlineFlowUnit.Items.Add(ONLINE_FLOW_UNIT_TEXTS[I]);
    cmbOnlineFlowUnit2.Items.Add(ONLINE_FLOW_UNIT_TEXTS[I]);
  end;
  cmbOnlineFlowUnit.ItemIndex := 0;
  cmbOnlineFlowUnit2.ItemIndex := 0;
  cmbOnlineTotalUnit.Items.Clear;
  cmbOnlineTotalUnit2.Items.Clear;
  for I := 0 to ONLINE_TOTAL_UNITS do begin
    cmbOnlineTotalUnit.Items.Add(ONLINE_TOTAL_UNIT_TEXTS[I]);
    cmbOnlineTotalUnit2.Items.Add(ONLINE_TOTAL_UNIT_TEXTS[I]);
  end;
  cmbOnlineTotalUnit.ItemIndex := 0;
  cmbOnlineTotalUnit2.ItemIndex := 0;
  imgPozadi.Picture.LoadFromFile(IncludeTrailingBackslash(ExtractFilePath(Application.ExeName)) + BACKGROUND_FILE_NAME);
  imgBack1.Picture.LoadFromFile(IncludeTrailingBackslash(ExtractFilePath(Application.ExeName)) + BACKGROUND_FILE_NAME);
  imgBack2.Picture.LoadFromFile(IncludeTrailingBackslash(ExtractFilePath(Application.ExeName)) + BACKGROUND_FILE_NAME);
  imgBack3.Picture.LoadFromFile(IncludeTrailingBackslash(ExtractFilePath(Application.ExeName)) + BACKGROUND_FILE_NAME);
  imgBack4.Picture.LoadFromFile(IncludeTrailingBackslash(ExtractFilePath(Application.ExeName)) + BACKGROUND_FILE_NAME);
  imgBack5.Picture.LoadFromFile(IncludeTrailingBackslash(ExtractFilePath(Application.ExeName)) + BACKGROUND_FILE_NAME);

  LoadFromIniFile;
end;

procedure TformMagB1Menu.LoadFromIniFile;
var sExePath: String;
    iniF: TWideIniFile;
begin
  //nacteni velikosti Formu
  sExePath := GetUserAppDataProductPath();
  iniF := TWideIniFile.Create(sExePath+'MagB1.ini');
  try
    Self.Width:=iniF.ReadInteger('Service','Width',800);
    Self.Height:=iniF.ReadInteger('Service','Height',600);
    if(iniF.ReadBool('Service','Maximized', false) = true) then begin
      Self.WindowState:=wsMaximized
    end else begin
      Self.WindowState:=wsNormal;
    end;
  finally
    iniF.Free;
  end;
end;

procedure TformMagB1Menu.SaveToIniFile;
var sExePath: String;
    iniF: TWideIniFile;
begin
  sExePath := GetUserAppDataProductPath();
  iniF := TWideIniFile.Create(sExePath+'MagB1.ini');
  try
    iniF.WriteBool('Service','Maximized', Self.WindowState=wsMaximized);
    if Self.WindowState = wsNormal then begin
      iniF.WriteInteger('Service','Width',Self.Width);
      iniF.WriteInteger('Service','Height',Self.Height);
    end;
    iniF.Save;
  finally
    iniF.Free;
  end;
end;

procedure TformMagB1Menu.FormDestroy(Sender: TObject);
begin
  SaveToIniFile;
  Nodes.Free;
  MenuNodes.Free;
  FreeAndNil(MyCommunication);
  inherited;
end;

procedure TformMagB1Menu.tvMenuChange(Sender: TObject; Node: TTreeNode);
begin
  if not EventsEnabled then Exit;
  Self.Enabled:=false;
  try
    if Assigned(Node.Data) then
    begin
      if Assigned(TMenuNode(Node.Data).MenuEdit) then
      begin
        pcMenu.ActivePageIndex:=0;
        if Assigned(EditFrame) then
            FreeAndNil(EditFrame);
        case TNode(TMenuNode(Node.Data).MenuEdit).Typ of
          1 : begin
                EditFrame:=TframeMagB1ValueInfo.CreateFrame(Self,MyCommunication,TNode(TMenuNode(Node.Data).MenuEdit), LocalLanguage, MultiLanguageTextIndex);
                EditFrame.Parent:=tsEdit;
              end;
          2 : begin
                EditFrame:=TframeMagB1ValueEdit.CreateFrame(Self,MyCommunication,TNode(TMenuNode(Node.Data).MenuEdit), LocalLanguage, MultiLanguageTextIndex);
                EditFrame.Parent:=tsEdit;
              end;
          3 : begin
                EditFrame:=TframeMagB1Value4Win.CreateFrame(Self,MyCommunication,TNode(TMenuNode(Node.Data).MenuEdit), LocalLanguage, MultiLanguageTextIndex);
                EditFrame.Parent:=tsEdit;
              end;
          4 : begin
                EditFrame:=TframeMagB1ValueISel.CreateFrame(Self,MyCommunication,TNode(TMenuNode(Node.Data).MenuEdit), LocalLanguage, MultiLanguageTextIndex);
                EditFrame.Parent:=tsEdit;
              end;
          5 : begin
                EditFrame:=TframeMagB1ValueBool.CreateFrame(Self,MyCommunication,TNode(TMenuNode(Node.Data).MenuEdit), LocalLanguage, MultiLanguageTextIndex);
                EditFrame.Parent:=tsEdit;
              end;
          9 : begin
                  EditFrame:=TframeMagB1ValuePasw.CreateFrame(Self,MyCommunication,TNode(TMenuNode(Node.Data).MenuEdit), LocalLanguage, MultiLanguageTextIndex);
                  EditFrame.Parent:=tsEdit;
                end;
          11: begin
                  EditFrame:=TframeMagB1ValueIHex.CreateFrame(Self,MyCommunication,TNode(TMenuNode(Node.Data).MenuEdit), LocalLanguage, MultiLanguageTextIndex);
                  EditFrame.Parent:=tsEdit;
              end;
          12: begin
                  EditFrame:=TframeMagB1ValueIBin.CreateFrame(Self,MyCommunication,TNode(TMenuNode(Node.Data).MenuEdit), LocalLanguage, MultiLanguageTextIndex);
                  EditFrame.Parent:=tsEdit;
              end;
          13: begin
                  EditFrame:=TframeMagB1ValueIHDT.CreateFrame(Self,MyCommunication,TNode(TMenuNode(Node.Data).MenuEdit), LocalLanguage, MultiLanguageTextIndex);
                  EditFrame.Parent:=tsEdit;
              end;
        end;
      end;
    end;
  finally
    Self.Enabled := true;
    tvMenu.SetFocus;
    tvMenu.Refresh;
  end;
end;

procedure TformMagB1Menu.tvMenuExpanding(Sender: TObject; Node: TTreeNode;
  var AllowExpansion: Boolean);
var	ReadOK:Boolean;
	FourByte: T4Byte;
        lang: TLanguage;
begin
    //if not EventsEnabled then Exit;
    lang:= LocalLanguage;
    if lang = langArabic then lang:= langEnglish;

    Self.Enabled:=false;
	if (TNode(TMenuNode(Node.Data).MenuEdit).Typ = 9) then	//pokud typ == PASSWD
    begin
      repeat	//cti zda je zadano heslo;
          ReadOK := MyCommunication.ReadMenuValue( TNode(TMenuNode(Node.Data).MenuEdit).ModbusAdr-1, FourByte)
      until(ReadOK)or(WideMessageDlg(GetTranslationText('READ_ERROR', STR_READ_ERROR),mtError, [mbRetry, mbAbort], 0) = mrAbort);
      if ReadOK then
      begin
         if(Integer(FourByte)=1) then	//pokud heslo OK, povol Expanding menu
         	AllowExpansion:=true
         else
         	AllowExpansion:=false;
      end
      else
          Raise Exception.Create(GetTranslationTextToLanguage(lang, 'READ_ERROR', STR_READ_ERROR));  //ukonci cteni
    end;
    Self.Enabled:=true;
end;

procedure TformMagB1Menu.FormClose(Sender: TObject; var Action: TCloseAction);
var FourByte: T4Byte;
    I: Integer;
begin
  inherited;
  if (not NeedReset) and (MyCommunication <> nil) then begin
    Integer(FourByte) := 0;
    for I := Low(PassValueName) to High(PassValueName) do begin
      if (Values.IndexOf(PassValueName[I]) >= 0) then begin
        MyCommunication.WriteMenuValue(GetAddress(ProtokolType, PassValueName[I], Values), FourByte);
      end;
    end;
  end else begin
    NeedReset := False;
  end;

  Action := caFree;
end;

//------------------------------------------------------------------------------

procedure TformMagB1Menu.Init(AMyCommunication : TMyCommunication; AfirmwareNO:integer; AProtokolType:TProtokolType);
var sExePath: string;
    filename: string;
    WaitFrm : TformWait;
    lang: TLanguage;
begin
  inherited;

  Self.MyCommunication := AMyCommunication;

  lang:= LocalLanguage;
  if lang = langArabic then lang:= langEnglish;

  if (MyCommunication.ProtokolType = ptDemo) then begin
    Caption := Caption + ' (DEMO)';
    lblDemo.Visible := True;
  end;

  MenuNodes:=TMenuNodes.Create;
  Nodes:=TNodes.Create;
  Nodes.LanguageIndex:= MultiLanguageTextIndex;
  Nodes.ConvertWideCharsPage:= GetConvertWideCharsPage(LocalLanguage, AfirmwareNO);
  sExePath := ExtractFilePath(Application.ExeName) + 'MagB1\';
  WaitFrm:=TformWait.Create(Self);
  try
    Screen.Cursor:=crHourGlass;
    WaitFrm.Caption:=GetTranslationText('LOAD_PROG_STRUCT', STR_LOAD_PROG_STRUCT);
    WaitFrm.gWait.Progress:=0;
    WaitFrm.Show;
    SetCenter(WaitFrm);
    Application.ProcessMessages;

    filename := Format('Menu%d.dat', [firmwareNO]);
    if FileExists(sExePath+filename) then begin
      Nodes.LoadFromFile(sExePath+filename,nil)
    end else begin
      raise Exception.Create(GetTranslationTextToLanguage(lang, 'UNSUPPORTED_FW', STR_UNSUPPORTED_FW) + Format(' (%d)', [firmwareNO]));  //ukonci cteni
    end;

    WaitFrm.gWait.Progress:=10;
    Application.ProcessMessages;
    MenuNodes.LoadMenuNodes(Nodes);
    WaitFrm.gWait.Progress:=20;
    Application.ProcessMessages;
    MenuNodes.FillTntTreeView(tvMenu);
    WaitFrm.gWait.Progress:=30;
    Application.ProcessMessages;
    tvMenu.FullExpand;
    WaitFrm.gWait.Progress:=100;
    Application.ProcessMessages;
    WaitFrm.Close;
  finally
    Screen.Cursor:=crDefault;
    FreeAndNil(WaitFrm);
  end;

  pcMenu.Enabled:=true;
  tvMenu.Enabled:=true;

  case MyCommunication.ProtokolType of
    ptMagX1:
        Raise Exception.Create(GetTranslationTextToLanguage(lang, 'UNSUPPORTED_COM_PROTOCOL', STR_UNSUPPORTED_COM_PROTOCOL));  //ukonci cteni
    else
    begin
      btnMenuBackSave.Visible:=true;
      btnMenuBackLoad.Visible:=true;
      tvMenu.Align:=alTop;
    end;
  end;
end;

(*
procedure TformMenu.ConnectTimerTimer(Sender: TObject);
var Idx:Integer;
    Data:T4Byte;
begin
  Idx:=Values.IndexOf('UNIT_NO');
  if TMyCommPort(MyCommunication).ReadMenuValue(4*Idx,Data) then
  begin
    if not SerNo=Cardinal(Data) then
    begin
      Label10.Caption:='disconnected';
      Shape2.Brush.Color:=clRed;
      OnlineTimer.Enabled:=false;
//      ConnectTimer.enabled:=true;
      tmrCounting.Enabled:=false;
      pcMenu.Enabled:=false;
      tvMenu.Enabled:=false;
      MyCommunication.Disconnect;
      ShowMessage('Connection error');
    end;
  end
  else if not OnlineTimer.Enabled then
  begin
    Inc(ConnectionErr);
    if ConnectionErr > ConnectionMAX_ERROR then
    begin
      Label10.Caption:='disconnected';
      Shape2.Brush.Color:=clRed;
      OnlineTimer.Enabled:=false;
      tmrCounting.Enabled:=false;
      pcMenu.Enabled:=false;
      tvMenu.Enabled:=false;
      MyCommunication.Disconnect;
      ShowMessage('Connection error');
    end;
  end
end;
*)

function TformMagB1Menu.CreateXmlElement(const DomDocument: TDomDocument; const Node: TNode; const TagName: string; const Values: TIntValues4): TDomElement;
var
  DomText: TDomText;
  ParameterElement: TDomElement;
begin
  Result := TDomElement.Create(DomDocument, TagName);

  ParameterElement := TDomElement.Create(DomDocument, TYPE_TAG_NAME);
  DomText := TDomText.Create(DomDocument);
  DomText.NodeValue := IntToStr(Node.Typ);
  ParameterElement.AppendChild(DomText);
  Result.AppendChild(ParameterElement);

  if Node.Typ = 3 then begin //4Win
    ParameterElement := TDomElement.Create(DomDocument, MODBUS_ADR1_TAG_NAME);
    DomText := TDomText.Create(DomDocument);
    DomText.NodeValue := IntToStr(Node.ModbusAdr - 1);
    ParameterElement.AppendChild(DomText);
    Result.AppendChild(ParameterElement);

    ParameterElement := TDomElement.Create(DomDocument, VALUE1_TAG_NAME);
    DomText := TDomText.Create(DomDocument);
    DomText.NodeValue := IntToStr(Values[1]);
    ParameterElement.AppendChild(DomText);
    Result.AppendChild(ParameterElement);

    ParameterElement := TDomElement.Create(DomDocument, MODBUS_ADR2_TAG_NAME);
    DomText := TDomText.Create(DomDocument);
    DomText.NodeValue := IntToStr(Node.ModbusAdr2 - 1);
    ParameterElement.AppendChild(DomText);
    Result.AppendChild(ParameterElement);

    ParameterElement := TDomElement.Create(DomDocument, VALUE2_TAG_NAME);
    DomText := TDomText.Create(DomDocument);
    DomText.NodeValue := IntToStr(Values[2]);
    ParameterElement.AppendChild(DomText);
    Result.AppendChild(ParameterElement);

    ParameterElement := TDomElement.Create(DomDocument, MODBUS_ADR3_TAG_NAME);
    DomText := TDomText.Create(DomDocument);
    DomText.NodeValue := IntToStr(Node.ModbusAdr3 - 1);
    ParameterElement.AppendChild(DomText);
    Result.AppendChild(ParameterElement);

    ParameterElement := TDomElement.Create(DomDocument, VALUE3_TAG_NAME);
    DomText := TDomText.Create(DomDocument);
    DomText.NodeValue := IntToStr(Values[3]);
    ParameterElement.AppendChild(DomText);
    Result.AppendChild(ParameterElement);

    ParameterElement := TDomElement.Create(DomDocument, MODBUS_ADR4_TAG_NAME);
    DomText := TDomText.Create(DomDocument);
    DomText.NodeValue := IntToStr(Node.ModbusAdr4 - 1);
    ParameterElement.AppendChild(DomText);
    Result.AppendChild(ParameterElement);

    ParameterElement := TDomElement.Create(DomDocument, VALUE4_TAG_NAME);
    DomText := TDomText.Create(DomDocument);
    DomText.NodeValue := IntToStr(Values[4]);
    ParameterElement.AppendChild(DomText);
    Result.AppendChild(ParameterElement);
  end else begin // 2,4 //Edit, Isel
    ParameterElement := TDomElement.Create(DomDocument, MODBUS_ADR_TAG_NAME);
    DomText := TDomText.Create(DomDocument);
    DomText.NodeValue := IntToStr(Node.ModbusAdr - 1);
    ParameterElement.AppendChild(DomText);
    Result.AppendChild(ParameterElement);

    ParameterElement := TDomElement.Create(DomDocument, VALUE_TAG_NAME);
    DomText := TDomText.Create(DomDocument);
    DomText.NodeValue := IntToStr(Values[1]);
    ParameterElement.AppendChild(DomText);
    Result.AppendChild(ParameterElement);
  end;
end;

procedure TformMagB1Menu.ParseXmlElement(const DomElement: TDomElement; var Node: TNode; var Values: TIntValues4);
var
  DomNodeList: TDomNodeList;
  DomText: TDomText;
  ParameterElement: TDomElement;
begin
  DomNodeList := DomElement.GetElementsByTagName(TYPE_TAG_NAME);
  ParameterElement := TDomElement(DomNodeList.Item(0));
  DomText := TDomText(ParameterElement.FirstChild);
  Node.Typ := StrToInt(DomText.Data);

  if Node.Typ = 3 then begin //4Win
    DomNodeList := DomElement.GetElementsByTagName(MODBUS_ADR1_TAG_NAME);
    ParameterElement := TDomElement(DomNodeList.Item(0));
    DomText := TDomText(ParameterElement.FirstChild);
    Node.ModbusAdr := StrToInt(DomText.Data) + 1;

    DomNodeList := DomElement.GetElementsByTagName(VALUE1_TAG_NAME);
    ParameterElement := TDomElement(DomNodeList.Item(0));
    DomText := TDomText(ParameterElement.FirstChild);
    Values[1] := StrToInt(DomText.Data);

    DomNodeList := DomElement.GetElementsByTagName(MODBUS_ADR2_TAG_NAME);
    ParameterElement := TDomElement(DomNodeList.Item(0));
    DomText := TDomText(ParameterElement.FirstChild);
    Node.ModbusAdr2 := StrToInt(DomText.Data) + 1;

    DomNodeList := DomElement.GetElementsByTagName(VALUE2_TAG_NAME);
    ParameterElement := TDomElement(DomNodeList.Item(0));
    DomText := TDomText(ParameterElement.FirstChild);
    Values[2] := StrToInt(DomText.Data);

    DomNodeList := DomElement.GetElementsByTagName(MODBUS_ADR3_TAG_NAME);
    ParameterElement := TDomElement(DomNodeList.Item(0));
    DomText := TDomText(ParameterElement.FirstChild);
    Node.ModbusAdr3 := StrToInt(DomText.Data) + 1;

    DomNodeList := DomElement.GetElementsByTagName(VALUE3_TAG_NAME);
    ParameterElement := TDomElement(DomNodeList.Item(0));
    DomText := TDomText(ParameterElement.FirstChild);
    Values[3] := StrToInt(DomText.Data);

    DomNodeList := DomElement.GetElementsByTagName(MODBUS_ADR4_TAG_NAME);
    ParameterElement := TDomElement(DomNodeList.Item(0));
    DomText := TDomText(ParameterElement.FirstChild);
    Node.ModbusAdr4 := StrToInt(DomText.Data) + 1;

    DomNodeList := DomElement.GetElementsByTagName(VALUE4_TAG_NAME);
    ParameterElement := TDomElement(DomNodeList.Item(0));
    DomText := TDomText(ParameterElement.FirstChild);
    Values[4] := StrToInt(DomText.Data);
  end else begin // 2,4 //Edit, Isel
    DomNodeList := DomElement.GetElementsByTagName(MODBUS_ADR_TAG_NAME);
    ParameterElement := TDomElement(DomNodeList.Item(0));
    DomText := TDomText(ParameterElement.FirstChild);
    Node.ModbusAdr := StrToInt(DomText.Data) + 1;

    DomNodeList := DomElement.GetElementsByTagName(VALUE_TAG_NAME);
    ParameterElement := TDomElement(DomNodeList.Item(0));
    DomText := TDomText(ParameterElement.FirstChild);
    Values[1] := StrToInt(DomText.Data);
  end;
end;

function TformMagB1Menu.GetNode(ModbusAdr: Integer) : TNode;
var
  i: Integer;
  Node: TNode;
begin
  Result := nil;
  for i:=0 to Nodes.Count-1 do begin
    Node := TNode(Nodes.Items[i]);
    if Node.ModbusAdr = ModbusAdr then begin
      Result := Node;
      break;
    end;
  end;
end;

procedure TformMagB1Menu.btnMenuBackSaveClick(Sender: TObject);
var
    i:Integer;
    FourByte:T4Byte;
    ReadOK:Boolean;
    WaitFrm : TformWait;
    Addr:Integer;
    sExePath, sPath: string;
    iniF:TWideIniFile;
    LastCursor : TCursor;
    DomImplementation : TDomImplementation;
    DomDocument : TDomDocument;
    DomToXMLParser: TDomToXMLParser;
    Stream: TFileStream;
    DomElement: TDomElement;
    DomText: TDomText;
    UserSettingsDomElement: TDomElement;
    IntValues4: TIntValues4;
    lang: TLanguage;
begin
  inherited;
  Self.Enabled :=false;

  lang:= LocalLanguage;
  if lang = langArabic then lang:= langEnglish;
  
  try
    Addr:=-1;
    for i:=0 To Nodes.Count-1 do
      if TNode(Nodes.Items[I]).ValueName='PASSWORD_USER' then
      begin
          Addr :=TNode(Nodes.Items[I]).ModbusAdr;
          break;
      end;

    if Addr=-1 then
    	raise Exception.Create(GetTranslationTextToLanguage(lang, 'INTERNAL_ERROR', STR_INTERNAL_ERROR));

    repeat	//cti zda je zadano heslo;
        ReadOK := MyCommunication.ReadMenuValue( Addr-1 , FourByte)
    until(ReadOK)or(WideMessageDlg(GetTranslationText('READ_ERROR', STR_READ_ERROR),mtError, [mbRetry, mbAbort], 0) = mrAbort);
    if ReadOK and not(Integer(FourByte)=1) then
    begin
      formMagB1Password:=TformMagB1Password.CreateFrm(Self,MyCommunication, Addr, GetTranslationText('USER_PSW', STR_USER_PSW), LocalLanguage);
      try
      	case formMagB1Password.ShowModal of
        	mrCancel:exit;
      	end;
      finally
      	FreeAndNil(formMagB1Password);
      end;
    end;

    sExePath := GetUserAppDataProductPath();
    iniF := TWideIniFile.Create(sExePath+'MagB1.ini');
    try
      sPath := iniF.ReadString('BACKUP', 'SavePath', sExePath);
    finally
      iniF.Free;
    end;

    SaveDialog2.InitialDir:=sPath;
    SaveDialog2.FileName:='MagB1Backup'+lblSerNo.Caption+'.dat';
    if (SaveDialog2.Execute) then
    begin
      WaitFrm:=TformWait.Create(Self);
      LastCursor := Screen.Cursor;
      DomImplementation := TDomImplementation.Create(nil);
      DomDocument := TDomDocument.Create(DomImplementation);
      
      try
        Screen.Cursor:=crHourGlass;
        WaitFrm.gWait.Progress:=0;
        WaitFrm.Show;
        SetCenter(WaitFrm);
        WaitFrm.gWait.MaxValue:=Nodes.Count-1;
        Application.ProcessMessages;

        DomElement := TDomElement.Create(DomDocument, ROOT_TAG_NAME);
        DomDocument.AppendChild(DomElement);

        DomElement := TDomElement.Create(DomDocument, SW_TAG_NAME);
        DomText := TDomText.Create(DomDocument);
        DomText.NodeValue := lblSW.Caption;
        DomElement.AppendChild(DomText);
        DomDocument.DocumentElement.AppendChild(DomElement);

        DomElement := TDomElement.Create(DomDocument, FW_TAG_NAME);
        DomText := TDomText.Create(DomDocument);
        DomText.NodeValue := lblFW.Caption;
        DomElement.AppendChild(DomText);
        DomDocument.DocumentElement.AppendChild(DomElement);

        DomElement := TDomElement.Create(DomDocument, SER_NO_TAG_NAME);
        DomText := TDomText.Create(DomDocument);
        DomText.NodeValue := lblSerNo.Caption;
        DomElement.AppendChild(DomText);
        DomDocument.DocumentElement.AppendChild(DomElement);

        UserSettingsDomElement := TDomElement.Create(DomDocument, USER_SETTINGS_TAG_NAME);
        DomDocument.DocumentElement.AppendChild(UserSettingsDomElement);
        
        for i:=0 To Nodes.Count-1 do
        begin
          WaitFrm.gWait.Progress:=i;
          Application.ProcessMessages;
          if (TNode(Nodes.Items[I]).ModbusAdr>2000-1)and(TNode(Nodes.Items[I]).ModbusAdr<3000-1) then
          case (TNode(Nodes.Items[I]).Typ) of
            2,4:          //Edit, Isel
            begin
              {zalohuje se pouze USER Setting bez polozek Password Setup,Modbus Slave Address,Modbus BaudRate,Modbus Parity,Modbus Interframe spacing
                Modbus Response delay,Time - hour,Time - min,Date - day,Date - month,Date - year}
                repeat
                  ReadOK:=MyCommunication.ReadMenuValue(TNode(Nodes.Items[I]).ModbusAdr-1 ,FourByte);
                until(ReadOK)or(WideMessageDlg(GetTranslationText('READ_ERROR', STR_READ_ERROR),mtError, [mbRetry, mbAbort], 0) = mrAbort);
                if ReadOK then begin
                  IntValues4[1] := Integer(FourByte);
                  UserSettingsDomElement.AppendChild(CreateXmlElement(DomDocument, TNode(Nodes.Items[I]), SETTING_TAG_NAME, IntValues4))
                end else
                  Raise Exception.Create(GetTranslationTextToLanguage(lang, 'READ_ERROR', STR_READ_ERROR));  //ukonci cteni
            end;
            3:            //4Win
            begin
              repeat
                ReadOK:=MyCommunication.ReadMenuValue(TNode(Nodes.Items[I]).ModbusAdr-1,FourByte);
              until(ReadOK)or(WideMessageDlg(GetTranslationText('READ_ERROR', STR_READ_ERROR),mtError, [mbRetry, mbAbort], 0) = mrAbort);
              if ReadOK then
                IntValues4[1] := Integer(FourByte)
              else
                Raise Exception.Create(GetTranslationTextToLanguage(lang, 'READ_ERROR', STR_READ_ERROR));  //ukonci cteni

              repeat
                ReadOK:=MyCommunication.ReadMenuValue(TNode(Nodes.Items[I]).ModbusAdr2-1,FourByte);
              until(ReadOK)or(WideMessageDlg(GetTranslationText('READ_ERROR', STR_READ_ERROR),mtError, [mbRetry, mbAbort], 0) = mrAbort);
              if ReadOK then
                IntValues4[2] := Integer(FourByte)
              else
                Raise Exception.Create(GetTranslationTextToLanguage(lang, 'READ_ERROR', STR_READ_ERROR));  //ukonci cteni

              repeat
                ReadOK:=MyCommunication.ReadMenuValue(TNode(Nodes.Items[I]).ModbusAdr3-1,FourByte);
              until(ReadOK)or(WideMessageDlg(GetTranslationText('READ_ERROR', STR_READ_ERROR),mtError, [mbRetry, mbAbort], 0) = mrAbort);
              if ReadOK then
                IntValues4[3] := Integer(FourByte)
              else
                Raise Exception.Create(GetTranslationTextToLanguage(lang, 'READ_ERROR', STR_READ_ERROR));  //ukonci cteni

              repeat
                ReadOK:=MyCommunication.ReadMenuValue(TNode(Nodes.Items[I]).ModbusAdr4-1,FourByte);
              until(ReadOK)or(WideMessageDlg(GetTranslationText('READ_ERROR', STR_READ_ERROR),mtError, [mbRetry, mbAbort], 0) = mrAbort);
              if ReadOK then
                IntValues4[4] := Integer(FourByte)
              else
                Raise Exception.Create(GetTranslationTextToLanguage(lang, 'READ_ERROR', STR_READ_ERROR));  //ukonci cteni

              UserSettingsDomElement.AppendChild(CreateXmlElement(DomDocument, TNode(Nodes.Items[I]), SETTING_TAG_NAME, IntValues4));              
            end;
          end;
        end;

        DomToXMLParser := TDomToXMLParser.Create(nil);
        try
          DomToXMLParser.DomImpl := DomDocument.DomImplementation;
          Stream := TFileStream.Create(SaveDialog2.FileName, fmCreate);
          try
            DomToXMLParser.WriteToStream(DomDocument, 'UTF-8', Stream);
          finally
            Stream.Free;
          end;
        finally
          DomToXMLParser.Free;
        end;

    finally
      FreeAndNil(WaitFrm);
      Screen.Cursor:=LastCursor;
      DomImplementation.Clear;      
    end;

    iniF := TWideIniFile.Create(sExePath+'MagB1.ini');
    try
      iniF.WriteString('BACKUP', 'SavePath', ExtractFilePath(SaveDialog2.FileName) );
      iniF.Save;
    finally
      iniF.Free;
    end;
  end;
  finally
    Self.Enabled :=true;
  end
end;

procedure TformMagB1Menu.btnMenuBackLoadClick(Sender: TObject);
var
    i:Integer;
    FourByte:T4Byte;
    WriteOK:Boolean;
    WaitFrm : TformWait;
    Addr:Integer;
    iniF : TWideIniFile;
    sExePath, sPath: string;
    LastCursor : TCursor;
    DomImplementation : TDomImplementation;
    DomDocument : TDomDocument;
    XMLToDomParser: TXMLToDomParser;
    DomNodeList: TDomNodeList;
    UserSettingsDomElement: TDomElement;
    DomElement: TDomElement;
    Node, FoundNode: TNode;
    IntValues4: TIntValues4;
    lang: TLanguage;    
begin
  inherited;

  lang:= LocalLanguage;
  if lang = langArabic then lang:= langEnglish;

  Self.Enabled :=false;
  try
    sExePath := GetUserAppDataProductPath();
    iniF := TWideIniFile.Create(sExePath+'MagB1.ini');
  try
    sPath := iniF.ReadString('BACKUP', 'OpenPath', sExePath);
  finally
    iniF.Free;
  end;
  OpenDialog2.InitialDir:=sPath;

  //cti heslo
  Addr:=-1;
  for i:=0 To Nodes.Count-1 do
    if TNode(Nodes.Items[I]).ValueName='PASSWORD_USER' then
    begin
        Addr :=TNode(Nodes.Items[I]).ModbusAdr;
        break;
    end;

  if Addr=-1 then
      raise Exception.Create(GetTranslationTextToLanguage(lang, 'INTERNAL_ERROR', STR_INTERNAL_ERROR));

  repeat	//cti zda je zadano heslo;
      WriteOK := MyCommunication.ReadMenuValue( Addr-1 , FourByte)
  until(WriteOK)or(WideMessageDlg(GetTranslationText('READ_ERROR', STR_READ_ERROR),mtError, [mbRetry, mbAbort], 0) = mrAbort);
  if WriteOK and not(Integer(FourByte)=1) then
  begin
    formMagB1Password:=TformMagB1Password.CreateFrm(Self,MyCommunication, Addr, GetTranslationText('USER_PSW', STR_USER_PSW), LocalLanguage);
    try
      case formMagB1Password.ShowModal of
          mrCancel:exit;
      end;
    finally
      FreeAndNil(formMagB1Password);
    end;
  end;

  OpenDialog2.FileName:='MagB1Backup'+lblSerNo.Caption+'.dat';
  if  OpenDialog2.Execute then
  begin
    WaitFrm:=TformWait.Create(Self);
    LastCursor:= Screen.Cursor;
    DomImplementation := TDomImplementation.Create(nil);
    try
      Screen.Cursor:=crHourGlass;
      WaitFrm.gWait.Progress:=0;
      WaitFrm.Show;
      SetCenter(WaitFrm);
      Application.ProcessMessages;

      if Length(ExtractFileExt(OpenDialog2.FileName))=0 then
        OpenDialog2.FileName:=ChangeFileExt(OpenDialog2.FileName,'.dat');

      XMLToDomParser := TXMLToDomParser.Create(nil);
      try
        XMLToDomParser.DomImpl := DomImplementation;
        try
          DomDocument := XMLToDomParser.ParseFile(OpenDialog2.FileName, true);

          DomNodeList := DomDocument.DocumentElement.GetElementsByTagName(USER_SETTINGS_TAG_NAME);
          UserSettingsDomElement := TDomElement(DomNodeList.Item(0));

          DomNodeList := UserSettingsDomElement.GetElementsByTagName(SETTING_TAG_NAME);
          WaitFrm.gWait.MaxValue := DomNodeList.Length-1;

          for i:=0 to DomNodeList.Length-1 do begin
            WaitFrm.gWait.Progress:=i;
            Application.ProcessMessages;
            DomElement := TDomElement(DomNodeList.Item(i));
            Node:= TNode.Create;
            try
              ParseXmlElement(DomElement, Node, IntValues4);
              if (Node.ModbusAdr>2000-1)and(Node.ModbusAdr<3000-1) then begin
                case (Node.Typ) of
                  2,4:          //Edit, Isel
                    begin
                      repeat
                        Integer(FourByte):=IntValues4[1];
                        WriteOK:=MyCommunication.WriteMenuValue(Node.ModbusAdr-1,FourByte);
                      until(WriteOK)or(WideMessageDlg(GetTranslationText('WRITE_ERROR', STR_WRITE_ERROR),mtError, [mbRetry, mbAbort], 0) = mrAbort);
                      if not WriteOK then
                        Raise Exception.Create(GetTranslationTextToLanguage(lang, 'WRITE_ERROR', STR_WRITE_ERROR));  //ukonci zapisu
                    end;
                  3:            //4Win
                    begin
                      //1.polozka
                      repeat
                        Integer(FourByte):=IntValues4[1];
                        WriteOK:=MyCommunication.WriteMenuValue(Node.ModbusAdr-1,FourByte);
                      until(WriteOK)or(WideMessageDlg(GetTranslationText('WRITE_ERROR', STR_WRITE_ERROR),mtError, [mbRetry, mbAbort], 0) = mrAbort);
                      if not WriteOK then
                        Raise Exception.Create(GetTranslationTextToLanguage(lang, 'WRITE_ERROR', STR_WRITE_ERROR));  //ukonci zapisu

                      //2.polozka
                      repeat
                        Integer(FourByte):=IntValues4[2];
                        WriteOK:=MyCommunication.WriteMenuValue(Node.ModbusAdr2-1,FourByte);
                      until(WriteOK)or(WideMessageDlg(GetTranslationText('WRITE_ERROR', STR_WRITE_ERROR),mtError, [mbRetry, mbAbort], 0) = mrAbort);
                      if not WriteOK then
                        Raise Exception.Create(GetTranslationTextToLanguage(lang, 'WRITE_ERROR', STR_WRITE_ERROR));  //ukonci zapisu

                      //3.polozka
                      repeat
                        Integer(FourByte):=IntValues4[3];
                        WriteOK:=MyCommunication.WriteMenuValue(Node.ModbusAdr3-1,FourByte);
                      until(WriteOK)or(WideMessageDlg(GetTranslationText('WRITE_ERROR', STR_WRITE_ERROR),mtError, [mbRetry, mbAbort], 0) = mrAbort);
                      if not WriteOK then
                        Raise Exception.Create(GetTranslationTextToLanguage(lang, 'WRITE_ERROR', STR_WRITE_ERROR));  //ukonci zapisu

                      //4.polozka
                      repeat
                        Integer(FourByte):=IntValues4[4];
                        WriteOK:=MyCommunication.WriteMenuValue(Node.ModbusAdr4-1,FourByte);
                      until(WriteOK)or(WideMessageDlg(GetTranslationText('WRITE_ERROR', STR_WRITE_ERROR),mtError, [mbRetry, mbAbort], 0) = mrAbort);
                      if not WriteOK then
                        Raise Exception.Create(GetTranslationTextToLanguage(lang, 'WRITE_ERROR', STR_WRITE_ERROR));  //ukonci zapisu
                    end;
                end;
              end else
                raise Exception.Create(GetTranslationTextToLanguage(lang, 'READ_ERROR', STR_READ_ERROR));
            finally
              Node.Free;
            end;
          end;
        except
          WideMessageDlg('This backup is not valid for this software',mtError, [mbOk], 0);
          exit;
        end;
      finally
        XMLToDomParser.Free;
      end;
    finally
      FreeAndNil(WaitFrm);
      Screen.Cursor:=LastCursor;
      DomImplementation.Clear;
    end;

    iniF := TWideIniFile.Create(sExePath+'MagB1.ini');
    try
      iniF.WriteString('BACKUP', 'OpenPath', ExtractFilePath(OpenDialog2.FileName) );
      iniF.Save;
    finally
      iniF.Free;
    end;
  end;
  finally
    Self.Enabled := true;
  end;
end;

procedure TformMagB1Menu.IncTime(Sender: TObject);
var
    s:string;
    value:integer;
    direct:boolean;
begin
  if  (time.sec=-1) or (time.min=-1) or (time.hour=-1) then begin
    time.sec:=0;
    time.min:=0;
    time.hour:=0;
  end;
  if TSpeedButton(Sender).tag > 0 then
  begin
    direct:=true;
    value:=TSpeedButton(Sender).tag;
  end else begin
    direct:=false;
    value:=TSpeedButton(Sender).tag*(-1);
  end;


  if direct = true then begin
    case value of
    1: inc(time.sec);
    2: inc(time.min);
    3: inc(time.hour);
    end;
    if time.sec=60 then begin
      time.sec:=0;
      inc(time.min);
    end;
    if time.min=60 then begin
      time.min:=0;
      inc(time.hour);
    end;
    if time.hour=24 then begin
      time.hour:=0;
    end;
  end
  else begin
    case value of
    1: time.sec:=time.sec-1;
    2: time.min:=time.min-1;
    3: time.hour:=time.hour-1;
    end;
    if time.sec=-1 then begin
      time.sec:=59;
      time.min:=time.min-1;
    end;
    if time.min=-1 then begin
      time.min:=59;
      time.hour:=time.hour-1;
    end;
    if time.hour=-1 then begin
      time.hour:=23;
    end;
  end;
  s:=format('%0.2d : %0.2d', [time.hour,time.min]);
  lblTime.Caption :=s;
end;

procedure TformMagB1Menu.SpeedButton3Click(Sender: TObject);
begin
  inherited;
  IncTime(Sender);
end;

procedure TformMagB1Menu.btnReadTimeClick(Sender: TObject);
var Value: Cardinal;
    ReadOK: Boolean;
begin
  if Busy then exit;
  Busy := true;
  Screen.Cursor := crHourGlass;
  try
    repeat
      ReadOK := MyCommunication.ReadRTCTime(Value);
    until (ReadOK) or (WideMessageDlg(GetTranslationText('READ_ERROR', STR_READ_ERROR),mtError, [mbRetry, mbAbort], 0) = mrAbort);
    if ReadOK then begin
      time := IntegerToMyTime(Value);
      lblTime.Caption :=format('%0.2d : %0.2d', [time.hour,time.min]);
      lblTimeMem.caption:=lblTime.Caption;
    end else begin
      WideShowMessage(GetTranslationText('RTC_NOT_AVAIL', STR_RTC_NOT_AVAIL));
    end;
  finally
    Screen.Cursor := crDefault;
    Busy := false;
  end;
end;

procedure TformMagB1Menu.btnWriteTimeClick(Sender: TObject);
var Value: Cardinal;
    WriteOK: Boolean;
begin
  if Busy then exit;
  Busy := True;
  Screen.Cursor := crHourGlass;
  try
    Value := MyTimeToInteger(time);
    repeat
      WriteOK:=MyCommunication.WriteRTCTime(Value);
    until (WriteOK) or (WideMessageDlg(GetTranslationText('WRITE_ERROR', STR_WRITE_ERROR),mtError, [mbRetry, mbAbort], 0) = mrAbort);
    if WriteOK then begin
      lblTimeMem.Caption := format('%0.2d : %0.2d', [time.hour,time.min]);
    end else begin
      WideShowMessage(GetTranslationText('RTC_NOT_AVAIL', STR_RTC_NOT_AVAIL));
    end;
  finally
    Screen.Cursor := crDefault;
    Busy := False;
  end;
end;

procedure TformMagB1Menu.btnReadDateClick(Sender: TObject);
var date: TmyDate;
    Value: Cardinal;
    ReadOK: Boolean;
begin
  if Busy then exit;
  Busy := true;
  Screen.Cursor := crHourGlass;
  try
    repeat
      ReadOK := MyCommunication.ReadRTCDate(Value);
    until (ReadOK) or (WideMessageDlg(GetTranslationText('READ_ERROR', STR_READ_ERROR),mtError, [mbRetry, mbAbort], 0) = mrAbort);
    if ReadOK then begin
      date := IntegerToMyDate(Value);
      lblDateMem.Caption:=format('%0.2d . %0.2d . %0.2d', [date.day, date.month, date.year]);
      mxCalendar1.Date:=EncodeDate(date.year,date.month,date.day);
    end else begin
      WideShowMessage(GetTranslationText('RTC_NOT_AVAIL', STR_RTC_NOT_AVAIL));
    end;
  finally
    Screen.Cursor := crDefault;
    Busy := false;
  end;
end;

procedure TformMagB1Menu.btnWriteDateClick(Sender: TObject);
var date:TmyDate;
    Value: Cardinal;
    WriteOK: Boolean;
begin
  if Busy then exit;
  Busy := True;
  Screen.Cursor := crHourGlass;
  try
    DecodeDate(mxCalendar1.Date, date.year, date.month, date.day);
    Value := MyDateToInteger(date);
    repeat
      WriteOK:=MyCommunication.WriteRTCDate(Value);
    until (WriteOK) or (WideMessageDlg(GetTranslationText('WRITE_ERROR', STR_WRITE_ERROR),mtError, [mbRetry, mbAbort], 0) = mrAbort);
    if WriteOK then begin
      lblDateMem.Caption:=format('%0.2d . %0.2d . %0.2d', [date.day, date.month, date.year]);
    end else begin
      WideShowMessage(GetTranslationText('RTC_NOT_AVAIL', STR_RTC_NOT_AVAIL));
    end;
  finally
    Screen.Cursor := crDefault;
    Busy := False;
  end;
end;

procedure TformMagB1Menu.btnGoTodayClick(Sender: TObject);
begin
  mxCalendar1.Date:=now;
end;

procedure TformMagB1Menu.OnlineTimerTimer(Sender: TObject);
var OnlineData:TOnlineB1;
    I: Integer;
begin
  OnlineTimer.Enabled := False;
  if MyCommunication.ReadOnlineB1(OnlineData, (firmwareNO >= 1027)) then begin
    lblOnlineFlow.Caption :=
      Format('%.3f %s', [ONLINE_FLOW_UNIT_COEFS[cmbOnlineFlowUnit.ItemIndex] * OnlineData.Flow,
                         ONLINE_FLOW_UNIT_TEXTS[cmbOnlineFlowUnit.ItemIndex]]);
    lblOnlineFlow2.Caption :=
      Format('%.3f %s', [ONLINE_FLOW_UNIT_COEFS[cmbOnlineFlowUnit2.ItemIndex] * OnlineData.Flow,
                         ONLINE_FLOW_UNIT_TEXTS[cmbOnlineFlowUnit2.ItemIndex]]);
    lblOnlineTotal.Caption :=
      Format('%.3f %s', [ONLINE_TOTAL_UNIT_COEFS[cmbOnlineTotalUnit.ItemIndex] * OnlineData.Total,
                         ONLINE_TOTAL_UNIT_TEXTS[cmbOnlineTotalUnit.ItemIndex]]);
    lblOnlineTotal2.Caption :=
      Format('%.3f %s', [ONLINE_TOTAL_UNIT_COEFS[cmbOnlineTotalUnit2.ItemIndex] * OnlineData.Total,
                         ONLINE_TOTAL_UNIT_TEXTS[cmbOnlineTotalUnit2.ItemIndex]]);
    lblOnlineTotalPlus.Caption :=
      Format('%.3f %s', [ONLINE_TOTAL_UNIT_COEFS[cmbOnlineTotalUnit.ItemIndex] * OnlineData.TotalPlus,
                         ONLINE_TOTAL_UNIT_TEXTS[cmbOnlineTotalUnit.ItemIndex]]);
    lblOnlineTotalPlus2.Caption :=
      Format('%.3f %s', [ONLINE_TOTAL_UNIT_COEFS[cmbOnlineTotalUnit2.ItemIndex] * OnlineData.TotalPlus,
                         ONLINE_TOTAL_UNIT_TEXTS[cmbOnlineTotalUnit2.ItemIndex]]);
    lblOnlineTotalMinus.Caption :=
      Format('%.3f %s', [ONLINE_TOTAL_UNIT_COEFS[cmbOnlineTotalUnit.ItemIndex] * OnlineData.TotalMinus,
                         ONLINE_TOTAL_UNIT_TEXTS[cmbOnlineTotalUnit.ItemIndex]]);
    lblOnlineTotalMinus2.Caption :=
      Format('%.3f %s', [ONLINE_TOTAL_UNIT_COEFS[cmbOnlineTotalUnit2.ItemIndex] * OnlineData.TotalMinus,
                         ONLINE_TOTAL_UNIT_TEXTS[cmbOnlineTotalUnit2.ItemIndex]]);
    lblOnlineAuxPlus.Caption :=
      Format('%.3f %s', [ONLINE_TOTAL_UNIT_COEFS[cmbOnlineTotalUnit.ItemIndex] * OnlineData.AuxPlus,
                         ONLINE_TOTAL_UNIT_TEXTS[cmbOnlineTotalUnit.ItemIndex]]);
    lblOnlineAuxPlus2.Caption :=
      Format('%.3f %s', [ONLINE_TOTAL_UNIT_COEFS[cmbOnlineTotalUnit2.ItemIndex] * OnlineData.AuxPlus,
                         ONLINE_TOTAL_UNIT_TEXTS[cmbOnlineTotalUnit2.ItemIndex]]);
    lblOnlineErrorCode.Caption := CardinalToBinaryStr16(OnlineData.ErrorCode);
    lblOnlineErrorCode2.Caption := lblOnlineErrorCode.Caption;
    for I := 1 to ERROR_CODE_BYTES_COUNT do begin
      if (((OnlineData.ErrorCode) and (1 shl (I - 1))) = (1 shl (I - 1))) then begin
        ErrorShapeArray[I].Brush.Color := clRed;
      end else begin
        ErrorShapeArray[I].Brush.Color := clWhite;
      end;
    end;
    // Chart
    OnlineChart.SeriesList[0].Delete(0);
    OnlineChart2.SeriesList[0].Delete(0);
    Inc(MaxChartCnt);
    Inc(MinChartCnt);
    if (OnlineData.Flow * CHART_TOLERANCE) > MaxChartValue then begin
      MaxChartValue := OnlineData.Flow * CHART_TOLERANCE;
      MaxChartCnt := 0;
    end;
    if (OnlineData.Flow * CHART_TOLERANCE) < MinChartValue then begin
      MinChartValue := OnlineData.Flow * CHART_TOLERANCE;
      MinChartCnt := 0;
    end;
    OnlineChart.SeriesList[0].AddXY(OnlineChart.SeriesList[0].XValues.Last + 1, OnlineData.Flow);
    OnlineChart2.SeriesList[0].AddXY(OnlineChart2.SeriesList[0].XValues.Last + 1, OnlineData.Flow);
    if (MinChartCnt > GRAPH_ITEMS_COUNT) or (MaxChartCnt > GRAPH_ITEMS_COUNT) then begin
      MaxChartCnt := 0;
      MinChartCnt := 0;
      MaxChartValue := 0;
      MinChartValue := 0;
      for I := OnlineChart.SeriesList[0].Count - 1 downto 0 do begin
        if (OnlineChart.SeriesList[0].YValue[I] * CHART_TOLERANCE) > MaxChartValue then begin
          MaxChartValue := OnlineChart.SeriesList[0].YValue[I] * CHART_TOLERANCE;
          MaxChartCnt := GRAPH_ITEMS_COUNT - I;
        end;
        if (OnlineChart.SeriesList[0].YValue[I] * CHART_TOLERANCE) < MinChartValue then begin
          MinChartValue := OnlineChart.SeriesList[0].YValue[I] * CHART_TOLERANCE;
          MinChartCnt := GRAPH_ITEMS_COUNT - I;
        end;
      end;
    end;
    OnlineChart.LeftAxis.Minimum := MinChartValue;
    OnlineChart.LeftAxis.Maximum := MaxChartValue;
    OnlineChart.RightAxis.Minimum := MinChartValue;
    OnlineChart.RightAxis.Maximum := MaxChartValue;
    OnlineChart2.LeftAxis.Minimum := MinChartValue;
    OnlineChart2.LeftAxis.Maximum := MaxChartValue;
    OnlineChart2.RightAxis.Minimum := MinChartValue;
    OnlineChart2.RightAxis.Maximum := MaxChartValue;
    // ---
    OnlineErr := 0;
  end else begin
    Inc(OnlineErr);
  end;
  if OnlineErr > MAX_ERROR then begin
    WideMessageDlg('Communication error', mtError, [mbRetry], 0);
    OnlineErr:=0;
  end;
  OnlineTimer.Enabled := (pcMenu.ActivePage = tsOnline) or (pcMenu.ActivePage = tsCalibration);
end;

procedure TformMagB1Menu.btnWriteCalDataClick(Sender: TObject);
var FourByte : T4Byte;
    R: Double;
    Idx: Integer;
    lang: TLanguage;
begin
  lang:= LocalLanguage;
  if lang = langArabic then lang:= langEnglish;
  
  CalibrationStatusLabel.Caption:='';
  MeasurmentStatusLabel.Caption:='';
  Idx := TTntButton(Sender).Tag;
  R := 0;
  case Idx of
    1 : R := Invert_Calibration_Flow_Conversion(StrToFloat(CalSpinEdit1.Text),TFlowUnit(cbFlowUnit.ItemIndex));
    2 : R := Invert_Calibration_Flow_Conversion(StrToFloat(CalSpinEdit2.Text),TFlowUnit(cbFlowUnit.ItemIndex));
    3 : R := Invert_Calibration_Flow_Conversion(StrToFloat(CalSpinEdit3.Text),TFlowUnit(cbFlowUnit.ItemIndex));
  end;
  if MyCommunication.WriteMenuValue(GetAddress(ProtokolType, CALIB_VALUE_NAMES[Idx], Values), T4Byte(Cardinal(Round(R*1000)))) then begin
    if MyCommunication.ReadMenuValue(GetAddress(ProtokolType, MEASURE_VALUE_NAMES[Idx], Values), FourByte) then begin
      case Idx of
        1 : MeasSpinEdit1.Text := IntToStr(Integer(FourByte));
        2 : MeasSpinEdit2.Text := IntToStr(Integer(FourByte));
        3 : MeasSpinEdit3.Text := IntToStr(Integer(FourByte));
      end;
    end;
    CalibrationStatusLabel.Caption := Format(GetTranslationTextToLanguage(lang, 'CALIB_POINT_WRITE_SUCC', STR_CALIB_POINT_WRITE_SUCC), [Idx]);
    CalibrationStatusLabel.Font.Color:=clGreen;
  end else begin
    CalibrationStatusLabel.Caption := Format(GetTranslationTextToLanguage(lang, 'CALIB_POINT_WRITE_FAIL', STR_CALIB_POINT_WRITE_FAIL), [Idx]);
    CalibrationStatusLabel.Font.Color:=clRed;
  end;
end;

procedure TformMagB1Menu.btnWriteMeasDataClick(Sender: TObject);
var CalibData: Integer;
    MeasurData: Cardinal;
    FourByte : T4Byte;
    Idx: Integer;
    lang: TLanguage;
begin
  lang:= LocalLanguage;
  if lang = langArabic then lang:= langEnglish;
  
  CalibrationStatusLabel.Caption:='';
  MeasurmentStatusLabel.Caption:='';
  Idx := TTntButton(Sender).Tag;
  CalibData := 0;
  MeasurData := 0;
  case Idx of
    1 : begin
          CalibData := Round(1000 * Invert_Calibration_Flow_Conversion(StrToFloat(CalSpinEdit1.Text),TFlowUnit(cbFlowUnit.ItemIndex)));
          MeasurData := Round(MeasSpinEdit1.Value * ((ErrorSpinEdit1.Value + 100.0) / 100.0));
        end;
    2 : begin
          CalibData := Round(1000 * Invert_Calibration_Flow_Conversion(StrToFloat(CalSpinEdit2.Text),TFlowUnit(cbFlowUnit.ItemIndex)));
          MeasurData := Round(MeasSpinEdit2.Value * ((ErrorSpinEdit2.Value + 100.0) / 100.0));
        end;
    3 : begin
          CalibData := Round(1000 * Invert_Calibration_Flow_Conversion(StrToFloat(CalSpinEdit3.Text),TFlowUnit(cbFlowUnit.ItemIndex)));
          MeasurData := Round(MeasSpinEdit3.Value * ((ErrorSpinEdit3.Value + 100.0) / 100.0));
        end;
  end;
  if MyCommunication.WriteMenuValue(GetAddress(ProtokolType, CALIB_VALUE_NAMES[Idx], Values), T4Byte(CalibData)) and
     MyCommunication.WriteMenuValue(GetAddress(ProtokolType, MEASURE_VALUE_NAMES[Idx], Values), T4Byte(MeasurData)) and
     MyCommunication.ReadMenuValue(GetAddress(ProtokolType, MEASURE_VALUE_NAMES[Idx], Values), FourByte) then
  begin
    case Idx of
      1 : begin
            MeasSpinEdit1.Text := IntToStr(Integer(FourByte));
            ErrorSpinEdit1.Value := 0;
          end;
      2 : begin
            MeasSpinEdit2.Text := IntToStr(Integer(FourByte));
            ErrorSpinEdit2.Value := 0;
          end;
      3 : begin
            MeasSpinEdit3.Text := IntToStr(Integer(FourByte));
            ErrorSpinEdit3.Value := 0;
          end;
    end;
    MeasurmentStatusLabel.Caption := Format(GetTranslationTextToLanguage(lang, 'MEAS_POINT_WRITE_SUCC', STR_MEAS_POINT_WRITE_SUCC), [Idx]);
    MeasurmentStatusLabel.Font.Color:=clGreen;
  end else begin
    MeasurmentStatusLabel.Caption := Format(GetTranslationTextToLanguage(lang, 'MEAS_POINT_WRITE_FAIL', STR_MEAS_POINT_WRITE_FAIL), [Idx]);
    MeasurmentStatusLabel.Font.Color:=clRed;
  end;
end;

procedure TformMagB1Menu.btnAutomaticZeroConstClick(Sender: TObject);
var FourByte: T4Byte;
    WaitFrm: TformWait;
    I: Integer;
begin
  Integer(FourByte) := 1;
  if MyCommunication.WriteMenuValue(GetAddress(ProtokolType, ZERO_FLOW_NAME, Values), FourByte) then begin
    CalibrationStatusLabel.Caption := GetTranslationText('PLS_WAIT', STR_PLS_WAIT);
    Application.ProcessMessages;
    // Wait
    Self.Enabled := False;
    try
      WaitFrm := TformWait.Create(Self);
      try
        Screen.Cursor := crHourGlass;
        WaitFrm.gWait.Progress := 0;
        WaitFrm.Show;
        SetCenter(WaitFrm);
        WaitFrm.caption:=GetTranslationText('PLS_WAIT', STR_PLS_WAIT);
        for I := 0 to 20 do begin
          WaitFrm.gWait.Progress := I * 5;
          Application.ProcessMessages;
          Sleep(1000);
        end;
      finally
        Screen.Cursor := crDefault;
        FreeAndNil(WaitFrm);
      end;
    finally
      Self.Enabled := True;
    end;
    // ---
    if MyCommunication.ReadMenuValue(GetAddress(ProtokolType, ZERO_FLOW_CONST_NAME, Values), FourByte) then begin
      ZeroConstSpinEdit.Value := Integer(FourByte);
      CalibrationStatusLabel.Caption := GetTranslationText('ZERO_FLOW_WRITE_SUCC', STR_ZERO_FLOW_WRITE_SUCC);
      CalibrationStatusLabel.Font.Color:=clGreen;
    end else begin
      CalibrationStatusLabel.Caption := GetTranslationText('READ_FAIL', STR_READ_FAIL);
      CalibrationStatusLabel.Font.Color:=clRed;
    end;
  end else begin
    CalibrationStatusLabel.Caption := GetTranslationText('ZERO_FLOW_WRITE_FAIL', STR_ZERO_FLOW_WRITE_FAIL);
    CalibrationStatusLabel.Font.Color:=clRed;
  end;
end;

procedure TformMagB1Menu.btnManualZeroConstClick(Sender: TObject);
var FourByte: T4Byte;
begin
  Integer(FourByte) := ZeroConstSpinEdit.Value;
  if MyCommunication.WriteMenuValue(GetAddress(ProtokolType, ZERO_FLOW_CONST_NAME, Values), FourByte) then begin
    CalibrationStatusLabel.Caption := GetTranslationText('ZERO_FLOW_WRITE_SUCC', STR_ZERO_FLOW_WRITE_SUCC);
    CalibrationStatusLabel.Font.Color:=clGreen;
  end else begin
    CalibrationStatusLabel.Caption := GetTranslationText('ZERO_FLOW_WRITE_FAIL', STR_ZERO_FLOW_WRITE_FAIL);
    CalibrationStatusLabel.Font.Color:=clRed;
  end;
end;

procedure TformMagB1Menu.btnMeasurementCalculateClick(Sender: TObject);
var a,b: double;
begin
  inherited;
    a  := 0;
    if (CalSpinEdit2.Value-CalSpinEdit1.Value) <> 0 then a  := (MeasSpinEdit2.Value-MeasSpinEdit1.Value)/(CalSpinEdit2.Value-CalSpinEdit1.Value); // vypocet hodnoty a pro rovnici primky
    b  := MeasSpinEdit2.Value-(CalSpinEdit2.Value * a);
    MeasSpinEdit3.Value:= a*CalSpinEdit3.Value + b;
end;

procedure TformMagB1Menu.btnReadAllClick(Sender: TObject);
var FourByte: T4Byte;
    WaitFrm: TformWait;
    I: Integer;
    lang: TLanguage;
begin
  lang:= LocalLanguage;
  if lang = langArabic then lang:= langEnglish;

  if not Busy then
  try
    Busy:=true;
    WaitFrm:=TformWait.Create(Self);
    Screen.Cursor:=crHourGlass;
    WaitFrm.gWait.Progress:=0;
    WaitFrm.Show;
    SetCenter(WaitFrm);
    WaitFrm.caption:=GetTranslationText('PLS_WAIT', STR_PLS_WAIT);
    Application.ProcessMessages;

    CalibrationStatusLabel.Caption:='';
    MeasurmentStatusLabel.Caption:='';

    try
      // Calib
      for I := 1 to 3 do begin
        if MyCommunication.ReadMenuValue(GetAddress(ProtokolType, CALIB_VALUE_NAMES[I], Values), FourByte) then begin
          case I of
            1 : CalSpinEdit1.Text := FormatFloat('0.000', Calibration_Flow_Conversion(Integer(FourByte) / 1000.0, TFlowUnit(cbFlowUnit.ItemIndex)));
            2 : CalSpinEdit2.Text := FormatFloat('0.000', Calibration_Flow_Conversion(Integer(FourByte) / 1000.0, TFlowUnit(cbFlowUnit.ItemIndex)));
            3 : CalSpinEdit3.Text := FormatFloat('0.000', Calibration_Flow_Conversion(Integer(FourByte) / 1000.0, TFlowUnit(cbFlowUnit.ItemIndex)));
          end;
          WaitFrm.gWait.Progress := I * 20;
          Application.ProcessMessages;
        end else begin
          raise Exception.Create(GetTranslationTextToLanguage(lang, 'READ_ERROR', STR_READ_ERROR));  //ukonci cteni
        end;
      end;
      // Zero Const
      if MyCommunication.ReadMenuValue(GetAddress(ProtokolType, ZERO_FLOW_CONST_NAME, Values), FourByte) then begin
        ZeroConstSpinEdit.Value := Integer(FourByte);
        WaitFrm.gWait.Progress := 70;
        Application.ProcessMessages;
      end;
      // Measure
      for I := 1 to 3 do begin
        if MyCommunication.ReadMenuValue(GetAddress(ProtokolType, MEASURE_VALUE_NAMES[I], Values), FourByte) then begin
          case I of
            1 : MeasSpinEdit1.Text := IntToStr(Integer(FourByte));
            2 : MeasSpinEdit2.Text := IntToStr(Integer(FourByte));
            3 : MeasSpinEdit3.Text := IntToStr(Integer(FourByte));
          end;
          WaitFrm.gWait.Progress := 70 + I * 10;
          Application.ProcessMessages;
        end else begin
          raise Exception.Create(GetTranslationTextToLanguage(lang, 'READ_ERROR', STR_READ_ERROR));  //ukonci cteni
        end;
      end;
      CalibrationStatusLabel.Caption:=GetTranslationText('READ_SUCCESS', STR_READ_SUCC);
      CalibrationStatusLabel.Font.Color:=clGreen;
    except
      CalibrationStatusLabel.Caption:=GetTranslationText('READ_FAILED', STR_READ_FAIL);
      CalibrationStatusLabel.Font.Color:=clRed;
    end;
  finally
    FreeAndNil(WaitFrm);
    Screen.Cursor:=crDefault;
  Busy:=false;
  end;
end;

procedure TformMagB1Menu.btnWriteAllClick(Sender: TObject);
var CalibData, MeasurData: Cardinal;
    WaitFrm : TformWait;
    I: Integer;
    lang: TLanguage;
begin
  lang:= LocalLanguage;
  if lang = langArabic then lang:= langEnglish;

  if not Busy then
  try
    Busy:=true;
    WaitFrm:=TformWait.Create(Self);
    Screen.Cursor:=crHourGlass;
    WaitFrm.gWait.Progress:=0;
    WaitFrm.Show;
    SetCenter(WaitFrm);
    WaitFrm.caption:=GetTranslationText('PLS_WAIT', STR_PLS_WAIT);
    Application.ProcessMessages;
    CalibrationStatusLabel.Caption:='';
    MeasurmentStatusLabel.Caption:='';
    try
      // Calib
      for I := 1 to 3 do begin
        case I of
          1 : CalibData := Round(1000 * Invert_Calibration_Flow_Conversion(CalSpinEdit1.Value, TFlowUnit(cbFlowUnit.ItemIndex)));
          2 : CalibData := Round(1000 * Invert_Calibration_Flow_Conversion(CalSpinEdit2.Value, TFlowUnit(cbFlowUnit.ItemIndex)));
          3 : CalibData := Round(1000 * Invert_Calibration_Flow_Conversion(CalSpinEdit3.Value, TFlowUnit(cbFlowUnit.ItemIndex)));
        end;
        if MyCommunication.WriteMenuValue(GetAddress(ProtokolType, CALIB_VALUE_NAMES[I], Values), T4Byte(CalibData)) then begin
          WaitFrm.gWait.Progress := I * 20;
          Application.ProcessMessages;
        end else begin
          raise Exception.Create(GetTranslationTextToLanguage(lang, 'WRITE_ERROR', STR_WRITE_ERROR));  //ukonci zapis
        end;
      end;
      // Zero Const
      if MyCommunication.WriteMenuValue(GetAddress(ProtokolType, ZERO_FLOW_CONST_NAME, Values), T4Byte(ZeroConstSpinEdit.Value)) then begin
        WaitFrm.gWait.Progress := 70;
        Application.ProcessMessages;
      end else begin
        raise Exception.Create(GetTranslationTextToLanguage(lang, 'WRITE_ERROR', STR_WRITE_ERROR));  //ukonci zapis
      end;
      // Measure

      for I := 1 to 3 do begin
        case I of
          1 : MeasurData := Round(MeasSpinEdit1.Value);
          2 : MeasurData := Round(MeasSpinEdit2.Value);
          3 : MeasurData := Round(MeasSpinEdit3.Value);
        end;
        if MyCommunication.WriteMenuValue(GetAddress(ProtokolType, MEASURE_VALUE_NAMES[I], Values), T4Byte(MeasurData)) then begin
          WaitFrm.gWait.Progress := 70 + I * 10;
          Application.ProcessMessages;
        end else begin
          raise Exception.Create(GetTranslationTextToLanguage(lang, 'WRITE_ERROR', STR_WRITE_ERROR));  //ukonci zapis
        end;
      end;
      MeasurmentStatusLabel.Caption := GetTranslationText('MEAS_POINTS_WRITE_SUCC', STR_MEAS_POINTS_WRITE_SUCC);
      MeasurmentStatusLabel.Font.Color:=clGreen;
    except
      MeasurmentStatusLabel.Caption := GetTranslationText('MEAS_POINTS_WRITE_FAIL', STR_MEAS_POINTS_WRITE_FAIL);
      MeasurmentStatusLabel.Font.Color:=clRed;
    end;
  finally
    FreeAndNil(WaitFrm);
    Screen.Cursor:=crDefault;
    Busy:=false;
  end;
end;

procedure TformMagB1Menu.btnOpenDataFileClick(Sender: TObject);
var iniF : TWideIniFile;
    sExePath, sPath, tmp:string;
begin
  sExePath := GetUserAppDataProductPath();
  iniF := TWideIniFile.Create(sExePath+'MagB1.ini');
  try
    sPath := iniF.ReadString('CALIBRATION', 'OpenPath', sExePath);
  finally
    iniF.Free;
  end;

  OpenDialog1.InitialDir:=sPath;
  if OpenDialog1.Execute then
  begin
    iniF:=TWideIniFile.Create(OpenDialog1.FileName);
    try
      tmp:=iniF.ReadString('CALIBRATION','CalibrationPoint1','0');
      CalSpinEdit1.Text:=FormatFloat('0.000', Calibration_Flow_Conversion(StrToFloat(tmp),TFlowUnit(cbFlowUnit.ItemIndex)));

      tmp:=iniF.ReadString('CALIBRATION','CalibrationPoint2','0');
      CalSpinEdit2.Text:=FormatFloat('0.000', Calibration_Flow_Conversion(StrToFloat(tmp),TFlowUnit(cbFlowUnit.ItemIndex)));

      tmp:=iniF.ReadString('CALIBRATION','CalibrationPoint3','0');
      CalSpinEdit3.Text:=FormatFloat('0.000', Calibration_Flow_Conversion(StrToFloat(tmp),TFlowUnit(cbFlowUnit.ItemIndex)));

      MeasSpinEdit1.Text:=iniF.ReadString('CALIBRATION','MeasuredPoint1','0');
      MeasSpinEdit2.Text:=iniF.ReadString('CALIBRATION','MeasuredPoint2','0');
      MeasSpinEdit3.Text:=iniF.ReadString('CALIBRATION','MeasuredPoint3','0');

      ZeroConstSpinEdit.Text:=iniF.ReadString('CALIBRATION','ZeroFlowConst','0');
    finally
     iniF.Free;
    end;

    iniF := TWideIniFile.Create(sExePath+'MagB1.ini');
    try
      iniF.WriteString('CALIBRATION', 'OpenPath', ExtractFilePath(OpenDialog1.FileName) );
      iniF.Save;
    finally
      iniF.Free;
    end;
  end;
end;

procedure TformMagB1Menu.btnSaveDataFileClick(Sender: TObject);
var iniF : TWideIniFile;
    R:double;
    sExePath, sPath: string;
begin
  sExePath := GetUserAppDataProductPath();
  iniF := TWideIniFile.Create(sExePath+'MagB1.ini');
  try
    sPath := iniF.ReadString('CALIBRATION', 'SavePath', sExePath);
  finally
    iniF.Free;
  end;

  SaveDialog1.FileName:='Calibration'+lblSerNo.Caption+'.ini';
  SaveDialog1.InitialDir:=sPath;
  if SaveDialog1.Execute then
  begin
    if Length(ExtractFileExt(SaveDialog1.FileName))=0 then
      ChangeFileExt(SaveDialog1.FileName,'ini');
    iniF:=TWideIniFile.Create(SaveDialog1.FileName);
    try
      R := Invert_Calibration_Flow_Conversion(StrToFloat(CalSpinEdit1.Text),TFlowUnit(cbFlowUnit.ItemIndex));
      iniF.WriteString('CALIBRATION','CalibrationPoint1',FloatToStr(R));

      R := Invert_Calibration_Flow_Conversion(StrToFloat(CalSpinEdit2.Text),TFlowUnit(cbFlowUnit.ItemIndex));
      iniF.WriteString('CALIBRATION','CalibrationPoint2',FloatToStr(R));

      R := Invert_Calibration_Flow_Conversion(StrToFloat(CalSpinEdit3.Text),TFlowUnit(cbFlowUnit.ItemIndex));
      iniF.WriteString('CALIBRATION','CalibrationPoint3',FloatToStr(R));

      iniF.WriteString('CALIBRATION','MeasuredPoint1',MeasSpinEdit1.Text);
      iniF.WriteString('CALIBRATION','MeasuredPoint2',MeasSpinEdit2.Text);
      iniF.WriteString('CALIBRATION','MeasuredPoint3',MeasSpinEdit3.Text);

      iniF.WriteString('CALIBRATION','ZeroFlowConst',ZeroConstSpinEdit.Text);

      iniF.Save;
    finally
     iniF.Free;
    end;

    iniF := TWideIniFile.Create(sExePath+'MagB1.ini');
    try
      iniF.WriteString('CALIBRATION', 'SavePath', ExtractFilePath(SaveDialog1.FileName) );
      iniF.Save;
    finally
      iniF.Free;
    end;
  end;
end;

procedure TformMagB1Menu.pcMenuChanging(Sender: TObject;
  var AllowChange: Boolean);
begin
//pokud je aktivni online tak zastav
  if (pcMenu.ActivePage=tsOnline) or (pcMenu.ActivePage=tsCalibration) then
  begin
    if(MyCommunication is TMyCommPort) then
    begin
      OnlineTimer.enabled:=false;
      TMyCommPort(MyCommunication).WriteOnline(0);
      OnlinePocitadlo:=0;
    end
    else if MyCommunication is TMyModbus then
    begin
      OnlineTimer.enabled:=false;
    end;
  end;
  TabIndexPred:=TTntPageControl(sender).ActivePageIndex;
end;

procedure TformMagB1Menu.pcMenuChange(Sender: TObject);
var PassForm:TformMagB1Pass;
    i:Integer;
begin
//Online
  if (pcMenu.ActivePage=tsOnline)then
  begin
    OnlinePocitadlo:=1;
    OnlineTimer.enabled:=true;

    MinChartCnt := GRAPH_ITEMS_COUNT;
    MaxChartCnt := GRAPH_ITEMS_COUNT;

    OnlineChart.SeriesList[0].Clear;
    OnlineChart2.SeriesList[0].Clear;
    for I := 0 to GRAPH_ITEMS_COUNT - 1 do begin
      OnlineChart.SeriesList[0].AddXY(I, 0);
      OnlineChart2.SeriesList[0].AddXY(I, 0);
    end;
    if(MyCommunication is TMyCommPort)then
    begin
      TMyCommPort(MyCommunication).FlushBuffers(True,False);
      TMyCommPort(MyCommunication).WriteOnline(1);
    end;
  end
//Kalibrace
  else if pcMenu.ActivePage=tsCalibration then
  begin
    CalSpinEdit1.SetFocus;
    if firmwareNO<107 then      //podle firmware zobraz kalibraci se zadavanim prutoku
    begin
      Panel6.Visible:=false;
      btnWriteAll.Visible:=false;
    end
    else
    begin
      Panel6.Visible:=true;
      btnWriteAll.Visible:=true;
    end;

    if secure[3] then   //pokud bylo zadano heslo cti vsechno
    begin
      btnReadAll.Click;
      OnlinePocitadlo:=1;
      OnlineTimer.enabled:=true;

      MinChartCnt := GRAPH_ITEMS_COUNT;
      MaxChartCnt := GRAPH_ITEMS_COUNT;

      OnlineChart.SeriesList[0].Clear;
      OnlineChart2.SeriesList[0].Clear;
      for I := 0 to GRAPH_ITEMS_COUNT - 1 do begin
        OnlineChart.SeriesList[0].AddXY(I, 0);
        OnlineChart2.SeriesList[0].AddXY(I, 0);
      end;
      if(MyCommunication is TMyCommPort)then
      begin
        TMyCommPort(MyCommunication).FlushBuffers(True,False);
        TMyCommPort(MyCommunication).WriteOnline(1);
      end;
    end
    else
    begin               //jinak zobraz okno hesla
      PassForm:=TformMagB1Pass.CreateFrm(self,3,MyCommunication,Values,ProtokolType, LocalLanguage);
      SetCenter(PassForm);
      case PassForm.Showmodal of
        mrOk:
        begin
          Secure[3]:=true;
          pcMenuChange(Sender);
        end;
        mrCancel:
        begin
         pcMenu.ActivePageIndex:=TabIndexPred;
         pcMenuChange(Sender);
        end;
      end;
    end;
  end
  //Firmware update
  else if pcMenu.ActivePage=tsUpdateFirmware then
  begin
    if secure[3] then   //pokud bylo zadano heslo cti vsechno
    begin
      // OK - Continue
    end
    else
    begin               //jinak zobraz okno hesla
      PassForm:=TformMagB1Pass.CreateFrm(self,3,MyCommunication,Values,ProtokolType, LocalLanguage);
      SetCenter(PassForm);
      case PassForm.Showmodal of
        mrOk:
        begin
          Secure[3]:=true;
          pcMenuChange(Sender);
        end;
        mrCancel:
        begin
         pcMenu.ActivePageIndex:=TabIndexPred;
         pcMenuChange(Sender);
        end;
      end;
    end;
  end
  else
//Time, Date, GPRS
  if (pcMenu.ActivePage=tsDate)or(pcMenu.ActivePage=tsTime) then
  begin
    if ((firmwareNO<1034) and Secure[1]) or ((firmwareNO>=1034) and Secure[3]) then begin
    end else begin
      //zabrazeni dialogu hesla
      if firmwareNO<1034 then
        PassForm:=TformMagB1Pass.CreateFrm(self,1,MyCommunication,Values,ProtokolType, LocalLanguage)
      else
        PassForm:=TformMagB1Pass.CreateFrm(self,3,MyCommunication,Values,ProtokolType, LocalLanguage);
      SetCenter(PassForm);
      case PassForm.Showmodal of
        mrCancel:
        begin
          pcMenu.ActivePageIndex:=TabIndexPred;
          pcMenuChange(Sender);
        end;
        mrOK:
        begin
          if firmwareNO<1034 then
            Secure[1]:=true
          else
            Secure[3]:=true;
          pcMenuChange(Sender);
        end
      end;
    end;
  end;
end;

procedure TformMagB1Menu.TranslateForm;
var
  lang: TLanguage;
begin
  lang:= LocalLanguage;
  if lang = langArabic then lang:= langEnglish;

  // Page control
  tsEdit.Caption := GetTranslationText('MENU', tsEdit.Caption);
  tsTime.Caption := GetTranslationText('TIME', tsTime.Caption);
  tsDate.Caption := GetTranslationText('DATE', tsDate.Caption);
  tsOnline.Caption := GetTranslationText('REAL_TIME', tsOnline.Caption);
  tsCalibration.Caption := GetTranslationText('CALIB', tsCalibration.Caption);
  tsUpdateFirmware.Caption := GetTranslationText('FW_UPDATE', tsUpdateFirmware.Caption);
  btnMenuBackLoad.Caption := GetTranslationText('LOAD_BCK_FILE', btnMenuBackLoad.Caption);
  btnMenuBackSave.Caption := GetTranslationText('SAVE_BCK_FILE', btnMenuBackSave.Caption);
  // Date/Time
  btnReadTime.Caption := GetTranslationText('READ_TIME', btnReadTime.Caption);
  btnWriteTime.Caption := GetTranslationText('WRITE_TIME', btnWriteTime.Caption);
  btnReadDate.Caption := GetTranslationText('READ_DATE', btnReadDate.Caption);
  btnWriteDate.Caption := GetTranslationText('WRITE_DATE', btnWriteDate.Caption);
  btnGoToday.Caption := GetTranslationText('GO_TODAY', btnGoToday.Caption);
  lblNowInMemTime.Caption := GetTranslationText('NOW_IN_MEM', lblNowInMemTime.Caption);
  lblNowInMemDate.Caption := GetTranslationText('NOW_IN_MEM', lblNowInMemDate.Caption);
  lblTimeMem.Caption := GetTranslationText('UNKNOWN', lblTimeMem.Caption);
  lblDateMem.Caption := GetTranslationText('UNKNOWN', lblDateMem.Caption);
  // Online
  lblFlow.Caption := GetTranslationText('FLOW', lblFlow.Caption);
  lblTotalPlus.Caption := GetTranslationText('TOTAL_PLUS', lblTotalPlus.Caption);
  lblTotalMinus.Caption := GetTranslationText('TOTAL_MINUS', lblTotalMinus.Caption);
  lblTotal.Caption := GetTranslationText('TOTAL', lblTotal.Caption);
  lblAuxPlus.Caption := GetTranslationText('AUX_PLUS', lblAuxPlus.Caption);
  lblErrorCode.Caption := GetTranslationText('ERROR_CODE', lblErrorCode.Caption);
  OnlineChart.Title.Text.Text := GetTranslationTextToLanguage(lang, 'ACTUAL_FLOW', OnlineChart.Title.Text.Text);
  OnlineChart.RightAxis.Title.Caption := GetTranslationTextToLanguage(lang, 'FLOW_UNIT', OnlineChart.RightAxis.Title.Caption);
  // Calibration
  lblCalibrationData.Caption := GetTranslationText('CALIB_DATA', lblCalibrationData.Caption);
  lblUnit.Caption := GetTranslationText('UNIT', lblUnit.Caption);
  btnWriteCalData1.Caption := GetTranslationText('CALIB_WRITE_DATA1', btnWriteCalData1.Caption);
  btnWriteCalData2.Caption := GetTranslationText('CALIB_WRITE_DATA2', btnWriteCalData2.Caption);
  btnWriteCalData3.Caption := GetTranslationText('CALIB_WRITE_DATA3', btnWriteCalData3.Caption);
  btnAutomaticZeroConst.Caption := GetTranslationText('AUTOMATIC_ZERO_CONST', btnAutomaticZeroConst.Caption);
  btnManualZeroConst.Caption := GetTranslationText('MANUAL_ZERO_CONST', btnManualZeroConst.Caption);
  lblMeasurementData.Caption := GetTranslationText('MEAS_DATA', lblMeasurementData.Caption);
  lblError.Caption := GetTranslationText('ERROR_PROC', lblError.Caption);
  btnWriteMeasData1.Caption := GetTranslationText('MEAS_WRITE_DATA1', btnWriteMeasData1.Caption);
  btnWriteMeasData2.Caption := GetTranslationText('MEAS_WRITE_DATA2', btnWriteMeasData2.Caption);
  btnWriteMeasData3.Caption := GetTranslationText('MEAS_WRITE_DATA3', btnWriteMeasData3.Caption);
  btnMeasurementCalculate.Caption := GetTranslationText('CALC_MEAS_POINT', btnMeasurementCalculate.Caption);
  btnReadAll.Caption := GetTranslationText('READ_ALL', btnReadAll.Caption);
  btnWriteAll.Caption := GetTranslationText('WRITE_ALL', btnWriteAll.Caption);
  btnOpenDataFile.Caption := GetTranslationText('OPEN_DATA', btnOpenDataFile.Caption);
  btnSaveDataFile.Caption := GetTranslationText('SAVE_DATA', btnSaveDataFile.Caption);
  lblFlow2.Caption := GetTranslationText('FLOW', lblFlow2.Caption);
  lblTotalPlus2.Caption := GetTranslationText('TOTAL_PLUS', lblTotalPlus2.Caption);
  lblTotalMinus2.Caption := GetTranslationText('TOTAL_MINUS', lblTotalMinus2.Caption);
  lblTotal2.Caption := GetTranslationText('TOTAL', lblTotal2.Caption);
  lblAuxPlus2.Caption := GetTranslationText('AUX_PLUS', lblAuxPlus2.Caption);
  lblErrorCode2.Caption := GetTranslationText('ERROR_CODE', lblErrorCode2.Caption);
  OnlineChart2.Title.Text.Text := GetTranslationTextToLanguage(lang, 'ACTUAL_FLOW', OnlineChart2.Title.Text.Text);
  OnlineChart2.RightAxis.Title.Caption := GetTranslationTextToLanguage(lang, 'FLOW_UNIT', OnlineChart2.RightAxis.Title.Caption);
  // FW Update
  lblUpdateFirmwareFile.Caption := GetTranslationText('FW_UPDATE_FILE', lblUpdateFirmwareFile.Caption);
  lblUpdateFirmwareComment.Caption := GetTranslationText('FW_UPDATE_COMMENT', lblUpdateFirmwareComment.Caption);
  btnUpdateFirmware.Caption := GetTranslationText('FW_UPDATE_BTN', btnUpdateFirmware.Caption);
end;

procedure TformMagB1Menu.btnUpdateFWFileSelectClick(Sender: TObject);
var OpenDlg: TTntOpenDialog;
begin
  inherited;
  OpenDlg := TTntOpenDialog.Create(nil);
  try
    OpenDlg.Filter := GetTranslationText('FW_UPDATE_FILTER', STR_FW_UPDATE_FILTER);
    OpenDlg.DefaultExt := 'MGB';
    OpenDlg.FileName := edtUpdateFirmwareFile.Text;
    if OpenDlg.Execute then begin
      edtUpdateFirmwareFile.Text := OpenDlg.FileName;
    end;
  finally
    OpenDlg.Free;
  end;
end;

procedure TformMagB1Menu.btnUpdateFirmwareClick(Sender: TObject);
var MyXModem: TXModem;
    MyStream: TMemoryStream;
    ZipMaster: TZipMaster;
    APort:Cardinal;
    AID:Cardinal;
    ABaudRate:TBaudRate;
    ADataBits:TDataBits;
    AStopBits:TStopBits;
    AParity:TParity;
    ARtsControl:TRtsControl;
    AEnableDTROnOpen:Boolean;
    ATimeout:Cardinal;
    AIPAddress:String;
    AIPPort:Integer;
    lang: TLanguage;
begin
  inherited;

  lang:= LocalLanguage;
  if lang = langArabic then lang:= langEnglish;
    
  if WideMessageDlg(Format(GetTranslationTextToLanguage(lang, 'FW_UPDATE_CONF', STR_FW_UPDATE_CONF),
                       [edtUpdateFirmwareFile.Text]), mtConfirmation, [mbYes, mbNo], 0) = mrYes then
  begin
    ZipMaster := TZipMaster.Create(nil);
    MyXModem := TXModem.Create(nil);
    try
      lblUpdateFirmwareStatus.Caption := 'Extracting file ...';
      Application.ProcessMessages;
      ZipMaster.ZipFileName := edtUpdateFirmwareFile.Text;
      MyStream := ZipMaster.ExtractFileToStream(ExtractFileName(ChangeFileExt(edtUpdateFirmwareFile.Text, '.bin')));
      if Assigned(MyStream) then begin
        lblUpdateFirmwareStatus.Caption := 'Device reset ...';
        Application.ProcessMessages;
        progUpdateFirmware.Max:= (MyStream.Size div 127);
        progUpdateFirmware.Position:=0;
        with MyXModem do begin
          MyCommunication.GetCommunicationParam(APort,AID,ABaudRate,ADataBits,AStopBits,AParity,ARtsControl,AEnableDTROnOpen,ATimeout,AIPAddress,AIPPort);
          ComPort := MyCommunication.GetComPort;
          ComPortSpeed := br115200;
          ComPortDataBits := db8BITS;
          ComPortStopBits :=sb1BITS;
          ComPortParity := ptNONE;
          ComPortRtsControl := rcRTSDISABLE;
          ComPortHwHandshaking := hhNONE;
          Timeout := ATimeout;
          EnableDTROnOpen := false;
        end;
        // RESET
        MyCommunication.WriteReset;
        MyCommunication.Disconnect;
        lblUpdateFirmwareStatus.Caption := 'Connecting device ...';
        Application.ProcessMessages;
        if MyXModem.Connect then begin
          lblUpdateFirmwareStatus.Caption := 'Updating firmware ...';
          Application.ProcessMessages;
          if MyXModem.SendFromStream(MyStream, progUpdateFirmware) then begin
            WideMessageDlg(GetTranslationText('FW_UPDATE_DONE', STR_FW_UPDATE_DONE) + #13#10 +
                       GetTranslationText('APP_RESTART', STR_APP_RESTART), mtInformation, [mbOK], 0);
            progUpdateFirmware.Position:=0;
            MyXModem.Disconnect();
          end else begin
            WideMessageDlg(GetTranslationText('FW_UPDATE_FAIL', STR_FW_UPDATE_FAIL) + #13#10 +
                       GetTranslationText('APP_RESTART', STR_APP_RESTART), mtError, [mbOK], 0);
            MyXModem.Disconnect();
          end;
        end else begin
          WideMessageDlg(GetTranslationText('FW_UPDATE_SKIP', STR_FW_UPDATE_SKIP) + #13#10 +
                     GetTranslationText('APP_RESTART', STR_APP_RESTART), mtWarning, [mbOK], 0);
        end;
        Konec := True;
        Close;
        MainForm.SetMagB1RunRequest;
      end;
    finally
      FreeAndNil(MyXModem);
      FreeAndNil(ZipMaster);
      lblUpdateFirmwareStatus.Caption := '-';
    end;
  end;
end;

procedure TformMagB1Menu.edtUpdateFirmwareFileChange(Sender: TObject);
begin
  inherited;
  btnUpdateFirmware.Enabled := (edtUpdateFirmwareFile.Text <> '');
end;

procedure TformMagB1Menu.WMNeedReset(var Message: TMessage);
begin
  NeedReset := True;
  Close;
end;

procedure TformMagB1Menu.EnableEvents;
begin
  EventsEnabled:= True;
end;

end.
