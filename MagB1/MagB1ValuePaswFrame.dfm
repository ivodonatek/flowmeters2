inherited frameMagB1ValuePasw: TframeMagB1ValuePasw
  OnEnter = FrameEnter
  object Panel1: TPanel
    Left = 24
    Top = 40
    Width = 313
    Height = 41
    BevelOuter = bvLowered
    Color = 14670037
    TabOrder = 0
    object eValue: TEdit
      Left = 16
      Top = 9
      Width = 113
      Height = 21
      AutoSelect = False
      BiDiMode = bdLeftToRight
      MaxLength = 8
      ParentBiDiMode = False
      PasswordChar = '*'
      TabOrder = 0
    end
    object btnOK: TTntButton
      Left = 152
      Top = 8
      Width = 73
      Height = 25
      BiDiMode = bdLeftToRight
      Caption = 'OK'
      Default = True
      ParentBiDiMode = False
      TabOrder = 1
      OnClick = btnConfirmClick
    end
    object btnCancel: TTntButton
      Left = 224
      Top = 8
      Width = 73
      Height = 25
      BiDiMode = bdLeftToRight
      Cancel = True
      Caption = 'Cancel'
      ParentBiDiMode = False
      TabOrder = 2
      OnClick = btnCancelClick
    end
  end
end
