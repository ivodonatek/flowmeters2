unit MagB1ValueISelFrame;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, MagB1MyCommunicationClass, NodeClass,
  ExtCtrls, Buttons, MagB1FunctionsUnit, MagB1ValueHeadFrame, TntStdCtrls,
  TntExtCtrls, FunctionsUnit;

type
  TframeMagB1ValueISel = class(TframeMagB1Head)
    Panel1: TTntPanel;
    cbValues: TTntComboBox;
    btnWrite: TTntButton;
    procedure btnWriteClick(Sender: TObject);
  protected
  private
    { Private declarations }
    procedure FillList;
  public
    { Public declarations }
    function GetValueText(FourByte : T4Byte) : String;
    constructor CreateFrame(AOwner: TComponent; MyCommunication : TMyCommunication;
      EditNode : TNode; Language: TLanguage; LanguageIndex: Integer); override;
  end;

var
  frameMagB1ValueISel: TframeMagB1ValueISel;

implementation

uses
  MagB1GlobalUtilsClass, MagB1TranslationUnit, MagB1MenuFrm, TntDialogs, Constants;

{$R *.DFM}

{ TframeValueISel }


constructor TframeMagB1ValueISel.CreateFrame(AOwner: TComponent; MyCommunication : TMyCommunication;
    EditNode : TNode; Language: TLanguage; LanguageIndex: Integer);
var ReadOK:Boolean;
  FourByte:T4Byte;
  LastCursor : TCursor;
begin
  inherited;
  FillList;

  btnWrite.Caption:=GetTranslationText('WRITE', btnWrite.Caption);

  LastCursor := Screen.Cursor;
  try
    if Assigned(WaitFrm) then
    	WaitFrm.Show;
    Screen.Cursor:=crHourGlass;

    repeat
      Application.ProcessMessages;
      ReadOK:=ReadValue(FEditNode.ModbusAdr-1,FourByte);
    until(ReadOK)or(WideMessageDlg(GetTranslationText('READ_ERROR', STR_READ_ERROR),mtError, [mbRetry, mbAbort], 0) = mrAbort);
    if ReadOK then
      cbValues.ItemIndex:=Integer(FourByte);
    WaitFrm.gWait.AddProgress(10);
    Application.ProcessMessages;
  finally
    Screen.Cursor:=LastCursor;
    if Assigned(WaitFrm) then
    	WaitFrm.Hide;
  end;
end;

procedure TframeMagB1ValueISel.FillList;
var I : Integer;
begin
  cbValues.Clear;
  for I:=1 to FEditNode.MultiTexts.Count-1 do begin
    if (TNodeText(FEditNode.MultiTexts.Items[I]).MultiText.Count>0) then begin
      cbValues.Items.Add(TransformWideChars(TNodeText(FEditNode.MultiTexts.Items[I]).MultiText[MultiLanguageTextIndex], FEditNode.ConvertWideCharsPage));
    end else begin
      cbValues.Items.Add('unknown');
    end;
  end;
end;

function TframeMagB1ValueISel.GetValueText(FourByte: T4Byte): String;
begin
  if (FEditNode.MultiTexts.Count>Integer(FourByte)) and (TNodeText(FEditNode.MultiTexts.Items[Integer(FourByte)]).MultiText.Count>0) then begin
    Result:=TNodeText(FEditNode.MultiTexts.Items[Integer(FourByte)]).MultiText[0];
  end else begin
    Result:='unknown';
  end;
end;

procedure TframeMagB1ValueISel.btnWriteClick(Sender: TObject);
var FourByte : T4Byte;
    WriteOK:Boolean;
    LastCursor : TCursor;
begin

  LastCursor := Screen.Cursor;
  try
    if Assigned(WaitFrm) then
    begin
    	WaitFrm.Show;
        WaitFrm.gWait.Progress:=0;
    end;
    Screen.Cursor:=crHourGlass;

    Integer(FourByte):=cbValues.ItemIndex;
    repeat
      Application.ProcessMessages;
      WriteOK:=WriteValue(FEditNode.ModbusAdr-1,FourByte);
    until (WriteOK)or(WideMessageDlg(GetTranslationText('WRITE_ERROR', STR_WRITE_ERROR),mtError, [mbRetry, mbAbort], 0) = mrAbort);
    if WriteOK then begin
      if( Pos('MODBUS_SLAVE_ADDRESS', FEditNode.ValueName)>0)or //Modbus comunication param
        ( Pos('MODBUS_BAUDRATE', FEditNode.ValueName)>0)or
        ( Pos('MODBUS_PARITY', FEditNode.ValueName)>0) then
      begin
        WideMessageDlg('You have changed transmitter communication parameters.'#13#10 +
                   'Please setup new communication parameters.',
                   mtWarning,[mbOK],0);
        //PostMessage(formMagB1Menu.Handle, WM_NEED_RESET, 0, 0);
      end;
    end;
    WaitFrm.gWait.AddProgress(10);
    Application.ProcessMessages;
  finally
    Screen.Cursor:=LastCursor;
    if Assigned(WaitFrm) then
    	WaitFrm.Hide;
  end;
end;

end.
