unit MagB1ValueIHDTFrame;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  MagB1ValueIHexFrame, ExtCtrls, StdCtrls, TntStdCtrls;

// IHDT = Info Hex DateTime

type
  TframeMagB1ValueIHDT = class(TframeMagB1ValueIHex)
  private
    { Private declarations }
  public
    { Public declarations }
    function GetValueText(Value:Integer): String; override;
  end;

implementation

{$R *.DFM}

{ TframeMagB1ValueIHDT }

function TframeMagB1ValueIHDT.GetValueText(Value: Integer): String;
begin
  Result := Format('%.8x', [Value]);
  Insert(':', Result, 7);
  Insert(' ', Result, 5);
  Insert('/', Result, 3);
end;

end.
