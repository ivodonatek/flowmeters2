unit MagB1PassFrm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, MagB1MyCommunicationClass, WaitFrm, TntForms,
  TntStdCtrls, FunctionsUnit;

type
  TformMagB1Password= class(TTntForm)
    GroupBox1: TTntGroupBox;
    Panel1: TPanel;
    eValue: TEdit;
    btnCancel: TTntButton;
    btnConfirm: TTntButton;
    procedure btnCancelClick(Sender: TObject);
    procedure btnConfirmClick(Sender: TObject);
    constructor CreateFrm(AOwner: TComponent;MyCommunication:TMyCommunication; Address:Integer;
      Caption:WideString; Language: TLanguage);
  private
    { Private declarations }
    LocalLanguage: TLanguage;
    FMyCommunication : TMyCommunication;
    FPassModbusAddress : Integer;
  public
    { Public declarations }
  end;

var
  formMagB1Password: TformMagB1Password;

implementation

uses
  MagB1GlobalUtilsClass, MagB1TranslationUnit, TntDialogs;

//uses MenuFrm;

{$R *.DFM}

constructor TformMagB1Password.CreateFrm(AOwner: TComponent;MyCommunication:TMyCommunication;Address:Integer;
  Caption:WideString; Language: TLanguage);
begin
  inherited Create(AOwner);
  LocalLanguage:= Language;
  FMyCommunication:= MyCommunication;
  FPassModbusAddress := Address;
  GroupBox1.Caption := Caption;
  btnCancel.Caption := GetTranslationText('CANCEL', btnCancel.Caption);
  btnConfirm.Caption := GetTranslationText('OK', btnConfirm.Caption);
end;


procedure TformMagB1Password.btnCancelClick(Sender: TObject);
begin
  ModalResult:=mrCancel;
end;

procedure TformMagB1Password.btnConfirmClick(Sender: TObject);
var FourByte:T4Byte;
    ReadOK:Boolean;
    LastCursor: TCursor;
    lang: TLanguage;
begin
  inherited;

  lang:= LocalLanguage;
  if lang = langArabic then lang:= langEnglish;
  
  LastCursor := Screen.Cursor;
  try
    Screen.Cursor:=crHourGlass;

    case FMyCommunication.ProtokolType of
      ptMagX1: raise Exception.Create('Unsuported protocol');
    else
        Integer(FourByte):=StrToIntDef(eValue.Text,0);
        repeat
          ReadOK:=FMyCommunication.WriteMenuValue(FPassModbusAddress-1 ,FourByte);
        until (ReadOK)or(WideMessageDlg(GetTranslationText('READ_ERROR', STR_READ_ERROR),mtError, [mbRetry, mbAbort], 0) = mrAbort);
        if not ReadOK then
        begin
          raise Exception.Create(GetTranslationTextToLanguage(lang, 'COMMUNICATION_ERROR', STR_COMMUNICATION_ERROR));
        end;

        repeat
            ReadOK:=FMyCommunication.ReadMenuValue(FPassModbusAddress-1,FourByte);
        until (ReadOK)or(WideMessageDlg(GetTranslationText('READ_ERROR', STR_READ_ERROR),mtError, [mbRetry, mbAbort], 0) = mrAbort);
        if (ReadOK)and(Integer(FourByte)=1) then
          Modalresult:=mrOk
        else
        begin
          WideMessageDlg(GetTranslationText('INCORRECT_PASSWORD', STR_INCORRECT_PASSWORD), mtError, [mbOk], 0);
          eValue.SetFocus;
        end;
    end;
  finally
    Screen.Cursor:=LastCursor;
  end;
end;

end.
