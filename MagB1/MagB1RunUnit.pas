unit MagB1RunUnit;

interface

procedure MagB1Run;

implementation

uses sysutils, controls, Dialogs, Forms, MagB1TranslationUnit,
  MagB1GlobalUtilsClass, MagB1MyCommunicationClass,
  MagB1StatFrm, MagB1EnterFrm, MagB1MenuFrm, TntDialogs;

var
    Demo : Boolean;
    ReadOK:Boolean;
    firmwareNO:integer;
    MyCommunication : TMyCommunication;

function GetDeviceFirmwareNumber(AProtokolType:TProtokolType;ASlaveID,AComNumber:Cardinal;
   BaudRate:TBaudRate;StopBits:TStopBits;Parity:TParity;Timeout:Integer;
   RtsControl:TRtsControl;EnableDTROnOpen:Boolean;ASwitchWait:Cardinal;
   AIPAddress: String; APort: Integer):integer;
var firmwareNO:integer;
begin
    MyCommunication.SetCommunicationParam(AComNumber,ASlaveID,BaudRate,db8BITS,
        StopBits,Parity,RtsControl,EnableDTROnOpen,Timeout,AIPAddress,APort);
    if not MyCommunication.Connect() then
        raise Exception.Create('Comunication port error');
    //cti verzi FW
    repeat
      ReadOK:=MyCommunication.ReadFirmNO(Cardinal(firmwareNO));
    until (ReadOK)or(WideMessageDlg(GetTranslationText('READ_ERROR', STR_READ_ERROR), mtError, [mbRetry, mbAbort], 0) = mrAbort);
    if not ReadOK then
        raise Exception.Create('No device was detected');
    Result:=firmwareNO;
end;

procedure MagB1Run;
var
  statForm: TformMagB1Stat;
  menuForm: TformMagB1Menu;
begin
    MyCommunication:= nil;
    statForm:= nil;
    menuForm:= nil;

    try
      case formMagB1Enter.ShowModal of
        //statistic
        mrOK:
        begin
              case formMagB1Enter.ProtokolType of
                ptMagX1:MyCommunication:=TMyCommPort.Create();
                ptModbus:MyCommunication:=TMyModbus.Create();
                ptTCPModbus:MyCommunication:=TMyTCPModbus.Create();
                ptDemo:MyCommunication:=TDemoCommunication.Create();
              else
                raise Exception.Create('Unsuported protocol');
              end;
              //cti verzi firmware
              firmwareNO := GetDeviceFirmwareNumber(formMagB1Enter.ProtokolType,formMagB1Enter.SlaveID,
                      formMagB1Enter.ComPortNumber,formMagB1Enter.BaudRate,formMagB1Enter.StopBits,
                      formMagB1Enter.Parity,formMagB1Enter.Timeout,formMagB1Enter.RtsControl ,true,
                      formMagB1Enter.rgConvertorRS485.ItemIndex, formMagB1Enter.edtTCPIPAddress.Text,
                      formMagB1Enter.seTCPPort.Value);

              statForm := TformMagB1Stat.Create(Application);
              statForm.Caption := 'MagB1 statistic';
              statForm.Init(MyCommunication, firmwareNO);
              statForm.SetLayoutByLanguage(GlobalLanguage);
        end;
        //Service
        mrNo:
        begin
              case formMagB1Enter.ProtokolType of
                ptMagX1:MyCommunication:=TMyCommPort.Create();
                ptModbus:MyCommunication:=TMyModbus.Create();
                ptTCPModbus:MyCommunication:=TMyTCPModbus.Create();
                ptDemo:MyCommunication:=TDemoCommunication.Create();
              else
                raise Exception.Create('Unsuported protocol');
              end;
              //cti verzi firmware
              firmwareNO := GetDeviceFirmwareNumber(formMagB1Enter.ProtokolType,formMagB1Enter.SlaveID,
                      formMagB1Enter.ComPortNumber,formMagB1Enter.BaudRate,formMagB1Enter.StopBits,
                      formMagB1Enter.Parity,formMagB1Enter.Timeout,formMagB1Enter.RtsControl ,true,
                      formMagB1Enter.rgConvertorRS485.ItemIndex, formMagB1Enter.edtTCPIPAddress.Text,
                      formMagB1Enter.seTCPPort.Value);

              menuForm := TformMagB1Menu.Create(Application);
              menuForm.Caption := 'MagB1 service';
              menuForm.SetLayoutByLanguage(GlobalLanguage);
              menuForm.Init(MyCommunication, firmwareNO, formMagB1Enter.ProtokolType);
              menuForm.EnableEvents;
        end;
        //Close
        mrCancel:
        begin
        end;
      end;

    except
      on E: Exception do
      begin
        WideMessageDlg(E.Message,mtError, [mbOk], 0);
        if MyCommunication <> nil then begin
          MyCommunication.Disconnect;
          FreeAndNil(MyCommunication);
        end;
        if statForm <> nil then statForm.Close;
        if menuForm <> nil then menuForm.Close;        
      end;
    end;
end;

end.
