unit MagB1HeadFrm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, MagB1FunctionsUnit, FunctionsUnit, ComCtrls, StdCtrls, WaitFrm,
  MagB1MyCommunicationClass, TntStdCtrls, TntForms, TntComCtrls, TntExtCtrls;

type
  TformMagB1Head = class(TTntForm)
    Image21: TTntImage;
    Panel7: TTntPanel;
    lblSW: TTntLabel;
    lblFW: TTntLabel;
    Label5: TTntLabel;
    Label4: TTntLabel;
    Label8: TTntLabel;
    lblSerNo: TTntLabel;
    StatusBar1: TTntStatusBar;
    lblDemo: TTntLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    { Private declarations }
    procedure TranslateForm;
  protected
    LocalLanguage: TLanguage;
    MultiLanguageTextIndex: Integer;
  public
    { Public declarations }
    firmwareNO: Cardinal;
    ProtokolType:TProtokolType;
    Values: TStringList; // ??? TODO
    procedure Init(AMyCommunication : TMyCommunication; AfirmwareNO:integer; AProtokolType:TProtokolType);
    procedure SetLayoutByLanguage(lang: TLanguage);
  end;

var
  formMagB1Head: TformMagB1Head;

const ADDRESS_UNIT_NO = 1004;

implementation

uses MagX1Exception, MagB1GlobalUtilsClass, MagB1TranslationUnit, TntDialogs;

{$R *.DFM}

procedure TformMagB1Head.Init(AMyCommunication : TMyCommunication; AfirmwareNO:integer; AProtokolType:TProtokolType);
var ReadOK:boolean;
    FourByte : T4Byte;
    sExePath, filename: string;
    lang: TLanguage;
begin
  LocalLanguage:= GlobalLanguage;
  lang:= LocalLanguage;
  MultiLanguageTextIndex:= GetMultiLanguageTextIndex(LocalLanguage, AfirmwareNO);
  if lang = langArabic then lang:= langEnglish;

  sExePath := ExtractFilePath(Application.ExeName) + 'MagB1\';
  ProtokolType := AProtokolType;
  firmwareNO := AfirmwareNO;
    lblFW.Caption:=format('%2d.%0.2d',[firmwareNO div 100,firmwareNO mod 100]);
    lblSW.Caption:= GetVersion; //zjisti verzi SW

    case AProtokolType of
      ptMagX1:
      	case firmwareNO of
          105:
          begin
            Values.LoadFromFile(sExePath+'Values105.dat');
          end;
          106,107:
          begin
            Values.LoadFromFile(sExePath+'Values106.dat');
          end
          else
          begin
            AMyCommunication.Disconnect;
            Raise EUnsuportedFirmwareException.Create(GetTranslationTextToLanguage(lang, 'UNSUPPORTED_FW', STR_UNSUPPORTED_FW));  //ukonci cteni
          end
        end;
      ptModbus,
      ptTCPModbus:
      begin
        filename:='Modbus'+IntToStr(firmwareNO)+'.dat';
        if FileExists(sExePath+filename) then
          Values.LoadFromFile(sExePath+filename)
        else
          Raise EUnsuportedFirmwareException.Create(GetTranslationTextToLanguage(lang, 'UNSUPPORTED_FW', STR_UNSUPPORTED_FW));  //ukonci cteni
	  end;
      ptDemo:
      begin
        lblDemo.Visible:=true;
        filename:='Modbus'+IntToStr(firmwareNO)+'.dat';
        if FileExists(sExePath+filename) then
          Values.LoadFromFile(sExePath+filename)
        else
          Raise EUnsuportedFirmwareException.Create(GetTranslationTextToLanguage(lang, 'UNSUPPORTED_FW', STR_UNSUPPORTED_FW));  //ukonci cteni
	  end
      else
        Raise Exception.Create(GetTranslationTextToLanguage(lang, 'UNKNOWN_COMM_PROTOCOL', STR_UNKNOWN_COMM_PROTOCOL))
    end;

    //Seriove cislo
    repeat
      ReadOK:=AMyCommunication.ReadMenuValue(ADDRESS_UNIT_NO-1 , FourByte);
    until (ReadOK)or(WideMessageDlg(GetTranslationText('READ_ERROR', STR_READ_ERROR), mtError, [mbRetry, mbAbort], 0) = mrAbort);
    if ReadOK then
    begin
      lblSerNo.Caption:=FormatFloat('00000000', Cardinal(FourByte));
    end
    else
      raise Exception.Create('Error read serial number');
end;

procedure TformMagB1Head.TranslateForm;
begin
  lblDemo.Caption := GetTranslationText('DEMO', lblDemo.Caption);
end;

procedure TformMagB1Head.SetLayoutByLanguage(lang: TLanguage);
begin
  SetControlLayoutByLanguage(self, lang);
  TranslateForm;
end;

procedure TformMagB1Head.FormCreate(Sender: TObject);
begin
  Values:=TStringList.Create();
end;

procedure TformMagB1Head.FormDestroy(Sender: TObject);
begin
  Values.Free;
end;

end.
