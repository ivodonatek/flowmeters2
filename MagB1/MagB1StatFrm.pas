unit MagB1StatFrm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Gauges, StdCtrls, FunctionsUnit, MagB1MyCommunicationClass, ComCtrls, ExtCtrls,
  TntStdCtrls, TntForms, TntComCtrls, TntExtCtrls, TntDialogs;

const
  B1_BLOCK_COUNT = 182;
                                     
resourcestring
  STR_LOAD_SUCC = 'Loaded succesfully!';
  STR_BLOCK_READ_ERR = '%d block read error occurs!';
  STR_EXPORT_DONE = 'Export done!';

type
  TB1Statistic = array[0 .. B1_BLOCK_COUNT - 1] of TDataBlockB1;

  TformMagB1Stat = class(TTntForm)
    pnlBottom: TTntPanel;
    progStat: TGauge;
    btnLoadStat: TTntButton;
    btnExportStat: TTntButton;
    btnPrintStat: TTntButton;
    saveDlg: TTntSaveDialog;
    Image21: TTntImage;
    pnlTop: TTntPanel;
    imgMagLogo: TTntImage;
    pnlClient: TTntPanel;
    pnlStat: TTntPanel;
    lvStat: TTntListView;
    lblDemo: TTntLabel;
    procedure btnLoadStatClick(Sender: TObject);
    procedure btnExportStatClick(Sender: TObject);
    procedure btnPrintStatClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
    LocalLanguage: TLanguage;
    FMyCommunicationRef: TMyCommunication;
    FFirmwareNO: Integer;
    FB1Statistic: TB1Statistic;
    FDataB1List: TDataB1List;
    procedure FillViewList;
    procedure FillDataList;
    procedure ExportToFile(AFileName: WideString);
    procedure LoadFromIniFile;
    procedure SaveToIniFile;
    procedure TranslateForm;
  public
    { Public declarations }
    procedure Init(AMyCommunicationRef: TMyCommunication; AFirmwareNO: Integer);
    procedure SetLayoutByLanguage(lang: TLanguage);
    property FirmwareNO: Integer read FFirmwareNO;
  end;

//var
//  formMagB1Stat: TformMagB1Stat;

implementation

uses
  MagB1StatRpt, MagB1FunctionsUnit, MagB1DataClass, IniFiles, MagB1EnterFrm,
  MagB1TranslationUnit, TntClasses, TntSysUtils, WideIniFile;

{$R *.DFM}

{ TformMagB1Stat }

procedure TformMagB1Stat.Init(AMyCommunicationRef: TMyCommunication; AFirmwareNO: Integer);
begin
  LocalLanguage:= GlobalLanguage;
  FMyCommunicationRef := AMyCommunicationRef;
  FFirmwareNO := AFirmwareNO;
  if (FMyCommunicationRef.ProtokolType = ptDemo) then begin
    Caption := Caption + ' (DEMO)';
    lblDemo.Visible := True;
  end;
end;

procedure TformMagB1Stat.btnLoadStatClick(Sender: TObject);
var
  I, ErrCnt: Integer;
  lang: TLanguage;
begin
  lang:= LocalLanguage;
  if lang = langArabic then lang:= langEnglish;
  
  progStat.Progress := 0;
  progStat.MaxValue := B1_BLOCK_COUNT;
  progStat.Visible := True;
  Screen.Cursor := crHourGlass;
  try
    ErrCnt := 0;
    for I := 0 to B1_BLOCK_COUNT - 1 do begin
      if not FMyCommunicationRef.ReadMagBDataloggerBlock(I, FB1Statistic[I], (FirmwareNO >= 1027)) then begin
        Inc(ErrCnt);
      end;
      progStat.Progress := I + 1;
      Application.ProcessMessages;
    end;
  finally
    Screen.Cursor := crDefault;
    progStat.Visible := False;
  end;
  FillDataList;
  FillViewList;
  if ErrCnt = 0 then begin
    WideShowMessage(GetTranslationText('LOAD_SUCC', STR_LOAD_SUCC));
  end else begin
    WideShowMessage(Format(GetTranslationTextToLanguage(lang, 'BLOCK_READ_ERR', STR_BLOCK_READ_ERR), [ErrCnt]));
  end;
end;

procedure TformMagB1Stat.FillViewList;
var I: Integer;
    NewItem: TListItem;
begin
  progStat.Progress := 0;
  progStat.MaxValue := FDataB1List.Count;
  progStat.Visible := True;
  Screen.Cursor := crHourGlass;
  lvStat.Items.BeginUpdate;
  try
    lvStat.Items.Clear;
    for I := 0 to FDataB1List.Count - 1 do begin
      NewItem := lvStat.Items.Add;
      NewItem.Caption := FormatStatDate(FDataB1List[I].DateTime);
      NewItem.SubItems.Add(FormatStatTime(FDataB1List[I].DateTime));
      NewItem.SubItems.Add(Format('%.2f', [FDataB1List[I].TotalPlus]));
      NewItem.SubItems.Add(Format('%.2f', [FDataB1List[I].TotalMinus]));
      NewItem.SubItems.Add(Format('%.2f', [FDataB1List[I].TotalPlus - FDataB1List[I].TotalMinus]));
      if (FDataB1List[I].TotalVolumeDef) then begin
        NewItem.SubItems.Add(Format('%.2f', [FDataB1List[I].TotalPlusVolume]));
        NewItem.SubItems.Add(Format('%.2f', [FDataB1List[I].TotalMinusVolume]));
        NewItem.SubItems.Add(Format('%.2f', [FDataB1List[I].TotalPlusVolume - FDataB1List[I].TotalMinusVolume]));
      end else begin
        NewItem.SubItems.Add('-');
        NewItem.SubItems.Add('-');
        NewItem.SubItems.Add('-');
      end;
      NewItem.SubItems.Add(CardinalToBinaryStr16(FDataB1List[I].ErrorCode));
      progStat.Progress := I + 1;
      Application.ProcessMessages;
    end;
  finally
    lvStat.Items.EndUpdate;
    Screen.Cursor := crDefault;
    progStat.Visible := False;
  end;
end;

procedure TformMagB1Stat.ExportToFile(AFileName: WideString);
var I: Integer;
    SL: TTntStringList;
    str: WideString;
begin
  progStat.Progress := 0;
  progStat.MaxValue := FDataB1List.Count;
  progStat.Visible := True;
  Screen.Cursor := crHourGlass;
  SL := TTntStringList.Create;
  try
    SL.Clear;

    str:=
        GetTranslationText('DATE', 'Date') + ';' +
        GetTranslationText('TIME', 'Time') + ';' +
        GetTranslationText('STAT_TOTAL_PLUS', 'Total +') + ';' +
        GetTranslationText('STAT_TOTAL_MINUS', 'Total -') + ';' +
        GetTranslationText('STAT_TOTAL', 'Total') + ';' +
        GetTranslationText('STAT_TOTAL_PLUS_VOL', 'Total + Volume') + ';' +
        GetTranslationText('STAT_TOTAL_MINUS_VOL', 'Total - Volume') + ';' +
        GetTranslationText('STAT_TOTAL_VOL', 'Total Volume') + ';' +
        GetTranslationText('STAT_ERROR_CODE', 'ErrorCode');
    SL.Add(str);

    for I := 0 to FDataB1List.Count - 1 do begin
      if FDataB1List[I].TotalVolumeDef then begin
        SL.Add(Format('%s;%s;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%s',
          [FormatStatDate(FDataB1List[I].DateTime),
           FormatStatTime(FDataB1List[I].DateTime),
           FDataB1List[I].TotalPlus,
           FDataB1List[I].TotalMinus,
           FDataB1List[I].TotalPlus - FDataB1List[I].TotalMinus,
           FDataB1List[I].TotalPlusVolume,
           FDataB1List[I].TotalMinusVolume,
           FDataB1List[I].TotalPlusVolume - FDataB1List[I].TotalMinusVolume,
           CardinalToBinaryStr16(FDataB1List[I].ErrorCode)]));
      end else begin
        SL.Add(Format('%s;%s;%.2f;%.2f;%.2f;-;-;-;%s',
          [DateToStr(FDataB1List[I].DateTime),
           TimeToStr(FDataB1List[I].DateTime),
           FDataB1List[I].TotalPlus,
           FDataB1List[I].TotalMinus,
           FDataB1List[I].TotalPlus - FDataB1List[I].TotalMinus,
           CardinalToBinaryStr16(FDataB1List[I].ErrorCode)]));
      end;
      progStat.Progress := I + 1;
      Application.ProcessMessages;
    end;
    SL.SaveToFile(AFileName);
  finally
    SL.Free;
    Screen.Cursor := crDefault;
    progStat.Visible := False;
  end;
end;

procedure TformMagB1Stat.btnExportStatClick(Sender: TObject);
begin
  if saveDlg.Execute then begin
    ExportToFile(saveDlg.FileName);
    WideShowMessage(GetTranslationText('EXPORT_DONE', STR_EXPORT_DONE));
  end;
end;

procedure TformMagB1Stat.btnPrintStatClick(Sender: TObject);
var
  qrMagBStat: TqrMagB1Stat;
begin
  qrMagBStat := TqrMagB1Stat.CreateRep(nil, FDataB1List, FMyCommunicationRef.ProtokolType = ptDemo);
  try
    qrMagBStat.PreviewModal;
  finally
    qrMagBStat.Free;
  end;
end;

procedure TformMagB1Stat.FormCreate(Sender: TObject);
begin
  FDataB1List := TDataB1List.Create;
  LoadFromIniFile;
  imgMagLogo.Picture.LoadFromFile(IncludeTrailingBackslash(ExtractFilePath(Application.ExeName)) + MAG_LOGO_FILE_NAME);
end;

procedure TformMagB1Stat.FormDestroy(Sender: TObject);
begin
  SaveToIniFile;
  FDataB1List.Free;
  FreeAndNil(FMyCommunicationRef);
end;

procedure TformMagB1Stat.FillDataList;
var I, J, LastI, LastJ: Integer;
    NewItem: TDataB1;
begin
  progStat.Progress := 0;
  progStat.MaxValue := B1_BLOCK_COUNT;
  progStat.Visible := True;
  Screen.Cursor := crHourGlass;
  try
    FDataB1List.Clear;
    LastI := -1;
    LastJ := -1;
    for I := 0 to B1_BLOCK_COUNT - 1 do begin
      for J := 0 to B1_BLOCK_ITEMS_COUNT - 1 do begin
        if FB1Statistic[I][J].Date <> High(Cardinal) then begin
          NewItem := TDataB1.Create;
          NewItem.DateTime := GetDateTimeFromHex(FB1Statistic[I][J].Date, FB1Statistic[I][J].Time);
          NewItem.TotalPlus := FB1Statistic[I][J].TotalPlus;
          NewItem.TotalMinus := FB1Statistic[I][J].TotalMinus;
          if (LastI > -1) and (LastJ > -1) then begin
            NewItem.TotalVolumeDef := True;
            NewItem.TotalPlusVolume := (FB1Statistic[I][J].TotalPlus - FB1Statistic[LastI][LastJ].TotalPlus);
            NewItem.TotalMinusVolume := (FB1Statistic[I][J].TotalMinus - FB1Statistic[LastI][LastJ].TotalMinus);
          end else begin
            NewItem.TotalVolumeDef := False;
            NewItem.TotalPlusVolume := 0;
            NewItem.TotalMinusVolume := 0;
          end;
          NewItem.ErrorCode := FB1Statistic[I][J].ErrorCode;
          FDataB1List.Add(NewItem);
          LastI := I;
          LastJ := J;
        end else begin
          LastI := -1;
          LastJ := -1;
        end;
      end;
      progStat.Progress := I + 1;
      Application.ProcessMessages;
    end;
  finally
    Screen.Cursor := crDefault;
    progStat.Visible := False;
  end;
end;

procedure TformMagB1Stat.LoadFromIniFile;
var sExePath: String;
    iniF: TWideIniFile;
begin
  //nacteni velikosti Formu
  sExePath := GetUserAppDataProductPath();
  iniF := TWideIniFile.Create(sExePath+'MagB1.ini');
  try
    Self.Width:=iniF.ReadInteger('Statistic','Width',800);
    Self.Height:=iniF.ReadInteger('Statistic','Height',600);
    if(iniF.ReadBool('Statistic','Maximized', false) = true) then begin
      Self.WindowState:=wsMaximized
    end else begin
      Self.WindowState:=wsNormal;
    end;
  finally
    iniF.Free;
  end;
end;

procedure TformMagB1Stat.SaveToIniFile;
var sExePath: String;
    iniF: TWideIniFile;
begin
  sExePath := GetUserAppDataProductPath();
  iniF := TWideIniFile.Create(sExePath+'MagB1.ini');
  try
    iniF.WriteBool('Statistic','Maximized', Self.WindowState=wsMaximized);
    if Self.WindowState = wsNormal then begin
      iniF.WriteInteger('Statistic','Width',Self.Width);
      iniF.WriteInteger('Statistic','Height',Self.Height);
    end;
    iniF.Save;
  finally
    iniF.Free;
  end;
end;

procedure TformMagB1Stat.TranslateForm;
begin
  lblDemo.Caption := GetTranslationText('DEMO', lblDemo.Caption);
  lvStat.Columns[0].Caption := GetTranslationText('DATE', lvStat.Columns[0].Caption);
  lvStat.Columns[1].Caption := GetTranslationText('TIME', lvStat.Columns[1].Caption);
  lvStat.Columns[2].Caption := GetTranslationText('STAT_TOTAL_PLUS', lvStat.Columns[2].Caption);
  lvStat.Columns[3].Caption := GetTranslationText('STAT_TOTAL_MINUS', lvStat.Columns[3].Caption);
  lvStat.Columns[4].Caption := GetTranslationText('STAT_TOTAL', lvStat.Columns[4].Caption);
  lvStat.Columns[5].Caption := GetTranslationText('STAT_TOTAL_PLUS_VOL', lvStat.Columns[5].Caption);
  lvStat.Columns[6].Caption := GetTranslationText('STAT_TOTAL_MINUS_VOL', lvStat.Columns[6].Caption);
  lvStat.Columns[7].Caption := GetTranslationText('STAT_TOTAL_VOL', lvStat.Columns[7].Caption);
  lvStat.Columns[8].Caption := GetTranslationText('STAT_ERROR_CODE', lvStat.Columns[8].Caption);
  btnLoadStat.Caption := GetTranslationText('LOAD', btnLoadStat.Caption);
  btnExportStat.Caption := GetTranslationText('EXPORT', btnExportStat.Caption);
  btnPrintStat.Caption := GetTranslationText('PRINT', btnPrintStat.Caption);
end;

procedure TformMagB1Stat.SetLayoutByLanguage(lang: TLanguage);
begin
  SetControlLayoutByLanguage(self, lang);
  TranslateForm;
end;

procedure TformMagB1Stat.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := caFree;
end;

end.
