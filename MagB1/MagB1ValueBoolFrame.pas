unit MagB1ValueBoolFrame;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
   StdCtrls, MagB1MyCommunicationClass, ExtCtrls, NodeClass, Buttons,
   MagB1FunctionsUnit, MagB1ValueHeadFrame, TntStdCtrls, FunctionsUnit;

type
  TframeMagB1ValueBool = class(TframeMagB1Head)
    Panel1: TPanel;
    lblValue: TTntLabel;
    lblStatus: TTntLabel;
    btnWrite: TTntButton;
    procedure btnWriteClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    constructor CreateFrame(AOwner: TComponent; MyCommunication : TMyCommunication;
      EditNode : TNode; Language: TLanguage; LanguageIndex: Integer); override;
  end;

var
  frameMagB1ValueBool: TframeMagB1ValueBool;

implementation

uses
  MagB1TranslationUnit, MagB1MenuFrm, ShellApi, MagB1GlobalUtilsClass, Main, TntDialogs;

{$R *.DFM}

{ TframeValueBool }

constructor TframeMagB1ValueBool.CreateFrame(AOwner: TComponent; MyCommunication : TMyCommunication;
    EditNode : TNode; Language: TLanguage; LanguageIndex: Integer);
begin
  inherited;

  lblStatus.Caption:=GetTranslationText('STATUS', lblStatus.Caption);
  lblValue.Caption:=GetTranslationText('UNKNOWN', lblValue.Caption);
  btnWrite.Caption:=GetTranslationText('YES', btnWrite.Caption);
end;

procedure TframeMagB1ValueBool.btnWriteClick(Sender: TObject);
var WriteOk, ReadOK, MemoryPressent:Boolean;
    FourByte:T4Byte;
    LastCursor : TCursor;

    APort,AID:Cardinal;
    ABaudRate:TBaudRate;
    ADataBits:TDataBits;
    AStopBits:TStopBits;
    AParity:TParity;
    ATimeout:Cardinal;
    ARtsControl:TRtsControl;
    AEnableDTROnOpen:Boolean;
    AIPAddress: String;
    AIPPort: Integer;
begin

  if WideMessageDlg(lblTitle.Caption, mtConfirmation, [mbYes, mbNo], 0) = mrYes then begin

  if ((FEditNode.ModbusAdr - 1) = MODBUS_REGISTRY_ADDRESS_RESET) then begin
    // RESET
    WideMessageDlg(GetTranslationText('APP_RESTART', STR_APP_RESTART), mtInformation, [mbOK], 0);
    FMyCommunication.WriteReset;
    FMyCommunication.Disconnect;
    Konec := True;
    TformMagB1Menu(Self.Owner).Close;
    MainForm.SetMagB1RunRequest;
  end else begin
    LastCursor := Screen.Cursor;
    try
      if Assigned(WaitFrm) then
      begin
          WaitFrm.Show;
          WaitFrm.gWait.Progress:=0;
      end;
      Screen.Cursor:=crHourGlass;

      Integer(FourByte):=1;

      case FEditNode.FOut of           //F OUT
        1:  //dataloger delete
        begin
          repeat
            Application.ProcessMessages;
             ReadOK:=FMyCommunication.MemoryPresent(MemoryPressent)
          until (ReadOK)or(WideMessageDlg(GetTranslationText('READ_ERROR', STR_READ_ERROR),mtError, [mbRetry, mbAbort], 0) = mrAbort);
          if not ReadOK then
          begin
            lblValue.Caption:=GetTranslationText('ERROR', 'Error');
          end
          else if not MemoryPressent then
          begin
            lblValue.Caption:='Module MEMORY is not present';
            ShowMessage('Module MEMORY is not present');
  //          Screen.Cursor:=crDefault;
  //          WaitFrm.Free;
            exit;
          end
          else
          begin
            FMyCommunication.GetCommunicationParam(APort,AID,ABaudRate,ADataBits,
              AStopBits,AParity,ARtsControl,AEnableDTROnOpen,ATimeout, AIPAddress, AIPPort);
            FMyCommunication.SetCommunicationParam(APort,AID,ABaudRate,ADataBits,
              AStopBits,AParity,ARtsControl,AEnableDTROnOpen,ATimeout+5000, AIPAddress, AIPPort);
          end;
        end;
      end;

          WaitFrm.gWait.AddProgress(10);
      repeat
        Application.ProcessMessages;
        WriteOK:=WriteValue(FEditNode.ModbusAdr-1,FourByte);
      until (WriteOK)or(WideMessageDlg(GetTranslationText('WRITE_ERROR', STR_WRITE_ERROR),mtError, [mbRetry, mbAbort], 0) = mrAbort);
      if WriteOK then
        lblValue.Caption:='Executed'
      else
        lblValue.Caption:='Not executed';

      case FEditNode.FOut of           //F OUT
        1:  //dataloger delete
        begin
          FMyCommunication.SetCommunicationParam(APort,AID,ABaudRate,ADataBits,
            AStopBits,AParity,ARtsControl,AEnableDTROnOpen,ATimeout, AIPAddress, AIPPort);
        end;
      end;

      WaitFrm.gWait.AddProgress(10);
      Application.ProcessMessages;
    finally
      Screen.Cursor:=LastCursor;
      if Assigned(WaitFrm) then
          WaitFrm.Hide;
    end;
  end;

  end;
  
end;

end.
