unit MagC1ValuePaswFrame;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, MagC1MyCommunicationClass, ExtCtrls, NodeClass,
  Buttons, WaitFrm, MagC1FunctionsUnit,
  MagC1ValueHeadFrame, comctrls;

type
  TframeMagC1ValuePasw = class(TframeMagC1Head)
    Panel1: TPanel;
    eValue: TEdit;
    btnOK: TButton;
    btnCancel: TButton;
    procedure btnConfirmClick(Sender: TObject);
    procedure btnCancelClick(Sender: TObject);
    procedure FrameEnter(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
//    function IsNumber(s: string): Boolean;
    constructor CreateFrame(AOwner: TComponent; MyCommunication : TMyCommunication;
    EditNode : TNode); override;
  end;

var
  frameMagC1ValuePasw: TframeMagC1ValuePasw;

implementation

uses MagC1MenuFrm, MagC1GlobalUtilsClass, MagC1TranslationUnit;

{$R *.DFM}

{ TframeValuePasw }
constructor TframeMagC1ValuePasw.CreateFrame(AOwner: TComponent; MyCommunication : TMyCommunication;
    EditNode : TNode);
begin
  inherited;
  lblTitle.Caption:=FEditNode.Popis;
end;

procedure TframeMagC1ValuePasw.btnConfirmClick(Sender: TObject);
var FourByte : T4Byte;
    ReadOK:Boolean;
    LastCursor:TCursor;
begin
  inherited;
  LastCursor:=Screen.Cursor;
  try
    if Assigned(WaitFrm) then
    	WaitFrm.Show;
    Screen.Cursor:=crHourGlass;
    Application.ProcessMessages();

    case FMyCommunication.ProtokolType of
      ptMagX1:
	  	raise Exception.Create('Unsuported communications protocol');
      else
      begin
        Integer(FourByte):=StrToIntDef(eValue.Text,0);
        // Write Password
        repeat
          ReadOK:=FMyCommunication.WriteMenuValue(FEditNode.ModbusAdr-1,FourByte);
        until (ReadOK)or(MessageDlg(GetTranslationText('READ_ERROR', STR_READ_ERROR),mtError, [mbRetry, mbAbort], 0) = mrAbort);
        if not ReadOK then raise Exception.Create('Error read password');
        // Read if Password is OK
        repeat
          ReadOK:=FMyCommunication.ReadMenuValue(FEditNode.ModbusAdr-1,FourByte);
        until (ReadOK)or(MessageDlg(GetTranslationText('READ_ERROR', STR_READ_ERROR),mtError, [mbRetry, mbAbort], 0) = mrAbort);
        if (ReadOK) and (Integer(FourByte)=1) then begin
          // Expand
          TformMagC1Menu(Self.Owner).tvMenu.Selected.Expand(false);
        end else begin
          // Show warning
          MessageDlg(GetTranslationText('INCORRECT_PASSWORD', STR_INCORRECT_PASSWORD), mtWarning, [mbOK], 0);
          eValue.SetFocus;
        end;
      end;
    end;
  finally
    Screen.Cursor:=LastCursor;
    if Assigned(WaitFrm) then
    	WaitFrm.Hide();
  end;
end;

procedure TframeMagC1ValuePasw.btnCancelClick(Sender: TObject);
begin
  eValue.Text:='';
  eValue.SetFocus;
end;

procedure TframeMagC1ValuePasw.FrameEnter(Sender: TObject);
begin
  inherited;
  eValue.SetFocus();
end;

end.
