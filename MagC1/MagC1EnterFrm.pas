unit MagC1EnterFrm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, MagC1FunctionsUnit, MagC1MyCommunicationClass,
  Spin, Mask, ComCtrls, PBMaskEdit, PBSpinEdit, IniFiles,
  JvExControls, JvComCtrls, JvExStdCtrls, JvTextListBox, SharedComms;

const //MODBUS
      DEFAULT_BAUDRATE = 1; //9600
      DEFAULT_PARITY = 0; //1stopbit, Even parity
      DEFAULT_COMPORT = 1; //1
      DEFAULT_SLAVEID = 1; //1
      DEFAULT_TIMEOUT = 1; //1
      DEFAULT_RTS_FLOW_CONTROL = false; //disable
      //GPRS
      DEFAULT_GPRS_BAUDRATE = 2; //19200
      DEFAULT_GPRS_PARITY = 3; //0stopbit, none parity
      DEFAULT_GPRS_SLAVEID = 1; //1
      DEFAULT_GPRS_TIMEOUT = 3; //3
      DEFAULT_GPRS_IPADDRESS = '0.0.0.0';
      DEFAULT_GPRS_PORT = 987;

      LOGO_FILE_NAME = 'Logo.jpg';
      MAG_LOGO_FILE_NAME = 'MagC1\MagLogo.jpg';


type
  TformMagC1Enter = class(TForm)
    btnCZ: TSpeedButton;
    btnEN: TSpeedButton;
    btnGE: TSpeedButton;
    btnSP: TSpeedButton;
    btnFR: TSpeedButton;
    btnRU: TSpeedButton;
    pcProtokolType: TPageControl;
    TabSheetMagC1: TTabSheet;
    TabSheetModbus: TTabSheet;
    MaskEditSerNo: TMaskEdit;
    lblLanguage: TLabel;
    lblBaudRate: TLabel;
    Label2: TLabel;
    lblModbusSlaveID: TLabel;
    lblComPortNum: TLabel;
    Label7: TLabel;
    lblParity: TLabel;
    btnStatistic: TButton;
    btnExit: TButton;
    btnService: TButton;
    cbModbusCommPort: TComboBox;
    PBseMagX1CommPort: TPBSpinEdit;
    PBseModbusSlaveID: TPBSpinEdit;
    rgConvertorRS485: TRadioGroup;
    cbBaudRate: TComboBox;
    cbParity: TComboBox;
    PBseModbusTimeout: TPBSpinEdit;
    lblTimeout: TLabel;
    lblDevList: TLabel;
    PBseMagX1Timeout: TPBSpinEdit;
    Label5: TLabel;
    RTSCheckBox: TCheckBox;
    DemoCheckBox: TCheckBox;
    TabSheetTCPModbus: TTabSheet;
    Label9: TLabel;
    Label10: TLabel;
    seTCPPort: TPBSpinEdit;
    btnAdd: TSpeedButton;
    btnSave: TSpeedButton;
    btnDel: TSpeedButton;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    cbTCPBaudRate: TComboBox;
    cbTCPParity: TComboBox;
    PBseTCPModbusSlaveID: TPBSpinEdit;
    PBseTCPModbusTimeout: TPBSpinEdit;
    edtTCPIPAddress: TJvIPAddress;
    lbDevices: TJvTextListBox;
    imgMagLogo: TImage;
    imgLogo: TImage;
    procedure btnLangClick(Sender: TObject);
    procedure btnCZMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);    
    procedure btnServiceStatisticClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure lbDevicesClick(Sender: TObject);
    procedure btnSaveClick(Sender: TObject);
    procedure DeviceDetailChange(Sender: TObject);
    procedure btnAddClick(Sender: TObject);
    procedure btnDelClick(Sender: TObject);
    procedure btnExitClick(Sender: TObject);
  private
    { Private declarations }
    procedure LoadDevices;
    procedure SetDefaultValues;
    procedure RemoveDevice(ADeviceName: String);
    procedure LoadDeviceDetail(ADeviceName: String);
    procedure SaveDeviceDetail(ADeviceName: String);
    function DeviceExist(ADeviceName: String): Boolean;
    function ValidDeviceName(ADeviceName: String): Boolean;
    procedure SaveLatestItemName;
  public
    { Public declarations }
    SlaveID:Cardinal;
    ProtokolType:TProtokolType;
    ComPortNumber:Cardinal;
    BaudRate:TBaudRate;
    Parity:TParity;
    StopBits:TStopBits;
    Timeout:Cardinal;
    RtsControl:TRtsControl;
    procedure TranslateForm;
  end;

var
  formMagC1Enter: TformMagC1Enter;
  STR_USER_DEF: String = '<user defined>';

implementation

uses MagC1MenuFrm, MagC1TranslationUnit;	//, MainFrm;

{$R *.DFM}

procedure TformMagC1Enter.btnLangClick(Sender: TObject);
begin
  GlobalLanguageSection := TSpeedButton(Sender).Caption;
  GlobalLanguageVersionName := TSpeedButton(Sender).Hint;
  TranslateForm;
  GlobalLanguageIndex := StrToIntDef(GetTranslationText('LANG_INDEX', ''), TSpeedButton(Sender).Tag);
end;

procedure TformMagC1Enter.btnCZMouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
begin
  btnCZ.Font.Style:=[];
  btnEN.Font.Style:=[];
  btnGE.Font.Style:=[];
  btnSP.Font.Style:=[];
  btnFR.Font.Style:=[];
  btnRU.Font.Style:=[];
  TSpeedButton(Sender).Font.Style:=[fsBold];
end;

procedure TformMagC1Enter.FormCreate(Sender: TObject);
var iniF: TIniFile;
    sExePath: string;
    LatestItemName:string;
begin
  FillComboBoxWithCommNumbers(cbModbusCommPort);
  TranslateForm;
  if btnCZ.Caption = GlobalLanguageSection then btnCZ.Down := True;
  if btnEN.Caption = GlobalLanguageSection then btnEN.Down := True;
  if btnGE.Caption = GlobalLanguageSection then btnGE.Down := True;
  if btnSP.Caption = GlobalLanguageSection then btnSP.Down := True;
  if btnFR.Caption = GlobalLanguageSection then btnFR.Down := True;
  if btnRU.Caption = GlobalLanguageSection then btnRU.Down := True;
  FormStyle := fsStayOnTop;
  LoadDevices;
//  SetDefaultValues;

  //load latest selected item in lbDevices
  sExePath := GetUserAppDataProductPath();
  iniF := TIniFile.Create(sExePath+'MagC1.ini');
  try
  	LatestItemName := iniF.ReadString('Enter','LatestItemName',STR_USER_DEF);
  finally
    iniF.Free;
  end;

  if DeviceExist(LatestItemName) then
  	lbDevices.ItemIndex := lbDevices.Items.IndexOf(LatestItemName)
  else
    lbDevices.ItemIndex := 0;
  lbDevicesClick(lbDevices);

  imgLogo.Picture.LoadFromFile(IncludeTrailingBackslash(ExtractFilePath(Application.ExeName)) + LOGO_FILE_NAME);
  imgMagLogo.Picture.LoadFromFile(IncludeTrailingBackslash(ExtractFilePath(Application.ExeName)) + MAG_LOGO_FILE_NAME);
end;

procedure TformMagC1Enter.FormShow(Sender: TObject);
begin
  FillComboBoxWithCommNumbers(cbModbusCommPort);
  lbDevicesClick(lbDevices);
end;

procedure TformMagC1Enter.btnServiceStatisticClick(Sender: TObject);
begin
//  SaveLatestItemName;
  if pcProtokolType.ActivePage=TabSheetMagC1 then
  begin
//MagX1
    SlaveID:=StrToIntDef(MaskEditSerNo.Text,1);
    if DemoCheckBox.Checked then
	    ProtokolType:=ptDemo
    else
    	ProtokolType:=ptMagX1;
    ComPortNumber:=PBseMagX1CommPort.Value;
    BaudRate:=br9600;
    StopBits:=sb1BITS;
    Parity:=ptNONE;
    Timeout:=PBseMagX1Timeout.Value*1000;
  end
  else if pcProtokolType.ActivePage=TabSheetModbus then
  begin
//MODBUS
    SlaveID:=PBseModbusSlaveID.Value;
    if DemoCheckBox.Checked then
	    ProtokolType:=ptDemo
    else
    	ProtokolType:=ptModbus;
    ComPortNumber:=GetCommNumberFromComboBox(cbModbusCommPort);
    Timeout:=PBseModbusTimeout.Value*1000;
    case cbBaudRate.ItemIndex of
      0 : BaudRate := br4800;
      1 : BaudRate := br9600;
      2 : BaudRate := br19200;
      3 : BaudRate := br38400;
      else BaudRate:=br9600;
    end;
    case cbParity.ItemIndex of
    0:begin
        Parity:=ptEVEN;
        StopBits:=sb1BITS;
      end;
    1:begin
        Parity:=ptODD;
        StopBits:=sb1BITS;
      end;
    2:begin
        Parity:=ptNONE;
        StopBits:=sb2BITS;
      end;
    3:begin
        Parity:=ptNONE;
        StopBits:=sb1BITS;
      end;
    end;

    if RTSCheckBox.Checked then
      RtsControl:=rcRTSToggle
    else
      RtsControl:=rcRTSEnable;
  end else if pcProtokolType.ActivePage=TabSheetTCPModbus then begin
//TCP using MODBUS
    SlaveID:=PBseTCPModbusSlaveID.Value;
    if DemoCheckBox.Checked then
	    ProtokolType:=ptDemo
    else
    	ProtokolType:=ptTCPModbus;
    Timeout:=PBseTCPModbusTimeout.Value*1000;
    case cbTCPBaudRate.ItemIndex of
    0:BaudRate:=br4800;
    1:BaudRate:=br9600;
    2:BaudRate:=br19200;
    3:BaudRate:=br38400;
    else
      BaudRate:=br9600;
    end;
    case cbTCPParity.ItemIndex of
    0:begin
        Parity:=ptEVEN;
        StopBits:=sb1BITS;
      end;
    1:begin
        Parity:=ptODD;
        StopBits:=sb1BITS;
      end;
    2:begin
        Parity:=ptNONE;
        StopBits:=sb2BITS;
      end;
    3:begin
        Parity:=ptNONE;
        StopBits:=sb1BITS;
      end;
    end;
  end;
end;

procedure TformMagC1Enter.FormDestroy(Sender: TObject);
begin
	SaveLatestItemName;
end;

procedure TformMagC1Enter.lbDevicesClick(Sender: TObject);
begin
  if (lbDevices.ItemIndex > 0) then
  	LoadDeviceDetail(lbDevices.Items.Strings[lbDevices.ItemIndex])
  else
    SetDefaultValues;
  btnSave.Enabled := False;
  btnDel.Enabled := (lbDevices.ItemIndex > 0);
end;

procedure TformMagC1Enter.LoadDeviceDetail(ADeviceName: String);
var IniF: TIniFile;
	sExePath:string;
begin
  sExePath := GetUserAppDataProductPath();
  iniF := TIniFile.Create(sExePath+'MagC1Devices.ini');
//  IniF := TIniFile.Create(IncludeTrailingBackslash(ExtractFilePath(Application.ExeName))+'Devices.ini');
  try
//    edtDeviceName.Text := ADeviceName;
    // Protokol
    pcProtokolType.ActivePageIndex := IniF.ReadInteger(ADeviceName, 'DeviceType', 0);
    // Modbus
    PBseModbusSlaveID.Value := IniF.ReadInteger(ADeviceName, 'ModbusSlaveID', 1);
    SetCommNumberInComboBox(cbModbusCommPort, IniF.ReadInteger(ADeviceName, 'ModbusComPort', 0));
    cbBaudRate.ItemIndex := IniF.ReadInteger(ADeviceName, 'ModbusBaudRate', 1);
    cbParity.ItemIndex := IniF.ReadInteger(ADeviceName, 'ModbusParity', 0);
    PBseModbusTimeout.Value := IniF.ReadInteger(ADeviceName, 'ModbusTimeout', 1);
    RTSCheckBox.Checked := IniF.ReadBool(ADeviceName, 'ModbusRTS', False);
    // RS
    MaskEditSerNo.Text := IniF.ReadString(ADeviceName, 'RSSerNo', '1');
    PBseMagX1CommPort.Value := IniF.ReadInteger(ADeviceName, 'RSComPort', 1);
    PBseMagX1Timeout.Value := IniF.ReadInteger(ADeviceName, 'RSTimeout', 1);
    rgConvertorRS485.ItemIndex := IniF.ReadInteger(ADeviceName, 'RSConvertor', 0);
    // GPRS
    edtTCPIPAddress.Text := IniF.ReadString(ADeviceName, 'GPRSIPAddress', '0.0.0.0');
    seTCPPort.Value := IniF.ReadInteger(ADeviceName, 'GPRSPort', 987);

    PBseTCPModbusSlaveID.Value := IniF.ReadInteger(ADeviceName, 'GPRSModbusSlaveID', 1);
    cbTCPBaudRate.ItemIndex := IniF.ReadInteger(ADeviceName, 'GPRSModbusBaudRate', 2);
    cbTCPParity.ItemIndex := IniF.ReadInteger(ADeviceName, 'GPRSModbusParity', 3);
    PBseTCPModbusTimeout.Value := IniF.ReadInteger(ADeviceName, 'GPRSModbusTimeout', 3);
  finally
    IniF.Free;
  end;
end;

procedure TformMagC1Enter.SaveDeviceDetail(ADeviceName: String);
var IniF: TIniFile;
	sExePath:string;
begin
  sExePath := GetUserAppDataProductPath();
  iniF := TIniFile.Create(sExePath+'MagC1Devices.ini');
//  IniF := TIniFile.Create(IncludeTrailingBackslash(ExtractFilePath(Application.ExeName))+'Devices.ini');
  try
    // Protokol
    IniF.WriteInteger(ADeviceName, 'DeviceType', pcProtokolType.ActivePageIndex);
    // Modbus
    IniF.WriteInteger(ADeviceName, 'ModbusSlaveID', PBseModbusSlaveID.Value);
    IniF.WriteInteger(ADeviceName, 'ModbusComPort', GetCommNumberFromComboBox(cbModbusCommPort));
    IniF.WriteInteger(ADeviceName, 'ModbusBaudRate', cbBaudRate.ItemIndex);
    IniF.WriteInteger(ADeviceName, 'ModbusParity', cbParity.ItemIndex);
    IniF.WriteInteger(ADeviceName, 'ModbusTimeout', PBseModbusTimeout.Value);
    IniF.WriteBool(ADeviceName, 'ModbusRTS', RTSCheckBox.Checked);
    // RS
    IniF.WriteString(ADeviceName, 'RSSerNo', MaskEditSerNo.Text);
    IniF.WriteInteger(ADeviceName, 'RSComPort', PBseMagX1CommPort.Value);
    IniF.WriteInteger(ADeviceName, 'RSTimeout', PBseMagX1Timeout.Value);
    IniF.WriteInteger(ADeviceName, 'RSConvertor', rgConvertorRS485.ItemIndex);
    // GPRS
    IniF.WriteString(ADeviceName, 'GPRSIPAddress', edtTCPIPAddress.Text);
    IniF.WriteInteger(ADeviceName, 'GPRSPort', seTCPPort.Value);

    IniF.WriteInteger(ADeviceName, 'GPRSModbusSlaveID', PBseTCPModbusSlaveID.Value);
    IniF.WriteInteger(ADeviceName, 'GPRSModbusBaudRate', cbTCPBaudRate.ItemIndex);
    IniF.WriteInteger(ADeviceName, 'GPRSModbusParity', cbTCPParity.ItemIndex);
    IniF.WriteInteger(ADeviceName, 'GPRSModbusTimeout', PBseTCPModbusTimeout.Value);
  finally
    IniF.Free;
  end;
end;

procedure TformMagC1Enter.btnSaveClick(Sender: TObject);
begin
      SaveDeviceDetail(lbDevices.Items.Strings[lbDevices.ItemIndex]);
      btnSave.Enabled := False;
end;

procedure TformMagC1Enter.DeviceDetailChange(Sender: TObject);
begin
  btnSave.Enabled := (lbDevices.ItemIndex > 0);
end;

procedure TformMagC1Enter.btnAddClick(Sender: TObject);
var Name: string;
begin
  if InputQuery('New device', 'Please insert name of new device', Name) then
  begin
    if DeviceExist(Name) then
      Raise Exception.Create('Duplicate device name!')  //ukonci cteni
    else if not ValidDeviceName(Name) then
      Raise Exception.Create('Invalid device name!'#13#10'Device name cannot contains space.')  //ukonci cteni
    else
    begin
      lbDevices.ItemIndex := lbDevices.Items.Add(Name);
//      SetDefaultValues;
	  lbDevicesClick(lbDevices);
      SaveDeviceDetail(Name);
	end;
  end;
end;

procedure TformMagC1Enter.btnDelClick(Sender: TObject);
var I: Integer;
begin
  I := lbDevices.ItemIndex;
  if MessageDlg('Do you really want to delete device "' + lbDevices.Items[I] + '"?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then begin
    RemoveDevice(lbDevices.Items[I]);
    lbDevices.Items.Delete(I);
    if I >= lbDevices.Items.Count then Dec(I);
    lbDevices.ItemIndex := I;
    lbDevicesClick(lbDevices);
  end;
end;

procedure TformMagC1Enter.LoadDevices;
var IniF: TIniFile;
	sExePath:string;
begin
  sExePath := GetUserAppDataProductPath();
  iniF := TIniFile.Create(sExePath+'MagC1Devices.ini');
//  IniF := TIniFile.Create(IncludeTrailingBackslash(ExtractFilePath(Application.ExeName))+'Devices.ini');
  try
    IniF.ReadSections(lbDevices.Items);
    if not(DeviceExist(STR_USER_DEF)) then
    begin
    	lbDevices.Items.Insert(0, STR_USER_DEF);
    	lbDevices.ItemIndex := 0;
   end;
//    lbDevicesClick(lbDevices);
  finally
    IniF.Free;
  end;
end;

procedure TformMagC1Enter.RemoveDevice(ADeviceName: String);
var IniF: TIniFile;
	sExePath:string;
begin
  sExePath := GetUserAppDataProductPath();
  IniF := TIniFile.Create(sExePath+'MagC1Devices.ini');
//  IniF := TIniFile.Create(IncludeTrailingBackslash(ExtractFilePath(Application.ExeName))+'Devices.ini');
  try
    IniF.EraseSection(ADeviceName);
  finally
    IniF.Free;
  end;
end;

function TformMagC1Enter.DeviceExist(ADeviceName: String): Boolean;
begin
  Result := (lbDevices.Items.IndexOf(ADeviceName) >= 0);
end;

function TformMagC1Enter.ValidDeviceName(ADeviceName: String): Boolean;
begin
//  Result := ((Pos('[', ADeviceName) = 0) and (Pos(']', ADeviceName) = 0));
	Result := Pos(' ', ADeviceName) = 0;
end;

procedure TformMagC1Enter.SetDefaultValues;
begin
  pcProtokolType.ActivePageIndex:=0;	//MODBUS
//MODBUS
  cbModbusCommPort.ItemIndex:=-1;
  PBseModbusSlaveID.Value:=1;
  cbBaudRate.ItemIndex:=1;
  cbParity.ItemIndex:=0;
  PBseModbusTimeout.Value:=1;
  RTSCheckBox.Checked := false;
//MagX1
  PBseMagX1CommPort.Value:=1;
  MaskEditSerNo.Text:=Format('%0.7d',[1]);
  rgConvertorRS485.ItemIndex:=0;
  PBseMagX1Timeout.Value:=1;
//GPRS
  PBseTCPModbusSlaveID.Value:=1;
  cbTCPBaudRate.ItemIndex:=2;	//19200
  cbTCPParity.ItemIndex:=3;      //none
  PBseTCPModbusTimeout.Value:=3;
  edtTCPIPAddress.Text := '0.0.0.0';
  seTCPPort.Value := 987;
end;

procedure TformMagC1Enter.btnExitClick(Sender: TObject);
begin
//	SaveLatestItemName;
end;

procedure TformMagC1Enter.SaveLatestItemName;
var iniF: TIniFile;
    sExePath: string;
begin
  sExePath := GetUserAppDataProductPath();
  iniF := TIniFile.Create(sExePath+'MagC1.ini');
  try
  	iniF.WriteString('Enter','LatestItemName',lbDevices.Items.Strings[lbDevices.ItemIndex]);
  finally
    iniF.Free;
  end;
end;

procedure TformMagC1Enter.TranslateForm;
var Idx: Integer;
begin
  lblLanguage.Caption := GlobalLanguageVersionName;
  lblDevList.Caption := GetTranslationText('DEV_LIST', lblDevList.Caption);
  btnService.Caption := GetTranslationText('SERV', btnService.Caption);
  btnStatistic.Caption := GetTranslationText('STAT', btnStatistic.Caption);
  btnExit.Caption := GetTranslationText('EXIT', btnExit.Caption);
  TabSheetModbus.Caption := GetTranslationText('MODBUS', TabSheetModbus.Caption);
  lblModbusSlaveID.Caption := GetTranslationText('MODBUS_SLAVE_ID', lblModbusSlaveID.Caption);
  lblComPortNum.Caption := GetTranslationText('COM_PORT_NUM', lblComPortNum.Caption);
  lblBaudRate.Caption := GetTranslationText('BAUD_RATE', lblBaudRate.Caption);
  lblParity.Caption := GetTranslationText('PARITY', lblParity.Caption);
  lblTimeout.Caption := GetTranslationText('TIMEOUT', lblTimeout.Caption);
  RTSCheckBox.Caption := GetTranslationText('RTS_FLOW', RTSCheckBox.Caption);
  DemoCheckBox.Caption := GetTranslationText('DEMO', DemoCheckBox.Caption);
  STR_USER_DEF := GetTranslationText('USER_DEF', STR_USER_DEF);
  Idx := lbDevices.ItemIndex;
  LoadDevices;
  lbDevices.ItemIndex := Idx;
  lbDevicesClick(lbDevices);
end;

end.
