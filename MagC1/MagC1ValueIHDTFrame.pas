unit MagC1ValueIHDTFrame;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  MagC1ValueIHexFrame, ExtCtrls, StdCtrls;

// IHDT = Info Hex DateTime

type
  TframeMagC1ValueIHDT = class(TframeMagC1ValueIHex)
  private
    { Private declarations }
  public
    { Public declarations }
    function GetValueText(Value:Integer): String; override;
  end;

implementation

{$R *.DFM}

{ TframeMagC1ValueIHDT }

function TframeMagC1ValueIHDT.GetValueText(Value: Integer): String;
begin
  Result := Format('%.8x', [Value]);
  Insert(':', Result, 7);
  Insert(' ', Result, 5);
  Insert('/', Result, 3);
end;

end.
