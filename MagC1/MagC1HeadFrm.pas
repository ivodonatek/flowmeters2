unit MagC1HeadFrm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, MagC1FunctionsUnit, StdCtrls, WaitFrm, ComCtrls, MagC1MyCommunicationClass;

type
  TformMagC1Head = class(TForm)
    Image21: TImage;
    Panel7: TPanel;
    lblSW: TLabel;
    lblFW: TLabel;
    Label5: TLabel;
    Label4: TLabel;
    Label8: TLabel;
    lblSerNo: TLabel;
    StatusBar1: TStatusBar;
    lblDemo: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    firmwareNO: Cardinal;
    ProtokolType:TProtokolType;
    Values: TStringList; // ??? TODO
    procedure Init(AMyCommunication : TMyCommunication; AfirmwareNO:integer; AProtokolType:TProtokolType);
  end;

var
  formMagC1Head: TformMagC1Head;

implementation

uses MagX1Exception, MagC1GlobalUtilsClass, MagC1TranslationUnit;

{$R *.DFM}

procedure TformMagC1Head.Init(AMyCommunication : TMyCommunication; AfirmwareNO:integer; AProtokolType:TProtokolType);
var ReadOK:boolean;
    FourByte : T4Byte;
    sExePath, filename: string;
begin
  sExePath := ExtractFilePath(Application.ExeName) + 'MagC1\';
  ProtokolType := AProtokolType;
  firmwareNO := AfirmwareNO;
    lblFW.Caption:=format('%2d.%0.2d',[firmwareNO div 100,firmwareNO mod 100]);
    lblSW.Caption:= GetVersion; //zjisti verzi SW

    case AProtokolType of
      ptMagX1:
      	case firmwareNO of
          105:
          begin
            Values.LoadFromFile(sExePath+'Values105.dat');
          end;
          106,107:
          begin
            Values.LoadFromFile(sExePath+'Values106.dat');
          end
          else
          begin
            AMyCommunication.Disconnect;
            Raise EUnsuportedFirmwareException.Create(GetTranslationText('UNSUPPORTED_FW', STR_UNSUPPORTED_FW));  //ukonci cteni
          end
        end;
      ptModbus,
      ptTCPModbus:
      begin
        filename:='Modbus'+IntToStr(firmwareNO)+'.dat';
        if FileExists(sExePath+filename) then
          Values.LoadFromFile(sExePath+filename)
        else
          Raise EUnsuportedFirmwareException.Create(GetTranslationText('UNSUPPORTED_FW', STR_UNSUPPORTED_FW));  //ukonci cteni
	  end;
      ptDemo:
      begin
        lblDemo.Visible:=true;
        filename:='Modbus'+IntToStr(firmwareNO)+'.dat';
        if FileExists(sExePath+filename) then
          Values.LoadFromFile(sExePath+filename)
        else
          Raise EUnsuportedFirmwareException.Create(GetTranslationText('UNSUPPORTED_FW', STR_UNSUPPORTED_FW));  //ukonci cteni
	  end
      else
        Raise Exception.Create(GetTranslationText('UNKNOWN_COMM_PROTOCOL', STR_UNKNOWN_COMM_PROTOCOL))
    end;

    //Seriove cislo
    repeat
      ReadOK:=AMyCommunication.ReadMenuValue(MODBUS_REGISTRY_ADDRESS_UNIT_NO, FourByte);
    until (ReadOK)or(MessageDlg(GetTranslationText('READ_ERROR', STR_READ_ERROR), mtError, [mbRetry, mbAbort], 0) = mrAbort);
    if ReadOK then
    begin
      lblSerNo.Caption:=FormatFloat('000000', Cardinal(FourByte));
    end
    else
      raise Exception.Create('Error read serial number');
end;

procedure TformMagC1Head.FormCreate(Sender: TObject);
begin
  Values:=TStringList.Create();
end;

procedure TformMagC1Head.FormDestroy(Sender: TObject);
begin
  Values.Free;
end;

end.
