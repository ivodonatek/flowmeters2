unit MagC1RunUnit;

interface

procedure MagC1Run;

implementation

uses sysutils, controls, Dialogs, Forms, MagC1TranslationUnit,
  MagC1GlobalUtilsClass, MagC1MyCommunicationClass,
  MagC1StatFrm, MagC1EnterFrm, MagC1MenuFrm;

var
    Demo : Boolean;
    ReadOK:Boolean;
    firmwareNO:integer;
    MyCommunication : TMyCommunication;

function GetDeviceFirmwareNumber(AProtokolType:TProtokolType;ASlaveID,AComNumber:Cardinal;
   BaudRate:TBaudRate;StopBits:TStopBits;Parity:TParity;Timeout:Integer;
   RtsControl:TRtsControl;EnableDTROnOpen:Boolean;ASwitchWait:Cardinal;
   AIPAddress: String; APort: Integer):integer;
var firmwareNO:integer;
begin
    MyCommunication.SetCommunicationParam(AComNumber,ASlaveID,BaudRate,db8BITS,
        StopBits,Parity,RtsControl,EnableDTROnOpen,Timeout,AIPAddress,APort);
    if not MyCommunication.Connect() then
        raise Exception.Create('Comunication port error');
    //cti verzi FW
    repeat
      ReadOK:=MyCommunication.ReadFirmNO(Cardinal(firmwareNO));
    until (ReadOK)or(MessageDlg(GetTranslationText('READ_ERROR', STR_READ_ERROR), mtError, [mbRetry, mbAbort], 0) = mrAbort);
    if not ReadOK then
        raise Exception.Create('No device was detected');
    Result:=firmwareNO;
end;

procedure MagC1Run;
var
  statForm: TformMagC1Stat;
  menuForm: TformMagC1Menu;
begin
    MyCommunication:= nil;
    statForm:= nil;
    menuForm:= nil;

    try
      case formMagC1Enter.ShowModal of
        //statistic
        mrOK:
        begin
              case formMagC1Enter.ProtokolType of
                ptMagX1:MyCommunication:=TMyCommPort.Create();
                ptModbus:MyCommunication:=TMyModbus.Create();
                ptTCPModbus:MyCommunication:=TMyTCPModbus.Create();
                ptDemo:MyCommunication:=TDemoCommunication.Create();
              else
                raise Exception.Create('Unsuported protocol');
              end;
              //cti verzi firmware
              firmwareNO := GetDeviceFirmwareNumber(formMagC1Enter.ProtokolType,formMagC1Enter.SlaveID,
                      formMagC1Enter.ComPortNumber,formMagC1Enter.BaudRate,formMagC1Enter.StopBits,
                      formMagC1Enter.Parity,formMagC1Enter.Timeout,formMagC1Enter.RtsControl ,true,
                      formMagC1Enter.rgConvertorRS485.ItemIndex, formMagC1Enter.edtTCPIPAddress.Text,
                      formMagC1Enter.seTCPPort.Value);

              statForm := TformMagC1Stat.Create(Application);
              statForm.Caption := 'MagC1 statistic';
              statForm.Init(MyCommunication, firmwareNO);
        end;
        //Service
        mrNo:
        begin
              case formMagC1Enter.ProtokolType of
                ptMagX1:MyCommunication:=TMyCommPort.Create();
                ptModbus:MyCommunication:=TMyModbus.Create();
                ptTCPModbus:MyCommunication:=TMyTCPModbus.Create();
                ptDemo:MyCommunication:=TDemoCommunication.Create();
              else
                raise Exception.Create('Unsuported protocol');
              end;
              //cti verzi firmware
              firmwareNO := GetDeviceFirmwareNumber(formMagC1Enter.ProtokolType,formMagC1Enter.SlaveID,
                      formMagC1Enter.ComPortNumber,formMagC1Enter.BaudRate,formMagC1Enter.StopBits,
                      formMagC1Enter.Parity,formMagC1Enter.Timeout,formMagC1Enter.RtsControl ,true,
                      formMagC1Enter.rgConvertorRS485.ItemIndex, formMagC1Enter.edtTCPIPAddress.Text,
                      formMagC1Enter.seTCPPort.Value);

              menuForm := TformMagC1Menu.Create(Application);
              menuForm.Caption := 'MagC1 service';
              menuForm.Init(MyCommunication, firmwareNO, formMagC1Enter.ProtokolType);
        end;
        //Close
        mrCancel:
        begin
        end;
      end;

    except
      on E: Exception do
      begin
        MessageDlg(E.Message,mtError, [mbOk], 0);
        if MyCommunication <> nil then begin
          MyCommunication.Disconnect;
          FreeAndNil(MyCommunication);
        end;
        if statForm <> nil then statForm.Close;
        if menuForm <> nil then menuForm.Close;
      end;
    end;
end;

end.
