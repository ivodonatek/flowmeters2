unit MagC1TranslationUnit;

interface

uses INIFiles;

const
  LANG_FILE_NAME = 'MagC1\MagC1.dic';

var
  GlobalLanguageIndex: Integer = 0;  // 0 = EN, 1 = CZ, 2 = DE, 3 = SP, 4 = FR, 5 = RU
  GlobalLanguageSection: String = 'EN';
  GlobalLanguageVersionName: String = 'English version';
  LanguageIniFile: TINIFile = nil;

function GetTranslationText(ACode: String; ADefValue: String = ''): String;
//function TranslationTextExist(ACode: String): Boolean;

implementation

uses Forms, SysUtils;

function GetTranslationText(ACode: String; ADefValue: String = ''): String;
begin
  Result := ADefValue;
  if Assigned(LanguageIniFile) then begin
    Result := LanguageIniFile.ReadString(GlobalLanguageSection, ACode, ADefValue);
  end;
end;

{
function TranslationTextExist(ACode: String): Boolean;
begin
  Result := False;
  if Assigned(LanguageIniFile) then begin
    Result := LanguageIniFile.ValueExists(GlobalLanguageSection, ACode);
  end;
end;
}

initialization
LanguageIniFile := TIniFile.Create(IncludeTrailingBackslash(ExtractFilePath(Application.ExeName)) + LANG_FILE_NAME);

finalization
FreeAndNil(LanguageIniFile);

end.
