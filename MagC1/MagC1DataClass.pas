unit MagC1DataClass;

interface

uses MagC1MyCommunicationClass;

type
  TFiveMinData = Cardinal;
  TFiveMinsBlock = array[0..251] of Byte;

  TmyDate = record
    day : Word;
    month : Word;
    year : Word;
  end;

  TSecure = record
    Pass : string;
    Enabled : boolean;
  end;
  TmyTime = record
    sec : Integer;
    min : Integer;
    hour : Integer;
  end;

  THourData = record
    SPlus,SMinus : Cardinal;
    Ch : Word;
  end;
  THourDataRead=THourData;
  THoursBlock = array[0..239] of Byte;

  TDayData = record
    D,M: Byte;
    Y : Integer;
    SPlus,SMinus : Cardinal;
    Ch : Word;
  end;

  TStatData = record
    M : Byte;
    Y : Integer;
    SPlus,SMinus : Cardinal;
    SPlusDec,SMinusDec: Cardinal;
    Ch : Word;
  end;
  TStatDataRead = record
    D,M,Y : Byte;
    SPlus,SMinus : Cardinal;
    SPlusDec,SMinusDec: Cardinal;
    Ch : Word;
  end;
  TDaysFstBlock = array[0..207] of Byte;
  TDaysSndBlock = array[0..194] of Byte;
  TMonthBlock = array[0..21] of Byte;

  TMonthData = class
    MonthIdx : Byte; // Index in
{    Y : Integer;
    M : Byte;
}    FiveMins : array[1..31,0..23,0..11] of TFiveMinData;
    Hours    : array[1..31,0..23] of THourData;
    Days     : array[1..31] of TDayData;
    Month   : TStatData;

    function LoadFiveMinBlock(COMPort: TMyCommunication; D,H : Byte) : Boolean;
    function LoadHoursBlock(COMPort: TMyCommunication; D : Byte) : Boolean;
    function LoadDaysFirstBlock(COMPort: TMyCommunication) : Boolean;  {01-16}
    function LoadDaysSecondBlock(COMPort: TMyCommunication) : Boolean; {17-31}
    function LoadMonthBlock(COMPort: TMyCommunication) : Boolean;
    {---}
    function LoadWholeMonth(COMPort: TMyCommunication): Boolean;
//    function LoadWholeMonth(COMPort: TMyCommPort) : Boolean;
    {---}
    procedure SaveToFile;
    {---}
    function LoadFromFile(FileName : String) : Boolean;
  end;

  function MyTimeToInteger(AMyTime: TMyTime): Cardinal;
  function IntegerToMyTime(AValue: Cardinal): TMyTime;
  function MyDateToInteger(AMyDate: TMyDate): Cardinal;
  function IntegerToMyDate(AValue: Cardinal): TMyDate;

  function CardinalToBinaryString(Value: Cardinal; BitCnt: Integer): String;
  function CardinalToBinaryStr16(Value: Cardinal): String;
  function CardinalToBinaryStr32(Value: Cardinal): String;

implementation

uses Sysutils, Classes;

function MyTimeToInteger(AMyTime: TMyTime): Cardinal;
begin
  Result := (((AMyTime.hour div 10) * 16) + (AMyTime.hour mod 10));
  Result := Result * 256;
  Result := Result + (((AMyTime.min  div 10) * 16) + (AMyTime.min  mod 10));
  Result := Result * 256;
  Result := Result + (((AMyTime.sec  div 10) * 16) + (AMyTime.sec  mod 10))
end;

function IntegerToMyTime(AValue: Cardinal): TMyTime;
begin
  Result.sec := 10 * ((AValue mod 256) div 16) + (AValue mod 16);
  AValue := AValue div 256;
  Result.min := 10 * ((AValue mod 256) div 16) + (AValue mod 16);
  AValue := AValue div 256;
  Result.hour := 10 * ((AValue mod 256) div 16) + (AValue mod 16);
end;

function MyDateToInteger(AMyDate: TMyDate): Cardinal;
begin
  Result := (((AMyDate.year div 1000) * 16) + ((AMyDate.year mod 1000) div 100));
  Result := Result * 256;
  Result := Result + ((((AMyDate.year mod 100) div 10) * 16) + (AMyDate.year mod 10));
  Result := Result * 256;
  Result := Result + (((AMyDate.month div 10) * 16) + (AMyDate.month mod 10));
  Result := Result * 256;
  Result := Result + (((AMyDate.day   div 10) * 16) + (AMyDate.day   mod 10))
end;

function IntegerToMyDate(AValue: Cardinal): TMyDate;
begin
  Result.day := 10 * ((AValue mod 256) div 16) + (AValue mod 16);
  AValue := AValue div 256;
  Result.month := 10 * ((AValue mod 256) div 16) + (AValue mod 16);
  AValue := AValue div 256;
  Result.year := 10 * ((AValue mod 256) div 16) + (AValue mod 16);
  AValue := AValue div 256;
  Result.year := Result.year + 100 * (10 * ((AValue mod 256) div 16) + (AValue mod 16));
end;

function CardinalToBinaryString(Value: Cardinal; BitCnt: Integer): String;
var I: Integer;
begin
  Result := '';
  for I := 1 to BitCnt do begin
    Result := IntToStr(Value mod 2) + Result;
    Value := Value div 2;
  end;
end;


function CardinalToBinaryStr16(Value: Cardinal): String;
begin
  Result := CardinalToBinaryString(Value, 16);
end;


function CardinalToBinaryStr32(Value: Cardinal): String;
begin
  Result := CardinalToBinaryString(Value, 32);
end;

{ TMonthData }
//precti petiminutovou statistiku za 3H
function TMonthData.LoadFiveMinBlock(COMPort: TMyCommunication; D,H: Byte): Boolean;
var Data : TFiveMinsBlock;
    I,J : Integer;
    const Size=7;
begin
  Result:=False;
  if COMPort.ReadFiveMins(Data,MonthIdx,D,H) then begin
    for I:=0 to 2 do begin
      for J:=0 to 11 do begin
        FiveMins[D,H+I,J]:=(Cardinal(Data[12*Size*I+J*Size+3])shl 24)and High(Integer);   //12* protoye J 0-11
        FiveMins[D,H+I,J]:=FiveMins[D,H+I,J] or Cardinal(Data[12*Size*I+J*Size+4])shl 16;
        FiveMins[D,H+I,J]:=FiveMins[D,H+I,J] or Cardinal(Data[12*Size*I+J*Size+5])shl 8;
        FiveMins[D,H+I,J]:=FiveMins[D,H+I,J] or Cardinal(Data[12*Size*I+J*Size+6]);
        if (FiveMins[D,H+I,J]>9999999)and(FiveMins[D,H+I,J]<high(Cardinal))then
          FiveMins[D,H+I,J]:=-(not(FiveMins[D,H+I,J]or Low(Cardinal)))-1;
      end;
    end;
    Result:=True;
  end;
end;

function TMonthData.LoadHoursBlock(COMPort: TMyCommunication; D: Byte): Boolean;
var Data : THoursBlock;
    I : Integer;
const Size=10;
begin
  Result:=False;
  if COMPort.ReadHours(Data,MonthIdx,D) then begin
    for I:=0 to 23 do begin
      Hours[D][I].SPlus:=Cardinal(Data[Size*I])shl 24;
      Hours[D][I].SPlus:=Hours[D][I].SPlus or Cardinal(Data[Size*I+1])shl 16;
      Hours[D][I].SPlus:=Hours[D][I].SPlus or Cardinal(Data[Size*(I)+2])shl 8;
      Hours[D][I].SPlus:=Hours[D][I].SPlus or Cardinal(Data[Size*(I)+3]);

      Hours[D][I].SMinus:=Cardinal(Data[Size*(I)+4])shl 24;
      Hours[D][I].SMinus:=Hours[D][I].SMinus or Cardinal(Data[Size*(I)+5])shl 16;
      Hours[D][I].SMinus:=Hours[D][I].SMinus or Cardinal(Data[Size*(I)+6])shl 8;
      Hours[D][I].SMinus:=Hours[D][I].SMinus or Cardinal(Data[Size*(I)+7]);

      Hours[D][I].Ch:=Word(Data[Size*(I)+8])shl 8;
      Hours[D][I].Ch:=Hours[D][I].Ch or Word(Data[Size*(I)+9]);
    end;
    Result:=True;
  end;
end;

function TMonthData.LoadDaysFirstBlock(COMPort: TMyCommunication): Boolean;
var Data : TDaysFstBlock;
    I : Integer;
const Size=13;
begin
  Result:=False;
  if COMPort.ReadDays(Data,MonthIdx,1) then begin
    for I:=1 to 16 do begin
      Days[I].D:=Data[Size*(I-1)+0];
      Days[I].M:=Data[Size*(I-1)+1];
      Days[I].Y:=2000+Data[Size*(I-1)+2];

      Days[I].SPlus:=Cardinal(Data[Size*(I-1)+3])shl 24;
      Days[I].SPlus:=Days[I].SPlus or Cardinal(Data[Size*(I-1)+4])shl 16;
      Days[I].SPlus:=Days[I].SPlus or Cardinal(Data[Size*(I-1)+5])shl 8;
      Days[I].SPlus:=Days[I].SPlus or Cardinal(Data[Size*(I-1)+6]);

      Days[I].SMinus:=Cardinal(Data[Size*(I-1)+7])shl 24;
      Days[I].SMinus:=Days[I].SMinus or Cardinal(Data[Size*(I-1)+8])shl 16;
      Days[I].SMinus:=Days[I].SMinus or Cardinal(Data[Size*(I-1)+9])shl 8;
      Days[I].SMinus:=Days[I].SMinus or Cardinal(Data[Size*(I-1)+10]);

      Days[I].Ch:=Word(Data[Size*(I-1)+11])shl 8;
      Days[I].Ch:=Days[I].Ch or Word(Data[Size*(I-1)+12]);
    end;
    Result:=True;
  end;
end;

function TMonthData.LoadDaysSecondBlock(COMPort: TMyCommunication): Boolean;
var Data : TDaysFstBlock;
    I : Integer;
const Size=13;
begin
  Result:=False;
  if COMPort.ReadDays(Data,MonthIdx,17) then
  begin
    for I:=17 to 31 do begin
            Days[I].D:=Data[Size*(I-17)+0];
      Days[I].M:=Data[Size*(I-17)+1];
      Days[I].Y:=2000+Data[Size*(I-17)+2];

      Days[I].SPlus:=Cardinal(Data[Size*(I-17)+3])shl 24;
      Days[I].SPlus:=Days[I].SPlus or Cardinal(Data[Size*(I-17)+4])shl 16;
      Days[I].SPlus:=Days[I].SPlus or Cardinal(Data[Size*(I-17)+5])shl 8;
      Days[I].SPlus:=Days[I].SPlus or Cardinal(Data[Size*(I-17)+6]);

      Days[I].SMinus:=Cardinal(Data[Size*(I-17)+7])shl 24;
      Days[I].SMinus:=Days[I].SMinus or Cardinal(Data[Size*(I-17)+8])shl 16;
      Days[I].SMinus:=Days[I].SMinus or Cardinal(Data[Size*(I-17)+9])shl 8;
      Days[I].SMinus:=Days[I].SMinus or Cardinal(Data[Size*(I-17)+10]);

      Days[I].Ch:=Word(Data[Size*(I-17)+11])shl 8;
      Days[I].Ch:=Days[I].Ch or Word(Data[Size*(I-17)+12]);
    end;
    Result:=True;
  end;
end;

function TMonthData.LoadMonthBlock(COMPort: TMyCommunication): Boolean;
var Data : TMonthBlock;
begin
  Result:=False;
  if COMPort.ReadMonth(Data,MonthIdx) then begin
    Month.M:=Data[1];
    Month.Y:=2000+Data[2];
    Month.SPlus:=Cardinal(Data[3])shl 24;
    Month.SPlus:=Month.SPlus or Cardinal(Data[4])shl 16;
    Month.SPlus:=Month.SPlus or Cardinal(Data[5])shl 8;
    Month.SPlus:=Month.SPlus or Cardinal(Data[6]);
    Month.SPlusDec:=Cardinal(Data[7])shl 24;
    Month.SPlusDec:=Month.SPlusDec or Cardinal(Data[8])shl 16;
    Month.SPlusDec:=Month.SPlusDec or Cardinal(Data[9])shl 8;
    Month.SPlusDec:=Month.SPlusDec or Cardinal(Data[10]);

    Month.SMinus:=Cardinal(Data[11])shl 24;
    Month.SMinus:=Month.SMinus or Cardinal(Data[12])shl 16;
    Month.SMinus:=Month.SMinus or Cardinal(Data[13])shl 8;
    Month.SMinus:=Month.SMinus or Cardinal(Data[14]);
    Month.SMinusDec:=Cardinal(Data[15])shl 24;
    Month.SMinusDec:=Month.SMinusDec or Cardinal(Data[16])shl 16;
    Month.SMinusDec:=Month.SMinusDec or Cardinal(Data[17])shl 8;
    Month.SMinusDec:=Month.SMinusDec or Cardinal(Data[18]);

    Month.Ch:=Word(Data[19])shl 8;
    Month.Ch:=Month.Ch or Word(Data[20]);
    Result:=True;
  end;
end;

function TMonthData.LoadWholeMonth(COMPort: TMyCommunication): Boolean;
const MaxErr = 5;
var Errors,Err : Integer;
    D,H : Byte;
begin
  Errors:=0;

  for D:=1 to 31 do begin
    for H:=0 to 23 do begin
      Err:=0;
      while (not LoadFiveMinBlock(COMPort,D,H)) and (Err<MaxErr) do Inc(Err);
      if Err>=MaxErr then Inc(Errors);
    end;
  end;
  for D:=1 to 31 do begin
    Err:=0;
    while (not LoadHoursBlock(COMPort,D)) and (Err<MaxErr) do Inc(Err);
    if Err>=MaxErr then Inc(Errors);
  end;
  Err:=0;
  while (not LoadDaysFirstBlock(COMPort)) and (Err<MaxErr) do Inc(Err);
  if Err>=MaxErr then Inc(Errors);
  Err:=0;
  while (not LoadDaysSecondBlock(COMPort)) and (Err<MaxErr) do Inc(Err);
  if Err>=MaxErr then Inc(Errors);
  Err:=0;
  while (not LoadMonthBlock(COMPort)) and (Err<MaxErr) do Inc(Err);
  if Err>=MaxErr then Inc(Errors);
  Result:=(Errors=0);
end;

procedure TMonthData.SaveToFile;
var F : TFileStream;
begin
  F:=TFileStream.Create(Format('MnthIdx_%.2d.dat',[MonthIdx]),fmCreate);
  try
    F.WriteBuffer(FiveMins,SizeOf(FiveMins));
    F.WriteBuffer(Hours,SizeOf(Hours));
    F.WriteBuffer(Days,SizeOf(Days));
    F.WriteBuffer(Month,SizeOf(Month));
  finally
    F.Free;
  end;
end;

function TMonthData.LoadFromFile(FileName: String): Boolean;
var F : TFileStream;
begin
//  Y:=StrToIntDef(Copy(FileName,1,4),0);
//  M:=StrToIntDef(Copy(FileName,6,2),0);
//  result:=false;
  F:=TFileStream.Create(FileName,fmOpenRead);
  try
    F.ReadBuffer(FiveMins,SizeOf(FiveMins));
    F.ReadBuffer(Hours,SizeOf(Hours));
    F.ReadBuffer(Days,SizeOf(Days));
    F.ReadBuffer(Month,SizeOf(Month));
//    Result:=(Y<>0) and (M<>0);
    result:=true;
  finally
    F.Free;
  end;
end;

end.
