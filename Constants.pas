unit Constants;

interface

type
  TX = record
        Ch : Char;
        B  : Byte;
      end;

  ConvertWideChar = record
    From: Integer;
    B: Byte;
  end;

  TConvertWideChars = array of ConvertWideChar;
  PConvertWideChars = ^TConvertWideChars;

var
  ConvertWideCharsArabic: TConvertWideChars;
  ConvertWideCharsSpanish: TConvertWideChars;

const
  // Kazdy znak se musi napsat do pole ve tvaru ( Ch:'puvodni znak';B:kod v nasi znakove sade )
  //   po pridani znaku je nutne zvysit hodnotu velikosti pole ... array[1..XXX] ...
  //   pro predhlednost je pole rozdeleno do sekci pro jednotlive jazyky (pouze pro prehlednost)
  //   pro spravne nahrazeni znaku je nutne aby znak vypadal stejne v EXCELu v DELPHach
  Znaky : array[1..72] of TX = (

    // TO DO : doplnit tabulku znaku ... - soucasna data jsou pouze testovaci !!!

    //CZ - 19 - �������������������
    (Ch:'�';B:65), (Ch:'�';B:65), (Ch:'�';B:65), (Ch:'�';B:65), (Ch:'�';B:65),
    (Ch:'�';B:65), (Ch:'�';B:65), (Ch:'�';B:65), (Ch:'�';B:65), (Ch:'�';B:65),
    (Ch:'�';B:65), (Ch:'�';B:65), (Ch:'�';B:65), (Ch:'�';B:65), (Ch:'�';B:65),
    (Ch:'�';B:65), (Ch:'�';B:65), (Ch:'�';B:65), (Ch:'�';B:65),
    //DE -  7 - �������
    (Ch:'�';B:66), (Ch:'�';B:66), (Ch:'�';B:66), (Ch:'�';B:66), (Ch:'�';B:66),
    (Ch:'�';B:66), (Ch:'�';B:66),
    //FR - 22 - ����������������������
    (Ch:'�';B:67), (Ch:'�';B:67), (Ch:'�';B:67), (Ch:'�';B:67), (Ch:'�';B:67),
    (Ch:'�';B:67), (Ch:'�';B:67), (Ch:'�';B:67), (Ch:'�';B:67), (Ch:'�';B:67),
    (Ch:'�';B:67), (Ch:'�';B:67), (Ch:'�';B:67), (Ch:'�';B:67), (Ch:'�';B:67),
    (Ch:'�';B:67), (Ch:'�';B:67), (Ch:'�';B:67), (Ch:'�';B:67), (Ch:'�';B:67),
    (Ch:'�';B:67), (Ch:'�';B:67),
    //ES - 24 - ������������������������
    (Ch:'�';B:68), (Ch:'�';B:68), (Ch:'�';B:68), (Ch:'�';B:68), (Ch:'�';B:68),
    (Ch:'�';B:68), (Ch:'�';B:68), (Ch:'�';B:68), (Ch:'�';B:68), (Ch:'�';B:68),
    (Ch:'�';B:68), (Ch:'�';B:68), (Ch:'�';B:68), (Ch:'�';B:68), (Ch:'�';B:68),
    (Ch:'�';B:68), (Ch:'�';B:68), (Ch:'�';B:68), (Ch:'�';B:68), (Ch:'�';B:68),
    (Ch:'�';B:68), (Ch:'�';B:68), (Ch:'�';B:68), (Ch:'�';B:68)
  );

implementation

const

  ConstConvertWideCharsArabic : array[1..36] of ConvertWideChar = (
    (From:$0631;B:221), (From:$0630;B:222), (From:$062F;B:223), (From:$062E;B:224), (From:$062D;B:225),
    (From:$062C;B:226), (From:$062B;B:227), (From:$062A;B:228), (From:$0628;B:229), (From:$0627;B:230),
    (From:$0641;B:231), (From:$063A;B:232), (From:$0639;B:233), (From:$0638;B:234), (From:$0637;B:235),
    (From:$0636;B:236), (From:$0635;B:237), (From:$0634;B:238), (From:$0633;B:239), (From:$0632;B:240),
    (From:$0621;B:241), (From:$064A;B:242), (From:$0648;B:243), (From:$0647;B:244), (From:$0646;B:245),
    (From:$0645;B:246), (From:$0644;B:247), (From:$0643;B:248), (From:$0642;B:249), (From:$0629;B:250),
    (From:$0623;B:251), (From:$0626;B:252), (From:$0625;B:253), (From:$061F;B:254), (From:$0649;B:255),
    (From:$064B;B:32)
  );

  ConstConvertWideCharsSpanish : array[1..25] of ConvertWideChar = (
    (From:ord('�');B:196), (From:ord('�');B:213), (From:ord('�');B:200), (From:ord('�');B:205), (From:ord('�');B:210),
    (From:ord('�');B:193), (From:ord('�');B:199), (From:ord('�');B:203), (From:ord('�');B:206), (From:ord('�');B:212),
    (From:ord('�');B:216), (From:ord('�');B:194), (From:ord('�');B:201), (From:ord('�');B:214), (From:ord('�');B:207),
    (From:ord('�');B:195), (From:ord('�');B:202), (From:ord('�');B:204), (From:ord('�');B:215), (From:ord('�');B:195),
    (From:ord('�');B:202), (From:ord('�');B:208), (From:ord('�');B:215), (From:ord('�');B:202), (From:ord('n');B:175)
  );

procedure MakeConvertWideCharArrays;
var
  I, J: Integer;
begin
  // ConvertWideCharsArabic
  SetLength(ConvertWideCharsArabic, Length(ConstConvertWideCharsArabic));
  J := 0;
  for I := Low(ConstConvertWideCharsArabic) to High(ConstConvertWideCharsArabic) do begin
    ConvertWideCharsArabic[J] := ConstConvertWideCharsArabic[I];
    Inc(J);
  end;

  // ConvertWideCharsSpanish
  SetLength(ConvertWideCharsSpanish, Length(ConstConvertWideCharsSpanish));
  J := 0;
  for I := Low(ConstConvertWideCharsSpanish) to High(ConstConvertWideCharsSpanish) do begin
    ConvertWideCharsSpanish[J] := ConstConvertWideCharsSpanish[I];
    Inc(J);
  end;
end;

initialization
  MakeConvertWideCharArrays;
  
end.
