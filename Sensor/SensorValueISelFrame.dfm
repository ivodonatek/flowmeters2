inherited frameSensorValueISel: TframeSensorValueISel
  object Panel1: TPanel
    Left = 24
    Top = 48
    Width = 297
    Height = 49
    BevelInner = bvRaised
    BevelOuter = bvLowered
    Color = 14670037
    TabOrder = 0
    object cbValues: TComboBox
      Left = 16
      Top = 14
      Width = 185
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      TabOrder = 0
    end
    object btnWrite: TButton
      Left = 212
      Top = 11
      Width = 73
      Height = 25
      Caption = 'Write'
      TabOrder = 1
      OnClick = btnWriteClick
    end
  end
end
