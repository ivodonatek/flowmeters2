unit SensorPassFrm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, StdCtrls,
  Buttons, ExtCtrls, SensorMyCommunicationClass, WaitFrm;

type
  TformSensorPassword= class(TForm)
    GroupBox1: TGroupBox;
    Panel1: TPanel;
    eValue: TEdit;
    btnCancel: TButton;
    btnConfirm: TButton;
    procedure btnCancelClick(Sender: TObject);
    procedure btnConfirmClick(Sender: TObject);
    constructor CreateFrm(AOwner: TComponent;MyCommunication:TMyCommunication; Address:Integer;Caption:string);
  private
    { Private declarations }
    FMyCommunication : TMyCommunication;
    FPassModbusAddress : Integer;
  public
    { Public declarations }
  end;

var
  formSensorPassword: TformSensorPassword;
implementation

uses SensorGlobalUtilsClass, SensorTranslationUnit;

//uses MenuFrm;

{$R *.DFM}

constructor TformSensorPassword.CreateFrm(AOwner: TComponent;MyCommunication:TMyCommunication;Address:Integer;Caption:string);
begin
  inherited Create(AOwner);
  FMyCommunication:= MyCommunication;
  FPassModbusAddress := Address;
  GroupBox1.Caption := Caption;
end;


procedure TformSensorPassword.btnCancelClick(Sender: TObject);
begin
  ModalResult:=mrCancel;
end;

procedure TformSensorPassword.btnConfirmClick(Sender: TObject);
var FourByte:T4Byte;
    ReadOK:Boolean;
    LastCursor: TCursor;
begin
  inherited;
  LastCursor := Screen.Cursor;
  try
    Screen.Cursor:=crHourGlass;

    case FMyCommunication.ProtokolType of
      ptMagX1: raise Exception.Create('Unsuported protocol');
    else
        Integer(FourByte):=StrToIntDef(eValue.Text,0);
        repeat
          ReadOK:=FMyCommunication.WriteMenuValue(FPassModbusAddress-1 ,FourByte);
        until (ReadOK)or(MessageDlg(GetTranslationText('READ_ERROR', STR_READ_ERROR),mtError, [mbRetry, mbAbort], 0) = mrAbort);
        if not ReadOK then
        begin
          raise Exception.Create(GetTranslationText('COMMUNICATION_ERROR', STR_COMMUNICATION_ERROR));
        end;

        repeat
            ReadOK:=FMyCommunication.ReadMenuValue(FPassModbusAddress-1,FourByte);
        until (ReadOK)or(MessageDlg(GetTranslationText('READ_ERROR', STR_READ_ERROR),mtError, [mbRetry, mbAbort], 0) = mrAbort);
        if (ReadOK)and(Integer(FourByte)=1) then
          Modalresult:=mrOk
        else
        begin
          MessageDlg(GetTranslationText('INCORRECT_PASSWORD', STR_INCORRECT_PASSWORD), mtError, [mbOk], 0);
          eValue.SetFocus;
        end;
    end;
  finally
    Screen.Cursor:=LastCursor;
  end;
end;

end.
