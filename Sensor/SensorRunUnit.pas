unit SensorRunUnit;

interface

procedure SensorRun;

implementation

uses sysutils, controls, Dialogs, Forms, SensorTranslationUnit,
  SensorGlobalUtilsClass, SensorMyCommunicationClass,
  SensorStatFrm, SensorEnterFrm, SensorMenuFrm;

var
    Demo : Boolean;
    ReadOK:Boolean;
    firmwareNO:integer;
    MyCommunication : TMyCommunication;

function GetDeviceFirmwareNumber(AProtokolType:TProtokolType;ASlaveID,AComNumber:Cardinal;
   BaudRate:TBaudRate;StopBits:TStopBits;Parity:TParity;Timeout:Integer;
   RtsControl:TRtsControl;EnableDTROnOpen:Boolean;ASwitchWait:Cardinal;
   AIPAddress: String; APort: Integer):integer;
var firmwareNO:integer;
begin
    MyCommunication.SetCommunicationParam(AComNumber,ASlaveID,BaudRate,db8BITS,
        StopBits,Parity,RtsControl,EnableDTROnOpen,Timeout,AIPAddress,APort);
    if not MyCommunication.Connect() then
        raise Exception.Create('Comunication port error');
    //cti verzi FW
    repeat
      ReadOK:=MyCommunication.ReadFirmNO(Cardinal(firmwareNO));
    until (ReadOK)or(MessageDlg(GetTranslationText('READ_ERROR', STR_READ_ERROR), mtError, [mbRetry, mbAbort], 0) = mrAbort);
    if not ReadOK then
        raise Exception.Create('No device was detected');
    Result:=firmwareNO;
end;

procedure SensorRun;
var
  statForm: TformSensorStat;
  menuForm: TformSensorMenu;
begin
    MyCommunication:= nil;
    statForm:= nil;
    menuForm:= nil;

    try
      case formSensorEnter.ShowModal of
        //statistic
        mrOK:
        begin
              case formSensorEnter.ProtokolType of
                ptMagX1:MyCommunication:=TMyCommPort.Create();
                ptModbus:MyCommunication:=TMyModbus.Create();
                ptTCPModbus:MyCommunication:=TMyTCPModbus.Create();
                ptDemo:MyCommunication:=TDemoCommunication.Create();
              else
                raise Exception.Create('Unsuported protocol');
              end;
              //cti verzi firmware
              firmwareNO := GetDeviceFirmwareNumber(formSensorEnter.ProtokolType,formSensorEnter.SlaveID,
                      formSensorEnter.ComPortNumber,formSensorEnter.BaudRate,formSensorEnter.StopBits,
                      formSensorEnter.Parity,formSensorEnter.Timeout,formSensorEnter.RtsControl ,true,
                      formSensorEnter.rgConvertorRS485.ItemIndex, formSensorEnter.edtTCPIPAddress.Text,
                      formSensorEnter.seTCPPort.Value);

              statForm := TformSensorStat.Create(Application);
              statForm.Caption := 'Sensor statistic';
              statForm.Init(MyCommunication, firmwareNO);
        end;
        //Service
        mrNo:
        begin
              case formSensorEnter.ProtokolType of
                ptMagX1:MyCommunication:=TMyCommPort.Create();
                ptModbus:MyCommunication:=TMyModbus.Create();
                ptTCPModbus:MyCommunication:=TMyTCPModbus.Create();
                ptDemo:MyCommunication:=TDemoCommunication.Create();
              else
                raise Exception.Create('Unsuported protocol');
              end;
              //cti verzi firmware
              firmwareNO := GetDeviceFirmwareNumber(formSensorEnter.ProtokolType,formSensorEnter.SlaveID,
                      formSensorEnter.ComPortNumber,formSensorEnter.BaudRate,formSensorEnter.StopBits,
                      formSensorEnter.Parity,formSensorEnter.Timeout,formSensorEnter.RtsControl ,true,
                      formSensorEnter.rgConvertorRS485.ItemIndex, formSensorEnter.edtTCPIPAddress.Text,
                      formSensorEnter.seTCPPort.Value);

              menuForm := TformSensorMenu.Create(Application);
              menuForm.Caption := 'Sensor service';
              menuForm.Init(MyCommunication, firmwareNO, formSensorEnter.ProtokolType);
        end;
        //Close
        mrCancel:
        begin
        end;
      end;

    except
      on E: Exception do
      begin
        MessageDlg(E.Message,mtError, [mbOk], 0);
        if MyCommunication <> nil then begin
          MyCommunication.Disconnect;
          FreeAndNil(MyCommunication);
        end;
        if statForm <> nil then statForm.Close;
        if menuForm <> nil then menuForm.Close;
      end;
    end;
end;

end.
