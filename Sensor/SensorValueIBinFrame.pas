unit SensorValueIBinFrame;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, StdCtrls,
  SensorMyCommunicationClass, NodeClass, ExtCtrls, SensorFunctionsUnit,
  SensorValueHeadFrame;

type
  TframeSensorValueIBin = class(TframeSensorHead)
    Panel1: TPanel;
    lblValue: TLabel;
  private
    { Private declarations }
  protected
  public
    { Public declarations }
    constructor CreateFrame(AOwner: TComponent; MyCommunication : TMyCommunication ; EditNode: TNode); override;
    function GetValueText(Value: Integer): String;
  end;

implementation

uses SensorDataClass, SensorGlobalUtilsClass, SensorTranslationUnit;

{$R *.DFM}

constructor TframeSensorValueIBin.CreateFrame(AOwner: TComponent; MyCommunication: TMyCommunication; EditNode: TNode);
var
  ReadOK: Boolean;
  FourByte: T4Byte;
  LastCursor: TCursor;
begin
  inherited;
  LastCursor := Screen.Cursor;
  try
    if Assigned(WaitFrm) then WaitFrm.Show;
    Screen.Cursor:=crHourGlass;
    repeat
      Application.ProcessMessages;
      ReadOK := ReadValue(FEditNode.ModbusAdr - 1, FourByte);
    until (ReadOK) or (MessageDlg(GetTranslationText('READ_ERROR', STR_READ_ERROR),mtError, [mbRetry, mbAbort], 0) = mrAbort);
    if ReadOK then begin
      lblValue.Caption := GetValueText(Integer(FourByte));
    end else begin
      lblValue.Caption:='Read Error';
    end;
    if Assigned(WaitFrm) then WaitFrm.gWait.AddProgress(10);
    Application.ProcessMessages;
  finally
    Screen.Cursor:=LastCursor;
    if Assigned(WaitFrm) then
    	WaitFrm.Hide;
  end;
end;


function TframeSensorValueIBin.GetValueText(Value: Integer): String;
begin
  Result := CardinalToBinaryString(Cardinal(Value), FEditNode.Digits);
end;

end.
