inherited frameSensorValueBool: TframeSensorValueBool
  object Label1: TLabel
    Left = 32
    Top = 56
    Width = 30
    Height = 13
    Caption = 'Status'
    Transparent = True
  end
  object Panel1: TPanel
    Left = 24
    Top = 80
    Width = 185
    Height = 25
    BevelOuter = bvLowered
    Color = 14670037
    TabOrder = 0
    object lblValue: TLabel
      Left = 8
      Top = 5
      Width = 46
      Height = 13
      Caption = 'Unknown'
    end
  end
  object btnWrite: TButton
    Left = 136
    Top = 48
    Width = 73
    Height = 25
    Caption = 'Yes'
    TabOrder = 1
    OnClick = btnWriteClick
  end
end
