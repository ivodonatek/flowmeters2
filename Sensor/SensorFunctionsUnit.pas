unit SensorFunctionsUnit;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, comctrls,
  shlobj, FileCtrl, FunctionsUnit;

type TFlowUnit = (UKG_min,USG_min,m3_h,l_min,l_s);
type TVolumeUnit = (UKG,USG,m3,l);
type TTempUnit = (C,F);

  Procedure SetCenter(O:Tform);
  function Flow_Conversion(Am3h:real; typ:TFlowUnit):real;
  function Volume_Conversion(_m3:real; typ:TVolumeUnit):real;
  function Calibration_Flow_Conversion(Am3h:real; typ:TFlowUnit):real;
  function Invert_Flow_Conversion(inn:real; typ:TFlowUnit):real;
  function Invert_Volume_Conversion(inn:real; typ:TVolumeUnit):real;
  function Invert_Calibration_Flow_Conversion(inn:real; typ:TFlowUnit):real;
//  procedure SaveListViewToCSV(listview: TLIstview; const filename: string);
  function SaveToCSVFile(ListView:TListView;FileName : String):Boolean;
  function SaveToExcelCSVFile(ListView:TListView;FileName : String):Boolean;
  function GetVersion: string;
  function GetUserAppDataProductPath():string;
  function GetDateTimeFromHex(Date: Cardinal; Time: Cardinal): TDateTime;
  function FormatStatDate(ADateTime: TDateTime): String;
  function FormatStatTime(ADateTime: TDateTime): String;


Const
  FlowUnitList : Array[0..4] of String = ('UKG/min','USG/min','m3/h','l/min','l/s');
  VolumeUnitList: Array[0..3] of String = ('UKG','USG','m3','l');
  TempUnitList: Array[0..1] of String = ('C','F');

implementation

Procedure SetCenter(O:Tform);
begin

  O.Left:=(GetSystemMetrics(0) div 2) - (O.Width div 2) ;
  O.top:=(GetSystemMetrics(1) div 2) - (O.Height div 2);

end;

//prevede m3/h na jinou jednotku
function Flow_Conversion(Am3h:real; typ:TFlowUnit):real;
begin
  Result := 0;
  case typ of
    UKG_min{UKG/min }  : Result := 13.19815 * Am3h / 3.6;        // prevod na UKG/min
    USG_min {/* USG/min  */} : Result := 15.85032 * Am3h / 3.6;  // prevod na USG/min
    m3_h {/* m3/h  */}    : Result := Am3h;                      // prevod na m3/h
    l_min {/* l/min */}    : Result := 1000 * Am3h / 60;	 // prevod na l/min
    l_s {/* l/s */  }  : Result := Am3h / 3.6;	                 // prevod na l/s
  end;
end;

//prevede m3 na jinou jednotku
function Volume_Conversion(_m3:real; typ:TVolumeUnit):real;
begin
  Result := 0;
  case typ of
    UKG {UKG }  : Result := _m3 * 219.969248;       // prevod m3 na UKG
    USG {/* USG  */} : Result := _m3 * 264.172052;  // prevod m3 na USG
    m3 {/* m3 */}    : Result := _m3;               // prevod m3 na m3
    l {/* l */}    : Result := _m3 * 1000.0 ;	    // prevod m3 na l
  end;
end;

//prevede m3/h na jinou jednotku
function Calibration_Flow_Conversion(Am3h:real; typ:TFlowUnit):real;
begin
  Result := 0;
  case typ of
    UKG_min{UKG/min }  : Result := 13.19815 * Am3h / 3.6;        // prevod na UKG/min
    USG_min {/* USG/min  */} : Result := 15.85032 * Am3h / 3.6;  // prevod na USG/min
    m3_h {/* m3/h  */}    : Result := Am3h;                      // prevod na m3/h
    l_min {/* l/min */}    : Result := 1000 * Am3h / 60;	 // prevod na l/min
    l_s {/* l/s */  }  : Result := Am3h / 3.6;	                 // prevod na l/s
  end;
end;

//prevede jinou jednotku na m3/h
function Invert_Flow_Conversion(inn:real; typ:TFlowUnit):real;
begin
  Result := 0;
  case typ of
    UKG_min {UKG/min }  : Result := 3.6 * inn / 13.19815;      // prevod na UKG/min
    USG_min {/* USG/min  */} : Result := 3.6 * inn / 15.85032; // prevod na USG/min
    m3_h {/* m3/h  */}    : Result := inn;                     // prevod na m3/h
    l_min {/* l/min */}    : Result := 60 * inn / 1000;	       // prevod na l/min
    l_s {/* l/s */  }  : Result := 3.6 * inn;	               // prevod na l/s
  end;
end;

//prevede jinou jednotku na m3
function Invert_Volume_Conversion(inn:real; typ:TVolumeUnit):real;
begin
  Result := 0;
  case typ of
    UKG {UKG }  : Result := inn / 219.969248;       // prevod m3 na UKG
    USG {/* USG  */} : Result := inn / 264.172052;  // prevod m3 na USG
    m3 {/* m3 */}    : Result := inn;               // prevod m3 na m3
    l {/* l */}    : Result := inn/1000.0 ;	    // prevod m3 na l
  end;
end;

//prevede jinou jednotku na m3/h
function Invert_Calibration_Flow_Conversion(inn:real; typ:TFlowUnit):real;
begin
  Result := 0;
  case typ of
    UKG_min {UKG/min }  : Result := 3.6 * inn / 13.19815;      // prevod na UKG/min
    USG_min {/* USG/min  */} : Result := 3.6 * inn / 15.85032; // prevod na USG/min
    m3_h {/* m3/h  */}    : Result := inn;                     // prevod na m3/h
    l_min {/* l/min */}    : Result := 60 * inn / 1000;	       // prevod na l/min
    l_s {/* l/s */  }  : Result := 3.6 * inn;	               // prevod na l/s
  end;
end;

//http://www.swissdelphicenter.ch/en/showcode.php?id=1743
function SaveToCSVFile(ListView:TListView;FileName : String):Boolean;
var I : Integer;
  CSV : TStrings;
  s:string;
begin
    CSV := TStringList.Create;
    s:='';
    Try
      For I := 0 To ListView.Columns.Count-1 Do
      begin
        if i>0 then s:=s+',';
        s:=s+'"'+ListView.Columns.Items[i].Caption+'"';
      end;
      CSV.Add(s);    //hlavicka
      For I := 0 To ListView.Items.Count-1 Do
        CSV.Add(ListView.Items.Item[i].Caption+','+ListView.Items.Item[i].SubItems.CommaText);  //pole
      CSV.SaveToFile(FileName);
      Result := True;
    Finally
      CSV.Free;
    End;
end;

function SaveToExcelCSVFile(ListView:TListView;FileName : String):Boolean;
var I,j : Integer;
  CSV : TStrings;
  s:string;
begin
  CSV := TStringList.Create;
  s:='';
  Try
    For I := 0 To ListView.Columns.Count-1 Do
    begin
      if i>0 then s:=s+';';
      s:=s+'"'+ListView.Columns.Items[i].Caption+'"';
    end;
    CSV.Add(s);    //hlavicka
    For I := 0 To ListView.Items.Count-1 Do
    begin
      s:='"'+ListView.Items.Item[i].Caption+'"';
      for j:= 0 to ListView.Items.Item[i].SubItems.Count-1 do
        s:=s+';"'+ListView.Items.Item[i].SubItems.Strings[j]+'"';
      CSV.Add(s);
   end;
    CSV.SaveToFile(FileName);
    Result := True;
  Finally
    CSV.Free;
  End;
end;

function GetVersion():string;
var
  VerInfoSize: DWORD;
  VerInfo: Pointer;
  VerValueSize: DWORD;
  VerValue: PVSFixedFileInfo;
  Dummy: DWORD;
begin
  VerInfoSize := GetFileVersionInfoSize(PChar(ParamStr(0)), Dummy);
  GetMem(VerInfo, VerInfoSize);
  GetFileVersionInfo(PChar(ParamStr(0)), 0, VerInfoSize, VerInfo);
  VerQueryValue(VerInfo, '\', Pointer(VerValue), VerValueSize);
  with VerValue^ do
  begin
    Result := IntToStr(dwFileVersionMS shr 16);
    Result := Result + '.' + IntToStr(dwFileVersionMS and $FFFF);
    Result := Result + '.' + IntToStr(dwFileVersionLS shr 16);
    Result := Result + '.' + IntToStr(dwFileVersionLS and $FFFF);
  end;
  FreeMem(VerInfo, VerInfoSize);
end;

function GetUserAppDataProductPath():string;
var
    productFolder : string;
begin
    productFolder := GetUserAppDataPath() + 'Sensor\';

    if not DirectoryExists(productFolder) then
        ForceDirectories(productFolder);

    result := productFolder;
end;

function GetDateTimeFromHex(Date: Cardinal; Time: Cardinal): TDateTime;
var ST: TSystemTime;
begin
  try
    ST.wMilliseconds := 0;
    // Date
    ST.wDay := StrToInt(Format('%.2x', [Date mod 256]));
    Date := Date div 256;
    ST.wMonth := StrToInt(Format('%.2x', [Date mod 256]));
    Date := Date div 256;
    ST.wYear := StrToInt(Format('%.4x', [Date]));
    // Time
    ST.wSecond := StrToInt(Format('%.2x', [Time mod 256]));
    Time := Time div 256;
    ST.wMinute := StrToInt(Format('%.2x', [Time mod 256]));
    Time := Time div 256;
    ST.wHour := StrToInt(Format('%.4x', [Time]));
    // DateTime
    Result := SystemTimeToDateTime(ST);
  except
    Result := 0; // ERROR
  end;
end;

function FormatStatDate(ADateTime: TDateTime): String;
begin
  if ADateTime = 0 then begin
    Result := 'ERROR';
  end else begin
    Result := DateToStr(ADateTime);
  end;
end;

function FormatStatTime(ADateTime: TDateTime): String;
begin
  if ADateTime = 0 then begin
    Result := 'ERROR';
  end else begin
    Result := TimeToStr(ADateTime);
  end;
end;

end.

