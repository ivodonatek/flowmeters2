unit SensorValueISelFrame;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, SensorMyCommunicationClass, NodeClass,
  ExtCtrls, Buttons, SensorFunctionsUnit, SensorValueHeadFrame;

type
  TframeSensorValueISel = class(TframeSensorHead)
    Panel1: TPanel;
    cbValues: TComboBox;
    btnWrite: TButton;
    procedure btnWriteClick(Sender: TObject);
  protected
  private
    { Private declarations }
    procedure FillList;
  public
    { Public declarations }
    function GetValueText(FourByte : T4Byte) : String;
    constructor CreateFrame(AOwner: TComponent; MyCommunication : TMyCommunication;
    EditNode : TNode); override;
  end;

var
  frameSensorValueISel: TframeSensorValueISel;

implementation

uses SensorGlobalUtilsClass, SensorTranslationUnit, SensorMenuFrm;

{$R *.DFM}

{ TframeValueISel }


constructor TframeSensorValueISel.CreateFrame(AOwner: TComponent; MyCommunication : TMyCommunication;
    EditNode : TNode);
var ReadOK:Boolean;
  FourByte:T4Byte;
  LastCursor : TCursor;
begin
  inherited;
  FillList;

  LastCursor := Screen.Cursor;
  try
    if Assigned(WaitFrm) then
    	WaitFrm.Show;
    Screen.Cursor:=crHourGlass;

    repeat
      Application.ProcessMessages;
      ReadOK:=ReadValue(FEditNode.ModbusAdr-1,FourByte);
    until(ReadOK)or(MessageDlg(GetTranslationText('READ_ERROR', STR_READ_ERROR),mtError, [mbRetry, mbAbort], 0) = mrAbort);
    if ReadOK then
      cbValues.ItemIndex:=Integer(FourByte);
    WaitFrm.gWait.AddProgress(10);
    Application.ProcessMessages;
  finally
    Screen.Cursor:=LastCursor;
    if Assigned(WaitFrm) then
    	WaitFrm.Hide;
  end;
end;

procedure TframeSensorValueISel.FillList;
var I : Integer;
begin
  cbValues.Clear;
  for I:=1 to FEditNode.MultiTexts.Count-1 do begin
    if (TNodeText(FEditNode.MultiTexts.Items[I]).MultiText.Count>0) then begin
      cbValues.Items.Add(TNodeText(FEditNode.MultiTexts.Items[I]).MultiText[0]);
    end else begin
      cbValues.Items.Add('unknown');
    end;
  end;
end;

function TframeSensorValueISel.GetValueText(FourByte: T4Byte): String;
begin
  if (FEditNode.MultiTexts.Count>Integer(FourByte)) and (TNodeText(FEditNode.MultiTexts.Items[Integer(FourByte)]).MultiText.Count>0) then begin
    Result:=TNodeText(FEditNode.MultiTexts.Items[Integer(FourByte)]).MultiText[0];
  end else begin
    Result:='unknown';
  end;
end;

procedure TframeSensorValueISel.btnWriteClick(Sender: TObject);
var FourByte : T4Byte;
    WriteOK:Boolean;
    LastCursor : TCursor;
begin

  LastCursor := Screen.Cursor;
  try
    if Assigned(WaitFrm) then
    begin
    	WaitFrm.Show;
        WaitFrm.gWait.Progress:=0;
    end;
    Screen.Cursor:=crHourGlass;

    Integer(FourByte):=cbValues.ItemIndex;
    repeat
      Application.ProcessMessages;
      WriteOK:=WriteValue(FEditNode.ModbusAdr-1,FourByte);
    until (WriteOK)or(MessageDlg(GetTranslationText('WRITE_ERROR', STR_WRITE_ERROR),mtError, [mbRetry, mbAbort], 0) = mrAbort);
    if WriteOK then begin
      if( Pos('MODBUS_SLAVE_ADDRESS', FEditNode.ValueName)>0)or //Modbus comunication param
        ( Pos('MODBUS_BAUDRATE', FEditNode.ValueName)>0)or
        ( Pos('MODBUS_PARITY', FEditNode.ValueName)>0) then
      begin
        MessageDlg('You have changed transmitter communication parameters.'#13#10 +
                   'Please setup new communication parameters.',
                   mtWarning,[mbOK],0);
        //PostMessage(formSensorMenu.Handle, WM_NEED_RESET, 0, 0);
      end;
    end;
    WaitFrm.gWait.AddProgress(10);
    Application.ProcessMessages;
  finally
    Screen.Cursor:=LastCursor;
    if Assigned(WaitFrm) then
    	WaitFrm.Hide;
  end;
end;

end.
