; -- MagB1.iss --

[Setup]
AppName=Flowmeters 2
AppVerName=Flowmeters 2.1.0.3
AppPublisher=Arkon Flow Systems, s.r.o.
AppVersion=2.1.0.3
VersionInfoVersion=2.1.0.3
DefaultDirName={pf}\Arkon Flow Systems\Flowmeters_2\
DisableDirPage=no
Disablewelcomepage=no
DefaultGroupName=Arkon Flow Systems\
UninstallDisplayIcon={app}\
Compression=lzma
SolidCompression=yes

[Files]
Source: AgrimagP2\AgrimagP2_Error.dat; DestDir: {app}\AgrimagP2
Source: AgrimagP2\Menu1022.dat; DestDir: {app}\AgrimagP2
Source: AgrimagP2\Menu5007.dat; DestDir: {app}\AgrimagP2
Source: AgrimagP2\Modbus1022.dat; DestDir: {app}\AgrimagP2
Source: AgrimagP2\Modbus5007.dat; DestDir: {app}\AgrimagP2
Source: AgrimagP2\AgrimagP2.dic; DestDir: {app}\AgrimagP2
Source: AgrimagP2\Background.jpg; DestDir: {app}\AgrimagP2
Source: AgrimagP2\MagLogo.jpg; DestDir: {app}\AgrimagP2
Source: AgrimagP2\DemoValues.dat; DestDir: {userappdata}\AgrimagP2

Source: MAGB1\MagB1_Error.dat; DestDir: {app}\MAGB1
Source: MAGB1\Menu1022.dat; DestDir: {app}\MAGB1
Source: MAGB1\Modbus1022.dat; DestDir: {app}\MAGB1
Source: MAGB1\Menu1023.dat; DestDir: {app}\MAGB1
Source: MAGB1\Modbus1023.dat; DestDir: {app}\MAGB1
Source: MAGB1\Menu1024.dat; DestDir: {app}\MAGB1
Source: MAGB1\Modbus1024.dat; DestDir: {app}\MAGB1
Source: MAGB1\Menu1025.dat; DestDir: {app}\MAGB1
Source: MAGB1\Modbus1025.dat; DestDir: {app}\MAGB1
Source: MAGB1\Menu1026.dat; DestDir: {app}\MAGB1
Source: MAGB1\Modbus1026.dat; DestDir: {app}\MAGB1
Source: MAGB1\Menu1027.dat; DestDir: {app}\MAGB1
Source: MAGB1\Modbus1027.dat; DestDir: {app}\MAGB1
Source: MAGB1\Menu1028.dat; DestDir: {app}\MAGB1
Source: MAGB1\Modbus1028.dat; DestDir: {app}\MAGB1
Source: MAGB1\Menu1029.dat; DestDir: {app}\MAGB1
Source: MAGB1\Modbus1029.dat; DestDir: {app}\MAGB1
Source: MAGB1\Menu1030.dat; DestDir: {app}\MAGB1
Source: MAGB1\Modbus1030.dat; DestDir: {app}\MAGB1
Source: MAGB1\Menu1031.dat; DestDir: {app}\MAGB1
Source: MAGB1\Modbus1031.dat; DestDir: {app}\MAGB1
Source: MAGB1\Menu1032.dat; DestDir: {app}\MAGB1
Source: MAGB1\Modbus1032.dat; DestDir: {app}\MAGB1
Source: MAGB1\Menu1034.dat; DestDir: {app}\MAGB1
Source: MAGB1\Modbus1034.dat; DestDir: {app}\MAGB1
Source: MAGB1\Menu1040.dat; DestDir: {app}\MAGB1
Source: MAGB1\Modbus1040.dat; DestDir: {app}\MAGB1
Source: MAGB1\Menu3001.dat; DestDir: {app}\MAGB1
Source: MAGB1\Modbus3001.dat; DestDir: {app}\MAGB1
Source: MAGB1\Menu4000.dat; DestDir: {app}\MAGB1
Source: MAGB1\Modbus4000.dat; DestDir: {app}\MAGB1
Source: MAGB1\Menu4001.dat; DestDir: {app}\MAGB1
Source: MAGB1\Modbus4001.dat; DestDir: {app}\MAGB1
Source: MAGB1\Menu5001.dat; DestDir: {app}\MAGB1
Source: MAGB1\Modbus5001.dat; DestDir: {app}\MAGB1
Source: MAGB1\Menu5003.dat; DestDir: {app}\MAGB1
Source: MAGB1\Modbus5003.dat; DestDir: {app}\MAGB1
Source: MAGB1\Menu5004.dat; DestDir: {app}\MAGB1
Source: MAGB1\Modbus5004.dat; DestDir: {app}\MAGB1
Source: MAGB1\Menu5006.dat; DestDir: {app}\MAGB1
Source: MAGB1\Modbus5006.dat; DestDir: {app}\MAGB1
Source: MAGB1\Background.jpg; DestDir: {app}\MAGB1
Source: MAGB1\MagLogo.jpg; DestDir: {app}\MAGB1
Source: MAGB1\MagB1.dic; DestDir: {app}\MAGB1
Source: MAGB1\DemoValues.dat; DestDir: {userappdata}\MAGB1

Source: MAGB2\MagB2_Error.dat; DestDir: {app}\MAGB2
Source: MAGB2\MagB2_Error_RU.dat; DestDir: {app}\MAGB2
Source: MAGB2\Menu2010.dat; DestDir: {app}\MAGB2
Source: MAGB2\Modbus2010.dat; DestDir: {app}\MAGB2
Source: MAGB2\Menu2216.dat; DestDir: {app}\MAGB2
Source: MAGB2\Modbus2216.dat; DestDir: {app}\MAGB2
Source: MAGB2\Menu2217.dat; DestDir: {app}\MAGB2
Source: MAGB2\Modbus2217.dat; DestDir: {app}\MAGB2
Source: MAGB2\Menu2218.dat; DestDir: {app}\MAGB2
Source: MAGB2\Modbus2218.dat; DestDir: {app}\MAGB2
Source: MAGB2\Menu2219.dat; DestDir: {app}\MAGB2
Source: MAGB2\Modbus2219.dat; DestDir: {app}\MAGB2
Source: MAGB2\Menu2220.dat; DestDir: {app}\MAGB2
Source: MAGB2\Modbus2220.dat; DestDir: {app}\MAGB2
Source: MAGB2\Menu2221.dat; DestDir: {app}\MAGB2
Source: MAGB2\Modbus2221.dat; DestDir: {app}\MAGB2
Source: MAGB2\Background.jpg; DestDir: {app}\MAGB2
Source: MAGB2\MagLogo.jpg; DestDir: {app}\MAGB2
Source: MAGB2\MagB2.dic; DestDir: {app}\MAGB2
Source: MAGB2\DemoValues.dat; DestDir: {userappdata}\MAGB2

Source: MAGC1\MAGC1_Error.dat; DestDir: {app}\MAGC1
Source: MAGC1\Menu2010.dat; DestDir: {app}\MAGC1
Source: MAGC1\Menu6000.dat; DestDir: {app}\MAGC1
Source: MAGC1\Menu6001.dat; DestDir: {app}\MAGC1
Source: MAGC1\Modbus2010.dat; DestDir: {app}\MAGC1
Source: MAGC1\Modbus6000.dat; DestDir: {app}\MAGC1
Source: MAGC1\Modbus6001.dat; DestDir: {app}\MAGC1
Source: MAGC1\MAGC1.dic; DestDir: {app}\MAGC1
Source: MAGC1\Background.jpg; DestDir: {app}\MAGC1
Source: MAGC1\MagLogo.jpg; DestDir: {app}\MAGC1
Source: MAGC1\DemoValues.dat; DestDir: {userappdata}\MAGC1

Source: MagE1\MagE1_Error.dat; DestDir: {app}\MagE1
Source: MagE1\Menu1022.dat; DestDir: {app}\MagE1
Source: MagE1\Menu5007.dat; DestDir: {app}\MagE1
Source: MagE1\Modbus1022.dat; DestDir: {app}\MagE1
Source: MagE1\Modbus5007.dat; DestDir: {app}\MagE1
Source: MagE1\MagE1.dic; DestDir: {app}\MagE1
Source: MagE1\Background.jpg; DestDir: {app}\MagE1
Source: MagE1\MagLogo.jpg; DestDir: {app}\MagE1
Source: MagE1\DemoValues.dat; DestDir: {userappdata}\MagE1

Source: MAGX2\MagX2_Error.dat; DestDir: {app}\MAGX2
Source: MAGX2\MagX2_Error_RU.dat; DestDir: {app}\MAGX2
Source: MAGX2\MagX2.dic; DestDir: {app}\MAGX2
Source: MAGX2\Menu2010.dat; DestDir: {app}\MAGX2
Source: MAGX2\Modbus2010.dat; DestDir: {app}\MAGX2
Source: MAGX2\Menu2100.dat; DestDir: {app}\MAGX2
Source: MAGX2\Modbus2100.dat; DestDir: {app}\MAGX2
Source: MAGX2\Menu2101.dat; DestDir: {app}\MAGX2
Source: MAGX2\Modbus2101.dat; DestDir: {app}\MAGX2
Source: MAGX2\Menu2110.dat; DestDir: {app}\MAGX2
Source: MAGX2\Modbus2110.dat; DestDir: {app}\MAGX2
Source: MAGX2\Menu2111.dat; DestDir: {app}\MAGX2
Source: MAGX2\Modbus2111.dat; DestDir: {app}\MAGX2
Source: MAGX2\Menu2112.dat; DestDir: {app}\MAGX2
Source: MAGX2\Modbus2112.dat; DestDir: {app}\MAGX2
Source: MAGX2\Menu2113.dat; DestDir: {app}\MAGX2
Source: MAGX2\Modbus2113.dat; DestDir: {app}\MAGX2
Source: MAGX2\Menu2114.dat; DestDir: {app}\MAGX2
Source: MAGX2\Modbus2114.dat; DestDir: {app}\MAGX2
Source: MAGX2\Menu2115.dat; DestDir: {app}\MAGX2
Source: MAGX2\Modbus2115.dat; DestDir: {app}\MAGX2
Source: MAGX2\Menu2116.dat; DestDir: {app}\MAGX2
Source: MAGX2\Modbus2116.dat; DestDir: {app}\MAGX2
Source: MAGX2\Menu2117.dat; DestDir: {app}\MAGX2
Source: MAGX2\Modbus2117.dat; DestDir: {app}\MAGX2
Source: MAGX2\Menu2118.dat; DestDir: {app}\MAGX2
Source: MAGX2\Modbus2118.dat; DestDir: {app}\MAGX2
Source: MAGX2\Menu2119.dat; DestDir: {app}\MAGX2
Source: MAGX2\Modbus2119.dat; DestDir: {app}\MAGX2
Source: MAGX2\Menu2120.dat; DestDir: {app}\MAGX2
Source: MAGX2\Modbus2120.dat; DestDir: {app}\MAGX2
Source: MAGX2\Menu2122.dat; DestDir: {app}\MAGX2
Source: MAGX2\Modbus2122.dat; DestDir: {app}\MAGX2
Source: MAGX2\Menu2130.dat; DestDir: {app}\MAGX2
Source: MAGX2\Modbus2130.dat; DestDir: {app}\MAGX2
Source: MAGX2\Menu2131.dat; DestDir: {app}\MAGX2
Source: MAGX2\Modbus2131.dat; DestDir: {app}\MAGX2
Source: MAGX2\Menu2133.dat; DestDir: {app}\MAGX2
Source: MAGX2\Modbus2133.dat; DestDir: {app}\MAGX2
Source: MAGX2\Menu2134.dat; DestDir: {app}\MAGX2
Source: MAGX2\Modbus2134.dat; DestDir: {app}\MAGX2
Source: MAGX2\Menu2135.dat; DestDir: {app}\MAGX2
Source: MAGX2\Modbus2135.dat; DestDir: {app}\MAGX2
Source: MAGX2\Menu2136.dat; DestDir: {app}\MAGX2
Source: MAGX2\Modbus2136.dat; DestDir: {app}\MAGX2
Source: MAGX2\Menu2137.dat; DestDir: {app}\MAGX2
Source: MAGX2\Modbus2137.dat; DestDir: {app}\MAGX2
Source: MAGX2\Menu2138.dat; DestDir: {app}\MAGX2
Source: MAGX2\Modbus2138.dat; DestDir: {app}\MAGX2
Source: MAGX2\Menu2139.dat; DestDir: {app}\MAGX2
Source: MAGX2\Modbus2139.dat; DestDir: {app}\MAGX2
Source: MAGX2\Menu2140.dat; DestDir: {app}\MAGX2
Source: MAGX2\Modbus2140.dat; DestDir: {app}\MAGX2
Source: MAGX2\Menu2145.dat; DestDir: {app}\MAGX2
Source: MAGX2\Modbus2145.dat; DestDir: {app}\MAGX2
Source: MAGX2\Menu2150.dat; DestDir: {app}\MAGX2
Source: MAGX2\Modbus2150.dat; DestDir: {app}\MAGX2
Source: MAGX2\MAGX2_USB_VCOM.inf; DestDir: {app}\MAGX2
Source: MAGX2\Background.jpg; DestDir: {app}\MAGX2
Source: MAGX2\MagLogo.jpg; DestDir: {app}\MAGX2
Source: MAGX2\DemoValues.dat; DestDir: {userappdata}\MAGX2

Source: Sensor\Menu2010.dat; DestDir: {app}\Sensor
Source: Sensor\Menu3003.dat; DestDir: {app}\Sensor
Source: Sensor\Modbus2010.dat; DestDir: {app}\Sensor
Source: Sensor\Modbus3003.dat; DestDir: {app}\Sensor
Source: Sensor\Sensor_Error.dat; DestDir: {app}\Sensor
Source: Sensor\Sensor.dic; DestDir: {app}\Sensor
Source: Sensor\Background.jpg; DestDir: {app}\Sensor
Source: Sensor\MagLogo.jpg; DestDir: {app}\Sensor
Source: Sensor\DemoValues.dat; DestDir: {userappdata}\Sensor

Source: MainScreen.jpg; DestDir: {app}\
Source: Flowmeters.exe; DestDir: {app}\
Source: Logo.jpg; DestDir: {app}\
Source: Translation.dic; DestDir: {app}\


[Icons]
Name: {group}\Flowmeters; Filename: {app}\Flowmeters.exe
Name: {commondesktop}\Flowmeters; Filename: {app}\Flowmeters.exe

[Run]
Filename: {sys}\rundll32.exe; Parameters: setupapi,InstallHinfSection Uninstall 132 {app}\MAGX2\MAGX2_USB_VCOM.inf; Description: Install MAGX2 USB driver; Flags: postinstall nowait skipifsilent unchecked
Filename: {app}\Flowmeters.exe; Description: Launch application; Flags: postinstall nowait skipifsilent unchecked
