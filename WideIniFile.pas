unit WideIniFile;

interface

uses TntClasses, Contnrs, SysUtils;

type
  TIntegerList = class
  private
    fCapacity           : Integer;
    fItems              : Array of Integer;
    fCount              : Integer;
    function GetItem (x: Integer): Integer;
    procedure SetCapacity (c: Integer);
    procedure SetItem (x: Integer; v: Integer);
  public
    property Capacity: Integer Read fCapacity Write SetCapacity;
    property Items[x: Integer]: Integer Read GetItem Write SetItem;
    property Count: Integer Read fCount;
    destructor Destroy; Override;
    procedure Add (v: Integer);
    procedure Clear;
  end;

  TWideKeyValuePair = class
  private
    fKey: WideString;
    fValue: WideString;
  public
    property Key: WideString Read fKey;
    property Value: WideString Read fValue Write fValue;
    constructor Create(key, value: WideString);
  end;

  TWideSection = class
  private
    fName: WideString;
    fPairs: TObjectList;
    procedure LoadFromSubRows(subRows: TTntStringList);
  public
    property Name: WideString Read fName;
    constructor Create(name: WideString; subRows: TTntStringList = nil);
    destructor Destroy; Override;
    function GetFileSubRows: TTntStringList;
    function GetKeyValuePair(key: WideString): TWideKeyValuePair;
    function ReadString(key: WideString; var str: WideString): Boolean;
    function ReadInteger(key: WideString; var val: Integer): Boolean;
    function ReadBool(key: WideString; var val: Boolean): Boolean;
    procedure WriteString(key, value: WideString);
    procedure WriteInteger(key: WideString; value: Integer);
    procedure WriteBool(key: WideString; value: Boolean);
  end;

  TWideSections = class
  private
    fSections: TObjectList;
    procedure LoadFromRows(fileRows: TTntStringList);
    function GetSectionRowIndexes(fileRows: TTntStringList): TIntegerList;
    function IsSectionRow(fileRow: WideString): Boolean;
    function GetSectionName(fileRow: WideString): WideString;
    function GetSectionSubRows(fileRows: TTntStringList; sectionRowIndex: Integer): TTntStringList;
    function GetSection(sectionName: WideString): TWideSection;
  public
    constructor Create(fileRows: TTntStringList);
    destructor Destroy; Override;
    function GetFileRows: TTntStringList;
    procedure ReadSections(sectionNames: TTntStrings);
    function ReadString(section, key: WideString; var str: WideString): Boolean;
    function ReadInteger(section, key: WideString; var val: Integer): Boolean;
    function ReadBool(section, key: WideString; var val: Boolean): Boolean;
    procedure WriteString(section, key, value: WideString);
    procedure WriteInteger(section, key: WideString; value: Integer);
    procedure WriteBool(section, key: WideString; value: Boolean);
    procedure EraseSection(section: WideString);
  end;

  TWideIniFile = class
  private
    fFilePath: WideString;
    fSections: TWideSections;
  public
    constructor Create(filePath: WideString);
    destructor Destroy; Override;
    procedure Save;
    procedure ReadSections(sectionNames: TTntStrings);
    function ReadString(section, key, defValue: WideString): WideString;
    function ReadInteger(section, key: WideString; defValue: Integer): Integer;
    function ReadBool(section, key: WideString; defValue: Boolean): Boolean;
    procedure WriteString(section, key, value: WideString);
    procedure WriteInteger(section, key: WideString; value: Integer);
    procedure WriteBool(section, key: WideString; value: Boolean);
    procedure EraseSection(section: WideString);
  end;

implementation

{ TIntegerList }
procedure TIntegerList.Add(v: Integer);
begin
     if (fCount >= fCapacity) then
     begin
         fCapacity := fCapacity + 10;
         SetLength (fItems, fCapacity);
     end;

     fItems[fCount] := v;
     Inc(fCount);
end;

procedure TIntegerList.Clear;
begin
     SetCapacity (0);
end;

destructor TIntegerList.Destroy;
begin
     Clear;
     inherited;
end;

function TIntegerList.GetItem(x: Integer): Integer;
begin
     Result := fItems[x];
end;

procedure TIntegerList.SetCapacity(c: Integer);
begin
     fCapacity := c;
     SetLength (fItems, fCapacity);
     if (fCount > fCapacity) then
         fCount := fCapacity;
end;

procedure TIntegerList.SetItem(x, v: Integer);
begin
     if (x < fCount) then
         fItems[x] := v;
end;

{ TWideIniFile }
constructor TWideIniFile.Create(filePath: WideString);
var
  fileHandle: Integer;
  rows: TTntStringList;
begin
  if not FileExists(filePath) then begin
    fileHandle := FileCreate(filePath);
    FileClose(fileHandle);
  end;

  fFilePath:= filePath;
  rows:= nil;
  try
    try
      rows:= TTntStringList.Create;
      rows.LoadFromFile(filePath);
      fSections:= TWideSections.Create(rows);
    finally
      rows.Free;
    end;
  except
  end;
end;

destructor TWideIniFile.Destroy;
begin
  fSections.Free;
  inherited;
end;

procedure TWideIniFile.Save;
var
  rows: TTntStringList;
begin
  rows:= nil;
  try
    try
      rows:= fSections.GetFileRows;
      rows.SaveToFile(fFilePath);
    finally
      rows.Free;
    end;
  except
  end;
end;

procedure TWideIniFile.ReadSections(sectionNames: TTntStrings);
begin
  try
    fSections.ReadSections(sectionNames);
  except
  end;
end;

function TWideIniFile.ReadString(section, key, defValue: WideString): WideString;
var
  str: WideString;
begin
  Result:= defValue;
  try
    if fSections.ReadString(section, key, str) then Result:= str;
  except
  end;
end;

function TWideIniFile.ReadInteger(section, key: WideString; defValue: Integer): Integer;
var
  val: Integer;
begin
  Result:= defValue;
  try
    if fSections.ReadInteger(section, key, val) then Result:= val;
  except
  end;
end;

function TWideIniFile.ReadBool(section, key: WideString; defValue: Boolean): Boolean;
var
  val: Boolean;
begin
  Result:= defValue;
  try
    if fSections.ReadBool(section, key, val) then Result:= val;
  except
  end;
end;

procedure TWideIniFile.WriteString(section, key, value: WideString);
begin
  try
    fSections.WriteString(section, key, value);
  except
  end;
end;

procedure TWideIniFile.WriteInteger(section, key: WideString; value: Integer);
begin
  try
    fSections.WriteInteger(section, key, value);
  except
  end;
end;

procedure TWideIniFile.WriteBool(section, key: WideString; value: Boolean);
begin
  try
    fSections.WriteBool(section, key, value);
  except
  end;
end;

procedure TWideIniFile.EraseSection(section: WideString);
begin
  try
    fSections.EraseSection(section);
  except
  end;
end;

{ TWideSections }
constructor TWideSections.Create(fileRows: TTntStringList);
begin
  fSections:= TObjectList.Create;
  LoadFromRows(fileRows);
end;

destructor TWideSections.Destroy;
begin
  fSections.Free;
  inherited;
end;

function TWideSections.GetFileRows: TTntStringList;
var
  i, j: Integer;
  section: TWideSection;
  rows, subRows: TTntStringList;
  str: WideString;
begin
  rows:= TTntStringList.Create;
  Result:= rows;
  try
    for i:= 0 to fSections.Count - 1 do begin
      section:= fSections[i] as TWideSection;
      str:= '[]';
      Insert(section.Name, str, 2);
      rows.Add(str);
      subRows:= section.GetFileSubRows;
      try
        for j:= 0 to subRows.Count - 1 do begin
          rows.Add(subRows[j]);
        end;
      finally
        subRows.Free;
      end;
    end;
  except
  end;
end;

procedure TWideSections.ReadSections(sectionNames: TTntStrings);
var
  i: Integer;
  section: TWideSection;
begin
  try
    sectionNames.Clear;
    for i:= 0 to fSections.Count - 1 do begin
      section:= fSections[i] as TWideSection;
      sectionNames.Append(section.Name);
    end;
  except
  end;
end;

function TWideSections.ReadString(section, key: WideString; var str: WideString): Boolean;
var
  wideSection: TWideSection;
begin
  Result:= false;
  try
    wideSection:= GetSection(section);
    if wideSection <> nil then begin
      if wideSection.ReadString(key, str) then Result:= true;
    end;
  except
  end;
end;

function TWideSections.ReadInteger(section, key: WideString; var val: Integer): Boolean;
var
  wideSection: TWideSection;
begin
  Result:= false;
  try
    wideSection:= GetSection(section);
    if wideSection <> nil then begin
      if wideSection.ReadInteger(key, val) then Result:= true;
    end;
  except
  end;
end;

function TWideSections.ReadBool(section, key: WideString; var val: Boolean): Boolean;
var
  wideSection: TWideSection;
begin
  Result:= false;
  try
    wideSection:= GetSection(section);
    if wideSection <> nil then begin
      if wideSection.ReadBool(key, val) then Result:= true;
    end;
  except
  end;
end;

procedure TWideSections.WriteString(section, key, value: WideString);
var
  wideSection: TWideSection;
begin
  try
    wideSection:= GetSection(section);
    if wideSection = nil then begin
      wideSection:= TWideSection.Create(section, nil);
      fSections.Add(wideSection);
    end;
    wideSection.WriteString(key, value);
  except
  end;
end;

procedure TWideSections.WriteInteger(section, key: WideString; value: Integer);
var
  wideSection: TWideSection;
begin
  try
    wideSection:= GetSection(section);
    if wideSection = nil then begin
      wideSection:= TWideSection.Create(section, nil);
      fSections.Add(wideSection);
    end;
    wideSection.WriteInteger(key, value);
  except
  end;
end;

procedure TWideSections.WriteBool(section, key: WideString; value: Boolean);
var
  wideSection: TWideSection;
begin
  try
    wideSection:= GetSection(section);
    if wideSection = nil then begin
      wideSection:= TWideSection.Create(section, nil);
      fSections.Add(wideSection);
    end;
    wideSection.WriteBool(key, value);
  except
  end;
end;

function TWideSections.GetSection(sectionName: WideString): TWideSection;
var
  i: Integer;
  section: TWideSection;
begin
  Result:= nil;
  try
    for i:= 0 to fSections.Count - 1 do begin
      section:= fSections[i] as TWideSection;
      if section.Name = sectionName then begin
        Result:= section;
        Break;
      end;
    end;
  except
  end;
end;

procedure TWideSections.LoadFromRows(fileRows: TTntStringList);
var
  indexes: TIntegerList;
  sectionName: WideString;
  sectionSubRows: TTntStringList;
  i: Integer;
  section: TWideSection;
begin
  indexes:= nil;
  sectionSubRows:= nil;
  try
    try
      indexes:= GetSectionRowIndexes(fileRows);
      for i:= 0 to indexes.Count - 1 do begin
        sectionName:= GetSectionName(fileRows[indexes.Items[i]]);
        sectionSubRows:= GetSectionSubRows(fileRows, indexes.Items[i]);
        section:= TWideSection.Create(sectionName, sectionSubRows);
        fSections.Add(section);
      end;
    finally
      indexes.Free;
      sectionSubRows.Free;
    end;
  except
  end;
end;

function TWideSections.GetSectionRowIndexes(fileRows: TTntStringList): TIntegerList;
var
  indexes: TIntegerList;
  i: Integer;
begin
  indexes:= TIntegerList.Create;
  Result:= indexes;
  for i:= 0 to fileRows.Count - 1 do begin
    if IsSectionRow(fileRows[i]) then begin
      indexes.Add(i);
    end;
  end;
end;

function TWideSections.IsSectionRow(fileRow: WideString): Boolean;
begin
  Result:= false;
  try
    if Length(fileRow) > 2 then begin
      if (fileRow[1] = '[') and (fileRow[Length(fileRow)] = ']') then Result:= true;
    end;
  except
  end;
end;

function TWideSections.GetSectionName(fileRow: WideString): WideString;
begin
  Result:= '';
  try
    if IsSectionRow(fileRow) then begin
      Result:= Copy(fileRow, 2, Length(fileRow) - 2);
    end;
  except
  end;
end;

function TWideSections.GetSectionSubRows(fileRows: TTntStringList; sectionRowIndex: Integer): TTntStringList;
var
  subRows: TTntStringList;
  i: Integer;
begin
  subRows:= TTntStringList.Create;
  Result:= SubRows;
  try
    for i:= sectionRowIndex + 1 to fileRows.Count - 1 do begin
      if IsSectionRow(fileRows[i]) then begin
        Break;
      end else begin
        subRows.Add(fileRows[i]);
      end;
    end;
  except
  end;
end;

procedure TWideSections.EraseSection(section: WideString);
var
  i: Integer;
  wideSection: TWideSection;
begin
  try
    for i:= 0 to fSections.Count - 1 do begin
      wideSection:= fSections[i] as TWideSection;
      if wideSection.Name = section then begin
        fSections.Remove(wideSection);
        Break;
      end;
    end;
  except
  end;
end;

{ TWideSection }
constructor TWideSection.Create(name: WideString; subRows: TTntStringList = nil);
begin
  fName:= name;
  fPairs:= TObjectList.Create;
  if subRows <> nil then
    LoadFromSubRows(subRows);
end;

destructor TWideSection.Destroy;
begin
  fPairs.Free;
  inherited;
end;

function TWideSection.GetFileSubRows: TTntStringList;
var
  i: Integer;
  pair: TWideKeyValuePair;
  rows: TTntStringList;
  str: WideString;
begin
  rows:= TTntStringList.Create;
  Result:= rows;
  try
    for i:= 0 to fPairs.Count - 1 do begin
      pair:= fPairs[i] as TWideKeyValuePair;
      str:= '=';
      Insert(pair.Key, str, 1);
      Insert(pair.Value, str, Length(str) + 1);
      rows.Add(str);
    end;
  except
  end;
end;

function TWideSection.GetKeyValuePair(key: WideString): TWideKeyValuePair;
var
  i: Integer;
  pair: TWideKeyValuePair;
begin
  Result:= nil;
  try
    for i:= 0 to fPairs.Count - 1 do begin
      pair:= fPairs[i] as TWideKeyValuePair;
      if pair.Key = key then begin
        Result:= pair;
        Break;
      end;
    end;
  except
  end;
end;

function TWideSection.ReadString(key: WideString; var str: WideString): Boolean;
var
  i: Integer;
  pair: TWideKeyValuePair;
begin
  Result:= false;
  try
    for i:= 0 to fPairs.Count - 1 do begin
      pair:= fPairs[i] as TWideKeyValuePair;
      if pair.Key = key then begin
        str:= pair.Value;
        Result:= true;
        Break;
      end;
    end;
  except
  end;
end;

function TWideSection.ReadInteger(key: WideString; var val: Integer): Boolean;
var
  str: WideString;
begin
  Result:= false;
  try
    if ReadString(key, str) then begin
      val:= StrToInt(str);
      Result:= true;
    end;
  except
  end;
end;

function TWideSection.ReadBool(key: WideString; var val: Boolean): Boolean;
var
  str: WideString;
  valInt: Integer;
begin
  Result:= false;
  try
    if ReadString(key, str) then begin
      valInt:= StrToInt(str);
      if valInt = 0 then val:= false else val:= true;
      Result:= true;
    end;
  except
  end;
end;

procedure TWideSection.WriteString(key, value: WideString);
var
  pair: TWideKeyValuePair;
begin
  try
    pair:= GetKeyValuePair(key);
    if pair = nil then begin
      pair:= TWideKeyValuePair.Create(key, '');
      fPairs.Add(pair);
    end;
    pair.Value:= value;
  except
  end;
end;

procedure TWideSection.WriteInteger(key: WideString; value: Integer);
var
  pair: TWideKeyValuePair;
begin
  try
    pair:= GetKeyValuePair(key);
    if pair = nil then begin
      pair:= TWideKeyValuePair.Create(key, '0');
      fPairs.Add(pair);
    end;
    pair.Value:= IntToStr(value);
  except
  end;
end;

procedure TWideSection.WriteBool(key: WideString; value: Boolean);
begin
  try
    if value then WriteInteger(key, 1) else WriteInteger(key, 0); 
  except
  end;
end;

procedure TWideSection.LoadFromSubRows(subRows: TTntStringList);
var
  i, posIdx: Integer;
  row, keyStr, valueStr: WideString;
  pair: TWideKeyValuePair;
begin
  try
    for i:= 0 to subRows.Count - 1 do begin
      row:= subRows[i];
      posIdx:= Pos('=', row);
      if posIdx <> 0 then begin
        keyStr:= Copy(row, 1, posIdx - 1);
        valueStr:= Copy(row, posIdx + 1, Length(row));
        pair:= TWideKeyValuePair.Create(keyStr, valueStr);
        fPairs.Add(pair);
      end;
    end;
  except
  end;
end;

{ TWideKeyValuePair }
constructor TWideKeyValuePair.Create(key, value: WideString);
begin
  fKey:= key;
  fValue:= value;
end;

end.
