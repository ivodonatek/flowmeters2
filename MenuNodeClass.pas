unit MenuNodeClass;

interface

uses
  Classes, NodeClass, Contnrs, Comctrls, Dialogs, Sysutils,
  TntComCtrls, TntExtCtrls, TntStdCtrls;

type
  TMenuNode = class;

  TMenuNodes = class(TObjectList)
  private
  public
    procedure LoadMenuNodes(Nodes : TNodes);
    procedure AddSubNodes(Nodes : TNodes; ParentMenuNode : TMenuNode);
    procedure FillTreeView(TreeView : TTreeView);
    procedure FillTntTreeView(TreeView : TTntTreeView);
    procedure FillTreeSubNodes(ParentNode : TTreeNode; ParentMenuNode : TMenuNode);
    procedure FillTntTreeSubNodes(ParentNode : TTntTreeNode; ParentMenuNode : TMenuNode);
  end;

  TMenuNode = class
  public
    Id,IdParent : Integer;
    Popis : WideString;
    Items : TMenuNodes;
    MenuEdit : TNode;
    constructor Create;
    destructor Destroy; override;
  end;

implementation

{ TMenuNode }

constructor TMenuNode.Create;
begin
  inherited;
  Items:=TMenuNodes.Create;
end;

destructor TMenuNode.Destroy;
begin
  Items.Free;
  inherited;
end;

{ TMenuNodes }

procedure TMenuNodes.LoadMenuNodes(Nodes: TNodes);
var TopNode : TMenuNode;
	i:Integer;
begin
  Clear;
(*  TopNode:=TMenuNode.Create;
  TopNode.Id:=0;
  TopNode.IdParent:=0;
  TopNode.Popis:='MENU';
*)
	for I:=0 To Nodes.Count-1 do
    begin
        if (TNode(Nodes.Items[I]).IdParent=0) and ((TNode(Nodes.Items[I]).Typ=0) or (TNode(Nodes.Items[I]).Typ=9)) then
        begin
          TopNode:=TMenuNode.Create();
          TopNode.Id:=TNode(Nodes.Items[I]).Id;
          TopNode.IdParent:=TNode(Nodes.Items[I]).IdParent;
          TopNode.Popis:=TNode(Nodes.Items[I]).Popis;
          TopNode.MenuEdit:=TNode(Nodes.Items[i]);
          Add(TopNode);
          AddSubNodes(Nodes,TopNode);
        end;
    end;
end;

procedure TMenuNodes.AddSubNodes(Nodes: TNodes; ParentMenuNode: TMenuNode);
var I,J : Integer;
    Child : TMenuNode;
    Cnt,Idx : Integer;
begin
  ParentMenuNode.Items.Clear;
  for I:=0 To Nodes.Count-1 do begin
    if (TNode(Nodes.Items[I]).IdParent=ParentMenuNode.Id) and ((TNode(Nodes.Items[I]).Typ=0) or (TNode(Nodes.Items[I]).Typ=9)) then begin
      Cnt:=0;
      Idx:=-1;
      for J:=0 To Nodes.Count-1 do begin
        if (TNode(Nodes.Items[J]).IdParent=TNode(Nodes.Items[I]).Id) then begin
          if (((TNode(Nodes.Items[J]).Typ=0)) or (TNode(Nodes.Items[J]).Typ=9)) then begin
            Inc(Cnt)
          end else begin
            Idx:=J;
          end;
        end;
      end;
      Child:=TMenuNode.Create;
      Child.Id:=TNode(Nodes.Items[I]).Id;
      Child.IdParent:=TNode(Nodes.Items[I]).IdParent;
      Child.Popis:=TNode(Nodes.Items[I]).Popis;
      Child.MenuEdit:=TNode(Nodes.Items[i]);
      ParentMenuNode.Items.Add(Child);
      if Cnt=0 then begin
        if Idx>-1 then begin
          Child.MenuEdit:=TNode(Nodes.Items[Idx]);
        end;
      end else begin
      AddSubNodes(Nodes,Child);
      end;
    end;
  end;
end;

procedure TMenuNodes.FillTreeView(TreeView: TTreeView);
var I : Integer;
    TopNode : TTreeNode;
begin
  TreeView.Items.Clear;
  for I:=0 to Count-1 do begin
    TopNode:=TreeView.Items.Add(TreeView.TopItem,TMenuNode(Items[I]).Popis);
    TopNode.Data:=Items[I];
    FillTreeSubNodes(TopNode,TMenuNode(Items[I]));
  end;
end;

procedure TMenuNodes.FillTntTreeView(TreeView: TTntTreeView);
var I : Integer;
    TopNode : TTntTreeNode;
begin
  TreeView.Items.Clear;
  for I:=0 to Count-1 do begin
    TopNode:=TreeView.Items.Add(TreeView.TopItem,TMenuNode(Items[I]).Popis);
    TopNode.Data:=Items[I];
    FillTntTreeSubNodes(TopNode,TMenuNode(Items[I]));
  end;
end;

procedure TMenuNodes.FillTreeSubNodes(ParentNode: TTreeNode;
  ParentMenuNode: TMenuNode);
var I : Integer;
    TreeChild : TTreeNode;
begin
  for I:=0 to ParentMenuNode.Items.Count-1 do begin
    TreeChild:=ParentNode.Owner.AddChild(ParentNode,TMenuNode(ParentMenuNode.Items[I]).Popis);
    TreeChild.Data:=ParentMenuNode.Items[I];
    FillTreeSubNodes(TreeChild,TMenuNode(ParentMenuNode.Items[I]));
  end;
end;

procedure TMenuNodes.FillTntTreeSubNodes(ParentNode : TTntTreeNode; ParentMenuNode : TMenuNode);
var I : Integer;
    TreeChild : TTntTreeNode;
begin
  for I:=0 to ParentMenuNode.Items.Count-1 do begin
    TreeChild:=ParentNode.Owner.AddChild(ParentNode,TMenuNode(ParentMenuNode.Items[I]).Popis);
    TreeChild.Data:=ParentMenuNode.Items[I];
    FillTntTreeSubNodes(TreeChild,TMenuNode(ParentMenuNode.Items[I]));
  end;
end;

end.
