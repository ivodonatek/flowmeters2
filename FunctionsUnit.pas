unit FunctionsUnit;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, comctrls,
  shlobj, FileCtrl, menus, Constants;

type
  // Language 0 = EN, 1 = CZ, 2 = DE, 3 = SP, 4 = FR, 5 = RU, 6 = AR
  TLanguage = (langEnglish, langCzech, langGerman, langSpanish, langFrench, langRussian, langArabic);

  function GetUserAppDataPath():string;
  function GetLanguageSection(language: TLanguage): string;
  procedure SetControlLayoutByLanguage(control: TControl; lang: TLanguage);
  procedure SetMainMenuLayoutByLanguage(menu: TMainMenu; lang: TLanguage);
  procedure SetAppLayoutByLanguage(app: TApplication; lang: TLanguage);
  function TransformWideChars(str: string; pConvertChars: PConvertWideChars): WideString;

implementation

const
  APP_GROUP_NAME = 'Arkon Flow Systems';
  APP_NAME = 'Flowmeters';
  LangSections: array[TLanguage] of string = ('EN', 'CZ', 'DE', 'SP', 'FR', 'RU', 'AR');

function GetUserAppDataPath():string;
var
	pidl : PItemIDList;
	InFolder   : string;
begin
    SHGetSpecialFolderLocation(Application.Handle, CSIDL_APPDATA, pidl);
    SetLength(InFolder, MAX_PATH);
    SHGetPathFromIDList(PIDL, PChar(InFolder));
    SetLength(InFolder, StrLen(PChar(InFolder)));
    result := ExcludeTrailingBackslash( InFolder ) + '\' + APP_GROUP_NAME + '\' + APP_NAME + '\';
end;

function GetLanguageSection(language: TLanguage): string;
begin
  Result:= LangSections[language];
end;

procedure SetControlLayoutByLanguage(control: TControl; lang: TLanguage);
begin
  if lang = langArabic then begin
    control.BiDiMode := bdRightToLeft;
  end else begin
    control.BiDiMode := bdLeftToRight;
  end;
end;

procedure SetMainMenuLayoutByLanguage(menu: TMainMenu; lang: TLanguage);
begin
  if lang = langArabic then begin
    menu.BiDiMode := bdRightToLeft;
  end else begin
    menu.BiDiMode := bdLeftToRight;
  end;
end;

procedure SetAppLayoutByLanguage(app: TApplication; lang: TLanguage);
begin
  if lang = langArabic then begin
    app.BiDiMode := bdRightToLeft;
  end else begin
    app.BiDiMode := bdLeftToRight;
  end;
end;

function TransformWideChars(str: string; pConvertChars: PConvertWideChars): WideString;
var
  I,J : Integer;
  ChOrd : Longint;
begin
  Result := WideString(str);
  if pConvertChars <> nil then begin
    for I := 1 to Length(str) do begin
      ChOrd := Ord(str[I]);
      for J := Low(pConvertChars^) to High(pConvertChars^) do begin
        if ChOrd = pConvertChars^[J].B then begin
          Result[I] := WideChar(pConvertChars^[J].From);
          Break;
        end;
      end;
    end;
  end;
end;

end.

