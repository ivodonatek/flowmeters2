unit AgrimagP2StatRpt;

interface

uses Windows, SysUtils, Messages, Classes, Graphics, Controls,
  StdCtrls, ExtCtrls, Forms, Quickrpt, QRCtrls, AgrimagP2MyCommunicationClass;

type
  TqrAgrimagP2Stat = class(TQuickRep)
    DetailBand1: TQRBand;
    TitleBand1: TQRBand;
    qrlTitle: TQRLabel;
    ColumnHeaderBand1: TQRBand;
    qrlDateTitle: TQRLabel;
    qrlTimeTitle: TQRLabel;
    qrlTotalTitle: TQRLabel;
    qrlDate: TQRLabel;
    qrlTime: TQRLabel;
    qrlTotal: TQRLabel;
    procedure QuickRepNeedData(Sender: TObject; var MoreData: Boolean);
    procedure QuickRepBeforePrint(Sender: TCustomQuickRep;
      var PrintReport: Boolean);
  private
    FIndex: Integer;
    FDataB1ListRef: TDataB1List;
  public
    constructor CreateRep(AOwner : TComponent; ADataB1ListRef: TDataB1List; ADemo: Boolean);
    procedure TranslateReport;
  end;

implementation

uses AgrimagP2FunctionsUnit, AgrimagP2DataClass, AgrimagP2TranslationUnit;

{$R *.DFM}

procedure TqrAgrimagP2Stat.QuickRepNeedData(Sender: TObject;
  var MoreData: Boolean);
begin
  if (FIndex < FDataB1ListRef.Count) then begin
    qrlDate.Caption := FormatStatDate(FDataB1ListRef[FIndex].DateTime);
    qrlTime.Caption := FormatStatTime(FDataB1ListRef[FIndex].DateTime);
    qrlTotal.Caption := Format('%.6f', [FDataB1ListRef[FIndex].Total]);
  end;
  MoreData := (FIndex < FDataB1ListRef.Count);
  Inc(FIndex);
end;

procedure TqrAgrimagP2Stat.QuickRepBeforePrint(Sender: TCustomQuickRep;
  var PrintReport: Boolean);
begin
  FIndex := 0;
end;

constructor TqrAgrimagP2Stat.CreateRep(AOwner: TComponent; ADataB1ListRef: TDataB1List; ADemo: Boolean);
begin
  inherited Create(AOwner);
  TranslateReport;
  FDataB1ListRef := ADataB1ListRef;
  if ADemo then begin
    qrlTitle.Caption := qrlTitle.Caption + ' (DEMO)';
  end;
end;

procedure TqrAgrimagP2Stat.TranslateReport;
begin
  qrlTitle.Caption := GetTranslationText('DATALOGGER', qrlTitle.Caption);
  qrlDateTitle.Caption := GetTranslationText('DATE', qrlDateTitle.Caption);
  qrlTimeTitle.Caption := GetTranslationText('TIME', qrlTimeTitle.Caption);
  qrlTotalTitle.Caption := GetTranslationText('STAT_TOTAL', qrlTotalTitle.Caption);
end;

end.
