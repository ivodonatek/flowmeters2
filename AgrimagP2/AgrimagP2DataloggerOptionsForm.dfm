object formAgrimagP2DataloggerOptions: TformAgrimagP2DataloggerOptions
  Left = 231
  Top = 131
  BorderIcons = []
  BorderStyle = bsDialog
  Caption = 'Options'
  ClientHeight = 231
  ClientWidth = 568
  Color = 12555391
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Verdana'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 106
  TextHeight = 16
  object GroupBox1: TGroupBox
    Left = 9
    Top = 9
    Width = 550
    Height = 161
    Caption = 'Record Filter Setting'
    TabOrder = 0
    object Panel1: TPanel
      Left = 9
      Top = 18
      Width = 532
      Height = 134
      BevelOuter = bvLowered
      Color = 14670037
      TabOrder = 0
      object chkDateTimeMin: TCheckBox
        Left = 11
        Top = 11
        Width = 204
        Height = 20
        Caption = 'First record time'
        TabOrder = 0
        OnClick = chkDateTimeMinClick
      end
      object chkDateTimeMax: TCheckBox
        Left = 11
        Top = 48
        Width = 204
        Height = 19
        Caption = 'Last record time'
        TabOrder = 1
        OnClick = chkDateTimeMaxClick
      end
      object chkRecordCountMax: TCheckBox
        Left = 11
        Top = 103
        Width = 231
        Height = 19
        Caption = 'Maximum number of records'
        TabOrder = 2
        OnClick = chkRecordCountMaxClick
      end
      object editRecordCountMax: TPBSpinEdit
        Left = 247
        Top = 101
        Width = 138
        Height = 26
        Cursor = crDefault
        MaxValue = 2147483647
        MinValue = 0
        TabOrder = 3
        Value = 0
        Alignment = taLeftJustify
      end
      object dtpDateMin: TDateTimePicker
        Left = 247
        Top = 9
        Width = 138
        Height = 24
        CalAlignment = dtaLeft
        Date = 42491.8347558912
        Time = 42491.8347558912
        DateFormat = dfShort
        DateMode = dmComboBox
        Kind = dtkDate
        ParseInput = False
        ParentShowHint = False
        ShowHint = False
        TabOrder = 4
      end
      object dtpTimeMin: TDateTimePicker
        Left = 384
        Top = 9
        Width = 138
        Height = 24
        CalAlignment = dtaLeft
        Date = 42491.8347558912
        Time = 42491.8347558912
        DateFormat = dfShort
        DateMode = dmComboBox
        Kind = dtkTime
        ParseInput = False
        ParentShowHint = False
        ShowHint = False
        TabOrder = 5
      end
      object dtpDateMax: TDateTimePicker
        Left = 247
        Top = 46
        Width = 138
        Height = 24
        CalAlignment = dtaLeft
        Date = 42491.8347558912
        Time = 42491.8347558912
        DateFormat = dfShort
        DateMode = dmComboBox
        Kind = dtkDate
        ParseInput = False
        ParentShowHint = False
        ShowHint = False
        TabOrder = 6
      end
      object dtpTimeMax: TDateTimePicker
        Left = 384
        Top = 46
        Width = 138
        Height = 24
        CalAlignment = dtaLeft
        Date = 42491.8347558912
        Time = 42491.8347558912
        DateFormat = dfShort
        DateMode = dmComboBox
        Kind = dtkTime
        ParseInput = False
        ParentShowHint = False
        ShowHint = False
        TabOrder = 7
      end
    end
  end
  object btnConfirm: TButton
    Left = 183
    Top = 192
    Width = 83
    Height = 29
    Caption = 'OK'
    Default = True
    TabOrder = 1
    OnClick = btnConfirmClick
  end
  object btnCancel: TButton
    Left = 320
    Top = 192
    Width = 83
    Height = 29
    Cancel = True
    Caption = 'Cancel'
    TabOrder = 2
    OnClick = btnCancelClick
  end
end
