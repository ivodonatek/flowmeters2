inherited frameAgrimagP2ValueEdit: TframeAgrimagP2ValueEdit
  inherited imgPozadi: TImage
    Width = 416
    Height = 273
  end
  object Label3: TLabel
    Left = 144
    Top = 176
    Width = 33
    Height = 13
    Caption = 'Status:'
    Transparent = True
  end
  object Panel1: TPanel
    Left = 24
    Top = 32
    Width = 297
    Height = 129
    BevelOuter = bvLowered
    Color = 14670037
    TabOrder = 0
    object spbDown1: TSpeedButton
      Tag = -1
      Left = 16
      Top = 92
      Width = 33
      Height = 25
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      Glyph.Data = {
        7A000000424D7A000000000000003E000000280000001B0000000F0000000100
        0100000000003C000000C40E0000C40E0000020000000000000000000000FFFF
        FF00FFFFFFE0FFFBFFE0FFF1FFE0FFE0FFE0FFC07FE0FF803FE0FF001FE0FE00
        0FE0FC0007E0F80003E0F00001E0E00000E0C000006080000020FFFFFFE0}
      Margin = 1
      ParentFont = False
      Spacing = 1
      OnClick = spbUp1Click
    end
    object spbDown2: TSpeedButton
      Tag = -2
      Left = 48
      Top = 92
      Width = 33
      Height = 25
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      Glyph.Data = {
        7A000000424D7A000000000000003E000000280000001B0000000F0000000100
        0100000000003C000000C40E0000C40E0000020000000000000000000000FFFF
        FF00FFFFFFE0FFFBFFE0FFF1FFE0FFE0FFE0FFC07FE0FF803FE0FF001FE0FE00
        0FE0FC0007E0F80003E0F00001E0E00000E0C000006080000020FFFFFFE0}
      Margin = 1
      ParentFont = False
      Spacing = 1
      OnClick = spbUp1Click
    end
    object spbDown3: TSpeedButton
      Tag = -3
      Left = 80
      Top = 92
      Width = 33
      Height = 25
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      Glyph.Data = {
        7A000000424D7A000000000000003E000000280000001B0000000F0000000100
        0100000000003C000000C40E0000C40E0000020000000000000000000000FFFF
        FF00FFFFFFE0FFFBFFE0FFF1FFE0FFE0FFE0FFC07FE0FF803FE0FF001FE0FE00
        0FE0FC0007E0F80003E0F00001E0E00000E0C000006080000020FFFFFFE0}
      Margin = 1
      ParentFont = False
      Spacing = 1
      OnClick = spbUp1Click
    end
    object spbDown4: TSpeedButton
      Tag = -4
      Left = 112
      Top = 92
      Width = 33
      Height = 25
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      Glyph.Data = {
        7A000000424D7A000000000000003E000000280000001B0000000F0000000100
        0100000000003C000000C40E0000C40E0000020000000000000000000000FFFF
        FF00FFFFFFE0FFFBFFE0FFF1FFE0FFE0FFE0FFC07FE0FF803FE0FF001FE0FE00
        0FE0FC0007E0F80003E0F00001E0E00000E0C000006080000020FFFFFFE0}
      Margin = 1
      ParentFont = False
      Spacing = 1
      OnClick = spbUp1Click
    end
    object spbDown5: TSpeedButton
      Tag = -5
      Left = 144
      Top = 92
      Width = 33
      Height = 25
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      Glyph.Data = {
        7A000000424D7A000000000000003E000000280000001B0000000F0000000100
        0100000000003C000000C40E0000C40E0000020000000000000000000000FFFF
        FF00FFFFFFE0FFFBFFE0FFF1FFE0FFE0FFE0FFC07FE0FF803FE0FF001FE0FE00
        0FE0FC0007E0F80003E0F00001E0E00000E0C000006080000020FFFFFFE0}
      Margin = 1
      ParentFont = False
      Spacing = 1
      OnClick = spbUp1Click
    end
    object spbDown6: TSpeedButton
      Tag = -6
      Left = 176
      Top = 92
      Width = 33
      Height = 25
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      Glyph.Data = {
        7A000000424D7A000000000000003E000000280000001B0000000F0000000100
        0100000000003C000000C40E0000C40E0000020000000000000000000000FFFF
        FF00FFFFFFE0FFFBFFE0FFF1FFE0FFE0FFE0FFC07FE0FF803FE0FF001FE0FE00
        0FE0FC0007E0F80003E0F00001E0E00000E0C000006080000020FFFFFFE0}
      Margin = 1
      ParentFont = False
      Spacing = 1
      OnClick = spbUp1Click
    end
    object spbDown7: TSpeedButton
      Tag = -7
      Left = 208
      Top = 92
      Width = 33
      Height = 25
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      Glyph.Data = {
        7A000000424D7A000000000000003E000000280000001B0000000F0000000100
        0100000000003C000000C40E0000C40E0000020000000000000000000000FFFF
        FF00FFFFFFE0FFFBFFE0FFF1FFE0FFE0FFE0FFC07FE0FF803FE0FF001FE0FE00
        0FE0FC0007E0F80003E0F00001E0E00000E0C000006080000020FFFFFFE0}
      Margin = 1
      ParentFont = False
      Spacing = 1
      OnClick = spbUp1Click
    end
    object spbDown8: TSpeedButton
      Tag = -8
      Left = 240
      Top = 92
      Width = 33
      Height = 25
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      Glyph.Data = {
        7A000000424D7A000000000000003E000000280000001B0000000F0000000100
        0100000000003C000000C40E0000C40E0000020000000000000000000000FFFF
        FF00FFFFFFE0FFFBFFE0FFF1FFE0FFE0FFE0FFC07FE0FF803FE0FF001FE0FE00
        0FE0FC0007E0F80003E0F00001E0E00000E0C000006080000020FFFFFFE0}
      Margin = 1
      ParentFont = False
      Spacing = 1
      OnClick = spbUp1Click
    end
    object spbUp1: TSpeedButton
      Tag = 1
      Left = 16
      Top = 15
      Width = 33
      Height = 25
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      Glyph.Data = {
        7A000000424D7A000000000000003E000000280000001B0000000F0000000100
        0100000000003C000000C40E0000C40E0000020000000000000000000000FFFF
        FF00FFFFFFE080000020C0000060E00000E0F00001E0F80003E0FC0007E0FE00
        0FE0FF001FE0FF803FE0FFC07FE0FFE0FFE0FFF1FFE0FFFBFFE0FFFFFFE0}
      Margin = 1
      ParentFont = False
      Spacing = 1
      OnClick = spbUp1Click
    end
    object spbUp2: TSpeedButton
      Tag = 2
      Left = 48
      Top = 15
      Width = 33
      Height = 25
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      Glyph.Data = {
        7A000000424D7A000000000000003E000000280000001B0000000F0000000100
        0100000000003C000000C40E0000C40E0000020000000000000000000000FFFF
        FF00FFFFFFE080000020C0000060E00000E0F00001E0F80003E0FC0007E0FE00
        0FE0FF001FE0FF803FE0FFC07FE0FFE0FFE0FFF1FFE0FFFBFFE0FFFFFFE0}
      Margin = 1
      ParentFont = False
      Spacing = 1
      OnClick = spbUp1Click
    end
    object spbUp3: TSpeedButton
      Tag = 3
      Left = 80
      Top = 15
      Width = 33
      Height = 25
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      Glyph.Data = {
        7A000000424D7A000000000000003E000000280000001B0000000F0000000100
        0100000000003C000000C40E0000C40E0000020000000000000000000000FFFF
        FF00FFFFFFE080000020C0000060E00000E0F00001E0F80003E0FC0007E0FE00
        0FE0FF001FE0FF803FE0FFC07FE0FFE0FFE0FFF1FFE0FFFBFFE0FFFFFFE0}
      Margin = 1
      ParentFont = False
      Spacing = 1
      OnClick = spbUp1Click
    end
    object spbUp4: TSpeedButton
      Tag = 4
      Left = 112
      Top = 15
      Width = 33
      Height = 25
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      Glyph.Data = {
        7A000000424D7A000000000000003E000000280000001B0000000F0000000100
        0100000000003C000000C40E0000C40E0000020000000000000000000000FFFF
        FF00FFFFFFE080000020C0000060E00000E0F00001E0F80003E0FC0007E0FE00
        0FE0FF001FE0FF803FE0FFC07FE0FFE0FFE0FFF1FFE0FFFBFFE0FFFFFFE0}
      Margin = 1
      ParentFont = False
      Spacing = 1
      OnClick = spbUp1Click
    end
    object spbUp5: TSpeedButton
      Tag = 5
      Left = 144
      Top = 15
      Width = 33
      Height = 25
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      Glyph.Data = {
        7A000000424D7A000000000000003E000000280000001B0000000F0000000100
        0100000000003C000000C40E0000C40E0000020000000000000000000000FFFF
        FF00FFFFFFE080000020C0000060E00000E0F00001E0F80003E0FC0007E0FE00
        0FE0FF001FE0FF803FE0FFC07FE0FFE0FFE0FFF1FFE0FFFBFFE0FFFFFFE0}
      Margin = 1
      ParentFont = False
      Spacing = 1
      OnClick = spbUp1Click
    end
    object spbUp6: TSpeedButton
      Tag = 6
      Left = 176
      Top = 15
      Width = 33
      Height = 25
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      Glyph.Data = {
        7A000000424D7A000000000000003E000000280000001B0000000F0000000100
        0100000000003C000000C40E0000C40E0000020000000000000000000000FFFF
        FF00FFFFFFE080000020C0000060E00000E0F00001E0F80003E0FC0007E0FE00
        0FE0FF001FE0FF803FE0FFC07FE0FFE0FFE0FFF1FFE0FFFBFFE0FFFFFFE0}
      Margin = 1
      ParentFont = False
      Spacing = 1
      OnClick = spbUp1Click
    end
    object spbUp7: TSpeedButton
      Tag = 7
      Left = 208
      Top = 15
      Width = 33
      Height = 25
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      Glyph.Data = {
        7A000000424D7A000000000000003E000000280000001B0000000F0000000100
        0100000000003C000000C40E0000C40E0000020000000000000000000000FFFF
        FF00FFFFFFE080000020C0000060E00000E0F00001E0F80003E0FC0007E0FE00
        0FE0FF001FE0FF803FE0FFC07FE0FFE0FFE0FFF1FFE0FFFBFFE0FFFFFFE0}
      Margin = 1
      ParentFont = False
      Spacing = 1
      OnClick = spbUp1Click
    end
    object spbUp8: TSpeedButton
      Tag = 8
      Left = 240
      Top = 15
      Width = 33
      Height = 25
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      Glyph.Data = {
        7A000000424D7A000000000000003E000000280000001B0000000F0000000100
        0100000000003C000000C40E0000C40E0000020000000000000000000000FFFF
        FF00FFFFFFE080000020C0000060E00000E0F00001E0F80003E0FC0007E0FE00
        0FE0FF001FE0FF803FE0FFC07FE0FFE0FFE0FFF1FFE0FFFBFFE0FFFFFFE0}
      Margin = 1
      ParentFont = False
      Spacing = 1
      OnClick = spbUp1Click
    end
    object lblValue1: TLabel
      Tag = 101
      Left = 22
      Top = 44
      Width = 18
      Height = 37
      Caption = '0'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -33
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object lblValue2: TLabel
      Tag = 102
      Left = 54
      Top = 44
      Width = 18
      Height = 37
      Caption = '0'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -33
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object lblValue3: TLabel
      Tag = 103
      Left = 86
      Top = 44
      Width = 18
      Height = 37
      Caption = '0'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -33
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object lblValue4: TLabel
      Tag = 104
      Left = 118
      Top = 44
      Width = 18
      Height = 37
      Caption = '0'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -33
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object lblValue5: TLabel
      Tag = 105
      Left = 150
      Top = 44
      Width = 18
      Height = 37
      Caption = '0'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -33
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object lblValue6: TLabel
      Tag = 106
      Left = 182
      Top = 44
      Width = 18
      Height = 37
      Caption = '0'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -33
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object lblValue7: TLabel
      Tag = 107
      Left = 214
      Top = 44
      Width = 18
      Height = 37
      Caption = '0'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -33
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object lblValue8: TLabel
      Tag = 108
      Left = 246
      Top = 44
      Width = 18
      Height = 37
      Caption = '0'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -33
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object lblCarka: TLabel
      Left = 75
      Top = 44
      Width = 9
      Height = 37
      Caption = '.'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -33
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
  end
  object Panel2: TPanel
    Left = 24
    Top = 192
    Width = 113
    Height = 41
    BevelOuter = bvLowered
    Color = 14670037
    TabOrder = 1
    object Label1: TLabel
      Left = 8
      Top = 8
      Width = 50
      Height = 13
      Caption = 'Min Value:'
    end
    object Label2: TLabel
      Left = 8
      Top = 24
      Width = 53
      Height = 13
      Caption = 'Max Value:'
    end
    object lblMin: TLabel
      Left = 64
      Top = 9
      Width = 6
      Height = 13
      Caption = '0'
    end
    object lblMax: TLabel
      Left = 64
      Top = 25
      Width = 6
      Height = 13
      Caption = '0'
    end
  end
  object Panel3: TPanel
    Left = 144
    Top = 192
    Width = 177
    Height = 41
    BevelOuter = bvLowered
    Color = 14670037
    TabOrder = 2
    object lblStatus: TLabel
      Left = 16
      Top = 13
      Width = 46
      Height = 13
      Caption = 'Unknown'
    end
  end
  object btnWrite: TButton
    Left = 136
    Top = 248
    Width = 73
    Height = 25
    Caption = 'Write'
    TabOrder = 3
    OnClick = btnWriteClick
  end
end
