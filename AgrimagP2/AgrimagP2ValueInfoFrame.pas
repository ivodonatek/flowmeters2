unit AgrimagP2ValueInfoFrame;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, StdCtrls,
  AgrimagP2MyCommunicationClass, NodeClass, ExtCtrls, AgrimagP2FunctionsUnit,
  AgrimagP2ValueHeadFrame;

type
  TframeAgrimagP2ValueInfo = class(TframeAgrimagP2Head)
    Panel1: TPanel;
    lblValue: TLabel;
  private
    { Private declarations }
  protected
    function ConvertUnits: Boolean; override;
  public
    { Public declarations }
    constructor CreateFrame(AOwner: TComponent; MyCommunication : TMyCommunication ; EditNode: TNode); override;
    function GetValueText(Value:Integer): String;
  end;

var
  frameAgrimagP2ValueInfo: TframeAgrimagP2ValueInfo;

implementation

uses AgrimagP2GlobalUtilsClass, AgrimagP2TranslationUnit;

{$R *.DFM}

function TframeAgrimagP2ValueInfo.ConvertUnits: Boolean;
begin
  Result := True;
end;

constructor TframeAgrimagP2ValueInfo.CreateFrame(AOwner: TComponent; MyCommunication: TMyCommunication; EditNode: TNode);
var ReadOK:Boolean;
  FourByte:T4Byte;
  LastCursor : TCursor;
  Value:real;
  Decimal, i:integer;
begin
  inherited;
  LastCursor := Screen.Cursor;
  try
    if Assigned(WaitFrm) then
    	WaitFrm.Show;
    Screen.Cursor:=crHourGlass;
    repeat
      Application.ProcessMessages;
      ReadOK:=ReadValue(FEditNode.ModbusAdr-1,FourByte);
    until(ReadOK)or(MessageDlg(GetTranslationText('READ_ERROR', STR_READ_ERROR),mtError, [mbRetry, mbAbort], 0) = mrAbort);
    if ReadOK then
    begin
      case FEditNode.Units of
      1..5: Value:=Flow_Conversion( integer(FourByte), FlowUnit);
      6..8: Value:=Volume_Conversion( integer(FourByte), VolumeUnit);
      else
        Value:= integer(FourByte);
//      	lblValue.Caption:=GetValueText(FourByte);
      end;
      Decimal:=1;
      for I:=1 to FEditNode.Decimal do
      	Decimal:=Decimal*10;
      lblValue.Caption:=Format('%.*f',[FEditNode.Decimal,Value/Decimal]);
//      lblValue.Caption:=Format('%.*f',[FEditNode.Digits,Value/Power(10,FEditNode.Decimal)]);

    end
    else
      lblValue.Caption:='Read Error';
    if Assigned(WaitFrm) then
    	WaitFrm.gWait.AddProgress(10);
    Application.ProcessMessages;
  finally
    Screen.Cursor:=LastCursor;
    if Assigned(WaitFrm) then
    	WaitFrm.Hide;
  end;
end;


function TframeAgrimagP2ValueInfo.GetValueText(Value:integer): String;
var I : Integer;
    Decimal : Integer;
begin
  Decimal:=1;
  for I:=1 to FEditNode.Decimal do begin
    Decimal:=Decimal*10;
  end;
//  I:=Integer(FourByte);
	I:=Value;
  Result:=Format('%.*f',[FEditNode.Decimal,I/Decimal]);
end;

end.
