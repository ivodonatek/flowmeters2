unit AgrimagP2StatFrm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Gauges, StdCtrls, AgrimagP2MyCommunicationClass, ComCtrls, ExtCtrls, jpeg;

resourcestring
  STR_LOAD_SUCC = 'Loaded succesfully!';
  STR_BLOCK_READ_ERR = '%d block read error occurs!';
  STR_EXPORT_DONE = 'Export done!';

type
  TformAgrimagP2Stat = class(TForm)
    pnlBottom: TPanel;
    progStat: TGauge;
    btnLoadStat: TButton;
    btnExportStat: TButton;
    btnPrintStat: TButton;
    saveDlg: TSaveDialog;
    Image21: TImage;
    pnlTop: TPanel;
    imgMagLogo: TImage;
    pnlClient: TPanel;
    pnlStat: TPanel;
    lvStat: TListView;
    lblDemo: TLabel;
    btnOptionsStat: TButton;
    procedure btnExportStatClick(Sender: TObject);
    procedure btnPrintStatClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure btnLoadStatClick(Sender: TObject);
    procedure btnOptionsStatClick(Sender: TObject);
    procedure btnLoadStatMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure Image21MouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
    FDataloggerFilter: TAgrimagP2DataloggerFilter;
    FLoadStatRun: Boolean;
    FMyCommunicationRef: TMyCommunication;
    FFirmwareNO: Integer;
    FDataB1List: TDataB1List;
    procedure FillViewList;
    procedure ExportToFile(AFileName: String);
    procedure LoadFromIniFile;
    procedure SaveToIniFile;
    procedure LoadStat();
  public
    { Public declarations }
    procedure Init(AMyCommunicationRef: TMyCommunication; AFirmwareNO: Integer);
    procedure TranslateForm;
    property FirmwareNO: Integer read FFirmwareNO;
  end;

//var
//  formAgrimagP2Stat: TformAgrimagP2Stat;

implementation

uses AgrimagP2StatRpt, AgrimagP2FunctionsUnit, AgrimagP2DataClass, IniFiles, AgrimagP2EnterFrm, AgrimagP2DataloggerOptionsForm,
  AgrimagP2TranslationUnit;

{$R *.DFM}

{ TformAgrimagP2Stat }

procedure TformAgrimagP2Stat.Init(AMyCommunicationRef: TMyCommunication; AFirmwareNO: Integer);
begin
  FLoadStatRun := false;
  FMyCommunicationRef := AMyCommunicationRef;
  FFirmwareNO := AFirmwareNO;
  if (FMyCommunicationRef.ProtokolType = ptDemo) then begin
    Caption := Caption + ' (DEMO)';
    lblDemo.Visible := True;
  end;
end;

procedure TformAgrimagP2Stat.LoadStat();
var J, ErrCnt: Integer;
    DataBlockB1 : TDataBlockB1;
begin
  progStat.Progress := 0;
  progStat.MaxValue := 1;
  progStat.Visible := True;
  Screen.Cursor := crHourGlass;

  try
    FDataB1List.Clear;
    ErrCnt := 0;

    if FMyCommunicationRef.CalculateDataloggerFilteredRecords(FLoadStatRun, FDataloggerFilter) then begin
      progStat.MaxValue := FMyCommunicationRef.DataloggerFilteredRecordCount;
      FDataB1List.Capacity := FMyCommunicationRef.DataloggerFilteredRecordCount;
      repeat
        if not FMyCommunicationRef.ReadDataloggerNextFilteredRecordBlock(FDataB1List, FDataloggerFilter) then begin
          Inc(ErrCnt);
        end;
        progStat.Progress := FMyCommunicationRef.DataloggerFilteredRecordReadCount;
        Application.ProcessMessages;
      until (not FLoadStatRun) or (FMyCommunicationRef.DataloggerFilteredRecordReadCount = FMyCommunicationRef.DataloggerFilteredRecordCount) or (not FDataloggerFilter.IsRecordCountValid(FDataB1List.Count));
    end else begin
      Inc(ErrCnt);
    end;
  finally
    Screen.Cursor := crDefault;
    progStat.Visible := False;
  end;

  FillViewList;
  if ErrCnt = 0 then begin
    ShowMessage(GetTranslationText('LOAD_SUCC', STR_LOAD_SUCC));
  end else begin
    ShowMessage(Format(GetTranslationText('BLOCK_READ_ERR', STR_BLOCK_READ_ERR), [ErrCnt]));
  end;
end;

procedure TformAgrimagP2Stat.FillViewList;
var I: Integer;
    NewItem: TListItem;
begin
  progStat.Progress := 0;
  progStat.MaxValue := FDataB1List.Count;
  progStat.Visible := True;
  Screen.Cursor := crHourGlass;
  lvStat.Items.BeginUpdate;
  try
    lvStat.Items.Clear;
    lvStat.AllocBy := FDataB1List.Count;
    for I := 0 to FDataB1List.Count - 1 do begin
      NewItem := lvStat.Items.Add;
      NewItem.Caption := FormatStatDate(FDataB1List[I].DateTime);
      NewItem.SubItems.Add(FormatStatTime(FDataB1List[I].DateTime));
      NewItem.SubItems.Add(Format('%.6f', [FDataB1List[I].Total]));
      progStat.Progress := I + 1;
      Application.ProcessMessages;
    end;
  finally
    lvStat.Items.EndUpdate;
    Screen.Cursor := crDefault;
    progStat.Visible := False;
  end;
end;

procedure TformAgrimagP2Stat.ExportToFile(AFileName: String);
var I: Integer;
    SL: TStringList;
begin
  progStat.Progress := 0;
  progStat.MaxValue := FDataB1List.Count;
  progStat.Visible := True;
  Screen.Cursor := crHourGlass;
  SL := TStringList.Create;
  try
    SL.Clear;
    SL.Add(Format('%s;%s;%s',
                  [GetTranslationText('DATE', 'Date'),
                   GetTranslationText('TIME', 'Time'),
                   GetTranslationText('STAT_TOTAL', 'Total')]));
    for I := 0 to FDataB1List.Count - 1 do begin
        SL.Add(Format('%s;%s;%.6f',
          [DateToStr(FDataB1List[I].DateTime),
           TimeToStr(FDataB1List[I].DateTime),
           FDataB1List[I].Total]));

      progStat.Progress := I + 1;
      Application.ProcessMessages;
    end;
    SL.SaveToFile(AFileName);
  finally
    SL.Free;
    Screen.Cursor := crDefault;
    progStat.Visible := False;
  end;
end;

procedure TformAgrimagP2Stat.btnExportStatClick(Sender: TObject);
begin
  if saveDlg.Execute then begin
    ExportToFile(saveDlg.FileName);
    ShowMessage(GetTranslationText('EXPORT_DONE', STR_EXPORT_DONE));
  end;
end;

procedure TformAgrimagP2Stat.btnPrintStatClick(Sender: TObject);
var
  qrMagBStat: TqrAgrimagP2Stat;
begin
  qrMagBStat := TqrAgrimagP2Stat.CreateRep(nil, FDataB1List, FMyCommunicationRef.ProtokolType = ptDemo);
  try
    qrMagBStat.PreviewModal;
  finally
    qrMagBStat.Free;
  end;
end;

procedure TformAgrimagP2Stat.FormCreate(Sender: TObject);
begin
  TranslateForm;
  FDataloggerFilter := TAgrimagP2DataloggerFilter.Create;
  FDataB1List := TDataB1List.Create;
  LoadFromIniFile;
  imgMagLogo.Picture.LoadFromFile(IncludeTrailingBackslash(ExtractFilePath(Application.ExeName)) + MAG_LOGO_FILE_NAME);
end;

procedure TformAgrimagP2Stat.FormDestroy(Sender: TObject);
begin
  SaveToIniFile;
  FDataB1List.Free;
  FDataloggerFilter.Free;
  FreeAndNil(FMyCommunicationRef);
end;

procedure TformAgrimagP2Stat.LoadFromIniFile;
var sExePath: String;
    iniF: TIniFile;
begin
  sExePath := GetUserAppDataProductPath();
  iniF := TIniFile.Create(sExePath+'AgrimagP2.ini');
  try
    Self.Width:=iniF.ReadInteger('Statistic','Width',800);
    Self.Height:=iniF.ReadInteger('Statistic','Height',600);
    if(iniF.ReadBool('Statistic','Maximized', false) = true) then begin
      Self.WindowState:=wsMaximized
    end else begin
      Self.WindowState:=wsNormal;
    end;
    FDataloggerFilter.LoadFromIniFile(iniF);
  finally
    iniF.Free;
  end;
end;

procedure TformAgrimagP2Stat.SaveToIniFile;
var sExePath: String;
    iniF: TIniFile;
begin
  sExePath := GetUserAppDataProductPath();
  iniF := TIniFile.Create(sExePath+'AgrimagP2.ini');
  try
    iniF.WriteBool('Statistic','Maximized', Self.WindowState=wsMaximized);
    if Self.WindowState = wsNormal then begin
      iniF.WriteInteger('Statistic','Width',Self.Width);
      iniF.WriteInteger('Statistic','Height',Self.Height);
    end;
    FDataloggerFilter.SaveToIniFile(iniF);
  finally
    iniF.Free;
  end;
end;

procedure TformAgrimagP2Stat.TranslateForm;
begin
  lblDemo.Caption := GetTranslationText('DEMO', lblDemo.Caption);
  lvStat.Columns[0].Caption := GetTranslationText('DATE', lvStat.Columns[0].Caption);
  lvStat.Columns[1].Caption := GetTranslationText('TIME', lvStat.Columns[1].Caption);
  lvStat.Columns[2].Caption := GetTranslationText('STAT_TOTAL', lvStat.Columns[2].Caption);
  btnLoadStat.Caption := GetTranslationText('LOAD', btnLoadStat.Caption);
  btnExportStat.Caption := GetTranslationText('EXPORT', btnExportStat.Caption);
  btnPrintStat.Caption := GetTranslationText('PRINT', btnPrintStat.Caption);
end;

procedure TformAgrimagP2Stat.btnLoadStatClick(Sender: TObject);
begin
  if not FLoadStatRun then begin
    FLoadStatRun := true;
    btnLoadStat.Caption := GetTranslationText('STOP', 'Stop');
    btnExportStat.Enabled := false;
    btnPrintStat.Enabled := false;
    btnOptionsStat.Enabled := false;
    LoadStat();
    btnLoadStat.Caption := GetTranslationText('LOAD', 'Load');
    btnExportStat.Enabled := true;
    btnPrintStat.Enabled := true;
    btnOptionsStat.Enabled := true;
  end;

  FLoadStatRun := false;
end;

procedure TformAgrimagP2Stat.btnOptionsStatClick(Sender: TObject);
var OptionsForm: TformAgrimagP2DataloggerOptions;
begin
  OptionsForm := TformAgrimagP2DataloggerOptions.CreateFrm(self, FDataloggerFilter);
  SetCenter(OptionsForm);
  OptionsForm.ShowModal;
end;

procedure TformAgrimagP2Stat.btnLoadStatMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
  Screen.Cursor := crDefault;
end;

procedure TformAgrimagP2Stat.Image21MouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
  if FLoadStatRun then
    Screen.Cursor := crHourGlass
  else
    Screen.Cursor := crDefault;
end;

procedure TformAgrimagP2Stat.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := caFree;
end;

end.
