unit AgrimagP2ValueHeadFrame;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, StdCtrls,
  AgrimagP2MyCommunicationClass, NodeClass, ExtCtrls, WaitFrm, AgrimagP2FunctionsUnit;

type
  TframeAgrimagP2Head = class(Tframe)
    lblTitle: TLabel;
    imgPozadi: TImage;
  public
    { Private declarations }
    constructor CreateFrame(AOwner: TComponent; MyCommunication : TMyCommunication;
    EditNode: TNode); virtual;
    destructor Destroy(); override;
    function ReadValue(Address:Integer; var RetFourByte : T4Byte): Boolean;
    function WriteValue(Address:Integer; WriteFourByte: T4Byte): Boolean;
  protected
    FMyCommunication : TMyCommunication;
    FEditNode : TNode;
    WaitFrm : TformWait;

    FlowUnit : TFlowUnit;
    VolumeUnit: TVolumeUnit;
    TempUnit : TTempUnit;
    function ConvertUnits: Boolean; virtual;
  private
  end;

const
  ADDRESS_UNIT_FLOW = 1500;
  ADDRESS_UNIT_VOLUME = 1502;
  ADDRESS_UNIT_TEMPERATURE = 1504;
  BACKGROUND_FILE_NAME = 'AgrimagP2\Background.jpg';

var
  frameAgrimagP2Head: TframeAgrimagP2Head;
implementation

uses AgrimagP2MenuFrm, AgrimagP2GlobalUtilsClass, AgrimagP2TranslationUnit;

{$R *.DFM}

{ TframeHead }

function TframeAgrimagP2Head.ConvertUnits: Boolean;
begin
  Result := False;
end;

constructor TframeAgrimagP2Head.CreateFrame(AOwner: TComponent;MyCommunication: TMyCommunication; EditNode: TNode);
var FourByte : T4Byte;
	ReadOK : boolean;
    LastCursor : TCursor;
begin
  inherited Create(AOwner);
  imgPozadi.Picture.LoadFromFile(IncludeTrailingBackslash(ExtractFilePath(Application.ExeName)) + BACKGROUND_FILE_NAME);
  FEditNode := EditNode;
  FMyCommunication:=MyCommunication;
  WaitFrm:=TformWait.Create(Self);

//cti jednotky
  LastCursor := Screen.Cursor;
  try
    if Assigned(WaitFrm) then WaitFrm.Show;
    Screen.Cursor := crHourGlass;

    //cti pouze kdyz je treba
    if FEditNode.Units in [1..8] then begin
        //Flow unit
        repeat
          Application.ProcessMessages;
          ReadOK:=FMyCommunication.ReadMenuValue(ADDRESS_UNIT_FLOW-1, FourByte);
        until(ReadOK)or(MessageDlg(GetTranslationText('READ_ERROR', STR_READ_ERROR),mtError, [mbRetry, mbAbort], 0) = mrAbort);

        if Assigned(WaitFrm) then WaitFrm.gWait.AddProgress(10);

        if (ReadOK)and(integer(FourByte)>=0)and(integer(FourByte)<= Length(Jednotky)) then
          FlowUnit := TFlowUnit(integer(FourByte))
        else
          raise Exception.Create(GetTranslationText('READ_ERROR', STR_READ_ERROR));
        //VolumeUnit
        repeat
          Application.ProcessMessages;
          ReadOK:=FMyCommunication.ReadMenuValue(ADDRESS_UNIT_VOLUME-1, FourByte);
        until(ReadOK)or(MessageDlg(GetTranslationText('READ_ERROR', STR_READ_ERROR),mtError, [mbRetry, mbAbort], 0) = mrAbort);

        if Assigned(WaitFrm) then WaitFrm.gWait.AddProgress(10);

        if (ReadOK)and(integer(FourByte)>=0)and(integer(FourByte)<= Length(Jednotky)) then
          VolumeUnit := TVolumeUnit(integer(FourByte))
        else
          raise Exception.Create(GetTranslationText('READ_ERROR', STR_READ_ERROR));
    end;
    if (ConvertUnits) and (FEditNode.Units in [1..8]) then begin
      case FEditNode.Units of
        //'UKG/min','USG/min','m3/h','l/min'
        1..5: lblTitle.Caption:=Format('%s [%s]', [FEditNode.Popis, FlowUnitList[Integer(FlowUnit)]]);
        //'UKG','USG','m3'
        6..8: lblTitle.Caption:=Format('%s [%s]', [FEditNode.Popis, VolumeUnitList[Integer(VolumeUnit)]]);
      end;
    end else begin
      if (FEditNode.Units >=0)and(FEditNode.Units<= Length(Jednotky)) then begin
        lblTitle.Caption:=Format('%s [%s]', [FEditNode.Popis, Jednotky[FEditNode.Units]]);
      end else begin
        lblTitle.Caption:=Format('%s [Unknown]', [FEditNode.Popis]);
      end;
    end;
  finally
    if Assigned(WaitFrm) then WaitFrm.Hide();
    Screen.Cursor := LastCursor;
  end;

end;

destructor TframeAgrimagP2Head.Destroy();
begin
	FreeAndNil(WaitFrm);
    inherited;
end;

function TframeAgrimagP2Head.ReadValue(Address:Integer;var RetFourByte : T4Byte): Boolean;
begin
    case FMyCommunication.ProtokolType of
      ptMagX1: raise Exception.Create('Unsuported protocol');
    else
      Result := FMyCommunication.ReadMenuValue(Address,RetFourByte);
    end;
end;

function TframeAgrimagP2Head.WriteValue(Address: Integer;WriteFourByte: T4Byte): Boolean;
begin
  case FMyCommunication.ProtokolType of
    ptMagX1: raise Exception.Create('Unsuported communications protocol');
  else
    Result := FMyCommunication.WriteMenuValue(Address ,WriteFourByte);
  end;
end;

end.
