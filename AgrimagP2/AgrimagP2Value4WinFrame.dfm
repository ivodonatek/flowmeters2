inherited frameAgrimagP2Value4Win: TframeAgrimagP2Value4Win
  AutoSize = True
  object GroupBox1: TGroupBox
    Left = 136
    Top = 56
    Width = 345
    Height = 41
    Caption = 'GroupBox1'
    Color = 14670037
    ParentColor = False
    TabOrder = 0
    object Label2: TLabel
      Left = 16
      Top = 16
      Width = 50
      Height = 13
      Caption = 'Min Value:'
    end
    object lblMin1: TLabel
      Left = 96
      Top = 17
      Width = 6
      Height = 13
      Caption = '0'
    end
    object Label3: TLabel
      Left = 200
      Top = 16
      Width = 53
      Height = 13
      Caption = 'Max Value:'
    end
    object lblMax1: TLabel
      Left = 280
      Top = 17
      Width = 6
      Height = 13
      Caption = '0'
    end
  end
  object Panel4: TPanel
    Left = 8
    Top = 112
    Width = 289
    Height = 112
    BevelOuter = bvLowered
    Color = 14670037
    TabOrder = 1
  end
  object Panel5: TPanel
    Left = 304
    Top = 112
    Width = 289
    Height = 112
    BevelOuter = bvLowered
    Color = 14670037
    TabOrder = 2
  end
  object GroupBox2: TGroupBox
    Left = 136
    Top = 240
    Width = 345
    Height = 41
    Caption = 'GroupBox2'
    Color = 14670037
    ParentColor = False
    TabOrder = 3
    object Label4: TLabel
      Left = 16
      Top = 16
      Width = 50
      Height = 13
      Caption = 'Min Value:'
    end
    object lblMin2: TLabel
      Left = 96
      Top = 16
      Width = 6
      Height = 13
      Caption = '0'
    end
    object Label5: TLabel
      Left = 196
      Top = 16
      Width = 53
      Height = 13
      Caption = 'Max Value:'
    end
    object lblMax2: TLabel
      Left = 283
      Top = 16
      Width = 6
      Height = 13
      Caption = '0'
    end
  end
  object Panel6: TPanel
    Left = 16
    Top = 296
    Width = 289
    Height = 112
    BevelOuter = bvLowered
    Color = 14670037
    TabOrder = 4
  end
  object Panel7: TPanel
    Left = 312
    Top = 296
    Width = 289
    Height = 112
    BevelOuter = bvLowered
    Color = 14670037
    TabOrder = 5
  end
  object btnWrite1: TButton
    Left = 272
    Top = 424
    Width = 73
    Height = 25
    Caption = 'Write all'
    TabOrder = 6
    OnClick = btnWrite1Click
  end
end
