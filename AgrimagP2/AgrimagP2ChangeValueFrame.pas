unit AgrimagP2ChangeValueFrame;

interface

uses 
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, math, AgrimagP2MyCommunicationClass;

const CarkaPozice:array[1..8] of word=(43,75,107,139,171,203,235,3000);

type
  TframeAgrimagP2ValueChange = class(TFrame)
    Panel2: TPanel;
    Image7: TImage;
    Image6: TImage;
    spbDown1: TSpeedButton;
    spbDown2: TSpeedButton;
    spbDown3: TSpeedButton;
    spbDown4: TSpeedButton;
    spbDown5: TSpeedButton;
    spbDown6: TSpeedButton;
    spbDown7: TSpeedButton;
    spbUp1: TSpeedButton;
    spbUp2: TSpeedButton;
    spbUp3: TSpeedButton;
    spbUp4: TSpeedButton;
    spbUp5: TSpeedButton;
    spbUp6: TSpeedButton;
    spbUp7: TSpeedButton;
    lblValue1: TLabel;
    lblValue2: TLabel;
    lblValue3: TLabel;
    lblValue4: TLabel;
    lblValue5: TLabel;
    lblValue6: TLabel;
    lblValue7: TLabel;
    lblCarka: TLabel;
    spbUp8: TSpeedButton;
    spbDown8: TSpeedButton;
    lblValue8: TLabel;
    Procedure IncValue(Sender: TObject);
    procedure FillLabels();
    procedure spbUp1Click(Sender: TObject);
    Constructor CreateFrame(x,y:integer);
    procedure FillArray(F:integer);
    function ArrayToInt():integer;
  private
    { Private declarations }
  ValueArray : array[1..8] of word;
  decimal, digit:integer;
  public
    { Public declarations }
  end;

var
  frameAgrimagP2ValueChange:TframeAgrimagP2ValueChange;

  CalibrationValue: integer;
  CalibrationValue4B: T4Byte;
  MyCommPort: TMyCommPort;
implementation

{$R *.DFM}

Constructor TframeAgrimagP2ValueChange.CreateFrame(x,y:integer);
begin
  inherited Create(nil);
  lblCarka.left:=CarkaPozice[8-y];
  digit:=x;
  decimal:=y;
end;


function TframeAgrimagP2ValueChange.ArrayToInt():integer;
var i:integer;
begin
  Result:=0;
  for i:=1 to (digit+decimal) do Result:=Result + ValueArray[9-i] * trunc(power(10, i-1));

end;

Procedure TframeAgrimagP2ValueChange.IncValue(Sender: TObject);
var value:iNTEGER;
begin
  if TSpeedButton(Sender).tag > 0 then begin
    value:=TSpeedButton(Sender).tag;
    if ValueArray[value] = 9 then ValueArray[value]:= 0 else inc(ValueArray[value]);
  end else begin
    value:=TSpeedButton(Sender).tag*(-1);
    if ValueArray[value] = 0 then ValueArray[value]:= 9 else ValueArray[value]:=ValueArray[value]-1;
  end;
    FillLabels;
end;

procedure TframeAgrimagP2ValueChange.FillArray(F:integer);
var X: integer;
    K, i: integer;

begin
  X:=length(inttostr(F));
  // y:=Decimal + Digit;
  for i:=1 to 8 do ValueArray[i]:=0;


  for i:= 1 to X do begin
  K:=strtoint(copy(inttostr(F), i, 1));
  ValueArray[8-X+i]:=K;
  end;
  FillLabels;
end;

procedure TframeAgrimagP2ValueChange.FillLabels();
var I, J, K:integer;
begin
  for J:=0 to componentcount-1 do TControl(components[J]).enabled:=true;
  for I:=8 downto 1 do begin
    for J:=0 to componentcount-1 do if components[J].Tag = 100 + I then Tlabel(components[J]).caption:=IntToStr(ValueArray[I]);
    if 8 - (Digit + Decimal) >= I then begin
      for K:=0 to componentcount-1 do begin
        if components[K].Tag = 100 + I then Tlabel(components[K]).enabled:=false;
        if components[K].Tag = - I then TSpeedButton(components[K]).enabled:=false;
        if components[K].Tag =   I then TSpeedButton(components[K]).enabled:=false;
      end;
    end;
  end;
end;






procedure TframeAgrimagP2ValueChange.spbUp1Click(Sender: TObject);
begin
  incValue(Sender);
end;

end.
