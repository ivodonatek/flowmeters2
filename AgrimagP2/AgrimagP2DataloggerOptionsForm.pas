unit AgrimagP2DataloggerOptionsForm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, StdCtrls,
  Buttons, ExtCtrls, AgrimagP2GlobalUtilsClass, Mask, AgrimagP2MyCommunicationClass, Spin,
  PBSpinEdit, PBNumEdit, PBSuperSpin, ComCtrls;

type
  TformAgrimagP2DataloggerOptions= class(TForm)
    GroupBox1: TGroupBox;
    Panel1: TPanel;
    btnConfirm: TButton;
    btnCancel: TButton;
    chkDateTimeMin: TCheckBox;
    chkDateTimeMax: TCheckBox;
    chkRecordCountMax: TCheckBox;
    editRecordCountMax: TPBSpinEdit;
    dtpDateMin: TDateTimePicker;
    dtpTimeMin: TDateTimePicker;
    dtpDateMax: TDateTimePicker;
    dtpTimeMax: TDateTimePicker;
    procedure btnCancelClick(Sender: TObject);
    procedure btnConfirmClick(Sender: TObject);
    constructor CreateFrm(AOwner: TComponent; ADataloggerFilter: TAgrimagP2DataloggerFilter);
    procedure chkDateTimeMinClick(Sender: TObject);
    procedure chkDateTimeMaxClick(Sender: TObject);
    procedure chkRecordCountMaxClick(Sender: TObject);
  private
    { Private declarations }
    FDataloggerFilter: TAgrimagP2DataloggerFilter;
    procedure LoadUIState();
    procedure GetUIState();    
    procedure UpdateUIState();
  public
    { Public declarations }
  end;

implementation

uses AgrimagP2TranslationUnit;

{$R *.DFM}

constructor TformAgrimagP2DataloggerOptions.CreateFrm(AOwner: TComponent; ADataloggerFilter: TAgrimagP2DataloggerFilter);
begin
  inherited Create(AOwner);
  FDataloggerFilter := ADataloggerFilter;
  LoadUIState();
  UpdateUIState();
end;

procedure TformAgrimagP2DataloggerOptions.btnCancelClick(Sender: TObject);
begin
  ModalResult:=mrCancel;
end;

procedure TformAgrimagP2DataloggerOptions.btnConfirmClick(Sender: TObject);
begin
  GetUIState();
  ModalResult:=mrOk;
end;

procedure TformAgrimagP2DataloggerOptions.LoadUIState();
begin
  chkDateTimeMin.Checked := FDataloggerFilter.DateTimeMinEnabled;
  chkDateTimeMax.Checked := FDataloggerFilter.DateTimeMaxEnabled;
  chkRecordCountMax.Checked := FDataloggerFilter.RecordCountMaxEnabled;

  dtpDateMin.DateTime := FDataloggerFilter.DateTimeMin;
  dtpTimeMin.DateTime := FDataloggerFilter.DateTimeMin;

  dtpDateMax.DateTime := FDataloggerFilter.DateTimeMax;
  dtpTimeMax.DateTime := FDataloggerFilter.DateTimeMax;

  editRecordCountMax.Value := FDataloggerFilter.RecordCountMax;
end;

procedure TformAgrimagP2DataloggerOptions.GetUIState();
var Year, Month, Day: Word;
  Hour, Min, Sec, MSec: Word;
begin
  FDataloggerFilter.DateTimeMinEnabled := chkDateTimeMin.Checked;
  FDataloggerFilter.DateTimeMaxEnabled := chkDateTimeMax.Checked;
  FDataloggerFilter.RecordCountMaxEnabled := chkRecordCountMax.Checked;

  DecodeDate(dtpDateMin.Date, Year, Month, Day);
  DecodeTime(dtpTimeMin.Time, Hour, Min, Sec, MSec);
  FDataloggerFilter.DateTimeMin := EncodeDate(Year, Month, Day) + EncodeTime(Hour, Min, Sec, 0);

  DecodeDate(dtpDateMax.Date, Year, Month, Day);
  DecodeTime(dtpTimeMax.Time, Hour, Min, Sec, MSec);
  FDataloggerFilter.DateTimeMax := EncodeDate(Year, Month, Day) + EncodeTime(Hour, Min, Sec, 0);

  FDataloggerFilter.RecordCountMax := editRecordCountMax.Value;
end;

procedure TformAgrimagP2DataloggerOptions.UpdateUIState();
begin
  dtpDateMin.Enabled := chkDateTimeMin.Checked;
  dtpTimeMin.Enabled := chkDateTimeMin.Checked;

  dtpDateMax.Enabled := chkDateTimeMax.Checked;
  dtpTimeMax.Enabled := chkDateTimeMax.Checked;

  editRecordCountMax.Enabled := chkRecordCountMax.Checked;
end;

procedure TformAgrimagP2DataloggerOptions.chkDateTimeMinClick(Sender: TObject);
begin
  UpdateUIState();
end;

procedure TformAgrimagP2DataloggerOptions.chkDateTimeMaxClick(Sender: TObject);
begin
  UpdateUIState();
end;

procedure TformAgrimagP2DataloggerOptions.chkRecordCountMaxClick(Sender: TObject);
begin
  UpdateUIState();
end;

end.
