unit AgrimagP2RunUnit;

interface

procedure AgrimagP2Run;

implementation

uses sysutils, controls, Dialogs, Forms, AgrimagP2TranslationUnit,
  AgrimagP2GlobalUtilsClass, AgrimagP2MyCommunicationClass,
  AgrimagP2StatFrm, AgrimagP2EnterFrm, AgrimagP2MenuFrm;

var
    Demo : Boolean;
    ReadOK:Boolean;
    firmwareNO:integer;
    MyCommunication : TMyCommunication;

function GetDeviceFirmwareNumber(AProtokolType:TProtokolType;ASlaveID,AComNumber:Cardinal;
   BaudRate:TBaudRate;StopBits:TStopBits;Parity:TParity;Timeout:Integer;
   RtsControl:TRtsControl;EnableDTROnOpen:Boolean;ASwitchWait:Cardinal;
   AIPAddress: String; APort: Integer):integer;
var firmwareNO:integer;
begin
    MyCommunication.SetCommunicationParam(AComNumber,ASlaveID,BaudRate,db8BITS,
        StopBits,Parity,RtsControl,EnableDTROnOpen,Timeout,AIPAddress,APort);
    if not MyCommunication.Connect() then
        raise Exception.Create('Comunication port error');
    //cti verzi FW
    repeat
      ReadOK:=MyCommunication.ReadFirmNO(Cardinal(firmwareNO));
    until (ReadOK)or(MessageDlg(GetTranslationText('READ_ERROR', STR_READ_ERROR), mtError, [mbRetry, mbAbort], 0) = mrAbort);
    if not ReadOK then
        raise Exception.Create('No device was detected');
    Result:=firmwareNO;
end;

procedure AgrimagP2Run;
var
  statForm: TformAgrimagP2Stat;
  menuForm: TformAgrimagP2Menu;
begin
    MyCommunication:= nil;
    statForm:= nil;
    menuForm:= nil;

    try
      case formAgrimagP2Enter.ShowModal of
        //statistic
        mrOK:
        begin
              case formAgrimagP2Enter.ProtokolType of
                ptMagX1:MyCommunication:=TMyCommPort.Create();
                ptModbus:MyCommunication:=TMyModbus.Create();
                ptTCPModbus:MyCommunication:=TMyTCPModbus.Create();
                ptDemo:MyCommunication:=TDemoCommunication.Create();
              else
                raise Exception.Create('Unsuported protocol');
              end;
              //cti verzi firmware
              firmwareNO := GetDeviceFirmwareNumber(formAgrimagP2Enter.ProtokolType,formAgrimagP2Enter.SlaveID,
                      formAgrimagP2Enter.ComPortNumber,formAgrimagP2Enter.BaudRate,formAgrimagP2Enter.StopBits,
                      formAgrimagP2Enter.Parity,formAgrimagP2Enter.Timeout,formAgrimagP2Enter.RtsControl ,true,
                      formAgrimagP2Enter.rgConvertorRS485.ItemIndex, formAgrimagP2Enter.edtTCPIPAddress.Text,
                      formAgrimagP2Enter.seTCPPort.Value);

              statForm := TformAgrimagP2Stat.Create(Application);
              statForm.Caption := 'AgrimagP2 statistic';
              statForm.Init(MyCommunication, firmwareNO);
        end;
        //Service
        mrNo:
        begin
              case formAgrimagP2Enter.ProtokolType of
                ptMagX1:MyCommunication:=TMyCommPort.Create();
                ptModbus:MyCommunication:=TMyModbus.Create();
                ptTCPModbus:MyCommunication:=TMyTCPModbus.Create();
                ptDemo:MyCommunication:=TDemoCommunication.Create();
              else
                raise Exception.Create('Unsuported protocol');
              end;
              //cti verzi firmware
              firmwareNO := GetDeviceFirmwareNumber(formAgrimagP2Enter.ProtokolType,formAgrimagP2Enter.SlaveID,
                      formAgrimagP2Enter.ComPortNumber,formAgrimagP2Enter.BaudRate,formAgrimagP2Enter.StopBits,
                      formAgrimagP2Enter.Parity,formAgrimagP2Enter.Timeout,formAgrimagP2Enter.RtsControl ,true,
                      formAgrimagP2Enter.rgConvertorRS485.ItemIndex, formAgrimagP2Enter.edtTCPIPAddress.Text,
                      formAgrimagP2Enter.seTCPPort.Value);

              menuForm := TformAgrimagP2Menu.Create(Application);
              menuForm.Caption := 'AgrimagP2 service';
              menuForm.Init(MyCommunication, firmwareNO, formAgrimagP2Enter.ProtokolType);
        end;
        //Close
        mrCancel:
        begin
        end;
      end;

    except
      on E: Exception do
      begin
        MessageDlg(E.Message,mtError, [mbOk], 0);
        if MyCommunication <> nil then begin
          MyCommunication.Disconnect;
          FreeAndNil(MyCommunication);
        end;
        if statForm <> nil then statForm.Close;
        if menuForm <> nil then menuForm.Close;        
      end;
    end;
end;

end.
