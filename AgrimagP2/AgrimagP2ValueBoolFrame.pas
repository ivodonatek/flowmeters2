unit AgrimagP2ValueBoolFrame;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
   StdCtrls, AgrimagP2MyCommunicationClass, ExtCtrls, NodeClass, Buttons,
   AgrimagP2FunctionsUnit, AgrimagP2ValueHeadFrame;

type
  TframeAgrimagP2ValueBool = class(TframeAgrimagP2Head)
    Panel1: TPanel;
    lblValue: TLabel;
    Label1: TLabel;
    btnWrite: TButton;
    procedure btnWriteClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frameAgrimagP2ValueBool: TframeAgrimagP2ValueBool;

implementation

uses AgrimagP2TranslationUnit, AgrimagP2MenuFrm, ShellApi, AgrimagP2GlobalUtilsClass, Main;

{$R *.DFM}

{ TframeValueBool }

procedure TframeAgrimagP2ValueBool.btnWriteClick(Sender: TObject);
var WriteOk, ReadOK, MemoryPressent:Boolean;
    FourByte:T4Byte;
    LastCursor : TCursor;

    APort,AID:Cardinal;
    ABaudRate:TBaudRate;
    ADataBits:TDataBits;
    AStopBits:TStopBits;
    AParity:TParity;
    ATimeout:Cardinal;
    ARtsControl:TRtsControl;
    AEnableDTROnOpen:Boolean;
    AIPAddress: String;
    AIPPort: Integer;
begin
  if ((FEditNode.ModbusAdr - 1) = MODBUS_REGISTRY_ADDRESS_RESET) then begin
    // RESET
    MessageDlg(GetTranslationText('APP_RESTART', STR_APP_RESTART), mtInformation, [mbOK], 0);
    FMyCommunication.WriteReset;
    FMyCommunication.Disconnect;
    Konec := True;
    TformAgrimagP2Menu(Self.Owner).Close;
    MainForm.SetAgrimagP2RunRequest;
  end else begin
    LastCursor := Screen.Cursor;
    try
      if Assigned(WaitFrm) then
      begin
          WaitFrm.Show;
          WaitFrm.gWait.Progress:=0;
      end;
      Screen.Cursor:=crHourGlass;

      Integer(FourByte):=1;

      case FEditNode.FOut of           //F OUT
        1:  //dataloger delete
        begin
          repeat
            Application.ProcessMessages;
             ReadOK:=FMyCommunication.MemoryPresent(MemoryPressent)
          until (ReadOK)or(MessageDlg(GetTranslationText('READ_ERROR', STR_READ_ERROR),mtError, [mbRetry, mbAbort], 0) = mrAbort);
          if not ReadOK then
          begin
            lblValue.Caption:='Error';
          end
          else if not MemoryPressent then
          begin
            lblValue.Caption:='Module MEMORY is not present';
            ShowMessage('Module MEMORY is not present');
  //          Screen.Cursor:=crDefault;
  //          WaitFrm.Free;
            exit;
          end
          else
          begin
            FMyCommunication.GetCommunicationParam(APort,AID,ABaudRate,ADataBits,
              AStopBits,AParity,ARtsControl,AEnableDTROnOpen,ATimeout, AIPAddress, AIPPort);
            FMyCommunication.SetCommunicationParam(APort,AID,ABaudRate,ADataBits,
              AStopBits,AParity,ARtsControl,AEnableDTROnOpen,ATimeout+5000, AIPAddress, AIPPort);
          end;
        end;
      end;

          WaitFrm.gWait.AddProgress(10);
      repeat
        Application.ProcessMessages;
        WriteOK:=WriteValue(FEditNode.ModbusAdr-1,FourByte);
      until (WriteOK)or(MessageDlg(GetTranslationText('WRITE_ERROR', STR_WRITE_ERROR),mtError, [mbRetry, mbAbort], 0) = mrAbort);
      if WriteOK then
        lblValue.Caption:='Executed'
      else
        lblValue.Caption:='Not executed';

      case FEditNode.FOut of           //F OUT
        1:  //dataloger delete
        begin
          FMyCommunication.SetCommunicationParam(APort,AID,ABaudRate,ADataBits,
            AStopBits,AParity,ARtsControl,AEnableDTROnOpen,ATimeout, AIPAddress, AIPPort);
        end;
      end;

      WaitFrm.gWait.AddProgress(10);
      Application.ProcessMessages;
    finally
      Screen.Cursor:=LastCursor;
      if Assigned(WaitFrm) then
          WaitFrm.Hide;
    end;
  end;
end;

end.
