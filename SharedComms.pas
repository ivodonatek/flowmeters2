unit SharedComms;

interface

uses
  Windows, Messages, SysUtils, Classes, ExtCtrls, Forms, Registry, stdctrls;

type
  TCommResourceClient = class
  private
    FClientId : Cardinal;     // identifikacni cislo klienta
  public
    property ClientId: Cardinal read FClientId;
    constructor Create( AClientId : Cardinal );
  end;

  TCommResource = class
  private
    FName               : String;       // communication resource name
    FComPortHandle      : THANDLE;      // COM Port Device Handle
    FRefCount           : Cardinal;     // pocitadlo sdilenych pripojeni
    FLocked             : Boolean;      // stav zamknuti
    FClients            : TList;        // fronta klientu komunikace
    FClientIdCount      : Cardinal;     // pro potreby pridelovani identifikacnich cisel klientum
    //function GetClient( AClientId: Cardinal ): TCommResourceClient;
    function GetClientIndex( AClientId: Cardinal ): Integer;
    procedure AddClient( AClientId: Cardinal );
    procedure RemoveClient( AClientId: Cardinal );
  public
    property Name: String read FName;
    property ComPortHandle: THANDLE read FComPortHandle;
    property ReferenceCount: Cardinal read FRefCount;
    property Locked: Boolean read FLocked;
    constructor Create( AName : String; AComPortHandle : THANDLE );
    function AddClientReference: Cardinal;
    procedure RemoveClientReference( AClientId: Cardinal );
    function SetLock( AClientId: Cardinal; Value : Boolean): Boolean;
  end;

  TSharedComms = class
  private
    FCommResources      : TList;
    function GetCommResourceByName( AName: String ): TCommResource;
    function GetCommResourceByHandle( AComPortHandle : THANDLE ): TCommResource;
    procedure FreeCommResources;
  public
    constructor Create;
    destructor Destroy; override;
    function GetConnection( AName: String ): THANDLE;
    procedure AddConnection( AName : String; AComPortHandle : THANDLE );
    procedure RemoveConnection( AComPortHandle : THANDLE );
    function GetConnectionRefCount( AComPortHandle : THANDLE ): Cardinal;
    function AddConnectionRef( AComPortHandle : THANDLE ): Cardinal;
    procedure RemoveConnectionRef( AComPortHandle : THANDLE; AClientId: Cardinal );
    function GetLock( AComPortHandle : THANDLE ): Boolean;
    function SetLock( AComPortHandle : THANDLE; AClientId: Cardinal; Value : Boolean): Boolean;
  end;

  procedure GetCommNumbers(CommNumbers: TStrings);
  function GetCommNumberFromComboBox(ComboBox: TComboBox): Cardinal;
  procedure SetCommNumberInComboBox(ComboBox: TComboBox; CommNumber: Cardinal);
  procedure FillComboBoxWithCommNumbers(ComboBox: TComboBox);

implementation

//------------------------------------------------------------------------------
// TCommResourceClient
//------------------------------------------------------------------------------

constructor TCommResourceClient.Create( AClientId : Cardinal );
begin
  inherited Create;
  FClientId := AClientId;
end;

//------------------------------------------------------------------------------
// TCommResource
//------------------------------------------------------------------------------

constructor TCommResource.Create( AName : String; AComPortHandle : THANDLE );
begin
  inherited Create;
  FName := AName;
  FComPortHandle := AComPortHandle;
  FRefCount := 0;
  FLocked := false;
  FClients := TList.Create;
  FClientIdCount := 0;
end;

function TCommResource.AddClientReference: Cardinal;
begin
  Inc(FRefCount);
  Result := FClientIdCount;
  Inc(FClientIdCount);
end;

procedure TCommResource.RemoveClientReference( AClientId: Cardinal );
begin
  if FRefCount > 0 then Dec(FRefCount);
  RemoveClient(AClientId);
end;

{function TCommResource.GetClient( AClientId: Cardinal ): TCommResourceClient;
var
  i: integer;
  client: TCommResourceClient;
begin
  Result := nil;
  for i := 0 to FClients.Count - 1 do begin
    client := TCommResourceClient(FClients.Items[i]);
    if client.ClientId = AClientId then begin
      Result := client;
      break;
    end;
  end;
end;}

function TCommResource.GetClientIndex( AClientId: Cardinal ): Integer;
var
  i: integer;
  client: TCommResourceClient;
begin
  Result := -1;
  for i := 0 to FClients.Count - 1 do begin
    client := TCommResourceClient(FClients.Items[i]);
    if client.ClientId = AClientId then begin
      Result := i;
      break;
    end;
  end;
end;

procedure TCommResource.AddClient( AClientId: Cardinal );
var
  client: TCommResourceClient;
begin
  client := TCommResourceClient.Create(AClientId);
  FClients.Add(client);
end;

procedure TCommResource.RemoveClient( AClientId: Cardinal );
var
  i: integer;
  client: TCommResourceClient;
begin
  for i := 0 to FClients.Count - 1 do begin
    client := TCommResourceClient(FClients.Items[i]);
    if client.ClientId = AClientId then begin
      FClients.Remove(client);
      client.Free;
      break;
    end;
  end;
end;

function TCommResource.SetLock( AClientId: Cardinal; Value : Boolean): Boolean;
var
  idx: integer;
begin
  Result := false;
  idx := GetClientIndex(AClientId);
  if Value then begin
    if idx = 0 then begin
      FLocked := true;
      Result := true;
    end else if idx < 0 then begin
      AddClient(AClientId);
      if FClients.Count = 1 then begin
        FLocked := true;
        Result := true;
      end;
    end;
  end else begin
    if idx = 0 then begin
      FLocked := false;
      RemoveClient(AClientId);
      Result := true;
    end
  end;
end;

//------------------------------------------------------------------------------
// TSharedComms
//------------------------------------------------------------------------------

constructor TSharedComms.Create;
begin
  inherited Create;
  FCommResources := TList.Create;
end;

destructor TSharedComms.Destroy;
begin
  FreeCommResources;
  FCommResources.Free;
  inherited Destroy;
end;

function TSharedComms.GetConnection( AName: String ): THANDLE;
var
  commRes: TCommResource;
begin
  Result := INVALID_HANDLE_VALUE;
  commRes := GetCommResourceByName(AName);
  if commRes <> nil then Result := commRes.ComPortHandle;
end;

procedure TSharedComms.AddConnection( AName : String; AComPortHandle : THANDLE );
var
  commRes: TCommResource;
begin
  commRes := TCommResource.Create(AName, AComPortHandle);
  FCommResources.Add(commRes);
end;

procedure TSharedComms.RemoveConnection( AComPortHandle : THANDLE );
var
  commRes: TCommResource;
begin
  commRes := GetCommResourceByHandle(AComPortHandle);
  if commRes <> nil then begin
    FCommResources.Remove(commRes);
    commRes.Free;
  end;
end;

function TSharedComms.GetConnectionRefCount( AComPortHandle : THANDLE ): Cardinal;
var
  commRes: TCommResource;
begin
  Result := 0;
  commRes := GetCommResourceByHandle(AComPortHandle);
  if commRes <> nil then Result := commRes.ReferenceCount;
end;

function TSharedComms.AddConnectionRef( AComPortHandle : THANDLE ): Cardinal;
var
  commRes: TCommResource;
begin
  Result := Cardinal(-1);
  commRes := GetCommResourceByHandle(AComPortHandle);
  if commRes <> nil then Result := commRes.AddClientReference;
end;

procedure TSharedComms.RemoveConnectionRef( AComPortHandle : THANDLE; AClientId: Cardinal );
var
  commRes: TCommResource;
begin
  commRes := GetCommResourceByHandle(AComPortHandle);
  if commRes <> nil then commRes.RemoveClientReference(AClientId);
end;

function TSharedComms.GetCommResourceByName( AName: String ): TCommResource;
var
  i: integer;
  commRes: TCommResource;
begin
  Result := nil;
  for i := 0 to FCommResources.Count - 1 do begin
    commRes := TCommResource(FCommResources.Items[i]);
    if commRes.Name = AName then begin
      Result := commRes;
      break;
    end;
  end;
end;

function TSharedComms.GetCommResourceByHandle( AComPortHandle : THANDLE ): TCommResource;
var
  i: integer;
  commRes: TCommResource;
begin
  Result := nil;
  for i := 0 to FCommResources.Count - 1 do begin
    commRes := TCommResource(FCommResources.Items[i]);
    if commRes.ComPortHandle = AComPortHandle then begin
      Result := commRes;
      break;
    end;
  end;
end;

procedure TSharedComms.FreeCommResources;
var
  i: integer;
  commRes: TCommResource;
begin
  for i := 0 to FCommResources.Count - 1 do begin
    commRes := TCommResource(FCommResources.Items[i]);
    commRes.Free;
  end;
end;

function TSharedComms.GetLock( AComPortHandle : THANDLE ): Boolean;
var
  commRes: TCommResource;
begin
  Result := false;
  commRes := GetCommResourceByHandle(AComPortHandle);
  if commRes <> nil then Result := commRes.Locked;
end;

function TSharedComms.SetLock( AComPortHandle : THANDLE; AClientId: Cardinal; Value : Boolean): Boolean;
var
  commRes: TCommResource;
begin
  Result := false;
  commRes := GetCommResourceByHandle(AComPortHandle);
  if commRes <> nil then Result := commRes.SetLock(AClientId, Value);
end;

{
  Get the numbers (1, 2, ...) of available comm ports (com1, com2, ...).
  Used registry key: hkey_local_machine\hardware\devicemap\serialcomm
}
procedure GetCommNumbers(CommNumbers: TStrings);
var
  reg: TRegistry;
  st: Tstrings;
  i: Integer;
  str: String;
begin
  reg := TRegistry.Create;
  try
    reg.Access := KEY_READ;
    reg.RootKey := HKEY_LOCAL_MACHINE;
    reg.OpenKey('hardware\devicemap\serialcomm', False);
    st := TstringList.Create;
    try
      reg.GetValueNames(st);
      for i := 0 to st.Count - 1 do
      begin
        str := reg.Readstring(st.strings[i]);
        str :=  StringReplace(str, 'COM', '', [rfIgnoreCase]);
        CommNumbers.Append(str);
      end;
    finally
      st.Free;
    end;
    reg.CloseKey;
  finally
    reg.Free;
  end;
end;

// Get selected COM number from combo box (from selected numeric string)
function GetCommNumberFromComboBox(ComboBox: TComboBox): Cardinal;
begin
  Result := 0;
  if ComboBox.ItemIndex >= 0 then
    Result := StrToInt(ComboBox.Items[ComboBox.ItemIndex]);
end;

// Set actual (selected) COM number in combo box (numeric strings)
procedure SetCommNumberInComboBox(ComboBox: TComboBox; CommNumber: Cardinal);
var
  i, idx: Integer;
  str: String;
begin
  idx := -1;
  str := IntToStr(CommNumber);
  for i := 0 to ComboBox.Items.Count - 1 do begin
    if str = ComboBox.Items[i] then begin
      idx := i;
      break;
    end;
  end;
  ComboBox.ItemIndex := idx;
end;

// Fill combo box (numeric strings) with available COM port numbers
procedure FillComboBoxWithCommNumbers(ComboBox: TComboBox);
begin
  ComboBox.Clear;
  GetCommNumbers(ComboBox.Items);
  ComboBox.ItemIndex := -1;
end;

end.
