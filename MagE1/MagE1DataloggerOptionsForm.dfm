object formMagE1DataloggerOptions: TformMagE1DataloggerOptions
  Left = 231
  Top = 131
  BorderIcons = []
  BorderStyle = bsDialog
  Caption = 'Options'
  ClientHeight = 202
  ClientWidth = 497
  Color = 12555391
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'Verdana'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 14
  object GroupBox1: TGroupBox
    Left = 8
    Top = 8
    Width = 481
    Height = 141
    Caption = 'Record Filter Setting'
    TabOrder = 0
    object Panel1: TPanel
      Left = 8
      Top = 16
      Width = 465
      Height = 117
      BevelOuter = bvLowered
      Color = 14670037
      TabOrder = 0
      object chkDateTimeMin: TCheckBox
        Left = 10
        Top = 10
        Width = 178
        Height = 17
        Caption = 'First record time'
        TabOrder = 0
        OnClick = chkDateTimeMinClick
      end
      object chkDateTimeMax: TCheckBox
        Left = 10
        Top = 42
        Width = 178
        Height = 17
        Caption = 'Last record time'
        TabOrder = 1
        OnClick = chkDateTimeMaxClick
      end
      object chkRecordCountMax: TCheckBox
        Left = 10
        Top = 90
        Width = 202
        Height = 17
        Caption = 'Maximum number of records'
        TabOrder = 2
        OnClick = chkRecordCountMaxClick
      end
      object editRecordCountMax: TPBSpinEdit
        Left = 216
        Top = 88
        Width = 121
        Height = 26
        Cursor = crDefault
        MaxValue = 2147483647
        MinValue = 0
        TabOrder = 3
        Value = 0
        Alignment = taLeftJustify
      end
      object dtpDateMin: TDateTimePicker
        Left = 216
        Top = 8
        Width = 121
        Height = 22
        CalAlignment = dtaLeft
        Date = 42491.8347558912
        Time = 42491.8347558912
        DateFormat = dfShort
        DateMode = dmComboBox
        Kind = dtkDate
        ParseInput = False
        ParentShowHint = False
        ShowHint = False
        TabOrder = 4
      end
      object dtpTimeMin: TDateTimePicker
        Left = 336
        Top = 8
        Width = 121
        Height = 22
        CalAlignment = dtaLeft
        Date = 42491.8347558912
        Time = 42491.8347558912
        DateFormat = dfShort
        DateMode = dmComboBox
        Kind = dtkTime
        ParseInput = False
        ParentShowHint = False
        ShowHint = False
        TabOrder = 5
      end
      object dtpDateMax: TDateTimePicker
        Left = 216
        Top = 40
        Width = 121
        Height = 22
        CalAlignment = dtaLeft
        Date = 42491.8347558912
        Time = 42491.8347558912
        DateFormat = dfShort
        DateMode = dmComboBox
        Kind = dtkDate
        ParseInput = False
        ParentShowHint = False
        ShowHint = False
        TabOrder = 6
      end
      object dtpTimeMax: TDateTimePicker
        Left = 336
        Top = 40
        Width = 121
        Height = 22
        CalAlignment = dtaLeft
        Date = 42491.8347558912
        Time = 42491.8347558912
        DateFormat = dfShort
        DateMode = dmComboBox
        Kind = dtkTime
        ParseInput = False
        ParentShowHint = False
        ShowHint = False
        TabOrder = 7
      end
    end
  end
  object btnConfirm: TButton
    Left = 160
    Top = 168
    Width = 73
    Height = 25
    Caption = 'OK'
    Default = True
    TabOrder = 1
    OnClick = btnConfirmClick
  end
  object btnCancel: TButton
    Left = 280
    Top = 168
    Width = 73
    Height = 25
    Cancel = True
    Caption = 'Cancel'
    TabOrder = 2
    OnClick = btnCancelClick
  end
end
