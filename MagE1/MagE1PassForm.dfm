object formMagE1Pass: TformMagE1Pass
  Left = 549
  Top = 318
  BorderIcons = []
  BorderStyle = bsDialog
  Caption = 'Authorization'
  ClientHeight = 93
  ClientWidth = 377
  Color = 12555391
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Verdana'
  Font.Style = [fsBold]
  OldCreateOrder = False
  PixelsPerInch = 106
  TextHeight = 16
  object GroupBox1: TGroupBox
    Left = 9
    Top = 9
    Width = 358
    Height = 74
    Caption = 'Factory Setting'
    TabOrder = 0
    object Panel1: TPanel
      Left = 9
      Top = 18
      Width = 340
      Height = 47
      BevelOuter = bvLowered
      Color = 14670037
      TabOrder = 0
      object eValue: TEdit
        Left = 9
        Top = 10
        Width = 120
        Height = 22
        MaxLength = 8
        PasswordChar = '*'
        TabOrder = 0
      end
      object btnCancel: TButton
        Left = 229
        Top = 9
        Width = 83
        Height = 29
        Cancel = True
        Caption = 'Cancel'
        TabOrder = 1
        OnClick = btnCancelClick
      end
      object btnConfirm: TButton
        Left = 146
        Top = 9
        Width = 84
        Height = 29
        Caption = 'OK'
        Default = True
        TabOrder = 2
        OnClick = btnConfirmClick
      end
    end
  end
end
