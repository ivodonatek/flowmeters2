unit MagE1MyCommunicationClass;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     Gauges, MagE1FunctionsUnit, ModbusM, IniFiles, ExtCtrls, ScktComp, Contnrs;

const
  MaxLength = 512;
  MAX_ERROR = 3;
  RECREATE_ERROR_COUNT = 9;
  RECONNECT_ERROR_COUNT = 3;
  TCP_TIMER_PROGRESS = 200;//3000; // 1,5 - for every error encountered
  DEMO_FIRMWARE_NUMBER = 1022;
  TCP_CONN_TIMEOUT = 5/24/60/60; // 5 seconds

  MODBUS_REGISTRY_ADDRESS_RTC_TIME = 2014 - 1;
  MODBUS_REGISTRY_ADDRESS_RTC_DATE = 2012 - 1;
  MODBUS_REGISTRY_ADDRESS_FIRMWARE = 1012 - 1;

  MODBUS_REGISTRY_ADDRESS_RESET    = 4042 - 1;

  B1_ONLINE_FLOW        = 100 - 1;
  B1_ONLINE_FLOW_SIGN   = 102 - 1;
  B1_ONLINE_TOTAL       = 104 - 1;
  B1_ONLINE_BATCH       = 106 - 1;
  B1_ONLINE_ERROR       = 108 - 1;

  DATALOGGER_RECORD_SIZE         = 8;
  DATALOGGER_RECORD_COUNT_MAX    = 30;
  DATALOGGER_BLOCK_SIZE_MAX      = DATALOGGER_RECORD_SIZE * DATALOGGER_RECORD_COUNT_MAX;
  DATALOGGER_DEF_MEMORY_SIZE     = $100000;

  DATALOGGER_MODBUS_SPACE_MIN     = 10000;
  DATALOGGER_MODBUS_SPACE_MAX     = 60000;
  DATALOGGER_MODBUS_SPACE_SIZE    = DATALOGGER_MODBUS_SPACE_MAX - DATALOGGER_MODBUS_SPACE_MIN;
  DATALOGGER_MEMORY_SIZE_ADDRESS  = 200 - 1;
  DATALOGGER_BASE_ADDRESS_ADDRESS = 202 - 1;

  DATALOGGER_FILTER_DATETIME_MIN_TXT = '01/01/15 00:00:00';
  DATALOGGER_FILTER_DATETIME_MAX_TXT = '01/01/40 00:00:00';
  DATALOGGER_FILTER_RECORD_COUNT_MAX = 100;

  DATALOGGER_INVALID_DATETIME     = 0;

const ModbusMeasurementDataAdr : Array[1..3] of Cardinal =
(4024 - 1, 4026 - 1, 4028 - 1);
const ModbusCalibrationDataAdr : Array[1..3] of Cardinal =
(4018 - 1, 4020 - 1, 4022 - 1);

const Memory_Pressent = 1119;

type
  TBuffer = array[0..MaxLength-1] of Byte;
  T4Byte = array[0..3] of Byte;
  T2Byte = array[0..1] of Byte;

  TOnline = record
    Flow,Temp:Cardinal;
    FlowUnit:TFlowUnit;
    Volume:Double;
    VolumeUnit:TVolumeUnit;
    VolumeFormat:Byte;
    FlowFormat:Byte;
    TempFormat:Byte;
    TempUnit:TTempUnit;
  end;

  TOnlineB1 = record
    Flow: Double;
    Total: Double;
    Batch: Double;
    ErrorCode: Cardinal;
  end;

  TDataB1 = class
    DateTime: TDateTime;
    Total: Double;
  end;

  TDataB1List = class(TObjectList)
  private
    function GetItem(Index: Integer): TDataB1;
  public
    property Items[Index: Integer]: TDataB1 read GetItem; default;
  end;

  //Datum a cas (4 byte)
  //Total (4 byte)
  TDataRecordB1 = record
    DateTime: Cardinal;
    Total: Double;
  end;

  TDataBlockB1 = array[0 .. DATALOGGER_RECORD_COUNT_MAX - 1] of TDataRecordB1;

  TProtokolType = (ptMagX1, ptModbus, ptTCPModbus, ptDemo);

  TBaudRate = ( br110, br300, br600, br1200, br2400, br4800,
                br9600, br14400, br19200, br38400, br56000,
                br57600, br115200, br128000, br256000 );
  TDataBits = ( db5BITS, db6BITS, db7BITS, db8BITS );
  TStopBits = ( sb1BITS, sb1HALFBITS, sb2BITS );
  TParity = ( ptNONE, ptODD, ptEVEN, ptMARK, ptSPACE );
  TRtsControl = ( rcRTSEnable, rcRTSDisable, rcRTSToggle );

  TResponse = (rnNONE,rnError,rnOK, rnWait);

  TSocketStatus = (ssUnknown, ssConnected, ssDisconnected);

  TMagE1DataloggerFilter = class
  private
    FDateTimeMin : TDateTime;
    FDateTimeMax : TDateTime;
    FRecordCountMax : Cardinal;
    FDateTimeMinEnabled : Boolean;
    FDateTimeMaxEnabled : Boolean;
    FRecordCountMaxEnabled : Boolean;
  public
    constructor Create();
    function TryParseDateTime(const Str: string; var Val: TDateTime) : Boolean;
    function TryParseRecordCountMax(const Str: string; var Val: Cardinal) : Boolean;
    function ConvertDateTimeToStr(Val: TDateTime) : string;
    function ConvertRecordCountMaxToStr(Val: Cardinal) : string;
    function TrySetDateTimeMin(const Str: string) : Boolean;
    function TrySetDateTimeMax(const Str: string) : Boolean;
    function TrySetRecordCountMax(const Str: string) : Boolean;
    function GetDateTimeMinAsStr() : string;
    function GetDateTimeMaxAsStr() : string;
    function GetRecordCountMaxAsStr() : string;
    function IsRecordDateTimeValid(RecordDateTime: TDateTime) : Boolean;
    function IsRecordCountValid(RecordCount: Cardinal) : Boolean;
    function IsRecordValid(RecordDateTime: TDateTime; RecordCount: Cardinal) : Boolean;
    procedure LoadFromIniFile(iniF: TIniFile);
    procedure SaveToIniFile(iniF: TIniFile);
    property DateTimeMin: TDateTime read FDateTimeMin write FDateTimeMin;
    property DateTimeMax: TDateTime read FDateTimeMax write FDateTimeMax;
    property RecordCountMax: Cardinal read FRecordCountMax write FRecordCountMax;
    property DateTimeMinEnabled: Boolean read FDateTimeMinEnabled write FDateTimeMinEnabled;
    property DateTimeMaxEnabled: Boolean read FDateTimeMaxEnabled write FDateTimeMaxEnabled;
    property RecordCountMaxEnabled: Boolean read FRecordCountMaxEnabled write FRecordCountMaxEnabled;
    property DateTimeMinText: string read GetDateTimeMinAsStr;
    property DateTimeMaxText: string read GetDateTimeMaxAsStr;
    property RecordCountMaxText: string read GetRecordCountMaxAsStr;
  end;

  TMyCommunication = class
    ProtokolType:TProtokolType;
  private
  protected
    // datalogger
    FDataloggerFilteredRecordCount : Cardinal;
    FDataloggerFilteredRecordReadCount : Cardinal;
    FDataloggerMemorySize : Cardinal;
    function GetDataloggerNextFilteredRecordBlockByteSize(): Cardinal;
  public
    function IntTo4B(I:integer):T4Byte;
    procedure SetCommunicationParam(APort:Cardinal;AID:Cardinal;ABaudRate:TBaudRate;ADataBits:TDataBits;
      AStopBits:TStopBits;AParity:TParity;ARtsControl:TRtsControl;AEnableDTROnOpen:Boolean;ATimeout:Cardinal;
      AIPAddress:String;AIPPort:Integer); virtual; abstract;
    procedure GetCommunicationParam(var APort:Cardinal;var AID:Cardinal;var ABaudRate:TBaudRate;var ADataBits:TDataBits;
      var AStopBits:TStopBits;var AParity:TParity;var ARtsControl:TRtsControl;var AEnableDTROnOpen:Boolean;var ATimeout:Cardinal;
      var AIPAddress:String; var AIPPort:Integer); virtual; abstract;
    function GetComPort: Cardinal; virtual; abstract;
    function Connect():Boolean; virtual; abstract;
    procedure Disconnect(); virtual; abstract;
    {Statistika}
    function ReadFiveMins(var Buffer : array of Byte; M,D,H3 : Byte) : Boolean; virtual; abstract;
    function ReadHours(var Buffer : array of Byte; M,D : Byte) : Boolean; virtual; abstract;
    function ReadDays(var Buffer : array of Byte; M,D : Byte) : Boolean; virtual; abstract;
    function ReadMonth(var Buffer : array of Byte; M : Byte) : Boolean; virtual; abstract;
    {MENU - Device no.3}
    function ReadMenuStr(Adr: Cardinal; var Str:String; Size: Integer):Boolean; virtual; abstract;
    function WriteMenuStr(Adr: Cardinal; Str: String; Size: Integer):Boolean; virtual; abstract;
    function ReadMenuValue(Adr: Cardinal;var Data:T4Byte):Boolean; virtual; abstract;
    function WriteMenuValue(Adr: Cardinal;Value:T4Byte):Boolean; virtual; abstract;

    procedure WriteMenuValueNoResponse(Adr: Cardinal; Value:T4Byte); virtual;
    function ReadFirmNO(var firmwareNO:Cardinal):Boolean; virtual; abstract;
    function ReadRTC(Adr: Cardinal;var RetValue:Cardinal): boolean ;virtual; abstract;
    function WriteRTC(Adr: Cardinal; Value:Cardinal): boolean;virtual; abstract;
    function ReadRTCAdr(Adr: Cardinal;var RetValue:Cardinal): boolean ;virtual;
    function WriteRTCAdr(Adr: Cardinal; Value:Cardinal): boolean;virtual;
    function ReadRTCTime(var RetValue:Cardinal): boolean ;virtual;
    function WriteRTCTime(Value:Cardinal): boolean;virtual;
    function ReadRTCDate(var RetValue:Cardinal): boolean ;virtual;
    function WriteRTCDate(Value:Cardinal): boolean;virtual;
    procedure WriteReset; virtual;
    function ReadOnlineB1(var OnlineData: TOnlineB1; ReadTotalValueAsDouble: Boolean = False): Boolean; virtual;
    function WriteCalibration(Bod: Cardinal; Value: Cardinal): boolean;virtual; abstract;//device 7
    function WriteMeasurment(Bod:Cardinal;CalibData:Cardinal;MeasurData:Cardinal): boolean;virtual; abstract;   //device 7
    function ReadCalibration(Bod: Cardinal;var  RetValue: Cardinal): boolean; virtual; abstract;//device 7
    function ReadMeasurment(Bod:Cardinal;var CalibData:Integer;var MeasurData:Integer): boolean; virtual; abstract;//device 7
    function MemoryPresent(var Present: Boolean): Boolean;virtual; abstract;
    procedure SetStateToError; virtual;
    // datalogger
    function CalculateDataloggerFilteredRecords(var Running: Boolean; Filter: TMagE1DataloggerFilter) : Boolean; virtual; abstract;
    function ReadDataloggerNextFilteredRecordBlock(Records: TDataB1List; Filter: TMagE1DataloggerFilter): Boolean; virtual;
    property DataloggerFilteredRecordCount: Cardinal read FDataloggerFilteredRecordCount;
    property DataloggerFilteredRecordReadCount: Cardinal read FDataloggerFilteredRecordReadCount;
  end;

  TMyCommPort = class(TMyCommunication)
  private
    { Private declarations }
    SwitchWait : Integer;
    Port:Cardinal;
    ID:Cardinal;
    BaudRate:TBaudRate;
    DataBits:TDataBits;
    StopBits:TStopBits;
    Parity:TParity;
    Timeout:Cardinal;
    RtsControl:TRtsControl;
    EnableDTROnOpen:Boolean;

    hCommFile : THandle;
    function CountRX: integer;
    procedure SetMyCommState(DCBlength, BaudRate : Cardinal; Flags : Integer;
                           wReserved, XonLim, XoffLim : Word;
                           ByteSize, Parity, StopBits : Byte);
    procedure SetMyTimeouts(ReadIntervalTimeout, ReadTotalTimeoutMultiplier,
                          ReadTotalTimeoutConstant, WriteTotalTimeoutMultiplier,
                          WriteTotalTimeoutConstant : Cardinal);
    function IsConnect:Boolean;
    function Sync():Boolean;
    function ReadIf(ID:Cardinal; Count:Byte; Device:Byte; Read:Byte;var Address:Cardinal; var S: TBuffer): Boolean;
    function WriteChar(ID:Cardinal; Size:Byte; Device:Byte;Read:Byte; Address:Cardinal; var S: TBuffer; Count:Byte):Boolean;
    procedure CRTS(Read:boolean);
//    function GetFlags : Integer;
  public
    Busy:Boolean;
    { Public declarations }
    constructor Create();
    destructor Destroy; override;
    procedure SetCommunicationParam(APort:Cardinal;AID:Cardinal;ABaudRate:TBaudRate;ADataBits:TDataBits;
      AStopBits:TStopBits;AParity:TParity;ARtsControl:TRtsControl;AEnableDTROnOpen:Boolean;ATimeout:Cardinal;
      AIPAddress:String;AIPPort:Integer); override;
    procedure GetCommunicationParam(var APort:Cardinal;var AID:Cardinal;var ABaudRate:TBaudRate;var ADataBits:TDataBits;
      var AStopBits:TStopBits;var AParity:TParity;var ARtsControl:TRtsControl;var AEnableDTROnOpen:Boolean;var ATimeout:Cardinal;
      var AIPAddress:String; var AIPPort:Integer); override;
    function GetComPort: Cardinal; override;
    function Connect():Boolean;override;
    procedure Disconnect(); override;

    procedure FlushBuffers( inBuf, outBuf: boolean );
    function COMSearch(pb:TGauge;AID:Cardinal): String;
    {DATA - Device no.2}
    function ReadBuffer(var Buffer : array of Byte; Size : Byte; Address1 : Byte; Address2 : Byte; Address3 : Byte) : Boolean;

    {Statistika}
    function ReadFiveMins(var Buffer : array of Byte; M,D,H3 : Byte) : Boolean; override;
    function ReadHours(var Buffer : array of Byte; M,D : Byte) : Boolean; override;
    function ReadDays(var Buffer : array of Byte; M,D : Byte) : Boolean; override;
    function ReadMonth(var Buffer : array of Byte; M : Byte) : Boolean; override;
    {MENU - Device no.3}
    function ReadMenuStr(Adr: Cardinal; var Str:String; Size: Integer):Boolean; override;
    function WriteMenuStr(Adr: Cardinal; Str: String; Size: Integer):Boolean; override;
    function ReadMenuValue(Adr: Cardinal;var Data:T4Byte):Boolean; override;
    function WriteMenuValue(Adr: Cardinal;Value:T4Byte):Boolean; override;

    function ReadRTC(Adr: Cardinal;var RetValue:Cardinal): Boolean; override;
    function WriteRTC(Adr: cardinal; Value:Cardinal): boolean; override;
    function ReadFirmNO(var firmwareNO:Cardinal):Boolean; override;
    function WriteCalibration(Adr: Cardinal; Value: Cardinal): boolean; override;//device 7
    function WriteMeasurment(Bod:Cardinal;CalibData:Cardinal;MeasurData:Cardinal): boolean; override;  //device 7
    function ReadCalibration(Bod: Cardinal;var  RetValue: Cardinal): boolean; override;  //
    function ReadMeasurment(Bod:Cardinal;var CalibData:Integer;var MeasurData:Integer): boolean; override;  //

    function WriteOnline(adr: Integer): boolean;
    function ReadOnline(Firm:Cardinal;var Adr:Cardinal;var OnlineData:TOnline):Boolean ;
    function MemoryPresent(var Present: Boolean): Boolean; override;

    // datalogger
    function CalculateDataloggerFilteredRecords(var Running: Boolean; Filter: TMagE1DataloggerFilter) : Boolean; override;
  end;

  TMyModbus = class(TMyCommunication)
  private
    ModbusM:TModbusM;
    ResponseFlag:TResponse;
//    LastError:Integer;
    // datalogger
    FDataloggerBaseAddress : Cardinal;
    FDataloggerFilteredFirstRecordAddress : Cardinal;
    //
    procedure ModbusMError(Sender: TObject; const ErrorMsg: String);
    procedure ModbusMResponseReady(Sender: TObject);
    function ReadBuffer(var Buffer : array of Byte; Size : Byte; Address : Word) : Boolean;
    // datalogger
    function ReadDataloggerBaseAddressRegister(var BaseAdrReg : Cardinal) : Boolean;
    function WriteDataloggerBaseAddressRegister(BaseAdrReg : Cardinal) : Boolean;
    function ReadDataloggerMemorySizeRegister(var MemorySizeReg : Cardinal) : Boolean;
    function ReadDataloggerRecordDateTimePack(RecordAddress : Cardinal; var RecordDateTimePack : T4Byte) : Boolean;
    function GetDataloggerNextFilteredRecordAbsoluteAddress(): Cardinal;
    function GetDataloggerNextFilteredRecordBaseAddress(): Cardinal;
    function GetDataloggerNextFilteredRecordModbusAddress(): Word;
    function FindDataloggerNextOldestRecordIndexInterval(var RecordIndex1: Cardinal; var RecordIndex2: Cardinal; var RecordDateTime1: TDateTime; var RecordDateTime2: TDateTime): Boolean;
    function FindDataloggerNextNewestRecordIndexInterval(var RecordIndex1: Cardinal; var RecordIndex2: Cardinal; var RecordDateTime1: TDateTime; var RecordDateTime2: TDateTime): Boolean;
    function FindDataloggerOldestRecordIndex(var OldestRecordIndex: Cardinal; var Running: Boolean; Filter: TMagE1DataloggerFilter): Boolean;
    function FindDataloggerNewestRecordIndex(var NewestRecordIndex: Cardinal; var Running: Boolean; Filter: TMagE1DataloggerFilter): Boolean;
  public
    constructor Create();
    destructor Destroy; override;
    procedure SetCommunicationParam(APort:Cardinal;AID:Cardinal;ABaudRate:TBaudRate;ADataBits:TDataBits;
      AStopBits:TStopBits;AParity:TParity;ARtsControl:TRtsControl;AEnableDTROnOpen:Boolean;ATimeout:Cardinal;
      AIPAddress:String;AIPPort:Integer); override;
    procedure GetCommunicationParam(var APort:Cardinal;var AID:Cardinal;var ABaudRate:TBaudRate;var ADataBits:TDataBits;
      var AStopBits:TStopBits;var AParity:TParity;var ARtsControl:TRtsControl;var AEnableDTROnOpen:Boolean;var ATimeout:Cardinal;
      var AIPAddress:String; var AIPPort:Integer); override;
    function GetComPort: Cardinal; override;
    function Connect():Boolean; override;
    procedure Disconnect(); override;
        {MENU - Device no.3}
    function ReadMenuStr(Adr: Cardinal; var Str:String; Size: Integer):Boolean; override;
    function WriteMenuStr(Adr: Cardinal; Str: String; Size: Integer):Boolean; override;
    function ReadMenuValue(Adr: Cardinal; var Data:T4Byte):Boolean; override;
    function WriteMenuValue(Adr: Cardinal;Value:T4Byte):Boolean; override;

    procedure WriteMenuValueNoResponse(Adr: Cardinal; Value:T4Byte); override;
    function ReadRTC(Adr: Cardinal;var RetValue:Cardinal): Boolean; override;
    function WriteRTC(Adr: Cardinal;Value:Cardinal): Boolean; override;
    function ReadFirmNO(var firmwareNO:Cardinal):Boolean; override;
    function GetLastError:integer;
    function ReadOnline(Adr:Cardinal;var OnlineData:TOnline):Boolean ;
    {Kalibrace}
    function ReadCalibration(Bod: Cardinal;var  RetValue: Cardinal): boolean; override;
    function WriteCalibration(Bod: Cardinal; Value: Cardinal): boolean;override;
    function ReadMeasurment(Bod:Cardinal;var CalibData:Integer;var MeasurData:Integer): boolean; override;
    function WriteMeasurment(Bod:Cardinal;CalibData:Cardinal;MeasurData:Cardinal): boolean;override;
    {Statistika}
    function ReadFiveMins(var Buffer : array of Byte; M,D,H3 : Byte) : Boolean; override;
    function ReadHours(var Buffer : array of Byte; M,D : Byte) : Boolean; override;
    function ReadDays(var Buffer : array of Byte; M,D : Byte) : Boolean; override;
    function ReadMonth(var Buffer : array of Byte; M : Byte) : Boolean; override;
    function MemoryPresent(var Present: Boolean): Boolean; override;
    // datalogger
    function CalculateDataloggerFilteredRecords(var Running: Boolean; Filter: TMagE1DataloggerFilter) : Boolean; override;
    function ReadDataloggerNextFilteredRecordBlock(Records: TDataB1List; Filter: TMagE1DataloggerFilter): Boolean; override;
  end;

  TMyTCPModbus = class(TMyCommunication)
  private
    FIPAddress: String;
    FIPPort: Integer;
    FIsReconnecting: Boolean;
    FSocketErrorCount: Integer;
    FErrorCount: Integer;
    FTimeout : Cardinal;
    // TCP
    FSocketStatus: TSocketStatus;
    FPacketStatus: TResponse;
    FClientSocket: TClientSocket;
    FTimer: TTimer;
    FTCPReadValues: TDataByte;
    FTCPReadSize: Integer;
    FTCPWriteValues: TDataByte;
    FTCPWriteSize: Integer;
    // Modbus
    FFunctionCode: Integer;
    FOffset: Integer;
    FQuantity: Integer;
    // Modbus Setting
    FRegisterDim: Integer;
    FSlaveID: Integer;
    // Data buffers
    FReadValues: TDataByte;
    FWriteValues: TDataByte;
    // Modbus functions
    function ExpectedLength: Integer;
    procedure ValidateInput; // TCPReadValues -> ReadValues [for specific FunctionCode, Offset and Quantity]
    procedure PrepareOutput; // FunctionCode + Offset + Quantity + WriteValues -> TCPWriteValues
    // TCP Events
    procedure SocketTimeout(Sender: TObject);
    procedure SocketConnect(Sender: TObject; Socket: TCustomWinSocket);
    procedure SocketDisconnect(Sender: TObject; Socket: TCustomWinSocket);
    procedure SocketError(Sender: TObject; Socket: TCustomWinSocket; ErrorEvent: TErrorEvent; var ErrorCode: Integer);
    procedure SocketRead(Sender: TObject; Socket: TCustomWinSocket);
    // Modbus Read function
    function ReadBuffer(var Buffer : array of Byte; Size : Byte; Address : Word) : Boolean;
  protected
    // Communication
    function SendData: Boolean;
    procedure ReadData;
    procedure SendAndReadData;
  public
    constructor Create();
    destructor Destroy; override;
    procedure ReConnectConnection;
    procedure ReCreateConnection;
    procedure SetCommunicationParam(APort:Cardinal;AID:Cardinal;ABaudRate:TBaudRate;ADataBits:TDataBits;
      AStopBits:TStopBits;AParity:TParity;ARtsControl:TRtsControl;AEnableDTROnOpen:Boolean;ATimeout:Cardinal;
      AIPAddress:String;AIPPort:Integer); override;
    procedure GetCommunicationParam(var APort:Cardinal;var AID:Cardinal;var ABaudRate:TBaudRate;var ADataBits:TDataBits;
      var AStopBits:TStopBits;var AParity:TParity;var ARtsControl:TRtsControl;var AEnableDTROnOpen:Boolean;var ATimeout:Cardinal;
      var AIPAddress:String; var AIPPort:Integer); override;
    function GetComPort: Cardinal; override;
    function Connect():Boolean; override;
    procedure Disconnect(); override;
    {MENU - Device no.3}
    function ReadMenuStr(Adr: Cardinal; var Str:String; Size: Integer):Boolean; override;
    function WriteMenuStr(Adr: Cardinal; Str: String; Size: Integer):Boolean; override;
    function ReadMenuValue(Adr: Cardinal; var Data:T4Byte):Boolean; override;
    function WriteMenuValue(Adr: Cardinal;Value:T4Byte):Boolean; override;

    function ReadRTC(Adr: Cardinal;var RetValue:Cardinal): Boolean; override;
    function WriteRTC(Adr: Cardinal;Value:Cardinal): Boolean; override;
    function ReadFirmNO(var firmwareNO:Cardinal):Boolean; override;
    function ReadOnline(Adr:Cardinal;var OnlineData:TOnline):Boolean ;
    {Kalibrace}
    function ReadCalibration(Bod: Cardinal;var  RetValue: Cardinal): boolean; override;
    function WriteCalibration(Bod: Cardinal; Value: Cardinal): boolean;override;
    function ReadMeasurment(Bod:Cardinal;var CalibData:Integer;var MeasurData:Integer): boolean; override;
    function WriteMeasurment(Bod:Cardinal;CalibData:Cardinal;MeasurData:Cardinal): boolean;override;
    {Statistika}
    function ReadFiveMins(var Buffer : array of Byte; M,D,H3 : Byte) : Boolean; override;
    function ReadHours(var Buffer : array of Byte; M,D : Byte) : Boolean; override;
    function ReadDays(var Buffer : array of Byte; M,D : Byte) : Boolean; override;
    function ReadMonth(var Buffer : array of Byte; M : Byte) : Boolean; override;
    function MemoryPresent(var Present: Boolean): Boolean; override;
    procedure SetStateToError; override;
    // datalogger
    function CalculateDataloggerFilteredRecords(var Running: Boolean; Filter: TMagE1DataloggerFilter) : Boolean; override;
  end;

  TDemoCommunication = class(TMyCommunication)
  private
    Port:Cardinal;
    ID:Cardinal;
    BaudRate:TBaudRate;
    DataBits:TDataBits;
    StopBits:TStopBits;
    Parity:TParity;
    Timeout:Cardinal;
    RtsControl:TRtsControl;
    EnableDTROnOpen:Boolean;
    DemoValues:TIniFile;
    FTimer:TTimer;
    Flow, Total, Batch: Integer;
    procedure OnTimeout(Sender: TObject);
  public
    constructor Create();
    destructor Destroy; override;
    procedure SetCommunicationParam(APort:Cardinal;AID:Cardinal;ABaudRate:TBaudRate;ADataBits:TDataBits;
      AStopBits:TStopBits;AParity:TParity;ARtsControl:TRtsControl;AEnableDTROnOpen:Boolean;ATimeout:Cardinal;
      AIPAddress:String;AIPPort:Integer); override;
    procedure GetCommunicationParam(var APort:Cardinal;var AID:Cardinal;var ABaudRate:TBaudRate;var ADataBits:TDataBits;
      var AStopBits:TStopBits;var AParity:TParity;var ARtsControl:TRtsControl;var AEnableDTROnOpen:Boolean;var ATimeout:Cardinal;
      var AIPAddress:String; var AIPPort:Integer); override;
    function GetComPort: Cardinal; override;
    function Connect():Boolean; override;
    procedure Disconnect(); override;
        {MENU - Device no.3}
    function ReadMenuStr(Adr: Cardinal; var Str:String; Size: Integer):Boolean; override;
    function WriteMenuStr(Adr: Cardinal; Str: String; Size: Integer):Boolean; override;
    function ReadMenuValue(Adr: Cardinal; var Data:T4Byte):Boolean; override;
    function WriteMenuValue(Adr: Cardinal;Value:T4Byte):Boolean; override;
    function ReadRTC(Adr: Cardinal;var RetValue:Cardinal): Boolean; override;
    function WriteRTC(Adr: Cardinal;Value:Cardinal): Boolean; override;
    function ReadFirmNO(var firmwareNO:Cardinal):Boolean; override;
    function GetLastError:integer;
    function ReadOnline(Adr:Cardinal;var OnlineData:TOnline):Boolean;
    function ReadOnlineB1(var OnlineData: TOnlineB1; ReadTotalValueAsDouble: Boolean = False): Boolean; override;
    {Kalibrace}
    function ReadCalibration(Bod: Cardinal;var  RetValue: Cardinal): boolean; override;
    function WriteCalibration(Bod: Cardinal; Value: Cardinal): boolean;override;
    function ReadMeasurment(Bod:Cardinal;var CalibData:Integer;var MeasurData:Integer): boolean; override;
    function WriteMeasurment(Bod:Cardinal;CalibData:Cardinal;MeasurData:Cardinal): boolean;override;
    {Statistika}
    function ReadFiveMins(var Buffer : array of Byte; M,D,H3 : Byte) : Boolean; override;
    function ReadHours(var Buffer : array of Byte; M,D : Byte) : Boolean; override;
    function ReadDays(var Buffer : array of Byte; M,D : Byte) : Boolean; override;
    function ReadMonth(var Buffer : array of Byte; M : Byte) : Boolean; override;
    function MemoryPresent(var Present: Boolean): Boolean; override;
    // datalogger
    function CalculateDataloggerFilteredRecords(var Running: Boolean; Filter: TMagE1DataloggerFilter) : Boolean; override;
    function ReadDataloggerNextFilteredRecordBlock(Records: TDataB1List; Filter: TMagE1DataloggerFilter): Boolean; override;
  end;

implementation

uses SyncObjs {$IFDEF LOG}, LogUnit{$ENDIF};
{ TMyCommPort }

//var CS: TCriticalSection;

{ TMyCommunication }

function TMyCommunication.IntTo4B(I:integer):T4Byte;
begin
  Result[3]:=Byte(I shr 24);
  Result[2]:=Byte(I shr 16);
  Result[1]:=Byte(I shr 8);
  Result[0]:=Byte(I);
end;

procedure TMyCommunication.SetStateToError;
begin
  // Do nothing
end;

function TMyCommunication.ReadRTCAdr(Adr: Cardinal;
  var RetValue: Cardinal): Boolean;
var AuxData: T4Byte;
begin
  Result := ReadMenuValue(Adr, AuxData);
  RetValue := Integer(AuxData);
end;

procedure TMyCommunication.WriteMenuValueNoResponse(Adr: Cardinal;
  Value: T4Byte);
begin
  raise Exception.Create('Not implemented!');
end;

function TMyCommunication.WriteRTCAdr(Adr, Value: Cardinal): boolean;
var AuxData: T4Byte;
begin
  AuxData := IntTo4B(Value);
  Result := WriteMenuValue(Adr, AuxData);
end;

function TMyCommunication.ReadRTCTime(var RetValue: Cardinal): Boolean;
begin
  Result := ReadRTCAdr(MODBUS_REGISTRY_ADDRESS_RTC_TIME, RetValue);


end;

function TMyCommunication.WriteRTCTime(Value: Cardinal): boolean;
begin
  Result := WriteRTCAdr(MODBUS_REGISTRY_ADDRESS_RTC_TIME, Value);
end;

function TMyCommunication.ReadRTCDate(var RetValue: Cardinal): Boolean;
begin
  Result := ReadRTCAdr(MODBUS_REGISTRY_ADDRESS_RTC_DATE, RetValue);
end;

function TMyCommunication.WriteRTCDate(Value: Cardinal): boolean;
begin
  Result := WriteRTCAdr(MODBUS_REGISTRY_ADDRESS_RTC_DATE, Value);
end;

procedure TMyCommunication.WriteReset;
var FourByte: T4Byte;
begin
  FourByte[0] := 1;
  FourByte[1] := 1;
  FourByte[2] := 1;
  FourByte[3] := 1;
  WriteMenuValueNoResponse(MODBUS_REGISTRY_ADDRESS_RESET, FourByte);
end;

function TMyCommunication.ReadOnlineB1(var OnlineData: TOnlineB1; ReadTotalValueAsDouble: Boolean): Boolean;
var Val4B: T4Byte;
    Ret: Boolean;
begin
  // Init
  Result := True;
  OnlineData.Flow := 0;
  OnlineData.Total := 0;
  OnlineData.Batch := 0;
  OnlineData.ErrorCode := 0;
  // Flow
  Ret := ReadMenuValue(B1_ONLINE_FLOW, Val4B);
  if Ret then begin
    OnlineData.Flow := Cardinal(Val4B) / 1000;
    // Flow SIGN
    Ret := ReadMenuValue(B1_ONLINE_FLOW_SIGN, Val4B);
    if Ret then begin
      if (Cardinal(Val4B) = 1) then OnlineData.Flow := - OnlineData.Flow;
    end else begin
      Result := False;
    end;
  end else begin
    Result := False;
  end;
  // Total
  Ret := ReadMenuValue(B1_ONLINE_TOTAL, Val4B);
  if Ret then begin
    if ReadTotalValueAsDouble then begin
      OnlineData.Total := Cardinal(Val4B) / 1000000.0;
    end else begin
      OnlineData.Total := Cardinal(Val4B) / 1000;
    end;
  end else begin
    Result := False;
  end;
  // Batch
  Ret := ReadMenuValue(B1_ONLINE_BATCH, Val4B);
  if Ret then begin
    if ReadTotalValueAsDouble then begin
      OnlineData.Batch := Cardinal(Val4B) / 1000000.0;
    end else begin
      OnlineData.Batch := Cardinal(Val4B) / 1000;
    end;
  end else begin
    Result := False;
  end;
  // Error
  Ret := ReadMenuValue(B1_ONLINE_ERROR, Val4B);
  if Ret then begin
    OnlineData.ErrorCode := Cardinal(Val4B);
  end else begin
    Result := False;
  end;
end;

function TMyCommunication.ReadDataloggerNextFilteredRecordBlock(Records: TDataB1List; Filter: TMagE1DataloggerFilter): Boolean;
begin
  raise Exception.Create('Not implemented!');
end;

function TMyCommunication.GetDataloggerNextFilteredRecordBlockByteSize(): Cardinal;
begin
  result := (self.FDataloggerFilteredRecordCount - self.FDataloggerFilteredRecordReadCount) * DATALOGGER_RECORD_SIZE;
  if result > DATALOGGER_BLOCK_SIZE_MAX then
    result := DATALOGGER_BLOCK_SIZE_MAX;
end;

(*
function TMyCommPort.GetFlags : Integer;
var
 temp : TDCB;
begin
 if (hCommFile > 0) then
  begin
   GetCommState(hCommFile,temp);
   result := temp.Flags;
  end
 else
   result := -1;
end;
*)

procedure TMyCommPort.CRTS(Read:boolean);
var Flag:integer;
 COMStateBuffer: PDCB;
begin
  if SwitchWait>0 then Sleep(SwitchWait);

   if Read then Flag:=$5091 else Flag:=$4091;
 //  SetMyCommState(28,9600,Flag,0,2048,512,8,0,0);
  GetMem(COMStateBuffer, sizeof(DCB));
  try
    GetCommState(hCommFile, COMStateBuffer^);
    COMStateBuffer.Flags:=Flag;
    SetCommState (hCommFile, COMStateBuffer^);
  finally
    FreeMem(COMStateBuffer, sizeof(DCB));
  end;

{
  if Read then
    dwFunc:=SETRTS
  else
    dwFunc:=CLRRTS;
  EscapeCommFunction(hCommFile,dwFunc);
}
end;


function TMyCommPort.COMSearch(pb:TGauge;AID:Cardinal): String;
var I:integer;
    FourByte : T4Byte;
begin
  Busy:=true;
  I:=1;
  Result:='noCOM';
  while Result='noCOM' do
  begin
    SetCommunicationParam(I,AID,br9600,db8BITS,sb1BITS,ptNONE,rcRTSEnable,true,500,'',0);
    if Connect() then
    begin
      Application.ProcessMessages;
      if not ReadMenuValue(34,FourByte) then
      begin
        CloseHandle(hCommFile);
        hCommFile:=INVALID_HANDLE_VALUE;
      end else
      begin
        CloseHandle(hCommFile);
        hCommFile:=INVALID_HANDLE_VALUE;
        Result:='COM' + IntToStr(I);
        Busy:=false;
        exit;
      end;
    end;
    inc(I);
    if I > 20 then break;
    pb.progress:=I*5;
    Application.ProcessMessages;
  end;
  Busy:=false;
end;

procedure TMyCommPort.SetMyCommState(DCBlength, BaudRate: Cardinal;
  Flags: Integer; wReserved, XonLim, XoffLim: Word; ByteSize, Parity,
  StopBits: Byte);
var COMStateBuffer: PDCB;
begin
  Busy:=true;
  GetMem(COMStateBuffer, sizeof(DCB));
  try
    GetCommState(hCommFile, COMStateBuffer^);
    COMStateBuffer.DCBlength:=DCBlength;
    COMStateBuffer.BaudRate:=BaudRate;
    COMStateBuffer.Flags:=Flags;
    COMStateBuffer.wReserved:=wReserved;
    COMStateBuffer.XonLim:=XonLim;
    COMStateBuffer.XoffLim:=XoffLim;
    COMStateBuffer.ByteSize:=ByteSize;
    COMStateBuffer.Parity:=Parity;
    COMStateBuffer.StopBits:=StopBits;

    SetCommState (hCommFile, COMStateBuffer^);
  finally
    FreeMem(COMStateBuffer, sizeof(DCB));
  end;
  Busy:=false;
end;

procedure TMyCommPort.SetMyTimeouts(ReadIntervalTimeout,
  ReadTotalTimeoutMultiplier, ReadTotalTimeoutConstant,
  WriteTotalTimeoutMultiplier, WriteTotalTimeoutConstant: Cardinal);
var TimeoutBuffer: PCOMMTIMEOUTS;
begin
  Busy:=true;
  {TimeOuts}
  GetMem(TimeoutBuffer, sizeof(COMMTIMEOUTS));
  try
    GetCommTimeouts (hCommFile, TimeoutBuffer^);
    TimeoutBuffer.ReadIntervalTimeout         := ReadIntervalTimeout;
    TimeoutBuffer.ReadTotalTimeoutMultiplier  := ReadTotalTimeoutMultiplier;
    TimeoutBuffer.ReadTotalTimeoutConstant    := ReadTotalTimeoutConstant;
    TimeoutBuffer.WriteTotalTimeoutMultiplier := WriteTotalTimeoutMultiplier;
    TimeoutBuffer.WriteTotalTimeoutConstant   := WriteTotalTimeoutConstant;
    SetCommTimeouts (hCommFile, TimeoutBuffer^);
  finally
    FreeMem(TimeoutBuffer, sizeof(COMMTIMEOUTS));
  end;
  Busy:=false;
end;

function TMyCommPort.Connect() : Boolean;
const
  COM_STR = '\\.\COM';
var lpdcb : _DCB;
begin
  Result:=False;

  if hCommFile<>INVALID_HANDLE_VALUE then
    CloseHandle(hCommFile);
  hCommFile := INVALID_HANDLE_VALUE;
  hCommFile := CreateFile(PChar(COM_STR+IntToStr(Port)),
                          GENERIC_READ or GENERIC_WRITE,
                          0,
                          nil,
                          OPEN_EXISTING,
                          FILE_ATTRIBUTE_NORMAL,
                          0);

  if hCommFile<>INVALID_HANDLE_VALUE then
  begin
    if GetCommState(hCommFile, lpdcb) then
    begin
      // DCBLength : 28 | BaudRate : 9600 | Flags : 21393 - XOn(256) a XOff(512)
      // wReserved : 0 | XonLim : 2048 | XoffLim : 512 | ByteSize : 8 | Parity : 0 | StopBits : 0
      SetMyCommState(28,9600,20649,0,2048,512,8,0,0);   //20625
      SetMyTimeouts(50,50,100,50,50);
      SetupComm(hCommFile,256,256);
      PurgeComm(hCommFile,PURGE_TXABORT or PURGE_RXABORT or PURGE_TXCLEAR or PURGE_RXCLEAR);
      Result:=True;
    end
    else
    begin
      CloseHandle(hCommFile);
      hCommFile:=INVALID_HANDLE_VALUE
    end;
  end;
end;

function TMyCommPort.ReadIf(ID:Cardinal; Count:Byte; Device:Byte;
Read:Byte; var Address:Cardinal; var S: TBuffer): Boolean;
var    Buff:TBuffer;
       i:Integer;
       ChkSum,RChkSum,RID,RAddress,Readed:Cardinal;
       ReadOK:Boolean;
       t1:DWORD;
begin
  result:=false;
  ReadOK:=false;
  t1 := GetTickCount;
  while (DWORD(GetTickCount-t1) < Timeout)and not ReadOK do
  begin
    if CountRX>=Count then
      ReadOK:=true;
  end;

  if not ReadOK then
    exit;

  ReadFile(hCommFile,Buff,Count+15,Readed,nil);
  if Count+15=Readed then
  begin
    ChkSum:=0;
    for i:=0 to Count-1 do
      ChkSum:=ChkSum+Cardinal(Buff[i+11]);
    RChkSum:=Cardinal(Buff[Readed-1]);
    RChkSum:=RChkSum or (Cardinal(Buff[Readed-2])shl 8);
    RChkSum:=RChkSum or (Cardinal(Buff[Readed-3])shl 16);
    RChkSum:=RChkSum or (Cardinal(Buff[Readed-4])shl 24);

    RID:=Cardinal(Buff[3]);
    RID:=RID or (Cardinal(Buff[2])shl 8);
    RID:=RID or (Cardinal(Buff[1])shl 16);
    RID:=RID or (Cardinal(Buff[0])shl 24);


    RAddress:=Cardinal(Buff[10]);
    RAddress:=RAddress or (Cardinal(Buff[9])shl 8);
    RAddress:=RAddress or (Cardinal(Buff[8])shl 16);

    if (ID=RID) and (Count=Cardinal(Buff[4])) and (Device=Cardinal(Buff[5]))
    and(Cardinal(Buff[6])=0)and (Read=Cardinal(Buff[7])){and(Address=RAddress)}then
    begin
      if RChkSum=ChkSum then
      begin
        for i:=0 to count-1 do
          S[i]:=Buff[i+11];
          Address:=RAddress;
        result:=true;
      end
      else
      begin
        result:=false;
//        ShowMessage('Jina Error');
      end;
    end
    else
    Result:=false;
  end;
end;

function TMyCommPort.ReadBuffer(var Buffer: array of Byte; Size, Address1,
  Address2, Address3: Byte): Boolean;






var J : Integer;
    S : TBuffer;
    Address:Cardinal;
//    Err:Integer;
begin
  if not IsConnect then
  begin
    Result:=False;
    exit;
  end;
  Result:=False;
  Address:=(Cardinal(Address1)shl 16) or (Cardinal(Address2)shl 8)or Cardinal(Address3);
  WriteChar(ID,Size,1,1,Address, S,0);
  if ReadIf(ID,Size,1,1,Address, S)then
  begin {Headers(11)+Value(Size)+CheckSum(4)}
    for J:=0 to Size-1 do Buffer[J]:=S[J];
    Result:=True;
  end
  else
  begin
    if Sync() then
      Result:=ReadBuffer(Buffer,Size,Address1,Address2,Address3);
  end;
end;


function TMyCommPort.ReadFirmNO(var firmwareNO:Cardinal):Boolean;
var S : TBuffer;
    FourByte: T4Byte;
    Err:integer;
    adr:Cardinal;
begin
  result:=false;
  Err:=0;
  Busy:=true;

  while (Err<MAX_ERROR) and (not Result) do
  begin
//    CRTS(false);
    inc(Err);
    WriteChar(ID,0,11,1,0, S,0);
//    CRTS(true);
    adr:=0;
    if (ReadIf(ID,4,11,1,adr, S)) then
    begin
      FourByte[0]:=S[3];
      FourByte[1]:=S[2];
      FourByte[2]:=S[1];
      FourByte[3]:=S[0];
      result:=true;
      firmwareNo:=Integer(FourByte);
    end
    else
    if ReadMenuValue(4*35, FourByte) then //pro FW=105
    begin
      if Integer(FourByte)=105 then
      begin
        result:=true;
        firmwareNo:=105;
      end;
    end
    else
      Sync;
  end;
  Busy:=false;
end;


function TMyCommPort.ReadRTC(Adr: Cardinal;var RetValue:Cardinal): Boolean;
var S : TBuffer;
    Err: Integer;
begin
  result:= false;
  Err:=0;
  Busy:=true;
  while (not result)and(Err<MAX_ERROR) do
  begin
    inc(Err);
    WriteChar(ID,0,5,1,adr, S,0);
    if ReadIf(ID,1,5,1,adr, S)then
    begin
      RetValue:=S[0];
      result:=true;
    end;
  end;
  Busy:=false;
end;

function TMyCommPort.WriteRTC(adr: cardinal; value:Cardinal): boolean;
var S : TBuffer;
    ReadOK:Boolean;
    Err:Integer;
begin
  Busy:=true;
  Err:=0;
  repeat
    S[0]:=value;
    WriteChar(ID,1,5,2,Adr, S,1);
    ReadOk:=ReadIf(ID,1,5,2,Adr,S);
    if not ReadOk then
    begin
      sync();
      Inc(Err);
    end;
  until (ReadOk) or (Err>=MAX_ERROR);
  result:=ReadOK;
  Busy:=false;
end;

function TMyCommPort.WriteCalibration(Adr: Cardinal; Value: Cardinal): boolean;
var S : TBuffer;
        FourByte:T4Byte;
        Err:integer;
begin
  result:=false;
  Err:=-1;
  Busy:=true;
  while (not result)and(Err<MAX_ERROR) do
  begin
    inc(Err);
    FourByte:=IntTo4B(Value);
    S[0]:=FourByte[3];
    S[1]:=FourByte[2];
    S[2]:=FourByte[1];
    S[3]:=FourByte[0];
    WriteChar(ID,4,7,2,Adr,S,4);
    if (ReadIf(ID,4,7,2,Adr,S)) then
      result:= true;
  end;
  if not result then
    sync;
  Busy:=false;
end;


function TMyCommPort.ReadCalibration(Bod: Cardinal;var  RetValue: Cardinal): boolean;
begin
  raise Exception.Create('Function disabled!');
  Result := False;
//  result:=ReadMenuValue(GetAddress(ProtokolType
//          ,MagX1CalibrationMeasurementIDX[Bod],Values),FourByte);
end;

function TMyCommPort.ReadMeasurment(Bod:Cardinal;var CalibData:Integer;var MeasurData:Integer): boolean;
begin
  raise Exception.Create('Function disabled!');
  Result := False;
//  result:=ReadMenuValue(GetAddress(ProtokolType
//          ,MagX1CalibrationMeasurementIDX[Bod],Values),FourByte);
end;


function TMyCommPort.WriteMeasurment(Bod:Cardinal;CalibData:Cardinal;MeasurData:Cardinal): boolean;
var S : TBuffer;
    FourByte:T4Byte;
    Adr:Cardinal;
    Err:integer;
begin
  result:=false;
  Err:=-1;
  Busy:=true;

  while (not result)and(Err<MAX_ERROR) do
  begin
    inc(Err);
    FourByte:=IntTo4B(CalibData);
    S[0]:=FourByte[3];
    S[1]:=FourByte[2];
    S[2]:=FourByte[1];
    S[3]:=FourByte[0];
    FourByte:=IntTo4B(MeasurData);
    S[4]:=FourByte[3];
    S[5]:=FourByte[2];
    S[6]:=FourByte[1];
    S[7]:=FourByte[0];
    Adr:=Bod+3;
    WriteChar(ID,8,7,2,Adr,S,8);
    if ReadIf(ID,8,7,2,Adr,S) then
      result:= true;
  end;
    Busy:=false;
    if not result then
      sync;
end;

function TMyCommPort.ReadOnline(Firm:Cardinal;var Adr:Cardinal;var OnlineData:TOnline):Boolean ;
var S : TBuffer;
    tmp: Cardinal;
begin
  Busy:=true;
  if Firm = 105 then
  begin
    if ReadIf(ID,13,9,1,Adr,S) then
    begin
    //Flow
      tmp:=S[3];
      tmp:=tmp or (Cardinal(S[2])shl 8);
      tmp:=tmp or (Cardinal(S[1])shl 16);
      tmp:=tmp or (Cardinal(S[0])shl 24);
      OnlineData.Flow:=tmp;
    //Volume
      tmp:=S[7];
      tmp:=tmp or (Cardinal(S[6])shl 8);
      tmp:=tmp or (Cardinal(S[5])shl 16);
      tmp:=tmp or (Cardinal(S[4])shl 24);
      OnlineData.Volume:=tmp;
    //Temp
      tmp:=S[11];
      tmp:=tmp or (Cardinal(S[10])shl 8);
      tmp:=tmp or (Cardinal(S[9])shl 16);
      tmp:=tmp or (Cardinal(S[8])shl 24);
      OnlineData.Temp:=tmp;
    //Jednotky
      OnlineData.FlowUnit:=TFlowUnit(S[12] and 7);
      OnlineData.VolumeUnit:=TVolumeUnit((S[12] shr 3)and 7);
      OnlineData.TempUnit:=TTempUnit((S[12] shr 6)and 3);
    //Vzdy stejnz format
      OnlineData.VolumeFormat:=6;
      OnlineData.FlowFormat:=3;
      OnlineData.TempFormat:=1;
      Result:=true;
    end
    else
      Result:=False;
  end
  else
  begin
    if ReadIf(ID,14,9,1,Adr,S) then
    begin
    //Flow
      tmp:=S[3];
      tmp:=tmp or (Cardinal(S[2])shl 8);
      tmp:=tmp or (Cardinal(S[1])shl 16);
      tmp:=tmp or (Cardinal(S[0])shl 24);
      OnlineData.Flow:=tmp;
    //Volume
      tmp:=S[7];
      tmp:=tmp or (Cardinal(S[6])shl 8);
      tmp:=tmp or (Cardinal(S[5])shl 16);
      tmp:=tmp or (Cardinal(S[4])shl 24);
      OnlineData.Volume:=tmp;
    //Temp
      tmp:=S[12];
      tmp:=tmp or (Cardinal(S[11])shl 8);
      tmp:=tmp or (Cardinal(S[10])shl 16);
      tmp:=tmp or (Cardinal(S[9])shl 24);
      OnlineData.Temp:=tmp;
    //Jednotky
      OnlineData.FlowUnit:=TFlowUnit(S[13] and 7);
      OnlineData.VolumeUnit:=TVolumeUnit((S[13] shr 3)and 7);
      OnlineData.TempUnit:=TTempUnit((S[13] shr 6)and 3);
    //Format
      OnlineData.VolumeFormat:=S[8];
    //ostatni format vzdy stejny
      OnlineData.FlowFormat:=3;
      OnlineData.TempFormat:=1;
      Result:=true;
    end
    else
      Result:=False;
  end;
  Busy:=false;
end;



function TMyCommPort.WriteOnline(adr: Integer): boolean;
var S : TBuffer;
begin
  Busy:=true;
  result:=WriteChar(ID,0,9,1,adr,S,0);
  Busy:=false;
end;

function TMyCommPort.Sync(): Boolean;
var i:integer;
    Buff:TBuffer;
    Res:Cardinal;
    LPDWORD:Cardinal;
begin
  Disconnect();
  Connect();

  CRTS(false);
  for i:=0 to 20 do
    Buff[i]:=$FF;
  Result:=WriteFile(hCommFile,Buff,20,Res,nil);
  LPDWORD:=CE_BREAK or CE_FRAME or CE_IOE or CE_MODE or CE_OVERRUN
  or CE_RXOVER or CE_RXPARITY or CE_TXFULL;
  ClearCommError(hCommFile,LPDWORD, nil);
  FlushBuffers(true,true);
  CRTS(true);
end;


function TMyCommPort.WriteChar(ID:Cardinal; Size:Byte; Device:Byte;
Read:Byte; Address:Cardinal; var S: TBuffer; Count:Byte):Boolean;
var FourByte:T4Byte;
    Buff:TBuffer;
    i:Integer;
    ChkSum,Res:Cardinal;
    Len,Writen:cardinal;
begin
  ChkSum:=0;
  FourByte := IntTo4B(ID);
  Buff[0]:=FourByte[3]; { ID }
  Buff[1]:=FourByte[2]; { ID }
  Buff[2]:=FourByte[1]; { ID }
  Buff[3]:=FourByte[0]; { ID }
  Buff[4]:=Size;       //Size
  Buff[5]:=Device;     //Device
  Buff[6]:=0;         //ERR
  Buff[7]:=Read;
  Buff[8]:=Byte(Address shr 16);
  Buff[9]:=Byte(Address shr 8);
  Buff[10]:=Byte(Address);
  for i:=0 to Count-1 do
  begin
    Buff[11+i]:=S[i];
    ChkSum:=ChkSum+S[i]
  end;
  Buff[Count+11]:=Byte(ChkSum shr 24);
  Buff[Count+11+1]:=Byte(ChkSum shr 16);
  Buff[Count+11+2]:=Byte(ChkSum shr 8);
  Buff[Count+11+3]:=Byte(ChkSum);

  //EscapeCommFunction(hCommFile,CLRRTS);
  if Count>0 then
    Len:=Count+15
  else
    len:=11;

  CRTS(false);
  Writen:=0;
  for i:=0 to Len-1 do
  begin
    WriteFile(hCommFile,Buff[i],1,Res,nil);
    Writen:=Writen+Res;
  end;
  result:=Writen=Len;
  CRTS(true);
end;

function TMyCommPort.ReadMenuValue(Adr: Cardinal;var Data:T4Byte):Boolean;
var S : TBuffer;
    Err: Integer;
    ReadOk:Boolean;
begin
  Busy:=true;
  if not IsConnect then
  begin
    Result:=false;
    Busy:=false;
    exit;
  end;

  Err := 0;
  repeat
    WriteChar(ID,0,3,1,Adr,S,0);
    ReadOk:=ReadIf(ID,4,3,1,Adr,S);
    if ReadOk then begin
      Data[0]:=S[3];
      Data[1]:=S[2];
      Data[2]:=S[1];
      Data[3]:=S[0];
    end
    else
    begin
      sync();
      Inc(Err);
    end;
  until (ReadOk) or (Err>=MAX_ERROR);
  result:=ReadOK;
  Busy:=false;
end;

function TMyCommPort.WriteMenuValue(Adr: Cardinal;Value:T4Byte):Boolean;
var S : TBuffer;
    Err: Integer;
begin
  Busy:=true;
  result:=false;
  Err:=-1;
  { Header 0001X203AAADDDD | X - SIZE | AAA - Address | DDDD - Data }
  repeat
    Inc(Err);
    S[0]:=Value[3];
    S[1]:=Value[2];
    S[2]:=Value[1];
    S[3]:=Value[0];
    WriteChar(ID,4,3,2,Adr,S,4);
//dodelat podminku if Value = S
    if ReadIf(ID,4,3,2,Adr,S) then begin
      Result := true;
    end
    else
      sync();
  until (Result=true) or (Err>=MAX_ERROR);
  Busy:=false;
end;

function TMyCommPort.ReadFiveMins(var Buffer: array of Byte; M, D, H3: Byte
): Boolean;
const Size = 252;
var Page : Word;
begin
  Busy:=true;
  Page:=300*M+(D-1)*8+(H3 div 3);
  Result:=ReadBuffer(Buffer,Size,Page shr 7,Page shl 1,0);
  Busy:=false;

end;

function TMyCommPort.ReadHours(var Buffer: array of Byte; M, D: Byte): Boolean;
const Size = 240;

var Page : Word;
begin
  Busy:=true;
  Page:=300*M+247+D;
  Result:=ReadBuffer(Buffer,Size,Page shr 7,Page shl 1,0);
  Busy:=False;
end;

function TMyCommPort.ReadDays(var Buffer: array of Byte; M, D: Byte): Boolean;
const Size16 = 208;
      Size15 = 195;
var Page : Word;
//    Day  : Word;
begin
  Busy:=true;












// rozdeleno na pul, protze Max Size je jen 255Byte
//Day:=(D-1)*12 + (D-1-(((D-1)div 16)*16)*13);
  if D<=16 then begin
    {1-16}
    Page:=300*M + 279;
    Result:=ReadBuffer(Buffer,Size16,Page shr 7,Page shl 1,0);
  end else begin
    {17-31}
   Page:=300*M+280;
//   Page:=(300*M)+(D-1)div 16 + 279;
   Result:=ReadBuffer(Buffer,Size15,Page shr 7,Page shl 1,0);
  end;
  Busy:=false;
end;


//vstup: Buffer o velikosti constSize, Mesic, Pocet neuspesnzch pokusu.
function TMyCommPort.ReadMonth(var Buffer: array of Byte; M: Byte): Boolean;
const Size = 21;
var Page : Word;
begin
  Busy:=true;
  Page:=300*M+281;
  Result:=ReadBuffer(Buffer,Size,Page shr 7,Page shl 1,0);
  Busy:=false;
end;

constructor TMyCommPort.Create();
begin
  inherited Create;
  ProtokolType:= ptMagX1;
  hCommFile:=INVALID_HANDLE_VALUE;
  Busy:=false;
end;

destructor TMyCommPort.Destroy;
begin
  Disconnect;
  inherited Destroy;
end;

procedure TMyCommPort.Disconnect();
begin
  if IsConnect then
  begin
    CloseHandle(hCommFile);
    hCommFile:=INVALID_HANDLE_VALUE;
    Busy:=false;
  end;
end;

function TMyCommPort.IsConnect:Boolean;
begin
  if hCommFile<>INVALID_HANDLE_VALUE then
    Result:=true
  else
    Result:=false;
end;

function TMyCommPort.MemoryPresent(var Present: Boolean): Boolean;
var Buff:byte;
begin
  Busy:=true;
  Result:=ReadBuffer(Buff,1,0,0,0);
  Present:=Buff=1;
  Busy:=false;
end;

function TMyCommPort.CalculateDataloggerFilteredRecords(var Running: Boolean; Filter: TMagE1DataloggerFilter) : Boolean;
begin
  result := true;
end;

procedure TMyCommPort.FlushBuffers( inBuf, outBuf: boolean );
var dwAction: DWORD;
begin
  if not IsConnect or Busy then
    exit;
  // Flush the incoming data buffer
  dwAction := 0;
  if outBuf then
    dwAction := dwAction or PURGE_TXABORT or PURGE_TXCLEAR;
  if inBuf then
    dwAction := dwAction or PURGE_RXABORT or PURGE_RXCLEAR;
  PurgeComm( hCommFile, dwAction );
end;

function TMyCommPort.CountRX: integer;
var stat: TCOMSTAT;
    errs: DWORD;
begin
  // Do nothing if port has not been opened
  Result := -1;
  if not IsConnect then
    exit;
  // Get count
  ClearCommError( hCommFile, errs, @stat );
  Result := stat.cbInQue;
end;

procedure TMyCommPort.SetCommunicationParam(APort:Cardinal;AID:Cardinal;ABaudRate:TBaudRate;ADataBits:TDataBits;
    AStopBits:TStopBits;AParity:TParity;ARtsControl:TRtsControl;AEnableDTROnOpen:Boolean;ATimeout:Cardinal;
    AIPAddress:String;AIPPort:Integer);
begin
  Port:=APort;
  ID:=AID;
  BaudRate:=ABaudRate;
  DataBits:=ADataBits;
  StopBits:=AStopBits;
  Parity:=AParity;
  Timeout:=ATimeout;
  RtsControl:=ARtsControl;
  EnableDTROnOpen:=AEnableDTROnOpen;
end;

procedure TMyCommPort.GetCommunicationParam(var APort:Cardinal;var AID:Cardinal;var ABaudRate:TBaudRate;var ADataBits:TDataBits;
      var AStopBits:TStopBits;var AParity:TParity;var ARtsControl:TRtsControl;var AEnableDTROnOpen:Boolean;var ATimeout:Cardinal;
      var AIPAddress:String; var AIPPort:Integer);
begin
  APort:=Port;
  AID:=ID;
  ABaudRate:=BaudRate;
  ADataBits:=DataBits;
  AStopBits:=StopBits;
  AParity:=Parity;
  ATimeout:=Timeout;
  ARtsControl:=RtsControl;
  AEnableDTROnOpen:=EnableDTROnOpen;
end;

function TMyCommPort.GetComPort: Cardinal;
begin
  Result := Port;
end;

{----------------------------------------------------------------
------------------------- MODBUS --------------------------------
----------------------------------------------------------------}

constructor TMyModbus.Create();
begin
  inherited;
  ProtokolType:=ptModbus;
  ModbusM:=TModbusM.Create(nil);
  ResponseFlag:=rnNONE;
  ModbusM.OnError:=ModbusMError;
  ModbusM.OnResponseReady:=ModbusMResponseReady;
end;

destructor TMyModbus.Destroy();
begin
  ModbusM.Free;
  inherited;
end;

procedure TMyModbus.SetCommunicationParam(APort:Cardinal;AID:Cardinal;ABaudRate:TBaudRate;ADataBits:TDataBits;
      AStopBits:TStopBits;AParity:TParity;ARtsControl:TRtsControl;AEnableDTROnOpen:Boolean;ATimeout:Cardinal;
      AIPAddress:String;AIPPort:Integer);
begin
  with ModbusM do
  begin
    ComPort:=APort;
    SlaveId:=AID;
    ComPortSpeed:=TComPortBaudRate(ABaudRate);
    ComPortDataBits:=TComPortDataBits(ADataBits);
    ComPortStopBits:=TComPortStopBits(AStopBits);
    ComPortParity:=TComPortParity(AParity);
    ComPortRtsControl := TComPortRtsControl(ARtsControl);
    EnableDTROnOpen:=AEnableDTROnOpen;
    Timeout:=ATimeout;
  end;
end;

procedure TMyModbus.GetCommunicationParam(var APort:Cardinal;var AID:Cardinal;var ABaudRate:TBaudRate;var ADataBits:TDataBits;
      var AStopBits:TStopBits;var AParity:TParity;var ARtsControl:TRtsControl;var AEnableDTROnOpen:Boolean;var ATimeout:Cardinal;
      var AIPAddress:String; var AIPPort:Integer);
begin
  with ModbusM do
  begin
    APort:=ComPort;
    AID:=SlaveId;
    TComPortBaudRate(ABaudRate):=ComPortSpeed;
    TComPortDataBits(ADataBits):=ComPortDataBits;
    TComPortStopBits(AStopBits):=ComPortStopBits;
    TComPortParity(AParity):=ComPortParity;
    TComPortRtsControl(ARtsControl):=ComPortRtsControl;
    AEnableDTROnOpen:=EnableDTROnOpen;
    ATimeout:=Timeout;
  end;
end;

function TMyModbus.GetComPort: Cardinal;
begin
  Result := ModbusM.ComPort;
end;

function TMyModbus.Connect():Boolean;
begin
  Result:=ModbusM.Connect();
end;

procedure TMyModbus.Disconnect();
begin
  ModbusM.Disconnect();
end;

function TMyModbus.ReadFirmNO(var firmwareNO:Cardinal):Boolean;
var Buff:TDataByte;
    Err:Integer;
begin
  ModbusM.FunctionCode:=3;      //cteni Holding Registru
  ModbusM.Offset:= MODBUS_REGISTRY_ADDRESS_FIRMWARE;
  ModbusM.Quantity:=2;
  Err:=0;
  SetLength(Buff, 0);
  Result:=false;
  While((Err<MAX_ERROR)and(Result=false)) do
  begin
    ResponseFlag:=rnWait;
    ModbusM.Query;
    while ResponseFlag=rnWait do Application.ProcessMessages;
    if ResponseFlag=rnOk then
    begin
      Buff:=ModbusM.ReadValues;
      firmwareNO:=Cardinal(Buff[2])shl 24;
      firmwareNO:=firmwareNO or(Cardinal(Buff[3])shl 16);
      firmwareNO:=firmwareNO or(Cardinal(Buff[0])shl 8);
      firmwareNO:=firmwareNO or(Buff[1]);
      Result:=true;
    end
    else
    begin
      Inc(Err);
      sleep(50);
    end;
  end;
  ResponseFlag:=rnNONE;
end;

procedure TMyModbus.ModbusMError(Sender: TObject; const ErrorMsg: String);
begin
  ResponseFlag:=rnError;
end;

procedure TMyModbus.ModbusMResponseReady(Sender: TObject);
begin
  ResponseFlag:=rnOK;
end;

function TMyModbus.ReadMenuValue(Adr: Cardinal;var Data:T4Byte):Boolean;
var Buff:TDataByte;
    Err:Integer;
begin
  ModbusM.FunctionCode:=3;      //cteni Holding Registru
  ModbusM.Offset:=Adr;
  ModbusM.Quantity:=2;
  Err:=0;
  SetLength(Buff, 0);
  Result:=false;
  While((Err<MAX_ERROR)and(Result=false)) do
  begin
    ResponseFlag:=rnWait;
    ModbusM.Query;
    while ResponseFlag=rnWait do
      Application.ProcessMessages;
    if ResponseFlag=rnOk then
    begin
      Buff:=ModbusM.ReadValues;
      Data[3]:=Buff[2];
      Data[2]:=Buff[3];
      Data[1]:=Buff[0];
      Data[0]:=Buff[1];
      Result:=true;
    end
    else
    begin
      Inc(Err);
      sleep(50);
    end;
  end;
  ResponseFlag:=rnNONE;
end;

function TMyModbus.WriteMenuValue(Adr: Cardinal; Value:T4Byte):Boolean;
var Buff:TDataByte;
    Err:Integer;
begin
  ModbusM.FunctionCode:=16;
  ModbusM.Offset:=Adr;
  ModbusM.Quantity:=2;
  Err:=0;
  Result:=false;
  While((Err<MAX_ERROR)and(Result=false)) do
  begin
    SetLength(Buff,4);
    Buff[0]:=Value[1];
    Buff[1]:=Value[0];
    Buff[2]:=Value[3];
    Buff[3]:=Value[2];
    ModbusM.WriteValues:=Buff;
    ResponseFlag:=rnWait;
    ModbusM.Query;
    while ResponseFlag=rnWait do
      Application.ProcessMessages;
    if ResponseFlag=rnOk then
    begin
      Result:=true;
    end
    else
    begin
      Inc(Err);
      sleep(50);
    end;
  end;
  ResponseFlag:=rnNONE;
end;

procedure TMyModbus.WriteMenuValueNoResponse(Adr: Cardinal; Value: T4Byte);
var Buff:TDataByte;
begin
  ModbusM.FunctionCode:=16;
  ModbusM.Offset:=Adr;
  ModbusM.Quantity:=2;
  SetLength(Buff,4);
  Buff[0]:=Value[1];
  Buff[1]:=Value[0];
  Buff[2]:=Value[3];
  Buff[3]:=Value[2];
  ModbusM.WriteValues:=Buff;
  ModbusM.Query;














  ResponseFlag:=rnNONE;
end;

function TMyModbus.GetLastError:integer;
begin
  result:=ModbusM.Error;











end;






function TMyModbus.ReadRTC(Adr: Cardinal;var RetValue:Cardinal): Boolean;
var DTAdr:Cardinal;
    FourByte:T4Byte;
begin
  result:=false;
  case Adr of
  1:
  begin
    result:=true;        //sec
    RetValue:=0;
    exit;
  end;
  2: DTAdr:= 2085;           //min
  3: DTAdr:= 2083;          //hod
  4: DTAdr:= 2087;          //den
  5: DTAdr:= 2089;          //mesic
  6: DTAdr:= 2091;          //rok
  0: DTAdr:= 1017;          //pritomnost
  else
    exit;
  end;
  result:=ReadMenuValue(DTAdr,FourByte);
  RetValue:=Integer(FourByte);
end;

function TMyModbus.WriteRTC(Adr: Cardinal;Value:Cardinal): Boolean;
var DTAdr:Cardinal;
    FourByte:T4Byte;
begin
  result:=false;
  case Adr of
  1:
  begin
    result:=true;        //sec
    exit;
  end;
  2: DTAdr:= 2085;           //min
  3: DTAdr:= 2083;          //hod
  4: DTAdr:= 2087;          //den
  5: DTAdr:= 2089;          //mesic
  6: DTAdr:= 2091;          //rok
  0: DTAdr:= 2093;          //pritomnost
  else
    exit;
  end;
  result:=WriteMenuValue(DTAdr,FourByte);
end;

function TMyModbus.ReadOnline(Adr:Cardinal;var OnlineData:TOnline):Boolean ;
var Buff:TDataByte;
    tmp:Cardinal;
    Err:Integer;
    Volume:Cardinal;
begin
  if not Adr in[1..5] then
  begin
    result:=false;
    exit;
  end;

  ModbusM.FunctionCode:=3;      //cteni Holding Registru
  ModbusM.Offset:=99;           //Adresa Online
  ModbusM.Quantity:=26;
  Err:=0;
  SetLength(Buff, 0);
  Result:=false;
  While((Err<MAX_ERROR)and(Result=false)) do
  begin
    ResponseFlag:=rnWait;
    ModbusM.Query;
    while ResponseFlag=rnWait do
      Application.ProcessMessages;
    if ResponseFlag=rnOk then
    begin
      Buff:=ModbusM.ReadValues;
      OnlineData.Flow:=Buff[1];
      OnlineData.Flow:=OnlineData.Flow or (Cardinal(Buff[0])shl 8);
      OnlineData.Flow:=OnlineData.Flow or (Cardinal(Buff[3])shl 16);
      OnlineData.Flow:=OnlineData.Flow or (Cardinal(Buff[2])shl 24);

      if Adr=5 then
      begin
        OnlineData.Volume:=0;
      end
      else
      begin
        Volume:=Buff[Adr*8-4+1];
        Volume:=Volume or (Cardinal(Buff[Adr*8-4+0])shl 8);
        Volume:=Volume or (Cardinal(Buff[Adr*8-4+3])shl 16);
        Volume:=Volume or (Cardinal(Buff[Adr*8-4+2])shl 24);

        tmp:=Buff[Adr*8+1];
        tmp:=tmp or (Cardinal(Buff[Adr*8])shl 8);
        tmp:=tmp or (Cardinal(Buff[Adr*8+3])shl 16);
        tmp:=tmp or (Cardinal(Buff[Adr*8+2])shl 24);

        OnlineData.Volume:=1000.0*Volume+(tmp div 1000);
      end;

      OnlineData.Temp:=Buff[36+1];
      OnlineData.Temp:=OnlineData.Temp or (Cardinal(Buff[36+0])shl 8);
      OnlineData.Temp:=OnlineData.Temp or (Cardinal(Buff[36+3])shl 16);
      OnlineData.Temp:=OnlineData.Temp or (Cardinal(Buff[36+2])shl 24);

      tmp:=Buff[40+1];
      tmp:=tmp or (Cardinal(Buff[40+0])shl 8);
      tmp:=tmp or (Cardinal(Buff[40+3])shl 16);
      tmp:=tmp or (Cardinal(Buff[40+2])shl 24);
      OnlineData.FlowUnit:=TFlowUnit(tmp);

      tmp:=Buff[44+1];
      tmp:=tmp or (Cardinal(Buff[44+0])shl 8);
      tmp:=tmp or (Cardinal(Buff[44+3])shl 16);
      tmp:=tmp or (Cardinal(Buff[44+2])shl 24);
      OnlineData.VolumeUnit:=TVolumeUnit(tmp);

      tmp:=Buff[48+1];
      tmp:=tmp or (Cardinal(Buff[48+0])shl 8);
      tmp:=tmp or (Cardinal(Buff[48+3])shl 16);
      tmp:=tmp or (Cardinal(Buff[48+2])shl 24);
      OnlineData.TempUnit:=TTempUnit(tmp);

      OnlineData.FlowFormat:=3;
      OnlineData.TempFormat:=1;
      OnlineData.VolumeFormat:=3;

      Result:=true;
    end
    else
    begin
      Inc(Err);
      sleep(50);
    end;
   end;
  ResponseFlag:=rnNONE;
end;

function TMyModbus.ReadDataloggerNextFilteredRecordBlock(Records: TDataB1List; Filter: TMagE1DataloggerFilter): Boolean;
var ABlockBuffer: array[0 .. DATALOGGER_BLOCK_SIZE_MAX - 1] of Byte;
    AFourByte: T4Byte;
    //ATwoByte: T2Byte;
    ADataRecordB1: TDataRecordB1;
    I: Integer;
    NewItem: TDataB1;
    BlockByteSize: Cardinal;
    BlockRecordCount: Cardinal;
    NextBaseAddress: Cardinal;
    RecordModbusAddress: Word;
    DateTime: TDateTime;
begin
  Result := True;
  if DataloggerFilteredRecordReadCount < DataloggerFilteredRecordCount then begin
    BlockByteSize := GetDataloggerNextFilteredRecordBlockByteSize();
    BlockRecordCount := BlockByteSize div DATALOGGER_RECORD_SIZE;
    RecordModbusAddress := GetDataloggerNextFilteredRecordModbusAddress();
    NextBaseAddress := GetDataloggerNextFilteredRecordBaseAddress();
    if NextBaseAddress <> FDataloggerBaseAddress then begin
      if WriteDataloggerBaseAddressRegister(NextBaseAddress) then begin
        FDataloggerBaseAddress := NextBaseAddress;
      end else begin
        Result := False;
      end;
    end;
    if Result and ReadBuffer(ABlockBuffer, BlockByteSize div 2, RecordModbusAddress) then begin
      for I := 0 to BlockRecordCount - 1 do begin
        AFourByte[3] := ABlockBuffer[(I * DATALOGGER_RECORD_SIZE) + 3];
        AFourByte[2] := ABlockBuffer[(I * DATALOGGER_RECORD_SIZE) + 2];
        AFourByte[1] := ABlockBuffer[(I * DATALOGGER_RECORD_SIZE) + 1];
        AFourByte[0] := ABlockBuffer[(I * DATALOGGER_RECORD_SIZE) + 0];
        ADataRecordB1.DateTime := Cardinal(AFourByte);
        AFourByte[3] := ABlockBuffer[(I * DATALOGGER_RECORD_SIZE) + 4 + 3];
        AFourByte[2] := ABlockBuffer[(I * DATALOGGER_RECORD_SIZE) + 4 + 2];
        AFourByte[1] := ABlockBuffer[(I * DATALOGGER_RECORD_SIZE) + 4 + 1];
        AFourByte[0] := ABlockBuffer[(I * DATALOGGER_RECORD_SIZE) + 4 + 0];
        ADataRecordB1.Total := Single(AFourByte);

        if GetDateTimeFromHex(ADataRecordB1.DateTime, DateTime) then begin
          if Filter.IsRecordValid(DateTime, Records.Count) then begin
            NewItem := TDataB1.Create;
            NewItem.DateTime := DateTime;
            NewItem.Total := ADataRecordB1.Total;
            Records.Add(NewItem);
          end;
        end;
      end;
    end else begin
      Result := False;
    end;
    Inc(FDataloggerFilteredRecordReadCount, BlockRecordCount);
  end;
end;

function TMyModbus.WriteCalibration(Bod: Cardinal; Value: Cardinal): boolean;
var FourByte:T4Byte;
begin
  if not (Bod in [1..3]) then
  begin
    result:=false;
    exit;
  end;
  Integer(FourByte):=Value;
  result:=WriteMenuValue(ModbusCalibrationDataAdr[Bod],FourByte);
end;

function TMyModbus.WriteMeasurment(Bod:Cardinal;CalibData:Cardinal;MeasurData:Cardinal): boolean;
var FourByte: T4Byte;
begin
  if not (Bod in [1..3]) then
  begin
    result:=false;
    exit;
  end;
  Integer(FourByte):=CalibData;
  Result:=WriteMenuValue(ModbusCalibrationDataAdr[Bod],FourByte);
  if Result then
  begin
    Cardinal(FourByte):=MeasurData;
    Result:=WriteMenuValue(ModbusMeasurementDataAdr[Bod],FourByte);
  end;
end;

function TMyModbus.ReadCalibration(Bod: Cardinal;var  RetValue: Cardinal): boolean;
var FourByte:T4Byte;
begin
  if not (Bod in [1..3]) then
  begin
    result:=false;
    exit;
  end;
  result:=ReadMenuValue(ModbusCalibrationDataAdr[Bod],FourByte);
  RetValue:=Cardinal(FourByte);
end;

function TMyModbus.ReadMeasurment(Bod:Cardinal;var CalibData:Integer;var MeasurData:Integer): boolean;
var FourByte: T4Byte;
begin
  if not (Bod in [1..3]) then
  begin
    result:=false;
    exit;
  end;
  Result:=ReadMenuValue(ModbusCalibrationDataAdr[Bod],FourByte);
  CalibData:=Integer(FourByte);
  if Result then
  begin
    Result:=ReadMenuValue(ModbusMeasurementDataAdr[Bod],FourByte);
    MeasurData:=Cardinal(FourByte);
  end;
end;

function TMyModbus.ReadBuffer(var Buffer : array of Byte; Size : Byte; Address : Word) : Boolean;
var Err:Cardinal;
i:Integer;
begin
  ModbusM.FunctionCode:=3;      //cteni Holding Registru
  ModbusM.Offset:=Address;
  ModbusM.Quantity:=Size;
  Err:=0;
  Result:=false;
  While((Err<MAX_ERROR)and(Result=false)) do
  begin
    ResponseFlag:=rnWait;
    ModbusM.Query;
    while ResponseFlag=rnWait do
      Application.ProcessMessages;
    if ResponseFlag=rnOk then
    begin
      for i:=0 to Sizeof(Buffer)-1 do begin
        Buffer[i]:=ModbusM.ReadValues[i];
      end;
      Result:=true;
    end
    else
    begin
      Inc(Err);
      Sleep(1);
    end;
  end;
  ResponseFlag:=rnNONE;
end;


function TMyModbus.ReadMonth(var Buffer : array of Byte; M : Byte) : Boolean;
//const Size = 21;
const Size = 11;
var Page : Word;
    FourByte:T4Byte;
begin
  Page:=300*M+281;
  Cardinal(FourByte):=Page;
  Result:=WriteMenuValue(49997,FourByte);
  if Result then
  begin
    Result:=ReadBuffer(Buffer, Size,49999+0);
  end;
end;

function TMyModbus.ReadDays(var Buffer: array of Byte; M, D: Byte) : Boolean;
//const Size16 = 208;
//      Size15 = 195;
const Size16 = 104;
      Size15 = 98;
var Page : Word;
    FourByte:T4Byte;
begin
// rozdeleno na pul, protze Max Size je jen 255Byte
//Day:=(D-1)*12 + (D-1-(((D-1)div 16)*16)*13);

  if D<=16 then
  begin
    Page:=300*M + 279;
    Cardinal(FourByte):=Page;
    Result:=WriteMenuValue(49997,FourByte);
    if Result then
    begin
      {1-16}
      Result:=ReadBuffer(Buffer,Size16,49999+0);
    end;
  end
  else
  begin
    {17-31}
    Page:=300*M+280;
//   Page:=(300*M)+(D-1)div 16 + 279;
    Cardinal(FourByte):=Page;
    Result:=WriteMenuValue(49997,FourByte);
    if Result then
    begin
      Result:=ReadBuffer(Buffer,Size15,49999+0);
    end;
  end;
end;

function TMyModbus.ReadFiveMins(var Buffer : array of Byte; M,D,H3 : Byte) : Boolean;
//const Size = 252;
//const Size = 126;
const Size12 = 64;
      Size22 = 62;
var Page : Word;
    FourByte:T4Byte;
    tmpBuff : Array[0..125] of Byte;
    i:Integer;
begin
  Page:=300*M+(D-1)*8+(H3 div 3);
  Cardinal(FourByte):=Page;
  Result:=WriteMenuValue(49997,FourByte);
  if Result then
  begin
    Result:=ReadBuffer(tmpBuff,Size12,49999+0);
    for i:=0 to Size12*2-1 do
      Buffer[i]:=tmpBuff[i];
    if Result then
    begin
      Result:=ReadBuffer(tmpBuff,Size22,49999+Size12);
      for i:=0 to Size22*2-1 do
        Buffer[Size12*2+i]:=tmpBuff[i];
    end;
  end;
end;

function TMyModbus.ReadHours(var Buffer : array of Byte; M,D : Byte) : Boolean;
//const Size = 240;
const Size = 120;
var Page : Word;
    FourByte:T4Byte;
begin
  Page:=300*M+247+D;
  Cardinal(FourByte):=Page;
  Result:=WriteMenuValue(49997,FourByte);
  if Result then
  begin
    Result:=ReadBuffer(Buffer,Size,49999+0);
  end;
end;

function TMyModbus.MemoryPresent(var Present: Boolean): Boolean;
var FourByte:T4Byte;
begin
  result:=ReadMenuValue(1019, FourByte);
  Present:=Integer(FourByte)=1;
end;

function TMyModbus.ReadDataloggerBaseAddressRegister(var BaseAdrReg : Cardinal) : Boolean;
var FourByte: T4Byte;
begin
  result := ReadMenuValue(DATALOGGER_BASE_ADDRESS_ADDRESS, FourByte);
  BaseAdrReg := Cardinal(FourByte);
end;

function TMyModbus.WriteDataloggerBaseAddressRegister(BaseAdrReg : Cardinal) : Boolean;
var FourByte: T4Byte;
begin
  Cardinal(FourByte) := BaseAdrReg;
  result := WriteMenuValue(DATALOGGER_BASE_ADDRESS_ADDRESS, FourByte);
end;

function TMyModbus.ReadDataloggerMemorySizeRegister(var MemorySizeReg : Cardinal) : Boolean;
var FourByte: T4Byte;
begin
  result := ReadMenuValue(DATALOGGER_MEMORY_SIZE_ADDRESS, FourByte);
  MemorySizeReg := Cardinal(FourByte);
end;

function TMyModbus.ReadDataloggerRecordDateTimePack(RecordAddress : Cardinal; var RecordDateTimePack : T4Byte) : Boolean;
begin
  result := false;
  if WriteDataloggerBaseAddressRegister(RecordAddress) then begin
    FDataloggerBaseAddress := RecordAddress;
    if ReadBuffer(RecordDateTimePack, 2, DATALOGGER_MODBUS_SPACE_MIN - 1) then begin
      result := true;
    end;
  end;
end;

function TMyModbus.GetDataloggerNextFilteredRecordAbsoluteAddress(): Cardinal;
begin
  result := FDataloggerFilteredFirstRecordAddress + (FDataloggerFilteredRecordReadCount * DATALOGGER_RECORD_SIZE);
  if FDataloggerMemorySize > 0 then
    result := result mod FDataloggerMemorySize;
end;

function TMyModbus.GetDataloggerNextFilteredRecordBaseAddress(): Cardinal;
var AbsoluteAddress, AddressSpace: Cardinal;
begin
  AbsoluteAddress := GetDataloggerNextFilteredRecordAbsoluteAddress();
  AddressSpace := AbsoluteAddress + GetDataloggerNextFilteredRecordBlockByteSize();
  result := (AddressSpace div DATALOGGER_MODBUS_SPACE_SIZE) * DATALOGGER_MODBUS_SPACE_SIZE;
  if result > AbsoluteAddress then
    result := AbsoluteAddress;
end;

function TMyModbus.GetDataloggerNextFilteredRecordModbusAddress(): Word;
var AbsoluteAddress, BaseAddress: Cardinal;
begin
  AbsoluteAddress := GetDataloggerNextFilteredRecordAbsoluteAddress();
  BaseAddress := GetDataloggerNextFilteredRecordBaseAddress();
  result := AbsoluteAddress - BaseAddress + DATALOGGER_MODBUS_SPACE_MIN - 1;
end;

function TMyModbus.FindDataloggerNextOldestRecordIndexInterval(var RecordIndex1: Cardinal; var RecordIndex2: Cardinal;
        var RecordDateTime1: TDateTime; var RecordDateTime2: TDateTime): Boolean;
var AverageRecordIndex: Cardinal;
    RecordDateTime : TDateTime;
    RecordDateTimePack : T4Byte;
begin
  result := false;
  AverageRecordIndex := (RecordIndex1 + RecordIndex2) div 2;
  if (AverageRecordIndex = RecordIndex1) or (AverageRecordIndex = RecordIndex2) then begin
    result := true;
  end else begin
    if ReadDataloggerRecordDateTimePack(AverageRecordIndex * DATALOGGER_RECORD_SIZE, RecordDateTimePack) then begin
      result := true;
      if not GetDateTimeFromHex(Cardinal(RecordDateTimePack), RecordDateTime) then begin
        RecordDateTime := DATALOGGER_INVALID_DATETIME;
      end;
      if (RecordDateTime1 = DATALOGGER_INVALID_DATETIME) and (RecordDateTime2 = DATALOGGER_INVALID_DATETIME) then begin
        if (RecordIndex1 <= RecordIndex2) then begin
          RecordIndex2 := AverageRecordIndex;
          RecordDateTime2 := RecordDateTime;
        end else begin
          RecordIndex1 := AverageRecordIndex;
          RecordDateTime1 := RecordDateTime;
        end;
      end else if (RecordDateTime1 = DATALOGGER_INVALID_DATETIME) then begin
          if (RecordDateTime = DATALOGGER_INVALID_DATETIME) then begin
            RecordIndex1 := AverageRecordIndex;
            RecordDateTime1 := RecordDateTime;
          end else begin
            if (RecordDateTime2 <= RecordDateTime) then begin
              RecordIndex1 := AverageRecordIndex;
              RecordDateTime1 := RecordDateTime;
            end else begin
              RecordIndex2 := AverageRecordIndex;
              RecordDateTime2 := RecordDateTime;
            end;
          end;
      end else if (RecordDateTime2 = DATALOGGER_INVALID_DATETIME) then begin
          if (RecordDateTime = DATALOGGER_INVALID_DATETIME) then begin
            RecordIndex2 := AverageRecordIndex;
            RecordDateTime2 := RecordDateTime;
          end else begin
            if (RecordDateTime1 <= RecordDateTime) then begin
              RecordIndex2 := AverageRecordIndex;
              RecordDateTime2 := RecordDateTime;
            end else begin
              RecordIndex1 := AverageRecordIndex;
              RecordDateTime1 := RecordDateTime;
            end;
          end;
      end else begin
        if (RecordDateTime1 <= RecordDateTime2) then begin
          RecordIndex2 := AverageRecordIndex;
          RecordDateTime2 := RecordDateTime;
        end else begin
          RecordIndex1 := AverageRecordIndex;
          RecordDateTime1 := RecordDateTime;
        end;
      end;
    end;
  end;
end;

function TMyModbus.FindDataloggerNextNewestRecordIndexInterval(var RecordIndex1: Cardinal; var RecordIndex2: Cardinal;
        var RecordDateTime1: TDateTime; var RecordDateTime2: TDateTime): Boolean;
var AverageRecordIndex: Cardinal;
    RecordDateTime : TDateTime;
    RecordDateTimePack : T4Byte;
begin
  result := false;
  AverageRecordIndex := (RecordIndex1 + RecordIndex2) div 2;
  if (AverageRecordIndex = RecordIndex1) or (AverageRecordIndex = RecordIndex2) then begin
    result := true;
  end else begin
    if ReadDataloggerRecordDateTimePack(AverageRecordIndex * DATALOGGER_RECORD_SIZE, RecordDateTimePack) then begin
      result := true;
      if not GetDateTimeFromHex(Cardinal(RecordDateTimePack), RecordDateTime) then begin
        RecordDateTime := DATALOGGER_INVALID_DATETIME;
      end;
      if (RecordDateTime1 = DATALOGGER_INVALID_DATETIME) and (RecordDateTime2 = DATALOGGER_INVALID_DATETIME) then begin
        if (RecordIndex1 <= RecordIndex2) then begin
          RecordIndex2 := AverageRecordIndex;
          RecordDateTime2 := RecordDateTime;
        end else begin
          RecordIndex1 := AverageRecordIndex;
          RecordDateTime1 := RecordDateTime;
        end;
      end else if (RecordDateTime1 = DATALOGGER_INVALID_DATETIME) then begin
          if (RecordDateTime = DATALOGGER_INVALID_DATETIME) then begin
            RecordIndex1 := AverageRecordIndex;
            RecordDateTime1 := RecordDateTime;
          end else begin
            if (RecordDateTime2 >= RecordDateTime) then begin
              RecordIndex1 := AverageRecordIndex;
              RecordDateTime1 := RecordDateTime;
            end else begin
              RecordIndex2 := AverageRecordIndex;
              RecordDateTime2 := RecordDateTime;
            end;
          end;      
      end else if (RecordDateTime2 = DATALOGGER_INVALID_DATETIME) then begin
          if (RecordDateTime = DATALOGGER_INVALID_DATETIME) then begin
            RecordIndex2 := AverageRecordIndex;
            RecordDateTime2 := RecordDateTime;
          end else begin
            if (RecordDateTime1 >= RecordDateTime) then begin
              RecordIndex2 := AverageRecordIndex;
              RecordDateTime2 := RecordDateTime;
            end else begin
              RecordIndex1 := AverageRecordIndex;
              RecordDateTime1 := RecordDateTime;
            end;
          end;
      end else begin
        if (RecordDateTime1 >= RecordDateTime2) then begin
          RecordIndex2 := AverageRecordIndex;
          RecordDateTime2 := RecordDateTime;
        end else begin
          RecordIndex1 := AverageRecordIndex;
          RecordDateTime1 := RecordDateTime;
        end;
      end;
    end;
  end;
end;

function TMyModbus.FindDataloggerOldestRecordIndex(var OldestRecordIndex: Cardinal; var Running: Boolean; Filter: TMagE1DataloggerFilter): Boolean;
var RecordIndex1, RecordIndex2: Cardinal;
    RecordDateTime1, RecordDateTime2 : TDateTime;
    RecordDateTimePack1, RecordDateTimePack2 : T4Byte;
begin
  result := false;
  RecordIndex1 := 0;
  if FDataloggerMemorySize >= DATALOGGER_RECORD_SIZE then
    RecordIndex2 := (FDataloggerMemorySize div DATALOGGER_RECORD_SIZE) - 1
  else
    RecordIndex2 := 0;
  if ReadDataloggerRecordDateTimePack(RecordIndex1 * DATALOGGER_RECORD_SIZE, RecordDateTimePack1) and
     ReadDataloggerRecordDateTimePack(RecordIndex2 * DATALOGGER_RECORD_SIZE, RecordDateTimePack2) then
  begin
    if not GetDateTimeFromHex(Cardinal(RecordDateTimePack1), RecordDateTime1) then
      RecordDateTime1 := DATALOGGER_INVALID_DATETIME;
    if not GetDateTimeFromHex(Cardinal(RecordDateTimePack2), RecordDateTime2) then
      RecordDateTime2 := DATALOGGER_INVALID_DATETIME;
    while FindDataloggerNextOldestRecordIndexInterval(RecordIndex1, RecordIndex2, RecordDateTime1, RecordDateTime2) do begin
      if (not Running) or (Abs(RecordIndex1 - RecordIndex2) <= 1) then begin
        result := true;
        Break;
      end;
    end;
    if (RecordDateTime1 = DATALOGGER_INVALID_DATETIME) and (RecordDateTime2 = DATALOGGER_INVALID_DATETIME) then begin
      if RecordIndex1 <= RecordIndex2 then
        OldestRecordIndex := RecordIndex1
      else
        OldestRecordIndex := RecordIndex2;
    end else if (RecordDateTime1 = DATALOGGER_INVALID_DATETIME) then begin
      OldestRecordIndex := RecordIndex2;
    end else if (RecordDateTime2 = DATALOGGER_INVALID_DATETIME) then begin
      OldestRecordIndex := RecordIndex1;
    end else begin
      if RecordDateTime1 <= RecordDateTime2 then
        OldestRecordIndex := RecordIndex1
      else
        OldestRecordIndex := RecordIndex2;
    end;
  end;
end;

function TMyModbus.FindDataloggerNewestRecordIndex(var NewestRecordIndex: Cardinal; var Running: Boolean; Filter: TMagE1DataloggerFilter): Boolean;
var RecordIndex1, RecordIndex2: Cardinal;
    RecordDateTime1, RecordDateTime2 : TDateTime;
    RecordDateTimePack1, RecordDateTimePack2 : T4Byte;
begin
  result := false;
  RecordIndex1 := 0;
  if FDataloggerMemorySize >= DATALOGGER_RECORD_SIZE then
    RecordIndex2 := (FDataloggerMemorySize div DATALOGGER_RECORD_SIZE) - 1
  else
    RecordIndex2 := 0;
  if ReadDataloggerRecordDateTimePack(RecordIndex1 * DATALOGGER_RECORD_SIZE, RecordDateTimePack1) and
     ReadDataloggerRecordDateTimePack(RecordIndex2 * DATALOGGER_RECORD_SIZE, RecordDateTimePack2) then
  begin
    if not GetDateTimeFromHex(Cardinal(RecordDateTimePack1), RecordDateTime1) then
      RecordDateTime1 := DATALOGGER_INVALID_DATETIME;
    if not GetDateTimeFromHex(Cardinal(RecordDateTimePack2), RecordDateTime2) then
      RecordDateTime2 := DATALOGGER_INVALID_DATETIME;
    while FindDataloggerNextNewestRecordIndexInterval(RecordIndex1, RecordIndex2, RecordDateTime1, RecordDateTime2) do begin
      if (not Running) or (Abs(RecordIndex1 - RecordIndex2) <= 1) then begin
        result := true;
        Break;
      end;
    end;
    if (RecordDateTime1 = DATALOGGER_INVALID_DATETIME) and (RecordDateTime2 = DATALOGGER_INVALID_DATETIME) then begin
      if RecordIndex1 <= RecordIndex2 then
        NewestRecordIndex := RecordIndex1
      else
        NewestRecordIndex := RecordIndex2;
    end else if (RecordDateTime1 = DATALOGGER_INVALID_DATETIME) then begin
      NewestRecordIndex := RecordIndex2;
    end else if (RecordDateTime2 = DATALOGGER_INVALID_DATETIME) then begin
      NewestRecordIndex := RecordIndex1;
    end else begin
      if RecordDateTime1 >= RecordDateTime2 then
        NewestRecordIndex := RecordIndex1
      else
        NewestRecordIndex := RecordIndex2;
    end;
  end;
end;

function TMyModbus.CalculateDataloggerFilteredRecords(var Running: Boolean; Filter: TMagE1DataloggerFilter) : Boolean;
var OldestRecordIndex, NewestRecordIndex, MaxDataloggerRecordCount: Cardinal;
begin
  result := false;
  if ReadDataloggerBaseAddressRegister(FDataloggerBaseAddress) and ReadDataloggerMemorySizeRegister(FDataloggerMemorySize) then begin
    if FindDataloggerOldestRecordIndex(OldestRecordIndex, Running, Filter) then begin
      if FindDataloggerNewestRecordIndex(NewestRecordIndex, Running, Filter) then begin
        FDataloggerFilteredFirstRecordAddress := OldestRecordIndex * DATALOGGER_RECORD_SIZE;
        FDataloggerFilteredRecordReadCount := 0;
        MaxDataloggerRecordCount := FDataloggerMemorySize div DATALOGGER_RECORD_SIZE;
        if NewestRecordIndex >= OldestRecordIndex then
          FDataloggerFilteredRecordCount := NewestRecordIndex - OldestRecordIndex + 1
        else
          FDataloggerFilteredRecordCount := MaxDataloggerRecordCount - OldestRecordIndex + NewestRecordIndex + 1;
        if Filter.RecordCountMaxEnabled and (FDataloggerFilteredRecordCount > Filter.RecordCountMax) then
          FDataloggerFilteredRecordCount := Filter.RecordCountMax;
        result := true;
      end;
    end;
  end;
end;

{----------------------------------------------------------------
------------------------- DEMO ----------------------------------
----------------------------------------------------------------}

constructor TDemoCommunication.Create();
var sExePath, filename: string;
    newFile: TextFile;
begin
    inherited;

    ProtokolType:=ptDemo;

    FTimer:=TTimer.Create( nil );		//timer pro generovani prutoku a pocitadel
    FTimer.Interval:=1000;
    FTimer.OnTimer:=OnTimeout;
    FTimer.Enabled:=true;

    sExePath := GetUserAppDataProductPath();
    filename:='DemoValues.dat';
    if not FileExists(sExePath+filename) then begin
        AssignFile(newFile, sExePath+filename);
        Rewrite(newFile);
        CloseFile(newFile);
        if not FileExists(sExePath+filename) then
      	        Raise EInOutError.Create('File "'+ filename +' " not found');  //ukonci cteni
    end;

    DemoValues:=TIniFile.Create(sExePath+filename);

    Flow:=0;
    Total:=0;
    Batch:=0;
end;

destructor TDemoCommunication.Destroy;
begin
    DemoValues.Free();
    FTimer.Free();
end;

procedure TDemoCommunication.SetCommunicationParam(APort:Cardinal;AID:Cardinal;ABaudRate:TBaudRate;ADataBits:TDataBits;
      AStopBits:TStopBits;AParity:TParity;ARtsControl:TRtsControl;AEnableDTROnOpen:Boolean;ATimeout:Cardinal;
      AIPAddress:String;AIPPort:Integer);
begin
  Port:=APort;
  ID:=AID;
  BaudRate:=ABaudRate;
  DataBits:=ADataBits;
  StopBits:=AStopBits;
  Parity:=AParity;
  Timeout:=ATimeout;
  RtsControl:=ARtsControl;
  EnableDTROnOpen:=AEnableDTROnOpen;
end;

procedure TDemoCommunication.GetCommunicationParam(var APort:Cardinal;var AID:Cardinal;var ABaudRate:TBaudRate;var ADataBits:TDataBits;
      var AStopBits:TStopBits;var AParity:TParity;var ARtsControl:TRtsControl;var AEnableDTROnOpen:Boolean;var ATimeout:Cardinal;
      var AIPAddress:String; var AIPPort:Integer);
begin
  APort:=Port;
  AID:=ID;
  ABaudRate:=BaudRate;
  ADataBits:=DataBits;
  AStopBits:=StopBits;
  AParity:=Parity;
  ATimeout:=Timeout;
  ARtsControl:=RtsControl;
  AEnableDTROnOpen:=EnableDTROnOpen;
end;

function TDemoCommunication.GetComPort: Cardinal;
begin
  Result := Port;
end;

function TDemoCommunication.Connect():Boolean;
begin
	Result:=true;
end;

procedure TDemoCommunication.Disconnect();
begin
end;

function TDemoCommunication.ReadMenuValue(Adr: Cardinal; var Data:T4Byte):Boolean;
begin
    case Adr of
    0..11:		//passwd
    	Integer(Data):=1;
    //info
    1003:
    	Integer(Data):=DemoValues.ReadInteger('Demo', IntToStr(4013), 0);
    1009:
    	Integer(Data):=DemoValues.ReadInteger('Demo', IntToStr(4011), 0);
    1011:
    	Integer(Data):=DEMO_FIRMWARE_NUMBER;
    1013:
    	Integer(Data):=DemoValues.ReadInteger('Demo', IntToStr(4015), 0);
    else
        Integer(Data):=DemoValues.ReadInteger('Demo', IntToStr(Adr), 0);
    end;
    Result:=true;
end;

function TDemoCommunication.WriteMenuValue(Adr: Cardinal;Value:T4Byte):Boolean;
begin
    case Adr of
        3999: Total:=0;
        4001: Batch:=0;
    else
    	DemoValues.WriteInteger('Demo', IntToStr(Adr), Integer(Value));
	end;
    Result:=true;
end;

function TDemoCommunication.ReadRTC(Adr: Cardinal;var RetValue:Cardinal): Boolean;
var Hour, Min, Sec, MSec: Word;
	Year, Month, Day: Word;
begin
  DecodeTime(Now, Hour, Min, Sec, MSec);
  DecodeDate(Now, Year, Month, Day);
  case Adr of
  1: RetValue:=Sec;
  2: RetValue:=Min;	//min
  3: RetValue:=Hour;          //hod
  4: RetValue:=Day;          //den
  5: RetValue:=Month;          //mesic
  6: RetValue:=Year -2000;          //rok
  0: RetValue:=1;	//pritomnost
  else
  	RetValue:=0;	//pritomnost
  end;
	Result:=true;
end;

function TDemoCommunication.WriteRTC(Adr: Cardinal;Value:Cardinal): Boolean;
begin
	Result:=true;
end;

function TDemoCommunication.ReadFirmNO(var firmwareNO:Cardinal):Boolean;
begin
    firmwareNO:=DEMO_FIRMWARE_NUMBER;
    Result:=true;
end;

function TDemoCommunication.GetLastError:integer;
begin
	Result:=0;
end;

function TDemoCommunication.ReadOnline(Adr:Cardinal;var OnlineData:TOnline):Boolean;
begin
  OnlineData.Flow:=Flow;
  OnlineData.Temp:=250;
  case Adr of
    1:OnlineData.Volume:=Total;
    //2:OnlineData.Volume:=Aux;
    //3:OnlineData.Volume:=FlowPlus;
    //4:OnlineData.Volume:=FlowMinus;
  else
    OnlineData.Volume:=0;
  end;

  OnlineData.VolumeFormat:=3;
  OnlineData.FlowFormat:=3;
  OnlineData.TempFormat:=1;

  OnlineData.FlowUnit:= TFlowUnit(DemoValues.ReadInteger('Demo', IntToStr(1499), 0));
  OnlineData.VolumeUnit:= TVolumeUnit(DemoValues.ReadInteger('Demo', IntToStr(1501), 0));
  OnlineData.TempUnit:=TTempUnit(0);
  Result:=true;
end;

function TDemoCommunication.ReadOnlineB1(var OnlineData: TOnlineB1; ReadTotalValueAsDouble: Boolean): Boolean;
begin
  // Init
  Result := True;
  OnlineData.Flow := Flow / 1000;
  OnlineData.Total := Total / 1000;
  OnlineData.Batch := Batch / 1000;
  OnlineData.ErrorCode := Random(4);
end;

function DateTimeToHex(ADateTime: TDateTime): Cardinal;
var ST: TSystemTime;
    Temp: Cardinal;
begin
  DateTimeToSystemTime(ADateTime, ST);
  Result := ((ST.wYear - DATALOGGER_BASE_YEAR) and $3F) shl 26;
  Temp := ST.wMonth shl 22;
  Result := Result or Temp;
  Temp := ST.wDay shl 17;
  Result := Result or Temp;
  Temp := ST.wHour shl 12;
  Result := Result or Temp;
  Temp := ST.wMinute shl 6;
  Result := Result or Temp;
  Temp := ST.wSecond;
  Result := Result or Temp;
end;

{
function DateTimeToHexTime(ADateTime: TDateTime): Cardinal;
var ST: TSystemTime;
begin
  DateTimeToSystemTime(ADateTime, ST);
  Result := 256 * 256 * (16 * (ST.wHour div 10) +  (ST.wHour mod 10)) +
            256 * (16 * (ST.wMinute div 10) +  (ST.wMinute mod 10)) +
            16 * (ST.wSecond div 10) +  (ST.wSecond mod 10)
end;
}

function TDemoCommunication.ReadDataloggerNextFilteredRecordBlock(Records: TDataB1List; Filter: TMagE1DataloggerFilter): Boolean;
var I: Integer;
    Total: Double;
    DataB: TDataB1;
    TempDateTime: TDateTime;
begin
  TempDateTime := Now;
  Total := 100 * Records.Count;
  for I := 0 to 1000 do begin
    Total := Total + (10 * Random);
    DataB := TDataB1.Create;
    DataB.Total := Total;
    TempDateTime := TempDateTime + EncodeTime(0, 5, 0, 0); // 5ti minutove intervaly
    DataB.DateTime := TempDateTime;
    Records.Add(DataB);
  end;
  Result := True;
end;

function TDemoCommunication.ReadCalibration(Bod: Cardinal;var  RetValue: Cardinal): boolean;
begin
    Integer(RetValue):=DemoValues.ReadInteger('Calibration', IntToStr(Bod), 0);
    Result:=true;
end;

function TDemoCommunication.WriteCalibration(Bod: Cardinal; Value: Cardinal): boolean;
begin
    DemoValues.WriteInteger('Calibration', IntToStr(Bod), Integer(Value));
    Result:=true;
end;

function TDemoCommunication.ReadMeasurment(Bod:Cardinal;var CalibData:Integer;var MeasurData:Integer): boolean;
begin
    CalibData:=DemoValues.ReadInteger('Calibration', IntToStr(Bod), 0);
    MeasurData:=DemoValues.ReadInteger('Measurement', IntToStr(Bod), 0);
    Result:=true;
end;

function TDemoCommunication.WriteMeasurment(Bod:Cardinal;CalibData:Cardinal;MeasurData:Cardinal): boolean;
begin
    DemoValues.WriteInteger('Calibration', IntToStr(Bod), Integer(CalibData));
    DemoValues.WriteInteger('Measurement', IntToStr(Bod), Integer(MeasurData));
    Result:=true;
end;

function TDemoCommunication.ReadFiveMins(var Buffer : array of Byte; M,D,H3 : Byte) : Boolean;
var   	I,J : Integer;
        Data:Integer;
const 	Size=7;
begin
    for I:=0 to 2 do
    begin
      for J:=0 to 11 do
      begin
      	Data:=Random(15000);
        Buffer[12*Size*I+J*Size+3]:=Data shr 24;
        Buffer[12*Size*I+J*Size+4]:=Data shr 16;
        Buffer[12*Size*I+J*Size+5]:=Data shr 8;
        Buffer[12*Size*I+J*Size+6]:=Data;
      end;
    end;
	Result:=true;
end;

function TDemoCommunication.ReadHours(var Buffer : array of Byte; M,D : Byte) : Boolean;
var		I : Integer;
        SPlus,SMinus : Cardinal;
	    Ch : Word;
const Size=10;
begin
    Ch:=0;
    for I:=0 to 23 do
    begin
    	SPlus:=50+Random(50);
    	SMinus:=Random(5);
        Buffer[Size*I]:=SPlus shr 24;
        Buffer[Size*I+1]:=SPlus shr 16;
        Buffer[Size*(I)+2]:=SPlus shr 8;
        Buffer[Size*(I)+3]:=SPlus;

        Buffer[Size*(I)+4]:=SMinus shr 24;
        Buffer[Size*(I)+5]:=SMinus shr 16;
        Buffer[Size*(I)+6]:=SMinus shr 8;
        Buffer[Size*(I)+7]:=SMinus;

        Buffer[Size*(I)+8]:=Ch shr 8;
        Buffer[Size*(I)+9]:=Ch;
    end;
	Result:=true;
end;

function TDemoCommunication.ReadDays(var Buffer : array of Byte; M,D : Byte) : Boolean;
var		I : Integer;
	   	Year, Month, Day: Word;
        SPlus,SMinus : Cardinal;
	    Ch : Word;
        Count:Integer;
const 	Size=13;
begin
    DecodeDate( Now, Year,Month,Day);
    Ch:=0;
    if D<=16 then
    	Count := 16
    else
    	Count := 15;

    for I:=1 to Count do
    begin
    	SPlus:=1000+Random(1000);
    	SMinus:=Random(100);
        if D<=16 then
        	Buffer[Size*(I-1)+0]:=I
        else
        	Buffer[Size*(I-1)+0]:=I+16;
        Buffer[Size*(I-1)+1]:=M;
        Buffer[Size*(I-1)+2]:=Year-2000;

        Buffer[Size*(I-1)+3]:=SPlus shr 24;
        Buffer[Size*(I-1)+4]:=SPlus shr 16;
        Buffer[Size*(I-1)+5]:=SPlus shr 8;
        Buffer[Size*(I-1)+6]:=SPlus;

        Buffer[Size*(I-1)+7]:=SMinus shr 24;
        Buffer[Size*(I-1)+8]:=SMinus shr 16;
        Buffer[Size*(I-1)+9]:=SMinus shr 8;
        Buffer[Size*(I-1)+10]:=SMinus;

        Buffer[Size*(I-1)+11]:= Ch shr 8;
        Buffer[Size*(I-1)+12]:=Ch;
	end;
    Result:=true;
end;

function TDemoCommunication.ReadMonth(var Buffer : array of Byte; M : Byte) : Boolean;
var Year, Month, Day: Word;
    SPlus,SMinus : Cardinal;
    SPlusDec,SMinusDec: Cardinal;
    Ch : Word;
begin
    SPlus:=10000+Random(10000);
    SPlusDec:=Random(10000);
    SMinus:=Random(1000);
    SMinusDec:=0;
    Ch:=0;
    DecodeDate( Now, Year,Month,Day);

	Buffer[1]:=M;	//Month.M
	Buffer[2]:=Year-2000;	// Month.Y:=2000+

    Buffer[3]:=SPlus shr 24;
    Buffer[4]:=SPlus shr 16;
    Buffer[5]:=SPlus shr 8;
    Buffer[6]:=SPlus ;

    Buffer[7]:=SPlusDec shr 24;
    Buffer[8]:=SPlusDec shr 16;
    Buffer[9]:=SPlusDec shr 8;
    Buffer[10]:=SPlusDec;

    Buffer[11]:=SMinus shr 24;
    Buffer[12]:=SMinus shr 16;
    Buffer[13]:=SMinus shr 8;
    Buffer[14]:=SMinus;

    Buffer[15]:=SMinusDec shr 24;
    Buffer[16]:=SMinusDec shr 16;
    Buffer[17]:=SMinusDec shr 8;
    Buffer[18]:=SMinusDec;

    Buffer[19]:=Ch shr 8;
    Buffer[20]:=Ch;
    
	Result:=true;
end;

function TDemoCommunication.MemoryPresent(var Present: Boolean): Boolean;
begin
    Present:=true;
    Result:=true;
end;

function TDemoCommunication.CalculateDataloggerFilteredRecords(var Running: Boolean; Filter: TMagE1DataloggerFilter) : Boolean;
begin
  result := true;
end;

procedure TDemoCommunication.OnTimeout(Sender : TObject);
begin
  Flow := Trunc(DemoValues.ReadInteger('Demo', IntToStr(4009), 0) * (0.9 + 0.2 * Random));
  Total:=Total+ Flow;
  Batch:=Batch + Flow;
end;

function TMyCommPort.ReadMenuStr(Adr: Cardinal; var Str: String;
  Size: Integer): Boolean;
begin
  raise Exception.Create('Not implemented for COM port!');
  Result := False;
end;

function TMyCommPort.WriteMenuStr(Adr: Cardinal; Str: String;
  Size: Integer): Boolean;
begin
  raise Exception.Create('Not implemented for COM port!');
  Result := False;
end;

function TMyModbus.ReadMenuStr(Adr: Cardinal; var Str: String;
  Size: Integer): Boolean;
var Buff:TDataByte;
    I,Err:Integer;
begin
  ModbusM.FunctionCode:=3;      //cteni Holding Registru
  ModbusM.Offset:=Adr;
  ModbusM.Quantity:=Size div 2; // cte se po dvoubajtech
  Err:=0;
  SetLength(Buff, 0);
  Result:=false;
  While((Err<MAX_ERROR)and(Result=false)) do
  begin
    ResponseFlag:=rnWait;
    ModbusM.Query;
    while ResponseFlag=rnWait do
      Application.ProcessMessages;
    if ResponseFlag=rnOk then
    begin
      Buff:=ModbusM.ReadValues;
      Str := '';
      for I := 0 to (Size div 2) - 1 do begin
        // Prehozeni sudy-lichy bajt (nejdriv pridame do retezce druhy pak prvni)
        if Buff[(2 * I) + 1] <> 0 then Str := Str + Chr(Buff[(2 * I) + 1]) else Break;
        if Buff[(2 * I)] <> 0 then Str := Str + Chr(Buff[(2 * I)]) else Break;
      end;
      Result:=true;
    end
    else
    begin
      Inc(Err);
      sleep(50);
    end;
  end;
  ResponseFlag:=rnNONE;
end;

function TMyModbus.WriteMenuStr(Adr: Cardinal; Str: String;
  Size: Integer): Boolean;
var Buff:TDataByte;
    I, Err:Integer;
begin
  ModbusM.FunctionCode:=16;      //cteni Holding Registru
  ModbusM.Offset:=Adr;
  ModbusM.Quantity:=Size div 2;  // zapisuje se po dvoubajtech
  Err:=0;
  Result:=false;
  While((Err<MAX_ERROR)and(Result=false)) do
  begin
    SetLength(Buff,Size);
    for I := 0 to (Size div 2) - 1 do begin
      // Kazdy prvni znak (ze dvou) na druhy v Bufferu
      if ((2 * I) + 1) <= Length(Str) then begin
        Buff[(2 * I) + 1] := Ord(Str[(2 * I) + 1]);
      end else begin
        Buff[(2 * I) + 1] := 0; // pokud je retezec kratky tak #0
      end;
      // Kazdy prvni znak (ze dvou) na prvni v Bufferu
      if ((2 * I) + 2) <= Length(Str) then begin
        Buff[(2 * I) + 0] := Ord(Str[(2 * I) + 2]);
      end else begin
        Buff[(2 * I) + 0] := 0; // pokud je retezec kratky tak #0
      end;
    end;
    ModbusM.WriteValues:=Buff;
    ResponseFlag:=rnWait;
    ModbusM.Query;
    while ResponseFlag=rnWait do
      Application.ProcessMessages;
    if ResponseFlag=rnOk then
    begin
      Result:=true;
    end
    else
    begin
      Inc(Err);
      sleep(50);
    end;
  end;
  ResponseFlag:=rnNONE;
end;

function TDemoCommunication.ReadMenuStr(Adr: Cardinal; var Str: String;
  Size: Integer): Boolean;
begin
  Str := DemoValues.ReadString('Demo', IntToStr(Adr), '');
  Result := True;
end;

function TDemoCommunication.WriteMenuStr(Adr: Cardinal; Str: String;
  Size: Integer): Boolean;
begin
  DemoValues.WriteString('Demo', IntToStr(Adr), Str);
  Result:=true;
end;

{ TMyTCPModbus }

constructor TMyTCPModbus.Create;
begin
  inherited Create;
  ProtokolType:=ptTCPModbus;

  FErrorCount := 0;
  FIsReconnecting := False;
  FSocketErrorCount := 0;
  FSocketStatus := ssUnknown;
  FPacketStatus := rnNONE;
  FTCPReadSize := 0;
  FTCPWriteSize := 0;
  FSlaveID := 1; // TODO
  FRegisterDim := 2;
  FClientSocket := TClientSocket.Create(nil);
  FClientSocket.OnConnect := SocketConnect;
  FClientSocket.OnDisconnect := SocketDisconnect;
  FClientSocket.OnRead := SocketRead;
  FClientSocket.OnError := SocketError;
  FTimer := TTimer.Create(nil);
//  FTimer.Interval := TCP_TIMER_INTERVAL;
  FTimer.Interval := FTimeout;
  FTimer.Enabled := False;
  FTimer.OnTimer := SocketTimeout;
end;

destructor TMyTCPModbus.Destroy;
begin
  inherited;
  Disconnect;
  FreeAndNil(FTimer);
  FreeAndNil(FClientSocket);
end;

function TMyTCPModbus.Connect: Boolean;
var TimeOut: TDateTime;
begin
  Result := False;
  try
    if not FClientSocket.Active then begin
      {$IFDEF LOG}
      WriteLog('CONNECT [BEFORE]');
      {$ENDIF}
      FSocketStatus := ssUnknown;
      FClientSocket.Active := True;
      TimeOut := Now + TCP_CONN_TIMEOUT;
      while (FSocketStatus = ssUnknown) and (TimeOut > Now) do begin
        Sleep(10);
        Application.ProcessMessages;
      end;
      Result := (FSocketStatus = ssConnected);
      {$IFDEF LOG}
      if Result then begin
        WriteLog('CONNECT [AFTER] - Succefull');
      end else begin
        WriteLog('CONNECT [AFTER] - Failure');
      end;
      {$ENDIF}
    end else begin
      Result := (FSocketStatus = ssConnected);
    end;
  except
    FSocketStatus := ssUnknown;
  end;
end;

procedure TMyTCPModbus.Disconnect;
var TimeOut: TDateTime;
begin
  if FClientSocket.Active then begin
    {$IFDEF LOG}
    WriteLog('DISCONNECT [BEFORE]');
    {$ENDIF}
    FSocketStatus := ssUnknown;
    FClientSocket.Active := False;
    TimeOut := Now + TCP_CONN_TIMEOUT;
    while (FSocketStatus = ssUnknown) and (TimeOut > Now) do begin
      Sleep(10);
      Application.ProcessMessages;
    end;
    {$IFDEF LOG}
    if (FSocketStatus = ssDisconnected) then begin
      WriteLog('DISCONNECT [AFTER] - Succefull');
    end else begin
      WriteLog('DISCONNECT [AFTER] - Failure');
    end;
    {$ENDIF}
  end;
end;

procedure TMyTCPModbus.SocketConnect(Sender: TObject;
  Socket: TCustomWinSocket);
begin
  FSocketStatus := ssConnected;
  FSocketErrorCount := 0;
end;

procedure TMyTCPModbus.SocketDisconnect(Sender: TObject;
  Socket: TCustomWinSocket);
begin
  FSocketStatus := ssDisconnected;
end;

procedure TMyTCPModbus.SocketRead(Sender: TObject;
  Socket: TCustomWinSocket);
var CurrentSize, Size: Integer;
    {$IFDEF LOG}
    I: Integer;
    StrData: String;
    {$ENDIF}
begin
  if (FPacketStatus = rnWait) then begin
    FTimer.Enabled := False;
    try
      CurrentSize := Length(FTCPReadValues);
      Size := Socket.ReceiveLength;
      SetLength(FTCPReadValues, CurrentSize + Size);
      Socket.ReceiveBuf(FTCPReadValues[CurrentSize], Size);
      if (CurrentSize + Size > 1) and (CurrentSize + Size >= ExpectedLength) then begin
        {$IFDEF LOG}
        StrData:='';
        for I := 0 to Size - 1 do begin
          StrData := StrData + Format('%.2x', [FTCPReadValues[I]]) + ' ';
        end;
        WriteLog(Format('READ (Size=%d): ', [Size]) + StrData);
        {$ENDIF}
        ValidateInput;
        FPacketStatus := rnOK;
        {$IFDEF LOG}
        WriteLog('READ (OK)');
        {$ENDIF}
      end;
    except
      FPacketStatus := rnError;
      {$IFDEF LOG}
      WriteLog('READ (ERROR)');
      {$ENDIF}
    end;
  end;
  FSocketErrorCount := 0;
end;

procedure TMyTCPModbus.SocketError(Sender: TObject;
  Socket: TCustomWinSocket; ErrorEvent: TErrorEvent;
  var ErrorCode: Integer);
begin
  {$IFDEF LOG}
  WriteLog(Format('SOCKET ERROR (%d)', [ErrorCode]));
  {$ENDIF}
  if (FPacketStatus = rnWait) then begin
    FTimer.Enabled := False;
    FPacketStatus := rnError;
  end;
  if (FSocketStatus = ssUnknown) then begin
    FSocketStatus := ssDisconnected;
  end;
  Inc(FSocketErrorCount);
  if FSocketErrorCount > RECREATE_ERROR_COUNT then begin
    ReCreateConnection;
    FSocketErrorCount := 0;
  end else begin
    if (FSocketErrorCount mod RECONNECT_ERROR_COUNT) = 0 then begin
      ReConnectConnection;
    end;
  end;
  ErrorCode := 0;
end;

procedure TMyTCPModbus.SocketTimeout(Sender: TObject);
begin
  {$IFDEF LOG}
  WriteLog('SOCKET TIMEOUT');
  {$ENDIF}
  if (FPacketStatus = rnWait) then begin
    FTimer.Enabled := False;
    FPacketStatus := rnError;
  end;
end;

procedure TMyTCPModbus.PrepareOutput;
var
  DataCRC: word;
  I: Integer;
  ByteTemp: Byte;
begin
  SetLength(FReadValues, 0);
  case FFunctionCode of
    1,2,3,4 :
       begin
         FTCPWriteSize := 8;
         Setlength(FTCPWriteValues, FTCPWriteSize);
         FTCPWriteValues[0]:=FSlaveId;
         FTCPWriteValues[1]:=FFunctionCode;
         FTCPWriteValues[2]:=Hi(Foffset);
         FTCPWriteValues[3]:=Lo(Foffset);
         FTCPWriteValues[4]:=Hi(Fquantity);
         FTCPWriteValues[5]:=Lo(Fquantity);
         DataCRC:=CRC(FTCPWriteValues);
         FTCPWriteValues[high(FTCPWriteValues)-1]:=lo(DataCRC);
         FTCPWriteValues[high(FTCPWriteValues)]:=hi(DataCRC);
       end;
    5: begin
         FTCPWriteSize := 8;
         Setlength(FTCPWriteValues, FTCPWriteSize);
         FTCPWriteValues[0]:=FSlaveId;
         FTCPWriteValues[1]:=FFunctionCode;
         FTCPWriteValues[2]:=Hi(Foffset);
         FTCPWriteValues[3]:=Lo(Foffset);
         if Length(FWriteValues)<2 then begin
           raise Exception.Create('Error 3');
         end;
         FTCPWriteValues[4]:=FWriteValues[0];
         FTCPWriteValues[5]:=FWriteValues[1];
         DataCRC:=CRC(FTCPWriteValues);
         FTCPWriteValues[high(FTCPWriteValues)-1]:=lo(DataCRC);
         FTCPWriteValues[high(FTCPWriteValues)]:=hi(DataCRC);
       end;
    6: begin
         FTCPWriteSize := 6 + FRegisterDim;
         Setlength(FTCPWriteValues, FTCPWriteSize);
         FTCPWriteValues[0]:=FSlaveId;
         FTCPWriteValues[1]:=FFunctionCode;
         FTCPWriteValues[2]:=Hi(Foffset);
         FTCPWriteValues[3]:=Lo(Foffset);
         if length(FWriteValues)<FRegisterDim then begin
           raise Exception.Create('Error 3');
         end;
         for I := 1 to FRegisterDim do begin
           FTCPWriteValues[3+i] := FWriteValues[I-1];
         end;
         DataCRC:=CRC(FTCPWriteValues);
         FTCPWriteValues[high(FTCPWriteValues)-1]:=lo(DataCRC);
         FTCPWriteValues[high(FTCPWriteValues)]:=hi(DataCRC);
       end;
    15:
       begin
         if ((FQuantity mod 8) > 0) then begin
           ByteTemp := (FQuantity div 8) + 1;
         end else begin
           ByteTemp := (FQuantity div 8);
         end;
         FTCPWriteSize := 9 + ByteTemp;
         Setlength(FTCPWriteValues, FTCPWriteSize);
         FTCPWriteValues[0]:=FSlaveId;
         FTCPWriteValues[1]:=FFunctionCode;
         FTCPWriteValues[2]:=Hi(Foffset);
         FTCPWriteValues[3]:=Lo(Foffset);
         if Length(FWriteValues)<ByteTemp then begin
           raise Exception.Create('Error 3');
         end;
         FTCPWriteValues[4]:=Hi(Fquantity);
         FTCPWriteValues[5]:=Lo(Fquantity);
         FTCPWriteValues[6]:=ByteTemp;
         for I := 1 to ByteTemp do begin
           FTCPWriteValues[6+i]:=FWriteValues[I-1];
         end;
         DataCRC:=CRC(FTCPWriteValues);
         FTCPWriteValues[high(FTCPWriteValues)-1]:=lo(DataCRC);
         FTCPWriteValues[high(FTCPWriteValues)]:=hi(DataCRC);
       end;
    16:
       begin
         ByteTemp := FQuantity * FRegisterDim;
         FTCPWriteSize := 9 + ByteTemp;
         Setlength(FTCPWriteValues, FTCPWriteSize);
         FTCPWriteValues[0]:=FSlaveId;
         FTCPWriteValues[1]:=FFunctionCode;
         FTCPWriteValues[2]:=Hi(Foffset);
         FTCPWriteValues[3]:=Lo(Foffset);
         if Length(FWriteValues)<ByteTemp then begin
           raise Exception.Create('Error 3');
         end;
         FTCPWriteValues[4]:=Hi(Fquantity);
         FTCPWriteValues[5]:=Lo(Fquantity);
         FTCPWriteValues[6]:=ByteTemp;
         for I := 1 to ByteTemp do begin
           FTCPWriteValues[6+I] := FWriteValues[I-1];
         end;
         DataCRC:=CRC(FTCPWriteValues);
         FTCPWriteValues[high(FTCPWriteValues)-1]:=lo(DataCRC);
         FTCPWriteValues[high(FTCPWriteValues)]:=hi(DataCRC);
       end;
    else begin
      raise Exception.Create('Error 1');
    end;
  end;
end;

function TMyTCPModbus.ExpectedLength: Integer;
var
  ByteTemp: Byte;
begin
  Result := 0;
  if (Length(FTCPReadValues) > 1) then begin
    case FTCPReadValues[1] of
      1,2:
         begin
           if ((FQuantity mod 8)>0) then begin
             ByteTemp := (FQuantity div 8) + 1
           end else begin
             ByteTemp := (FQuantity div 8);
           end;
           Result := ByteTemp + 5;
         end;
      3,4:
         begin
           Result := FQuantity * FRegisterDim + 5;
         end;
      5,15,16:
         begin
           Result := 8;
         end;
      6: begin
           Result := 6 + FRegisterDim;
         end;
      $81,$82,$83,$84,$85,$86,$8f,$90:
         begin
           Result := 5;
         end;
      else begin
        Result := -1;
      end;
    end;
  end;
end;

procedure TMyTCPModbus.ValidateInput;
var
  DataCRC: Word;
  I : Integer;
  ByteTemp: Byte;

begin
  DataCRC:=CRC(FTCPReadValues);
  if (Lo(DataCRC)<>FTCPReadValues[high(FTCPReadValues)-1]) or (Hi(DataCRC)<>FTCPReadValues[high(FTCPReadValues)]) then begin
    raise Exception.Create('Error 9');
  end;

  case FTCPReadValues[1] of
    1,2:
       begin
         if ((FQuantity mod 8)>0) then begin
           ByteTemp := (FQuantity div 8) + 1
         end else begin
           ByteTemp := (FQuantity div 8);
         end;
         if length(FTCPReadValues)= ByteTemp+5 then begin
           SetLength(FReadValues, FTCPReadValues[2]);
           for I := 0 to FTCPReadValues[2]-1 do begin
             FReadValues[I]:=FTCPReadValues[3+I];
           end;
         end else begin
           raise Exception.Create('Error 4');
         end;
       end;
    3,4:
       begin
         if Length(FTCPReadValues)= FQuantity * FRegisterDim + 5 then  begin
           SetLength(FReadValues, FTCPReadValues[2] );
           for I := 0 to FTCPReadValues[2]-1 do begin
             FReadValues[I]:=FTCPReadValues[3+I];
           end;
         end else begin
           raise Exception.Create('Error 4');
         end;
       end;
    5,15,16:
       begin
         if Length(FTCPReadValues) = 8 then begin
           SetLength(FReadValues,2);
           for I := 0 to 1 do begin
             FReadValues[I] := FTCPReadValues[4+I];
           end;
         end else begin
           raise Exception.Create('Error 4');
         end;
       end;
    6: begin
         if Length(FTCPReadValues) = 6 + FRegisterDim then begin
           SetLength(FReadValues,2);
           for I:=0 to 1 do begin
             FReadValues[I] := FTCPReadValues[4+I];
           end;
         end else begin
           raise Exception.Create('Error 4');
         end;
       end;
    $81,$82,$83,$84,$85,$86,$8f,$90:
       begin
         case FTCPReadValues[2] of
           1: begin
                raise Exception.Create('Error 1');
              end;
           2: begin
                raise Exception.Create('Error 2');
              end;
           3: begin
                raise Exception.Create('Error 3');
              end;
           4: begin
                raise Exception.Create('Error 4');
              end;
           5: begin
                raise Exception.Create('Error 5');
              end;
           6: begin
                raise Exception.Create('Error 6');
              end;
           8: begin
                raise Exception.Create('Error 8');
             end;
         end;
       end;
    else begin
      raise Exception.Create('Error 1');
    end;
  end;
end;

procedure TMyTCPModbus.ReadData;
begin
  // FClientSocket.Socket.Read(FClientSocket.Socket.SocketHandle);
  SetLength(FTCPReadValues, 0);
  FPacketStatus := rnWait;
  while (FPacketStatus = rnWait) do begin
    Sleep(10);
    Application.ProcessMessages;
  end;
end;

function TMyTCPModbus.SendData: Boolean;
var SentCnt: Integer;
    {$IFDEF LOG}
    StrData: String;
    I: Integer;
    {$ENDIF}
begin
  Result := False;
  PrepareOutput;
  SentCnt := FClientSocket.Socket.SendBuf(Pointer(FTCPWriteValues)^, FTCPWriteSize);
  {$IFDEF LOG}
  StrData:='';
  for I := 0 to FTCPWriteSize - 1 do begin
    StrData := StrData + Format('%.2x', [FTCPWriteValues[I]]) + ' ';
  end;
  WriteLog(Format('SEND %d of %d (ErrorCount=%d): ', [SentCnt, FTCPWriteSize, FErrorCount]) + StrData);
  {$ENDIF}
  if (FErrorCount > 0) then FClientSocket.Socket.SendBuf(Pointer(FTCPWriteValues)^, FTCPWriteSize);
  if (FErrorCount > 1) then FClientSocket.Socket.SendBuf(Pointer(FTCPWriteValues)^, FTCPWriteSize);
  if (SentCnt <> -1) then begin
//    FTimer.Interval := TCP_TIMER_INTERVAL + (FErrorCount * TCP_TIMER_PROGRESS);
	FTimer.Interval := FTimeout + Cardinal(FErrorCount * TCP_TIMER_PROGRESS);
    FTimer.Enabled := True;
    Result := True;
  end;
end;

procedure TMyTCPModbus.SendAndReadData;
begin
  try
    if SendData then begin
      ReadData;
    end else begin
      FPacketStatus := rnError;
    end;
  except
    FPacketStatus := rnError;
  end;
  if FPacketStatus = rnError then begin
    Inc(FErrorCount);
    if FErrorCount < MAX_ERROR then
    begin
    	FTimer.Enabled := False;
    	SendAndReadData;
    end
    else if FErrorCount > RECREATE_ERROR_COUNT then begin
      ReCreateConnection;
      FErrorCount := 0;
    end else begin
      if (FErrorCount > RECONNECT_ERROR_COUNT) then begin
        ReConnectConnection;
        FErrorCount := 0;
      end;
    end;
    // if FErrorCount > RECONNECT_ERROR_COUNT then ReCreateConnection;
  end else begin
    FErrorCount := 0;
  end;
end;

function TMyTCPModbus.ReadBuffer(var Buffer: array of Byte; Size: Byte;
  Address: Word): Boolean;
var //Err:Cardinal;
i:Integer;
begin
  FFunctionCode:=3;      //cteni Holding Registru
  FOffset:=Address;
  FQuantity:=Size div 2;
//  Err:=0;
  Result:=false;
  // Send ONLY ones
  SendAndReadData;
  if FPacketStatus = rnOk then begin
    for i:=0 to Sizeof(Buffer)-1 do Buffer[i]:=FReadValues[i];
    Result:=true;
  end;
{
  While((Err<MAX_ERROR)and(Result=false)) do
  begin
    FPacketStatus := rnWait;
    SendAndReadData;
    while FPacketStatus = rnWait do Application.ProcessMessages;
    if FPacketStatus = rnOk then
    begin
      for i:=0 to Sizeof(Buffer)-1 do Buffer[i]:=FReadValues[i];
      Result:=true;
    end
    else
    begin
      Inc(Err);
      Sleep(50);
    end;
  end;
 }
  FPacketStatus := rnNONE;
end;

procedure TMyTCPModbus.SetCommunicationParam(APort, AID: Cardinal;
  ABaudRate: TBaudRate; ADataBits: TDataBits; AStopBits: TStopBits;
  AParity: TParity; ARtsControl: TRtsControl; AEnableDTROnOpen: Boolean;
  ATimeout: Cardinal; AIPAddress: String; AIPPort: Integer);
begin
  // NOT IMPLEMENTED FOR TCP
  FIPAddress := AIPAddress;
  FIPPort := AIPPort;
  FClientSocket.Address := AIPAddress;
  FClientSocket.Port := AIPPort;
  FSlaveID := AID;
  FTimeout := ATimeout;
  FTimer.Interval := FTimeout;
end;

procedure TMyTCPModbus.GetCommunicationParam(var APort, AID: Cardinal;
  var ABaudRate: TBaudRate; var ADataBits: TDataBits;
  var AStopBits: TStopBits; var AParity: TParity;
  var ARtsControl: TRtsControl; var AEnableDTROnOpen: Boolean;
  var ATimeout: Cardinal;
  var AIPAddress:String; var AIPPort:Integer);
begin
  // NOT IMPLEMENTED FOR TCP
  AIPAddress := FClientSocket.Address;
  AIPPort := FClientSocket.Port;
  AID := FSlaveID;
end;

function TMyTCPModbus.GetComPort: Cardinal;
begin
  Result := 0;
end;

function TMyTCPModbus.ReadFiveMins(var Buffer: array of Byte; M, D,
  H3: Byte): Boolean;
//const Size = 252;
//const Size = 126;
const Size12 = 64;
      Size22 = 62;
var Page : Word;
    FourByte:T4Byte;
    tmpBuff : Array[0..125] of Byte;
    i:Integer;
begin
  Page:=300*M+(D-1)*8+(H3 div 3);
  Cardinal(FourByte):=Page;
  Result:=WriteMenuValue(49997,FourByte);
  if Result then
  begin
    Result:=ReadBuffer(tmpBuff,Size12,49999+0);
    for i:=0 to Size12*2-1 do
      Buffer[i]:=tmpBuff[i];
    if Result then
    begin
      Result:=ReadBuffer(tmpBuff,Size22,49999+Size12);
      for i:=0 to Size22*2-1 do
        Buffer[Size12*2+i]:=tmpBuff[i];
    end;
  end;
end;

function TMyTCPModbus.ReadHours(var Buffer: array of Byte; M,
  D: Byte): Boolean;
//const Size = 240;
const Size = 120;
var Page : Word;
    FourByte:T4Byte;
begin
  Page:=300*M+247+D;
  Cardinal(FourByte):=Page;
  Result:=WriteMenuValue(49997,FourByte);
  if Result then
  begin
    Result:=ReadBuffer(Buffer,Size,49999+0);
  end;
end;

function TMyTCPModbus.ReadDays(var Buffer: array of Byte; M,
  D: Byte): Boolean;
//const Size16 = 208;
//      Size15 = 195;
const Size16 = 104;
      Size15 = 98;
var Page : Word;
    FourByte:T4Byte;
begin
// rozdeleno na pul, protze Max Size je jen 255Byte
//Day:=(D-1)*12 + (D-1-(((D-1)div 16)*16)*13);

  if D<=16 then
  begin
    Page:=300*M + 279;
    Cardinal(FourByte):=Page;
    Result:=WriteMenuValue(49997,FourByte);
    if Result then
    begin
      {1-16}
      Result:=ReadBuffer(Buffer,Size16,49999+0);
    end;
  end
  else
  begin
    {17-31}
    Page:=300*M+280;
//   Page:=(300*M)+(D-1)div 16 + 279;
    Cardinal(FourByte):=Page;
    Result:=WriteMenuValue(49997,FourByte);
    if Result then
    begin
      Result:=ReadBuffer(Buffer,Size15,49999+0);
    end;
  end;
end;

function TMyTCPModbus.ReadMonth(var Buffer: array of Byte;
  M: Byte): Boolean;
//const Size = 21;
const Size = 11;
var Page : Word;
    FourByte:T4Byte;
begin
  Page:=300*M+281;
  Cardinal(FourByte):=Page;
  Result:=WriteMenuValue(49997,FourByte);
  if Result then
  begin
    Result:=ReadBuffer(Buffer, Size,49999+0);
  end;
end;

function TMyTCPModbus.MemoryPresent(var Present: Boolean): Boolean;
var FourByte:T4Byte;
begin
  result:=ReadMenuValue(1019, FourByte);
  Present:=Integer(FourByte)=1;
end;

function TMyTCPModbus.CalculateDataloggerFilteredRecords(var Running: Boolean; Filter: TMagE1DataloggerFilter) : Boolean;
begin
  result := true;
end;

function TMyTCPModbus.ReadCalibration(Bod: Cardinal;
  var RetValue: Cardinal): boolean;
var FourByte:T4Byte;
begin
  if not (Bod in [1..3]) then
  begin
    result:=false;
    exit;
  end;
  result:=ReadMenuValue(ModbusCalibrationDataAdr[Bod],FourByte);
  RetValue:=Cardinal(FourByte);
end;

function TMyTCPModbus.ReadMeasurment(Bod: Cardinal; var CalibData,
  MeasurData: Integer): boolean;
var FourByte: T4Byte;
begin
  if not (Bod in [1..3]) then
  begin
    result:=false;
    exit;
  end;
  Result:=ReadMenuValue(ModbusCalibrationDataAdr[Bod],FourByte);
  CalibData:=Integer(FourByte);
  if Result then
  begin
    Result:=ReadMenuValue(ModbusMeasurementDataAdr[Bod],FourByte);
    MeasurData:=Cardinal(FourByte);
  end;
end;

function TMyTCPModbus.ReadMenuStr(Adr: Cardinal; var Str: String;
  Size: Integer): Boolean;
var Buff:TDataByte;
    I:Integer;
begin
  FFunctionCode:=3;      //cteni Holding Registru
  FOffset:=Adr;
  FQuantity:=Size div 2; // cte se po dvoubajtech
  SetLength(Buff, 0);
  Result:=false;
  // Send ONLY ones
  SendAndReadData;
  if FPacketStatus = rnOk then begin
    Buff := FReadValues;
    Str := '';
    for I := 0 to (Size div 2) - 1 do begin
      // Prehozeni sudy-lichy bajt (nejdriv pridame do retezce druhy pak prvni)
      if Buff[(2 * I) + 1] <> 0 then Str := Str + Chr(Buff[(2 * I) + 1]) else Break;
      if Buff[(2 * I)] <> 0 then Str := Str + Chr(Buff[(2 * I)]) else Break;
    end;
    Result:=true;
  end;
  FPacketStatus := rnNONE;
end;

function TMyTCPModbus.ReadMenuValue(Adr: Cardinal; var Data: T4Byte): Boolean;
var Buff:TDataByte;
begin
  FFunctionCode:=3;      //cteni Holding Registru
  FOffset:=Adr;
  FQuantity:=2;
  SetLength(Buff, 0);
  Result:=false;
  // Send ONLY ones
  SendAndReadData;
  if FPacketStatus = rnOk then begin
    Buff:=FReadValues;
    Data[3]:=Buff[2];
    Data[2]:=Buff[3];
    Data[1]:=Buff[0];
    Data[0]:=Buff[1];
    Result:=true;
  end;
  FPacketStatus:=rnNONE;
end;

function TMyTCPModbus.ReadRTC(Adr: Cardinal;
  var RetValue: Cardinal): Boolean;
var DTAdr:Cardinal;
    FourByte:T4Byte;
begin
  result:=false;
  case Adr of
  1:
  begin
    result:=true;        //sec
    RetValue:=0;
    exit;
  end;
  2: DTAdr:= 2085;           //min
  3: DTAdr:= 2083;          //hod
  4: DTAdr:= 2087;          //den
  5: DTAdr:= 2089;          //mesic
  6: DTAdr:= 2091;          //rok
  0: DTAdr:= 1017;          //pritomnost
  else
    exit;
  end;
  result:=ReadMenuValue(DTAdr,FourByte);
  RetValue:=Integer(FourByte);
end;

function TMyTCPModbus.WriteRTC(Adr, Value: Cardinal): Boolean;
var DTAdr:Cardinal;
    FourByte:T4Byte;
begin
  result:=false;
  case Adr of
  1:
  begin
    result:=true;        //sec
    exit;
  end;
  2: DTAdr:= 2085;           //min
  3: DTAdr:= 2083;          //hod
  4: DTAdr:= 2087;          //den
  5: DTAdr:= 2089;          //mesic
  6: DTAdr:= 2091;          //rok
  0: DTAdr:= 2093;          //pritomnost
  else
    exit;
  end;
  result:=WriteMenuValue(DTAdr,FourByte);
end;

function TMyTCPModbus.ReadFirmNO(var firmwareNO: Cardinal): Boolean;
var Buff:TDataByte;
begin
  FFunctionCode:=3;      //cteni Holding Registru
  FOffset := MODBUS_REGISTRY_ADDRESS_FIRMWARE;
  FQuantity:=2;
  SetLength(Buff, 0);
  Result:=false;
  // Send ONLY ones
  SendAndReadData;
  if FPacketStatus = rnOk then begin
    Buff := FReadValues;
    firmwareNO:=Cardinal(Buff[2])shl 24;
    firmwareNO:=firmwareNO or(Cardinal(Buff[3])shl 16);
    firmwareNO:=firmwareNO or(Cardinal(Buff[0])shl 8);
    firmwareNO:=firmwareNO or(Buff[1]);
    Result:=true;
  end;
  FPacketStatus := rnNONE;
end;

function TMyTCPModbus.ReadOnline(Adr: Cardinal;
  var OnlineData: TOnline): Boolean;
var Buff:TDataByte;
    tmp:Cardinal;
    Volume:Cardinal;
begin
  if not Adr in[1..5] then
  begin
    result:=false;
    exit;
  end;
  FFunctionCode:=3;      //cteni Holding Registru
  FOffset:=99;           //Adresa Online
  FQuantity:=26;
  SetLength(Buff, 0);
  Result:=false;
  // Send ONLY ones
  SendAndReadData;
  if FPacketStatus = rnOk then begin
    Buff:=FReadValues;
    OnlineData.Flow:=Buff[1];
    OnlineData.Flow:=OnlineData.Flow or (Cardinal(Buff[0])shl 8);
    OnlineData.Flow:=OnlineData.Flow or (Cardinal(Buff[3])shl 16);
    OnlineData.Flow:=OnlineData.Flow or (Cardinal(Buff[2])shl 24);

    if Adr=5 then begin
      OnlineData.Volume:=0;
    end else begin
      Volume:=Buff[Adr*8-4+1];
      Volume:=Volume or (Cardinal(Buff[Adr*8-4+0])shl 8);
      Volume:=Volume or (Cardinal(Buff[Adr*8-4+3])shl 16);
      Volume:=Volume or (Cardinal(Buff[Adr*8-4+2])shl 24);

      tmp:=Buff[Adr*8+1];
      tmp:=tmp or (Cardinal(Buff[Adr*8])shl 8);
      tmp:=tmp or (Cardinal(Buff[Adr*8+3])shl 16);
      tmp:=tmp or (Cardinal(Buff[Adr*8+2])shl 24);

      OnlineData.Volume:=1000.0*Volume+(tmp div 1000);
    end;

    OnlineData.Temp:=Buff[36+1];
    OnlineData.Temp:=OnlineData.Temp or (Cardinal(Buff[36+0])shl 8);
    OnlineData.Temp:=OnlineData.Temp or (Cardinal(Buff[36+3])shl 16);
    OnlineData.Temp:=OnlineData.Temp or (Cardinal(Buff[36+2])shl 24);

    tmp:=Buff[40+1];
    tmp:=tmp or (Cardinal(Buff[40+0])shl 8);
    tmp:=tmp or (Cardinal(Buff[40+3])shl 16);
    tmp:=tmp or (Cardinal(Buff[40+2])shl 24);
    OnlineData.FlowUnit:=TFlowUnit(tmp);

    tmp:=Buff[44+1];
    tmp:=tmp or (Cardinal(Buff[44+0])shl 8);
    tmp:=tmp or (Cardinal(Buff[44+3])shl 16);
    tmp:=tmp or (Cardinal(Buff[44+2])shl 24);
    OnlineData.VolumeUnit:=TVolumeUnit(tmp);

    tmp:=Buff[48+1];
    tmp:=tmp or (Cardinal(Buff[48+0])shl 8);
    tmp:=tmp or (Cardinal(Buff[48+3])shl 16);
    tmp:=tmp or (Cardinal(Buff[48+2])shl 24);
    OnlineData.TempUnit:=TTempUnit(tmp);

    OnlineData.FlowFormat:=3;
    OnlineData.TempFormat:=1;
    OnlineData.VolumeFormat:=3;

    Result:=true;
  end;
  FPacketStatus := rnNONE;
end;

function TMyTCPModbus.WriteCalibration(Bod, Value: Cardinal): boolean;
var FourByte:T4Byte;
begin
  if not (Bod in [1..3]) then
  begin
    result:=false;
    exit;
  end;
  Integer(FourByte):=Value;
  result:=WriteMenuValue(ModbusCalibrationDataAdr[Bod],FourByte);
end;

function TMyTCPModbus.WriteMeasurment(Bod:Cardinal;CalibData:Cardinal;MeasurData:Cardinal): boolean;
var FourByte: T4Byte;
begin
  if not (Bod in [1..3]) then
  begin
    result:=false;
    exit;
  end;
  Integer(FourByte):=CalibData;
  Result:=WriteMenuValue(ModbusCalibrationDataAdr[Bod],FourByte);
  if Result then
  begin
    Cardinal(FourByte):=MeasurData;
    Result:=WriteMenuValue(ModbusMeasurementDataAdr[Bod],FourByte);
  end;
end;

function TMyTCPModbus.WriteMenuStr(Adr: Cardinal; Str: String;
  Size: Integer): Boolean;
var Buff:TDataByte;
    I:Integer;
begin
  FFunctionCode:=16;      //cteni Holding Registru
  FOffset:=Adr;
  FQuantity:=Size div 2;  // zapisuje se po dvoubajtech
//  Err:=0;
  Result:=false;
  // Send ONLY ones
  SetLength(Buff,Size);
  for I := 0 to (Size div 2) - 1 do begin
    // Kazdy prvni znak (ze dvou) na druhy v Bufferu
    if ((2 * I) + 1) <= Length(Str) then begin
      Buff[(2 * I) + 1] := Ord(Str[(2 * I) + 1]);
    end else begin
      Buff[(2 * I) + 1] := 0; // pokud je retezec kratky tak #0
    end;
    // Kazdy prvni znak (ze dvou) na prvni v Bufferu
    if ((2 * I) + 2) <= Length(Str) then begin
      Buff[(2 * I) + 0] := Ord(Str[(2 * I) + 2]);
    end else begin
      Buff[(2 * I) + 0] := 0; // pokud je retezec kratky tak #0
    end;
  end;
  FWriteValues:=Buff;
  SendAndReadData;
  if FPacketStatus = rnOk then begin
    Result:=true;
  end;
  {while((Err<MAX_ERROR)and(Result=false)) do begin
    SetLength(Buff,Size);
    for I := 0 to (Size div 2) - 1 do begin
      // Kazdy prvni znak (ze dvou) na druhy v Bufferu
      if ((2 * I) + 1) <= Length(Str) then begin
        Buff[(2 * I) + 1] := Ord(Str[(2 * I) + 1]);
      end else begin
        Buff[(2 * I) + 1] := 0; // pokud je retezec kratky tak #0
      end;
      // Kazdy prvni znak (ze dvou) na prvni v Bufferu
      if ((2 * I) + 2) <= Length(Str) then begin
        Buff[(2 * I) + 0] := Ord(Str[(2 * I) + 2]);
      end else begin
        Buff[(2 * I) + 0] := 0; // pokud je retezec kratky tak #0
      end;
    end;
    FWriteValues:=Buff;
    SendAndReadData;
    if FPacketStatus = rnOk then begin
      Result:=true;
    end else begin
      Inc(Err);
      sleep(50);
    end;
  end;}
  FPacketStatus := rnNONE;
end;

function TMyTCPModbus.WriteMenuValue(Adr: Cardinal;
  Value: T4Byte): Boolean;
var Buff:TDataByte;
//    Err:Integer;
begin
  FFunctionCode:=16;      //cteni Holding Registru
  FOffset:=Adr;
  FQuantity:=2;
//  Err:=0;
  Result:=false;
  // Send ONLY ones
  SetLength(Buff,4);
  Buff[0]:=Value[1];
  Buff[1]:=Value[0];
  Buff[2]:=Value[3];
  Buff[3]:=Value[2];
  FWriteValues:=Buff;
  SendAndReadData;
  if FPacketStatus = rnOk then begin
    Result:=true;
  end;
  {While((Err<MAX_ERROR)and(Result=false)) do
  begin
    SetLength(Buff,4);
    Buff[0]:=Value[1];
    Buff[1]:=Value[0];
    Buff[2]:=Value[3];
    Buff[3]:=Value[2];
    FWriteValues:=Buff;
    SendAndReadData;
    if FPacketStatus = rnOk then begin
      Result:=true;
    end else begin
      Inc(Err);
      sleep(50);
    end;
  end;}
  FPacketStatus:=rnNONE;
end;

















procedure TMyTCPModbus.ReCreateConnection;
begin
  // DESTROY AND CREATE
  {$IFDEF LOG}
  WriteLog('RE-CREATE Connection [BEFORE]');
  {$ENDIF}
  FTimer.Enabled := False;
  // Destroy
  Disconnect;
  FreeAndNil(FClientSocket);
  // Create
  FErrorCount := 0;
  FTCPReadSize := 0;
  FTCPWriteSize := 0;
  SetLength(FTCPReadValues, 0);
  FClientSocket := TClientSocket.Create(nil);
  FClientSocket.OnConnect := SocketConnect;
  FClientSocket.OnDisconnect := SocketDisconnect;
  FClientSocket.OnRead := SocketRead;
  FClientSocket.OnError := SocketError;
  FClientSocket.Address := FIPAddress;
  FClientSocket.Port := FIPPort;
  // Connect
  Connect;
  {$IFDEF LOG}
  WriteLog('RE-CREATE Connection [AFTER]');
  {$ENDIF}
end;

procedure TMyTCPModbus.SetStateToError;
begin
  inherited;
  FPacketStatus := rnError;
end;

procedure TMyTCPModbus.ReConnectConnection;
begin
  // DISCONNECT AND CONNECT
  if not FIsReconnecting then begin
    {$IFDEF LOG}
    WriteLog('ReConnectConnection [BEFORE]');
    {$ENDIF}
    FIsReconnecting := True;
    try
      Disconnect;
      Sleep(1000);
      Connect;
    finally
      FIsReconnecting := False;
    end;
    {$IFDEF LOG}
    WriteLog('ReConnectConnection [AFTER]');
    {$ENDIF}
  end;
end;

{ TDataB1List }

function TDataB1List.GetItem(Index: Integer): TDataB1;
begin
  Result := TDataB1(inherited Items[Index]);
end;

{ TMagE1DataloggerFilter }

constructor TMagE1DataloggerFilter.Create();
begin
  inherited Create;
  FDateTimeMin := 0;
  FDateTimeMax := 0;
  FRecordCountMax := DATALOGGER_FILTER_RECORD_COUNT_MAX;
  FDateTimeMinEnabled := false;
  FDateTimeMaxEnabled := false;
  FRecordCountMaxEnabled := false;
end;

function TMagE1DataloggerFilter.TryParseDateTime(const Str: string; var Val: TDateTime) : Boolean;
begin
  result := false;
  try
    Val := StrToDateTime(Str);
    result := true;
  except
  end;
end;

function TMagE1DataloggerFilter.TryParseRecordCountMax(const Str: string; var Val: Cardinal) : Boolean;
var Val64: Int64;
begin
  result := false;
  try
    Val64 := StrToInt64(Str);
    if (Val64 >= 0) and (Val64 <= 2147483647) then
      result := true;
  except
  end;
end;

function TMagE1DataloggerFilter.ConvertDateTimeToStr(Val: TDateTime) : string;
begin
  result := DateTimeToStr(Val);
end;

function TMagE1DataloggerFilter.ConvertRecordCountMaxToStr(Val: Cardinal) : string;
begin
  result := IntToStr(Int64(Val));
end;

function TMagE1DataloggerFilter.GetDateTimeMinAsStr() : string;
begin
  result := ConvertDateTimeToStr(FDateTimeMin);
end;

function TMagE1DataloggerFilter.GetDateTimeMaxAsStr() : string;
begin
  result := ConvertDateTimeToStr(FDateTimeMax);
end;

function TMagE1DataloggerFilter.GetRecordCountMaxAsStr() : string;
begin
  result := ConvertRecordCountMaxToStr(FRecordCountMax);
end;

function TMagE1DataloggerFilter.TrySetDateTimeMin(const Str: string) : Boolean;
begin
  result := TryParseDateTime(Str, FDateTimeMin);
end;

function TMagE1DataloggerFilter.TrySetDateTimeMax(const Str: string) : Boolean;
begin
  result := TryParseDateTime(Str, FDateTimeMax);
end;

function TMagE1DataloggerFilter.TrySetRecordCountMax(const Str: string) : Boolean;
begin
  result := TryParseRecordCountMax(Str, FRecordCountMax);
end;

function TMagE1DataloggerFilter.IsRecordDateTimeValid(RecordDateTime: TDateTime) : Boolean;
begin
  result := true;

  if FDateTimeMinEnabled and (RecordDateTime < FDateTimeMin) then
    result := false;

  if result and FDateTimeMaxEnabled and (RecordDateTime > FDateTimeMax) then
    result := false;
end;

function TMagE1DataloggerFilter.IsRecordCountValid(RecordCount: Cardinal) : Boolean;
begin
  result := true;
  if FRecordCountMaxEnabled and (RecordCount >= FRecordCountMax) then
    result := false;
end;

function TMagE1DataloggerFilter.IsRecordValid(RecordDateTime: TDateTime; RecordCount: Cardinal) : Boolean;
begin
  result := IsRecordDateTimeValid(RecordDateTime) and IsRecordCountValid(RecordCount);
end;

procedure TMagE1DataloggerFilter.LoadFromIniFile(iniF: TIniFile);
begin
  FDateTimeMin := iniF.ReadFloat('Statistic', 'DateTimeMin', 0);
  FDateTimeMax := iniF.ReadFloat('Statistic', 'DateTimeMax', 0);
  FRecordCountMax := iniF.ReadInteger('Statistic', 'RecordCountMax', DATALOGGER_FILTER_RECORD_COUNT_MAX);
  FDateTimeMinEnabled := iniF.ReadBool('Statistic', 'DateTimeMinEnabled', false);
  FDateTimeMaxEnabled := iniF.ReadBool('Statistic', 'DateTimeMaxEnabled', false);
  FRecordCountMaxEnabled := iniF.ReadBool('Statistic', 'RecordCountMaxEnabled', false);
end;

procedure TMagE1DataloggerFilter.SaveToIniFile(iniF: TIniFile);
begin
  iniF.WriteFloat('Statistic', 'DateTimeMin', FDateTimeMin);
  iniF.WriteFloat('Statistic', 'DateTimeMax', FDateTimeMax);
  iniF.WriteInteger('Statistic', 'RecordCountMax', FRecordCountMax);
  iniF.WriteBool('Statistic', 'DateTimeMinEnabled', FDateTimeMinEnabled);
  iniF.WriteBool('Statistic', 'DateTimeMaxEnabled', FDateTimeMaxEnabled);
  iniF.WriteBool('Statistic', 'RecordCountMaxEnabled', FRecordCountMaxEnabled);
end;

end.

