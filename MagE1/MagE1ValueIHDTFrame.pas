unit MagE1ValueIHDTFrame;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  MagE1ValueIHexFrame, ExtCtrls, StdCtrls;

// IHDT = Info Hex DateTime

type
  TframeMagE1ValueIHDT = class(TframeMagE1ValueIHex)
  private
    { Private declarations }
  public
    { Public declarations }
    function GetValueText(Value:Integer): String; override;
  end;

implementation

{$R *.DFM}

{ TframeMagE1ValueIHDT }

function TframeMagE1ValueIHDT.GetValueText(Value: Integer): String;
begin
  Result := Format('%.8x', [Value]);
  Insert(':', Result, 7);
  Insert(' ', Result, 5);
  Insert('/', Result, 3);
end;

end.
