unit MagE1StatRpt;

interface

uses Windows, SysUtils, Messages, Classes, Graphics, Controls,
  StdCtrls, ExtCtrls, Forms, Quickrpt, QRCtrls, MagE1MyCommunicationClass;

type
  TqrMagE1Stat = class(TQuickRep)
    DetailBand1: TQRBand;
    TitleBand1: TQRBand;
    qrlTitle: TQRLabel;
    ColumnHeaderBand1: TQRBand;
    qrlDateTitle: TQRLabel;
    qrlTimeTitle: TQRLabel;
    qrlTotalTitle: TQRLabel;
    qrlDate: TQRLabel;
    qrlTime: TQRLabel;
    qrlTotal: TQRLabel;
    procedure QuickRepNeedData(Sender: TObject; var MoreData: Boolean);
    procedure QuickRepBeforePrint(Sender: TCustomQuickRep;
      var PrintReport: Boolean);
  private
    FIndex: Integer;
    FDataB1ListRef: TDataB1List;
  public
    constructor CreateRep(AOwner : TComponent; ADataB1ListRef: TDataB1List; ADemo: Boolean);
    procedure TranslateReport;
  end;

implementation

uses MagE1FunctionsUnit, MagE1DataClass, MagE1TranslationUnit;

{$R *.DFM}

procedure TqrMagE1Stat.QuickRepNeedData(Sender: TObject;
  var MoreData: Boolean);
begin
  if (FIndex < FDataB1ListRef.Count) then begin
    qrlDate.Caption := FormatStatDate(FDataB1ListRef[FIndex].DateTime);
    qrlTime.Caption := FormatStatTime(FDataB1ListRef[FIndex].DateTime);
    qrlTotal.Caption := Format('%.6f', [FDataB1ListRef[FIndex].Total]);
  end;
  MoreData := (FIndex < FDataB1ListRef.Count);
  Inc(FIndex);
end;

procedure TqrMagE1Stat.QuickRepBeforePrint(Sender: TCustomQuickRep;
  var PrintReport: Boolean);
begin
  FIndex := 0;
end;

constructor TqrMagE1Stat.CreateRep(AOwner: TComponent; ADataB1ListRef: TDataB1List; ADemo: Boolean);
begin
  inherited Create(AOwner);
  TranslateReport;
  FDataB1ListRef := ADataB1ListRef;
  if ADemo then begin
    qrlTitle.Caption := qrlTitle.Caption + ' (DEMO)';
  end;
end;

procedure TqrMagE1Stat.TranslateReport;
begin
  qrlTitle.Caption := GetTranslationText('DATALOGGER', qrlTitle.Caption);
  qrlDateTitle.Caption := GetTranslationText('DATE', qrlDateTitle.Caption);
  qrlTimeTitle.Caption := GetTranslationText('TIME', qrlTimeTitle.Caption);
  qrlTotalTitle.Caption := GetTranslationText('STAT_TOTAL', qrlTotalTitle.Caption);
end;

end.
