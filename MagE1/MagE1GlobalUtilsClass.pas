unit MagE1GlobalUtilsClass;

interface

uses Sysutils,INIFiles,MagE1MyCommunicationClass,classes, Windows,
	 controls, shlobj, shellapi, Forms;

const
  defCOMPortName = 'COM1';
  defMenuValuesOffset = 0;
  {$EXTERNALSYM CSIDL_LOCAL_APPDATA}
  CSIDL_LOCAL_APPDATA = $001C;

resourcestring
  STR_READ_ERROR = 'Reading error';
  STR_WRITE_ERROR = 'Write error';
  STR_COMMUNICATION_ERROR = 'Communication error';
  STR_UNKNOWN_COMM_PROTOCOL = 'Unknown communications protokol';
  STR_UNSUPPORTED_FW = 'Unsupported firmware';
  STR_INCORRECT_PASSWORD = 'Incorrect password';

  STR_READ_SUCC = 'Reading successfully';
  STR_READ_FAIL = 'Reading failed';
  STR_LOAD_PROG_STRUCT = 'Loading program structure...';
  STR_UNSUPPORTED_COM_PROTOCOL = 'Unsuported communications protokol';
  STR_INTERNAL_ERROR = 'Internal error';
  STR_USER_PSW = 'User password';
  STR_RTC_NOT_AVAIL = 'RTC modul is not available';
  STR_PLS_WAIT = 'Please wait ...';
  STR_UNKNOWN_CALIB_POINT = 'Unknown calibration point';
  STR_CALIB_POINT_WRITE_SUCC = 'Calibration point %d successfully writed';
  STR_CALIB_POINT_WRITE_FAIL = 'Calibration point %d writed failed';
  STR_ZERO_FLOW_WRITE_SUCC = 'Zero Flow successfully writed';
  STR_ZERO_FLOW_WRITE_FAIL = 'Zero Flow writed failed';
  STR_UNKNOWN_MEAS_POINT = 'Unknown measurement point';
  STR_MEAS_POINT_WRITE_SUCC = 'Measurment point %d successfully writed';
  STR_MEAS_POINT_WRITE_FAIL = 'Measurment point %d writed failed';
  STR_MEAS_POINTS_WRITE_SUCC = 'Measurment points successfully writed';
  STR_MEAS_POINTS_WRITE_FAIL = 'Measurment points writed failed';
  STR_FW_UPDATE_FILTER = 'Firmware update files|*.mgb|All files|*.*';
  STR_FW_UPDATE_CONF = 'Do you really want to update firmware from "%s" file?';
  STR_FW_UPDATE_DONE = 'Firmware update succesfuly finished!';
  STR_FW_UPDATE_FAIL = 'Firmware update failed!';
  STR_FW_UPDATE_SKIP = 'Firmware update skipped (not started) - communication port error!';
  STR_APP_RESTART    = 'MagE1 application will be restarted.';

function GetCOM : String;
procedure SetCOM(COMPortName : String);

function GetMenuValuesOffset : Integer;
procedure SetMenuValuesOffset(Offset : Integer);

function GetXlsPath : String;
procedure SetXlsPath( Path : String);

function GetXlsValues : String;
procedure SetXlsValues( Path : String);

function GetAddress(ProtokolType:TProtokolType;Name:string;Values:TStringList):Integer;

var Konec: Boolean;

implementation

(*
var
  _SHGetSpecialFolderPathA: function(hwndOwner: HWND; lpszPath: PAnsiChar;
    nFolder: Integer; fCreate: BOOL): BOOL; stdcall;
*)

function GetCOM : String;
var iniF : TINIFile;
begin
  iniF:=TINIFile.Create(ChangeFileExt(ParamStr(0),'.ini'));
  try
    Result:=iniF.ReadString('COMPort','Name',defCOMPortName);
  finally
    iniF.Free;
  end;
end;

procedure SetCOM(COMPortName : String);
var iniF : TINIFile;
begin
  iniF:=TINIFile.Create(ChangeFileExt(ParamStr(0),'.ini'));
  try
    iniF.WriteString('COMPort','Name',COMPortName);
  finally
    iniF.Free;
  end;
end;

function GetMenuValuesOffset : Integer;
var iniF : TINIFile;
begin
  iniF:=TINIFile.Create(ChangeFileExt(ParamStr(0),'.ini'));
  try
    Result:=iniF.ReadInteger('MenuValues','Offset',defMenuValuesOffset);
  finally
    iniF.Free;
  end;
end;

procedure SetMenuValuesOffset(Offset : Integer);
var iniF : TINIFile;
begin
  iniF:=TINIFile.Create(ChangeFileExt(ParamStr(0),'.ini'));
  try
    iniF.WriteInteger('MenuValues','Offset',Offset);
  finally
    iniF.Free;
  end;
end;

function GetXlsPath : String;
var iniF : TINIFile;
begin
  iniF:=TINIFile.Create(ChangeFileExt(ParamStr(0),'.ini'));
  try
    Result:=iniF.ReadString('Paths','Xls','Cesta k souboru .Xls');
  finally
    iniF.Free;
  end;
end;

procedure SetXlsPath( Path : String);
var iniF : TINIFile;
begin
  iniF:=TINIFile.Create(ChangeFileExt(ParamStr(0),'.ini'));
  try
    iniF.WriteString('Paths','Xls', Path);
  finally
    iniF.Free;
  end;
end;

function GetXlsValues : String;
var iniF : TINIFile;
begin
  iniF:=TINIFile.Create(ChangeFileExt(ParamStr(0),'.ini'));
  try
    Result:=iniF.ReadString('Paths','Values','Cesta k souboru .Dat');
  finally
    iniF.Free;
  end;
end;

procedure SetXlsValues( Path : String);
var iniF : TINIFile;
begin
  iniF:=TINIFile.Create(ChangeFileExt(ParamStr(0),'.ini'));
  try
    iniF.WriteString('Paths','Values', Path);
  finally
    iniF.Free;
  end;
end;

function GetAddress(ProtokolType:TProtokolType;Name:string;Values:TStringList):Integer;
var tmp:Integer;
begin
  result:=-1;
  case ProtokolType of
    ptMagX1:
    begin
      result:=4*Values.IndexOf(Name);
      if result<0 then
        Raise Exception.Create('Invalid address');
    end;

    ptTCPModbus, ptModbus, ptDemo:
    begin
      tmp:=Values.IndexOf(Name);
      if tmp>=0 then
        result:=StrToInt(Values.Strings[tmp+1])-1;
      if result<0 then
        Raise Exception.Create('Invalid address');
    end;
  end;
end;

end.
