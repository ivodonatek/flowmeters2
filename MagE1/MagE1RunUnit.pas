unit MagE1RunUnit;

interface

procedure MagE1Run;

implementation

uses sysutils, controls, Dialogs, Forms, MagE1TranslationUnit,
  MagE1GlobalUtilsClass, MagE1MyCommunicationClass,
  MagE1StatFrm, MagE1EnterFrm, MagE1MenuFrm;

var
    Demo : Boolean;
    ReadOK:Boolean;
    firmwareNO:integer;
    MyCommunication : TMyCommunication;

function GetDeviceFirmwareNumber(AProtokolType:TProtokolType;ASlaveID,AComNumber:Cardinal;
   BaudRate:TBaudRate;StopBits:TStopBits;Parity:TParity;Timeout:Integer;
   RtsControl:TRtsControl;EnableDTROnOpen:Boolean;ASwitchWait:Cardinal;
   AIPAddress: String; APort: Integer):integer;
var firmwareNO:integer;
begin
    MyCommunication.SetCommunicationParam(AComNumber,ASlaveID,BaudRate,db8BITS,
        StopBits,Parity,RtsControl,EnableDTROnOpen,Timeout,AIPAddress,APort);
    if not MyCommunication.Connect() then
        raise Exception.Create('Comunication port error');
    //cti verzi FW
    repeat
      ReadOK:=MyCommunication.ReadFirmNO(Cardinal(firmwareNO));
    until (ReadOK)or(MessageDlg(GetTranslationText('READ_ERROR', STR_READ_ERROR), mtError, [mbRetry, mbAbort], 0) = mrAbort);
    if not ReadOK then
        raise Exception.Create('No device was detected');
    Result:=firmwareNO;
end;

procedure MagE1Run;
var
  statForm: TformMagE1Stat;
  menuForm: TformMagE1Menu;
begin
    MyCommunication:= nil;
    statForm:= nil;
    menuForm:= nil;

    try
      case formMagE1Enter.ShowModal of
        //statistic
        mrOK:
        begin
              case formMagE1Enter.ProtokolType of
                ptMagX1:MyCommunication:=TMyCommPort.Create();
                ptModbus:MyCommunication:=TMyModbus.Create();
                ptTCPModbus:MyCommunication:=TMyTCPModbus.Create();
                ptDemo:MyCommunication:=TDemoCommunication.Create();
              else
                raise Exception.Create('Unsuported protocol');
              end;
              //cti verzi firmware
              firmwareNO := GetDeviceFirmwareNumber(formMagE1Enter.ProtokolType,formMagE1Enter.SlaveID,
                      formMagE1Enter.ComPortNumber,formMagE1Enter.BaudRate,formMagE1Enter.StopBits,
                      formMagE1Enter.Parity,formMagE1Enter.Timeout,formMagE1Enter.RtsControl ,true,
                      formMagE1Enter.rgConvertorRS485.ItemIndex, formMagE1Enter.edtTCPIPAddress.Text,
                      formMagE1Enter.seTCPPort.Value);

              statForm := TformMagE1Stat.Create(Application);
              statForm.Caption := 'MagE1 statistic';
              statForm.Init(MyCommunication, firmwareNO);
        end;
        //Service
        mrNo:
        begin
              case formMagE1Enter.ProtokolType of
                ptMagX1:MyCommunication:=TMyCommPort.Create();
                ptModbus:MyCommunication:=TMyModbus.Create();
                ptTCPModbus:MyCommunication:=TMyTCPModbus.Create();
                ptDemo:MyCommunication:=TDemoCommunication.Create();
              else
                raise Exception.Create('Unsuported protocol');
              end;
              //cti verzi firmware
              firmwareNO := GetDeviceFirmwareNumber(formMagE1Enter.ProtokolType,formMagE1Enter.SlaveID,
                      formMagE1Enter.ComPortNumber,formMagE1Enter.BaudRate,formMagE1Enter.StopBits,
                      formMagE1Enter.Parity,formMagE1Enter.Timeout,formMagE1Enter.RtsControl ,true,
                      formMagE1Enter.rgConvertorRS485.ItemIndex, formMagE1Enter.edtTCPIPAddress.Text,
                      formMagE1Enter.seTCPPort.Value);

              menuForm := TformMagE1Menu.Create(Application);
              menuForm.Caption := 'MagE1 service';
              menuForm.Init(MyCommunication, firmwareNO, formMagE1Enter.ProtokolType);
        end;
        //Close
        mrCancel:
        begin
        end;
      end;

    except
      on E: Exception do
      begin
        MessageDlg(E.Message,mtError, [mbOk], 0);
        if MyCommunication <> nil then begin
          MyCommunication.Disconnect;
          FreeAndNil(MyCommunication);
        end;
        if statForm <> nil then statForm.Close;
        if menuForm <> nil then menuForm.Close;        
      end;
    end;
end;

end.
