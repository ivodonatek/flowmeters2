unit NodeClass;

//$DEFINE Menu105}

interface

uses Contnrs, Classes, StdCtrls, ComObj, Dialogs, SysUtils, ComCtrls, Forms, Constants,
     FunctionsUnit, TntClasses;

const
  TYPES_COUNT = 14;
  Popisky : array[-1 .. TYPES_COUNT - 1] of String = ('NONE','Menu','Info','Edit','4Win','ISel','Bool','Time','Date','CalW','Pasw','Func','IHex','IBin','IHDT');
  Jednotky : array[0..21] of String = ('-','UKG/min','USG/min','m3/h','l/min','l/s','UKG','USG','m3','min','s','%','ms','Hz','mm','mA','mH','l','V','�C','bar','W');

type
  TNodeString = String[100];

  TNodeRecord = record
    Id,IdParent,Typ,Digits,Decimal,Units,Digits2,Decimal2,Units2,Count,FIn,FOut: Integer;
    Popis,ValueName,ValueName2,ValueName3,ValueName4 : TNodeString;
    ModbusAdr,ModbusAdr2,ModbusAdr3,ModbusAdr4:Integer;
    Value,Value2,Value3,Value4,Min,Max,Min2,Max2 : Double;
    MultiTextsCount : Integer;
  end;

  TNode = class;
  TNodeText = class;

  TNodes = class(TObjectList)
  public
    LanguageIndex : Integer;
    ConvertWideCharsPage : PConvertWideChars;
    IselFlow:array[0..12]of string;
    function GetIsel(value:word):string;
    class procedure FilterString(var S : String);
    class function GetTypFromText(Text : String) : Integer;
    class function GetStringFromMultiLang(MultiLang : TStringList) : String;
    class function GetIntegerFromValueAndDecimals(Value : Double; Decimal : Integer) : Integer;
    class function ReplaceCharWithAnother(Str : String; RepCh,WithCh : Char) : String;
    procedure WriteToListBox(listBox : TListBox);
    procedure LoadFromXLS(fileName : String; ProgBar : TProgressBar);
    procedure FillRecord(var NodeRec : TNodeRecord; Node : TNode);
    procedure FillNode(Node : TNode; NodeRec : TNodeRecord);
    procedure WriteMultiTexts(Stream : TStream; Node : TNode);
    procedure ReadMultiTexts(Stream : TStream; Node : TNode; MTCnt : Integer);
    procedure LoadFromFile(fileName : String; ProgBar : TProgressBar);
    procedure LoadFromStream(Stream : TStream; ProgBar : TProgressBar);
    procedure SaveToFile(fileName : String; ProgBar : TProgressBar);
    procedure SaveToStream(Stream : TStream; ProgBar : TProgressBar);
  end;
  TNode = class
  protected
    function GetPopis: WideString;
    procedure SetPopis(AValue: WideString);
  public
    LanguageIndex : Integer;
    ConvertWideCharsPage : PConvertWideChars;
    Id,IdParent, Typ,Digits,Decimal,Units,Digits2,Decimal2,Units2,Count,FIn,FOut: Integer;
    NameParent,NameGrandParent, DefPopis,ValueName,ValueName2,ValueName3,ValueName4 : String;
    ModbusAdr,ModbusAdr2,ModbusAdr3,ModbusAdr4:Integer;
    Value,Value2,Value3,Value4,Min,Max,Min2,Max2 : Double;
    MultiTexts : TObjectList;
    property Popis: WideString read GetPopis write SetPopis;
    constructor Create;
    destructor Destroy; override;
  end;
  TNodeText = class
  public                                      
    MultiText : TTntStringList;
    constructor Create;
    destructor Destroy; override;
  end;
  TID = class
  public
    Id : Integer;
    TreeNode : TTreeNode;
  end;
var
    IselFlow: TStringList;

implementation

//uses TranslationUnit;

{ TNodes }

class procedure TNodes.FilterString(var S : String);
var I,J : Integer;
begin
  for I:=1 to Length(S) do begin
    for J:=Low(Znaky) to High(Znaky) do begin
      if S[I]=Znaky[J].Ch then
        S[I]:=Chr(Znaky[J].B);
    end;
  end;
end;

class function TNodes.GetIntegerFromValueAndDecimals(Value: Double;
  Decimal: Integer): Integer;
var I : Integer;
    Pom : Double;
begin
  Pom:=Value;
  for I:=1 to Decimal do Pom:=Pom*10;
  Result:=Round(Pom);
end;

class function TNodes.ReplaceCharWithAnother(Str: String; RepCh,
  WithCh: Char): String;
begin
  Result:=Str;
  while Pos(RepCh,Result)>0 do Result[Pos(RepCh,Result)]:=WithCh;
end;

class function TNodes.GetStringFromMultiLang(
  MultiLang: TStringList): String;
var I : Integer;
begin
  Result:='';
  if MultiLang.Count>0 then begin
    Result:=ReplaceCharWithAnother(MultiLang[0],'|',';');
    for I:=1 to MultiLang.Count-1 do begin
      Result:=Result+'#'+ReplaceCharWithAnother(MultiLang[I],'|',';');
    end;
  end;
end;

class function TNodes.GetTypFromText(Text: String): Integer;
var I : Integer;
begin
  Result:=-1;
  for I:=0 to TYPES_COUNT - 1 do begin
    if UpperCase(Text)=UpperCase(Popisky[I]) then begin
      Result := I;
      Exit;
    end;
  end;
end;

function Tnodes.GetIsel(value:word):string;
begin
    beep;
end;

procedure TNodes.LoadFromXLS(fileName: String; ProgBar : TProgressBar);
var ExcelApp : Variant;
    Lang,StartX,StartY,I,J : Integer;
    Row: Integer;
    Node : TNode;
    NodeText : TNodeText;
    X_Active,X_ID,X_Parent,X_Typ,X_Text,X_Digits,X_Decimal,X_Min,X_Max,X_Value,
    X_Units,X_Count,X_FIn,X_FOut,X_ValueName,X_ModbusAdr: Integer;
    bFinished : Boolean;
    S : String;

    StringList : TStrings;      //kvuli duplicite ValueName
    postfix:Integer;
    tmpS:String;
begin
  Clear;
  bFinished:=False;
  if Assigned(ProgBar) then begin
    ProgBar.Visible:=True;
    ProgBar.Position:=0;
    ProgBar.Max:=1000;
  end;
  ExcelApp:=CreateOleObject('Excel.Application');
  StringList:=TStringList.Create;

  if not VarIsEmpty(ExcelApp) then begin
    ExcelApp.Workbooks.Open(fileName);
    StartX:=StrToIntDef(ExcelApp.ActiveWorkbook.Sheets.Item[2].Cells.Item[1,2],1);
    StartY:=StrToIntDef(ExcelApp.ActiveWorkbook.Sheets.Item[2].Cells.Item[2,2],1);
    Lang:=StrToIntDef(ExcelApp.ActiveWorkbook.Sheets.Item[2].Cells.Item[3,2],1);
    Row:=StartY;
    {Init}
    X_Active    := StartX+18+Lang-1;
    X_ID        := StartX+1;
    X_Parent    := StartX+2;
    X_Typ       := StartX+3;
    X_Text      := StartX+4;
    X_Digits    := StartX+5+Lang-1;
    X_Decimal   := StartX+6+Lang-1;
    X_Min       := StartX+7+Lang-1;
    X_Max       := StartX+8+Lang-1;
    X_Value     := StartX+9+Lang-1;
    X_Units     := StartX+10+Lang-1;
    X_Count     := StartX+11+Lang-1;
    X_FIn       := StartX+12+Lang-1;
    X_FOut      := StartX+13+Lang-1;
    X_ValueName := StartX+14+Lang-1;
    X_ModbusAdr := StartX+19+Lang-1;
    {---}
    While (not bFinished) and (not VarIsEmpty(ExcelApp.ActiveWorkbook.Sheets.Item[1].Cells.Item[Row,X_ID])) and (Row<1000) Do Begin
      if Assigned(ProgBar) then begin
        ProgBar.Position:=Row;
        Application.ProcessMessages;
      end;
      I:=StrToIntDef(ExcelApp.ActiveWorkbook.Sheets.Item[1].Cells.Item[Row,X_Active],0);
      If I=1 Then begin
        Node:=TNode.Create;
        Node.Typ:=0;
        Try
          Node.Id:=ExcelApp.ActiveWorkbook.Sheets.Item[1].Cells.Item[Row,X_ID];
          Node.IdParent:=ExcelApp.ActiveWorkbook.Sheets.Item[1].Cells.Item[Row,X_Parent];
          Node.NameParent:=ExcelApp.ActiveWorkbook.Sheets.Item[1].Cells.Item[row - Node.Id + Node.IdParent, X_Text];
          Node.FIn:=ExcelApp.ActiveWorkbook.Sheets.Item[1].Cells.Item[Row,X_FIn];
          Node.FOut:=ExcelApp.ActiveWorkbook.Sheets.Item[1].Cells.Item[Row,X_FOut];
          Node.Typ:=GetTypFromText(ExcelApp.ActiveWorkbook.Sheets.Item[1].Cells.Item[Row,X_Typ]);
          Node.IdParent:=ExcelApp.ActiveWorkbook.Sheets.Item[1].Cells.Item[Row,X_Parent];
          Node.Digits:=StrToIntDef(ExcelApp.ActiveWorkbook.Sheets.Item[1].Cells.Item[Row,X_Digits],0);
          Node.Decimal:=StrToIntDef(ExcelApp.ActiveWorkbook.Sheets.Item[1].Cells.Item[Row,X_Decimal],0);
          Node.ModbusAdr:=StrToIntDef(ExcelApp.ActiveWorkbook.Sheets.Item[1].Cells.Item[Row,X_ModbusAdr],0);
          If Node.Typ in [2,3,8,9] Then Begin
            Node.Min:=ExcelApp.ActiveWorkbook.Sheets.Item[1].Cells.Item[Row,X_Min];
            Node.Max:=ExcelApp.ActiveWorkbook.Sheets.Item[1].Cells.Item[Row,X_Max];
          End;
          Node.Value:=ExcelApp.ActiveWorkbook.Sheets.Item[1].Cells.Item[Row,X_Value];
          Node.Units:=StrToIntDef(ExcelApp.ActiveWorkbook.Sheets.Item[1].Cells.Item[Row,X_Units],0);
          {ValueName}
          S:=ExcelApp.ActiveWorkbook.Sheets.Item[1].Cells.Item[Row,X_ValueName];
          FilterString(S);
{$IFDEF Menu105}
        Node.ValueName:=S;
{$ELSE}
          if S='' then
          begin
            Node.ValueName:=S;
          end
          else if (StringList.IndexOf(S)<0) then       //hledej duplicitni nazvy
          begin
            StringList.Add(S);
            Node.ValueName:=S;
          end
          else
          begin
            postfix:=0;
            tmpS:=S+'0';
            while (StringList.IndexOf(tmpS)>=0) do
            begin
              Inc(postfix);
              tmpS:=S+IntToStr(postfix);
            end;
            StringList.Add(tmpS);
            Node.ValueName:=tmpS;
          end;
{$ENDIF}
          If Node.Typ=4 Then Node.Count:=ExcelApp.ActiveWorkbook.Sheets.Item[1].Cells.Item[Row,X_Count] Else Node.Count:=0;
          {Popis}
          S:=ExcelApp.ActiveWorkbook.Sheets.Item[1].Cells.Item[Row,X_Text];
          FilterString(S);
          Node.Popis:=S;
          {Texts}
          NodeText:=TNodeText.Create;
          For I:=0 To Lang-1 Do Begin
            S:=ExcelApp.ActiveWorkbook.Sheets.Item[1].Cells.Item[Row,X_Text+I];
            FilterString(S);
            NodeText.MultiText.Add(S);
         End;
          Node.MultiTexts.Add(NodeText);

        Except
          ShowMessage('Error on LINE '+IntToStr(Row));
        End;
        {---}
        Inc(Row);
        Case Node.Typ Of
         3,8 : Begin {4WIN}
                 Try
                   Node.Value2:=ExcelApp.ActiveWorkbook.Sheets.Item[1].Cells.Item[Row,X_Value];
                   Node.ModbusAdr2:=StrToIntDef(ExcelApp.ActiveWorkbook.Sheets.Item[1].Cells.Item[Row,X_ModbusAdr],0);
                   {ValueName2}
                   S:=ExcelApp.ActiveWorkbook.Sheets.Item[1].Cells.Item[Row,X_ValueName];
                   FilterString(S);
//                   Node.ValueName2:=S;
{$IFDEF Menu105}
      Node.ValueName2:=S;
{$ELSE}
                   if S='' then
                    begin
                      Node.ValueName2:=S;
                    end
                    else if StringList.IndexOf(S)<0 then       //hledej duplicitni nazvy
                    begin
                      StringList.Add(S);
                      Node.ValueName2:=S;
                    end
                    else
                    begin
                      postfix:=0;
                      tmpS:=S+'0';
                      while (StringList.IndexOf(tmpS)>=0) do
                      begin
                        Inc(postfix);
                        tmpS:=S+IntToStr(postfix);
                      end;
                      StringList.Add(S);
                      Node.ValueName2:=S;
                    end;
{$ENDIF}
                 Except
                   ShowMessage('Error on LINE '+IntToStr(Row));
                 End;
                 Inc(Row);
                 Try
                   Node.Digits2:=StrToIntDef(ExcelApp.ActiveWorkbook.Sheets.Item[1].Cells.Item[Row,X_Digits],0);
                   Node.Decimal2:=StrToIntDef(ExcelApp.ActiveWorkbook.Sheets.Item[1].Cells.Item[Row,X_Decimal],0);
                   Node.Min2:=StrToIntDef(ExcelApp.ActiveWorkbook.Sheets.Item[1].Cells.Item[Row,X_Min],0);
                   Node.Max2:=StrToIntDef(ExcelApp.ActiveWorkbook.Sheets.Item[1].Cells.Item[Row,X_Max],0);
                   Node.ModbusAdr3:=StrToIntDef(ExcelApp.ActiveWorkbook.Sheets.Item[1].Cells.Item[Row,X_ModbusAdr],0);

                   Node.Units2:=StrToIntDef(ExcelApp.ActiveWorkbook.Sheets.Item[1].Cells.Item[Row,X_Units],0);
                   Node.Value3:=ExcelApp.ActiveWorkbook.Sheets.Item[1].Cells.Item[Row,X_Value];
                   {ValueName3}
                   S:=ExcelApp.ActiveWorkbook.Sheets.Item[1].Cells.Item[Row,X_ValueName];
                   FilterString(S);
{$IFDEF Menu105}
      Node.ValueName3:=S;
{$ELSE}
                   if S='' then
                    begin
                      Node.ValueName3:=S;
                    end
                    else if StringList.IndexOf(S)<0 then       //hledej duplicitni nazvy
                    begin
                      StringList.Add(S);
                      Node.ValueName3:=S;
                    end
                    else
                    begin
                      postfix:=0;
                      tmpS:=S+'0';
                      while (StringList.IndexOf(tmpS)>=0) do
                      begin
                        Inc(postfix);
                        tmpS:=S+IntToStr(postfix);
                      end;
                      StringList.Add(S);
                      Node.ValueName3:=S;
                    end;
{$ENDIF}
                   {Texts}
                   NodeText:=TNodeText.Create;
                   For I:=0 To Lang-1 Do Begin
                     S:=ExcelApp.ActiveWorkbook.Sheets.Item[1].Cells.Item[Row,X_Text+I];
                     FilterString(S);
                     NodeText.MultiText.Add(S);
                   End;
                   Node.MultiTexts.Add(NodeText);
                   {---}
                 Except
                   ShowMessage('Error on LINE '+IntToStr(Row));
                 End;
                 Inc(Row);
                 Try
                   Node.Value4:=ExcelApp.ActiveWorkbook.Sheets.Item[1].Cells.Item[Row,X_Value];
                   Node.ModbusAdr4:=StrToIntDef(ExcelApp.ActiveWorkbook.Sheets.Item[1].Cells.Item[Row,X_ModbusAdr],0);
                   {ValueName4}
                   S:=ExcelApp.ActiveWorkbook.Sheets.Item[1].Cells.Item[Row,X_ValueName];
                   FilterString(S);
{$IFDEF Menu105}
      Node.ValueName4:=S;
{$ELSE}
                   if S='' then
                    begin
                      Node.ValueName4:=S;
                    end
                    else if StringList.IndexOf(S)<0 then       //hledej duplicitni nazvy
                    begin
                      StringList.Add(S);
                      Node.ValueName4:=S;
                    end
                    else
                    begin
                      postfix:=0;
                      tmpS:=S+'0';
                      while (StringList.IndexOf(tmpS)>=0) do
                      begin
                        Inc(postfix);
                        tmpS:=S+IntToStr(postfix);
                      end;
                      StringList.Add(S);
                      Node.ValueName4:=S;
                    end;
{$ENDIF}
                 Except
                   ShowMessage('Error on LINE '+IntToStr(Row));
                 End;
                 Inc(Row);
               End;
           4 : Begin {ISEL}
                 For J:=1 To Node.Count Do Begin
                   Try
                     {Texts}
                     NodeText:=TNodeText.Create;
                     For I:=0 To Lang-1 Do Begin
                       S:=ExcelApp.ActiveWorkbook.Sheets.Item[1].Cells.Item[Row,X_Text+I];
                       FilterString(S);
                       NodeText.MultiText.Add(S);
                     End;
                     Node.MultiTexts.Add(NodeText);
                     {---}
                   Except
                     ShowMessage('Error on LINE '+IntToStr(Row));
                   End;
                   Inc(Row);
                 End;
               End;
        End;
        Add(Node);
      End Else Begin
        If I=2 Then bFinished:=True;
        Inc(Row);
      End;
    End;
    ExcelApp.Quit;
  end;
  if Assigned(ProgBar) then ProgBar.Visible:=False;
  StringList.Free;
end;

procedure TNodes.WriteToListBox(listBox: TListBox);
var I : Integer;
begin
  listBox.Clear;
  For I:=0 To Count-1 Do Begin
    listBox.Items.Add(Format('%.3d %.3d ',[TNode(Items[I]).Id,TNode(Items[I]).IdParent])+'['+Popisky[TNode(Items[I]).Typ]+'] '+TNode(Items[I]).Popis);
  End;
end;


procedure TNodes.LoadFromFile(fileName: String; ProgBar: TProgressBar);
var FS : TFileStream;
begin
  FS := TFileStream.Create(fileName,fmOpenRead);
  try
    LoadFromStream(FS,ProgBar);
  finally
    FS.Free;
  end;
end;

procedure TNodes.SaveToFile(fileName: String; ProgBar: TProgressBar);
var FS : TFileStream;
begin
  FS := TFileStream.Create(fileName,fmCreate);
  try
    SaveToStream(FS,ProgBar);
  finally
    FS.Free;
  end;
end;

procedure TNodes.FillRecord(var NodeRec: TNodeRecord; Node: TNode);
begin
  NodeRec.Id := Node.Id;
  NodeRec.IdParent := Node.IdParent;
  NodeRec.Typ := Node.Typ;
  NodeRec.Digits := Node.Digits;
  NodeRec.Decimal := Node.Decimal;
  NodeRec.Units := Node.Units;
  NodeRec.Digits2 := Node.Digits2;
  NodeRec.Decimal2 := Node.Decimal2;
  NodeRec.Units2 := Node.Units2;
  NodeRec.Count := Node.Count;
  NodeRec.FIn := Node.FIn;
  NodeRec.FOut := Node.FOut;
  NodeRec.Popis := Node.Popis;
  NodeRec.ValueName := Node.ValueName;
  NodeRec.ValueName2 := Node.ValueName2;
  NodeRec.ValueName3 := Node.ValueName3;
  NodeRec.ValueName4 := Node.ValueName4;
  NodeRec.Value := Node.Value;
  NodeRec.Value2 := Node.Value2;
  NodeRec.Value3 := Node.Value3;
  NodeRec.Value4 := Node.Value4;
  NodeRec.Min := Node.Min;
  NodeRec.Max := Node.Max;
  NodeRec.Min2 := Node.Min2;
  NodeRec.Max2 := Node.Max2;
  NodeRec.MultiTextsCount := Node.MultiTexts.Count;
  NodeRec.ModbusAdr := Node.ModbusAdr;
  NodeRec.ModbusAdr2 := Node.ModbusAdr2;
  NodeRec.ModbusAdr3 := Node.ModbusAdr3;
  NodeRec.ModbusAdr4 := Node.ModbusAdr4;
end;

procedure TNodes.WriteMultiTexts(Stream: TStream; Node: TNode);
var I, J, Cnt : Integer;
    S : TNodeString;
begin
  for I := 0 to Node.MultiTexts.Count-1 do begin
    Cnt := TNodeText(Node.MultiTexts.Items[I]).MultiText.Count;
    Stream.Write(Cnt,SizeOf(Cnt));
    for J := 0 to TNodeText(Node.MultiTexts.Items[I]).MultiText.Count-1 do begin
      S := TNodeText(Node.MultiTexts.Items[I]).MultiText.Strings[J];
      Stream.Write(S,SizeOf(S));
    end;
  end;
end;

procedure TNodes.LoadFromStream(Stream: TStream; ProgBar: TProgressBar);
var NR : TNodeRecord;
    Node : TNode;
begin
  if Assigned(ProgBar) then begin
    ProgBar.Visible := True;
    ProgBar.Max := Stream.Size;
  end;
  try
    while Stream.Read(NR,SizeOf(NR)) = SizeOf(NR) do begin
      Node := TNode.Create;
      FillNode(Node,NR);
      ReadMultiTexts(Stream,Node,NR.MultiTextsCount);
      Add(Node);
      if Assigned(ProgBar) then begin
        ProgBar.Position := Stream.Position;
        Application.ProcessMessages;
      end;
    end;
  finally
    if Assigned(ProgBar) then begin
      ProgBar.Visible := False;
    end;
  end;
end;

procedure TNodes.SaveToStream(Stream: TStream; ProgBar: TProgressBar);
var I : Integer;
    NR : TNodeRecord;
begin
  if Assigned(ProgBar) then begin
    ProgBar.Visible := True;
    ProgBar.Max := Count;
  end;
  try
    for I:=0 to Count-1 do begin
      FillRecord(NR,TNode(Items[I]));
      Stream.Write(NR,SizeOf(NR));
      WriteMultiTexts(Stream,TNode(Items[I]));
      if Assigned(ProgBar) then begin
        ProgBar.Position := I+1;
        Application.ProcessMessages;
      end;
    end;
  finally
    if Assigned(ProgBar) then begin
      ProgBar.Visible := False;
    end;
  end;
end;

procedure TNodes.FillNode(Node: TNode; NodeRec: TNodeRecord);
begin
  Node.LanguageIndex := LanguageIndex;
  Node.ConvertWideCharsPage := ConvertWideCharsPage; 
  Node.Id := NodeRec.Id;
  Node.IdParent := NodeRec.IdParent;
  Node.Typ := NodeRec.Typ;
  Node.Digits := NodeRec.Digits;
  Node.Decimal := NodeRec.Decimal;
  Node.Units := NodeRec.Units;
  Node.Digits2 := NodeRec.Digits2;
  Node.Decimal2 := NodeRec.Decimal2;
  Node.Units2 := NodeRec.Units2;
  Node.Count := NodeRec.Count;
  Node.FIn := NodeRec.FIn;
  Node.FOut := NodeRec.FOut;
  Node.Popis := NodeRec.Popis;
  Node.ValueName := NodeRec.ValueName;
  Node.ValueName2 := NodeRec.ValueName2;
  Node.ValueName3 := NodeRec.ValueName3;
  Node.ValueName4 := NodeRec.ValueName4;
  Node.Value := NodeRec.Value;
  Node.Value2 := NodeRec.Value2;
  Node.Value3 := NodeRec.Value3;
  Node.Value4 := NodeRec.Value4;
  Node.Min := NodeRec.Min;
  Node.Max := NodeRec.Max;
  Node.Min2 := NodeRec.Min2;
  Node.Max2 := NodeRec.Max2;
  Node.ModbusAdr := NodeRec.ModbusAdr;
  Node.ModbusAdr2 := NodeRec.ModbusAdr2;
  Node.ModbusAdr3 := NodeRec.ModbusAdr3;
  Node.ModbusAdr4 := NodeRec.ModbusAdr4;
end;

procedure TNodes.ReadMultiTexts(Stream: TStream; Node: TNode; MTCnt : Integer);
var I, J, Cnt : Integer;
    S : TNodeString;
    NText : TNodeText;
begin
  for I := 0 to MTCnt-1 do begin
    Stream.Read(Cnt,SizeOf(Cnt));
    NText := TNodeText.Create;
    for J := 0 to Cnt-1 do begin
      Stream.Read(S,SizeOf(S));
      NText.MultiText.Add(S);
    end;
    Node.MultiTexts.Add(NText);
  end;
end;

{ TNode }

constructor TNode.Create;
begin
  inherited;
  MultiTexts:=TObjectList.Create;
end;

destructor TNode.Destroy;
begin
  MultiTexts.Free;
  inherited;
end;

function TNode.GetPopis: WideString;
begin
  if (LanguageIndex > 0) and (Self.MultiTexts.Count > 0) and (LanguageIndex < TNodeText(Self.MultiTexts[0]).MultiText.Count) then begin
    if ConvertWideCharsPage <> nil then
      Result := TransformWideChars(TNodeText(Self.MultiTexts[0]).MultiText[LanguageIndex], ConvertWideCharsPage)
    else
      Result := TNodeText(Self.MultiTexts[0]).MultiText[LanguageIndex];
  end else begin
    Result := DefPopis;
  end;
end;

procedure TNode.SetPopis(AValue: WideString);
begin
  DefPopis := AValue;
end;

{ TNodeText }

constructor TNodeText.Create;
begin
  inherited;
  MultiText:=TTntStringList.Create;
end;

destructor TNodeText.Destroy;
begin
  MultiText.Free;
  inherited;
end;
end.
