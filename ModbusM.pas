unit ModbusM;

interface

uses
  Windows, Messages, SysUtils, Classes, ExtCtrls, Forms, Main;

type
  // arreglo de bytes que conforman el mensaje modbus y un puntero al mismo
  TDataByte = array of byte;
  PByte     = ^byte;

  // COM Port Baud Rates
  TComPortBaudRate = ( br110, br300, br600, br1200, br2400, br4800,
                       br9600, br14400, br19200, br38400, br56000,
                       br57600, br115200, br128000, br256000 );
  // COM Port Numbers
//  TComPortNumber = ( pnCOM1, pnCOM2, pnCOM3, pnCOM4, pnCOM5, pnCOM6 );
  TComPortNumber = byte;
  // COM Port Data bits
  TComPortDataBits = ( db5BITS, db6BITS, db7BITS, db8BITS );
  // COM Port Stop bits
  TComPortStopBits = ( sb1BITS, sb1HALFBITS, sb2BITS );
  // COM Port Parity
  TComPortParity = ( ptNONE, ptODD, ptEVEN, ptMARK, ptSPACE );
  // COM Port Hardware Handshaking
  TComPortHwHandshaking = ( hhNONE, hhRTSCTS );
  // COM Port Software Handshaking
  TComPortSwHandshaking = ( shNONE, shXONXOFF );
  TComPortRtsControl = ( rcRTSEnable, rcRTSDISABLE, rcRTSToggle );


  TOnErrorEvent = procedure(Sender : TObject; const ErrorMsg : String) of Object;


  TModbusM = class(TComponent)
  private
    FTimer                     : TTimer; // para contar el QueryTimeOut
    FQueryTimer                : TTimer; // timer pro zkouseni pristupu na zamcenou linku
    FResponseReady             : TNotifyEvent;// Evento que informa que se recibi� la respuesta de la consulta
    FOnError                   : TOnErrorEvent;// Evento que informa que se produjo un error
    FSlaveId                   : Byte;      // Direcci�n del esclavo
    FFunction                  : Byte;      // Funcion del query
    FOffset                    : word;      // Direcci�n de inicio
    FQuantity                  : word;      // Cantidad de direcciones
    FRegisterDim               : Byte;      // Cantidad de bytes por cada registro
    FError                     : Byte;
    FBusy                      : Boolean;
    FComPortHandle             : THANDLE; // COM Port Device Handle
    FComClientId               : Cardinal; // klient sdilene komunikace
    FReadValues                : TDataByte;
    FWriteValues               : TDataByte;
    FComPort                   : TComPortNumber; // COM Port to use (1..4)
    FComPortBaudRate           : TComPortBaudRate; // COM Port speed (brXXXX)
    FComPortDataBits           : TComPortDataBits; // Data bits size (5..8)
    FComPortStopBits           : TComPortStopBits; // How many stop bits to use (1,1.5,2)
    FComPortParity             : TComPortParity; // Type of parity to use (none,odd,even,mark,space)
    FComPortHwHandshaking      : TComPortHwHandshaking; // Type of hw handshaking to use
    FComPortSwHandshaking      : TComPortSwHandshaking; // Type of sw handshaking to use
    fComPortRtsControl         : TComPortRtsControl;
    FComPortInBufSize          : word; // Size of the input buffer
    FComPortOutBufSize         : word; // Size of the output buffer
    FComPortPollingDelay       : word; // ms of delay between COM port pollings
    FEnableDTROnOpen           : boolean; { enable/disable DTR line on connect }
    FOutputTimeout             : word; { output timeout - milliseconds }
    FNotifyWnd                 : HWND; // This is used for the timer
    FTempInBuffer              : pointer;
    FTimeout                   : word;      // Timeout del modbus

    ReadBuffer : TDataByte;

    procedure SetComHandle( Value: THANDLE );
    procedure SetComPort( Value: TComPortNumber );
    procedure SetComPortBaudRate( Value: TComPortBaudRate );
    procedure SetComPortDataBits( Value: TComPortDataBits );
    procedure SetComPortStopBits( Value: TComPortStopBits );
    procedure SetComPortParity( Value: TComPortParity );
    procedure SetComPortHwHandshaking( Value: TComPortHwHandshaking );
    procedure SetComPortRtsControl( Value: TComPortRtsControl );
    procedure ApplyCOMSettings;
    procedure TimerWndProc( var msg: TMessage );
    procedure Response( Sender: TObject; DataPtr: pointer; DataSize: integer );
    procedure OnTimeout(Sender: TObject); // para cuando ocurra el QueryTimeout
    procedure OnQueryTimeout(Sender: TObject);    
    { v1.02: returns the output buffer free space or 65535 if
             not connected }
    function OutFreeSpace: word;
   { v1.02: flushes the rx/tx buffers }
    procedure FlushBuffers( inBuf, outBuf: boolean );
    { Send data }
    { v1.02: changed result time from 'boolean' to 'integer'. See the docs
             for more info }
    function SendData( DataPtr: pointer; DataSize: integer ): integer;
    procedure SetFTimeout(const Value: word);
    procedure SetComPortEnableDTROnOpen( Value: Boolean );
    procedure SetBusy( Value: Boolean );

  public
    property WriteValues: TDataByte read FWriteValues write FWriteValues; // Arreglo de valores a escribir
    property ReadValues: TDataByte read FReadValues; // Arreglo de valores leidos
    property Error: Byte read FError;   // Byte de error del amo
    property Busy: Boolean read FBusy;   // Informa si el amo est� Busy
    procedure Query;

    constructor Create( AOwner: TComponent ); override;
    destructor Destroy; override;
    // v1.02: make the Handle to the com port public (for TAPI...)
    property ComHandle: THANDLE read FComPortHandle write SetComHandle;
    function Connect: boolean;
    procedure Disconnect;
    function Connected: boolean;

  published
    // Which COM Port to use
    property ComPort: TComPortNumber read FComPort write SetComPort default 2;
    // COM Port speed (bauds)
    property ComPortSpeed: TComPortBaudRate read FComPortBaudRate write SetComPortBaudRate default br9600;
    // Data bits to used (5..8, for the 8250 the use of 5 data bits with 2 stop bits is an invalid combination,
    // as is 6, 7, or 8 data bits with 1.5 stop bits)
    property ComPortDataBits: TComPortDataBits read FComPortDataBits write SetComPortDataBits default db8BITS;
    // Stop bits to use (1, 1.5, 2)
    property ComPortStopBits: TComPortStopBits read FComPortStopBits write SetComPortStopBits default sb1BITS;
    // Parity Type to use (none,odd,even,mark,space)
    property ComPortParity: TComPortParity read FComPortParity write SetComPortParity default ptNONE;
    // Hardware Handshaking Type to use:
    //  cdNONE          no handshaking
    //  cdCTSRTS        both cdCTS and cdRTS apply (** this is the more common method**)
    property ComPortHwHandshaking: TComPortHwHandshaking
             read FComPortHwHandshaking write SetComPortHwHandshaking default hhNONE;
    // Event to raise when there is a Response available
    property ComPortRtsControl: TComPortRtsControl
             read FComPortRtsControl write SetComPortRtsControl default rcRTSDISABLE;

    property OnResponseReady: TNotifyEvent read FResponseReady write FResponseReady;
    property OnError: TOnErrorEvent read FOnError write FOnError;

    property SlaveId: Byte read FSlaveId write FSlaveId;
    property FunctionCode: Byte read FFunction write FFunction;
    property Offset: Word read FOffset write FOffset;
    property Quantity: Word read FQuantity write FQuantity;
    property RegisterDim: Byte read FRegisterDim write FRegisterDim;
    property Timeout: Word read FTimeout write SetFTimeout;
    property EnableDTROnOpen: Boolean read FEnableDTROnOpen write SetComPortEnableDTROnOpen;
  end;

function CRC(BufferData : TDataByte) : word;

procedure Register;

implementation

const
  QUERY_TIMEOUT_MSEC = 10; // prodleva do dalsiho pokusu o dotaz pres zatim zamcenou linku

{
01-funci�n ilegal
02-direcci�n ilegal
04-error de trabajo del esclavo
05-el esclavo requiere m�s tiempo
06-el esclavo est� ocupado y no atiende al query
07-time out
08-error de acceso a memoria del esclavo
09-error de comunicaci�n
10-controlador ocupado
11-puerto no iniciado
}
  SErrorCodes : Array[0..11] of String =
  (' 00-No Hay Error',
   ' 01-Funci�n ilegal',
   ' 02-Direcci�n ilegal',
   '  ',
   ' 04-Error de trabajo del esclavo',
   ' 05-El esclavo requiere m�s tiempo',
   ' 06-El esclavo est� ocupado y no atiende al query',
   ' 07-Time out',
   ' 08-Error de acceso a memoria del esclavo',
   ' 09-Error de comunicaci�n',
   ' 10-Controlador ocupado',
   ' 11-Puerto no iniciado'  );



function CRC(BufferData : TDataByte) : word;
var
  Data: byte;
  i,j: integer;
const
  polinomio:word= $0A001;
begin
  result:= $0FFFF;
  for i:=0 to high(BufferData)- 2 do
  begin
    data:= BufferData[i];
    for j:=1 to 8 do
    begin
      if (((data xor result) and $0001) = 1) then
         result:=(result shr 1) xor polinomio
      else
         result:=result shr 1;
      data:= data shr 1;
    end;
  end;
end;

procedure TModbusM.Query;
var
  CadenaCRC: word;
  Cadena: TDataByte;
  PCadena: PByte;
  LCadena,i: Integer;
  ByteTemp: Byte;
begin
  if not connected then begin
    FError:=11;
    if Assigned(FOnError) then FOnError(Self, SErrorCodes[11] );
    Exit;
  end;
  if FBusy then begin
    FError:=10;
    if Assigned(FOnError) then FOnError(self, SErrorCodes[11]);
    SetBusy(False);
    exit;
  end;

  SetBusy(True);
  if not FBusy then begin
    // zamceno, spusti se timer cekani
    FQueryTimer.Enabled:=True;
    exit;
  end;

  FlushBuffers( true,true);
  FError:=0;
  SetLength(ReadBuffer, 0);

  case FFunction of
  1: begin
       Setlength(Cadena,8);
       Cadena[0]:=FslaveId;
       Cadena[1]:=FFunction;
       Cadena[2]:=Hi(Foffset);
       Cadena[3]:=Lo(Foffset);
       Cadena[4]:=Hi(Fquantity);
       Cadena[5]:=Lo(Fquantity);
       CadenaCRC:=CRC(Cadena);
       Cadena[high(Cadena)-1]:=lo(CadenaCRC);
       Cadena[high(Cadena)]:=hi(CadenaCRC);
       PCadena:=@Cadena[0];
       LCadena:=High(Cadena)+1;
       if SendData(PCadena,LCadena)=LCadena
         then FTimer.Enabled:=True
         else begin
           FError:=9;
           if Assigned(FOnError) then FOnError(Self, SErrorCodes[FError]);
           SetBusy(False);
         end;
     end;
  2: begin
       setlength(Cadena,8);
       Cadena[0]:=FslaveId;
       Cadena[1]:=FFunction;
       Cadena[2]:=Hi(Foffset);
       Cadena[3]:=Lo(Foffset);
       Cadena[4]:=Hi(Fquantity);
       Cadena[5]:=Lo(Fquantity);
       CadenaCRC:=CRC(Cadena);
       Cadena[high(Cadena)-1]:=lo(CadenaCRC);
       Cadena[high(Cadena)]:=hi(CadenaCRC);
       PCadena:=@Cadena[0];
       LCadena:=High(Cadena)+1;
       if SendData(PCadena,LCadena)=LCadena
         then FTimer.Enabled:=True
         else begin
           FError:=9;
           if Assigned(FOnError) then FOnError(Self, SErrorCodes[FError]);
           SetBusy(False);
         end;
     end;

  3: begin
       Setlength(Cadena,8);
       Cadena[0]:=FslaveId;
       Cadena[1]:=FFunction;
       Cadena[2]:=Hi(Foffset);
       Cadena[3]:=Lo(Foffset);
       Cadena[4]:=Hi(Fquantity);
       Cadena[5]:=Lo(Fquantity);
       CadenaCRC:=CRC(Cadena);
       Cadena[high(Cadena)-1]:=lo(CadenaCRC);
       Cadena[high(Cadena)]:=hi(CadenaCRC);
       PCadena:=@Cadena[0];
       LCadena:=High(Cadena)+1;
       if SendData(PCadena, LCadena)=LCadena
         then FTimer.Enabled:=True
         else begin
           FError:=9;
           if Assigned(FOnError) then FOnError(Self, SErrorCodes[FError]);;
           SetBusy(False);
         end;
     end;
  4: begin
       setlength(Cadena,8);
       Cadena[0]:=FslaveId;
       Cadena[1]:=FFunction;
       Cadena[2]:=Hi(Foffset);
       Cadena[3]:=Lo(Foffset);
       Cadena[4]:=Hi(Fquantity);
       Cadena[5]:=Lo(Fquantity);
       CadenaCRC:=CRC(Cadena);
       Cadena[high(Cadena)-1]:=lo(CadenaCRC);
       Cadena[high(Cadena)]:=hi(CadenaCRC);
       PCadena:=@Cadena[0];
       LCadena:=High(Cadena)+1;
       if SendData(PCadena,LCadena)=LCadena
         then FTimer.Enabled:=True
         else begin
           FError:=9;
           if Assigned(FOnError) then FOnError(Self, SErrorCodes[FError]);;
           SetBusy(False);
         end;
     end;
  5: begin
       setlength(Cadena,8);
       Cadena[0]:=FslaveId;
       Cadena[1]:=FFunction;
       Cadena[2]:=Hi(Foffset);
       Cadena[3]:=Lo(Foffset);
       if length(FWriteValues)<2 then
         begin
           FError:=3;
           if Assigned(FOnError) then FOnError(Self, SErrorCodes[FError]);;
           SetBusy(False);
           Exit;
         end;
       Cadena[4]:=FWriteValues[0];
       Cadena[5]:=FWriteValues[1];
       CadenaCRC:=CRC(Cadena);
       Cadena[high(Cadena)-1]:=lo(CadenaCRC);
       Cadena[high(Cadena)]:=hi(CadenaCRC);
       PCadena:=@Cadena[0];
       LCadena:=High(Cadena)+1;
       if SendData(PCadena,LCadena)=LCadena
         then FTimer.Enabled:=True
         else begin
           FError:=9;
           if Assigned(FOnError) then FOnError(Self, SErrorCodes[FError]);;
           SetBusy(False);
         end;
     end;
  6: begin
       setlength(Cadena,6+FRegisterDim);
       Cadena[0]:=FslaveId;
       Cadena[1]:=FFunction;
       Cadena[2]:=Hi(Foffset);
       Cadena[3]:=Lo(Foffset);
       if length(FWriteValues)<FRegisterDim then
         begin
           FError:=3;
           if Assigned(FOnError) then FOnError(Self, SErrorCodes[FError]);;
           SetBusy(False);
           Exit;
         end;
       for i:=1 to FRegisterDim do
         Cadena[3+i]:=FWriteValues[i-1];
       CadenaCRC:=CRC(Cadena);
       Cadena[high(Cadena)-1]:=lo(CadenaCRC);
       Cadena[high(Cadena)]:=hi(CadenaCRC);
       PCadena:=@Cadena[0];
       LCadena:=High(Cadena)+1;
       if SendData(PCadena,LCadena)=LCadena
         then FTimer.Enabled:=True
         else begin
           FError:=9;
           if Assigned(FOnError) then FOnError(Self, SErrorCodes[FError]);;
           SetBusy(False);
         end;
     end;
  15: begin
      if Fquantity mod 8>0 then ByteTemp:=Fquantity div 8 +1
      else ByteTemp:=Fquantity div 8;
      setlength(cadena,9+ByteTemp);
      Cadena[0]:=FslaveId;
      Cadena[1]:=FFunction;
      Cadena[2]:=Hi(Foffset);
      Cadena[3]:=Lo(Foffset);
       if length(FWriteValues)<ByteTemp then
         begin
           FError:=3;
           if Assigned(FOnError) then FOnError(Self, SErrorCodes[FError]);;
           SetBusy(False);
           Exit;
         end;
       Cadena[4]:=Hi(Fquantity);
       Cadena[5]:=Lo(Fquantity);
       Cadena[6]:=ByteTemp;
       for i:=1 to ByteTemp do
         Cadena[6+i]:=FWriteValues[i-1];
       CadenaCRC:=CRC(Cadena);
       Cadena[high(Cadena)-1]:=lo(CadenaCRC);
       Cadena[high(Cadena)]:=hi(CadenaCRC);
       PCadena:=@Cadena[0];
       LCadena:=High(Cadena)+1;
       if SendData(PCadena,LCadena)=LCadena
         then FTimer.Enabled:=True
         else begin
           FError:=9;
           if Assigned(FOnError) then FOnError(Self, SErrorCodes[FError]);;
           SetBusy(False);
         end;
      end;
  16: begin
      ByteTemp:=Fquantity*FRegisterDim;
      setlength(cadena,9+ByteTemp);
      Cadena[0]:=FslaveId;
      Cadena[1]:=FFunction;
      Cadena[2]:=Hi(Foffset);
      Cadena[3]:=Lo(Foffset);
       if length(FWriteValues)<ByteTemp then
         begin
           FError:=3;
           if Assigned(FOnError) then FOnError(Self, SErrorCodes[FError]);;
           SetBusy(False);
           Exit;
         end;
       Cadena[4]:=Hi(Fquantity);
       Cadena[5]:=Lo(Fquantity);
       Cadena[6]:=ByteTemp;
       for i:=1 to ByteTemp do
         Cadena[6+i]:=FWriteValues[i-1];
       CadenaCRC:=CRC(Cadena);
       Cadena[high(Cadena)-1]:=lo(CadenaCRC);
       Cadena[high(Cadena)]:=hi(CadenaCRC);
       PCadena:=@Cadena[0];
       LCadena:=High(Cadena)+1;
       if SendData(PCadena,LCadena)=LCadena
         then FTimer.Enabled:=True
         else begin
           FError:=9;
           if Assigned(FOnError) then FOnError(Self, SErrorCodes[FError]);;
           SetBusy(False);
         end;
       end;
  else begin
      FError:=1;
      if Assigned(FOnError) then FOnError(Self, SErrorCodes[FError]);;
      SetBusy(False);
    end;
  end;
end;

procedure TModbusM.OnTimeout(Sender : TObject);
begin
    FTimer.Enabled:=false;
    FError:=7;
    FlushBuffers(true,true);
    if Assigned(FOnError) then FOnError(Self, SErrorCodes[FError]);
    SetBusy(False);
end;

procedure TModbusM.OnQueryTimeout(Sender : TObject);
begin
  FQueryTimer.Enabled:=false;
  Query;
end;

procedure TModbusM.Response( Sender: TObject; DataPtr: pointer; DataSize: integer );
var
  CadenaCRC: Word;
  PCadena: PByte;
  i, LCadena : Integer;
  ByteTemp: Byte;

begin

  FTimer.Enabled:=False;  //Apagar el TimeOut;

  PCadena:=DataPtr;
  LCadena:=DataSize;

  //SetLength(Cadena, Legth(Cadena)+LCadena);
  //Setlength(Cadena,  LCadena);

  for i:=0 to LCadena-1 do
  begin
    SetLength(ReadBuffer, Length(ReadBuffer)+1 );
    ReadBuffer[Length(ReadBuffer)-1]:=PCadena^;
    inc(PCadena);
  end;

  CadenaCRC:=CRC(ReadBuffer);
  if (Lo(CadenaCRC)<>ReadBuffer[high(ReadBuffer)-1]) or (Hi(CadenaCRC)<>ReadBuffer[high(ReadBuffer)]) then
  begin
    FError:=9;
    if Assigned(FOnError) then FOnError(Self, SErrorCodes[FError]);;
    SetBusy(False);
    Exit;
  end;
  
  case ReadBuffer[1] of
  1: begin
       if Fquantity Mod 8>0 then
         ByteTemp:=(FQuantity div 8)+1
       else ByteTemp:=Fquantity div 8;
       if length(ReadBuffer)= ByteTemp+5 then
         begin
           SetLength(FReadValues, ReadBuffer[2]);
           for i:=0 to ReadBuffer[2]-1 do
           FReadValues[i]:=ReadBuffer[3+i];
         end
       else begin
         FError:= 4;
         if Assigned(FOnError) then FOnError(Self, SErrorCodes[FError]);;
         SetBusy(False);
         exit;
       end;
     end;
  2: begin
       if Fquantity Mod 8>0 then
         ByteTemp:=(Fquantity div 8)+1
       else ByteTemp:=Fquantity div 8;
       if length(ReadBuffer)= ByteTemp+5 then
         begin
           SetLength(FReadValues, ReadBuffer[2]);
           for i:=0 to ReadBuffer[2]-1 do
           FReadValues[i]:=ReadBuffer[3+i];
         end
       else begin
         FError:= 4;
         if Assigned(FOnError) then FOnError(Self, SErrorCodes[FError]);;
         SetBusy(False);
         exit;
       end;
     end;

  3: begin

       if Length(ReadBuffer)= Quantity * RegisterDim + 5 then
       begin
           SetLength( FReadValues, ReadBuffer[2] );
           for i:=0 to ReadBuffer[2]-1 do
               FReadValues[i]:=ReadBuffer[3+i];
       end else
       begin
           FError:= 4;
           if Assigned(FOnError) then FOnError(Self, SErrorCodes[FError]);
           SetBusy(False);
           exit;
       end;

     end;


  4: begin
       if length(ReadBuffer)=Fquantity*FRegisterDim+5 then
         begin
           SetLength(FReadValues, ReadBuffer[2]);
           for i:=0 to ReadBuffer[2]-1 do
           FReadValues[i]:=ReadBuffer[3+i];
         end
       else begin
         FError:= 4;
         if Assigned(FOnError) then FOnError(Self, SErrorCodes[FError]);;
         SetBusy(False);
         exit;
       end;
     end;
  5: begin
       if length(ReadBuffer)=8 then begin
         SetLength(FReadValues,2);
         for i:=0 to 1 do
           FReadValues[i]:=ReadBuffer[4+i];
       end
       else begin
         FError:= 4;
         if Assigned(FOnError) then FOnError(Self, SErrorCodes[FError]);;
         SetBusy(False);
         exit;
       end;
     end;
  6: begin
       if length(ReadBuffer)=6+FRegisterDim then begin
         SetLength(FReadValues,2);
         for i:=0 to 1 do
           FReadValues[i]:=ReadBuffer[4+i];
       end
       else begin
         FError:= 4;
         if Assigned(FOnError) then FOnError(Self, SErrorCodes[FError]);;
         SetBusy(False);
         exit;
       end;
     end;
  15: begin
        if length(ReadBuffer)=8 then begin
          SetLength(FReadValues,2);
          for i:=0 to 1 do
            FReadValues[i]:=ReadBuffer[4+i];
          end
        else begin
          FError:= 4;
          if Assigned(FOnError) then FOnError(Self, SErrorCodes[FError]);;
          SetBusy(False);
          exit;
        end;
      end;
  16: begin
        if length(ReadBuffer)=8 then begin
          SetLength(FReadValues,2);
          for i:=0 to 1 do
            FReadValues[i]:=ReadBuffer[4+i];
          end
        else begin
          FError:= 4;
          if Assigned(FOnError) then FOnError(Self, SErrorCodes[FError]);;
          SetBusy(False);
          exit;
        end;
      end;
  $81,$82,$83,$84,$85,$86,$8f,$90:begin
        case ReadBuffer[2] of
        1: begin
            FError:=1;
            if Assigned(FOnError) then FOnError(Self, SErrorCodes[FError]);;
            SetBusy(False);
            exit;
          end;
        2: begin
            FError:=2;
            if Assigned(FOnError) then FOnError(Self, SErrorCodes[FError]);;
            SetBusy(False);
            exit;
          end;
        3: begin
            FError:=3;
            if Assigned(FOnError) then FOnError(Self, SErrorCodes[FError]);;
            SetBusy(False);
            exit;
          end;
        4: begin
            FError:=4;
            if Assigned(FOnError) then FOnError(Self, SErrorCodes[FError]);;
            SetBusy(False);
            exit;
          end;
        5: begin
            FError:=5;
            Ftimer.Enabled:=True;
            if Assigned(FOnError) then FOnError(Self, SErrorCodes[FError]);;
            exit;
          end;
        6: begin
            FError:=6;
            if Assigned(FOnError) then FOnError(Self, SErrorCodes[FError]);;
            SetBusy(False);
            exit;
          end;
        8: begin
            FError:=8;
            if Assigned(FOnError) then FOnError(Self, SErrorCodes[FError]);;
            SetBusy(False);
            exit;
          end;
        end;
      end;
  else begin
      FError:=1;
      if Assigned(FOnError) then FOnError(Self, SErrorCodes[FError]);;
      SetBusy(False);
      exit;
    end;
  end;
  if Assigned(FResponseReady) then FResponseReady(self);
  SetBusy(False);
end;

constructor TModbusM.Create( AOwner: TComponent );
begin

  inherited Create( AOwner );
  SetLength(ReadBuffer, 0);
  // Initialize to default values
  FComPortHandle             := INVALID_HANDLE_VALUE;       // Not connected
  FComPort                   := 0;  // COM 2
  FComPortBaudRate           := br9600;  // 9600 bauds
  FComPortDataBits           := db8BITS; // 8 data bits
  FComPortStopBits           := sb1BITS; // 1 stop bit
  FComPortParity             := ptNONE;  // no parity
  FComPortHwHandshaking      := hhNONE;  // no hardware handshaking
  FComPortSwHandshaking      := shNONE;  // no software handshaking
  FComPortInBufSize          := 1028;    // input buffer of 1028 bytes
  FComPortOutBufSize         := 1028;    // output buffer of 1028 bytes
  FComPortPollingDelay       := 300;    // poll COM port every 300ms
  FOutputTimeout             := 5000;    // output timeout - 5000ms
  FEnableDTROnOpen           := false;   // DTR high on connect
  // Temporary buffer for received data
  GetMem( FTempInBuffer, FComPortInBufSize ); //reserva memoria para el buffer de entrada
  // Allocate a window handle to catch timer's notification messages

  if not (csDesigning in ComponentState) then  //ComponentState es una propiedad de TComponent, que especifica el estado de actual del componente
    FNotifyWnd := AllocateHWnd( TimerWndProc ); //devuelve un handle para el m�todo especificado

  FTimeout                   := 3000;      // query timeout - 3000ms
  FResponseReady             := nil;       // evento sin implementar
  FTimer:=TTimer.Create(Self);             // Creamos el timer que contar� el QueryTimeout
  FTimer.Interval:=FTimeout;               // Asignamos el intervalo del timer = FQueryTimeout
  FTimer.OnTimer:=OnTimeout;                 // Asiganamos el m�todo Timeout
  FTimer.Enabled:=false;
  FError                     :=0;
  FOnError                   := nil;       // evento sin implementar
  FBusy                      :=False;
  FRegisterDim               := 2;
  FComClientId := Cardinal(-1);

  FQueryTimer:=TTimer.Create(Self);
  FQueryTimer.Interval:=QUERY_TIMEOUT_MSEC;
  FQueryTimer.OnTimer:=OnQueryTimeout;
  FQueryTimer.Enabled:=false;
end;

destructor TModbusM.Destroy;
begin
  FTimer.Free;          //Liberamos el timer
  FQueryTimer.Free;
  // Be sure to release the COM device
  Disconnect;
  // Free the temporary buffer
  FreeMem( FTempInBuffer, FComPortInBufSize );
  // Destroy the timer's window
  DeallocateHWnd( FNotifyWnd );
  inherited Destroy;
end;

// v1.02: The COM port handle made public and writeable.
// This lets you connect to external opened com port.
// Setting ComPortHandle to 0 acts as Disconnect.
procedure TModbusM.SetComHandle( Value: THANDLE );
begin
  // If same COM port then do nothing
  if FComPortHandle = Value then
    exit;
  { If value is $FFFFFFFF then stop controlling the COM port
    without closing in }
  if Value = $FFFFFFFF then
  begin
    if Connected then
      { Stop the timer }
      if Connected then
        KillTimer( FNotifyWnd, 1 );
    { No more connected }
    FComPortHandle :=  INVALID_HANDLE_VALUE;
  end
  else
  begin
    { Disconnect }
    Disconnect;
    { If Value is = 0 then exit now }
    { (ComPortHandle := 0 acts as Disconnect) }
    if Value = 0  then
      exit;

    { Set COM port handle }
    FComPortHandle := Value;

    { Start the timer (used for polling) }
    SetTimer( FNotifyWnd, 1, FComPortPollingDelay, nil );
  end;
end;

procedure TModbusM.SetComPort( Value: TComPortNumber );
begin
  // Be sure we are not using any COM port
  if Connected then
    exit;
  // Change COM port
  FComPort := Value;
end;

procedure TModbusM.SetComPortBaudRate( Value: TComPortBaudRate );
begin
  // Set new COM speed
  FComPortBaudRate := Value;
  // Apply changes
  if Connected then
    ApplyCOMSettings;
end;

procedure TModbusM.SetComPortDataBits( Value: TComPortDataBits );
begin
  // Set new data bits
  FComPortDataBits := Value;
  // Apply changes
  if Connected then
    ApplyCOMSettings;
end;

procedure TModbusM.SetComPortStopBits( Value: TComPortStopBits );
begin
  // Set new stop bits
  FComPortStopBits := Value;
  // Apply changes
  if Connected then
    ApplyCOMSettings;
end;

procedure TModbusM.SetComPortParity( Value: TComPortParity );
begin
  // Set new parity
  FComPortParity := Value;
  // Apply changes
  if Connected then
    ApplyCOMSettings;
end;

procedure TModbusM.SetComPortHwHandshaking( Value: TComPortHwHandshaking );
begin
  // Set new hardware handshaking
  FComPortHwHandshaking := Value;
  // Apply changes
  if Connected then
    ApplyCOMSettings;
end;

procedure TModbusM.SetComPortEnableDTROnOpen( Value: Boolean );
begin
  // Set new hardware handshaking
  FEnableDTROnOpen := Value;
  // Apply changes
  if Connected then
    ApplyCOMSettings;
end;

procedure TModbusM.SetComPortRtsControl( Value: TComPortRtsControl );
begin
  // Set new hardware handshaking
  fComPortRtsControl := Value;
  // Apply changes
  if Connected then
    ApplyCOMSettings;
end;

procedure TModbusM.SetBusy( Value: Boolean );
begin
  SetLength(ReadBuffer, 0);
  if Value then begin
    FBusy := GSharedComms.SetLock(FComPortHandle, FComClientId, Value);
  end else begin
    FBusy := false;
    GSharedComms.SetLock(FComPortHandle, FComClientId, false);
  end;
end;

const
  Win32BaudRates: array[br110..br115200] of DWORD =
    ( CBR_110, CBR_300, CBR_600, CBR_1200, CBR_2400, CBR_4800, CBR_9600,
      CBR_14400, CBR_19200, CBR_38400, CBR_56000, CBR_57600, CBR_115200{v1.02 removed: CRB_128000, CBR_256000} );

const
  dcb_Binary              = $00000001;
  dcb_ParityCheck         = $00000002;
  dcb_OutxCtsFlow         = $00000004;
  dcb_OutxDsrFlow         = $00000008;
  dcb_DtrControlMask      = $00000030;
  dcb_DtrControlDisable   = $00000000;
  dcb_DtrControlEnable    = $00000010;
  dcb_DtrControlHandshake = $00000020;
  dcb_DsrSensivity        = $00000040;
  dcb_TXContinueOnXoff    = $00000080;
  dcb_OutX                = $00000100;
  dcb_InX                 = $00000200;
  dcb_ErrorChar           = $00000400;
  dcb_NullStrip           = $00000800;
  dcb_RtsControlMask      = $00003000;
  dcb_RtsControlDisable   = $00000000;
  dcb_RtsControlEnable    = $00001000;
  dcb_RtsControlHandshake = $00002000;
  dcb_RtsControlToggle    = $00003000;
  dcb_AbortOnError        = $00004000;
  dcb_Reserveds           = $FFFF8000;

// Apply COM settings.
procedure TModbusM.ApplyCOMSettings;
var dcb: TDCB;
begin
  // Do nothing if not connected
  if not Connected then
    exit;

  // Clear all
  fillchar( dcb, sizeof(dcb), 0 );
  // Setup dcb (Device Control Block) fields
  dcb.DCBLength := sizeof(dcb); // dcb structure size
  dcb.BaudRate := Win32BaudRates[ FComPortBaudRate ]; // baud rate to use
  // Set fBinary: Win32 does not support non binary mode transfers
  // (also disable EOF check)
  dcb.Flags := dcb_Binary;
  if FEnableDTROnOpen then
    { Enabled the DTR line when the device is opened and leaves it on }
    dcb.Flags := dcb.Flags or dcb_DtrControlEnable;

  case FComPortHwHandshaking of // Type of hw handshaking to use
    hhNONE:; // No hardware handshaking
    hhRTSCTS: // RTS/CTS (request-to-send/clear-to-send) hardware handshaking
      dcb.Flags := dcb.Flags or dcb_OutxCtsFlow or dcb_RtsControlHandshake;
  end;
  case FComPortSwHandshaking of // Type of sw handshaking to use
    shNONE:; // No software handshaking
    shXONXOFF: // XON/XOFF handshaking
      dcb.Flags := dcb.Flags or dcb_OutX or dcb_InX;
  end;
  case fComPortRtsControl of // Type of hw handshaking to use
    rcRTSDISABLE:;
    rcRTSEnable:
      dcb.Flags := dcb.Flags or dcb_RtsControlEnable;
    rcRTSToggle:
      dcb.Flags := dcb.Flags or dcb_RtsControlToggle;
  end;

  dcb.XONLim := FComPortInBufSize div 4; // Specifies the minimum number of bytes allowed
                                         // in the input buffer before the XON character is sent
                                         // (or CTS is set)
  dcb.XOFFLim := 1; // Specifies the maximum number of bytes allowed in the input buffer
                    // before the XOFF character is sent. The maximum number of bytes
                    // allowed is calculated by subtracting this value from the size,
                    // in bytes, of the input buffer
  dcb.ByteSize := 5 + ord(FComPortDataBits); // how many data bits to use
  dcb.Parity := ord(FComPortParity); // type of parity to use
  dcb.StopBits := ord(FComPortStopbits); // how many stop bits to use
  dcb.XONChar := #17; // XON ASCII char
  dcb.XOFFChar := #19; // XOFF ASCII char
  SetCommState( FComPortHandle, dcb );
  { Flush buffers }
  FlushBuffers( true, true );
  // Setup buffers size
  SetupComm( FComPortHandle, FComPortInBufSize, FComPortOutBufSize );
end;

function TModbusM.Connect: boolean;
var comName :string;
    tms: TCOMMTIMEOUTS;
    handle : THANDLE;
begin
  // Do nothing if already connected
  Result := Connected;
  if Result then
    exit;

  comName:= Format( '\\.\COM%d', [FComPort]);

  handle := GSharedComms.GetConnection(comName);
  if handle <> INVALID_HANDLE_VALUE then begin
    FComClientId := GSharedComms.AddConnectionRef(handle);
    FComPortHandle := handle;
  end else begin
    // Open the COM port
    FComPortHandle := CreateFile( pchar(comName),
                                  GENERIC_READ or GENERIC_WRITE,
                                  0, // Not shared
                                  nil, // No security attributes
                                  OPEN_EXISTING,
                                  FILE_ATTRIBUTE_NORMAL,
                                  0 // No template
                                ) ;

    if FComPortHandle <> INVALID_HANDLE_VALUE then begin
      GSharedComms.AddConnection(comName, FComPortHandle);
      FComClientId := GSharedComms.AddConnectionRef(FComPortHandle);
    end;
  end;

  Result := Connected;
  if not Result then
    exit;

  // Apply settings
  ApplyCOMSettings;
  // Setup timeouts: we disable timeouts because we are polling the com port!
  tms.ReadIntervalTimeout := 1; // Specifies the maximum time, in milliseconds,
                                // allowed to elapse between the arrival of two
                                // characters on the communications line
  tms.ReadTotalTimeoutMultiplier := 0; // Specifies the multiplier, in milliseconds,
                                       // used to calculate the total time-out period
                                       // for read operations.
  tms.ReadTotalTimeoutConstant := 1; // Specifies the constant, in milliseconds,
                                     // used to calculate the total time-out period
                                     // for read operations.
  tms.WriteTotalTimeoutMultiplier := 0; // Spec<ifies the multiplier, in milliseconds,
                                        // used to calculate the total time-out period
                                        // for write operations.
  tms.WriteTotalTimeoutConstant := 0; // Specifies the constant, in milliseconds,
                                      // used to calculate the total time-out period
                                      // for write operations.
  SetCommTimeOuts( FComPortHandle, tms );
  // Start the timer (used for polling)
  SetTimer( FNotifyWnd, 1, FComPortPollingDelay, nil );
end;

procedure TModbusM.Disconnect;
begin
  if Connected then
  begin
    // Stop the timer (used for polling)
    KillTimer( FNotifyWnd, 1 );

    if FBusy then GSharedComms.SetLock(FComPortHandle, FComClientId, False);

    GSharedComms.RemoveConnectionRef(FComPortHandle, FComClientId);
    if GSharedComms.GetConnectionRefCount(FComPortHandle) = 0 then begin
      GSharedComms.RemoveConnection(FComPortHandle);
      // Release the COM port
      CloseHandle( FComPortHandle );
    end;

    // No more connected
    FComPortHandle :=  INVALID_HANDLE_VALUE;
  end;
end;

function TModbusM.Connected: boolean;
begin
  Result := (FComPortHandle <> INVALID_HANDLE_VALUE);
end;

// v1.02: flish rx/rx buffers
procedure TModbusM.FlushBuffers( inBuf, outBuf: boolean );
var dwAction: DWORD;
begin
  if not Connected then
    exit;
  // Flush the incoming data buffer
  dwAction := 0;
  if outBuf then
    dwAction := dwAction or PURGE_TXABORT or PURGE_TXCLEAR;
  if inBuf then
    dwAction := dwAction or PURGE_RXABORT or PURGE_RXCLEAR;
  PurgeComm( FComPortHandle, dwAction );
end;

// v1.02: returns the output buffer free space or 65535 if
//        not connected }
function TModbusM.OutFreeSpace: word;
var stat: TCOMSTAT;
    errs: DWORD;
begin
  if not Connected then
    Result := 65535
  else
  begin
    ClearCommError( FComPortHandle, errs, @stat );
    Result := FComPortOutBufSize - stat.cbOutQue;
  end;
end;

// Send data
{function TModbusM.SendData( DataPtr: pointer; DataSize: integer ): boolean;
var nsent: DWORD;
begin
  Result := WriteFile( FComPortHandle, DataPtr^, DataSize, nsent, nil );
  Result := Result and (nsent=DataSize);
end;}

{ Send data (breaks the data in small packets if it doesn't fit in the output
  buffer) }
function TModbusM.SendData( DataPtr: pointer; DataSize: integer ): integer;
var nToSend: integer;
    nsent :dword;
    t1: Cardinal;
begin
  { 0 bytes sent }
  Result := 0;
  { Do nothing if not connected }
  if not Connected then
    exit;
  { Current time }
  t1 := GetTickCount;
  { Loop until all data sent or timeout occurred }
  while DataSize > 0 do
  begin
    { Get output buffer free space }
    nToSend := OutFreeSpace;
    { If output buffer has some free space... }
    if nToSend > 0 then
    begin
      { Don't send more bytes than we actually have to send }
      if nToSend > DataSize then
        nToSend := DataSize;
      { Send }
      WriteFile( FComPortHandle, DataPtr^, nToSend, nsent, nil );
      // WriteFile( FComPortHandle, DataPtr^, DataSize, nsent, nil );

      if nsent=0 then
        raise Exception.Create('Comunication port error');

      { Update number of bytes sent }
      Result := Result + abs(nsent);
      { Decrease the count of bytes to send }
      DataSize := DataSize - abs(nsent);
      { Get current time }
      t1 := GetTickCount;
      { Continue. This skips the time check below (don't stop
        trasmitting if the FOutputTimeout is set too low) }
      continue;
    end;
    { Buffer is full. If we are waiting too long then
      invert the number of bytes sent and exit }
    if (GetTickCount-t1) > FOutputTimeout then
    begin
      Result := -Result;
      exit;
    end;
  end;
end;

// COM port polling proc
procedure TModbusM.TimerWndProc( var msg: TMessage );
var nRead: dword;
    stat: TCOMSTAT;
    errs: DWORD;
    ByteTemp:Cardinal;
    flagResponse:Boolean;
begin
  flagResponse:=false;
  if (msg.Msg = WM_TIMER) and Connected and FBusy then
  begin
    ClearCommError( FComPortHandle, errs, @stat );
    case FFunction of
    1,2:
    begin
      if Fquantity Mod 8>0 then
         ByteTemp:=(FQuantity div 8)+1
      else ByteTemp:=Fquantity div 8;
      if stat.cbInQue=ByteTemp+5 then
        flagResponse:=true;
    end;
    3,4:
      if stat.cbInQue=Quantity * RegisterDim + 5 then
        flagResponse:=true;
    5,15,16:
      if stat.cbInQue=8 then
        flagResponse:=true;
    6:
      if stat.cbInQue=6+FRegisterDim then
        flagResponse:=true;
    end;    
    nRead := 0;
    if flagResponse then
      if ReadFile( FComPortHandle, FTempInBuffer^, FComPortInBufSize, nRead, nil ) then
//        if (nRead <> 0) then
        Response( Self, FTempInBuffer, nRead );
  end;
end;

procedure Register;
begin
  { Register this component and show it in the 'System' tab
    of the component palette }
  RegisterComponents('CirComponents', [TModbusM]);
end;

procedure TModbusM.SetFTimeout(const Value: word);
begin
  if ( (Value<>FTimeout ) and (Value>10)) then
  begin
    FTimeOut:=Value;
    FTimer.Interval:=FTimeOut;
  end;
end;


end.

