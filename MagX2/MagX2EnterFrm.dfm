object formMagX2Enter: TformMagX2Enter
  Left = 409
  Top = 358
  BorderStyle = bsDialog
  Caption = 'Arkon flow system'
  ClientHeight = 352
  ClientWidth = 589
  Color = 12424827
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object lblLanguage: TLabel
    Left = 12
    Top = 127
    Width = 177
    Height = 13
    Alignment = taCenter
    AutoSize = False
    Caption = 'English version'
    Transparent = True
  end
  object btnCZ: TSpeedButton
    Tag = 1
    Left = 11
    Top = 88
    Width = 30
    Height = 30
    Hint = '�esk� verze'
    GroupIndex = 1
    Caption = 'CZ'
    Flat = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    ParentShowHint = False
    ShowHint = True
    OnClick = btnLangClick
    OnMouseMove = btnCZMouseMove
  end
  object btnEN: TSpeedButton
    Left = 41
    Top = 88
    Width = 30
    Height = 30
    Hint = 'English version'
    GroupIndex = 1
    Down = True
    Caption = 'EN'
    Flat = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    ParentShowHint = False
    ShowHint = True
    OnClick = btnLangClick
    OnMouseMove = btnCZMouseMove
  end
  object btnGE: TSpeedButton
    Tag = 2
    Left = 71
    Top = 88
    Width = 30
    Height = 30
    Hint = 'Deutsch Version'
    GroupIndex = 1
    Caption = 'DE'
    Flat = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    ParentShowHint = False
    ShowHint = True
    OnClick = btnLangClick
    OnMouseMove = btnCZMouseMove
  end
  object btnSP: TSpeedButton
    Tag = 3
    Left = 101
    Top = 88
    Width = 30
    Height = 30
    Hint = 'Spanish version'
    GroupIndex = 1
    Caption = 'SP'
    Flat = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    ParentShowHint = False
    ShowHint = True
    OnClick = btnLangClick
    OnMouseMove = btnCZMouseMove
  end
  object btnFR: TSpeedButton
    Tag = 4
    Left = 131
    Top = 88
    Width = 30
    Height = 30
    Hint = 'French version'
    GroupIndex = 1
    Caption = 'FR'
    Flat = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    ParentShowHint = False
    ShowHint = True
    OnClick = btnLangClick
    OnMouseMove = btnCZMouseMove
  end
  object btnRU: TSpeedButton
    Tag = 5
    Left = 159
    Top = 88
    Width = 30
    Height = 30
    Hint = 'Rusian version'
    GroupIndex = 1
    Caption = 'RU'
    Flat = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    ParentShowHint = False
    ShowHint = True
    OnClick = btnLangClick
    OnMouseMove = btnCZMouseMove
  end
  object lblDevList: TLabel
    Left = 200
    Top = 8
    Width = 65
    Height = 13
    Caption = 'Device list:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object btnAdd: TSpeedButton
    Left = 205
    Top = 312
    Width = 25
    Height = 25
    Hint = 'Add device'
    Flat = True
    Glyph.Data = {
      76010000424D7601000000000000760000002800000020000000100000000100
      04000000000000010000130B0000130B00001000000000000000000000000000
      800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
      33333333FF33333333FF333993333333300033377F3333333777333993333333
      300033F77FFF3333377739999993333333333777777F3333333F399999933333
      33003777777333333377333993333333330033377F3333333377333993333333
      3333333773333333333F333333333333330033333333F33333773333333C3333
      330033333337FF3333773333333CC333333333FFFFF77FFF3FF33CCCCCCCCCC3
      993337777777777F77F33CCCCCCCCCC3993337777777777377333333333CC333
      333333333337733333FF3333333C333330003333333733333777333333333333
      3000333333333333377733333333333333333333333333333333}
    NumGlyphs = 2
    ParentShowHint = False
    ShowHint = True
    OnClick = btnAddClick
  end
  object btnSave: TSpeedButton
    Left = 254
    Top = 312
    Width = 25
    Height = 25
    Hint = 'Save device'
    Flat = True
    Glyph.Data = {
      76010000424D7601000000000000760000002800000020000000100000000100
      04000000000000010000130B0000130B00001000000000000000000000000000
      800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333330070
      7700333333337777777733333333008088003333333377F73377333333330088
      88003333333377FFFF7733333333000000003FFFFFFF77777777000000000000
      000077777777777777770FFFFFFF0FFFFFF07F3333337F3333370FFFFFFF0FFF
      FFF07F3FF3FF7FFFFFF70F00F0080CCC9CC07F773773777777770FFFFFFFF039
      99337F3FFFF3F7F777F30F0000F0F09999937F7777373777777F0FFFFFFFF999
      99997F3FF3FFF77777770F00F000003999337F773777773777F30FFFF0FF0339
      99337F3FF7F3733777F30F08F0F0337999337F7737F73F7777330FFFF0039999
      93337FFFF7737777733300000033333333337777773333333333}
    NumGlyphs = 2
    ParentShowHint = False
    ShowHint = True
    OnClick = btnSaveClick
  end
  object btnDel: TSpeedButton
    Left = 229
    Top = 312
    Width = 25
    Height = 25
    Hint = 'Delete device'
    Flat = True
    Glyph.Data = {
      76010000424D7601000000000000760000002800000020000000100000000100
      04000000000000010000130B0000130B00001000000000000000000000000000
      800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
      333333333333333333FF33333333333330003333333333333777333333333333
      300033FFFFFF3333377739999993333333333777777F3333333F399999933333
      3300377777733333337733333333333333003333333333333377333333333333
      3333333333333333333F333333333333330033333F33333333773333C3333333
      330033337F3333333377333CC3333333333333F77FFFFFFF3FF33CCCCCCCCCC3
      993337777777777F77F33CCCCCCCCCC399333777777777737733333CC3333333
      333333377F33333333FF3333C333333330003333733333333777333333333333
      3000333333333333377733333333333333333333333333333333}
    NumGlyphs = 2
    ParentShowHint = False
    ShowHint = True
    OnClick = btnDelClick
  end
  object imgMagLogo: TImage
    Left = 11
    Top = 141
    Width = 177
    Height = 63
    Center = True
  end
  object imgLogo: TImage
    Left = 11
    Top = 10
    Width = 177
    Height = 73
  end
  object btnService: TButton
    Left = 24
    Top = 208
    Width = 73
    Height = 25
    Caption = 'Service'
    ModalResult = 7
    TabOrder = 1
    OnClick = btnServiceStatisticClick
  end
  object btnExit: TButton
    Left = 24
    Top = 232
    Width = 73
    Height = 25
    Caption = 'Exit'
    ModalResult = 2
    TabOrder = 3
    OnClick = btnExitClick
  end
  object btnStatistic: TButton
    Left = 96
    Top = 208
    Width = 73
    Height = 25
    Caption = 'Statistic'
    Enabled = False
    ModalResult = 1
    TabOrder = 2
    OnClick = btnServiceStatisticClick
  end
  object pcProtokolType: TPageControl
    Left = 368
    Top = 24
    Width = 215
    Height = 281
    ActivePage = TabSheetModbus
    Anchors = [akLeft, akTop, akRight]
    TabOrder = 0
    OnChange = DeviceDetailChange
    object TabSheetModbus: TTabSheet
      Caption = 'Modbus'
      ImageIndex = 1
      object lblModbusSlaveID: TLabel
        Left = 8
        Top = 13
        Width = 83
        Height = 13
        Alignment = taCenter
        Caption = 'Modbus slave ID:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object lblComPortNum: TLabel
        Left = 8
        Top = 40
        Width = 86
        Height = 13
        Caption = 'Com Port Number:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lblBaudRate: TLabel
        Left = 8
        Top = 68
        Width = 51
        Height = 13
        Caption = 'BaudRate:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lblParity: TLabel
        Left = 8
        Top = 96
        Width = 29
        Height = 13
        Caption = 'Parity:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lblTimeout: TLabel
        Left = 8
        Top = 124
        Width = 55
        Height = 13
        Caption = 'Timeout [s]:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object cbModbusCommPort: TComboBox
        Left = 96
        Top = 36
        Width = 102
        Height = 22
        Style = csDropDownList
        Anchors = [akLeft, akTop, akRight]
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ItemHeight = 13
        ParentFont = False
        TabOrder = 0
        OnChange = DeviceDetailChange
      end
      object cbBaudRate: TComboBox
        Left = 96
        Top = 64
        Width = 102
        Height = 21
        Style = csDropDownList
        Anchors = [akLeft, akTop, akRight]
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ItemHeight = 13
        ParentFont = False
        TabOrder = 1
        OnChange = DeviceDetailChange
        Items.Strings = (
          '4800'
          '9600'
          '19200'
          '38400')
      end
      object cbParity: TComboBox
        Left = 96
        Top = 92
        Width = 102
        Height = 21
        Style = csDropDownList
        Anchors = [akLeft, akTop, akRight]
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ItemHeight = 13
        ParentFont = False
        TabOrder = 2
        OnChange = DeviceDetailChange
        Items.Strings = (
          'Even, 1 stopbit'
          'Odd, 1 stopbit'
          'None, 2 stopbit'
          'None, 1 stopbit')
      end
      object PBseModbusSlaveID: TPBSpinEdit
        Left = 96
        Top = 8
        Width = 102
        Height = 22
        Cursor = crDefault
        Anchors = [akLeft, akTop, akRight]
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        MaxValue = 247
        MinValue = 1
        ParentFont = False
        TabOrder = 3
        Value = 1
        OnChange = DeviceDetailChange
        Alignment = taLeftJustify
      end
      object PBseModbusTimeout: TPBSpinEdit
        Left = 96
        Top = 120
        Width = 102
        Height = 22
        Cursor = crDefault
        Anchors = [akLeft, akTop, akRight]
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        MaxValue = 60
        MinValue = 1
        ParentFont = False
        TabOrder = 4
        Value = 1
        OnChange = DeviceDetailChange
        Alignment = taLeftJustify
      end
      object RTSCheckBox: TCheckBox
        Left = 8
        Top = 152
        Width = 190
        Height = 17
        Alignment = taLeftJustify
        Anchors = [akLeft, akTop, akRight]
        Caption = 'RTS flow control:'
        TabOrder = 5
        OnClick = DeviceDetailChange
      end
    end
    object TabSheetMagX2: TTabSheet
      Caption = 'proprietary MagX2'
      TabVisible = False
      object Label2: TLabel
        Left = 8
        Top = 5
        Width = 83
        Height = 26
        Caption = 'Serial number remote flowmeter:'
        Transparent = True
        WordWrap = True
      end
      object Label7: TLabel
        Left = 8
        Top = 40
        Width = 83
        Height = 13
        Caption = 'Com port number:'
      end
      object Label5: TLabel
        Left = 8
        Top = 68
        Width = 55
        Height = 13
        Caption = 'Timeout [s]:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object MaskEditSerNo: TMaskEdit
        Left = 96
        Top = 8
        Width = 102
        Height = 21
        Anchors = [akLeft, akTop, akRight]
        EditMask = '!9999999;1;_'
        MaxLength = 7
        TabOrder = 0
        Text = '0000000'
        OnChange = DeviceDetailChange
      end
      object PBseMagX1CommPort: TPBSpinEdit
        Left = 96
        Top = 36
        Width = 102
        Height = 22
        Cursor = crDefault
        Anchors = [akLeft, akTop, akRight]
        MaxValue = 20
        MinValue = 1
        TabOrder = 1
        Value = 1
        OnChange = DeviceDetailChange
        Alignment = taLeftJustify
      end
      object rgConvertorRS485: TRadioGroup
        Left = 8
        Top = 96
        Width = 190
        Height = 57
        Anchors = [akLeft, akTop, akRight]
        Caption = 'Convertor to RS485'
        Color = 12424827
        ItemIndex = 0
        Items.Strings = (
          'From USB(None)'
          'From RS232')
        ParentColor = False
        TabOrder = 2
        OnClick = DeviceDetailChange
      end
      object PBseMagX1Timeout: TPBSpinEdit
        Left = 96
        Top = 64
        Width = 102
        Height = 22
        Cursor = crDefault
        Anchors = [akLeft, akTop, akRight]
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        MaxValue = 60
        MinValue = 1
        ParentFont = False
        TabOrder = 3
        Value = 1
        OnChange = DeviceDetailChange
        Alignment = taLeftJustify
      end
    end
    object TabSheetTCPModbus: TTabSheet
      Caption = 'GPRS && TCP/IP'
      ImageIndex = 2
      object Label9: TLabel
        Left = 8
        Top = 125
        Width = 53
        Height = 13
        Caption = 'IP address:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        Transparent = True
        WordWrap = True
      end
      object Label10: TLabel
        Left = 8
        Top = 152
        Width = 22
        Height = 13
        Caption = 'Port:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label12: TLabel
        Left = 8
        Top = 12
        Width = 83
        Height = 13
        Alignment = taCenter
        Caption = 'Modbus slave ID:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        Transparent = True
        WordWrap = True
      end
      object Label13: TLabel
        Left = 8
        Top = 40
        Width = 51
        Height = 13
        Caption = 'BaudRate:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label14: TLabel
        Left = 8
        Top = 68
        Width = 29
        Height = 13
        Caption = 'Parity:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label15: TLabel
        Left = 8
        Top = 96
        Width = 55
        Height = 13
        Caption = 'Timeout [s]:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object seTCPPort: TPBSpinEdit
        Left = 96
        Top = 148
        Width = 100
        Height = 22
        Cursor = crDefault
        Anchors = [akLeft, akTop, akRight]
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        MaxValue = 65535
        MinValue = 1
        ParentFont = False
        TabOrder = 0
        Value = 23
        OnChange = DeviceDetailChange
        Alignment = taLeftJustify
      end
      object cbTCPBaudRate: TComboBox
        Left = 96
        Top = 36
        Width = 100
        Height = 21
        Style = csDropDownList
        Anchors = [akLeft, akTop, akRight]
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ItemHeight = 13
        ParentFont = False
        TabOrder = 1
        OnChange = DeviceDetailChange
        Items.Strings = (
          '4800'
          '9600'
          '19200'
          '38400')
      end
      object cbTCPParity: TComboBox
        Left = 96
        Top = 64
        Width = 100
        Height = 21
        Style = csDropDownList
        Anchors = [akLeft, akTop, akRight]
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ItemHeight = 13
        ParentFont = False
        TabOrder = 2
        OnChange = DeviceDetailChange
        Items.Strings = (
          'Even, 1 stopbit'
          'Odd, 1 stopbit'
          'None, 2 stopbit'
          'None, 1 stopbit')
      end
      object PBseTCPModbusSlaveID: TPBSpinEdit
        Left = 96
        Top = 7
        Width = 100
        Height = 22
        Cursor = crDefault
        Anchors = [akLeft, akTop, akRight]
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        MaxValue = 247
        MinValue = 1
        ParentFont = False
        TabOrder = 3
        Value = 1
        OnChange = DeviceDetailChange
        Alignment = taLeftJustify
      end
      object PBseTCPModbusTimeout: TPBSpinEdit
        Left = 96
        Top = 92
        Width = 100
        Height = 22
        Cursor = crDefault
        Anchors = [akLeft, akTop, akRight]
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        MaxValue = 60
        MinValue = 1
        ParentFont = False
        TabOrder = 4
        Value = 1
        OnChange = DeviceDetailChange
        Alignment = taLeftJustify
      end
      object edtTCPIPAddress: TJvIPAddress
        Left = 96
        Top = 120
        Width = 100
        Height = 21
        Address = -1
        AddressValues.Address = -1
        AddressValues.Value1 = 255
        AddressValues.Value2 = 255
        AddressValues.Value3 = 255
        AddressValues.Value4 = 255
        Anchors = [akLeft, akTop, akRight]
        ParentColor = False
        TabOrder = 5
        Text = '255.255.255.255'
        OnChange = DeviceDetailChange
      end
    end
  end
  object DemoCheckBox: TCheckBox
    Left = 24
    Top = 304
    Width = 145
    Height = 17
    Caption = 'Demo mode'
    TabOrder = 4
  end
  object lbDevices: TJvTextListBox
    Left = 200
    Top = 24
    Width = 161
    Height = 281
    ExtendedSelect = False
    ItemHeight = 13
    TabOrder = 5
    OnClick = lbDevicesClick
  end
end
