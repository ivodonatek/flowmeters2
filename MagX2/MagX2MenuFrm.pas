unit MagX2MenuFrm;

interface

uses
  Windows,  filectrl, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, ComCtrls, StdCtrls, NodeClass, MenuNodeClass, MagX2MyCommunicationClass, Spin, Buttons,
  ImgList,IniFiles, MagX2DataClass, TeEngine, TeeFunci, Series, TeeProcs, Chart,
  PBSpinEdit, PBNumEdit, PBSuperSpin, MagX2ValueInfoFrame, MagX2HeadFrm, MagX2ValueEditFrame,
  MagX2Value4WinFrame, MagX2ValueBoolFrame, MagX2ValueISelFrame, MagX2ValuePaswFrame,
  MagX2FunctionsUnit, mxCalendar,WaitFrm, MagX2PassFrm, AdomCore_4_3;

const
  BACKGROUND_FILE_NAME = 'MagX2\Background.jpg';
  ERROR_TEXTS_FILE_NAME = 'MagX2\MagX2_Error.dat';
  ERROR_TEXTS_FILE_NAME_LANG = 'MagX2\MagX2_Error_%s.dat';
  CHART_TOLERANCE = 1.2; // 20 %
  GRAPH_ITEMS_COUNT = 100;
  ERROR_CODE_BYTES_COUNT = 32;
  ZERO_FLOW_NAME = 'ZERO_FLOW';
  ZERO_FLOW_CONST_NAME = 'ZERO_FLOW_CONSTANT';
  CALIB_VALUE_NAMES : Array[1..3] of String = ('CALIBRATION_POINT_ONE','CALIBRATION_POINT_TWO','CALIBRATION_POINT_THREE');
  MEASURE_VALUE_NAMES : Array[1..3] of String = ('MEASURED_POINT_ONE','MEASURED_POINT_TWO','MEASURED_POINT_THREE');
  WM_NEED_RESET = WM_USER + 1;

  GSM_IP_VALUE_ADDRESS = 1022 - 1;

  // xml tagy
  ROOT_TAG_NAME = 'Settings';
  SW_TAG_NAME = 'SW';
  FW_TAG_NAME = 'FW';
  SER_NO_TAG_NAME = 'SerNo';
  USER_SETTINGS_TAG_NAME = 'UserSettings';
  SETTING_TAG_NAME = 'Setting';
  TYPE_TAG_NAME = 'Type';
  MODBUS_ADR_TAG_NAME = 'ModbusAdr';
  MODBUS_ADR1_TAG_NAME = 'ModbusAdr1';
  MODBUS_ADR2_TAG_NAME = 'ModbusAdr2';
  MODBUS_ADR3_TAG_NAME = 'ModbusAdr3';
  MODBUS_ADR4_TAG_NAME = 'ModbusAdr4';
  VALUE_TAG_NAME = 'Value';
  VALUE1_TAG_NAME = 'Value1';
  VALUE2_TAG_NAME = 'Value2';
  VALUE3_TAG_NAME = 'Value3';
  VALUE4_TAG_NAME = 'Value4';

type
  TErrorShapeArray = array[1 .. ERROR_CODE_BYTES_COUNT] of TShape;
  TErrorLabelArray = array[1 .. ERROR_CODE_BYTES_COUNT] of TLabel;
  TIntValues4 = array[1..4] of Integer;
  TformMagX2Menu = class(TformMagX2Head)
    tvMenu: TTreeView;
    pcMenu: TPageControl;
    tsEdit: TTabSheet;
    imgPozadi: TImage;
    pnlMenu: TPanel;
    OpenDialog2: TOpenDialog;
    SaveDialog2: TSaveDialog;
    btnMenuBackSave: TButton;
    btnMenuBackLoad: TButton;
    Splitter1: TSplitter;
    tsTime: TTabSheet;
    imgBack1: TImage;
    lblNowInMemTime: TLabel;
    Panel1: TPanel;
    lblTime: TLabel;
    Image13: TImage;
    Image14: TImage;
    Image16: TImage;
    Image17: TImage;
    SpeedButton1: TSpeedButton;
    SpeedButton2: TSpeedButton;
    SpeedButton15: TSpeedButton;
    SpeedButton16: TSpeedButton;
    Panel4: TPanel;
    lblTimeMem: TLabel;
    btnReadTime: TButton;
    btnWriteTime: TButton;
    tsDate: TTabSheet;
    imgBack2: TImage;
    lblNowInMemDate: TLabel;
    Panel3: TPanel;
    lblDateMem: TLabel;
    btnReadDate: TButton;
    btnGoToday: TButton;
    mxCalendar1: TmxCalendar;
    btnWriteDate: TButton;
    tsOnline: TTabSheet;
    imgBack3: TImage;
    Panel5: TPanel;
    lblFlow: TLabel;
    lblTotal: TLabel;
    lblTotalPlus: TLabel;
    lblOnlineFlow: TLabel;
    lblOnlineTotal: TLabel;
    lblOnlineTotalPlus: TLabel;
    OnlineChart: TChart;
    tsCalibration: TTabSheet;
    imgBack4: TImage;
    Panel2: TPanel;
    CalibrationStatusLabel: TLabel;
    lblUnit: TLabel;
    lblCalibrationData: TLabel;
    cbFlowUnit: TComboBox;
    CalSpinEdit1: TPBSuperSpin;
    CalSpinEdit2: TPBSuperSpin;
    CalSpinEdit3: TPBSuperSpin;
    btnWriteCalData1: TButton;
    btnWriteCalData2: TButton;
    btnWriteCalData3: TButton;
    btnAutomaticZeroConst: TButton;
    Panel6: TPanel;
    MeasurmentStatusLabel: TLabel;
    lblMeasurementData: TLabel;
    lblError: TLabel;
    MeasSpinEdit1: TPBSuperSpin;
    MeasSpinEdit2: TPBSuperSpin;
    MeasSpinEdit3: TPBSuperSpin;
    btnWriteMeasData1: TButton;
    btnWriteMeasData2: TButton;
    btnWriteMeasData3: TButton;
    ErrorSpinEdit1: TPBSuperSpin;
    ErrorSpinEdit2: TPBSuperSpin;
    ErrorSpinEdit3: TPBSuperSpin;
    btnMeasurementCalculate: TButton;
    Panel8: TPanel;
    btnReadAll: TButton;
    btnWriteAll: TButton;
    btnOpenDataFile: TButton;
    btnSaveDataFile: TButton;
    OnlineTimer: TTimer;
    SaveDialog1: TSaveDialog;
    OpenDialog1: TOpenDialog;
    lblTotalMinus: TLabel;
    lblOnlineTotalMinus: TLabel;
    lblAuxPlus: TLabel;
    lblOnlineAuxPlus: TLabel;
    lblErrorCode: TLabel;
    lblOnlineErrorCode: TLabel;
    cmbOnlineFlowUnit: TComboBox;
    cmbOnlineTotalUnit: TComboBox;
    Series2: TFastLineSeries;
    tsUpdateFirmware: TTabSheet;
    imgBack5: TImage;
    Panel9: TPanel;
    lblUpdateFirmwareFile: TLabel;
    btnUpdateFWFileSelect: TButton;
    btnUpdateFirmware: TButton;
    progUpdateFirmware: TProgressBar;
    lblUpdateFirmwareStatus: TLabel;
    edtUpdateFirmwareFile: TEdit;
    lblUpdateFirmwareComment: TLabel;
    tsGPRS: TTabSheet;
    Panel10: TPanel;
    Label9: TLabel;
    Label10: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    lblGPRS_IPaddress: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    shpGPRS_Status: TShape;
    Label2: TLabel;
    lblGPRS_Status: TLabel;
    btnGPRS_Read: TButton;
    edtGPRS_Gateway: TEdit;
    edtGPRS_User: TEdit;
    edtGPRS_Password: TEdit;
    seGPRS_Port: TSpinEdit;
    seGPRS_PIN: TSpinEdit;
    btnGPRS_Write: TButton;
    imgBack6: TImage;
    lblTemp: TLabel;
    lblOnlineTemp: TLabel;
    btnManualZeroConst: TButton;
    ZeroConstSpinEdit: TSpinEdit;
    tsGSM: TTabSheet;
    imgBack7: TImage;
    Panel11: TPanel;
    Label1: TLabel;
    btnGSMReadNumbers: TButton;
    edtGSMPhone1: TEdit;
    btnGSMWriteNumbers: TButton;
    edtGSMPhone2: TEdit;
    Label3: TLabel;
    edtGSMPhone3: TEdit;
    Label6: TLabel;
    lblGSMNumbersStatus: TLabel;
    Panel12: TPanel;
    lblGSMEventsStatus: TLabel;
    btnGSMReadEvents: TButton;
    btnGSMWriteEvents: TButton;
    GroupBox1: TGroupBox;
    Label12: TLabel;
    cmbGSMEventEmptyPipe: TComboBox;
    Label21: TLabel;
    cmbGSMEventZeroFlow: TComboBox;
    Label22: TLabel;
    cmbGSMEventError: TComboBox;
    GroupBox2: TGroupBox;
    Label13: TLabel;
    Label18: TLabel;
    cmbGSMSendEventEmptyPipe: TComboBox;
    cmbGSMSendEventZeroFlow: TComboBox;
    Label23: TLabel;
    seGSMInterval: TSpinEdit;
    Label11: TLabel;
    Label20: TLabel;
    Label24: TLabel;
    Panel13: TPanel;
    shpRealTimeError1: TShape;
    shpRealTimeError2: TShape;
    shpRealTimeError3: TShape;
    shpRealTimeError4: TShape;
    shpRealTimeError5: TShape;
    shpRealTimeError6: TShape;
    shpRealTimeError7: TShape;
    shpRealTimeError8: TShape;
    shpRealTimeError9: TShape;
    shpRealTimeError10: TShape;
    shpRealTimeError11: TShape;
    shpRealTimeError12: TShape;
    shpRealTimeError13: TShape;
    shpRealTimeError14: TShape;
    shpRealTimeError15: TShape;
    shpRealTimeError16: TShape;
    lblRealTimeError1: TLabel;
    lblRealTimeError5: TLabel;
    lblRealTimeError9: TLabel;
    lblRealTimeError13: TLabel;
    lblRealTimeError2: TLabel;
    lblRealTimeError6: TLabel;
    lblRealTimeError10: TLabel;
    lblRealTimeError14: TLabel;
    lblRealTimeError3: TLabel;
    lblRealTimeError4: TLabel;
    lblRealTimeError7: TLabel;
    lblRealTimeError8: TLabel;
    lblRealTimeError11: TLabel;
    lblRealTimeError12: TLabel;
    lblRealTimeError15: TLabel;
    lblRealTimeError16: TLabel;
    shpRealTimeError17: TShape;
    shpRealTimeError18: TShape;
    shpRealTimeError19: TShape;
    shpRealTimeError20: TShape;
    shpRealTimeError21: TShape;
    shpRealTimeError22: TShape;
    shpRealTimeError23: TShape;
    shpRealTimeError24: TShape;
    shpRealTimeError25: TShape;
    shpRealTimeError26: TShape;
    shpRealTimeError27: TShape;
    shpRealTimeError28: TShape;
    shpRealTimeError29: TShape;
    shpRealTimeError30: TShape;
    shpRealTimeError31: TShape;
    shpRealTimeError32: TShape;
    lblRealTimeError17: TLabel;
    lblRealTimeError18: TLabel;
    lblRealTimeError21: TLabel;
    lblRealTimeError22: TLabel;
    lblRealTimeError23: TLabel;
    lblRealTimeError19: TLabel;
    lblRealTimeError20: TLabel;
    lblRealTimeError24: TLabel;
    lblRealTimeError28: TLabel;
    lblRealTimeError27: TLabel;
    lblRealTimeError26: TLabel;
    lblRealTimeError25: TLabel;
    lblRealTimeError29: TLabel;
    lblRealTimeError30: TLabel;
    lblRealTimeError31: TLabel;
    lblRealTimeError32: TLabel;
    lblErrorTable: TLabel;
    lblSensorUnitNoTitle: TLabel;
    seSensorUnitNo: TSpinEdit;
    btnWriteSensorUnitNo: TButton;
    Panel14: TPanel;
    lblFlow2: TLabel;
    lblTotal2: TLabel;
    lblTotalPlus2: TLabel;
    lblOnlineFlow2: TLabel;
    lblOnlineTotal2: TLabel;
    lblOnlineTotalPlus2: TLabel;
    lblTotalMinus2: TLabel;
    lblOnlineTotalMinus2: TLabel;
    lblAuxPlus2: TLabel;
    lblOnlineAuxPlus2: TLabel;
    lblErrorCode2: TLabel;
    lblOnlineErrorCode2: TLabel;
    lblTemp2: TLabel;
    lblOnlineTemp2: TLabel;
    cmbOnlineFlowUnit2: TComboBox;
    cmbOnlineTotalUnit2: TComboBox;
    OnlineChart2: TChart;
    FastLineSeries1: TFastLineSeries;
    lblA1: TLabel;
    lblA2: TLabel;
    lblA3: TLabel;
    lblA: TLabel;
    lblOnlineA1: TLabel;
    lblOnlineA2: TLabel;
    lblOnlineA3: TLabel;
    lblOnlineA: TLabel;
    lblXTemp: TLabel;
    lblOnlineXTemp: TLabel;
    lblXPress: TLabel;
    lblOnlineXPress: TLabel;
    procedure FormDestroy(Sender: TObject);
    procedure tvMenuChange(Sender: TObject; Node: TTreeNode);
    procedure tvMenuExpanding(Sender: TObject; Node: TTreeNode;
      var AllowExpansion: Boolean);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure btnMenuBackSaveClick(Sender: TObject);
    procedure btnMenuBackLoadClick(Sender: TObject);
    Procedure IncTime( Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure btnReadTimeClick(Sender: TObject);
    procedure btnWriteTimeClick(Sender: TObject);
    procedure btnReadDateClick(Sender: TObject);
    procedure btnWriteDateClick(Sender: TObject);
    procedure btnGoTodayClick(Sender: TObject);
    procedure OnlineTimerTimer(Sender: TObject);
    procedure btnWriteCalDataClick(Sender: TObject);
    procedure btnAutomaticZeroConstClick(Sender: TObject);
    procedure btnWriteMeasDataClick(Sender: TObject);
    procedure btnMeasurementCalculateClick(Sender: TObject);
    procedure btnReadAllClick(Sender: TObject);
    procedure btnWriteAllClick(Sender: TObject);
    procedure btnOpenDataFileClick(Sender: TObject);
    procedure btnSaveDataFileClick(Sender: TObject);
    procedure pcMenuChanging(Sender: TObject; var AllowChange: Boolean);
    procedure pcMenuChange(Sender: TObject);
    procedure btnUpdateFWFileSelectClick(Sender: TObject);
    //procedure btnUpdateFirmwareClick(Sender: TObject);
    procedure edtUpdateFirmwareFileChange(Sender: TObject);
    procedure btnGPRS_ReadClick(Sender: TObject);
    procedure btnGPRS_WriteClick(Sender: TObject);
    procedure btnManualZeroConstClick(Sender: TObject);
    procedure btnGSMReadNumbersClick(Sender: TObject);
    procedure btnGSMWriteNumbersClick(Sender: TObject);
    procedure edtGSMPhoneChange(Sender: TObject);
    procedure btnGSMReadEventsClick(Sender: TObject);
    procedure btnGSMWriteEventsClick(Sender: TObject);
    procedure btnWriteSensorUnitNoClick(Sender: TObject);
  private
    { Private declarations }
    NeedReset: Boolean;
    MenuNodes : TMenuNodes;
    Nodes : TNodes;
    MyCommunication : TMyCommunication;
    {---}
    EditFrame : Tframe;
    OnlineErr:integer;
    time:TmyTime;
    OnlinePocitadlo:Integer;
    TabIndexPred: word;
    Busy:Boolean;
    MinChartValue, MaxChartValue: Double;
    MinChartCnt, MaxChartCnt: Integer;
    ErrorShapeArray: TErrorShapeArray;
    ErrorLabelArray: TErrorLabelArray;
    procedure LoadFromIniFile;
    procedure SaveToIniFile;
    function CreateXmlElement(const DomDocument: TDomDocument; const Node: TNode; const TagName: string; const Values: TIntValues4): TDomElement;
    procedure ParseXmlElement(const DomElement: TDomElement; var Node: TNode; var Values: TIntValues4);
    function GetNode(ModbusAdr: Integer) : TNode;
  public
    { Public declarations }
    procedure WMNeedReset(var Message: TMessage); message WM_NEED_RESET;
    procedure Init(AMyCommunication : TMyCommunication; AfirmwareNO:integer; AProtokolType:TProtokolType);
    procedure TranslateForm;
  end;

var
  //formMagX2Menu: TformMagX2Menu;
  Secure: array [1..4] of Boolean;

const
  ONLINE_FLOW_UNITS = 5;
  ONLINE_FLOW_UNIT_TEXTS: array [0 .. ONLINE_FLOW_UNITS - 1] of String = ('m3/h', 'UKG/min', 'USG/min' , 'l/min', 'l/s');
  ONLINE_FLOW_UNIT_COEFS: array [0 .. ONLINE_FLOW_UNITS - 1] of Double = (1.0, 3.6661541383, 4.4028675393, 16.66666666666, 0.27777777778);
  ONLINE_TOTAL_UNITS = 4;
  ONLINE_TOTAL_UNIT_TEXTS: array[0 .. ONLINE_TOTAL_UNITS - 1] of String = ('m3', 'UKG', 'USG', 'l');
  ONLINE_TOTAL_UNIT_COEFS: array[0 .. ONLINE_TOTAL_UNITS - 1] of Double = (1.0, 219.9692483, 264.17205236, 1000.0);

implementation

uses MagX1Exception, Math, MagX2GlobalUtilsClass, MagX2PassForm, MagX2ValueIBinFrame,
  MagX2ValueIHexFrame, MagX2TranslationUnit, XModem, ShellApi, {ZipMstr,}
  MagX2ValueIHDTFrame;

{$R *.DFM}

procedure TformMagX2Menu.FormCreate(Sender: TObject);
var I: Integer;
    FileName: String;
    SL: TStringList;
begin
  inherited;
  TranslateForm;
  NeedReset := False;

  // Error Labels
  ErrorLabelArray[1] := lblRealTimeError1;
  ErrorLabelArray[2] := lblRealTimeError2;
  ErrorLabelArray[3] := lblRealTimeError3;
  ErrorLabelArray[4] := lblRealTimeError4;
  ErrorLabelArray[5] := lblRealTimeError5;
  ErrorLabelArray[6] := lblRealTimeError6;
  ErrorLabelArray[7] := lblRealTimeError7;
  ErrorLabelArray[8] := lblRealTimeError8;
  ErrorLabelArray[9] := lblRealTimeError9;
  ErrorLabelArray[10] := lblRealTimeError10;
  ErrorLabelArray[11] := lblRealTimeError11;
  ErrorLabelArray[12] := lblRealTimeError12;
  ErrorLabelArray[13] := lblRealTimeError13;
  ErrorLabelArray[14] := lblRealTimeError14;
  ErrorLabelArray[15] := lblRealTimeError15;
  ErrorLabelArray[16] := lblRealTimeError16;
  ErrorLabelArray[17] := lblRealTimeError17;
  ErrorLabelArray[18] := lblRealTimeError18;
  ErrorLabelArray[19] := lblRealTimeError19;
  ErrorLabelArray[20] := lblRealTimeError20;
  ErrorLabelArray[21] := lblRealTimeError21;
  ErrorLabelArray[22] := lblRealTimeError22;
  ErrorLabelArray[23] := lblRealTimeError23;
  ErrorLabelArray[24] := lblRealTimeError24;
  ErrorLabelArray[25] := lblRealTimeError25;
  ErrorLabelArray[26] := lblRealTimeError26;
  ErrorLabelArray[27] := lblRealTimeError27;
  ErrorLabelArray[28] := lblRealTimeError28;
  ErrorLabelArray[29] := lblRealTimeError29;
  ErrorLabelArray[30] := lblRealTimeError30;
  ErrorLabelArray[31] := lblRealTimeError31;
  ErrorLabelArray[32] := lblRealTimeError32;

  // Error Shapes
  ErrorShapeArray[1] := shpRealTimeError1;
  ErrorShapeArray[2] := shpRealTimeError2;
  ErrorShapeArray[3] := shpRealTimeError3;
  ErrorShapeArray[4] := shpRealTimeError4;
  ErrorShapeArray[5] := shpRealTimeError5;
  ErrorShapeArray[6] := shpRealTimeError6;
  ErrorShapeArray[7] := shpRealTimeError7;
  ErrorShapeArray[8] := shpRealTimeError8;
  ErrorShapeArray[9] := shpRealTimeError9;
  ErrorShapeArray[10] := shpRealTimeError10;
  ErrorShapeArray[11] := shpRealTimeError11;
  ErrorShapeArray[12] := shpRealTimeError12;
  ErrorShapeArray[13] := shpRealTimeError13;
  ErrorShapeArray[14] := shpRealTimeError14;
  ErrorShapeArray[15] := shpRealTimeError15;
  ErrorShapeArray[16] := shpRealTimeError16;
  ErrorShapeArray[17] := shpRealTimeError17;
  ErrorShapeArray[18] := shpRealTimeError18;
  ErrorShapeArray[19] := shpRealTimeError19;
  ErrorShapeArray[20] := shpRealTimeError20;
  ErrorShapeArray[21] := shpRealTimeError21;
  ErrorShapeArray[22] := shpRealTimeError22;
  ErrorShapeArray[23] := shpRealTimeError23;
  ErrorShapeArray[24] := shpRealTimeError24;
  ErrorShapeArray[25] := shpRealTimeError25;
  ErrorShapeArray[26] := shpRealTimeError26;
  ErrorShapeArray[27] := shpRealTimeError27;
  ErrorShapeArray[28] := shpRealTimeError28;
  ErrorShapeArray[29] := shpRealTimeError29;
  ErrorShapeArray[30] := shpRealTimeError30;
  ErrorShapeArray[31] := shpRealTimeError31;
  ErrorShapeArray[32] := shpRealTimeError32;

  try
    SL := TStringList.Create();
    try
      FileName := IncludeTrailingBackslash(ExtractFilePath(Application.ExeName)) + Format(ERROR_TEXTS_FILE_NAME_LANG, [GlobalLanguageSection]);
      if not FileExists(FileName) then begin
        FileName := IncludeTrailingBackslash(ExtractFilePath(Application.ExeName)) + ERROR_TEXTS_FILE_NAME;
      end;
      SL.LoadFromFile(FileName);
      for I := 1 to ERROR_CODE_BYTES_COUNT do begin
        if (I <= SL.Count) then begin
          ErrorLabelArray[I].Caption := SL[I-1];
        end;
      end;
    finally
      SL.Free;
    end;
  except
    on E: Exception do begin
      MessageDlg(Format('Loading ErrorCodes from file "%s" failed!'#13#10'%s [%s]', [FileName, E.Message, E.ClassName]), mtError, [mbOK], 0);
    end;
  end;

  pcMenu.ActivePageIndex:=0;

  for I := 1 to 4 do Secure[I] := False;
  CalibrationStatusLabel.Caption := '';
  MeasurmentStatusLabel.Caption := '';

  // naplneni tabulky jednotek kalibrace
  cbFlowUnit.Items.Clear;
  for I := Low(FlowUnitList) to High(FlowUnitList) do begin
    cbFlowUnit.Items.Add(FlowUnitList[I]);
  end;
  cbFlowUnit.ItemIndex := Ord(m3_h);

  //vymaz online graf a nastav 100 nul
  OnlineChart.SeriesList[0].Clear;
  OnlineChart2.SeriesList[0].Clear;
  for I := 0 to GRAPH_ITEMS_COUNT - 1 do begin
    OnlineChart.SeriesList[0].AddXY(I, 0);
    OnlineChart2.SeriesList[0].AddXY(I, 0);    
  end;

  MinChartValue := 0;
  MaxChartValue := 0;
  MinChartCnt := 0;
  MaxChartCnt := 0;
  cmbOnlineFlowUnit.Items.Clear;
  cmbOnlineFlowUnit2.Items.Clear;  
  for I := 0 to ONLINE_FLOW_UNITS do begin
    cmbOnlineFlowUnit.Items.Add(ONLINE_FLOW_UNIT_TEXTS[I]);
    cmbOnlineFlowUnit2.Items.Add(ONLINE_FLOW_UNIT_TEXTS[I]);
  end;
  cmbOnlineFlowUnit.ItemIndex := 0;
  cmbOnlineFlowUnit2.ItemIndex := 0;
  cmbOnlineTotalUnit.Items.Clear;
  cmbOnlineTotalUnit2.Items.Clear;  
  for I := 0 to ONLINE_TOTAL_UNITS do begin
    cmbOnlineTotalUnit.Items.Add(ONLINE_TOTAL_UNIT_TEXTS[I]);
    cmbOnlineTotalUnit2.Items.Add(ONLINE_TOTAL_UNIT_TEXTS[I]);
  end;
  cmbOnlineTotalUnit.ItemIndex := 0;
  cmbOnlineTotalUnit2.ItemIndex := 0;  
  imgPozadi.Picture.LoadFromFile(IncludeTrailingBackslash(ExtractFilePath(Application.ExeName)) + BACKGROUND_FILE_NAME);
  imgBack1.Picture.LoadFromFile(IncludeTrailingBackslash(ExtractFilePath(Application.ExeName)) + BACKGROUND_FILE_NAME);
  imgBack2.Picture.LoadFromFile(IncludeTrailingBackslash(ExtractFilePath(Application.ExeName)) + BACKGROUND_FILE_NAME);
  imgBack3.Picture.LoadFromFile(IncludeTrailingBackslash(ExtractFilePath(Application.ExeName)) + BACKGROUND_FILE_NAME);
  imgBack4.Picture.LoadFromFile(IncludeTrailingBackslash(ExtractFilePath(Application.ExeName)) + BACKGROUND_FILE_NAME);
  imgBack5.Picture.LoadFromFile(IncludeTrailingBackslash(ExtractFilePath(Application.ExeName)) + BACKGROUND_FILE_NAME);
  imgBack6.Picture.LoadFromFile(IncludeTrailingBackslash(ExtractFilePath(Application.ExeName)) + BACKGROUND_FILE_NAME);
  imgBack7.Picture.LoadFromFile(IncludeTrailingBackslash(ExtractFilePath(Application.ExeName)) + BACKGROUND_FILE_NAME);

  // Hide upgrade tab
  tsUpdateFirmware.TabVisible := False;
  tsUpdateFirmware.Visible := False;

  LoadFromIniFile;
end;

procedure TformMagX2Menu.LoadFromIniFile;
var sExePath: String;
    iniF: TIniFile;
begin
  //nacteni velikosti Formu
  sExePath := GetUserAppDataProductPath();
  iniF := TIniFile.Create(sExePath+'MagX2.ini');
  try
    Self.Width:=iniF.ReadInteger('Service','Width',800);
    Self.Height:=iniF.ReadInteger('Service','Height',600);
    if(iniF.ReadBool('Service','Maximized', false) = true) then begin
      Self.WindowState:=wsMaximized
    end else begin
      Self.WindowState:=wsNormal;
    end;
  finally
    iniF.Free;
  end;
end;

procedure TformMagX2Menu.SaveToIniFile;
var sExePath: String;
    iniF: TIniFile;
begin
  sExePath := GetUserAppDataProductPath();
  iniF := TIniFile.Create(sExePath+'MagX2.ini');
  try
    iniF.WriteBool('Service','Maximized', Self.WindowState=wsMaximized);
    if Self.WindowState = wsNormal then begin
      iniF.WriteInteger('Service','Width',Self.Width);
      iniF.WriteInteger('Service','Height',Self.Height);
    end;
  finally
    iniF.Free;
  end;
end;

procedure TformMagX2Menu.FormDestroy(Sender: TObject);
begin
  SaveToIniFile;
  Nodes.Free;
  MenuNodes.Free;
  FreeAndNil(MyCommunication);
  inherited;
end;

procedure TformMagX2Menu.tvMenuChange(Sender: TObject; Node: TTreeNode);
begin
  Self.Enabled:=false;
  try
    if Assigned(Node.Data) then
    begin
      if Assigned(TMenuNode(Node.Data).MenuEdit) then
      begin
        pcMenu.ActivePageIndex:=0;
        if Assigned(EditFrame) then
            FreeAndNil(EditFrame);
        case TNode(TMenuNode(Node.Data).MenuEdit).Typ of
          1 : begin
                EditFrame:=TframeMagX2ValueInfo.CreateFrame(Self,MyCommunication,TNode(TMenuNode(Node.Data).MenuEdit), LocalLanguage, MultiLanguageTextIndex);
                EditFrame.Parent:=tsEdit;
              end;
          2 : begin
                EditFrame:=TframeMagX2ValueEdit.CreateFrame(Self,MyCommunication,TNode(TMenuNode(Node.Data).MenuEdit), LocalLanguage, MultiLanguageTextIndex);
                EditFrame.Parent:=tsEdit;
              end;
          3 : begin
                EditFrame:=TframeMagX2Value4Win.CreateFrame(Self,MyCommunication,TNode(TMenuNode(Node.Data).MenuEdit), LocalLanguage, MultiLanguageTextIndex);
                EditFrame.Parent:=tsEdit;
              end;
          4 : begin
                EditFrame:=TframeMagX2ValueISel.CreateFrame(Self,MyCommunication,TNode(TMenuNode(Node.Data).MenuEdit), LocalLanguage, MultiLanguageTextIndex);
                EditFrame.Parent:=tsEdit;
              end;
          5 : begin
                EditFrame:=TframeMagX2ValueBool.CreateFrame(Self,MyCommunication,TNode(TMenuNode(Node.Data).MenuEdit), LocalLanguage, MultiLanguageTextIndex);
                EditFrame.Parent:=tsEdit;
              end;
          9 : begin
                  EditFrame:=TframeMagX2ValuePasw.CreateFrame(Self,MyCommunication,TNode(TMenuNode(Node.Data).MenuEdit), LocalLanguage, MultiLanguageTextIndex);
                  EditFrame.Parent:=tsEdit;
                end;
          11: begin
                  EditFrame:=TframeMagX2ValueIHex.CreateFrame(Self,MyCommunication,TNode(TMenuNode(Node.Data).MenuEdit), LocalLanguage, MultiLanguageTextIndex);
                  EditFrame.Parent:=tsEdit;
              end;
          12: begin
                  EditFrame:=TframeMagX2ValueIBin.CreateFrame(Self,MyCommunication,TNode(TMenuNode(Node.Data).MenuEdit), LocalLanguage, MultiLanguageTextIndex);
                  EditFrame.Parent:=tsEdit;
              end;
          13: begin
                  EditFrame:=TframeMagX2ValueIHDT.CreateFrame(Self,MyCommunication,TNode(TMenuNode(Node.Data).MenuEdit), LocalLanguage, MultiLanguageTextIndex);
                  EditFrame.Parent:=tsEdit;
              end;
        end;
      end;
    end;
  finally
    Self.Enabled := true;
    tvMenu.SetFocus;
  end;
end;

procedure TformMagX2Menu.tvMenuExpanding(Sender: TObject; Node: TTreeNode;
  var AllowExpansion: Boolean);
var	ReadOK:Boolean;
	FourByte: T4Byte;
begin
    Self.Enabled:=false;
	if (TNode(TMenuNode(Node.Data).MenuEdit).Typ = 9) then	//pokud typ == PASSWD
    begin
      repeat	//cti zda je zadano heslo;
          ReadOK := MyCommunication.ReadMenuValue( TNode(TMenuNode(Node.Data).MenuEdit).ModbusAdr-1, FourByte)
      until(ReadOK)or(MessageDlg(GetTranslationText('READ_ERROR', STR_READ_ERROR),mtError, [mbRetry, mbAbort], 0) = mrAbort);
      if ReadOK then
      begin
         if(Integer(FourByte)=1) then	//pokud heslo OK, povol Expanding menu
         	AllowExpansion:=true
         else
         	AllowExpansion:=false;
      end
      else
          Raise Exception.Create(GetTranslationText('READ_ERROR', STR_READ_ERROR));  //ukonci cteni
    end;
    Self.Enabled:=true;
end;

procedure TformMagX2Menu.FormClose(Sender: TObject; var Action: TCloseAction);
var FourByte: T4Byte;
    I: Integer;
begin
  inherited;
  if (not NeedReset) and (MyCommunication <> nil) then begin
    Integer(FourByte) := 0;
    for I := Low(PassValueName) to High(PassValueName) do begin
      if (Values.IndexOf(PassValueName[I]) >= 0) then begin
        if not MyCommunication.WriteMenuValue(GetAddress(ProtokolType, PassValueName[I], Values), FourByte) then begin
          break; // Do not try again
        end;
      end;
    end;
  end else begin
    NeedReset := False;
  end;

  Action := caFree;
end;

//------------------------------------------------------------------------------

procedure TformMagX2Menu.Init(AMyCommunication : TMyCommunication; AfirmwareNO:integer; AProtokolType:TProtokolType);
var sExePath: string;
	filename: string;
    WaitFrm : TformWait;
begin
  inherited;
  Self.MyCommunication := AMyCommunication;

  if (MyCommunication.ProtokolType = ptDemo) then begin
    Caption := Caption + ' (DEMO)';
    lblDemo.Visible := True;
  end;

  // Tabs
  tsGPRS.TabVisible := (firmwareNO >= 2110);
  tsGSM.TabVisible := (firmwareNO >= 2100);

  lblSensorUnitNoTitle.Visible := (firmwareNO >= 2112);
  seSensorUnitNo.Visible := (firmwareNO >= 2112);
  btnWriteSensorUnitNo.Visible := (firmwareNO >= 2112);

  MenuNodes:=TMenuNodes.Create;
  Nodes:=TNodes.Create;
  Nodes.LanguageIndex:= MultiLanguageTextIndex;
  Nodes.ConvertWideCharsPage:= GetConvertWideCharsPage(LocalLanguage, AfirmwareNO);
  sExePath := ExtractFilePath(Application.ExeName) + 'MagX2\';
  WaitFrm:=TformWait.Create(Self);
  try
    Screen.Cursor:=crHourGlass;
    WaitFrm.Caption:=GetTranslationText('LOAD_PROG_STRUCT', STR_LOAD_PROG_STRUCT);
    WaitFrm.gWait.Progress:=0;
    WaitFrm.Show;
    SetCenter(WaitFrm);
    Application.ProcessMessages;

    filename := Format('Menu%d.dat', [firmwareNO]);
    if FileExists(sExePath+filename) then begin
      Nodes.LoadFromFile(sExePath+filename,nil)
    end else begin
      raise Exception.Create(GetTranslationText('UNSUPPORTED_FW', STR_UNSUPPORTED_FW) + Format(' (%d)', [firmwareNO]));  //ukonci cteni
    end;

    WaitFrm.gWait.Progress:=10;
    Application.ProcessMessages;
    MenuNodes.LoadMenuNodes(Nodes);
    WaitFrm.gWait.Progress:=20;
    Application.ProcessMessages;
    MenuNodes.FillTreeView(tvMenu);
    WaitFrm.gWait.Progress:=30;
    Application.ProcessMessages;
    tvMenu.FullExpand;
    WaitFrm.gWait.Progress:=100;
    Application.ProcessMessages;
    WaitFrm.Close;
  finally
    Screen.Cursor:=crDefault;
    FreeAndNil(WaitFrm);
  end;

  pcMenu.Enabled:=true;
  tvMenu.Enabled:=true;

  case MyCommunication.ProtokolType of
    ptMagX1:
        Raise Exception.Create(GetTranslationText('UNSUPPORTED_COM_PROTOCOL', STR_UNSUPPORTED_COM_PROTOCOL));  //ukonci cteni
    else
    begin
      btnMenuBackSave.Visible:=true;
      btnMenuBackLoad.Visible:=true;
      tvMenu.Align:=alTop;
    end;
  end;
end;

(*
procedure TformMenu.ConnectTimerTimer(Sender: TObject);
var Idx:Integer;
    Data:T4Byte;
begin
  Idx:=Values.IndexOf('UNIT_NO');
  if TMyCommPort(MyCommunication).ReadMenuValue(4*Idx,Data) then
  begin
    if not SerNo=Cardinal(Data) then
    begin
      Label10.Caption:='disconnected';
      Shape2.Brush.Color:=clRed;
      OnlineTimer.Enabled:=false;
//      ConnectTimer.enabled:=true;
      tmrCounting.Enabled:=false;
      pcMenu.Enabled:=false;
      tvMenu.Enabled:=false;
      MyCommunication.Disconnect;
      ShowMessage('Connection error');
    end;
  end
  else if not OnlineTimer.Enabled then
  begin
    Inc(ConnectionErr);
    if ConnectionErr > ConnectionMAX_ERROR then
    begin
      Label10.Caption:='disconnected';
      Shape2.Brush.Color:=clRed;
      OnlineTimer.Enabled:=false;
      tmrCounting.Enabled:=false;
      pcMenu.Enabled:=false;
      tvMenu.Enabled:=false;
      MyCommunication.Disconnect;
      ShowMessage('Connection error');
    end;
  end
end;
*)

function TformMagX2Menu.CreateXmlElement(const DomDocument: TDomDocument; const Node: TNode; const TagName: string; const Values: TIntValues4): TDomElement;
var
  DomText: TDomText;
  ParameterElement: TDomElement;
begin
  Result := TDomElement.Create(DomDocument, TagName);

  ParameterElement := TDomElement.Create(DomDocument, TYPE_TAG_NAME);
  DomText := TDomText.Create(DomDocument);
  DomText.NodeValue := IntToStr(Node.Typ);
  ParameterElement.AppendChild(DomText);
  Result.AppendChild(ParameterElement);

  if Node.Typ = 3 then begin //4Win
    ParameterElement := TDomElement.Create(DomDocument, MODBUS_ADR1_TAG_NAME);
    DomText := TDomText.Create(DomDocument);
    DomText.NodeValue := IntToStr(Node.ModbusAdr - 1);
    ParameterElement.AppendChild(DomText);
    Result.AppendChild(ParameterElement);

    ParameterElement := TDomElement.Create(DomDocument, VALUE1_TAG_NAME);
    DomText := TDomText.Create(DomDocument);
    DomText.NodeValue := IntToStr(Values[1]);
    ParameterElement.AppendChild(DomText);
    Result.AppendChild(ParameterElement);

    ParameterElement := TDomElement.Create(DomDocument, MODBUS_ADR2_TAG_NAME);
    DomText := TDomText.Create(DomDocument);
    DomText.NodeValue := IntToStr(Node.ModbusAdr2 - 1);
    ParameterElement.AppendChild(DomText);
    Result.AppendChild(ParameterElement);

    ParameterElement := TDomElement.Create(DomDocument, VALUE2_TAG_NAME);
    DomText := TDomText.Create(DomDocument);
    DomText.NodeValue := IntToStr(Values[2]);
    ParameterElement.AppendChild(DomText);
    Result.AppendChild(ParameterElement);

    ParameterElement := TDomElement.Create(DomDocument, MODBUS_ADR3_TAG_NAME);
    DomText := TDomText.Create(DomDocument);
    DomText.NodeValue := IntToStr(Node.ModbusAdr3 - 1);
    ParameterElement.AppendChild(DomText);
    Result.AppendChild(ParameterElement);

    ParameterElement := TDomElement.Create(DomDocument, VALUE3_TAG_NAME);
    DomText := TDomText.Create(DomDocument);
    DomText.NodeValue := IntToStr(Values[3]);
    ParameterElement.AppendChild(DomText);
    Result.AppendChild(ParameterElement);

    ParameterElement := TDomElement.Create(DomDocument, MODBUS_ADR4_TAG_NAME);
    DomText := TDomText.Create(DomDocument);
    DomText.NodeValue := IntToStr(Node.ModbusAdr4 - 1);
    ParameterElement.AppendChild(DomText);
    Result.AppendChild(ParameterElement);

    ParameterElement := TDomElement.Create(DomDocument, VALUE4_TAG_NAME);
    DomText := TDomText.Create(DomDocument);
    DomText.NodeValue := IntToStr(Values[4]);
    ParameterElement.AppendChild(DomText);
    Result.AppendChild(ParameterElement);
  end else begin // 2,4 //Edit, Isel
    ParameterElement := TDomElement.Create(DomDocument, MODBUS_ADR_TAG_NAME);
    DomText := TDomText.Create(DomDocument);
    DomText.NodeValue := IntToStr(Node.ModbusAdr - 1);
    ParameterElement.AppendChild(DomText);
    Result.AppendChild(ParameterElement);

    ParameterElement := TDomElement.Create(DomDocument, VALUE_TAG_NAME);
    DomText := TDomText.Create(DomDocument);
    DomText.NodeValue := IntToStr(Values[1]);
    ParameterElement.AppendChild(DomText);
    Result.AppendChild(ParameterElement);
  end;
end;

procedure TformMagX2Menu.ParseXmlElement(const DomElement: TDomElement; var Node: TNode; var Values: TIntValues4);
var
  DomNodeList: TDomNodeList;
  DomText: TDomText;
  ParameterElement: TDomElement;
begin
  DomNodeList := DomElement.GetElementsByTagName(TYPE_TAG_NAME);
  ParameterElement := TDomElement(DomNodeList.Item(0));
  DomText := TDomText(ParameterElement.FirstChild);
  Node.Typ := StrToInt(DomText.Data);

  if Node.Typ = 3 then begin //4Win
    DomNodeList := DomElement.GetElementsByTagName(MODBUS_ADR1_TAG_NAME);
    ParameterElement := TDomElement(DomNodeList.Item(0));
    DomText := TDomText(ParameterElement.FirstChild);
    Node.ModbusAdr := StrToInt(DomText.Data) + 1;

    DomNodeList := DomElement.GetElementsByTagName(VALUE1_TAG_NAME);
    ParameterElement := TDomElement(DomNodeList.Item(0));
    DomText := TDomText(ParameterElement.FirstChild);
    Values[1] := StrToInt(DomText.Data);

    DomNodeList := DomElement.GetElementsByTagName(MODBUS_ADR2_TAG_NAME);
    ParameterElement := TDomElement(DomNodeList.Item(0));
    DomText := TDomText(ParameterElement.FirstChild);
    Node.ModbusAdr2 := StrToInt(DomText.Data) + 1;

    DomNodeList := DomElement.GetElementsByTagName(VALUE2_TAG_NAME);
    ParameterElement := TDomElement(DomNodeList.Item(0));
    DomText := TDomText(ParameterElement.FirstChild);
    Values[2] := StrToInt(DomText.Data);

    DomNodeList := DomElement.GetElementsByTagName(MODBUS_ADR3_TAG_NAME);
    ParameterElement := TDomElement(DomNodeList.Item(0));
    DomText := TDomText(ParameterElement.FirstChild);
    Node.ModbusAdr3 := StrToInt(DomText.Data) + 1;

    DomNodeList := DomElement.GetElementsByTagName(VALUE3_TAG_NAME);
    ParameterElement := TDomElement(DomNodeList.Item(0));
    DomText := TDomText(ParameterElement.FirstChild);
    Values[3] := StrToInt(DomText.Data);

    DomNodeList := DomElement.GetElementsByTagName(MODBUS_ADR4_TAG_NAME);
    ParameterElement := TDomElement(DomNodeList.Item(0));
    DomText := TDomText(ParameterElement.FirstChild);
    Node.ModbusAdr4 := StrToInt(DomText.Data) + 1;

    DomNodeList := DomElement.GetElementsByTagName(VALUE4_TAG_NAME);
    ParameterElement := TDomElement(DomNodeList.Item(0));
    DomText := TDomText(ParameterElement.FirstChild);
    Values[4] := StrToInt(DomText.Data);
  end else begin // 2,4 //Edit, Isel
    DomNodeList := DomElement.GetElementsByTagName(MODBUS_ADR_TAG_NAME);
    ParameterElement := TDomElement(DomNodeList.Item(0));
    DomText := TDomText(ParameterElement.FirstChild);
    Node.ModbusAdr := StrToInt(DomText.Data) + 1;

    DomNodeList := DomElement.GetElementsByTagName(VALUE_TAG_NAME);
    ParameterElement := TDomElement(DomNodeList.Item(0));
    DomText := TDomText(ParameterElement.FirstChild);
    Values[1] := StrToInt(DomText.Data);
  end;
end;

function TformMagX2Menu.GetNode(ModbusAdr: Integer) : TNode;
var
  i: Integer;
  Node: TNode;
begin
  Result := nil;
  for i:=0 to Nodes.Count-1 do begin
    Node := TNode(Nodes.Items[i]);
    if Node.ModbusAdr = ModbusAdr then begin
      Result := Node;
      break;
    end;
  end;
end;

procedure TformMagX2Menu.btnMenuBackSaveClick(Sender: TObject);
var
    i:Integer;
    FourByte:T4Byte;
    ReadOK:Boolean;
    WaitFrm : TformWait;
    Addr:Integer;
    sExePath, sPath: string;
    iniF:TIniFile;
    LastCursor : TCursor;
    DomImplementation : TDomImplementation;
    DomDocument : TDomDocument;
    DomToXMLParser: TDomToXMLParser;
    Stream: TFileStream;
    DomElement: TDomElement;
    DomText: TDomText;
    UserSettingsDomElement: TDomElement;
    IntValues4: TIntValues4;
begin
  inherited;
  Self.Enabled :=false;

  try
    // Password (user)
    Addr:=-1;
    for i:=0 To Nodes.Count-1 do
      if TNode(Nodes.Items[I]).ValueName='PASSWORD_USER' then
      begin
          Addr :=TNode(Nodes.Items[I]).ModbusAdr;
          break;
      end;

    if Addr=-1 then
    	raise Exception.Create(GetTranslationText('INTERNAL_ERROR', STR_INTERNAL_ERROR));

    repeat	//cti zda je zadano heslo;
        ReadOK := MyCommunication.ReadMenuValue( Addr-1 , FourByte)
    until(ReadOK)or(MessageDlg(GetTranslationText('READ_ERROR', STR_READ_ERROR),mtError, [mbRetry, mbAbort], 0) = mrAbort);
    if ReadOK and not(Integer(FourByte)=1) then
    begin
      formMagX2Password:=TformMagX2Password.CreateFrm(Self,MyCommunication, Addr, GetTranslationText('USER_PSW', STR_USER_PSW), LocalLanguage);
      try
      	case formMagX2Password.ShowModal of
        	mrCancel:exit;
      	end;
      finally
      	FreeAndNil(formMagX2Password);
      end;
    end;

    // External Measurements Password (user)
    Addr:=-1;
    for i:=0 To Nodes.Count-1 do
      if TNode(Nodes.Items[I]).ValueName='PASSWORD_EXT_MEAS' then
      begin
          Addr :=TNode(Nodes.Items[I]).ModbusAdr;
          break;
      end;

    if Addr<>-1 then begin
      repeat	//cti zda je zadano heslo;
        ReadOK := MyCommunication.ReadMenuValue( Addr-1 , FourByte)
      until(ReadOK)or(MessageDlg(GetTranslationText('READ_ERROR', STR_READ_ERROR),mtError, [mbRetry, mbAbort], 0) = mrAbort);
      if ReadOK and not(Integer(FourByte)=1) then
      begin
        formMagX2Password:=TformMagX2Password.CreateFrm(Self,MyCommunication, Addr, GetTranslationText('EXT_MEAS_PSW', STR_EXT_MEAS_PSW), LocalLanguage);
        try
      	  case formMagX2Password.ShowModal of
        	mrCancel:exit;
      	  end;
        finally
      	  FreeAndNil(formMagX2Password);
        end;
      end;
    end;

    sExePath := GetUserAppDataProductPath();
    iniF := TIniFile.Create(sExePath+'MagX2.ini');
    try
      sPath := iniF.ReadString('BACKUP', 'SavePath', sExePath);
    finally
      iniF.Free;
    end;

    SaveDialog2.InitialDir:=sPath;
    SaveDialog2.FileName:='MagX2Backup'+lblSerNo.Caption+'.dat';
    if (SaveDialog2.Execute) then
    begin
      WaitFrm:=TformWait.Create(Self);
      LastCursor := Screen.Cursor;
      DomImplementation := TDomImplementation.Create(nil);
      DomDocument := TDomDocument.Create(DomImplementation);

      try
        Screen.Cursor:=crHourGlass;
        WaitFrm.gWait.Progress:=0;
        WaitFrm.Show;
        SetCenter(WaitFrm);
        WaitFrm.gWait.MaxValue:=Nodes.Count-1;
        Application.ProcessMessages;

        DomElement := TDomElement.Create(DomDocument, ROOT_TAG_NAME);
        DomDocument.AppendChild(DomElement);

        DomElement := TDomElement.Create(DomDocument, SW_TAG_NAME);
        DomText := TDomText.Create(DomDocument);
        DomText.NodeValue := lblSW.Caption;
        DomElement.AppendChild(DomText);
        DomDocument.DocumentElement.AppendChild(DomElement);

        DomElement := TDomElement.Create(DomDocument, FW_TAG_NAME);
        DomText := TDomText.Create(DomDocument);
        DomText.NodeValue := lblFW.Caption;
        DomElement.AppendChild(DomText);
        DomDocument.DocumentElement.AppendChild(DomElement);

        DomElement := TDomElement.Create(DomDocument, SER_NO_TAG_NAME);
        DomText := TDomText.Create(DomDocument);
        DomText.NodeValue := lblSerNo.Caption;
        DomElement.AppendChild(DomText);
        DomDocument.DocumentElement.AppendChild(DomElement);

        UserSettingsDomElement := TDomElement.Create(DomDocument, USER_SETTINGS_TAG_NAME);
        DomDocument.DocumentElement.AppendChild(UserSettingsDomElement);

        for i:=0 To Nodes.Count-1 do
        begin
          WaitFrm.gWait.Progress:=i;
          Application.ProcessMessages;
          if (TNode(Nodes.Items[I]).ModbusAdr>2000-1)and(TNode(Nodes.Items[I]).ModbusAdr<3000-1) then
          case (TNode(Nodes.Items[I]).Typ) of
            2,4:          //Edit, Isel
            begin
              {zalohuje se pouze USER Setting bez polozek Password Setup,Modbus Slave Address,Modbus BaudRate,Modbus Parity,Modbus Interframe spacing
                Modbus Response delay,Time - hour,Time - min,Date - day,Date - month,Date - year}
                repeat
                  ReadOK:=MyCommunication.ReadMenuValue(TNode(Nodes.Items[I]).ModbusAdr-1 ,FourByte);
                until(ReadOK)or(MessageDlg(GetTranslationText('READ_ERROR', STR_READ_ERROR),mtError, [mbRetry, mbAbort], 0) = mrAbort);
                if ReadOK then begin
                  IntValues4[1] := Integer(FourByte);
                  UserSettingsDomElement.AppendChild(CreateXmlElement(DomDocument, TNode(Nodes.Items[I]), SETTING_TAG_NAME, IntValues4))
                end else
                  Raise Exception.Create(GetTranslationText('READ_ERROR', STR_READ_ERROR));  //ukonci cteni
            end;
            3:            //4Win
            begin
              repeat
                ReadOK:=MyCommunication.ReadMenuValue(TNode(Nodes.Items[I]).ModbusAdr-1,FourByte);
              until(ReadOK)or(MessageDlg(GetTranslationText('READ_ERROR', STR_READ_ERROR),mtError, [mbRetry, mbAbort], 0) = mrAbort);
              if ReadOK then
                IntValues4[1] := Integer(FourByte)
              else
                Raise Exception.Create(GetTranslationText('READ_ERROR', STR_READ_ERROR));  //ukonci cteni

              repeat
                ReadOK:=MyCommunication.ReadMenuValue(TNode(Nodes.Items[I]).ModbusAdr2-1,FourByte);
              until(ReadOK)or(MessageDlg(GetTranslationText('READ_ERROR', STR_READ_ERROR),mtError, [mbRetry, mbAbort], 0) = mrAbort);
              if ReadOK then
                IntValues4[2] := Integer(FourByte)
              else
                Raise Exception.Create(GetTranslationText('READ_ERROR', STR_READ_ERROR));  //ukonci cteni

              repeat
                ReadOK:=MyCommunication.ReadMenuValue(TNode(Nodes.Items[I]).ModbusAdr3-1,FourByte);
              until(ReadOK)or(MessageDlg(GetTranslationText('READ_ERROR', STR_READ_ERROR),mtError, [mbRetry, mbAbort], 0) = mrAbort);
              if ReadOK then
                IntValues4[3] := Integer(FourByte)
              else
                Raise Exception.Create(GetTranslationText('READ_ERROR', STR_READ_ERROR));  //ukonci cteni

              repeat
                ReadOK:=MyCommunication.ReadMenuValue(TNode(Nodes.Items[I]).ModbusAdr4-1,FourByte);
              until(ReadOK)or(MessageDlg(GetTranslationText('READ_ERROR', STR_READ_ERROR),mtError, [mbRetry, mbAbort], 0) = mrAbort);
              if ReadOK then
                IntValues4[4] := Integer(FourByte)
              else
                Raise Exception.Create(GetTranslationText('READ_ERROR', STR_READ_ERROR));  //ukonci cteni

              UserSettingsDomElement.AppendChild(CreateXmlElement(DomDocument, TNode(Nodes.Items[I]), SETTING_TAG_NAME, IntValues4));
            end;
          end;
        end;

        DomToXMLParser := TDomToXMLParser.Create(nil);
        try
          DomToXMLParser.DomImpl := DomDocument.DomImplementation;
          Stream := TFileStream.Create(SaveDialog2.FileName, fmCreate);
          try
            DomToXMLParser.WriteToStream(DomDocument, 'UTF-8', Stream);
          finally
            Stream.Free;
          end;
        finally
          DomToXMLParser.Free;
        end;

    finally
      FreeAndNil(WaitFrm);
      Screen.Cursor:=LastCursor;
      DomImplementation.Clear;
    end;

    iniF := TIniFile.Create(sExePath+'MagX2.ini');
    try
      iniF.WriteString('BACKUP', 'SavePath', ExtractFilePath(SaveDialog2.FileName) );
    finally
      iniF.Free;
    end;
  end;
  finally
    Self.Enabled :=true;
  end
end;

procedure TformMagX2Menu.btnMenuBackLoadClick(Sender: TObject);
var
    i:Integer;
    FourByte:T4Byte;
    WriteOK:Boolean;
    WaitFrm : TformWait;
    Addr:Integer;
    iniF : TINIFile;
    sExePath, sPath: string;
    LastCursor : TCursor;
    DomImplementation : TDomImplementation;
    DomDocument : TDomDocument;
    XMLToDomParser: TXMLToDomParser;
    DomNodeList: TDomNodeList;
    UserSettingsDomElement: TDomElement;
    DomElement: TDomElement;
    Node, FoundNode: TNode;
    IntValues4: TIntValues4;
begin
  inherited;

  Self.Enabled :=false;
  try
  sExePath := GetUserAppDataProductPath();
  iniF := TIniFile.Create(sExePath+'MagX2.ini');
  try
  	sPath := iniF.ReadString('BACKUP', 'OpenPath', sExePath);
  finally
  	iniF.Free;
  end;
  OpenDialog2.InitialDir:=sPath;

  //cti heslo
  Addr:=-1;
  for i:=0 To Nodes.Count-1 do
    if TNode(Nodes.Items[I]).ValueName='PASSWORD_USER' then
    begin
        Addr :=TNode(Nodes.Items[I]).ModbusAdr;
        break;
    end;

  if Addr=-1 then
      raise Exception.Create(GetTranslationText('INTERNAL_ERROR', STR_INTERNAL_ERROR));

  repeat	//cti zda je zadano heslo;
      WriteOK := MyCommunication.ReadMenuValue( Addr-1 , FourByte)
  until(WriteOK)or(MessageDlg(GetTranslationText('READ_ERROR', STR_READ_ERROR),mtError, [mbRetry, mbAbort], 0) = mrAbort);
  if WriteOK and not(Integer(FourByte)=1) then
  begin
    formMagX2Password:=TformMagX2Password.CreateFrm(Self,MyCommunication, Addr, GetTranslationText('USER_PSW', STR_USER_PSW), LocalLanguage);
    try
      case formMagX2Password.ShowModal of
          mrCancel:exit;
      end;
    finally
      FreeAndNil(formMagX2Password);
    end;
  end;

  //cti heslo (external measurements)
  Addr:=-1;
  for i:=0 To Nodes.Count-1 do
    if TNode(Nodes.Items[I]).ValueName='PASSWORD_EXT_MEAS' then
    begin
        Addr :=TNode(Nodes.Items[I]).ModbusAdr;
        break;
    end;

  if Addr<>-1 then begin
    repeat	//cti zda je zadano heslo;
      WriteOK := MyCommunication.ReadMenuValue( Addr-1 , FourByte)
    until(WriteOK)or(MessageDlg(GetTranslationText('READ_ERROR', STR_READ_ERROR),mtError, [mbRetry, mbAbort], 0) = mrAbort);
    if WriteOK and not(Integer(FourByte)=1) then
    begin
      formMagX2Password:=TformMagX2Password.CreateFrm(Self,MyCommunication, Addr, GetTranslationText('EXT_MEAS_PSW', STR_EXT_MEAS_PSW), LocalLanguage);
      try
        case formMagX2Password.ShowModal of
          mrCancel:exit;
        end;
      finally
        FreeAndNil(formMagX2Password);
      end;
    end;
  end;

  OpenDialog2.FileName:='MagX2Backup'+lblSerNo.Caption+'.dat';
  if  OpenDialog2.Execute then
  begin
    WaitFrm:=TformWait.Create(Self);
    LastCursor:= Screen.Cursor;
    DomImplementation := TDomImplementation.Create(nil);
    try
      Screen.Cursor:=crHourGlass;
      WaitFrm.gWait.Progress:=0;
      WaitFrm.Show;
      SetCenter(WaitFrm);
      Application.ProcessMessages;

      if Length(ExtractFileExt(OpenDialog2.FileName))=0 then
        OpenDialog2.FileName:=ChangeFileExt(OpenDialog2.FileName,'.dat');

      XMLToDomParser := TXMLToDomParser.Create(nil);
      try
        XMLToDomParser.DomImpl := DomImplementation;
        try
          DomDocument := XMLToDomParser.ParseFile(OpenDialog2.FileName, true);

          DomNodeList := DomDocument.DocumentElement.GetElementsByTagName(USER_SETTINGS_TAG_NAME);
          UserSettingsDomElement := TDomElement(DomNodeList.Item(0));

          DomNodeList := UserSettingsDomElement.GetElementsByTagName(SETTING_TAG_NAME);
          WaitFrm.gWait.MaxValue := DomNodeList.Length-1;

          for i:=0 to DomNodeList.Length-1 do begin
            WaitFrm.gWait.Progress:=i;
            Application.ProcessMessages;
            DomElement := TDomElement(DomNodeList.Item(i));
            Node:= TNode.Create;
            try
              ParseXmlElement(DomElement, Node, IntValues4);
              if (Node.ModbusAdr>2000-1)and(Node.ModbusAdr<3000-1) then begin
                case (Node.Typ) of
                  2,4:          //Edit, Isel
                    begin
                      repeat
                        Integer(FourByte):=IntValues4[1];
                        WriteOK:=MyCommunication.WriteMenuValue(Node.ModbusAdr-1,FourByte);
                      until(WriteOK)or(MessageDlg(GetTranslationText('WRITE_ERROR', STR_WRITE_ERROR),mtError, [mbRetry, mbAbort], 0) = mrAbort);
                      if not WriteOK then
                        Raise Exception.Create(GetTranslationText('WRITE_ERROR', STR_WRITE_ERROR));  //ukonci zapisu
                    end;
                  3:            //4Win
                    begin
                      //1.polozka
                      repeat
                        Integer(FourByte):=IntValues4[1];
                        WriteOK:=MyCommunication.WriteMenuValue(Node.ModbusAdr-1,FourByte);
                      until(WriteOK)or(MessageDlg(GetTranslationText('WRITE_ERROR', STR_WRITE_ERROR),mtError, [mbRetry, mbAbort], 0) = mrAbort);
                      if not WriteOK then
                        Raise Exception.Create(GetTranslationText('WRITE_ERROR', STR_WRITE_ERROR));  //ukonci zapisu

                      //2.polozka
                      repeat
                        Integer(FourByte):=IntValues4[2];
                        WriteOK:=MyCommunication.WriteMenuValue(Node.ModbusAdr2-1,FourByte);
                      until(WriteOK)or(MessageDlg(GetTranslationText('WRITE_ERROR', STR_WRITE_ERROR),mtError, [mbRetry, mbAbort], 0) = mrAbort);
                      if not WriteOK then
                        Raise Exception.Create(GetTranslationText('WRITE_ERROR', STR_WRITE_ERROR));  //ukonci zapisu

                      //3.polozka
                      repeat
                        Integer(FourByte):=IntValues4[3];
                        WriteOK:=MyCommunication.WriteMenuValue(Node.ModbusAdr3-1,FourByte);
                      until(WriteOK)or(MessageDlg(GetTranslationText('WRITE_ERROR', STR_WRITE_ERROR),mtError, [mbRetry, mbAbort], 0) = mrAbort);
                      if not WriteOK then
                        Raise Exception.Create(GetTranslationText('WRITE_ERROR', STR_WRITE_ERROR));  //ukonci zapisu

                      //4.polozka
                      repeat
                        Integer(FourByte):=IntValues4[4];
                        WriteOK:=MyCommunication.WriteMenuValue(Node.ModbusAdr4-1,FourByte);
                      until(WriteOK)or(MessageDlg(GetTranslationText('WRITE_ERROR', STR_WRITE_ERROR),mtError, [mbRetry, mbAbort], 0) = mrAbort);
                      if not WriteOK then
                        Raise Exception.Create(GetTranslationText('WRITE_ERROR', STR_WRITE_ERROR));  //ukonci zapisu
                    end;
                end;
              end else
                raise Exception.Create(GetTranslationText('READ_ERROR', STR_READ_ERROR));
            finally
              Node.Free;
            end;
          end;
        except
          MessageDlg('This backup is not valid for this software',mtError, [mbOk], 0);
          exit;
        end;
      finally
        XMLToDomParser.Free;
      end;
    finally
      FreeAndNil(WaitFrm);
      Screen.Cursor:=LastCursor;
      DomImplementation.Clear;
    end;

    iniF := TIniFile.Create(sExePath+'MagX2.ini');
    try
      iniF.WriteString('BACKUP', 'OpenPath', ExtractFilePath(OpenDialog2.FileName) );
    finally
      iniF.Free;
    end;
  end;
  finally
    Self.Enabled := true;
  end;
end;

procedure TformMagX2Menu.IncTime(Sender: TObject);
var
    s:string;
    value:integer;
    direct:boolean;
begin
  if  (time.sec=-1) or (time.min=-1) or (time.hour=-1) then begin
    time.sec:=0;
    time.min:=0;
    time.hour:=0;
  end;
  if TSpeedButton(Sender).tag > 0 then
  begin
    direct:=true;
    value:=TSpeedButton(Sender).tag;
  end else begin
    direct:=false;
    value:=TSpeedButton(Sender).tag*(-1);
  end;


  if direct = true then begin
    case value of
    1: inc(time.sec);
    2: inc(time.min);
    3: inc(time.hour);
    end;
    if time.sec=60 then begin
      time.sec:=0;
      inc(time.min);
    end;
    if time.min=60 then begin
      time.min:=0;
      inc(time.hour);
    end;
    if time.hour=24 then begin
      time.hour:=0;
    end;
  end
  else begin
    case value of
    1: time.sec:=time.sec-1;
    2: time.min:=time.min-1;
    3: time.hour:=time.hour-1;
    end;
    if time.sec=-1 then begin
      time.sec:=59;
      time.min:=time.min-1;
    end;
    if time.min=-1 then begin
      time.min:=59;
      time.hour:=time.hour-1;
    end;
    if time.hour=-1 then begin
      time.hour:=23;
    end;
  end;
  s:=format('%0.2d : %0.2d', [time.hour,time.min]);
  lblTime.Caption :=s;
end;

procedure TformMagX2Menu.SpeedButton3Click(Sender: TObject);
begin
  inherited;
  IncTime(Sender);
end;

procedure TformMagX2Menu.btnReadTimeClick(Sender: TObject);
var Value: Cardinal;
    ReadOK: Boolean;
begin
  if Busy then exit;
  Busy := true;
  Screen.Cursor := crHourGlass;
  try
    repeat
      ReadOK := MyCommunication.ReadRTCTime(Value);
    until (ReadOK) or (MessageDlg(GetTranslationText('READ_ERROR', STR_READ_ERROR),mtError, [mbRetry, mbAbort], 0) = mrAbort);
    if ReadOK then begin
      time := IntegerToMyTime(Value);
      lblTime.Caption :=format('%0.2d : %0.2d', [time.hour,time.min]);
      lblTimeMem.caption:=lblTime.Caption;
    end else begin
      ShowMessage(GetTranslationText('RTC_NOT_AVAIL', STR_RTC_NOT_AVAIL));
    end;
  finally
    Screen.Cursor := crDefault;
    Busy := false;
  end;
end;

procedure TformMagX2Menu.btnWriteTimeClick(Sender: TObject);
var Value: Cardinal;
    WriteOK: Boolean;
begin
  if Busy then exit;
  Busy := True;
  Screen.Cursor := crHourGlass;
  try
    Value := MyTimeToInteger(time);
    repeat
      WriteOK:=MyCommunication.WriteRTCTime(Value);
    until (WriteOK) or (MessageDlg(GetTranslationText('WRITE_ERROR', STR_WRITE_ERROR),mtError, [mbRetry, mbAbort], 0) = mrAbort);
    if WriteOK then begin
      lblTimeMem.Caption := format('%0.2d : %0.2d', [time.hour,time.min]);
    end else begin
      ShowMessage(GetTranslationText('RTC_NOT_AVAIL', STR_RTC_NOT_AVAIL));
    end;
  finally
    Screen.Cursor := crDefault;
    Busy := False;
  end;
end;

procedure TformMagX2Menu.btnReadDateClick(Sender: TObject);
var date: TmyDate;
    Value: Cardinal;
    ReadOK: Boolean;
begin
  if Busy then exit;
  Busy := true;
  Screen.Cursor := crHourGlass;
  try
    repeat
      ReadOK := MyCommunication.ReadRTCDate(Value);
    until (ReadOK) or (MessageDlg(GetTranslationText('READ_ERROR', STR_READ_ERROR),mtError, [mbRetry, mbAbort], 0) = mrAbort);
    if ReadOK then begin
      date := IntegerToMyDate(Value);
      lblDateMem.Caption:=format('%0.2d . %0.2d . %0.2d', [date.day, date.month, date.year]);
      mxCalendar1.Date:=EncodeDate(date.year,date.month,date.day);
    end else begin
      ShowMessage(GetTranslationText('RTC_NOT_AVAIL', STR_RTC_NOT_AVAIL));
    end;
  finally
    Screen.Cursor := crDefault;
    Busy := false;
  end;
end;

procedure TformMagX2Menu.btnWriteDateClick(Sender: TObject);
var date:TmyDate;
    Value: Cardinal;
    WriteOK: Boolean;
begin
  if Busy then exit;
  Busy := True;
  Screen.Cursor := crHourGlass;
  try
    DecodeDate(mxCalendar1.Date, date.year, date.month, date.day);
    Value := MyDateToInteger(date);
    repeat
      WriteOK:=MyCommunication.WriteRTCDate(Value);
    until (WriteOK) or (MessageDlg(GetTranslationText('WRITE_ERROR', STR_WRITE_ERROR),mtError, [mbRetry, mbAbort], 0) = mrAbort);
    if WriteOK then begin
      lblDateMem.Caption:=format('%0.2d . %0.2d . %0.2d', [date.day, date.month, date.year]);
    end else begin
      ShowMessage(GetTranslationText('RTC_NOT_AVAIL', STR_RTC_NOT_AVAIL));
    end;
  finally
    Screen.Cursor := crDefault;
    Busy := False;
  end;
end;

procedure TformMagX2Menu.btnGoTodayClick(Sender: TObject);
begin
  mxCalendar1.Date:=now;
end;

procedure TformMagX2Menu.OnlineTimerTimer(Sender: TObject);
var OnlineData:TOnlineX2;
    OnlineExtData:TOnlineExtX2;
    OnlineServiceData:TOnlineServiceX2;
    I: Integer;
    IsReadOnlineX2, IsReadOnlineExtX2, IsReadOnlineServiceX2 : Boolean;
begin
  if not Busy then
  try
  Busy := true;
  OnlineTimer.Enabled := False;
  IsReadOnlineX2 := MyCommunication.ReadOnlineX2(OnlineData);
  IsReadOnlineExtX2 := MyCommunication.ReadOnlineExtX2(OnlineExtData);
  IsReadOnlineServiceX2 := MyCommunication.ReadOnlineServiceX2(OnlineServiceData);
  if IsReadOnlineX2 then begin
    lblOnlineFlow.Caption :=
      Format('%.3f %s', [ONLINE_FLOW_UNIT_COEFS[cmbOnlineFlowUnit.ItemIndex] * OnlineData.Flow,
                         ONLINE_FLOW_UNIT_TEXTS[cmbOnlineFlowUnit.ItemIndex]]);
    lblOnlineFlow2.Caption :=
      Format('%.3f %s', [ONLINE_FLOW_UNIT_COEFS[cmbOnlineFlowUnit2.ItemIndex] * OnlineData.Flow,
                         ONLINE_FLOW_UNIT_TEXTS[cmbOnlineFlowUnit2.ItemIndex]]);
    lblOnlineTotal.Caption :=
      Format('%.3f %s', [ONLINE_TOTAL_UNIT_COEFS[cmbOnlineTotalUnit.ItemIndex] * OnlineData.Total,
                         ONLINE_TOTAL_UNIT_TEXTS[cmbOnlineTotalUnit.ItemIndex]]);
    lblOnlineTotal2.Caption :=
      Format('%.3f %s', [ONLINE_TOTAL_UNIT_COEFS[cmbOnlineTotalUnit2.ItemIndex] * OnlineData.Total,
                         ONLINE_TOTAL_UNIT_TEXTS[cmbOnlineTotalUnit2.ItemIndex]]);
    lblOnlineTotalPlus.Caption :=
      Format('%.3f %s', [ONLINE_TOTAL_UNIT_COEFS[cmbOnlineTotalUnit.ItemIndex] * OnlineData.TotalPlus,
                         ONLINE_TOTAL_UNIT_TEXTS[cmbOnlineTotalUnit.ItemIndex]]);
    lblOnlineTotalPlus2.Caption :=
      Format('%.3f %s', [ONLINE_TOTAL_UNIT_COEFS[cmbOnlineTotalUnit2.ItemIndex] * OnlineData.TotalPlus,
                         ONLINE_TOTAL_UNIT_TEXTS[cmbOnlineTotalUnit2.ItemIndex]]);
    lblOnlineTotalMinus.Caption :=
      Format('%.3f %s', [ONLINE_TOTAL_UNIT_COEFS[cmbOnlineTotalUnit.ItemIndex] * OnlineData.TotalMinus,
                         ONLINE_TOTAL_UNIT_TEXTS[cmbOnlineTotalUnit.ItemIndex]]);
    lblOnlineTotalMinus2.Caption :=
      Format('%.3f %s', [ONLINE_TOTAL_UNIT_COEFS[cmbOnlineTotalUnit2.ItemIndex] * OnlineData.TotalMinus,
                         ONLINE_TOTAL_UNIT_TEXTS[cmbOnlineTotalUnit2.ItemIndex]]);
    lblOnlineAuxPlus.Caption :=
      Format('%.3f %s', [ONLINE_TOTAL_UNIT_COEFS[cmbOnlineTotalUnit.ItemIndex] * OnlineData.AuxPlus,
                         ONLINE_TOTAL_UNIT_TEXTS[cmbOnlineTotalUnit.ItemIndex]]);
    lblOnlineAuxPlus2.Caption :=
      Format('%.3f %s', [ONLINE_TOTAL_UNIT_COEFS[cmbOnlineTotalUnit2.ItemIndex] * OnlineData.AuxPlus,
                         ONLINE_TOTAL_UNIT_TEXTS[cmbOnlineTotalUnit2.ItemIndex]]);
    lblOnlineTemp.Caption := Format('%.1f �C', [OnlineData.Temp]);
    lblOnlineTemp2.Caption := lblOnlineTemp.Caption;
    lblOnlineErrorCode.Caption := CardinalToBinaryStr32(OnlineData.ErrorCode);
    lblOnlineErrorCode2.Caption := lblOnlineErrorCode.Caption;
    if IsReadOnlineExtX2 then begin
      lblOnlineXTemp.Caption := Format('%.1f �C', [OnlineExtData.XTemp]);
      if firmwareNO >= 2150 then begin
        lblXPress.Caption := 'XInput:';
        lblOnlineXPress.Caption := Format('%d', [round(OnlineExtData.XPress * 1000)]);
      end else begin
        lblOnlineXPress.Caption := Format('%.3f bar', [OnlineExtData.XPress]);
      end;
    end;
    if IsReadOnlineServiceX2 then begin
      lblOnlineA1.Caption := IntToStr(OnlineServiceData.A1);
      lblOnlineA2.Caption := IntToStr(OnlineServiceData.A2);
      lblOnlineA3.Caption := IntToStr(OnlineServiceData.A3);
      lblOnlineA.Caption := IntToStr(OnlineServiceData.A);
    end;
    for I := 1 to ERROR_CODE_BYTES_COUNT do begin
      if (((OnlineData.ErrorCode) and (1 shl (I - 1))) = (1 shl (I - 1))) then begin
        ErrorShapeArray[I].Brush.Color := clRed;
      end else begin
        ErrorShapeArray[I].Brush.Color := clWhite;
      end;
    end;
    // Chart
    OnlineChart.SeriesList[0].Delete(0);
    OnlineChart2.SeriesList[0].Delete(0);    
    Inc(MaxChartCnt);
    Inc(MinChartCnt);
    if (OnlineData.Flow * CHART_TOLERANCE) > MaxChartValue then begin
      MaxChartValue := OnlineData.Flow * CHART_TOLERANCE;
      MaxChartCnt := 0;
    end;
    if (OnlineData.Flow * CHART_TOLERANCE) < MinChartValue then begin
      MinChartValue := OnlineData.Flow * CHART_TOLERANCE;
      MinChartCnt := 0;
    end;
    OnlineChart.SeriesList[0].AddXY(OnlineChart.SeriesList[0].XValues.Last + 1, OnlineData.Flow);
    OnlineChart2.SeriesList[0].AddXY(OnlineChart2.SeriesList[0].XValues.Last + 1, OnlineData.Flow);    
    if (MinChartCnt > GRAPH_ITEMS_COUNT) or (MaxChartCnt > GRAPH_ITEMS_COUNT) then begin
      MaxChartCnt := 0;
      MinChartCnt := 0;
      MaxChartValue := 0;
      MinChartValue := 0;
      for I := OnlineChart.SeriesList[0].Count - 1 downto 0 do begin
        if (OnlineChart.SeriesList[0].YValue[I] * CHART_TOLERANCE) > MaxChartValue then begin
          MaxChartValue := OnlineChart.SeriesList[0].YValue[I] * CHART_TOLERANCE;
          MaxChartCnt := GRAPH_ITEMS_COUNT - I;
        end;
        if (OnlineChart.SeriesList[0].YValue[I] * CHART_TOLERANCE) < MinChartValue then begin
          MinChartValue := OnlineChart.SeriesList[0].YValue[I] * CHART_TOLERANCE;
          MinChartCnt := GRAPH_ITEMS_COUNT - I;
        end;
      end;
    end;
    OnlineChart.LeftAxis.Minimum := MinChartValue;
    OnlineChart.LeftAxis.Maximum := MaxChartValue;
    OnlineChart.RightAxis.Minimum := MinChartValue;
    OnlineChart.RightAxis.Maximum := MaxChartValue;
    OnlineChart2.LeftAxis.Minimum := MinChartValue;
    OnlineChart2.LeftAxis.Maximum := MaxChartValue;
    OnlineChart2.RightAxis.Minimum := MinChartValue;
    OnlineChart2.RightAxis.Maximum := MaxChartValue;
    // ---
    OnlineErr := 0;
  end else begin
    Inc(OnlineErr);
  end;
  if OnlineErr > MAX_ERROR then begin
    MessageDlg('Communication error', mtError, [mbRetry], 0);
    OnlineErr:=0;
  end;
  OnlineTimer.Enabled := (pcMenu.ActivePage = tsOnline) or (pcMenu.ActivePage = tsCalibration);
  finally
    Busy := false;
  end
end;

procedure TformMagX2Menu.btnWriteCalDataClick(Sender: TObject);
var FourByte : T4Byte;
    R: Double;
    Idx: Integer;
begin
  if not Busy then begin
  Busy := true;
  CalibrationStatusLabel.Caption:='';
  MeasurmentStatusLabel.Caption:='';
  Idx := TButton(Sender).Tag;
  R := 0;
  case Idx of
    1 : R := Invert_Calibration_Flow_Conversion(StrToFloat(CalSpinEdit1.Text),TFlowUnit(cbFlowUnit.ItemIndex));
    2 : R := Invert_Calibration_Flow_Conversion(StrToFloat(CalSpinEdit2.Text),TFlowUnit(cbFlowUnit.ItemIndex));
    3 : R := Invert_Calibration_Flow_Conversion(StrToFloat(CalSpinEdit3.Text),TFlowUnit(cbFlowUnit.ItemIndex));
  end;
  if MyCommunication.WriteMenuValue(GetAddress(ProtokolType, CALIB_VALUE_NAMES[Idx], Values), T4Byte(Cardinal(Round(R*1000)))) then begin
    if MyCommunication.ReadMenuValue(GetAddress(ProtokolType, MEASURE_VALUE_NAMES[Idx], Values), FourByte) then begin
      case Idx of
        1 : MeasSpinEdit1.Text := IntToStr(Integer(FourByte));
        2 : MeasSpinEdit2.Text := IntToStr(Integer(FourByte));
        3 : MeasSpinEdit3.Text := IntToStr(Integer(FourByte));
      end;
    end;
    CalibrationStatusLabel.Caption := Format(GetTranslationText('CALIB_POINT_WRITE_SUCC', STR_CALIB_POINT_WRITE_SUCC), [Idx]);
    CalibrationStatusLabel.Font.Color:=clGreen;
  end else begin
    CalibrationStatusLabel.Caption := Format(GetTranslationText('CALIB_POINT_WRITE_FAIL', STR_CALIB_POINT_WRITE_FAIL), [Idx]);
    CalibrationStatusLabel.Font.Color:=clRed;
  end;
  Busy := false;
  end;
end;

procedure TformMagX2Menu.btnWriteMeasDataClick(Sender: TObject);
var CalibData: Integer;
    MeasurData: Cardinal;
    FourByte : T4Byte;
    Idx: Integer;
begin
  if not Busy then begin
  Busy := true;
  CalibrationStatusLabel.Caption:='';
  MeasurmentStatusLabel.Caption:='';
  Idx := TButton(Sender).Tag;
  CalibData := 0;
  MeasurData := 0;
  case Idx of
    1 : begin
          CalibData := Round(1000 * Invert_Calibration_Flow_Conversion(StrToFloat(CalSpinEdit1.Text),TFlowUnit(cbFlowUnit.ItemIndex)));
          MeasurData := Round(MeasSpinEdit1.Value * ((ErrorSpinEdit1.Value + 100.0) / 100.0));
        end;
    2 : begin
          CalibData := Round(1000 * Invert_Calibration_Flow_Conversion(StrToFloat(CalSpinEdit2.Text),TFlowUnit(cbFlowUnit.ItemIndex)));
          MeasurData := Round(MeasSpinEdit2.Value * ((ErrorSpinEdit2.Value + 100.0) / 100.0));
        end;
    3 : begin
          CalibData := Round(1000 * Invert_Calibration_Flow_Conversion(StrToFloat(CalSpinEdit3.Text),TFlowUnit(cbFlowUnit.ItemIndex)));
          MeasurData := Round(MeasSpinEdit3.Value * ((ErrorSpinEdit3.Value + 100.0) / 100.0));
        end;
  end;
  if MyCommunication.WriteMenuValue(GetAddress(ProtokolType, CALIB_VALUE_NAMES[Idx], Values), T4Byte(CalibData)) and
     MyCommunication.WriteMenuValue(GetAddress(ProtokolType, MEASURE_VALUE_NAMES[Idx], Values), T4Byte(MeasurData)) and
     MyCommunication.ReadMenuValue(GetAddress(ProtokolType, MEASURE_VALUE_NAMES[Idx], Values), FourByte) then
  begin
    case Idx of
      1 : begin
            MeasSpinEdit1.Text := IntToStr(Integer(FourByte));
            ErrorSpinEdit1.Value := 0;
          end;
      2 : begin
            MeasSpinEdit2.Text := IntToStr(Integer(FourByte));
            ErrorSpinEdit2.Value := 0;
          end;
      3 : begin
            MeasSpinEdit3.Text := IntToStr(Integer(FourByte));
            ErrorSpinEdit3.Value := 0;
          end;
    end;
    MeasurmentStatusLabel.Caption := Format(GetTranslationText('MEAS_POINT_WRITE_SUCC', STR_MEAS_POINT_WRITE_SUCC), [Idx]);
    MeasurmentStatusLabel.Font.Color:=clGreen;
  end else begin
    MeasurmentStatusLabel.Caption := Format(GetTranslationText('MEAS_POINT_WRITE_FAIL', STR_MEAS_POINT_WRITE_FAIL), [Idx]);
    MeasurmentStatusLabel.Font.Color:=clRed;
  end;
  Busy := false;
  end;
end;

procedure TformMagX2Menu.btnAutomaticZeroConstClick(Sender: TObject);
var FourByte: T4Byte;
begin
  if not Busy then begin
  Busy := true;
  Integer(FourByte) := 1;
  if MyCommunication.WriteMenuValue(GetAddress(ProtokolType, ZERO_FLOW_NAME, Values), FourByte) then begin
    CalibrationStatusLabel.Caption := GetTranslationText('PLS_WAIT', STR_PLS_WAIT);
    Application.ProcessMessages;
    Sleep(1000);
    if MyCommunication.ReadMenuValue(GetAddress(ProtokolType, ZERO_FLOW_CONST_NAME, Values), FourByte) then begin
      ZeroConstSpinEdit.Value := Integer(FourByte);
      CalibrationStatusLabel.Caption := GetTranslationText('ZERO_FLOW_WRITE_SUCC', STR_ZERO_FLOW_WRITE_SUCC);
      CalibrationStatusLabel.Font.Color:=clGreen;
    end else begin
      CalibrationStatusLabel.Caption := GetTranslationText('READ_FAIL', STR_READ_FAIL);
      CalibrationStatusLabel.Font.Color:=clRed;
    end;
  end else begin
    CalibrationStatusLabel.Caption := GetTranslationText('ZERO_FLOW_WRITE_FAIL', STR_ZERO_FLOW_WRITE_FAIL);
    CalibrationStatusLabel.Font.Color:=clRed;
  end;
  Busy := false;
  end;
end;

procedure TformMagX2Menu.btnManualZeroConstClick(Sender: TObject);
var FourByte: T4Byte;
begin
  if not Busy then begin
  Busy := true;
  Integer(FourByte) := ZeroConstSpinEdit.Value;
  if MyCommunication.WriteMenuValue(GetAddress(ProtokolType, ZERO_FLOW_CONST_NAME, Values), FourByte) then begin
    CalibrationStatusLabel.Caption := GetTranslationText('ZERO_FLOW_WRITE_SUCC', STR_ZERO_FLOW_WRITE_SUCC);
    CalibrationStatusLabel.Font.Color:=clGreen;
  end else begin
    CalibrationStatusLabel.Caption := GetTranslationText('ZERO_FLOW_WRITE_FAIL', STR_ZERO_FLOW_WRITE_FAIL);
    CalibrationStatusLabel.Font.Color:=clRed;
  end;
  Busy := false;
  end;
end;

procedure TformMagX2Menu.btnMeasurementCalculateClick(Sender: TObject);
var a,b: double;
begin
  inherited;
    a  := 0;
    if (CalSpinEdit2.Value-CalSpinEdit1.Value) <> 0 then a  := (MeasSpinEdit2.Value-MeasSpinEdit1.Value)/(CalSpinEdit2.Value-CalSpinEdit1.Value); // vypocet hodnoty a pro rovnici primky
    b  := MeasSpinEdit2.Value-(CalSpinEdit2.Value * a);
    MeasSpinEdit3.Value:= a*CalSpinEdit3.Value + b;
end;

procedure TformMagX2Menu.btnReadAllClick(Sender: TObject);
var FourByte: T4Byte;
    WaitFrm: TformWait;
    I: Integer;
    SensorUnitNo: Cardinal;
begin
  if not Busy then
  try
    Busy:=true;
    WaitFrm:=TformWait.Create(Self);
    Screen.Cursor:=crHourGlass;
    WaitFrm.gWait.Progress:=0;
    WaitFrm.Show;
    SetCenter(WaitFrm);
    WaitFrm.caption:=GetTranslationText('PLS_WAIT', STR_PLS_WAIT);
    Application.ProcessMessages;

    CalibrationStatusLabel.Caption:='';
    MeasurmentStatusLabel.Caption:='';

    try
      // Calib
      for I := 1 to 3 do begin
        if MyCommunication.ReadMenuValue(GetAddress(ProtokolType, CALIB_VALUE_NAMES[I], Values), FourByte) then begin
          case I of
            1 : CalSpinEdit1.Text := FormatFloat('0.000', Calibration_Flow_Conversion(Integer(FourByte) / 1000.0, TFlowUnit(cbFlowUnit.ItemIndex)));
            2 : CalSpinEdit2.Text := FormatFloat('0.000', Calibration_Flow_Conversion(Integer(FourByte) / 1000.0, TFlowUnit(cbFlowUnit.ItemIndex)));
            3 : CalSpinEdit3.Text := FormatFloat('0.000', Calibration_Flow_Conversion(Integer(FourByte) / 1000.0, TFlowUnit(cbFlowUnit.ItemIndex)));
          end;
          WaitFrm.gWait.Progress := I * 20;
          Application.ProcessMessages;
        end else begin
          raise Exception.Create(GetTranslationText('READ_ERROR', STR_READ_ERROR));  //ukonci cteni
        end;
      end;
      // Zero Const
      if MyCommunication.ReadMenuValue(GetAddress(ProtokolType, ZERO_FLOW_CONST_NAME, Values), FourByte) then begin
        ZeroConstSpinEdit.Value := Integer(FourByte);
        WaitFrm.gWait.Progress := 70;
        Application.ProcessMessages;
      end;
      // Measure
      for I := 1 to 3 do begin
        if MyCommunication.ReadMenuValue(GetAddress(ProtokolType, MEASURE_VALUE_NAMES[I], Values), FourByte) then begin
          case I of
            1 : MeasSpinEdit1.Text := IntToStr(Integer(FourByte));
            2 : MeasSpinEdit2.Text := IntToStr(Integer(FourByte));
            3 : MeasSpinEdit3.Text := IntToStr(Integer(FourByte));
          end;
          WaitFrm.gWait.Progress := 70 + I * 10;
          Application.ProcessMessages;
        end else begin
          raise Exception.Create(GetTranslationText('READ_ERROR', STR_READ_ERROR));  //ukonci cteni
        end;
      end;
      // Sensor Unit No
      if seSensorUnitNo.Visible then begin
        if MyCommunication.ReadSensorUnitNo(SensorUnitNo) then begin
          seSensorUnitNo.Value := SensorUnitNo;
        end else begin
          raise Exception.Create(GetTranslationText('READ_ERROR', STR_READ_ERROR));  //ukonci cteni
        end;
      end;
      CalibrationStatusLabel.Caption:=GetTranslationText('READ_SUCCESS', STR_READ_SUCC);
      CalibrationStatusLabel.Font.Color:=clGreen;
    except
      CalibrationStatusLabel.Caption:=GetTranslationText('READ_FAILED', STR_READ_FAIL);
      CalibrationStatusLabel.Font.Color:=clRed;
    end;
  finally
    FreeAndNil(WaitFrm);
    Screen.Cursor:=crDefault;
  Busy:=false;
  end;
end;

procedure TformMagX2Menu.btnWriteAllClick(Sender: TObject);
var CalibData, MeasurData, SensorUnitNo: Cardinal;
    WaitFrm : TformWait;
    I: Integer;
begin
  if not Busy then
  try
    Busy:=true;
    WaitFrm:=TformWait.Create(Self);
    Screen.Cursor:=crHourGlass;
    WaitFrm.gWait.Progress:=0;
    WaitFrm.Show;
    SetCenter(WaitFrm);
    WaitFrm.caption:=GetTranslationText('PLS_WAIT', STR_PLS_WAIT);
    Application.ProcessMessages;
    CalibrationStatusLabel.Caption:='';
    MeasurmentStatusLabel.Caption:='';
    try
      // Calib
      for I := 1 to 3 do begin
        case I of
          1 : CalibData := Round(1000 * Invert_Calibration_Flow_Conversion(CalSpinEdit1.Value, TFlowUnit(cbFlowUnit.ItemIndex)));
          2 : CalibData := Round(1000 * Invert_Calibration_Flow_Conversion(CalSpinEdit2.Value, TFlowUnit(cbFlowUnit.ItemIndex)));
          3 : CalibData := Round(1000 * Invert_Calibration_Flow_Conversion(CalSpinEdit3.Value, TFlowUnit(cbFlowUnit.ItemIndex)));
        end;
        if MyCommunication.WriteMenuValue(GetAddress(ProtokolType, CALIB_VALUE_NAMES[I], Values), T4Byte(CalibData)) then begin
          WaitFrm.gWait.Progress := I * 20;
          Application.ProcessMessages;
        end else begin
          raise Exception.Create(GetTranslationText('WRITE_ERROR', STR_WRITE_ERROR));  //ukonci zapis
        end;
      end;
      // Zero Const
      if MyCommunication.WriteMenuValue(GetAddress(ProtokolType, ZERO_FLOW_CONST_NAME, Values), T4Byte(ZeroConstSpinEdit.Value)) then begin
        WaitFrm.gWait.Progress := 70;
        Application.ProcessMessages;
      end else begin
        raise Exception.Create(GetTranslationText('WRITE_ERROR', STR_WRITE_ERROR));  //ukonci zapis
      end;
      // Measure
      for I := 1 to 3 do begin
        case I of
          1 : MeasurData := Round(MeasSpinEdit1.Value);
          2 : MeasurData := Round(MeasSpinEdit2.Value);
          3 : MeasurData := Round(MeasSpinEdit3.Value);
        end;
        if MyCommunication.WriteMenuValue(GetAddress(ProtokolType, MEASURE_VALUE_NAMES[I], Values), T4Byte(MeasurData)) then begin
          WaitFrm.gWait.Progress := 70 + I * 10;
          Application.ProcessMessages;
        end else begin
          raise Exception.Create(GetTranslationText('WRITE_ERROR', STR_WRITE_ERROR));  //ukonci zapis
        end;
      end;
      MeasurmentStatusLabel.Caption := GetTranslationText('MEAS_POINTS_WRITE_SUCC', STR_MEAS_POINTS_WRITE_SUCC);
      MeasurmentStatusLabel.Font.Color:=clGreen;
      // Sensor Unit No
      if seSensorUnitNo.Visible then begin
        SensorUnitNo := Round(seSensorUnitNo.Value);
        if not MyCommunication.WriteSensorUnitNo(SensorUnitNo) then begin
          raise Exception.Create(GetTranslationText('WRITE_ERROR', STR_WRITE_ERROR));  //ukonci zapis
        end;
      end;
    except
      MeasurmentStatusLabel.Caption := GetTranslationText('MEAS_POINTS_WRITE_FAIL', STR_MEAS_POINTS_WRITE_FAIL);
      MeasurmentStatusLabel.Font.Color:=clRed;
    end;
  finally
    FreeAndNil(WaitFrm);
    Screen.Cursor:=crDefault;
    Busy:=false;
  end;
end;

procedure TformMagX2Menu.btnOpenDataFileClick(Sender: TObject);
var iniF : TINIFile;
	sExePath, sPath, tmp:string;
begin
  sExePath := GetUserAppDataProductPath();
  iniF := TIniFile.Create(sExePath+'MagX2.ini');
  try
  	sPath := iniF.ReadString('CALIBRATION', 'OpenPath', sExePath);
  finally
  	iniF.Free;
  end;

  OpenDialog1.InitialDir:=sPath;
  if OpenDialog1.Execute then
  begin
    iniF:=TINIFile.Create(OpenDialog1.FileName);
    try
      tmp:=iniF.ReadString('CALIBRATION','CalibrationPoint1','0');
      CalSpinEdit1.Text:=FormatFloat('0.000', Calibration_Flow_Conversion(StrToFloat(tmp),TFlowUnit(cbFlowUnit.ItemIndex)));

      tmp:=iniF.ReadString('CALIBRATION','CalibrationPoint2','0');
      CalSpinEdit2.Text:=FormatFloat('0.000', Calibration_Flow_Conversion(StrToFloat(tmp),TFlowUnit(cbFlowUnit.ItemIndex)));

      tmp:=iniF.ReadString('CALIBRATION','CalibrationPoint3','0');
      CalSpinEdit3.Text:=FormatFloat('0.000', Calibration_Flow_Conversion(StrToFloat(tmp),TFlowUnit(cbFlowUnit.ItemIndex)));

      MeasSpinEdit1.Text:=iniF.ReadString('CALIBRATION','MeasuredPoint1','0');
      MeasSpinEdit2.Text:=iniF.ReadString('CALIBRATION','MeasuredPoint2','0');
      MeasSpinEdit3.Text:=iniF.ReadString('CALIBRATION','MeasuredPoint3','0');

      ZeroConstSpinEdit.Text:=iniF.ReadString('CALIBRATION','ZeroFlowConst','0');
    finally
     iniF.Free;
    end;

    iniF := TIniFile.Create(sExePath+'MagX2.ini');
    try
      iniF.WriteString('CALIBRATION', 'OpenPath', ExtractFilePath(OpenDialog1.FileName) );
    finally
      iniF.Free;
    end;
  end;
end;

procedure TformMagX2Menu.btnSaveDataFileClick(Sender: TObject);
var iniF : TINIFile;
    R:double;
    sExePath, sPath: string;
begin
  sExePath := GetUserAppDataProductPath();
  iniF := TIniFile.Create(sExePath+'MagX2.ini');
  try
  	sPath := iniF.ReadString('CALIBRATION', 'SavePath', sExePath);
  finally
  	iniF.Free;
  end;

  SaveDialog1.FileName:='Calibration'+lblSerNo.Caption+'.ini';
  SaveDialog1.InitialDir:=sPath;
  if SaveDialog1.Execute then
  begin
    if Length(ExtractFileExt(SaveDialog1.FileName))=0 then
      ChangeFileExt(SaveDialog1.FileName,'ini');
    iniF:=TINIFile.Create(SaveDialog1.FileName);
    try
      R := Invert_Calibration_Flow_Conversion(StrToFloat(CalSpinEdit1.Text),TFlowUnit(cbFlowUnit.ItemIndex));
      iniF.WriteString('CALIBRATION','CalibrationPoint1',FloatToStr(R));

      R := Invert_Calibration_Flow_Conversion(StrToFloat(CalSpinEdit2.Text),TFlowUnit(cbFlowUnit.ItemIndex));
      iniF.WriteString('CALIBRATION','CalibrationPoint2',FloatToStr(R));

      R := Invert_Calibration_Flow_Conversion(StrToFloat(CalSpinEdit3.Text),TFlowUnit(cbFlowUnit.ItemIndex));
      iniF.WriteString('CALIBRATION','CalibrationPoint3',FloatToStr(R));

      iniF.WriteString('CALIBRATION','MeasuredPoint1',MeasSpinEdit1.Text);
      iniF.WriteString('CALIBRATION','MeasuredPoint2',MeasSpinEdit2.Text);
      iniF.WriteString('CALIBRATION','MeasuredPoint3',MeasSpinEdit3.Text);

      iniF.WriteString('CALIBRATION','ZeroFlowConst',ZeroConstSpinEdit.Text);
    finally
     iniF.Free;
    end;

    iniF := TIniFile.Create(sExePath+'MagX2.ini');
    try
      iniF.WriteString('CALIBRATION', 'SavePath', ExtractFilePath(SaveDialog1.FileName) );
    finally
      iniF.Free;
    end;
  end;
end;

procedure TformMagX2Menu.pcMenuChanging(Sender: TObject;
  var AllowChange: Boolean);
begin
//pokud je aktivni online tak zastav
  if (pcMenu.ActivePage=tsOnline) or (pcMenu.ActivePage=tsCalibration) then
  begin
    if(MyCommunication is TMyCommPort) then
    begin
      OnlineTimer.enabled:=false;
      TMyCommPort(MyCommunication).WriteOnline(0);
      OnlinePocitadlo:=0;
    end
    else if MyCommunication is TMyModbus then
    begin
      OnlineTimer.enabled:=false;
    end;
  end;
  TabIndexPred:=TPageControl(sender).ActivePageIndex;
end;

procedure TformMagX2Menu.pcMenuChange(Sender: TObject);
var PassForm:TformMagX2Pass;
    i:Integer;
begin
  //Online
  if (pcMenu.ActivePage=tsOnline)then
  begin
    OnlinePocitadlo:=1;
    OnlineTimer.enabled:=true;

    MinChartCnt := GRAPH_ITEMS_COUNT;
    MaxChartCnt := GRAPH_ITEMS_COUNT;

    OnlineChart.SeriesList[0].Clear;
    OnlineChart2.SeriesList[0].Clear;
    for I := 0 to GRAPH_ITEMS_COUNT - 1 do begin
      OnlineChart.SeriesList[0].AddXY(I, 0);
      OnlineChart2.SeriesList[0].AddXY(I, 0);
    end;
    if(MyCommunication is TMyCommPort)then
    begin
      TMyCommPort(MyCommunication).FlushBuffers(True,False);
      TMyCommPort(MyCommunication).WriteOnline(1);
    end;
  end
  //Kalibrace
  else if pcMenu.ActivePage=tsCalibration then
  begin
    CalSpinEdit1.SetFocus;
    if firmwareNO<107 then      //podle firmware zobraz kalibraci se zadavanim prutoku
    begin
      Panel6.Visible:=false;
      btnWriteAll.Visible:=false;
    end
    else
    begin
      Panel6.Visible:=true;
      btnWriteAll.Visible:=true;
    end;

    if secure[3] then   //pokud bylo zadano heslo cti vsechno
    begin
      btnReadAll.Click;
      OnlinePocitadlo:=1;
      OnlineTimer.enabled:=true;
      
      MinChartCnt := GRAPH_ITEMS_COUNT;
      MaxChartCnt := GRAPH_ITEMS_COUNT;

      OnlineChart.SeriesList[0].Clear;
      OnlineChart2.SeriesList[0].Clear;
      for I := 0 to GRAPH_ITEMS_COUNT - 1 do begin
        OnlineChart.SeriesList[0].AddXY(I, 0);
        OnlineChart2.SeriesList[0].AddXY(I, 0);
      end;
      if(MyCommunication is TMyCommPort)then
      begin
        TMyCommPort(MyCommunication).FlushBuffers(True,False);
        TMyCommPort(MyCommunication).WriteOnline(1);
      end;
    end
    else
    begin               //jinak zobraz okno hesla
      PassForm:=TformMagX2Pass.CreateFrm(self,3,MyCommunication,Values,ProtokolType, LocalLanguage);
      SetCenter(PassForm);
      case PassForm.Showmodal of
        mrOk:
        begin
          Secure[3]:=true;
          pcMenuChange(Sender);
        end;
        mrCancel:
        begin
         pcMenu.ActivePageIndex:=TabIndexPred;
         pcMenuChange(Sender);
        end;
      end;
    end;
  end
  //Firmware update
  else if pcMenu.ActivePage=tsUpdateFirmware then
  begin
    if secure[3] then   //pokud bylo zadano heslo cti vsechno
    begin
      // OK - Continue
    end
    else
    begin               //jinak zobraz okno hesla
      PassForm:=TformMagX2Pass.CreateFrm(self,3,MyCommunication,Values,ProtokolType, LocalLanguage);
      SetCenter(PassForm);
      case PassForm.Showmodal of
        mrOk:
        begin
          Secure[3]:=true;
          pcMenuChange(Sender);
        end;
        mrCancel:
        begin
         pcMenu.ActivePageIndex:=TabIndexPred;
         pcMenuChange(Sender);
        end;
      end;
    end;
  end
  //Time, Date
  else if (pcMenu.ActivePage=tsDate)or(pcMenu.ActivePage=tsTime) then
  begin
    if ((firmwareNO<2139) and Secure[1]) or ((firmwareNO>=2139) and (firmwareNO<2150) and Secure[2]) or ((firmwareNO>=2150) and Secure[1]) then begin
    end else begin
      //zabrazeni dialogu hesla
      if (firmwareNO<2139) or (firmwareNO>=2150) then
        PassForm:=TformMagX2Pass.CreateFrm(self,1,MyCommunication,Values,ProtokolType, LocalLanguage)
      else
        PassForm:=TformMagX2Pass.CreateFrm(self,2,MyCommunication,Values,ProtokolType, LocalLanguage);
      SetCenter(PassForm);
      case PassForm.Showmodal of
        mrCancel:
        begin
          pcMenu.ActivePageIndex:=TabIndexPred;
          pcMenuChange(Sender);
        end;
        mrOK:
        begin
          if (firmwareNO<2139) or (firmwareNO>=2150) then
            Secure[1]:=true
          else
            Secure[2]:=true;
          pcMenuChange(Sender);
        end
      end;
    end;
  end
  //GPRS, SMS
  else if (pcMenu.ActivePage=tsGPRS)or(pcMenu.ActivePage=tsGSM) then
  begin
    if Secure[1]then begin
      if (pcMenu.ActivePage=tsGPRS) then begin
        btnGPRS_Read.Click;
      end;
      if (pcMenu.ActivePage=tsGSM) then begin
        btnGSMReadNumbers.Click;
        btnGSMReadEvents.Click;
      end;
    end else begin
      //zabrazeni dialogu hesla
      PassForm:=TformMagX2Pass.CreateFrm(self,1,MyCommunication,Values,ProtokolType, LocalLanguage);
      SetCenter(PassForm);
      case PassForm.Showmodal of
        mrCancel:
        begin
          pcMenu.ActivePageIndex:=TabIndexPred;
          pcMenuChange(Sender);
        end;
        mrOK:
        begin
          Secure[1]:=true;
          pcMenuChange(Sender);
        end
      end;
    end;
  end;
end;

procedure TformMagX2Menu.TranslateForm;
begin
  // Page control
  tsEdit.Caption := GetTranslationText('MENU', tsEdit.Caption);
  tsTime.Caption := GetTranslationText('TIME', tsTime.Caption);
  tsDate.Caption := GetTranslationText('DATE', tsDate.Caption);
  tsOnline.Caption := GetTranslationText('REAL_TIME', tsOnline.Caption);
  tsCalibration.Caption := GetTranslationText('CALIB', tsCalibration.Caption);
  tsUpdateFirmware.Caption := GetTranslationText('FW_UPDATE', tsUpdateFirmware.Caption);
  btnMenuBackLoad.Caption := GetTranslationText('LOAD_BCK_FILE', btnMenuBackLoad.Caption);
  btnMenuBackSave.Caption := GetTranslationText('SAVE_BCK_FILE', btnMenuBackSave.Caption);
  // Date/Time
  btnReadTime.Caption := GetTranslationText('READ_TIME', btnReadTime.Caption);
  btnWriteTime.Caption := GetTranslationText('WRITE_TIME', btnWriteTime.Caption);
  btnReadDate.Caption := GetTranslationText('READ_DATE', btnReadDate.Caption);
  btnWriteDate.Caption := GetTranslationText('WRITE_DATE', btnWriteDate.Caption);
  btnGoToday.Caption := GetTranslationText('GO_TODAY', btnGoToday.Caption);
  lblNowInMemTime.Caption := GetTranslationText('NOW_IN_MEM', lblNowInMemTime.Caption);
  lblNowInMemDate.Caption := GetTranslationText('NOW_IN_MEM', lblNowInMemDate.Caption);
  lblTimeMem.Caption := GetTranslationText('UNKNOWN', lblTimeMem.Caption);
  lblDateMem.Caption := GetTranslationText('UNKNOWN', lblDateMem.Caption);
  // Online
  lblFlow.Caption := GetTranslationText('FLOW', lblFlow.Caption);
  lblTotalPlus.Caption := GetTranslationText('TOTAL_PLUS', lblTotalPlus.Caption);
  lblTotalMinus.Caption := GetTranslationText('TOTAL_MINUS', lblTotalMinus.Caption);
  lblTotal.Caption := GetTranslationText('TOTAL', lblTotal.Caption);
  lblAuxPlus.Caption := GetTranslationText('AUX_PLUS', lblAuxPlus.Caption);
  lblErrorCode.Caption := GetTranslationText('ERROR_CODE', lblErrorCode.Caption);
  OnlineChart.Title.Text.Text := GetTranslationText('ACTUAL_FLOW', OnlineChart.Title.Text.Text);
  OnlineChart.RightAxis.Title.Caption := GetTranslationText('FLOW_UNIT', OnlineChart.RightAxis.Title.Caption);
  // Calibration
  lblCalibrationData.Caption := GetTranslationText('CALIB_DATA', lblCalibrationData.Caption);
  lblUnit.Caption := GetTranslationText('UNIT', lblUnit.Caption);
  btnWriteCalData1.Caption := GetTranslationText('CALIB_WRITE_DATA1', btnWriteCalData1.Caption);
  btnWriteCalData2.Caption := GetTranslationText('CALIB_WRITE_DATA2', btnWriteCalData2.Caption);
  btnWriteCalData3.Caption := GetTranslationText('CALIB_WRITE_DATA3', btnWriteCalData3.Caption);
  btnAutomaticZeroConst.Caption := GetTranslationText('AUTOMATIC_ZERO_CONST', btnAutomaticZeroConst.Caption);
  btnManualZeroConst.Caption := GetTranslationText('MANUAL_ZERO_CONST', btnManualZeroConst.Caption);
  lblMeasurementData.Caption := GetTranslationText('MEAS_DATA', lblMeasurementData.Caption);
  lblError.Caption := GetTranslationText('ERROR_PROC', lblError.Caption);
  btnWriteMeasData1.Caption := GetTranslationText('MEAS_WRITE_DATA1', btnWriteMeasData1.Caption);
  btnWriteMeasData2.Caption := GetTranslationText('MEAS_WRITE_DATA2', btnWriteMeasData2.Caption);
  btnWriteMeasData3.Caption := GetTranslationText('MEAS_WRITE_DATA3', btnWriteMeasData3.Caption);
  btnMeasurementCalculate.Caption := GetTranslationText('CALC_MEAS_POINT', btnMeasurementCalculate.Caption);
  btnReadAll.Caption := GetTranslationText('READ_ALL', btnReadAll.Caption);
  btnWriteAll.Caption := GetTranslationText('WRITE_ALL', btnWriteAll.Caption);
  btnOpenDataFile.Caption := GetTranslationText('OPEN_DATA', btnOpenDataFile.Caption);
  btnSaveDataFile.Caption := GetTranslationText('SAVE_DATA', btnSaveDataFile.Caption);
  lblFlow2.Caption := GetTranslationText('FLOW', lblFlow2.Caption);
  lblTotalPlus2.Caption := GetTranslationText('TOTAL_PLUS', lblTotalPlus2.Caption);
  lblTotalMinus2.Caption := GetTranslationText('TOTAL_MINUS', lblTotalMinus2.Caption);
  lblTotal2.Caption := GetTranslationText('TOTAL', lblTotal2.Caption);
  lblAuxPlus2.Caption := GetTranslationText('AUX_PLUS', lblAuxPlus2.Caption);
  lblErrorCode2.Caption := GetTranslationText('ERROR_CODE', lblErrorCode2.Caption);
  OnlineChart2.Title.Text.Text := GetTranslationText('ACTUAL_FLOW', OnlineChart2.Title.Text.Text);
  OnlineChart2.RightAxis.Title.Caption := GetTranslationText('FLOW_UNIT', OnlineChart2.RightAxis.Title.Caption);
  // FW Update
  lblUpdateFirmwareFile.Caption := GetTranslationText('FW_UPDATE_FILE', lblUpdateFirmwareFile.Caption);
  lblUpdateFirmwareComment.Caption := GetTranslationText('FW_UPDATE_COMMENT', lblUpdateFirmwareComment.Caption);
  btnUpdateFirmware.Caption := GetTranslationText('FW_UPDATE_BTN', btnUpdateFirmware.Caption);
end;

procedure TformMagX2Menu.btnUpdateFWFileSelectClick(Sender: TObject);
var OpenDlg: TOpenDialog;
begin
  inherited;
  OpenDlg := TOpenDialog.Create(nil);
  try
    OpenDlg.Filter := GetTranslationText('FW_UPDATE_FILTER', STR_FW_UPDATE_FILTER);
    OpenDlg.DefaultExt := 'MGB';
    OpenDlg.FileName := edtUpdateFirmwareFile.Text;
    if OpenDlg.Execute then begin
      edtUpdateFirmwareFile.Text := OpenDlg.FileName;
    end;
  finally
    OpenDlg.Free;
  end;
end;

{
procedure TformMagX2Menu.btnUpdateFirmwareClick(Sender: TObject);
var MyXModem: TXModem;
    MyStream: TMemoryStream;
    ZipMaster: TZipMaster;
begin
  inherited;
  if MessageDlg(Format(GetTranslationText('FW_UPDATE_CONF', STR_FW_UPDATE_CONF),
                       [edtUpdateFirmwareFile.Text]), mtConfirmation, [mbYes, mbNo], 0) = mrYes then
  begin
    ZipMaster := TZipMaster.Create(nil);
    MyXModem := TXModem.Create(nil);
    try
      lblUpdateFirmwareStatus.Caption := 'Extracting file ...';
      Application.ProcessMessages;
      ZipMaster.ZipFileName := edtUpdateFirmwareFile.Text;
      MyStream := ZipMaster.ExtractFileToStream(ExtractFileName(ChangeFileExt(edtUpdateFirmwareFile.Text, '.bin')));
      if Assigned(MyStream) then begin
        lblUpdateFirmwareStatus.Caption := 'Device reset ...';
        Application.ProcessMessages;
        progUpdateFirmware.Max:= (MyStream.Size div 127);
        progUpdateFirmware.Position:=0;
        with MyXModem do begin
          ComPort := MyCommunication.GetComPort;
          ComPortSpeed := br115200;
          ComPortDataBits := db8BITS;
          ComPortStopBits :=sb1BITS;
          ComPortParity := ptNONE;
          ComPortRtsControl := rcRTSDISABLE;
          ComPortHwHandshaking := hhNONE;
          Timeout := 1500;
          EnableDTROnOpen := false;
        end;
        // RESET
        MyCommunication.WriteReset;
        MyCommunication.Disconnect;
        lblUpdateFirmwareStatus.Caption := 'Connecting device ...';
        Application.ProcessMessages;
        if MyXModem.Connect then begin
          lblUpdateFirmwareStatus.Caption := 'Updating firmware ...';
          Application.ProcessMessages;
          if MyXModem.SendFromStream(MyStream, progUpdateFirmware) then begin
            MessageDlg(GetTranslationText('FW_UPDATE_DONE', STR_FW_UPDATE_DONE) + #13#10 +
                       GetTranslationText('APP_RESTART', STR_APP_RESTART), mtInformation, [mbOK], 0);
            progUpdateFirmware.Position:=0;
            MyXModem.Disconnect();
          end else begin
            MessageDlg(GetTranslationText('FW_UPDATE_FAIL', STR_FW_UPDATE_FAIL) + #13#10 +
                       GetTranslationText('APP_RESTART', STR_APP_RESTART), mtError, [mbOK], 0);
            MyXModem.Disconnect();
          end;
        end else begin
          MessageDlg(GetTranslationText('FW_UPDATE_SKIP', STR_FW_UPDATE_SKIP) + #13#10 +
                     GetTranslationText('APP_RESTART', STR_APP_RESTART), mtWarning, [mbOK], 0);
        end;
        ShellExecute(0, 'open', PChar(Application.ExeName), nil, nil, SW_SHOWNORMAL);
        Konec := True;
        Application.Terminate;
      end;
    finally
      FreeAndNil(MyXModem);
      FreeAndNil(ZipMaster);
      lblUpdateFirmwareStatus.Caption := '-';
    end;
  end;
end;
}

procedure TformMagX2Menu.edtUpdateFirmwareFileChange(Sender: TObject);
begin
  inherited;
  btnUpdateFirmware.Enabled := (edtUpdateFirmwareFile.Text <> '');
end;

procedure TformMagX2Menu.btnGPRS_ReadClick(Sender: TObject);
var FourByte:T4Byte;
    Str: String;
    ReadOK:Boolean;
    WaitFrm : TformWait;
begin
  // GPRS - Read all data
  if not Busy then
  try
    Busy:=true;
    WaitFrm:=TformWait.Create(Self);
    Screen.Cursor:=crHourGlass;
    WaitFrm.gWait.Progress:=0;
    WaitFrm.Show;
    SetCenter(WaitFrm);
    WaitFrm.caption:='Please wait...';
    Application.ProcessMessages;

    lblGPRS_IPaddress.Caption := '';
    lblGPRS_Status.Caption :='';
    shpGPRS_Status.Brush.Color := clWhite;

    try
      // Gateway
      repeat
        ReadOK:=MyCommunication.ReadMenuStr(GetAddress(ProtokolType,'GSM_GATEWAY0',Values),Str,64);
      until(ReadOK)or(MessageDlg('Reading error',mtError, [mbRetry, mbAbort], 0) = mrAbort);
      if not ReadOK then
        Raise Exception.Create('Reading error');  //ukonci cteni
      edtGPRS_Gateway.Text := Str;
      WaitFrm.gWait.Progress:=15;
      Application.ProcessMessages;
      // User
      repeat
        ReadOK:=MyCommunication.ReadMenuStr(GetAddress(ProtokolType,'GSM_USER0',Values),Str,12);
      until(ReadOK)or(MessageDlg('Reading error',mtError, [mbRetry, mbAbort], 0) = mrAbort);
      if not ReadOK then
        Raise Exception.Create('Reading error');  //ukonci cteni
      edtGPRS_User.Text := Str;
      WaitFrm.gWait.Progress:=30;
      Application.ProcessMessages;
      // Password
      repeat
        ReadOK:=MyCommunication.ReadMenuStr(GetAddress(ProtokolType,'GSM_PASSWORD0',Values),Str,12);
      until(ReadOK)or(MessageDlg('Reading error',mtError, [mbRetry, mbAbort], 0) = mrAbort);
      if not ReadOK then
        Raise Exception.Create('Reading error');  //ukonci cteni
      edtGPRS_Password.Text := Str;
      WaitFrm.gWait.Progress:=45;
      Application.ProcessMessages;
      // Port
      repeat
        ReadOK:=MyCommunication.ReadMenuValue(GetAddress(ProtokolType,'GSM_PORT',Values),FourByte);
      until(ReadOK)or(MessageDlg('Reading error',mtError, [mbRetry, mbAbort], 0) = mrAbort);
      if not ReadOK then
        Raise Exception.Create('Reading error');  //ukonci cteni
      seGPRS_Port.Value := Integer(FourByte);
      WaitFrm.gWait.Progress:=60;
      Application.ProcessMessages;
      // PIN
      repeat
        ReadOK:=MyCommunication.ReadMenuValue(GetAddress(ProtokolType,'GSM_PIN',Values),FourByte);
      until(ReadOK)or(MessageDlg('Reading error',mtError, [mbRetry, mbAbort], 0) = mrAbort);
      if not ReadOK then
        Raise Exception.Create('Reading error');  //ukonci cteni
      seGPRS_PIN.Value := Integer(FourByte);
      WaitFrm.gWait.Progress:=75;
      Application.ProcessMessages;
      // IP
      repeat
        ReadOK:=MyCommunication.ReadMenuValue(GSM_IP_VALUE_ADDRESS{GetAddress(ProtokolType,'GSM_IP',Values)},FourByte);
      until(ReadOK)or(MessageDlg('Reading error',mtError, [mbRetry, mbAbort], 0) = mrAbort);
      if not ReadOK then
        Raise Exception.Create('Reading error');  //ukonci cteni
      lblGPRS_IPaddress.Caption := Format('%d.%d.%d.%d', [FourByte[3], FourByte[2], FourByte[1], FourByte[0]]);
      WaitFrm.gWait.Progress:=90;
      Application.ProcessMessages;
      // Status
      repeat
        ReadOK:=MyCommunication.ReadMenuValue(GetAddress(ProtokolType,'GSM_PRESSENT',Values),FourByte);
      until(ReadOK)or(MessageDlg('Reading error',mtError, [mbRetry, mbAbort], 0) = mrAbort);
      if not ReadOK then
        Raise Exception.Create('Reading error');  //ukonci cteni
      if (Integer(FourByte) = 1) then begin
        shpGPRS_Status.Brush.Color := clGreen;
      end else begin
        shpGPRS_Status.Brush.Color := clLtGray;
      end;
      WaitFrm.gWait.Progress:=100;
      Application.ProcessMessages;
      lblGPRS_Status.Caption :='Reading successfully';
    except
      lblGPRS_Status.Caption := 'Reading failed';
      shpGPRS_Status.Brush.Color := clRed;
    end;
  finally
    FreeAndNil(WaitFrm);
    Screen.Cursor:=crDefault;
  Busy:=false;
  end;
end;

procedure TformMagX2Menu.btnGPRS_WriteClick(Sender: TObject);
var WriteOK:Boolean;
    WaitFrm : TformWait;
begin
  // GPRS - Read all data
  if not Busy then
  try
    Busy:=true;
    WaitFrm:=TformWait.Create(Self);
    Screen.Cursor:=crHourGlass;
    WaitFrm.gWait.Progress:=0;
    WaitFrm.Show;
    SetCenter(WaitFrm);
    WaitFrm.caption:='Please wait...';
    Application.ProcessMessages;

    lblGPRS_Status.Caption :='';
    try
      // Gateway
      repeat
        WriteOK:=MyCommunication.WriteMenuStr(GetAddress(ProtokolType,'GSM_GATEWAY0',Values),edtGPRS_Gateway.Text,64);
      until (WriteOK)or(MessageDlg('Write error',mtError, [mbRetry, mbAbort], 0) = mrAbort);
      if not WriteOK then
        Raise Exception.Create('Write error');  //ukonci zapis
      WaitFrm.gWait.Progress:=15;
      Application.ProcessMessages;
      // User
      repeat
        WriteOK:=MyCommunication.WriteMenuStr(GetAddress(ProtokolType,'GSM_USER0',Values),edtGPRS_User.Text,12);
      until (WriteOK)or(MessageDlg('Write error',mtError, [mbRetry, mbAbort], 0) = mrAbort);
      if not WriteOK then
        Raise Exception.Create('Write error');  //ukonci zapis
      WaitFrm.gWait.Progress:=30;
      Application.ProcessMessages;
      // Password
      repeat
        WriteOK:=MyCommunication.WriteMenuStr(GetAddress(ProtokolType,'GSM_PASSWORD0',Values),edtGPRS_Password.Text,12);
      until (WriteOK)or(MessageDlg('Write error',mtError, [mbRetry, mbAbort], 0) = mrAbort);
      if not WriteOK then
        Raise Exception.Create('Write error');  //ukonci zapis
      WaitFrm.gWait.Progress:=45;
      Application.ProcessMessages;
      // Port
      repeat
        WriteOK:=MyCommunication.WriteMenuValue(GetAddress(ProtokolType,'GSM_PORT',Values),T4Byte(seGPRS_Port.Value));
      until (WriteOK)or(MessageDlg('Write error',mtError, [mbRetry, mbAbort], 0) = mrAbort);
      if not WriteOK then
        Raise Exception.Create('Write error');  //ukonci zapis
      WaitFrm.gWait.Progress:=60;
      Application.ProcessMessages;
      // PIN
      repeat
        WriteOK:=MyCommunication.WriteMenuValue(GetAddress(ProtokolType,'GSM_PIN',Values),T4Byte(seGPRS_PIN.Value));
      until (WriteOK)or(MessageDlg('Write error',mtError, [mbRetry, mbAbort], 0) = mrAbort);
      if not WriteOK then
        Raise Exception.Create('Write error');  //ukonci zapis
      WaitFrm.gWait.Progress:=100;
      Application.ProcessMessages;
      lblGPRS_Status.Caption :='GPRS setting writed successfully';
    except
      lblGPRS_Status.Caption := 'GPRS setting writed failed';
    end;
  finally
    FreeAndNil(WaitFrm);
    Screen.Cursor:=crDefault;
  Busy:=false;
  end;
end;

procedure TformMagX2Menu.btnGSMReadNumbersClick(Sender: TObject);
var FourByte: T4Byte;
    NumH, NumL: Integer;
    ReadOK:Boolean;
    WaitFrm : TformWait;

  function GetNumber(ANumH, ANumL: Integer): String;
  begin
    if (ANumH > 0) then begin
      Result := Format('%d%.8d', [ANumH, ANumL]);
    end else begin
      Result := Format('%d', [ANumL]);
    end;
  end;

begin
  // GSM - Read Numbers
  if not Busy then begin
    try
      Busy := true;
      WaitFrm := TformWait.Create(Self);
      Screen.Cursor := crHourGlass;
      WaitFrm.gWait.Progress:=0;
      WaitFrm.Show;
      SetCenter(WaitFrm);
      WaitFrm.caption := 'Please wait...';
      Application.ProcessMessages;

      lblGSMNumbersStatus.Caption := '';
      try
        // Phone1
        repeat
          ReadOK:=MyCommunication.ReadMenuValue(GetAddress(ProtokolType,'GSM_PHONE_1_H',Values), FourByte);
        until(ReadOK)or(MessageDlg('Reading error',mtError, [mbRetry, mbAbort], 0) = mrAbort);
        if not ReadOK then
          Raise Exception.Create('Reading error');  //ukonci cteni
        NumH := Integer(FourByte);
        WaitFrm.gWait.Progress := 10;
        Application.ProcessMessages;
        repeat
          ReadOK:=MyCommunication.ReadMenuValue(GetAddress(ProtokolType,'GSM_PHONE_1_L',Values), FourByte);
        until(ReadOK)or(MessageDlg('Reading error',mtError, [mbRetry, mbAbort], 0) = mrAbort);
        if not ReadOK then
          Raise Exception.Create('Reading error');  //ukonci cteni
        NumL := Integer(FourByte);
        WaitFrm.gWait.Progress := 20;
        Application.ProcessMessages;
        edtGSMPhone1.Text := GetNumber(NumH, NumL);
        WaitFrm.gWait.Progress := 30;
        Application.ProcessMessages;
        // Phone2
        repeat
          ReadOK:=MyCommunication.ReadMenuValue(GetAddress(ProtokolType,'GSM_PHONE_2_H',Values), FourByte);
        until(ReadOK)or(MessageDlg('Reading error',mtError, [mbRetry, mbAbort], 0) = mrAbort);
        if not ReadOK then
          Raise Exception.Create('Reading error');  //ukonci cteni
        NumH := Integer(FourByte);
        WaitFrm.gWait.Progress := 40;
        Application.ProcessMessages;
        repeat
          ReadOK:=MyCommunication.ReadMenuValue(GetAddress(ProtokolType,'GSM_PHONE_2_L',Values), FourByte);
        until(ReadOK)or(MessageDlg('Reading error',mtError, [mbRetry, mbAbort], 0) = mrAbort);
        if not ReadOK then
          Raise Exception.Create('Reading error');  //ukonci cteni
        NumL := Integer(FourByte);
        WaitFrm.gWait.Progress := 50;
        Application.ProcessMessages;
        edtGSMPhone2.Text := GetNumber(NumH, NumL);
        WaitFrm.gWait.Progress := 60;
        Application.ProcessMessages;
        // Phone3
        repeat
          ReadOK:=MyCommunication.ReadMenuValue(GetAddress(ProtokolType,'GSM_PHONE_3_H',Values), FourByte);
        until(ReadOK)or(MessageDlg('Reading error',mtError, [mbRetry, mbAbort], 0) = mrAbort);
        if not ReadOK then
          Raise Exception.Create('Reading error');  //ukonci cteni
        NumH := Integer(FourByte);
        WaitFrm.gWait.Progress := 70;
        Application.ProcessMessages;
        repeat
          ReadOK:=MyCommunication.ReadMenuValue(GetAddress(ProtokolType,'GSM_PHONE_3_L',Values), FourByte);
        until(ReadOK)or(MessageDlg('Reading error',mtError, [mbRetry, mbAbort], 0) = mrAbort);
        if not ReadOK then
          Raise Exception.Create('Reading error');  //ukonci cteni
        NumL := Integer(FourByte);
        WaitFrm.gWait.Progress := 80;
        Application.ProcessMessages;
        edtGSMPhone3.Text := GetNumber(NumH, NumL);
        WaitFrm.gWait.Progress := 100;
        Application.ProcessMessages;
        lblGSMNumbersStatus.Caption :='Reading successfully';
      except
        lblGSMNumbersStatus.Caption := 'Reading failed';
      end;
    finally
      FreeAndNil(WaitFrm);
      Screen.Cursor := crDefault;
      Busy := false;
    end;
  end;
end;

procedure TformMagX2Menu.btnGSMWriteNumbersClick(Sender: TObject);
var WriteOK: Boolean;
    NumH, NumL: Integer;
    WaitFrm : TformWait;

  function ExtractNumber(ANum: String; var ANumH, ANumL: Integer): Boolean;
  begin
    Result := False;
    try
      if Length(ANum) <= 8 then begin
        ANumH := 0;
        ANumL := StrToInt(ANum);
      end else begin
        ANumH := StrToInt(Copy(ANum, 1, Length(ANum) - 8));
        ANumL := StrToInt(Copy(ANum, Length(ANum) - 7, 8));
      end;
      Result := True;
    except
    end;
  end;

begin
  // GSM Numbers - Write data
  if not Busy then begin
    try
      Busy := true;
      WaitFrm := TformWait.Create(Self);
      Screen.Cursor := crHourGlass;
      WaitFrm.gWait.Progress := 0;
      WaitFrm.Show;
      SetCenter(WaitFrm);
      WaitFrm.caption := 'Please wait...';
      Application.ProcessMessages;

      lblGSMNumbersStatus.Caption := '';
      try
        // Phone1
        if not ExtractNumber(edtGSMPhone1.Text, NumH, NumL) then begin
          edtGSMPhone1.Font.Color := clRed;
          raise Exception.Create('Incorrect format');
        end;
        WaitFrm.gWait.Progress:=10;
        Application.ProcessMessages;
        repeat
          WriteOK:=MyCommunication.WriteMenuValue(GetAddress(ProtokolType,'GSM_PHONE_1_H',Values),T4Byte(NumH));
        until (WriteOK)or(MessageDlg('Write error',mtError, [mbRetry, mbAbort], 0) = mrAbort);
        if not WriteOK then
          Raise Exception.Create('Write error');  //ukonci zapis
        WaitFrm.gWait.Progress:=20;
        Application.ProcessMessages;
        repeat
          WriteOK:=MyCommunication.WriteMenuValue(GetAddress(ProtokolType,'GSM_PHONE_1_L',Values),T4Byte(NumL));
        until (WriteOK)or(MessageDlg('Write error',mtError, [mbRetry, mbAbort], 0) = mrAbort);
        if not WriteOK then
          Raise Exception.Create('Write error');  //ukonci zapis
        WaitFrm.gWait.Progress:=30;
        Application.ProcessMessages;
        // Phone2
        if not ExtractNumber(edtGSMPhone2.Text, NumH, NumL) then begin
          edtGSMPhone2.Font.Color := clRed;
          raise Exception.Create('Incorrect format');
        end;
        WaitFrm.gWait.Progress:=40;
        Application.ProcessMessages;
        repeat
          WriteOK:=MyCommunication.WriteMenuValue(GetAddress(ProtokolType,'GSM_PHONE_2_H',Values),T4Byte(NumH));
        until (WriteOK)or(MessageDlg('Write error',mtError, [mbRetry, mbAbort], 0) = mrAbort);
        if not WriteOK then
          Raise Exception.Create('Write error');  //ukonci zapis
        WaitFrm.gWait.Progress:=50;
        Application.ProcessMessages;
        repeat
          WriteOK:=MyCommunication.WriteMenuValue(GetAddress(ProtokolType,'GSM_PHONE_2_L',Values),T4Byte(NumL));
        until (WriteOK)or(MessageDlg('Write error',mtError, [mbRetry, mbAbort], 0) = mrAbort);
        if not WriteOK then
          Raise Exception.Create('Write error');  //ukonci zapis
        WaitFrm.gWait.Progress:=60;
        Application.ProcessMessages;
        // Phone3
        if not ExtractNumber(edtGSMPhone3.Text, NumH, NumL) then begin
          edtGSMPhone3.Font.Color := clRed;
          raise Exception.Create('Incorrect format');
        end;
        WaitFrm.gWait.Progress:=70;
        Application.ProcessMessages;
        repeat
          WriteOK:=MyCommunication.WriteMenuValue(GetAddress(ProtokolType,'GSM_PHONE_3_H',Values),T4Byte(NumH));
        until (WriteOK)or(MessageDlg('Write error',mtError, [mbRetry, mbAbort], 0) = mrAbort);
        if not WriteOK then
          Raise Exception.Create('Write error');  //ukonci zapis
        WaitFrm.gWait.Progress:=80;
        Application.ProcessMessages;
        repeat
          WriteOK:=MyCommunication.WriteMenuValue(GetAddress(ProtokolType,'GSM_PHONE_3_L',Values),T4Byte(NumL));
        until (WriteOK)or(MessageDlg('Write error',mtError, [mbRetry, mbAbort], 0) = mrAbort);
        if not WriteOK then
          Raise Exception.Create('Write error');  //ukonci zapis
        WaitFrm.gWait.Progress:=100;
        Application.ProcessMessages;
        lblGSMNumbersStatus.Caption :='Writed successfully';
      except
        on E: Exception do begin
          lblGSMNumbersStatus.Caption := E.Message;
        end;
      end;
    finally
      FreeAndNil(WaitFrm);
      Screen.Cursor := crDefault;
      Busy := false;
    end;
  end;
end;

procedure TformMagX2Menu.edtGSMPhoneChange(Sender: TObject);
begin
  inherited;
  TEdit(Sender).Font.Color := clWindowText;
end;

procedure TformMagX2Menu.btnGSMReadEventsClick(Sender: TObject);
var FourByte: T4Byte;
    ReadOK:Boolean;
    WaitFrm : TformWait;
begin
  // GSM - Read Numbers
  if not Busy then begin
    try
      Busy := true;
      WaitFrm := TformWait.Create(Self);
      Screen.Cursor := crHourGlass;
      WaitFrm.gWait.Progress:=0;
      WaitFrm.Show;
      SetCenter(WaitFrm);
      WaitFrm.caption := 'Please wait...';
      Application.ProcessMessages;

      lblGSMEventsStatus.Caption := '';
      try
        // Set Event - EmptyPipe
        repeat
          ReadOK:=MyCommunication.ReadMenuValue(GetAddress(ProtokolType,'GSM_EMPTY_PIPE',Values), FourByte);
        until(ReadOK)or(MessageDlg('Reading error',mtError, [mbRetry, mbAbort], 0) = mrAbort);
        if not ReadOK then
          Raise Exception.Create('Reading error');  //ukonci cteni
        cmbGSMEventEmptyPipe.ItemIndex := Integer(FourByte);
        WaitFrm.gWait.Progress := 15;
        Application.ProcessMessages;
        // Set Event - ZeroFlow
        repeat
          ReadOK:=MyCommunication.ReadMenuValue(GetAddress(ProtokolType,'GSM_ZERO_FLOW',Values), FourByte);
        until(ReadOK)or(MessageDlg('Reading error',mtError, [mbRetry, mbAbort], 0) = mrAbort);
        if not ReadOK then
          Raise Exception.Create('Reading error');  //ukonci cteni
        cmbGSMEventZeroFlow.ItemIndex := Integer(FourByte);
        WaitFrm.gWait.Progress := 30;
        Application.ProcessMessages;
        // Set Event - Error
        repeat
          ReadOK:=MyCommunication.ReadMenuValue(GetAddress(ProtokolType,'GSM_ERROR_DETECT',Values), FourByte);
        until(ReadOK)or(MessageDlg('Reading error',mtError, [mbRetry, mbAbort], 0) = mrAbort);
        if not ReadOK then
          Raise Exception.Create('Reading error');  //ukonci cteni
        cmbGSMEventError.ItemIndex := Integer(FourByte);
        WaitFrm.gWait.Progress := 45;
        Application.ProcessMessages;
        // Set Event - EmptyPipe
        repeat
          ReadOK:=MyCommunication.ReadMenuValue(GetAddress(ProtokolType,'GSM_EMPTY_PIPE_SEND',Values), FourByte);
        until(ReadOK)or(MessageDlg('Reading error',mtError, [mbRetry, mbAbort], 0) = mrAbort);
        if not ReadOK then
          Raise Exception.Create('Reading error');  //ukonci cteni
        cmbGSMSendEventEmptyPipe.ItemIndex := Integer(FourByte);
        WaitFrm.gWait.Progress := 60;
        Application.ProcessMessages;
        // Set Event - ZeroFlow
        repeat
          ReadOK:=MyCommunication.ReadMenuValue(GetAddress(ProtokolType,'GSM_ZERO_FLOW_SEND',Values), FourByte);
        until(ReadOK)or(MessageDlg('Reading error',mtError, [mbRetry, mbAbort], 0) = mrAbort);
        if not ReadOK then
          Raise Exception.Create('Reading error');  //ukonci cteni
        cmbGSMSendEventZeroFlow.ItemIndex := Integer(FourByte);
        WaitFrm.gWait.Progress := 75;
        Application.ProcessMessages;
        // Interval
        repeat
          ReadOK:=MyCommunication.ReadMenuValue(GetAddress(ProtokolType,'GSM_DATA_INTERVAL',Values), FourByte);
        until(ReadOK)or(MessageDlg('Reading error',mtError, [mbRetry, mbAbort], 0) = mrAbort);
        if not ReadOK then
          Raise Exception.Create('Reading error');  //ukonci cteni
        seGSMInterval.Value := Integer(FourByte);
        WaitFrm.gWait.Progress := 100;
        Application.ProcessMessages;
        lblGSMEventsStatus.Caption :='Reading successfully';
      except
        lblGSMEventsStatus.Caption := 'Reading failed';
      end;
    finally
      FreeAndNil(WaitFrm);
      Screen.Cursor := crDefault;
      Busy := false;
    end;
  end;
end;

procedure TformMagX2Menu.btnGSMWriteEventsClick(Sender: TObject);
var WriteOK: Boolean;
    WaitFrm : TformWait;
begin
  // GSM Numbers - Write data
  if not Busy then begin
    try
      Busy := true;
      WaitFrm := TformWait.Create(Self);
      Screen.Cursor := crHourGlass;
      WaitFrm.gWait.Progress := 0;
      WaitFrm.Show;
      SetCenter(WaitFrm);
      WaitFrm.caption := 'Please wait...';
      Application.ProcessMessages;

      lblGSMNumbersStatus.Caption := '';
      try
        // Set Event - EmptyPipe
        repeat
          WriteOK:=MyCommunication.WriteMenuValue(GetAddress(ProtokolType,'GSM_EMPTY_PIPE',Values),T4Byte(cmbGSMEventEmptyPipe.ItemIndex));
        until (WriteOK)or(MessageDlg('Write error',mtError, [mbRetry, mbAbort], 0) = mrAbort);
        if not WriteOK then
          Raise Exception.Create('Write error');  //ukonci zapis
        WaitFrm.gWait.Progress:=15;
        Application.ProcessMessages;
        // Set Event - ZeroFlow
        repeat
          WriteOK:=MyCommunication.WriteMenuValue(GetAddress(ProtokolType,'GSM_ZERO_FLOW',Values),T4Byte(cmbGSMEventZeroFlow.ItemIndex));
        until (WriteOK)or(MessageDlg('Write error',mtError, [mbRetry, mbAbort], 0) = mrAbort);
        if not WriteOK then
          Raise Exception.Create('Write error');  //ukonci zapis
        WaitFrm.gWait.Progress:=30;
        Application.ProcessMessages;
        // Set Event - Error
        repeat
          WriteOK:=MyCommunication.WriteMenuValue(GetAddress(ProtokolType,'GSM_ERROR_DETECT',Values),T4Byte(cmbGSMEventError.ItemIndex));
        until (WriteOK)or(MessageDlg('Write error',mtError, [mbRetry, mbAbort], 0) = mrAbort);
        if not WriteOK then
          Raise Exception.Create('Write error');  //ukonci zapis
        WaitFrm.gWait.Progress:=45;
        Application.ProcessMessages;
        // Send Event - EmptyPipe
        repeat
          WriteOK:=MyCommunication.WriteMenuValue(GetAddress(ProtokolType,'GSM_EMPTY_PIPE_SEND',Values),T4Byte(cmbGSMSendEventEmptyPipe.ItemIndex));
        until (WriteOK)or(MessageDlg('Write error',mtError, [mbRetry, mbAbort], 0) = mrAbort);
        if not WriteOK then
          Raise Exception.Create('Write error');  //ukonci zapis
        WaitFrm.gWait.Progress:=60;
        Application.ProcessMessages;
        // Send Event - ZeroFlow
        repeat
          WriteOK:=MyCommunication.WriteMenuValue(GetAddress(ProtokolType,'GSM_ZERO_FLOW_SEND',Values),T4Byte(cmbGSMSendEventZeroFlow.ItemIndex));
        until (WriteOK)or(MessageDlg('Write error',mtError, [mbRetry, mbAbort], 0) = mrAbort);
        if not WriteOK then
          Raise Exception.Create('Write error');  //ukonci zapis
        WaitFrm.gWait.Progress:=75;
        Application.ProcessMessages;
        // Interval
        repeat
          WriteOK:=MyCommunication.WriteMenuValue(GetAddress(ProtokolType,'GSM_DATA_INTERVAL',Values),T4Byte(seGSMInterval.Value));
        until (WriteOK)or(MessageDlg('Write error',mtError, [mbRetry, mbAbort], 0) = mrAbort);
        if not WriteOK then
          Raise Exception.Create('Write error');  //ukonci zapis
        WaitFrm.gWait.Progress:=100;
        Application.ProcessMessages;
        lblGSMEventsStatus.Caption :='Writed successfully';
      except
        on E: Exception do begin
          lblGSMEventsStatus.Caption := E.Message;
        end;
      end;
    finally
      FreeAndNil(WaitFrm);
      Screen.Cursor := crDefault;
      Busy := false;
    end;
  end;
end;

procedure TformMagX2Menu.WMNeedReset(var Message: TMessage);
begin
  NeedReset := True;
  Close;
end;

procedure TformMagX2Menu.btnWriteSensorUnitNoClick(Sender: TObject);
var SensorUnitNo: Cardinal;
begin
  if not Busy then begin
  Busy := true;
  if seSensorUnitNo.Visible then begin
    SensorUnitNo := seSensorUnitNo.Value;
    if MyCommunication.WriteSensorUnitNo(SensorUnitNo) then begin
      MeasurmentStatusLabel.Caption := GetTranslationText('WRITE_SUCCESS', STR_WRITE_SUCC);
      MeasurmentStatusLabel.Font.Color:=clGreen;
    end else begin
      MeasurmentStatusLabel.Caption := GetTranslationText('WRITE_FAILED', STR_WRITE_FAIL);
      MeasurmentStatusLabel.Font.Color:=clRed;
    end;
  end;
  Busy := false;
  end;
end;

end.
