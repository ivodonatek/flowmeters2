unit MagX2ValueIHDTFrame;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  MagX2ValueIHexFrame, ExtCtrls, StdCtrls;

// IHDT = Info Hex DateTime

type
  TframeMagX2ValueIHDT = class(TframeMagX2ValueIHex)
  private
    { Private declarations }
  public
    { Public declarations }
    function GetValueText(Value:Integer): String; override;
  end;

implementation

{$R *.DFM}

{ TframeMagX2ValueIHDT }

function TframeMagX2ValueIHDT.GetValueText(Value: Integer): String;
begin
  Result := Format('%.8x', [Value]);
  Insert(':', Result, 7);
  Insert(' ', Result, 5);
  Insert('/', Result, 3);
end;

end.
