unit MagX2ValuePaswFrame;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, MagX2MyCommunicationClass, ExtCtrls, NodeClass,
  Buttons, WaitFrm, MagX2FunctionsUnit,
  MagX2ValueHeadFrame, comctrls, FunctionsUnit;

type
  TframeMagX2ValuePasw = class(TframeMagX2Head)
    Panel1: TPanel;
    eValue: TEdit;
    btnOK: TButton;
    btnCancel: TButton;
    procedure btnConfirmClick(Sender: TObject);
    procedure btnCancelClick(Sender: TObject);
    procedure FrameEnter(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
//    function IsNumber(s: string): Boolean;
    constructor CreateFrame(AOwner: TComponent; MyCommunication : TMyCommunication;
      EditNode : TNode; Language: TLanguage; LanguageIndex: Integer); override;
  end;

var
  frameMagX2ValuePasw: TframeMagX2ValuePasw;

implementation

uses MagX2MenuFrm, MagX2GlobalUtilsClass, MagX2TranslationUnit;

{$R *.DFM}

{ TframeValuePasw }
constructor TframeMagX2ValuePasw.CreateFrame(AOwner: TComponent; MyCommunication : TMyCommunication;
    EditNode : TNode; Language: TLanguage; LanguageIndex: Integer);
begin
  inherited;
  lblTitle.Caption:=FEditNode.Popis;
end;

procedure TframeMagX2ValuePasw.btnConfirmClick(Sender: TObject);
var FourByte : T4Byte;
    ReadOK:Boolean;
    LastCursor:TCursor;
begin
  inherited;
  LastCursor:=Screen.Cursor;
  try
    if Assigned(WaitFrm) then
    	WaitFrm.Show;
    Screen.Cursor:=crHourGlass;
    Application.ProcessMessages();

    case FMyCommunication.ProtokolType of
      ptMagX1:
	  	raise Exception.Create('Unsuported communications protocol');
      else
      begin
        Integer(FourByte):=StrToIntDef(eValue.Text,0);
        // Write Password
        repeat
          ReadOK:=FMyCommunication.WriteMenuValue(FEditNode.ModbusAdr-1,FourByte);
        until (ReadOK)or(MessageDlg(GetTranslationText('READ_ERROR', STR_READ_ERROR),mtError, [mbRetry, mbAbort], 0) = mrAbort);
        if not ReadOK then raise Exception.Create('Error read password');
        // Read if Password is OK
        repeat
          ReadOK:=FMyCommunication.ReadMenuValue(FEditNode.ModbusAdr-1,FourByte);
        until (ReadOK)or(MessageDlg(GetTranslationText('READ_ERROR', STR_READ_ERROR),mtError, [mbRetry, mbAbort], 0) = mrAbort);
        if (ReadOK) and (Integer(FourByte)=1) then begin
          // Expand
          TformMagX2Menu(Self.Owner).tvMenu.Selected.Expand(false);
        end else begin
          // Show warning
          MessageDlg(GetTranslationText('INCORRECT_PASSWORD', STR_INCORRECT_PASSWORD), mtWarning, [mbOK], 0);
          eValue.SetFocus;
        end;
      end;
    end;
  finally
    Screen.Cursor:=LastCursor;
    if Assigned(WaitFrm) then
    	WaitFrm.Hide();
  end;
end;

procedure TframeMagX2ValuePasw.btnCancelClick(Sender: TObject);
begin
  eValue.Text:='';
  eValue.SetFocus;
end;

procedure TframeMagX2ValuePasw.FrameEnter(Sender: TObject);
begin
  inherited;
	eValue.SetFocus();
end;

end.
