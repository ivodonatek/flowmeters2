unit MagX2RunUnit;

interface

procedure MagX2Run;

implementation

uses sysutils, controls, Dialogs, Forms, MagX2TranslationUnit,
  MagX2GlobalUtilsClass, MagX2MyCommunicationClass,
  MagX2StatFrm, MagX2EnterFrm, MagX2MenuFrm;

var
    Demo : Boolean;
    ReadOK:Boolean;
    firmwareNO:integer;
    MyCommunication : TMyCommunication;

function GetDeviceFirmwareNumber(AProtokolType:TProtokolType;ASlaveID,AComNumber:Cardinal;
   BaudRate:TBaudRate;StopBits:TStopBits;Parity:TParity;Timeout:Integer;
   RtsControl:TRtsControl;EnableDTROnOpen:Boolean;ASwitchWait:Cardinal;
   AIPAddress: String; APort: Integer):integer;
var firmwareNO:integer;
begin
    MyCommunication.SetCommunicationParam(AComNumber,ASlaveID,BaudRate,db8BITS,
        StopBits,Parity,RtsControl,EnableDTROnOpen,Timeout,AIPAddress,APort);
    if not MyCommunication.Connect() then
        raise Exception.Create('Comunication port error');
    //cti verzi FW
    repeat
      ReadOK:=MyCommunication.ReadFirmNO(Cardinal(firmwareNO));
    until (ReadOK)or(MessageDlg(GetTranslationText('READ_ERROR', STR_READ_ERROR), mtError, [mbRetry, mbAbort], 0) = mrAbort);
    if not ReadOK then
        raise Exception.Create('No device was detected');
    Result:=firmwareNO;
end;

procedure MagX2Run;
var
  statForm: TformMagX2Stat;
  menuForm: TformMagX2Menu;
begin
    MyCommunication:= nil;
    statForm:= nil;
    menuForm:= nil;

    try
      case formMagX2Enter.ShowModal of
        //statistic
        mrOK:
        begin
              case formMagX2Enter.ProtokolType of
                ptMagX1:MyCommunication:=TMyCommPort.Create();
                ptModbus:MyCommunication:=TMyModbus.Create();
                ptTCPModbus:MyCommunication:=TMyTCPModbus.Create();
                ptDemo:MyCommunication:=TDemoCommunication.Create();
              else
                raise Exception.Create('Unsuported protocol');
              end;
              //cti verzi firmware
              firmwareNO := GetDeviceFirmwareNumber(formMagX2Enter.ProtokolType,formMagX2Enter.SlaveID,
                      formMagX2Enter.ComPortNumber,formMagX2Enter.BaudRate,formMagX2Enter.StopBits,
                      formMagX2Enter.Parity,formMagX2Enter.Timeout,formMagX2Enter.RtsControl ,true,
                      formMagX2Enter.rgConvertorRS485.ItemIndex, formMagX2Enter.edtTCPIPAddress.Text,
                      formMagX2Enter.seTCPPort.Value);

              statForm := TformMagX2Stat.Create(Application);
              statForm.Caption := 'MagX2 statistic';
              statForm.Init(MyCommunication, firmwareNO);
        end;
        //Service
        mrNo:
        begin
              case formMagX2Enter.ProtokolType of
                ptMagX1:MyCommunication:=TMyCommPort.Create();
                ptModbus:MyCommunication:=TMyModbus.Create();
                ptTCPModbus:MyCommunication:=TMyTCPModbus.Create();
                ptDemo:MyCommunication:=TDemoCommunication.Create();
              else
                raise Exception.Create('Unsuported protocol');
              end;
              //cti verzi firmware
              firmwareNO := GetDeviceFirmwareNumber(formMagX2Enter.ProtokolType,formMagX2Enter.SlaveID,
                      formMagX2Enter.ComPortNumber,formMagX2Enter.BaudRate,formMagX2Enter.StopBits,
                      formMagX2Enter.Parity,formMagX2Enter.Timeout,formMagX2Enter.RtsControl ,true,
                      formMagX2Enter.rgConvertorRS485.ItemIndex, formMagX2Enter.edtTCPIPAddress.Text,
                      formMagX2Enter.seTCPPort.Value);

              menuForm := TformMagX2Menu.Create(Application);
              menuForm.Caption := 'MagX2 service';
              menuForm.Init(MyCommunication, firmwareNO, formMagX2Enter.ProtokolType);
        end;
        //Close
        mrCancel:
        begin
        end;
      end;

    except
      on E: Exception do
      begin
        MessageDlg(E.Message,mtError, [mbOk], 0);
        if MyCommunication <> nil then begin
          MyCommunication.Disconnect;
          FreeAndNil(MyCommunication);
        end;
        if statForm <> nil then statForm.Close;
        if menuForm <> nil then menuForm.Close;        
      end;
    end;
end;

end.
