object formMagX2Pass: TformMagX2Pass
  Left = 549
  Top = 318
  BorderIcons = []
  BorderStyle = bsDialog
  Caption = 'Authorization'
  ClientHeight = 81
  ClientWidth = 330
  Color = 12555391
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'Verdana'
  Font.Style = [fsBold]
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 14
  object GroupBox1: TGroupBox
    Left = 8
    Top = 8
    Width = 313
    Height = 65
    Caption = 'Factory Setting'
    TabOrder = 0
    object Panel1: TPanel
      Left = 8
      Top = 16
      Width = 297
      Height = 41
      BevelOuter = bvLowered
      Color = 14670037
      TabOrder = 0
      object eValue: TEdit
        Left = 8
        Top = 9
        Width = 105
        Height = 24
        MaxLength = 8
        PasswordChar = '*'
        TabOrder = 0
      end
      object btnCancel: TButton
        Left = 200
        Top = 8
        Width = 73
        Height = 25
        Cancel = True
        Caption = 'Cancel'
        TabOrder = 1
        OnClick = btnCancelClick
      end
      object btnConfirm: TButton
        Left = 128
        Top = 8
        Width = 73
        Height = 25
        Caption = 'OK'
        Default = True
        TabOrder = 2
        OnClick = btnConfirmClick
      end
    end
  end
end
