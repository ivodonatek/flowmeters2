unit TranslationUnit;

interface

uses WideIniFile, FunctionsUnit;

procedure SetTranslationLanguage(ALanguage: TLanguage);
function GetTranslationText(ACode: WideString; ADefValue: WideString = ''): WideString;

implementation

uses Forms, SysUtils;

const
  LANG_FILE_NAME = 'Translation.dic';

var
  language: TLanguage = langEnglish;
  LanguageIniFile: TWideIniFile = nil;

procedure SetTranslationLanguage(ALanguage: TLanguage);
begin
  language:= ALanguage;
end;

function GetTranslationText(ACode: WideString; ADefValue: WideString = ''): WideString;
begin
  Result := ADefValue;
  if Assigned(LanguageIniFile) then begin
    Result := LanguageIniFile.ReadString(GetLanguageSection(language), ACode, ADefValue);
  end;
end;

initialization
LanguageIniFile := TWideIniFile.Create(IncludeTrailingBackslash(ExtractFilePath(Application.ExeName)) + LANG_FILE_NAME);

finalization
FreeAndNil(LanguageIniFile);

end.
