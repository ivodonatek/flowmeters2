unit MagB2ValueIHDTFrame;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  MagB2ValueIHexFrame, ExtCtrls, StdCtrls;

// IHDT = Info Hex DateTime

type
  TframeMagB2ValueIHDT = class(TframeMagB2ValueIHex)
  private
    { Private declarations }
  public
    { Public declarations }
    function GetValueText(Value:Integer): String; override;
  end;

implementation

{$R *.DFM}

{ TframeMagB2ValueIHDT }

function TframeMagB2ValueIHDT.GetValueText(Value: Integer): String;
begin
  Result := Format('%.8x', [Value]);
  Insert(':', Result, 7);
  Insert(' ', Result, 5);
  Insert('/', Result, 3);
end;

end.
