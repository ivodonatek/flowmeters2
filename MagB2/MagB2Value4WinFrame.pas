unit MagB2Value4WinFrame;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Spin, MagB2MyCommunicationClass, NodeClass, Buttons, ExtCtrls, Math,
  MagB2ChangeValueFrame, MagB2FunctionsUnit, MagB2ValueHeadFrame, FunctionsUnit;

type
 TframeMagB2Value4Win = class(TframeMagB2Head)
    GroupBox1: TGroupBox;
    Label2: TLabel;
    lblMin1: TLabel;
    Label3: TLabel;
    lblMax1: TLabel;
    Panel4: TPanel;
    Panel5: TPanel;
    GroupBox2: TGroupBox;
    Label4: TLabel;
    lblMin2: TLabel;
    Label5: TLabel;
    lblMax2: TLabel;
    Panel6: TPanel;
    Panel7: TPanel;
    btnWrite1: TButton;
    procedure btnWrite1Click(Sender: TObject);
  private
    { Private declarations }
    function FormatNameAndUnit(name:WideString; unitStr:string): WideString;
  public
    { Public declarations }
    constructor CreateFrame(AOwner: TComponent; MyCommunication : TMyCommunication;
      EditNode : TNode; Language: TLanguage; LanguageIndex: Integer); override;
  end;

var
  frameMagB2Value4Win: TframeMagB2Value4Win;
  VChange1,VChange2,VChange3,VChange4:TframeMagB2ValueChange;
implementation

uses MagB2GlobalUtilsClass, MagB2TranslationUnit, Constants;

{$R *.DFM}

{ TframeValue4Win }

constructor TframeMagB2Value4Win.CreateFrame(AOwner: TComponent; MyCommunication : TMyCommunication;
    EditNode : TNode; Language: TLanguage; LanguageIndex: Integer);
var FourByte : T4Byte;
    ReadOK:Boolean;
    LastCursor: TCursor;
    Value : real;
begin
  inherited;
  GroupBox1.Caption:=lblTitle.Caption;
  lblTitle.Caption:=FEditNode.NameParent;
  case FEditNode.Units2 of
    //'UKG/min','USG/min','m3/h','l/min'
    1..5:
        GroupBox2.Caption:=FormatNameAndUnit(TransformWideChars(TNodeText(FEditNode.MultiTexts.Items[1]).MultiText[MultiLanguageTextIndex], FEditNode.ConvertWideCharsPage), FlowUnitList[Integer(FlowUnit)]);
    //'UKG','USG','m3'
    6..8:
        GroupBox2.Caption:=FormatNameAndUnit(TransformWideChars(TNodeText(FEditNode.MultiTexts.Items[1]).MultiText[MultiLanguageTextIndex], FEditNode.ConvertWideCharsPage), VolumeUnitList[Integer(VolumeUnit)]);
    else
      begin
        if (FEditNode.Units2 >=0)and(FEditNode.Units2<= Length(Jednotky)) then begin
          GroupBox2.Caption:=FormatNameAndUnit(TransformWideChars(TNodeText(FEditNode.MultiTexts.Items[1]).MultiText[MultiLanguageTextIndex], FEditNode.ConvertWideCharsPage), Jednotky[FEditNode.Units2]);
        end else begin
          GroupBox2.Caption:=FormatNameAndUnit(FEditNode.Popis, 'Unknown');
        end;
      end;
  end;

  lblMin1.Caption:=FloatToStr(FEditNode.Min);
  lblMax1.Caption:=FloatToStr(FEditNode.Max);
  lblMin2.Caption:=FloatToStr(FEditNode.Min2);
  lblMax2.Caption:=FloatToStr(FEditNode.Max2);

  VChange1:=TframeMagB2ValueChange.CreateFrame(FEditNode.Digits, FEditNode.Decimal);
  VChange1.parent:=panel4;
  VChange1.show();
  VChange2:=TframeMagB2ValueChange.CreateFrame(FEditNode.Digits, FEditNode.Decimal);
  VChange2.parent:=panel5;
  VChange2.show();
  VChange3:=TframeMagB2ValueChange.CreateFrame(FEditNode.Digits2, FEditNode.Decimal2);
  VChange3.parent:=panel6;
  VChange3.show();
  VChange4:=TframeMagB2ValueChange.CreateFrame(FEditNode.Digits2, FEditNode.Decimal2);
  VChange4.parent:=panel7;
  VChange4.show();

  LastCursor := Screen.Cursor;
  try
    if Assigned(WaitFrm) then WaitFrm.Show;
    Screen.Cursor:=crHourGlass;
    //value1
    repeat
      Application.ProcessMessages;
      ReadOK:=ReadValue(FEditNode.ModbusAdr-1,FourByte);
    until(ReadOK)or(MessageDlg(GetTranslationText('READ_ERROR', STR_READ_ERROR),mtError, [mbRetry, mbAbort], 0) = mrAbort);
    if ReadOK then
    begin
      case FEditNode.Units of
        1..5: Value:= Flow_Conversion(Integer(FourByte), FlowUnit);
        6..8: Value:= Volume_Conversion(Integer(FourByte), VolumeUnit);
        else Value:=Integer(FourByte);
      end;
      VChange1.FillArray(round(Value));
      WaitFrm.gWait.AddProgress(10);
      Application.ProcessMessages;
      //value2
      repeat
        Application.ProcessMessages;
        ReadOK:=ReadValue(FEditNode.ModbusAdr2-1,FourByte);
      until(ReadOK)or(MessageDlg(GetTranslationText('READ_ERROR', STR_READ_ERROR),mtError, [mbRetry, mbAbort], 0) = mrAbort);
      if ReadOK then
      begin
      	case FEditNode.Units of
          1..5: Value:= Flow_Conversion(Integer(FourByte), FlowUnit);
      	  6..8: Value:= Volume_Conversion(Integer(FourByte), VolumeUnit);
      	  else Value:=Integer(FourByte);
      	end;
        VChange2.FillArray(round(Value));
        WaitFrm.gWait.AddProgress(10);
        Application.ProcessMessages;
        //value3
        repeat
          Application.ProcessMessages;
          ReadOK:=ReadValue(FEditNode.ModbusAdr3-1,FourByte);
        until(ReadOK)or(MessageDlg(GetTranslationText('READ_ERROR', STR_READ_ERROR),mtError, [mbRetry, mbAbort], 0) = mrAbort);
        if ReadOK then
        begin
          case FEditNode.Units2 of
            1..5: Value:= Flow_Conversion(Integer(FourByte), FlowUnit);
            6..8: Value:= Volume_Conversion(Integer(FourByte), VolumeUnit);
            else Value:=Integer(FourByte);
          end;
          VChange3.FillArray(round(Value));
          WaitFrm.gWait.AddProgress(10);
          Application.ProcessMessages;
          //value4
          repeat
            Application.ProcessMessages;
            ReadOK:=ReadValue(FEditNode.ModbusAdr4-1,FourByte);
          until(ReadOK)or(MessageDlg(GetTranslationText('READ_ERROR', STR_READ_ERROR),mtError, [mbRetry, mbAbort], 0) = mrAbort);
          if ReadOK then
          begin
            case FEditNode.Units2 of
              1..5: Value:= Flow_Conversion(Integer(FourByte), FlowUnit);
              6..8: Value:= Volume_Conversion(Integer(FourByte), VolumeUnit);
              else Value:=Integer(FourByte);
            end;
            VChange4.FillArray(round(Value));
            WaitFrm.gWait.AddProgress(10);
            Application.ProcessMessages;
          end;
        end;
      end;
    end;
    WaitFrm.gWait.AddProgress(10);
    Application.ProcessMessages;
  finally
    Screen.Cursor:=LastCursor;
    if Assigned(WaitFrm) then WaitFrm.Hide;
  end;
end;

procedure TframeMagB2Value4Win.btnWrite1Click(Sender: TObject);
var FourByte : T4Byte;
    Value1, Value2,Value3,Value4:integer;
    WriteOK:Boolean;
    LastCursor: TCursor;
    Value:real;
begin
  Value1:=VChange1.ArrayToInt;
  Value2:=VChange2.ArrayToInt;
  Value3:=VChange3.ArrayToInt;
  Value4:=VChange4.ArrayToInt;

  //kontrola platnosti dat
  if Value2 < Value1 then
    ShowMessage('The second value must be higher than first')
  else if (Value4 < Value3) and (FEditNode.Popis <> 'Calibration point 1,2') then
    ShowMessage('The fourth value must be higher than third')
  else if (Value1 > (FEditNode.Max * power(10, FEditNode.Decimal)))or(Value1 < FEditNode.Min * power(10, FEditNode.Decimal))then
    ShowMessage('First number is out of bounds')
  else if (Value2 > (FEditNode.Max * power(10, FEditNode.Decimal)))or(Value2 < FEditNode.Min * power(10, FEditNode.Decimal))then
   ShowMessage('Second number is out of bounds')
  else if (Value3 > (FEditNode.Max2 * power(10, FEditNode.Decimal2)))or(Value3 < FEditNode.Min2 * power(10, FEditNode.Decimal2))then
    ShowMessage('Third number is out of bounds')
  else if (Value4 > (FEditNode.Max2 * power(10, FEditNode.Decimal2)))or(Value4 < FEditNode.Min2 * power(10, FEditNode.Decimal2))then
    ShowMessage('Fourth number is out of bounds')
  else
  begin
   //data ok, zapis
   LastCursor := Screen.Cursor;
    try
      if Assigned(WaitFrm) then
      begin
      	WaitFrm.Show;
        WaitFrm.gWait.Progress:=0;
      end;
      Screen.Cursor:=crHourGlass;

      case FEditNode.Units of	//prevod jednotky
        1..5: Value:= Invert_Flow_Conversion(Value1, FlowUnit);
        6..8: Value:= Invert_Volume_Conversion(Value1, VolumeUnit);
        else
          Value:=Value1;
      end;
      Integer(FourByte):=Round(Value);
      repeat
        WriteOK:=WriteValue(FEditNode.ModbusAdr-1,FourByte);
      until (WriteOK)or(MessageDlg(GetTranslationText('WRITE_ERROR', STR_WRITE_ERROR),mtError, [mbRetry, mbAbort], 0) = mrAbort);
      WaitFrm.gWait.AddProgress(10);
      if not WriteOK then
        Raise Exception.Create(GetTranslationText('WRITE_ERROR', STR_WRITE_ERROR));  //ukonci zapis

      case FEditNode.Units of //prevod jednotky
        1..5: Value:= Invert_Flow_Conversion(Value2, FlowUnit);
        6..8: Value:= Invert_Volume_Conversion(Value2, VolumeUnit);
        else
          Value:=Value2;
      end;
      Integer(FourByte):=Round(Value);
      repeat
        WriteOK:=WriteValue(FEditNode.ModbusAdr2-1,FourByte);
      until (WriteOK)or(MessageDlg(GetTranslationText('WRITE_ERROR', STR_WRITE_ERROR),mtError, [mbRetry, mbAbort], 0) = mrAbort);
      WaitFrm.gWait.AddProgress(10);
      if not WriteOK then
        Raise Exception.Create(GetTranslationText('WRITE_ERROR', STR_WRITE_ERROR));  //ukonci zapis

        case FEditNode.Units2 of //prevod jednotky
        1..5: Value:= Invert_Flow_Conversion(Value3, FlowUnit);
        6..8: Value:= Invert_Volume_Conversion(Value3, VolumeUnit);
        else
          Value:=Value3;
      end;
      Integer(FourByte):=Round(Value);
      repeat
        WriteOK:=WriteValue(FEditNode.ModbusAdr3-1,FourByte);
      until (WriteOK)or(MessageDlg(GetTranslationText('WRITE_ERROR', STR_WRITE_ERROR),mtError, [mbRetry, mbAbort], 0) = mrAbort);
      WaitFrm.gWait.AddProgress(10);
      if not WriteOK then
        Raise Exception.Create(GetTranslationText('WRITE_ERROR', STR_WRITE_ERROR));  //ukonci zapis

        case FEditNode.Units of //prevod jednotky
        1..5: Value:= Invert_Flow_Conversion(Value4, FlowUnit);
        6..8: Value:= Invert_Volume_Conversion(Value4, VolumeUnit);
        else
          Value:=Value4;
      end;
      Integer(FourByte):=Round(Value);
      repeat
        WriteOK:=WriteValue(FEditNode.ModbusAdr4-1,FourByte);
      until (WriteOK)or(MessageDlg(GetTranslationText('WRITE_ERROR', STR_WRITE_ERROR),mtError, [mbRetry, mbAbort], 0) = mrAbort);
      WaitFrm.gWait.AddProgress(10);
      if not WriteOK then
        Raise Exception.Create(GetTranslationText('WRITE_ERROR', STR_WRITE_ERROR));  //ukonci zapis
      WaitFrm.gWait.AddProgress(10);
      Application.ProcessMessages;
    finally
      Screen.Cursor:=LastCursor;
      if Assigned(WaitFrm) then
    	WaitFrm.Hide;
    end;
  end;
end;

function TframeMagB2Value4Win.FormatNameAndUnit(name:WideString; unitStr:string): WideString;
var
  str: WideString;
begin
  str:= ' []';
  Insert(unitStr, str, 3);
  Insert(name, str, 1);
  Result:= str;
end;

end.
