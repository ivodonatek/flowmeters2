unit MagB2ValueIHexFrame;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, StdCtrls,
  MagB2MyCommunicationClass, NodeClass, ExtCtrls, MagB2FunctionsUnit,
  MagB2ValueHeadFrame, FunctionsUnit;

type
  TframeMagB2ValueIHex = class(TframeMagB2Head)
    Panel1: TPanel;
    lblValue: TLabel;
  private
    { Private declarations }
  protected
  public
    { Public declarations }
    constructor CreateFrame(AOwner: TComponent; MyCommunication : TMyCommunication ; 
      EditNode: TNode; Language: TLanguage; LanguageIndex: Integer); override;
    function GetValueText(Value:Integer): String; virtual;
  end;

implementation

uses MagB2GlobalUtilsClass, MagB2TranslationUnit;

{$R *.DFM}

constructor TframeMagB2ValueIHex.CreateFrame(AOwner: TComponent; MyCommunication: TMyCommunication; 
  EditNode: TNode; Language: TLanguage; LanguageIndex: Integer);
var
  ReadOK: Boolean;
  FourByte: T4Byte;
  LastCursor: TCursor;
begin
  inherited;
  LastCursor := Screen.Cursor;
  try
    if Assigned(WaitFrm) then WaitFrm.Show;
    Screen.Cursor:=crHourGlass;
    repeat
      Application.ProcessMessages;
      ReadOK := ReadValue(FEditNode.ModbusAdr - 1, FourByte);
    until (ReadOK) or (MessageDlg(GetTranslationText('READ_ERROR', STR_READ_ERROR),mtError, [mbRetry, mbAbort], 0) = mrAbort);
    if ReadOK then begin
      lblValue.Caption := GetValueText(Integer(FourByte));
    end else begin
      lblValue.Caption:='Read Error';
    end;
    if Assigned(WaitFrm) then WaitFrm.gWait.AddProgress(10);
    Application.ProcessMessages;
  finally
    Screen.Cursor:=LastCursor;
    if Assigned(WaitFrm) then
    	WaitFrm.Hide;
  end;
end;

function TframeMagB2ValueIHex.GetValueText(Value:integer): String;
begin
  Result := Format('%.*x', [FEditNode.Digits, Value]);
end;

end.
