unit MagB2ValueEditFrame;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,Dialogs,
  StdCtrls, MagB2MyCommunicationClass, ExtCtrls, Buttons, Math,
  NodeClass, MagB2FunctionsUnit, MagB2ValueHeadFrame, FunctionsUnit;

const CarkaPozice:array[1..8] of word=(75,107,139,171,203,235,267,3000);

type
  TValueArray = array[1..8] of word;
  TframeMagB2ValueEdit = class(TframeMagB2Head)
    Panel1: TPanel;
    spbDown1: TSpeedButton;
    spbDown2: TSpeedButton;
    spbDown3: TSpeedButton;
    spbDown4: TSpeedButton;
    spbDown5: TSpeedButton;
    spbDown6: TSpeedButton;
    spbDown7: TSpeedButton;
    spbDown8: TSpeedButton;
    spbUp1: TSpeedButton;
    spbUp2: TSpeedButton;
    spbUp3: TSpeedButton;
    spbUp4: TSpeedButton;
    spbUp5: TSpeedButton;
    spbUp6: TSpeedButton;
    spbUp7: TSpeedButton;
    spbUp8: TSpeedButton;
    lblValue1: TLabel;
    lblValue2: TLabel;
    lblValue3: TLabel;
    lblValue4: TLabel;
    lblValue5: TLabel;
    lblValue6: TLabel;
    lblValue7: TLabel;
    lblValue8: TLabel;
    lblCarka: TLabel;
    Panel2: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    lblMin: TLabel;
    lblMax: TLabel;
    Label3: TLabel;
    Panel3: TPanel;
    lblStatus: TLabel;
    btnWrite: TButton;
    spbUpSign: TSpeedButton;
    spbDownSign: TSpeedButton;
    lblValueSign: TLabel;
    procedure btnWriteClick(Sender: TObject);
    procedure spbUp1Click(Sender: TObject);
    procedure spbUpDownSignClick(Sender: TObject);
  private
    { Private declarations }
  public
    constructor CreateFrame(AOwner: TComponent; MyCommunication : TMyCommunication;
      EditNode : TNode; Language: TLanguage; LanguageIndex: Integer); override;
    procedure FillLabels();
    Procedure IncValue(Sender: TObject);
    procedure FillArray(F:integer);
    function ArrayToInt(pole:TValueArray):integer;
    { Public declarations }
  end;
var
  ValueArray: TValueArray;
  ValueSign: integer;
  frameMagB2ValueEdit: TframeMagB2ValueEdit;
  textPred:string;

implementation

uses MagB2MenuFrm, MagB2GlobalUtilsClass, MagB2TranslationUnit;

{$R *.DFM}

constructor TframeMagB2ValueEdit.CreateFrame(AOwner: TComponent;MyCommunication: TMyCommunication;
  EditNode: TNode; Language: TLanguage; LanguageIndex: Integer);
var FourByte : T4Byte;
    ReadOK:Boolean;
    LastCursor:TCursor;
begin
  inherited;
  lblStatus.Caption:='Unknown';
  LastCursor := Screen.Cursor;
  try
    if Assigned(WaitFrm) then
    	WaitFrm.Show;
    Screen.Cursor:=crHourGlass;
    repeat
      Application.ProcessMessages;
      ReadOK:=ReadValue(FEditNode.ModbusAdr-1,FourByte);
    until(ReadOK)or(MessageDlg(GetTranslationText('READ_ERROR', STR_READ_ERROR),mtError, [mbRetry, mbAbort], 0) = mrAbort);
    if ReadOK then
    begin
      FillArray(Integer(FourByte));
      lblCarka.left:=CarkaPozice[EditNode.Digits];
      lblMin.Caption:=FloatToStr(FEditNode.Min);
      lblMax.Caption:=FloatToStr(FEditNode.Max);
      lblStatus.Caption:='Read OK';
    end else begin
      lblStatus.Caption:='Read failed';
    end;
    if Assigned(WaitFrm) then
    	WaitFrm.gWait.AddProgress(10);
    Application.ProcessMessages;
  finally
    Screen.Cursor := LastCursor;
    if Assigned(WaitFrm) then
    	WaitFrm.Hide();
  end;
end;

Procedure TframeMagB2ValueEdit.IncValue(Sender: TObject);
var value:integer;
begin
  if TSpeedButton(Sender).tag > 0 then
  begin
    value:=TSpeedButton(Sender).tag;
    if ValueArray[value] = 9 then ValueArray[value]:= 0 else inc(ValueArray[value]);
  end
  else
  begin
    value:=TSpeedButton(Sender).tag*(-1);
    if ValueArray[value] = 0 then ValueArray[value]:= 9 else ValueArray[value]:=ValueArray[value]-1;
  end;
  FillLabels();
end;

procedure TframeMagB2ValueEdit.FillArray(F:integer);
var X: integer;
    y, i:integer;
begin
  X:=F;
  ValueSign:= 1;

  if X < 0 then begin
    X:= -X;
    ValueSign:= -1;
  end;

  y:=FEditNode.Decimal+ FEditNode.Digits;
  for i:=1 to 8 do ValueArray[i]:=0;
  for i:= 1 to y do begin
    ValueArray[i]:=trunc(X / Power(10, (y-i)));
    X:=X - trunc(Power(10, (y-i)) * ValueArray[i]);
  end;
  FillLabels;
end;

procedure TframeMagB2ValueEdit.FillLabels();
var I, J, K:integer;
begin
  for J:=0 to componentcount-1 do TControl(components[J]).enabled:=true;
  if ValueSign < 0 then lblValueSign.Caption:= '-' else lblValueSign.Caption:= '+'; 
  for I:=1 to 8 do begin
    for J:=0 to componentcount-1 do if components[J].Tag = 100 + I then Tlabel(components[J]).caption:=IntToStr(ValueArray[I]);
    if (FEditNode.Digits + FEditNode.Decimal) < I then begin
      for K:=0 to componentcount-1 do begin
        if components[K].Tag = 100 + I then Tlabel(components[K]).enabled:=false;
        if components[K].Tag = - I then TSpeedButton(components[K]).enabled:=false;
        if components[K].Tag =   I then TSpeedButton(components[K]).enabled:=false;
      end;
    end;
  end;
end;

procedure TframeMagB2ValueEdit.btnWriteClick(Sender: TObject);
var FourByte : T4Byte;
    Value: Integer;
    WriteOk:Boolean;
    LastCursor: TCursor;
 begin
  Value:=ArrayToInt(ValueArray);
  Value:=ValueSign*Value;
  if (Value > FEditNode.Max*Power(10, FEditNode.Decimal))
    or (Value < FEditNode.Min*Power(10, FEditNode.Decimal))then
    ShowMessage('Number is out of bounds')
  else
  begin
    Integer(FourByte):=Value;
    LastCursor := Screen.Cursor;
    try
      if Assigned(WaitFrm) then
    begin
    	WaitFrm.Show;
        WaitFrm.gWait.Progress:=0;
    end;
      Screen.Cursor:=crHourGlass;

      repeat
        Application.ProcessMessages;
        WriteOK:=WriteValue(FEditNode.ModbusAdr-1,FourByte);
      until (WriteOK)or(MessageDlg(GetTranslationText('WRITE_ERROR', STR_WRITE_ERROR),mtError, [mbRetry, mbAbort], 0) = mrAbort);
      if WriteOK then
      begin
        lblStatus.Caption:='Write OK';
        if( Pos('UNIT_NO', FEditNode.ValueName)>0) then begin
            TformMagB2Menu(Self.Owner).lblSerNo.Caption:=FormatFloat('0000000', Cardinal(FourByte))
        end else begin
          if (Pos('MODBUS_SLAVE_ADDRESS', FEditNode.ValueName)>0)or //Modbus comunication param
      	     (Pos('MODBUS_BAUDRATE', FEditNode.ValueName)>0) or
      	     (Pos('MODBUS_PARITY', FEditNode.ValueName)>0) or
      	     (Pos('CM_PASSWORD_USER', FEditNode.ValueName)>0) or
      	     (Pos('CM_PASSWORD_USER0', FEditNode.ValueName)>0) or
      	     (Pos('CM_PASSWORD_USER1', FEditNode.ValueName)>0) then
          begin
            MessageDlg('You have changed transmitter communication parameters.'#13#10 +
                       'Please setup new communication parameters.',
                       mtWarning,[mbOK],0);
            //PostMessage(formMagB2Menu.Handle, WM_NEED_RESET, 0, 0);
          end;
        end;
      end
      else
        lblStatus.Caption:='Write failed';
      WaitFrm.gWait.AddProgress(10);
      Application.ProcessMessages;
    finally
      Screen.Cursor:=LastCursor;
      if Assigned(WaitFrm) then
    	WaitFrm.Hide;
    end;
  end;
end;


function TframeMagB2ValueEdit.ArrayToInt(pole:TValueArray):integer;
var i,j:integer;
begin
  result:=0;
  j:=(FEditNode.Digits + FEditNode.Decimal);
  for i:= 1 to j do begin
    Result:=Result + Round(Power(10, j-i)) * pole[i];
  end;
end;

procedure TframeMagB2ValueEdit.spbUp1Click(Sender: TObject);
begin
  inherited;
  IncValue(Sender);
end;

procedure TframeMagB2ValueEdit.spbUpDownSignClick(Sender: TObject);
begin
  inherited;
  ValueSign:= -ValueSign;
  FillLabels();
end;

end.
