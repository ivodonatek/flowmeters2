inherited frameMagB2ValuePasw: TframeMagB2ValuePasw
  OnEnter = FrameEnter
  object Panel1: TPanel
    Left = 24
    Top = 40
    Width = 313
    Height = 41
    BevelOuter = bvLowered
    Color = 14670037
    TabOrder = 0
    object eValue: TEdit
      Left = 16
      Top = 9
      Width = 113
      Height = 21
      AutoSelect = False
      MaxLength = 8
      PasswordChar = '*'
      TabOrder = 0
    end
    object btnOK: TButton
      Left = 152
      Top = 8
      Width = 73
      Height = 25
      Caption = 'OK'
      Default = True
      TabOrder = 1
      OnClick = btnConfirmClick
    end
    object btnCancel: TButton
      Left = 224
      Top = 8
      Width = 73
      Height = 25
      Cancel = True
      Caption = 'Cancel'
      TabOrder = 2
      OnClick = btnCancelClick
    end
  end
end
