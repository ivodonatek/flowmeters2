unit MagB2MenuFrm;

interface

uses
  Windows,  filectrl, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, ComCtrls, StdCtrls, NodeClass, MenuNodeClass, MagB2MyCommunicationClass, Spin, Buttons,
  ImgList,IniFiles, MagB2DataClass, TeEngine, TeeFunci, Series, TeeProcs, Chart,
  PBSpinEdit, PBNumEdit, PBSuperSpin, MagB2ValueInfoFrame, MagB2HeadFrm, MagB2ValueEditFrame,
  MagB2Value4WinFrame, MagB2ValueBoolFrame, MagB2ValueISelFrame, MagB2ValuePaswFrame,
  MagB2FunctionsUnit, mxCalendar,WaitFrm, MagB2PassFrm, AdomCore_4_3;

const
  BACKGROUND_FILE_NAME = 'MagB2\Background.jpg';
  ERROR_TEXTS_FILE_NAME = 'MagB2\MagB2_Error.dat';
  ERROR_TEXTS_FILE_NAME_LANG = 'MagB2\MagB2_Error_%s.dat';
  CHART_TOLERANCE = 1.2; // 20 %
  GRAPH_ITEMS_COUNT = 100;
  ERROR_CODE_BYTES_COUNT = 32;
  ZERO_FLOW_NAME = 'ZERO_FLOW';
  ZERO_FLOW_CONST_NAME = 'ZERO_FLOW_CONSTANT';
  CALIB_VALUE_NAMES : Array[1..3] of String = ('CALIBRATION_POINT_ONE','CALIBRATION_POINT_TWO','CALIBRATION_POINT_THREE');
  MEASURE_VALUE_NAMES : Array[1..3] of String = ('MEASURED_POINT_ONE','MEASURED_POINT_TWO','MEASURED_POINT_THREE');
  WM_NEED_RESET = WM_USER + 1;

  GSM_IP_VALUE_ADDRESS = 1022 - 1;

  // xml tagy
  ROOT_TAG_NAME = 'Settings';
  SW_TAG_NAME = 'SW';
  FW_TAG_NAME = 'FW';
  SER_NO_TAG_NAME = 'SerNo';
  USER_SETTINGS_TAG_NAME = 'UserSettings';
  SETTING_TAG_NAME = 'Setting';
  TYPE_TAG_NAME = 'Type';
  MODBUS_ADR_TAG_NAME = 'ModbusAdr';
  MODBUS_ADR1_TAG_NAME = 'ModbusAdr1';
  MODBUS_ADR2_TAG_NAME = 'ModbusAdr2';
  MODBUS_ADR3_TAG_NAME = 'ModbusAdr3';
  MODBUS_ADR4_TAG_NAME = 'ModbusAdr4';
  VALUE_TAG_NAME = 'Value';
  VALUE1_TAG_NAME = 'Value1';
  VALUE2_TAG_NAME = 'Value2';
  VALUE3_TAG_NAME = 'Value3';
  VALUE4_TAG_NAME = 'Value4';

type
  TErrorShapeArray = array[1 .. ERROR_CODE_BYTES_COUNT] of TShape;
  TErrorLabelArray = array[1 .. ERROR_CODE_BYTES_COUNT] of TLabel;
  TIntValues4 = array[1..4] of Integer;
  TformMagB2Menu = class(TformMagB2Head)
    tvMenu: TTreeView;
    pcMenu: TPageControl;
    tsEdit: TTabSheet;
    imgPozadi: TImage;
    pnlMenu: TPanel;
    OpenDialog2: TOpenDialog;
    SaveDialog2: TSaveDialog;
    btnMenuBackSave: TButton;
    btnMenuBackLoad: TButton;
    Splitter1: TSplitter;
    tsTime: TTabSheet;
    imgBack1: TImage;
    lblNowInMemTime: TLabel;
    Panel1: TPanel;
    lblTime: TLabel;
    Image13: TImage;
    Image14: TImage;
    Image16: TImage;
    Image17: TImage;
    SpeedButton1: TSpeedButton;
    SpeedButton2: TSpeedButton;
    SpeedButton15: TSpeedButton;
    SpeedButton16: TSpeedButton;
    Panel4: TPanel;
    lblTimeMem: TLabel;
    btnReadTime: TButton;
    btnWriteTime: TButton;
    tsDate: TTabSheet;
    imgBack2: TImage;
    lblNowInMemDate: TLabel;
    Panel3: TPanel;
    lblDateMem: TLabel;
    btnReadDate: TButton;
    btnGoToday: TButton;
    mxCalendar1: TmxCalendar;
    btnWriteDate: TButton;
    tsOnline: TTabSheet;
    imgBack3: TImage;
    Panel5: TPanel;
    lblFlow: TLabel;
    lblTotal: TLabel;
    lblTotalPlus: TLabel;
    lblOnlineFlow: TLabel;
    lblOnlineTotal: TLabel;
    lblOnlineTotalPlus: TLabel;
    OnlineChart: TChart;
    tsCalibration: TTabSheet;
    imgBack4: TImage;
    Panel8: TPanel;
    btnReadAll: TButton;
    btnWriteAll: TButton;
    btnOpenDataFile: TButton;
    btnSaveDataFile: TButton;
    OnlineTimer: TTimer;
    SaveDialog1: TSaveDialog;
    OpenDialog1: TOpenDialog;
    lblTotalMinus: TLabel;
    lblOnlineTotalMinus: TLabel;
    lblAuxPlus: TLabel;
    lblOnlineAuxPlus: TLabel;
    lblErrorCode: TLabel;
    lblOnlineErrorCode: TLabel;
    cmbOnlineFlowUnit: TComboBox;
    cmbOnlineTotalUnit: TComboBox;
    Series2: TFastLineSeries;
    tsUpdateFirmware: TTabSheet;
    imgBack5: TImage;
    Panel9: TPanel;
    lblUpdateFirmwareFile: TLabel;
    btnUpdateFWFileSelect: TButton;
    btnUpdateFirmware: TButton;
    progUpdateFirmware: TProgressBar;
    lblUpdateFirmwareStatus: TLabel;
    edtUpdateFirmwareFile: TEdit;
    lblUpdateFirmwareComment: TLabel;
    tsGPRS: TTabSheet;
    Panel10: TPanel;
    Label9: TLabel;
    Label10: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    lblGPRS_IPaddress: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    shpGPRS_Status: TShape;
    Label2: TLabel;
    lblGPRS_Status: TLabel;
    btnGPRS_Read: TButton;
    edtGPRS_Gateway: TEdit;
    edtGPRS_User: TEdit;
    edtGPRS_Password: TEdit;
    seGPRS_Port: TSpinEdit;
    seGPRS_PIN: TSpinEdit;
    btnGPRS_Write: TButton;
    imgBack6: TImage;
    tsGSM: TTabSheet;
    imgBack7: TImage;
    Panel11: TPanel;
    Label1: TLabel;
    btnGSMReadNumbers: TButton;
    edtGSMPhone1: TEdit;
    btnGSMWriteNumbers: TButton;
    edtGSMPhone2: TEdit;
    Label3: TLabel;
    edtGSMPhone3: TEdit;
    Label6: TLabel;
    lblGSMNumbersStatus: TLabel;
    Panel12: TPanel;
    lblGSMEventsStatus: TLabel;
    btnGSMReadEvents: TButton;
    btnGSMWriteEvents: TButton;
    GroupBox1: TGroupBox;
    Label12: TLabel;
    cmbGSMEventEmptyPipe: TComboBox;
    Label21: TLabel;
    cmbGSMEventZeroFlow: TComboBox;
    Label22: TLabel;
    cmbGSMEventError: TComboBox;
    GroupBox2: TGroupBox;
    Label13: TLabel;
    Label18: TLabel;
    cmbGSMSendEventEmptyPipe: TComboBox;
    cmbGSMSendEventZeroFlow: TComboBox;
    Label23: TLabel;
    seGSMInterval: TSpinEdit;
    Label11: TLabel;
    Label20: TLabel;
    Label24: TLabel;
    Panel13: TPanel;
    shpRealTimeError1: TShape;
    shpRealTimeError2: TShape;
    shpRealTimeError3: TShape;
    shpRealTimeError4: TShape;
    shpRealTimeError5: TShape;
    shpRealTimeError6: TShape;
    shpRealTimeError7: TShape;
    shpRealTimeError8: TShape;
    shpRealTimeError9: TShape;
    shpRealTimeError10: TShape;
    shpRealTimeError11: TShape;
    shpRealTimeError12: TShape;
    shpRealTimeError13: TShape;
    shpRealTimeError14: TShape;
    shpRealTimeError15: TShape;
    shpRealTimeError16: TShape;
    lblRealTimeError1: TLabel;
    lblRealTimeError5: TLabel;
    lblRealTimeError9: TLabel;
    lblRealTimeError13: TLabel;
    lblRealTimeError2: TLabel;
    lblRealTimeError6: TLabel;
    lblRealTimeError10: TLabel;
    lblRealTimeError14: TLabel;
    lblRealTimeError3: TLabel;
    lblRealTimeError4: TLabel;
    lblRealTimeError7: TLabel;
    lblRealTimeError8: TLabel;
    lblRealTimeError11: TLabel;
    lblRealTimeError12: TLabel;
    lblRealTimeError15: TLabel;
    lblRealTimeError16: TLabel;
    shpRealTimeError17: TShape;
    shpRealTimeError18: TShape;
    shpRealTimeError19: TShape;
    shpRealTimeError20: TShape;
    shpRealTimeError21: TShape;
    shpRealTimeError22: TShape;
    shpRealTimeError23: TShape;
    shpRealTimeError24: TShape;
    shpRealTimeError25: TShape;
    shpRealTimeError26: TShape;
    shpRealTimeError27: TShape;
    shpRealTimeError28: TShape;
    shpRealTimeError29: TShape;
    shpRealTimeError30: TShape;
    shpRealTimeError31: TShape;
    shpRealTimeError32: TShape;
    lblRealTimeError17: TLabel;
    lblRealTimeError18: TLabel;
    lblRealTimeError21: TLabel;
    lblRealTimeError22: TLabel;
    lblRealTimeError23: TLabel;
    lblRealTimeError19: TLabel;
    lblRealTimeError20: TLabel;
    lblRealTimeError24: TLabel;
    lblRealTimeError28: TLabel;
    lblRealTimeError27: TLabel;
    lblRealTimeError26: TLabel;
    lblRealTimeError25: TLabel;
    lblRealTimeError29: TLabel;
    lblRealTimeError30: TLabel;
    lblRealTimeError31: TLabel;
    lblRealTimeError32: TLabel;
    lblErrorTable: TLabel;
    Panel14: TPanel;
    lblFlow2: TLabel;
    lblTotal2: TLabel;
    lblTotalPlus2: TLabel;
    lblOnlineFlow2: TLabel;
    lblOnlineTotal2: TLabel;
    lblOnlineTotalPlus2: TLabel;
    lblTotalMinus2: TLabel;
    lblOnlineTotalMinus2: TLabel;
    lblAuxPlus2: TLabel;
    lblOnlineAuxPlus2: TLabel;
    lblErrorCode2: TLabel;
    lblOnlineErrorCode2: TLabel;
    cmbOnlineFlowUnit2: TComboBox;
    cmbOnlineTotalUnit2: TComboBox;
    OnlineChart2: TChart;
    FastLineSeries1: TFastLineSeries;
    lblAdcRaw: TLabel;
    lblEpipe: TLabel;
    lblOnlineAdcRaw: TLabel;
    lblOnlineEpipe: TLabel;
    lblXTemp: TLabel;
    lblOnlineXTemp: TLabel;
    lblXPress: TLabel;
    lblOnlineXPress: TLabel;
    Panel15: TPanel;
    lblSensorUnitNoTitle: TLabel;
    seSensorUnitNo: TSpinEdit;
    btnWriteSensorUnitNo: TButton;
    seDiameter: TSpinEdit;
    lblDiameter: TLabel;
    btnReadDiameter: TButton;
    btnWriteDiameter: TButton;
    Label25: TLabel;
    cbExcitationEnable: TComboBox;
    btnReadExcitationEnable: TButton;
    btnWriteExcitationEnable: TButton;
    Label26: TLabel;
    cbExcitationCurrent: TComboBox;
    btnReadExcitationCurrent: TButton;
    btnWriteExcitationCurrent: TButton;
    cbExcitationFrequency: TComboBox;
    Label27: TLabel;
    btnReadExcitationFrequency: TButton;
    btnWriteExcitationFrequency: TButton;
    cbServiceMode: TComboBox;
    Label28: TLabel;
    btnReadServiceMode: TButton;
    btnWriteServiceMode: TButton;
    Panel16: TPanel;
    Panel17: TPanel;
    ssFlowPosPoint1: TPBSuperSpin;
    seAdcPosPoint1: TSpinEdit;
    Label29: TLabel;
    Label30: TLabel;
    Label31: TLabel;
    ssFlowPosPoint2: TPBSuperSpin;
    ssFlowNegPoint1: TPBSuperSpin;
    ssFlowNegPoint2: TPBSuperSpin;
    ssFlowZeroPoint: TPBSuperSpin;
    seAdcPosPoint2: TSpinEdit;
    seAdcNegPoint1: TSpinEdit;
    seAdcNegPoint2: TSpinEdit;
    seAdcZeroPoint: TSpinEdit;
    Label32: TLabel;
    Label33: TLabel;
    Label34: TLabel;
    Label35: TLabel;
    Label36: TLabel;
    btnReadPosPoint1: TButton;
    btnReadPosPoint2: TButton;
    btnReadNegPoint1: TButton;
    btnReadNegPoint2: TButton;
    btnReadZeroPoint: TButton;
    btnMeasureZero: TButton;
    btnWritePosPoint1: TButton;
    btnWritePosPoint2: TButton;
    btnWriteNegPoint1: TButton;
    btnWriteNegPoint2: TButton;
    btnWriteZeroPoint: TButton;
    CalDataPointsStatusLabel: TLabel;
    Label38: TLabel;
    ssPosFlowCorrection: TPBSuperSpin;
    ssPosErrLowerCorrection: TPBSuperSpin;
    ssPosErrUpperCorrection: TPBSuperSpin;
    btnWritePosDoErrorCorrection: TButton;
    Label39: TLabel;
    Label40: TLabel;
    Label41: TLabel;
    ssNegFlowCorrection: TPBSuperSpin;
    ssNegErrLowerCorrection: TPBSuperSpin;
    ssNegErrUpperCorrection: TPBSuperSpin;
    btnWriteNegDoErrorCorrection: TButton;
    Label42: TLabel;
    CalTunningStatusLabel: TLabel;
    StatusLabel: TLabel;
    Label45: TLabel;
    Label46: TLabel;
    seMeasureZero: TSpinEdit;
    Panel2: TPanel;
    ServiceStatusLabel: TLabel;
    btnSetAfterCalibration: TButton;
    btnService1: TButton;
    btnService2: TButton;
    btnService3: TButton;
    procedure FormDestroy(Sender: TObject);
    procedure tvMenuChange(Sender: TObject; Node: TTreeNode);
    procedure tvMenuExpanding(Sender: TObject; Node: TTreeNode;
      var AllowExpansion: Boolean);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure btnMenuBackSaveClick(Sender: TObject);
    procedure btnMenuBackLoadClick(Sender: TObject);
    Procedure IncTime( Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure btnReadTimeClick(Sender: TObject);
    procedure btnWriteTimeClick(Sender: TObject);
    procedure btnReadDateClick(Sender: TObject);
    procedure btnWriteDateClick(Sender: TObject);
    procedure btnGoTodayClick(Sender: TObject);
    procedure OnlineTimerTimer(Sender: TObject);
    procedure btnReadAllClick(Sender: TObject);
    procedure btnWriteAllClick(Sender: TObject);
    procedure btnOpenDataFileClick(Sender: TObject);
    procedure btnSaveDataFileClick(Sender: TObject);
    procedure pcMenuChanging(Sender: TObject; var AllowChange: Boolean);
    procedure pcMenuChange(Sender: TObject);
    procedure btnUpdateFWFileSelectClick(Sender: TObject);
    //procedure btnUpdateFirmwareClick(Sender: TObject);
    procedure edtUpdateFirmwareFileChange(Sender: TObject);
    procedure btnGPRS_ReadClick(Sender: TObject);
    procedure btnGPRS_WriteClick(Sender: TObject);
    procedure btnGSMReadNumbersClick(Sender: TObject);
    procedure btnGSMWriteNumbersClick(Sender: TObject);
    procedure edtGSMPhoneChange(Sender: TObject);
    procedure btnGSMReadEventsClick(Sender: TObject);
    procedure btnGSMWriteEventsClick(Sender: TObject);
    procedure btnWriteSensorUnitNoClick(Sender: TObject);
    procedure btnWriteDiameterClick(Sender: TObject);
    procedure btnReadDiameterClick(Sender: TObject);
    procedure btnWriteExcitationEnableClick(Sender: TObject);
    procedure btnReadExcitationEnableClick(Sender: TObject);
    procedure btnWriteExcitationCurrentClick(Sender: TObject);
    procedure btnReadExcitationCurrentClick(Sender: TObject);
    procedure btnWriteExcitationFrequencyClick(Sender: TObject);
    procedure btnReadExcitationFrequencyClick(Sender: TObject);
    procedure btnWriteServiceModeClick(Sender: TObject);
    procedure btnReadServiceModeClick(Sender: TObject);
    procedure btnWritePosPoint1Click(Sender: TObject);
    procedure btnReadPosPoint1Click(Sender: TObject);
    procedure btnWritePosPoint2Click(Sender: TObject);
    procedure btnReadPosPoint2Click(Sender: TObject);
    procedure btnWriteNegPoint1Click(Sender: TObject);
    procedure btnReadNegPoint1Click(Sender: TObject);
    procedure btnWriteNegPoint2Click(Sender: TObject);
    procedure btnReadNegPoint2Click(Sender: TObject);
    procedure btnWriteZeroPointClick(Sender: TObject);
    procedure btnReadZeroPointClick(Sender: TObject);
    procedure btnMeasureZeroClick(Sender: TObject);
    procedure btnWritePosDoErrorCorrectionClick(Sender: TObject);
    procedure btnWriteNegDoErrorCorrectionClick(Sender: TObject);
    procedure btnSetAfterCalibrationClick(Sender: TObject);
    procedure btnService1Click(Sender: TObject);
    procedure btnService2Click(Sender: TObject);
    procedure btnService3Click(Sender: TObject);
  private
    { Private declarations }
    NeedReset: Boolean;
    MenuNodes : TMenuNodes;
    Nodes : TNodes;
    MyCommunication : TMyCommunication;
    {---}
    EditFrame : Tframe;
    OnlineErr:integer;
    time:TmyTime;
    OnlinePocitadlo:Integer;
    TabIndexPred: word;
    Busy:Boolean;
    MinChartValue, MaxChartValue: Double;
    MinChartCnt, MaxChartCnt: Integer;
    ErrorShapeArray: TErrorShapeArray;
    ErrorLabelArray: TErrorLabelArray;
    procedure LoadFromIniFile;
    procedure SaveToIniFile;
    function CreateXmlElement(const DomDocument: TDomDocument; const Node: TNode; const TagName: string; const Values: TIntValues4): TDomElement;
    procedure ParseXmlElement(const DomElement: TDomElement; var Node: TNode; var Values: TIntValues4);
    function GetNode(ModbusAdr: Integer) : TNode;
  public
    { Public declarations }
    procedure WMNeedReset(var Message: TMessage); message WM_NEED_RESET;
    procedure Init(AMyCommunication : TMyCommunication; AfirmwareNO:integer; AProtokolType:TProtokolType);
    procedure TranslateForm;
  end;

var
  //formMagB2Menu: TformMagB2Menu;
  Secure: array [1..4] of Boolean;

const
  ONLINE_FLOW_UNITS = 5;
  ONLINE_FLOW_UNIT_TEXTS: array [0 .. ONLINE_FLOW_UNITS - 1] of String = ('m3/h', 'UKG/min', 'USG/min' , 'l/min', 'l/s');
  ONLINE_FLOW_UNIT_COEFS: array [0 .. ONLINE_FLOW_UNITS - 1] of Double = (1.0, 3.6661541383, 4.4028675393, 16.66666666666, 0.27777777778);
  ONLINE_TOTAL_UNITS = 4;
  ONLINE_TOTAL_UNIT_TEXTS: array[0 .. ONLINE_TOTAL_UNITS - 1] of String = ('m3', 'UKG', 'USG', 'l');
  ONLINE_TOTAL_UNIT_COEFS: array[0 .. ONLINE_TOTAL_UNITS - 1] of Double = (1.0, 219.9692483, 264.17205236, 1000.0);

implementation

uses MagX1Exception, Math, MagB2GlobalUtilsClass, MagB2PassForm, MagB2ValueIBinFrame,
  MagB2ValueIHexFrame, MagB2TranslationUnit, XModem, ShellApi, {ZipMstr,}
  MagB2ValueIHDTFrame;

{$R *.DFM}

procedure TformMagB2Menu.FormCreate(Sender: TObject);
var I: Integer;
    FileName: String;
    SL: TStringList;
begin
  inherited;
  TranslateForm;
  NeedReset := False;

  // Error Labels
  ErrorLabelArray[1] := lblRealTimeError1;
  ErrorLabelArray[2] := lblRealTimeError2;
  ErrorLabelArray[3] := lblRealTimeError3;
  ErrorLabelArray[4] := lblRealTimeError4;
  ErrorLabelArray[5] := lblRealTimeError5;
  ErrorLabelArray[6] := lblRealTimeError6;
  ErrorLabelArray[7] := lblRealTimeError7;
  ErrorLabelArray[8] := lblRealTimeError8;
  ErrorLabelArray[9] := lblRealTimeError9;
  ErrorLabelArray[10] := lblRealTimeError10;
  ErrorLabelArray[11] := lblRealTimeError11;
  ErrorLabelArray[12] := lblRealTimeError12;
  ErrorLabelArray[13] := lblRealTimeError13;
  ErrorLabelArray[14] := lblRealTimeError14;
  ErrorLabelArray[15] := lblRealTimeError15;
  ErrorLabelArray[16] := lblRealTimeError16;
  ErrorLabelArray[17] := lblRealTimeError17;
  ErrorLabelArray[18] := lblRealTimeError18;
  ErrorLabelArray[19] := lblRealTimeError19;
  ErrorLabelArray[20] := lblRealTimeError20;
  ErrorLabelArray[21] := lblRealTimeError21;
  ErrorLabelArray[22] := lblRealTimeError22;
  ErrorLabelArray[23] := lblRealTimeError23;
  ErrorLabelArray[24] := lblRealTimeError24;
  ErrorLabelArray[25] := lblRealTimeError25;
  ErrorLabelArray[26] := lblRealTimeError26;
  ErrorLabelArray[27] := lblRealTimeError27;
  ErrorLabelArray[28] := lblRealTimeError28;
  ErrorLabelArray[29] := lblRealTimeError29;
  ErrorLabelArray[30] := lblRealTimeError30;
  ErrorLabelArray[31] := lblRealTimeError31;
  ErrorLabelArray[32] := lblRealTimeError32;

  // Error Shapes
  ErrorShapeArray[1] := shpRealTimeError1;
  ErrorShapeArray[2] := shpRealTimeError2;
  ErrorShapeArray[3] := shpRealTimeError3;
  ErrorShapeArray[4] := shpRealTimeError4;
  ErrorShapeArray[5] := shpRealTimeError5;
  ErrorShapeArray[6] := shpRealTimeError6;
  ErrorShapeArray[7] := shpRealTimeError7;
  ErrorShapeArray[8] := shpRealTimeError8;
  ErrorShapeArray[9] := shpRealTimeError9;
  ErrorShapeArray[10] := shpRealTimeError10;
  ErrorShapeArray[11] := shpRealTimeError11;
  ErrorShapeArray[12] := shpRealTimeError12;
  ErrorShapeArray[13] := shpRealTimeError13;
  ErrorShapeArray[14] := shpRealTimeError14;
  ErrorShapeArray[15] := shpRealTimeError15;
  ErrorShapeArray[16] := shpRealTimeError16;
  ErrorShapeArray[17] := shpRealTimeError17;
  ErrorShapeArray[18] := shpRealTimeError18;
  ErrorShapeArray[19] := shpRealTimeError19;
  ErrorShapeArray[20] := shpRealTimeError20;
  ErrorShapeArray[21] := shpRealTimeError21;
  ErrorShapeArray[22] := shpRealTimeError22;
  ErrorShapeArray[23] := shpRealTimeError23;
  ErrorShapeArray[24] := shpRealTimeError24;
  ErrorShapeArray[25] := shpRealTimeError25;
  ErrorShapeArray[26] := shpRealTimeError26;
  ErrorShapeArray[27] := shpRealTimeError27;
  ErrorShapeArray[28] := shpRealTimeError28;
  ErrorShapeArray[29] := shpRealTimeError29;
  ErrorShapeArray[30] := shpRealTimeError30;
  ErrorShapeArray[31] := shpRealTimeError31;
  ErrorShapeArray[32] := shpRealTimeError32;

  try
    SL := TStringList.Create();
    try
      FileName := IncludeTrailingBackslash(ExtractFilePath(Application.ExeName)) + Format(ERROR_TEXTS_FILE_NAME_LANG, [GlobalLanguageSection]);
      if not FileExists(FileName) then begin
        FileName := IncludeTrailingBackslash(ExtractFilePath(Application.ExeName)) + ERROR_TEXTS_FILE_NAME;
      end;
      SL.LoadFromFile(FileName);
      for I := 1 to ERROR_CODE_BYTES_COUNT do begin
        if (I <= SL.Count) then begin
          ErrorLabelArray[I].Caption := SL[I-1];
        end;
      end;
    finally
      SL.Free;
    end;
  except
    on E: Exception do begin
      MessageDlg(Format('Loading ErrorCodes from file "%s" failed!'#13#10'%s [%s]', [FileName, E.Message, E.ClassName]), mtError, [mbOK], 0);
    end;
  end;

  pcMenu.ActivePageIndex:=0;

  for I := 1 to 4 do Secure[I] := False;
  StatusLabel.Caption := '';
  CalDataPointsStatusLabel.Caption := '';
  CalTunningStatusLabel.Caption := '';
  ServiceStatusLabel.Caption := '';

  // Excitation enable
  cbExcitationEnable.Items.Clear;
  for I := Low(OffOnList) to High(OffOnList) do begin
    cbExcitationEnable.Items.Add(OffOnList[I]);
  end;

  // Excitation current
  cbExcitationCurrent.Items.Clear;
  for I := Low(ExcitationCurrentList) to High(ExcitationCurrentList) do begin
    cbExcitationCurrent.Items.Add(ExcitationCurrentList[I]);
  end;

  // Excitation frequency
  cbExcitationFrequency.Items.Clear;
  for I := Low(ExcitationFrequencyList) to High(ExcitationFrequencyList) do begin
    cbExcitationFrequency.Items.Add(ExcitationFrequencyList[I]);
  end;

  // Service mode
  cbServiceMode.Items.Clear;
  for I := Low(OffOnList) to High(OffOnList) do begin
    cbServiceMode.Items.Add(OffOnList[I]);
  end;

  //vymaz online graf a nastav 100 nul
  OnlineChart.SeriesList[0].Clear;
  OnlineChart2.SeriesList[0].Clear;
  for I := 0 to GRAPH_ITEMS_COUNT - 1 do begin
    OnlineChart.SeriesList[0].AddXY(I, 0);
    OnlineChart2.SeriesList[0].AddXY(I, 0);    
  end;

  MinChartValue := 0;
  MaxChartValue := 0;
  MinChartCnt := 0;
  MaxChartCnt := 0;
  cmbOnlineFlowUnit.Items.Clear;
  cmbOnlineFlowUnit2.Items.Clear;  
  for I := 0 to ONLINE_FLOW_UNITS do begin
    cmbOnlineFlowUnit.Items.Add(ONLINE_FLOW_UNIT_TEXTS[I]);
    cmbOnlineFlowUnit2.Items.Add(ONLINE_FLOW_UNIT_TEXTS[I]);
  end;
  cmbOnlineFlowUnit.ItemIndex := 0;
  cmbOnlineFlowUnit2.ItemIndex := 0;
  cmbOnlineTotalUnit.Items.Clear;
  cmbOnlineTotalUnit2.Items.Clear;  
  for I := 0 to ONLINE_TOTAL_UNITS do begin
    cmbOnlineTotalUnit.Items.Add(ONLINE_TOTAL_UNIT_TEXTS[I]);
    cmbOnlineTotalUnit2.Items.Add(ONLINE_TOTAL_UNIT_TEXTS[I]);
  end;
  cmbOnlineTotalUnit.ItemIndex := 0;
  cmbOnlineTotalUnit2.ItemIndex := 0;  
  imgPozadi.Picture.LoadFromFile(IncludeTrailingBackslash(ExtractFilePath(Application.ExeName)) + BACKGROUND_FILE_NAME);
  imgBack1.Picture.LoadFromFile(IncludeTrailingBackslash(ExtractFilePath(Application.ExeName)) + BACKGROUND_FILE_NAME);
  imgBack2.Picture.LoadFromFile(IncludeTrailingBackslash(ExtractFilePath(Application.ExeName)) + BACKGROUND_FILE_NAME);
  imgBack3.Picture.LoadFromFile(IncludeTrailingBackslash(ExtractFilePath(Application.ExeName)) + BACKGROUND_FILE_NAME);
  imgBack4.Picture.LoadFromFile(IncludeTrailingBackslash(ExtractFilePath(Application.ExeName)) + BACKGROUND_FILE_NAME);
  imgBack5.Picture.LoadFromFile(IncludeTrailingBackslash(ExtractFilePath(Application.ExeName)) + BACKGROUND_FILE_NAME);
  imgBack6.Picture.LoadFromFile(IncludeTrailingBackslash(ExtractFilePath(Application.ExeName)) + BACKGROUND_FILE_NAME);
  imgBack7.Picture.LoadFromFile(IncludeTrailingBackslash(ExtractFilePath(Application.ExeName)) + BACKGROUND_FILE_NAME);

  // Hide upgrade tab
  tsUpdateFirmware.TabVisible := False;
  tsUpdateFirmware.Visible := False;

  LoadFromIniFile;
end;

procedure TformMagB2Menu.LoadFromIniFile;
var sExePath: String;
    iniF: TIniFile;
begin
  //nacteni velikosti Formu
  sExePath := GetUserAppDataProductPath();
  iniF := TIniFile.Create(sExePath+'MagB2.ini');
  try
    Self.Width:=iniF.ReadInteger('Service','Width',800);
    Self.Height:=iniF.ReadInteger('Service','Height',600);
    if(iniF.ReadBool('Service','Maximized', false) = true) then begin
      Self.WindowState:=wsMaximized
    end else begin
      Self.WindowState:=wsNormal;
    end;
  finally
    iniF.Free;
  end;
end;

procedure TformMagB2Menu.SaveToIniFile;
var sExePath: String;
    iniF: TIniFile;
begin
  sExePath := GetUserAppDataProductPath();
  iniF := TIniFile.Create(sExePath+'MagB2.ini');
  try
    iniF.WriteBool('Service','Maximized', Self.WindowState=wsMaximized);
    if Self.WindowState = wsNormal then begin
      iniF.WriteInteger('Service','Width',Self.Width);
      iniF.WriteInteger('Service','Height',Self.Height);
    end;
  finally
    iniF.Free;
  end;
end;

procedure TformMagB2Menu.FormDestroy(Sender: TObject);
begin
  SaveToIniFile;
  Nodes.Free;
  MenuNodes.Free;
  FreeAndNil(MyCommunication);
  inherited;
end;

procedure TformMagB2Menu.tvMenuChange(Sender: TObject; Node: TTreeNode);
begin
  Self.Enabled:=false;
  try
    if Assigned(Node.Data) then
    begin
      if Assigned(TMenuNode(Node.Data).MenuEdit) then
      begin
        pcMenu.ActivePageIndex:=0;
        if Assigned(EditFrame) then
            FreeAndNil(EditFrame);
        case TNode(TMenuNode(Node.Data).MenuEdit).Typ of
          1 : begin
                EditFrame:=TframeMagB2ValueInfo.CreateFrame(Self,MyCommunication,TNode(TMenuNode(Node.Data).MenuEdit), LocalLanguage, MultiLanguageTextIndex);
                EditFrame.Parent:=tsEdit;
              end;
          2 : begin
                EditFrame:=TframeMagB2ValueEdit.CreateFrame(Self,MyCommunication,TNode(TMenuNode(Node.Data).MenuEdit), LocalLanguage, MultiLanguageTextIndex);
                EditFrame.Parent:=tsEdit;
              end;
          3 : begin
                EditFrame:=TframeMagB2Value4Win.CreateFrame(Self,MyCommunication,TNode(TMenuNode(Node.Data).MenuEdit), LocalLanguage, MultiLanguageTextIndex);
                EditFrame.Parent:=tsEdit;
              end;
          4 : begin
                EditFrame:=TframeMagB2ValueISel.CreateFrame(Self,MyCommunication,TNode(TMenuNode(Node.Data).MenuEdit), LocalLanguage, MultiLanguageTextIndex);
                EditFrame.Parent:=tsEdit;
              end;
          5 : begin
                EditFrame:=TframeMagB2ValueBool.CreateFrame(Self,MyCommunication,TNode(TMenuNode(Node.Data).MenuEdit), LocalLanguage, MultiLanguageTextIndex);
                EditFrame.Parent:=tsEdit;
              end;
          9 : begin
                  EditFrame:=TframeMagB2ValuePasw.CreateFrame(Self,MyCommunication,TNode(TMenuNode(Node.Data).MenuEdit), LocalLanguage, MultiLanguageTextIndex);
                  EditFrame.Parent:=tsEdit;
                end;
          11: begin
                  EditFrame:=TframeMagB2ValueIHex.CreateFrame(Self,MyCommunication,TNode(TMenuNode(Node.Data).MenuEdit), LocalLanguage, MultiLanguageTextIndex);
                  EditFrame.Parent:=tsEdit;
              end;
          12: begin
                  EditFrame:=TframeMagB2ValueIBin.CreateFrame(Self,MyCommunication,TNode(TMenuNode(Node.Data).MenuEdit), LocalLanguage, MultiLanguageTextIndex);
                  EditFrame.Parent:=tsEdit;
              end;
          13: begin
                  EditFrame:=TframeMagB2ValueIHDT.CreateFrame(Self,MyCommunication,TNode(TMenuNode(Node.Data).MenuEdit), LocalLanguage, MultiLanguageTextIndex);
                  EditFrame.Parent:=tsEdit;
              end;
        end;
      end;
    end;
  finally
    Self.Enabled := true;
    tvMenu.SetFocus;
  end;
end;

procedure TformMagB2Menu.tvMenuExpanding(Sender: TObject; Node: TTreeNode;
  var AllowExpansion: Boolean);
var	ReadOK:Boolean;
	FourByte: T4Byte;
begin
    Self.Enabled:=false;
	if (TNode(TMenuNode(Node.Data).MenuEdit).Typ = 9) then	//pokud typ == PASSWD
    begin
      repeat	//cti zda je zadano heslo;
          ReadOK := MyCommunication.ReadMenuValue( TNode(TMenuNode(Node.Data).MenuEdit).ModbusAdr-1, FourByte)
      until(ReadOK)or(MessageDlg(GetTranslationText('READ_ERROR', STR_READ_ERROR),mtError, [mbRetry, mbAbort], 0) = mrAbort);
      if ReadOK then
      begin
         if(Integer(FourByte)=1) then	//pokud heslo OK, povol Expanding menu
         	AllowExpansion:=true
         else
         	AllowExpansion:=false;
      end
      else
          Raise Exception.Create(GetTranslationText('READ_ERROR', STR_READ_ERROR));  //ukonci cteni
    end;
    Self.Enabled:=true;
end;

procedure TformMagB2Menu.FormClose(Sender: TObject; var Action: TCloseAction);
var FourByte: T4Byte;
    I: Integer;
begin
  inherited;
  if (not NeedReset) and (MyCommunication <> nil) then begin
    Integer(FourByte) := 0;
    for I := Low(PassValueName) to High(PassValueName) do begin
      if (Values.IndexOf(PassValueName[I]) >= 0) then begin
        if not MyCommunication.WriteMenuValue(GetAddress(ProtokolType, PassValueName[I], Values), FourByte) then begin
          break; // Do not try again
        end;
      end;
    end;
  end else begin
    NeedReset := False;
  end;

  Action := caFree;
end;

//------------------------------------------------------------------------------

procedure TformMagB2Menu.Init(AMyCommunication : TMyCommunication; AfirmwareNO:integer; AProtokolType:TProtokolType);
var sExePath: string;
	filename: string;
    WaitFrm : TformWait;
begin
  inherited;
  Self.MyCommunication := AMyCommunication;

  if (MyCommunication.ProtokolType = ptDemo) then begin
    Caption := Caption + ' (DEMO)';
    lblDemo.Visible := True;
  end;

  // Tabs
  tsGPRS.TabVisible := False;
  tsGSM.TabVisible := False;

  MenuNodes:=TMenuNodes.Create;
  Nodes:=TNodes.Create;
  Nodes.LanguageIndex:= MultiLanguageTextIndex;
  Nodes.ConvertWideCharsPage:= GetConvertWideCharsPage(LocalLanguage, AfirmwareNO);
  sExePath := ExtractFilePath(Application.ExeName) + 'MagB2\';
  WaitFrm:=TformWait.Create(Self);
  try
    Screen.Cursor:=crHourGlass;
    WaitFrm.Caption:=GetTranslationText('LOAD_PROG_STRUCT', STR_LOAD_PROG_STRUCT);
    WaitFrm.gWait.Progress:=0;
    WaitFrm.Show;
    SetCenter(WaitFrm);
    Application.ProcessMessages;

    filename := Format('Menu%d.dat', [firmwareNO]);
    if FileExists(sExePath+filename) then begin
      Nodes.LoadFromFile(sExePath+filename,nil)
    end else begin
      raise Exception.Create(GetTranslationText('UNSUPPORTED_FW', STR_UNSUPPORTED_FW) + Format(' (%d)', [firmwareNO]));  //ukonci cteni
    end;

    WaitFrm.gWait.Progress:=10;
    Application.ProcessMessages;
    MenuNodes.LoadMenuNodes(Nodes);
    WaitFrm.gWait.Progress:=20;
    Application.ProcessMessages;
    MenuNodes.FillTreeView(tvMenu);
    WaitFrm.gWait.Progress:=30;
    Application.ProcessMessages;
    tvMenu.FullExpand;
    WaitFrm.gWait.Progress:=100;
    Application.ProcessMessages;
    WaitFrm.Close;
  finally
    Screen.Cursor:=crDefault;
    FreeAndNil(WaitFrm);
  end;

  pcMenu.Enabled:=true;
  tvMenu.Enabled:=true;

  case MyCommunication.ProtokolType of
    ptMagX1:
        Raise Exception.Create(GetTranslationText('UNSUPPORTED_COM_PROTOCOL', STR_UNSUPPORTED_COM_PROTOCOL));  //ukonci cteni
    else
    begin
      btnMenuBackSave.Visible:=true;
      btnMenuBackLoad.Visible:=true;
      tvMenu.Align:=alTop;
    end;
  end;
end;

(*
procedure TformMenu.ConnectTimerTimer(Sender: TObject);
var Idx:Integer;
    Data:T4Byte;
begin
  Idx:=Values.IndexOf('UNIT_NO');
  if TMyCommPort(MyCommunication).ReadMenuValue(4*Idx,Data) then
  begin
    if not SerNo=Cardinal(Data) then
    begin
      Label10.Caption:='disconnected';
      Shape2.Brush.Color:=clRed;
      OnlineTimer.Enabled:=false;
//      ConnectTimer.enabled:=true;
      tmrCounting.Enabled:=false;
      pcMenu.Enabled:=false;
      tvMenu.Enabled:=false;
      MyCommunication.Disconnect;
      ShowMessage('Connection error');
    end;
  end
  else if not OnlineTimer.Enabled then
  begin
    Inc(ConnectionErr);
    if ConnectionErr > ConnectionMAX_ERROR then
    begin
      Label10.Caption:='disconnected';
      Shape2.Brush.Color:=clRed;
      OnlineTimer.Enabled:=false;
      tmrCounting.Enabled:=false;
      pcMenu.Enabled:=false;
      tvMenu.Enabled:=false;
      MyCommunication.Disconnect;
      ShowMessage('Connection error');
    end;
  end
end;
*)

function TformMagB2Menu.CreateXmlElement(const DomDocument: TDomDocument; const Node: TNode; const TagName: string; const Values: TIntValues4): TDomElement;
var
  DomText: TDomText;
  ParameterElement: TDomElement;
begin
  Result := TDomElement.Create(DomDocument, TagName);

  ParameterElement := TDomElement.Create(DomDocument, TYPE_TAG_NAME);
  DomText := TDomText.Create(DomDocument);
  DomText.NodeValue := IntToStr(Node.Typ);
  ParameterElement.AppendChild(DomText);
  Result.AppendChild(ParameterElement);

  if Node.Typ = 3 then begin //4Win
    ParameterElement := TDomElement.Create(DomDocument, MODBUS_ADR1_TAG_NAME);
    DomText := TDomText.Create(DomDocument);
    DomText.NodeValue := IntToStr(Node.ModbusAdr - 1);
    ParameterElement.AppendChild(DomText);
    Result.AppendChild(ParameterElement);

    ParameterElement := TDomElement.Create(DomDocument, VALUE1_TAG_NAME);
    DomText := TDomText.Create(DomDocument);
    DomText.NodeValue := IntToStr(Values[1]);
    ParameterElement.AppendChild(DomText);
    Result.AppendChild(ParameterElement);

    ParameterElement := TDomElement.Create(DomDocument, MODBUS_ADR2_TAG_NAME);
    DomText := TDomText.Create(DomDocument);
    DomText.NodeValue := IntToStr(Node.ModbusAdr2 - 1);
    ParameterElement.AppendChild(DomText);
    Result.AppendChild(ParameterElement);

    ParameterElement := TDomElement.Create(DomDocument, VALUE2_TAG_NAME);
    DomText := TDomText.Create(DomDocument);
    DomText.NodeValue := IntToStr(Values[2]);
    ParameterElement.AppendChild(DomText);
    Result.AppendChild(ParameterElement);

    ParameterElement := TDomElement.Create(DomDocument, MODBUS_ADR3_TAG_NAME);
    DomText := TDomText.Create(DomDocument);
    DomText.NodeValue := IntToStr(Node.ModbusAdr3 - 1);
    ParameterElement.AppendChild(DomText);
    Result.AppendChild(ParameterElement);

    ParameterElement := TDomElement.Create(DomDocument, VALUE3_TAG_NAME);
    DomText := TDomText.Create(DomDocument);
    DomText.NodeValue := IntToStr(Values[3]);
    ParameterElement.AppendChild(DomText);
    Result.AppendChild(ParameterElement);

    ParameterElement := TDomElement.Create(DomDocument, MODBUS_ADR4_TAG_NAME);
    DomText := TDomText.Create(DomDocument);
    DomText.NodeValue := IntToStr(Node.ModbusAdr4 - 1);
    ParameterElement.AppendChild(DomText);
    Result.AppendChild(ParameterElement);

    ParameterElement := TDomElement.Create(DomDocument, VALUE4_TAG_NAME);
    DomText := TDomText.Create(DomDocument);
    DomText.NodeValue := IntToStr(Values[4]);
    ParameterElement.AppendChild(DomText);
    Result.AppendChild(ParameterElement);
  end else begin // 2,4 //Edit, Isel
    ParameterElement := TDomElement.Create(DomDocument, MODBUS_ADR_TAG_NAME);
    DomText := TDomText.Create(DomDocument);
    DomText.NodeValue := IntToStr(Node.ModbusAdr - 1);
    ParameterElement.AppendChild(DomText);
    Result.AppendChild(ParameterElement);

    ParameterElement := TDomElement.Create(DomDocument, VALUE_TAG_NAME);
    DomText := TDomText.Create(DomDocument);
    DomText.NodeValue := IntToStr(Values[1]);
    ParameterElement.AppendChild(DomText);
    Result.AppendChild(ParameterElement);
  end;
end;

procedure TformMagB2Menu.ParseXmlElement(const DomElement: TDomElement; var Node: TNode; var Values: TIntValues4);
var
  DomNodeList: TDomNodeList;
  DomText: TDomText;
  ParameterElement: TDomElement;
begin
  DomNodeList := DomElement.GetElementsByTagName(TYPE_TAG_NAME);
  ParameterElement := TDomElement(DomNodeList.Item(0));
  DomText := TDomText(ParameterElement.FirstChild);
  Node.Typ := StrToInt(DomText.Data);

  if Node.Typ = 3 then begin //4Win
    DomNodeList := DomElement.GetElementsByTagName(MODBUS_ADR1_TAG_NAME);
    ParameterElement := TDomElement(DomNodeList.Item(0));
    DomText := TDomText(ParameterElement.FirstChild);
    Node.ModbusAdr := StrToInt(DomText.Data) + 1;

    DomNodeList := DomElement.GetElementsByTagName(VALUE1_TAG_NAME);
    ParameterElement := TDomElement(DomNodeList.Item(0));
    DomText := TDomText(ParameterElement.FirstChild);
    Values[1] := StrToInt(DomText.Data);

    DomNodeList := DomElement.GetElementsByTagName(MODBUS_ADR2_TAG_NAME);
    ParameterElement := TDomElement(DomNodeList.Item(0));
    DomText := TDomText(ParameterElement.FirstChild);
    Node.ModbusAdr2 := StrToInt(DomText.Data) + 1;

    DomNodeList := DomElement.GetElementsByTagName(VALUE2_TAG_NAME);
    ParameterElement := TDomElement(DomNodeList.Item(0));
    DomText := TDomText(ParameterElement.FirstChild);
    Values[2] := StrToInt(DomText.Data);

    DomNodeList := DomElement.GetElementsByTagName(MODBUS_ADR3_TAG_NAME);
    ParameterElement := TDomElement(DomNodeList.Item(0));
    DomText := TDomText(ParameterElement.FirstChild);
    Node.ModbusAdr3 := StrToInt(DomText.Data) + 1;

    DomNodeList := DomElement.GetElementsByTagName(VALUE3_TAG_NAME);
    ParameterElement := TDomElement(DomNodeList.Item(0));
    DomText := TDomText(ParameterElement.FirstChild);
    Values[3] := StrToInt(DomText.Data);

    DomNodeList := DomElement.GetElementsByTagName(MODBUS_ADR4_TAG_NAME);
    ParameterElement := TDomElement(DomNodeList.Item(0));
    DomText := TDomText(ParameterElement.FirstChild);
    Node.ModbusAdr4 := StrToInt(DomText.Data) + 1;

    DomNodeList := DomElement.GetElementsByTagName(VALUE4_TAG_NAME);
    ParameterElement := TDomElement(DomNodeList.Item(0));
    DomText := TDomText(ParameterElement.FirstChild);
    Values[4] := StrToInt(DomText.Data);
  end else begin // 2,4 //Edit, Isel
    DomNodeList := DomElement.GetElementsByTagName(MODBUS_ADR_TAG_NAME);
    ParameterElement := TDomElement(DomNodeList.Item(0));
    DomText := TDomText(ParameterElement.FirstChild);
    Node.ModbusAdr := StrToInt(DomText.Data) + 1;

    DomNodeList := DomElement.GetElementsByTagName(VALUE_TAG_NAME);
    ParameterElement := TDomElement(DomNodeList.Item(0));
    DomText := TDomText(ParameterElement.FirstChild);
    Values[1] := StrToInt(DomText.Data);
  end;
end;

function TformMagB2Menu.GetNode(ModbusAdr: Integer) : TNode;
var
  i: Integer;
  Node: TNode;
begin
  Result := nil;
  for i:=0 to Nodes.Count-1 do begin
    Node := TNode(Nodes.Items[i]);
    if Node.ModbusAdr = ModbusAdr then begin
      Result := Node;
      break;
    end;
  end;
end;

procedure TformMagB2Menu.btnMenuBackSaveClick(Sender: TObject);
var
    i:Integer;
    FourByte:T4Byte;
    ReadOK:Boolean;
    WaitFrm : TformWait;
    Addr:Integer;
    sExePath, sPath: string;
    iniF:TIniFile;
    LastCursor : TCursor;
    DomImplementation : TDomImplementation;
    DomDocument : TDomDocument;
    DomToXMLParser: TDomToXMLParser;
    Stream: TFileStream;
    DomElement: TDomElement;
    DomText: TDomText;
    UserSettingsDomElement: TDomElement;
    IntValues4: TIntValues4;
begin
  inherited;
  Self.Enabled :=false;

  try
    // Password (user)
    Addr:=-1;
    for i:=0 To Nodes.Count-1 do
      if TNode(Nodes.Items[I]).ValueName='CM_PASSWORD_USER' then
      begin
          Addr :=TNode(Nodes.Items[I]).ModbusAdr;
          break;
      end;

    if Addr=-1 then
    	raise Exception.Create(GetTranslationText('INTERNAL_ERROR', STR_INTERNAL_ERROR));

    repeat	//cti zda je zadano heslo;
        ReadOK := MyCommunication.ReadMenuValue( Addr-1 , FourByte)
    until(ReadOK)or(MessageDlg(GetTranslationText('READ_ERROR', STR_READ_ERROR),mtError, [mbRetry, mbAbort], 0) = mrAbort);
    if ReadOK and not(Integer(FourByte)=1) then
    begin
      formMagB2Password:=TformMagB2Password.CreateFrm(Self,MyCommunication, Addr, GetTranslationText('USER_PSW', STR_USER_PSW), LocalLanguage);
      try
      	case formMagB2Password.ShowModal of
        	mrCancel:exit;
      	end;
      finally
      	FreeAndNil(formMagB2Password);
      end;
    end;

    // External Measurements Password (user)
    Addr:=-1;
    for i:=0 To Nodes.Count-1 do
      if TNode(Nodes.Items[I]).ValueName='CM_PASSWORD_EXT_MEAS' then
      begin
          Addr :=TNode(Nodes.Items[I]).ModbusAdr;
          break;
      end;

    if Addr<>-1 then begin
      repeat	//cti zda je zadano heslo;
        ReadOK := MyCommunication.ReadMenuValue( Addr-1 , FourByte)
      until(ReadOK)or(MessageDlg(GetTranslationText('READ_ERROR', STR_READ_ERROR),mtError, [mbRetry, mbAbort], 0) = mrAbort);
      if ReadOK and not(Integer(FourByte)=1) then
      begin
        formMagB2Password:=TformMagB2Password.CreateFrm(Self,MyCommunication, Addr, GetTranslationText('EXT_MEAS_PSW', STR_EXT_MEAS_PSW), LocalLanguage);
        try
      	  case formMagB2Password.ShowModal of
        	mrCancel:exit;
      	  end;
        finally
      	  FreeAndNil(formMagB2Password);
        end;
      end;
    end;

    sExePath := GetUserAppDataProductPath();
    iniF := TIniFile.Create(sExePath+'MagB2.ini');
    try
      sPath := iniF.ReadString('BACKUP', 'SavePath', sExePath);
    finally
      iniF.Free;
    end;

    SaveDialog2.InitialDir:=sPath;
    SaveDialog2.FileName:='MagB2Backup'+lblSerNo.Caption+'.dat';
    if (SaveDialog2.Execute) then
    begin
      WaitFrm:=TformWait.Create(Self);
      LastCursor := Screen.Cursor;
      DomImplementation := TDomImplementation.Create(nil);
      DomDocument := TDomDocument.Create(DomImplementation);

      try
        Screen.Cursor:=crHourGlass;
        WaitFrm.gWait.Progress:=0;
        WaitFrm.Show;
        SetCenter(WaitFrm);
        WaitFrm.gWait.MaxValue:=Nodes.Count-1;
        Application.ProcessMessages;

        DomElement := TDomElement.Create(DomDocument, ROOT_TAG_NAME);
        DomDocument.AppendChild(DomElement);

        DomElement := TDomElement.Create(DomDocument, SW_TAG_NAME);
        DomText := TDomText.Create(DomDocument);
        DomText.NodeValue := lblSW.Caption;
        DomElement.AppendChild(DomText);
        DomDocument.DocumentElement.AppendChild(DomElement);

        DomElement := TDomElement.Create(DomDocument, FW_TAG_NAME);
        DomText := TDomText.Create(DomDocument);
        DomText.NodeValue := lblFW.Caption;
        DomElement.AppendChild(DomText);
        DomDocument.DocumentElement.AppendChild(DomElement);

        DomElement := TDomElement.Create(DomDocument, SER_NO_TAG_NAME);
        DomText := TDomText.Create(DomDocument);
        DomText.NodeValue := lblSerNo.Caption;
        DomElement.AppendChild(DomText);
        DomDocument.DocumentElement.AppendChild(DomElement);

        UserSettingsDomElement := TDomElement.Create(DomDocument, USER_SETTINGS_TAG_NAME);
        DomDocument.DocumentElement.AppendChild(UserSettingsDomElement);

        for i:=0 To Nodes.Count-1 do
        begin
          WaitFrm.gWait.Progress:=i;
          Application.ProcessMessages;
          if (TNode(Nodes.Items[I]).ModbusAdr>2000-1)and(TNode(Nodes.Items[I]).ModbusAdr<3000-1) then
          case (TNode(Nodes.Items[I]).Typ) of
            2,4:          //Edit, Isel
            begin
              {zalohuje se pouze USER Setting bez polozek Password Setup,Modbus Slave Address,Modbus BaudRate,Modbus Parity,Modbus Interframe spacing
                Modbus Response delay,Time - hour,Time - min,Date - day,Date - month,Date - year}
                repeat
                  ReadOK:=MyCommunication.ReadMenuValue(TNode(Nodes.Items[I]).ModbusAdr-1 ,FourByte);
                until(ReadOK)or(MessageDlg(GetTranslationText('READ_ERROR', STR_READ_ERROR),mtError, [mbRetry, mbAbort], 0) = mrAbort);
                if ReadOK then begin
                  IntValues4[1] := Integer(FourByte);
                  UserSettingsDomElement.AppendChild(CreateXmlElement(DomDocument, TNode(Nodes.Items[I]), SETTING_TAG_NAME, IntValues4))
                end else
                  Raise Exception.Create(GetTranslationText('READ_ERROR', STR_READ_ERROR));  //ukonci cteni
            end;
            3:            //4Win
            begin
              repeat
                ReadOK:=MyCommunication.ReadMenuValue(TNode(Nodes.Items[I]).ModbusAdr-1,FourByte);
              until(ReadOK)or(MessageDlg(GetTranslationText('READ_ERROR', STR_READ_ERROR),mtError, [mbRetry, mbAbort], 0) = mrAbort);
              if ReadOK then
                IntValues4[1] := Integer(FourByte)
              else
                Raise Exception.Create(GetTranslationText('READ_ERROR', STR_READ_ERROR));  //ukonci cteni

              repeat
                ReadOK:=MyCommunication.ReadMenuValue(TNode(Nodes.Items[I]).ModbusAdr2-1,FourByte);
              until(ReadOK)or(MessageDlg(GetTranslationText('READ_ERROR', STR_READ_ERROR),mtError, [mbRetry, mbAbort], 0) = mrAbort);
              if ReadOK then
                IntValues4[2] := Integer(FourByte)
              else
                Raise Exception.Create(GetTranslationText('READ_ERROR', STR_READ_ERROR));  //ukonci cteni

              repeat
                ReadOK:=MyCommunication.ReadMenuValue(TNode(Nodes.Items[I]).ModbusAdr3-1,FourByte);
              until(ReadOK)or(MessageDlg(GetTranslationText('READ_ERROR', STR_READ_ERROR),mtError, [mbRetry, mbAbort], 0) = mrAbort);
              if ReadOK then
                IntValues4[3] := Integer(FourByte)
              else
                Raise Exception.Create(GetTranslationText('READ_ERROR', STR_READ_ERROR));  //ukonci cteni

              repeat
                ReadOK:=MyCommunication.ReadMenuValue(TNode(Nodes.Items[I]).ModbusAdr4-1,FourByte);
              until(ReadOK)or(MessageDlg(GetTranslationText('READ_ERROR', STR_READ_ERROR),mtError, [mbRetry, mbAbort], 0) = mrAbort);
              if ReadOK then
                IntValues4[4] := Integer(FourByte)
              else
                Raise Exception.Create(GetTranslationText('READ_ERROR', STR_READ_ERROR));  //ukonci cteni

              UserSettingsDomElement.AppendChild(CreateXmlElement(DomDocument, TNode(Nodes.Items[I]), SETTING_TAG_NAME, IntValues4));
            end;
          end;
        end;

        DomToXMLParser := TDomToXMLParser.Create(nil);
        try
          DomToXMLParser.DomImpl := DomDocument.DomImplementation;
          Stream := TFileStream.Create(SaveDialog2.FileName, fmCreate);
          try
            DomToXMLParser.WriteToStream(DomDocument, 'UTF-8', Stream);
          finally
            Stream.Free;
          end;
        finally
          DomToXMLParser.Free;
        end;

    finally
      FreeAndNil(WaitFrm);
      Screen.Cursor:=LastCursor;
      DomImplementation.Clear;
    end;

    iniF := TIniFile.Create(sExePath+'MagB2.ini');
    try
      iniF.WriteString('BACKUP', 'SavePath', ExtractFilePath(SaveDialog2.FileName) );
    finally
      iniF.Free;
    end;
  end;
  finally
    Self.Enabled :=true;
  end
end;

procedure TformMagB2Menu.btnMenuBackLoadClick(Sender: TObject);
var
    i:Integer;
    FourByte:T4Byte;
    WriteOK:Boolean;
    WaitFrm : TformWait;
    Addr:Integer;
    iniF : TINIFile;
    sExePath, sPath: string;
    LastCursor : TCursor;
    DomImplementation : TDomImplementation;
    DomDocument : TDomDocument;
    XMLToDomParser: TXMLToDomParser;
    DomNodeList: TDomNodeList;
    UserSettingsDomElement: TDomElement;
    DomElement: TDomElement;
    Node, FoundNode: TNode;
    IntValues4: TIntValues4;
begin
  inherited;

  Self.Enabled :=false;
  try
  sExePath := GetUserAppDataProductPath();
  iniF := TIniFile.Create(sExePath+'MagB2.ini');
  try
  	sPath := iniF.ReadString('BACKUP', 'OpenPath', sExePath);
  finally
  	iniF.Free;
  end;
  OpenDialog2.InitialDir:=sPath;

  //cti heslo
  Addr:=-1;
  for i:=0 To Nodes.Count-1 do
    if TNode(Nodes.Items[I]).ValueName='CM_PASSWORD_USER' then
    begin
        Addr :=TNode(Nodes.Items[I]).ModbusAdr;
        break;
    end;

  if Addr=-1 then
      raise Exception.Create(GetTranslationText('INTERNAL_ERROR', STR_INTERNAL_ERROR));

  repeat	//cti zda je zadano heslo;
      WriteOK := MyCommunication.ReadMenuValue( Addr-1 , FourByte)
  until(WriteOK)or(MessageDlg(GetTranslationText('READ_ERROR', STR_READ_ERROR),mtError, [mbRetry, mbAbort], 0) = mrAbort);
  if WriteOK and not(Integer(FourByte)=1) then
  begin
    formMagB2Password:=TformMagB2Password.CreateFrm(Self,MyCommunication, Addr, GetTranslationText('USER_PSW', STR_USER_PSW), LocalLanguage);
    try
      case formMagB2Password.ShowModal of
          mrCancel:exit;
      end;
    finally
      FreeAndNil(formMagB2Password);
    end;
  end;

  //cti heslo (external measurements)
  Addr:=-1;
  for i:=0 To Nodes.Count-1 do
    if TNode(Nodes.Items[I]).ValueName='CM_PASSWORD_EXT_MEAS' then
    begin
        Addr :=TNode(Nodes.Items[I]).ModbusAdr;
        break;
    end;

  if Addr<>-1 then begin
    repeat	//cti zda je zadano heslo;
      WriteOK := MyCommunication.ReadMenuValue( Addr-1 , FourByte)
    until(WriteOK)or(MessageDlg(GetTranslationText('READ_ERROR', STR_READ_ERROR),mtError, [mbRetry, mbAbort], 0) = mrAbort);
    if WriteOK and not(Integer(FourByte)=1) then
    begin
      formMagB2Password:=TformMagB2Password.CreateFrm(Self,MyCommunication, Addr, GetTranslationText('EXT_MEAS_PSW', STR_EXT_MEAS_PSW), LocalLanguage);
      try
        case formMagB2Password.ShowModal of
          mrCancel:exit;
        end;
      finally
        FreeAndNil(formMagB2Password);
      end;
    end;
  end;

  OpenDialog2.FileName:='MagB2Backup'+lblSerNo.Caption+'.dat';
  if  OpenDialog2.Execute then
  begin
    WaitFrm:=TformWait.Create(Self);
    LastCursor:= Screen.Cursor;
    DomImplementation := TDomImplementation.Create(nil);
    try
      Screen.Cursor:=crHourGlass;
      WaitFrm.gWait.Progress:=0;
      WaitFrm.Show;
      SetCenter(WaitFrm);
      Application.ProcessMessages;

      if Length(ExtractFileExt(OpenDialog2.FileName))=0 then
        OpenDialog2.FileName:=ChangeFileExt(OpenDialog2.FileName,'.dat');

      XMLToDomParser := TXMLToDomParser.Create(nil);
      try
        XMLToDomParser.DomImpl := DomImplementation;
        try
          DomDocument := XMLToDomParser.ParseFile(OpenDialog2.FileName, true);

          DomNodeList := DomDocument.DocumentElement.GetElementsByTagName(USER_SETTINGS_TAG_NAME);
          UserSettingsDomElement := TDomElement(DomNodeList.Item(0));

          DomNodeList := UserSettingsDomElement.GetElementsByTagName(SETTING_TAG_NAME);
          WaitFrm.gWait.MaxValue := DomNodeList.Length-1;

          for i:=0 to DomNodeList.Length-1 do begin
            WaitFrm.gWait.Progress:=i;
            Application.ProcessMessages;
            DomElement := TDomElement(DomNodeList.Item(i));
            Node:= TNode.Create;
            try
              ParseXmlElement(DomElement, Node, IntValues4);
              if (Node.ModbusAdr>2000-1)and(Node.ModbusAdr<3000-1) then begin
                case (Node.Typ) of
                  2,4:          //Edit, Isel
                    begin
                      repeat
                        Integer(FourByte):=IntValues4[1];
                        WriteOK:=MyCommunication.WriteMenuValue(Node.ModbusAdr-1,FourByte);
                      until(WriteOK)or(MessageDlg(GetTranslationText('WRITE_ERROR', STR_WRITE_ERROR),mtError, [mbRetry, mbAbort], 0) = mrAbort);
                      if not WriteOK then
                        Raise Exception.Create(GetTranslationText('WRITE_ERROR', STR_WRITE_ERROR));  //ukonci zapisu
                    end;
                  3:            //4Win
                    begin
                      //1.polozka
                      repeat
                        Integer(FourByte):=IntValues4[1];
                        WriteOK:=MyCommunication.WriteMenuValue(Node.ModbusAdr-1,FourByte);
                      until(WriteOK)or(MessageDlg(GetTranslationText('WRITE_ERROR', STR_WRITE_ERROR),mtError, [mbRetry, mbAbort], 0) = mrAbort);
                      if not WriteOK then
                        Raise Exception.Create(GetTranslationText('WRITE_ERROR', STR_WRITE_ERROR));  //ukonci zapisu

                      //2.polozka
                      repeat
                        Integer(FourByte):=IntValues4[2];
                        WriteOK:=MyCommunication.WriteMenuValue(Node.ModbusAdr2-1,FourByte);
                      until(WriteOK)or(MessageDlg(GetTranslationText('WRITE_ERROR', STR_WRITE_ERROR),mtError, [mbRetry, mbAbort], 0) = mrAbort);
                      if not WriteOK then
                        Raise Exception.Create(GetTranslationText('WRITE_ERROR', STR_WRITE_ERROR));  //ukonci zapisu

                      //3.polozka
                      repeat
                        Integer(FourByte):=IntValues4[3];
                        WriteOK:=MyCommunication.WriteMenuValue(Node.ModbusAdr3-1,FourByte);
                      until(WriteOK)or(MessageDlg(GetTranslationText('WRITE_ERROR', STR_WRITE_ERROR),mtError, [mbRetry, mbAbort], 0) = mrAbort);
                      if not WriteOK then
                        Raise Exception.Create(GetTranslationText('WRITE_ERROR', STR_WRITE_ERROR));  //ukonci zapisu

                      //4.polozka
                      repeat
                        Integer(FourByte):=IntValues4[4];
                        WriteOK:=MyCommunication.WriteMenuValue(Node.ModbusAdr4-1,FourByte);
                      until(WriteOK)or(MessageDlg(GetTranslationText('WRITE_ERROR', STR_WRITE_ERROR),mtError, [mbRetry, mbAbort], 0) = mrAbort);
                      if not WriteOK then
                        Raise Exception.Create(GetTranslationText('WRITE_ERROR', STR_WRITE_ERROR));  //ukonci zapisu
                    end;
                end;
              end else
                raise Exception.Create(GetTranslationText('READ_ERROR', STR_READ_ERROR));
            finally
              Node.Free;
            end;
          end;
        except
          MessageDlg('This backup is not valid for this software',mtError, [mbOk], 0);
          exit;
        end;
      finally
        XMLToDomParser.Free;
      end;
    finally
      FreeAndNil(WaitFrm);
      Screen.Cursor:=LastCursor;
      DomImplementation.Clear;
    end;

    iniF := TIniFile.Create(sExePath+'MagB2.ini');
    try
      iniF.WriteString('BACKUP', 'OpenPath', ExtractFilePath(OpenDialog2.FileName) );
    finally
      iniF.Free;
    end;
  end;
  finally
    Self.Enabled := true;
  end;
end;

procedure TformMagB2Menu.IncTime(Sender: TObject);
var
    s:string;
    value:integer;
    direct:boolean;
begin
  if  (time.sec=-1) or (time.min=-1) or (time.hour=-1) then begin
    time.sec:=0;
    time.min:=0;
    time.hour:=0;
  end;
  if TSpeedButton(Sender).tag > 0 then
  begin
    direct:=true;
    value:=TSpeedButton(Sender).tag;
  end else begin
    direct:=false;
    value:=TSpeedButton(Sender).tag*(-1);
  end;


  if direct = true then begin
    case value of
    1: inc(time.sec);
    2: inc(time.min);
    3: inc(time.hour);
    end;
    if time.sec=60 then begin
      time.sec:=0;
      inc(time.min);
    end;
    if time.min=60 then begin
      time.min:=0;
      inc(time.hour);
    end;
    if time.hour=24 then begin
      time.hour:=0;
    end;
  end
  else begin
    case value of
    1: time.sec:=time.sec-1;
    2: time.min:=time.min-1;
    3: time.hour:=time.hour-1;
    end;
    if time.sec=-1 then begin
      time.sec:=59;
      time.min:=time.min-1;
    end;
    if time.min=-1 then begin
      time.min:=59;
      time.hour:=time.hour-1;
    end;
    if time.hour=-1 then begin
      time.hour:=23;
    end;
  end;
  s:=format('%0.2d : %0.2d', [time.hour,time.min]);
  lblTime.Caption :=s;
end;

procedure TformMagB2Menu.SpeedButton3Click(Sender: TObject);
begin
  inherited;
  IncTime(Sender);
end;

procedure TformMagB2Menu.btnReadTimeClick(Sender: TObject);
var Value: Cardinal;
    ReadOK: Boolean;
begin
  if Busy then exit;
  Busy := true;
  Screen.Cursor := crHourGlass;
  try
    repeat
      ReadOK := MyCommunication.ReadRTCTime(Value);
    until (ReadOK) or (MessageDlg(GetTranslationText('READ_ERROR', STR_READ_ERROR),mtError, [mbRetry, mbAbort], 0) = mrAbort);
    if ReadOK then begin
      time := IntegerToMyTime(Value);
      lblTime.Caption :=format('%0.2d : %0.2d', [time.hour,time.min]);
      lblTimeMem.caption:=lblTime.Caption;
    end else begin
      ShowMessage(GetTranslationText('RTC_NOT_AVAIL', STR_RTC_NOT_AVAIL));
    end;
  finally
    Screen.Cursor := crDefault;
    Busy := false;
  end;
end;

procedure TformMagB2Menu.btnWriteTimeClick(Sender: TObject);
var Value: Cardinal;
    WriteOK: Boolean;
begin
  if Busy then exit;
  Busy := True;
  Screen.Cursor := crHourGlass;
  try
    Value := MyTimeToInteger(time);
    repeat
      WriteOK:=MyCommunication.WriteRTCTime(Value);
    until (WriteOK) or (MessageDlg(GetTranslationText('WRITE_ERROR', STR_WRITE_ERROR),mtError, [mbRetry, mbAbort], 0) = mrAbort);
    if WriteOK then begin
      lblTimeMem.Caption := format('%0.2d : %0.2d', [time.hour,time.min]);
    end else begin
      ShowMessage(GetTranslationText('RTC_NOT_AVAIL', STR_RTC_NOT_AVAIL));
    end;
  finally
    Screen.Cursor := crDefault;
    Busy := False;
  end;
end;

procedure TformMagB2Menu.btnReadDateClick(Sender: TObject);
var date: TmyDate;
    Value: Cardinal;
    ReadOK: Boolean;
begin
  if Busy then exit;
  Busy := true;
  Screen.Cursor := crHourGlass;
  try
    repeat
      ReadOK := MyCommunication.ReadRTCDate(Value);
    until (ReadOK) or (MessageDlg(GetTranslationText('READ_ERROR', STR_READ_ERROR),mtError, [mbRetry, mbAbort], 0) = mrAbort);
    if ReadOK then begin
      date := IntegerToMyDate(Value);
      lblDateMem.Caption:=format('%0.2d . %0.2d . %0.2d', [date.day, date.month, date.year]);
      mxCalendar1.Date:=EncodeDate(date.year,date.month,date.day);
    end else begin
      ShowMessage(GetTranslationText('RTC_NOT_AVAIL', STR_RTC_NOT_AVAIL));
    end;
  finally
    Screen.Cursor := crDefault;
    Busy := false;
  end;
end;

procedure TformMagB2Menu.btnWriteDateClick(Sender: TObject);
var date:TmyDate;
    Value: Cardinal;
    WriteOK: Boolean;
begin
  if Busy then exit;
  Busy := True;
  Screen.Cursor := crHourGlass;
  try
    DecodeDate(mxCalendar1.Date, date.year, date.month, date.day);
    Value := MyDateToInteger(date);
    repeat
      WriteOK:=MyCommunication.WriteRTCDate(Value);
    until (WriteOK) or (MessageDlg(GetTranslationText('WRITE_ERROR', STR_WRITE_ERROR),mtError, [mbRetry, mbAbort], 0) = mrAbort);
    if WriteOK then begin
      lblDateMem.Caption:=format('%0.2d . %0.2d . %0.2d', [date.day, date.month, date.year]);
    end else begin
      ShowMessage(GetTranslationText('RTC_NOT_AVAIL', STR_RTC_NOT_AVAIL));
    end;
  finally
    Screen.Cursor := crDefault;
    Busy := False;
  end;
end;

procedure TformMagB2Menu.btnGoTodayClick(Sender: TObject);
begin
  mxCalendar1.Date:=now;
end;

procedure TformMagB2Menu.OnlineTimerTimer(Sender: TObject);
var OnlineData:TOnlineX2;
    OnlineServiceData:TOnlineServiceX2;
    I: Integer;
    IsReadOnlineX2, IsReadOnlineExtX2, IsReadOnlineServiceX2 : Boolean;
begin
  if not Busy then
  try
  Busy := true;
  OnlineTimer.Enabled := False;
  IsReadOnlineX2 := MyCommunication.ReadOnlineX2(OnlineData);
  IsReadOnlineServiceX2 := MyCommunication.ReadOnlineServiceX2(OnlineServiceData);
  if IsReadOnlineX2 then begin
    lblOnlineFlow.Caption :=
      Format('%.3f %s', [ONLINE_FLOW_UNIT_COEFS[cmbOnlineFlowUnit.ItemIndex] * OnlineData.Flow,
                         ONLINE_FLOW_UNIT_TEXTS[cmbOnlineFlowUnit.ItemIndex]]);
    lblOnlineFlow2.Caption :=
      Format('%.3f %s', [ONLINE_FLOW_UNIT_COEFS[cmbOnlineFlowUnit2.ItemIndex] * OnlineData.Flow,
                         ONLINE_FLOW_UNIT_TEXTS[cmbOnlineFlowUnit2.ItemIndex]]);
    lblOnlineTotal.Caption :=
      Format('%.3f %s', [ONLINE_TOTAL_UNIT_COEFS[cmbOnlineTotalUnit.ItemIndex] * OnlineData.Total,
                         ONLINE_TOTAL_UNIT_TEXTS[cmbOnlineTotalUnit.ItemIndex]]);
    lblOnlineTotal2.Caption :=
      Format('%.3f %s', [ONLINE_TOTAL_UNIT_COEFS[cmbOnlineTotalUnit2.ItemIndex] * OnlineData.Total,
                         ONLINE_TOTAL_UNIT_TEXTS[cmbOnlineTotalUnit2.ItemIndex]]);
    lblOnlineTotalPlus.Caption :=
      Format('%.3f %s', [ONLINE_TOTAL_UNIT_COEFS[cmbOnlineTotalUnit.ItemIndex] * OnlineData.TotalPlus,
                         ONLINE_TOTAL_UNIT_TEXTS[cmbOnlineTotalUnit.ItemIndex]]);
    lblOnlineTotalPlus2.Caption :=
      Format('%.3f %s', [ONLINE_TOTAL_UNIT_COEFS[cmbOnlineTotalUnit2.ItemIndex] * OnlineData.TotalPlus,
                         ONLINE_TOTAL_UNIT_TEXTS[cmbOnlineTotalUnit2.ItemIndex]]);
    lblOnlineTotalMinus.Caption :=
      Format('%.3f %s', [ONLINE_TOTAL_UNIT_COEFS[cmbOnlineTotalUnit.ItemIndex] * OnlineData.TotalMinus,
                         ONLINE_TOTAL_UNIT_TEXTS[cmbOnlineTotalUnit.ItemIndex]]);
    lblOnlineTotalMinus2.Caption :=
      Format('%.3f %s', [ONLINE_TOTAL_UNIT_COEFS[cmbOnlineTotalUnit2.ItemIndex] * OnlineData.TotalMinus,
                         ONLINE_TOTAL_UNIT_TEXTS[cmbOnlineTotalUnit2.ItemIndex]]);
    lblOnlineAuxPlus.Caption :=
      Format('%.3f %s', [ONLINE_TOTAL_UNIT_COEFS[cmbOnlineTotalUnit.ItemIndex] * OnlineData.AuxPlus,
                         ONLINE_TOTAL_UNIT_TEXTS[cmbOnlineTotalUnit.ItemIndex]]);
    lblOnlineAuxPlus2.Caption :=
      Format('%.3f %s', [ONLINE_TOTAL_UNIT_COEFS[cmbOnlineTotalUnit2.ItemIndex] * OnlineData.AuxPlus,
                         ONLINE_TOTAL_UNIT_TEXTS[cmbOnlineTotalUnit2.ItemIndex]]);
    //lblOnlineTemp.Caption := Format('%.1f �C', [OnlineData.Temp]);
    //lblOnlineTemp2.Caption := lblOnlineTemp.Caption;
    lblOnlineErrorCode.Caption := CardinalToBinaryStr32(OnlineData.ErrorCode);
    lblOnlineErrorCode2.Caption := lblOnlineErrorCode.Caption;
    lblOnlineXTemp.Caption := Format('%.1f �C', [OnlineData.XTemp]);
    lblXPress.Caption := 'XInput:';
    lblOnlineXPress.Caption := Format('%d', [round(OnlineData.XPress * 1000)]);
    if IsReadOnlineServiceX2 then begin
      lblOnlineAdcRaw.Caption := FloatToStr(OnlineServiceData.AdcRaw);
      lblOnlineEpipe.Caption := FloatToStr(OnlineServiceData.Epipe);
    end;
    for I := 1 to ERROR_CODE_BYTES_COUNT do begin
      if (((OnlineData.ErrorCode) and (1 shl (I - 1))) = (1 shl (I - 1))) then begin
        ErrorShapeArray[I].Brush.Color := clRed;
      end else begin
        ErrorShapeArray[I].Brush.Color := clWhite;
      end;
    end;
    // Chart
    OnlineChart.SeriesList[0].Delete(0);
    OnlineChart2.SeriesList[0].Delete(0);    
    Inc(MaxChartCnt);
    Inc(MinChartCnt);
    if (OnlineData.Flow * CHART_TOLERANCE) > MaxChartValue then begin
      MaxChartValue := OnlineData.Flow * CHART_TOLERANCE;
      MaxChartCnt := 0;
    end;
    if (OnlineData.Flow * CHART_TOLERANCE) < MinChartValue then begin
      MinChartValue := OnlineData.Flow * CHART_TOLERANCE;
      MinChartCnt := 0;
    end;
    OnlineChart.SeriesList[0].AddXY(OnlineChart.SeriesList[0].XValues.Last + 1, OnlineData.Flow);
    OnlineChart2.SeriesList[0].AddXY(OnlineChart2.SeriesList[0].XValues.Last + 1, OnlineData.Flow);    
    if (MinChartCnt > GRAPH_ITEMS_COUNT) or (MaxChartCnt > GRAPH_ITEMS_COUNT) then begin
      MaxChartCnt := 0;
      MinChartCnt := 0;
      MaxChartValue := 0;
      MinChartValue := 0;
      for I := OnlineChart.SeriesList[0].Count - 1 downto 0 do begin
        if (OnlineChart.SeriesList[0].YValue[I] * CHART_TOLERANCE) > MaxChartValue then begin
          MaxChartValue := OnlineChart.SeriesList[0].YValue[I] * CHART_TOLERANCE;
          MaxChartCnt := GRAPH_ITEMS_COUNT - I;
        end;
        if (OnlineChart.SeriesList[0].YValue[I] * CHART_TOLERANCE) < MinChartValue then begin
          MinChartValue := OnlineChart.SeriesList[0].YValue[I] * CHART_TOLERANCE;
          MinChartCnt := GRAPH_ITEMS_COUNT - I;
        end;
      end;
    end;
    OnlineChart.LeftAxis.Minimum := MinChartValue;
    OnlineChart.LeftAxis.Maximum := MaxChartValue;
    OnlineChart.RightAxis.Minimum := MinChartValue;
    OnlineChart.RightAxis.Maximum := MaxChartValue;
    OnlineChart2.LeftAxis.Minimum := MinChartValue;
    OnlineChart2.LeftAxis.Maximum := MaxChartValue;
    OnlineChart2.RightAxis.Minimum := MinChartValue;
    OnlineChart2.RightAxis.Maximum := MaxChartValue;
    // ---
    OnlineErr := 0;
  end else begin
    Inc(OnlineErr);
  end;
  if OnlineErr > MAX_ERROR then begin
    MessageDlg('Communication error', mtError, [mbRetry], 0);
    OnlineErr:=0;
  end;
  OnlineTimer.Enabled := (pcMenu.ActivePage = tsOnline) or (pcMenu.ActivePage = tsCalibration);
  finally
    Busy := false;
  end
end;

procedure TformMagB2Menu.btnReadAllClick(Sender: TObject);
var FourByte: T4Byte;
    WaitFrm: TformWait;
    I: Integer;
    SensorUnitNo, Diameter, ExcitationEnable, ExcitationCurrent, ExcitationFrequency, ServiceMode, Adc: Cardinal;
    Flow, LowerErr, UpperErr: Single;
begin
  if not Busy then
  try
    Busy:=true;
    WaitFrm:=TformWait.Create(Self);
    Screen.Cursor:=crHourGlass;
    WaitFrm.gWait.Progress:=0;
    WaitFrm.Show;
    SetCenter(WaitFrm);
    WaitFrm.caption:=GetTranslationText('PLS_WAIT', STR_PLS_WAIT);
    Application.ProcessMessages;

    StatusLabel.Caption:='';
    CalDataPointsStatusLabel.Caption:='';
    CalTunningStatusLabel.Caption:='';

    try
      // Sensor Unit No
      if seSensorUnitNo.Visible then begin
        if MyCommunication.ReadSensorUnitNo(SensorUnitNo) then begin
          seSensorUnitNo.Value := SensorUnitNo;
        end else begin
          raise Exception.Create(GetTranslationText('READ_ERROR', STR_READ_ERROR));  //ukonci cteni
        end;
      end;
      Application.ProcessMessages;
      // Diameter
      if seDiameter.Visible then begin
        if MyCommunication.ReadDiameter(Diameter) then begin
          seDiameter.Value := Diameter;
        end else begin
          raise Exception.Create(GetTranslationText('READ_ERROR', STR_READ_ERROR));  //ukonci cteni
        end;
      end;
      Application.ProcessMessages;
      // Excitation enable
      if cbExcitationEnable.Visible then begin
        if MyCommunication.ReadExcitationEnable(ExcitationEnable) then begin
          cbExcitationEnable.ItemIndex := ExcitationEnable;
        end else begin
          raise Exception.Create(GetTranslationText('READ_ERROR', STR_READ_ERROR));  //ukonci cteni
        end;
      end;
      Application.ProcessMessages;
      // Excitation current
      if cbExcitationCurrent.Visible then begin
        if MyCommunication.ReadExcitationCurrent(ExcitationCurrent) then begin
          cbExcitationCurrent.ItemIndex := ExcitationCurrent;
        end else begin
          raise Exception.Create(GetTranslationText('READ_ERROR', STR_READ_ERROR));  //ukonci cteni
        end;
      end;
      Application.ProcessMessages;
      // Excitation frequency
      if cbExcitationFrequency.Visible then begin
        if MyCommunication.ReadExcitationFrequency(ExcitationFrequency) then begin
          cbExcitationFrequency.ItemIndex := ExcitationFrequency;
        end else begin
          raise Exception.Create(GetTranslationText('READ_ERROR', STR_READ_ERROR));  //ukonci cteni
        end;
      end;
      Application.ProcessMessages;
      // Service mode
      if cbServiceMode.Visible then begin
        if MyCommunication.ReadServiceMode(ServiceMode) then begin
          cbServiceMode.ItemIndex := ServiceMode;
        end else begin
          raise Exception.Create(GetTranslationText('READ_ERROR', STR_READ_ERROR));  //ukonci cteni
        end;
      end;
      StatusLabel.Caption:=GetTranslationText('READ_SUCCESS', STR_READ_SUCC);
      StatusLabel.Font.Color:=clGreen;
    except
      StatusLabel.Caption:=GetTranslationText('READ_FAILED', STR_READ_FAIL);
      StatusLabel.Font.Color:=clRed;
    end;
    // Calibration data points
    try
      Application.ProcessMessages;
      // Pos point 1
      if ssFlowPosPoint1.Visible then begin
        if MyCommunication.ReadPosPoint1(Flow, Adc) then begin
          ssFlowPosPoint1.Value := Flow;
          seAdcPosPoint1.Value := Adc;
        end else begin
          raise Exception.Create(GetTranslationText('READ_ERROR', STR_READ_ERROR));  //ukonci cteni
        end;
      end;
      Application.ProcessMessages;
      // Pos point 2
      if ssFlowPosPoint2.Visible then begin
        if MyCommunication.ReadPosPoint2(Flow, Adc) then begin
          ssFlowPosPoint2.Value := Flow;
          seAdcPosPoint2.Value := Adc;
        end else begin
          raise Exception.Create(GetTranslationText('READ_ERROR', STR_READ_ERROR));  //ukonci cteni
        end;
      end;
      Application.ProcessMessages;
      // Neg point 1
      if ssFlowNegPoint1.Visible then begin
        if MyCommunication.ReadNegPoint1(Flow, Adc) then begin
          ssFlowNegPoint1.Value := Flow;
          seAdcNegPoint1.Value := Adc;
        end else begin
          raise Exception.Create(GetTranslationText('READ_ERROR', STR_READ_ERROR));  //ukonci cteni
        end;
      end;
      Application.ProcessMessages;
      // Neg point 2
      if ssFlowNegPoint2.Visible then begin
        if MyCommunication.ReadNegPoint2(Flow, Adc) then begin
          ssFlowNegPoint2.Value := Flow;
          seAdcNegPoint2.Value := Adc;
        end else begin
          raise Exception.Create(GetTranslationText('READ_ERROR', STR_READ_ERROR));  //ukonci cteni
        end;
      end;
      Application.ProcessMessages;
      // Zero point
      if ssFlowZeroPoint.Visible then begin
        if MyCommunication.ReadPosPoint1(Flow, Adc) then begin
          ssFlowZeroPoint.Value := Flow;
          seAdcZeroPoint.Value := Adc;
        end else begin
          raise Exception.Create(GetTranslationText('READ_ERROR', STR_READ_ERROR));  //ukonci cteni
        end;
      end;
      Application.ProcessMessages;
      // Measure Zero
      if seMeasureZero.Visible then begin
        if MyCommunication.ReadMeasureZero(Adc) then begin
          seMeasureZero.Value := Adc;
        end else begin
          raise Exception.Create(GetTranslationText('READ_ERROR', STR_READ_ERROR));  //ukonci cteni
        end;
      end;
      CalDataPointsStatusLabel.Caption:=GetTranslationText('READ_SUCCESS', STR_READ_SUCC);
      CalDataPointsStatusLabel.Font.Color:=clGreen;
    except
      CalDataPointsStatusLabel.Caption:=GetTranslationText('READ_FAILED', STR_READ_FAIL);
      CalDataPointsStatusLabel.Font.Color:=clRed;
    end;
    // Calibration tunning
    try
      Application.ProcessMessages;
      // Positive
      if ssPosFlowCorrection.Visible then begin
        if MyCommunication.ReadPosCorrection(Flow, LowerErr, UpperErr) then begin
          ssPosFlowCorrection.Value := Flow;
          ssPosErrLowerCorrection.Value := LowerErr;
          ssPosErrUpperCorrection.Value := UpperErr;
        end else begin
          raise Exception.Create(GetTranslationText('READ_ERROR', STR_READ_ERROR));  //ukonci cteni
        end;
      end;
      Application.ProcessMessages;
      // Negative
      if ssNegFlowCorrection.Visible then begin
        if MyCommunication.ReadNegCorrection(Flow, LowerErr, UpperErr) then begin
          ssNegFlowCorrection.Value := Flow;
          ssNegErrLowerCorrection.Value := LowerErr;
          ssNegErrUpperCorrection.Value := UpperErr;
        end else begin
          raise Exception.Create(GetTranslationText('READ_ERROR', STR_READ_ERROR));  //ukonci cteni
        end;
      end;
      CalTunningStatusLabel.Caption:=GetTranslationText('READ_SUCCESS', STR_READ_SUCC);
      CalTunningStatusLabel.Font.Color:=clGreen;
    except
      CalTunningStatusLabel.Caption:=GetTranslationText('READ_FAILED', STR_READ_FAIL);
      CalTunningStatusLabel.Font.Color:=clRed;
    end;
  finally
    FreeAndNil(WaitFrm);
    Screen.Cursor:=crDefault;
  Busy:=false;
  end;
end;

procedure TformMagB2Menu.btnWriteAllClick(Sender: TObject);
var CalibData, MeasurData, SensorUnitNo, Diameter, ExcitationEnable, ExcitationCurrent, ExcitationFrequency, ServiceMode, Adc: Cardinal;
    Flow, LowerErr, UpperErr: Single;
    WaitFrm : TformWait;
    I: Integer;
begin
  if not Busy then
  try
    Busy:=true;
    WaitFrm:=TformWait.Create(Self);
    Screen.Cursor:=crHourGlass;
    WaitFrm.gWait.Progress:=0;
    WaitFrm.Show;
    SetCenter(WaitFrm);
    WaitFrm.caption:=GetTranslationText('PLS_WAIT', STR_PLS_WAIT);
    Application.ProcessMessages;
    StatusLabel.Caption:='';
    CalDataPointsStatusLabel.Caption:='';
    CalTunningStatusLabel.Caption:='';
    try
      // Sensor Unit No
      if seSensorUnitNo.Visible then begin
        SensorUnitNo := Round(seSensorUnitNo.Value);
        if not MyCommunication.WriteSensorUnitNo(SensorUnitNo) then begin
          raise Exception.Create(GetTranslationText('WRITE_ERROR', STR_WRITE_ERROR));  //ukonci zapis
        end;
      end;
      Application.ProcessMessages;
      // Diameter
      if seDiameter.Visible then begin
        Diameter := Round(seDiameter.Value);
        if not MyCommunication.WriteDiameter(Diameter) then begin
          raise Exception.Create(GetTranslationText('WRITE_ERROR', STR_WRITE_ERROR));  //ukonci zapis
        end;
      end;
      Application.ProcessMessages;
      // Excitation enable
      if cbExcitationEnable.Visible then begin
        ExcitationEnable := cbExcitationEnable.ItemIndex;
        if not MyCommunication.WriteExcitationEnable(ExcitationEnable) then begin
          raise Exception.Create(GetTranslationText('WRITE_ERROR', STR_WRITE_ERROR));  //ukonci zapis
        end;
      end;
      Application.ProcessMessages;
      // Excitation current
      if cbExcitationCurrent.Visible then begin
        ExcitationCurrent := cbExcitationCurrent.ItemIndex;
        if not MyCommunication.WriteExcitationCurrent(ExcitationCurrent) then begin
          raise Exception.Create(GetTranslationText('WRITE_ERROR', STR_WRITE_ERROR));  //ukonci zapis
        end;
      end;
      Application.ProcessMessages;
      // Excitation frequency
      if cbExcitationFrequency.Visible then begin
        ExcitationFrequency := cbExcitationFrequency.ItemIndex;
        if not MyCommunication.WriteExcitationFrequency(ExcitationFrequency) then begin
          raise Exception.Create(GetTranslationText('WRITE_ERROR', STR_WRITE_ERROR));  //ukonci zapis
        end;
      end;
      Application.ProcessMessages;
      // Service mode
      if cbServiceMode.Visible then begin
        ServiceMode := cbServiceMode.ItemIndex;
        if not MyCommunication.WriteServiceMode(ServiceMode) then begin
          raise Exception.Create(GetTranslationText('WRITE_ERROR', STR_WRITE_ERROR));  //ukonci zapis
        end;
      end;
      StatusLabel.Caption := GetTranslationText('WRITE_SUCCESS', STR_WRITE_SUCC);
      StatusLabel.Font.Color:=clGreen;
    except
      StatusLabel.Caption := GetTranslationText('WRITE_FAILED', STR_WRITE_FAIL);
      StatusLabel.Font.Color:=clRed;
    end;
    // Calibration data points
    try
      Application.ProcessMessages;
      // Pos point 1
      if ssFlowPosPoint1.Visible then begin
        Flow := ssFlowPosPoint1.Value;
        Adc := seAdcPosPoint1.Value;
        if not MyCommunication.WritePosPoint1(Flow, Adc) then begin
          raise Exception.Create(GetTranslationText('WRITE_ERROR', STR_WRITE_ERROR));  //ukonci zapis
        end;
      end;
      Application.ProcessMessages;
      // Pos point 2
      if ssFlowPosPoint2.Visible then begin
        Flow := ssFlowPosPoint2.Value;
        Adc := seAdcPosPoint2.Value;
        if not MyCommunication.WritePosPoint2(Flow, Adc) then begin
          raise Exception.Create(GetTranslationText('WRITE_ERROR', STR_WRITE_ERROR));  //ukonci zapis
        end;
      end;
      Application.ProcessMessages;
      // Neg point 1
      if ssFlowNegPoint1.Visible then begin
        Flow := ssFlowNegPoint1.Value;
        Adc := seAdcNegPoint1.Value;
        if not MyCommunication.WriteNegPoint1(Flow, Adc) then begin
          raise Exception.Create(GetTranslationText('WRITE_ERROR', STR_WRITE_ERROR));  //ukonci zapis
        end;
      end;
      Application.ProcessMessages;
      // Neg point 2
      if ssFlowNegPoint2.Visible then begin
        Flow := ssFlowNegPoint2.Value;
        Adc := seAdcNegPoint2.Value;
        if not MyCommunication.WriteNegPoint2(Flow, Adc) then begin
          raise Exception.Create(GetTranslationText('WRITE_ERROR', STR_WRITE_ERROR));  //ukonci zapis
        end;
      end;
      Application.ProcessMessages;
      // Zero point
      if ssFlowZeroPoint.Visible then begin
        Flow := ssFlowZeroPoint.Value;
        Adc := seAdcZeroPoint.Value;
        if not MyCommunication.WritePosPoint1(Flow, Adc) then begin
          raise Exception.Create(GetTranslationText('WRITE_ERROR', STR_WRITE_ERROR));  //ukonci zapis
        end;
      end;
      Application.ProcessMessages;
      // Measure Zero
      if seMeasureZero.Visible then begin
        Adc := seMeasureZero.Value;
        if not MyCommunication.WriteMeasureZero(Adc) then begin
          raise Exception.Create(GetTranslationText('WRITE_ERROR', STR_WRITE_ERROR));  //ukonci zapis
        end;
      end;
      CalDataPointsStatusLabel.Caption := GetTranslationText('WRITE_SUCCESS', STR_WRITE_SUCC);
      CalDataPointsStatusLabel.Font.Color:=clGreen;
    except
      CalDataPointsStatusLabel.Caption := GetTranslationText('WRITE_FAILED', STR_WRITE_FAIL);
      CalDataPointsStatusLabel.Font.Color:=clRed;
    end;
    // Calibration tunning
    try
      Application.ProcessMessages;
      // Positive
      if ssPosFlowCorrection.Visible then begin
        Flow := ssPosFlowCorrection.Value;
        LowerErr := ssPosErrLowerCorrection.Value;
        UpperErr := ssPosErrUpperCorrection.Value;
        if not MyCommunication.WritePosCorrection(Flow, LowerErr, UpperErr) then begin
          raise Exception.Create(GetTranslationText('WRITE_ERROR', STR_WRITE_ERROR));  //ukonci zapis
        end;
      end;
      Application.ProcessMessages;
      // Negative
      if ssNegFlowCorrection.Visible then begin
        Flow := ssNegFlowCorrection.Value;
        LowerErr := ssNegErrLowerCorrection.Value;
        UpperErr := ssNegErrUpperCorrection.Value;
        if not MyCommunication.WriteNegCorrection(Flow, LowerErr, UpperErr) then begin
          raise Exception.Create(GetTranslationText('WRITE_ERROR', STR_WRITE_ERROR));  //ukonci zapis
        end;
      end;
      CalTunningStatusLabel.Caption := GetTranslationText('WRITE_SUCCESS', STR_WRITE_SUCC);
      CalTunningStatusLabel.Font.Color:=clGreen;
    except
      CalTunningStatusLabel.Caption := GetTranslationText('WRITE_FAILED', STR_WRITE_FAIL);
      CalTunningStatusLabel.Font.Color:=clRed;
    end;
  finally
    FreeAndNil(WaitFrm);
    Screen.Cursor:=crDefault;
    Busy:=false;
  end;
end;

procedure TformMagB2Menu.btnOpenDataFileClick(Sender: TObject);
var iniF : TINIFile;
	sExePath, sPath, tmp:string;
begin
  sExePath := GetUserAppDataProductPath();
  iniF := TIniFile.Create(sExePath+'MagB2.ini');
  try
  	sPath := iniF.ReadString('CALIBRATION', 'OpenPath', sExePath);
  finally
  	iniF.Free;
  end;

  OpenDialog1.InitialDir:=sPath;
  if OpenDialog1.Execute then
  begin
    iniF:=TINIFile.Create(OpenDialog1.FileName);
    try
      tmp:=iniF.ReadString('CALIBRATION','SensorUnitNo','0');
      seSensorUnitNo.Value:=StrToInt(tmp);

      tmp:=iniF.ReadString('CALIBRATION','Diameter','0');
      seDiameter.Value:=StrToInt(tmp);

      tmp:=iniF.ReadString('CALIBRATION','ExcitationEnable','0');
      cbExcitationEnable.ItemIndex:=StrToInt(tmp);

      tmp:=iniF.ReadString('CALIBRATION','ExcitationCurrent','0');
      cbExcitationCurrent.ItemIndex:=StrToInt(tmp);

      tmp:=iniF.ReadString('CALIBRATION','ExcitationFrequency','0');
      cbExcitationFrequency.ItemIndex:=StrToInt(tmp);

      tmp:=iniF.ReadString('CALIBRATION','ServiceMode','0');
      cbServiceMode.ItemIndex:=StrToInt(tmp);

      tmp:=iniF.ReadString('CALIBRATION','FlowPosPoint1','0');
      ssFlowPosPoint1.Value:=StrToFloat(tmp);

      tmp:=iniF.ReadString('CALIBRATION','AdcPosPoint1','0');
      seAdcPosPoint1.Value:=StrToInt(tmp);

      tmp:=iniF.ReadString('CALIBRATION','FlowPosPoint2','0');
      ssFlowPosPoint2.Value:=StrToFloat(tmp);

      tmp:=iniF.ReadString('CALIBRATION','AdcPosPoint2','0');
      seAdcPosPoint2.Value:=StrToInt(tmp);

      tmp:=iniF.ReadString('CALIBRATION','FlowNegPoint1','0');
      ssFlowNegPoint1.Value:=StrToFloat(tmp);

      tmp:=iniF.ReadString('CALIBRATION','AdcNegPoint1','0');
      seAdcNegPoint1.Value:=StrToInt(tmp);

      tmp:=iniF.ReadString('CALIBRATION','FlowNegPoint2','0');
      ssFlowNegPoint2.Value:=StrToFloat(tmp);

      tmp:=iniF.ReadString('CALIBRATION','AdcNegPoint2','0');
      seAdcNegPoint2.Value:=StrToInt(tmp);

      tmp:=iniF.ReadString('CALIBRATION','FlowZeroPoint','0');
      ssFlowZeroPoint.Value:=StrToFloat(tmp);

      tmp:=iniF.ReadString('CALIBRATION','AdcZeroPoint','0');
      seAdcZeroPoint.Value:=StrToInt(tmp);
    finally
     iniF.Free;
    end;

    iniF := TIniFile.Create(sExePath+'MagB2.ini');
    try
      iniF.WriteString('CALIBRATION', 'OpenPath', ExtractFilePath(OpenDialog1.FileName) );
    finally
      iniF.Free;
    end;
  end;
end;

procedure TformMagB2Menu.btnSaveDataFileClick(Sender: TObject);
var iniF : TINIFile;
    R:double;
    I:integer;
    sExePath, sPath: string;
begin
  sExePath := GetUserAppDataProductPath();
  iniF := TIniFile.Create(sExePath+'MagB2.ini');
  try
  	sPath := iniF.ReadString('CALIBRATION', 'SavePath', sExePath);
  finally
  	iniF.Free;
  end;

  SaveDialog1.FileName:='Calibration'+lblSerNo.Caption+'.ini';
  SaveDialog1.InitialDir:=sPath;
  if SaveDialog1.Execute then
  begin
    if Length(ExtractFileExt(SaveDialog1.FileName))=0 then
      ChangeFileExt(SaveDialog1.FileName,'ini');
    iniF:=TINIFile.Create(SaveDialog1.FileName);
    try
      I := seSensorUnitNo.Value;
      iniF.WriteString('CALIBRATION','SensorUnitNo',IntToStr(I));

      I := seDiameter.Value;
      iniF.WriteString('CALIBRATION','Diameter',IntToStr(I));

      I := cbExcitationEnable.ItemIndex;
      iniF.WriteString('CALIBRATION','ExcitationEnable',IntToStr(I));

      I := cbExcitationCurrent.ItemIndex;
      iniF.WriteString('CALIBRATION','ExcitationCurrent',IntToStr(I));

      I := cbExcitationFrequency.ItemIndex;
      iniF.WriteString('CALIBRATION','ExcitationFrequency',IntToStr(I));

      I := cbServiceMode.ItemIndex;
      iniF.WriteString('CALIBRATION','ServiceMode',IntToStr(I));

      R := ssFlowPosPoint1.Value;
      iniF.WriteString('CALIBRATION','FlowPosPoint1',FloatToStr(R));

      I := seAdcPosPoint1.Value;
      iniF.WriteString('CALIBRATION','AdcPosPoint1',IntToStr(I));

      R := ssFlowPosPoint2.Value;
      iniF.WriteString('CALIBRATION','FlowPosPoint2',FloatToStr(R));

      I := seAdcPosPoint2.Value;
      iniF.WriteString('CALIBRATION','AdcPosPoint2',IntToStr(I));

      R := ssFlowNegPoint1.Value;
      iniF.WriteString('CALIBRATION','FlowNegPoint1',FloatToStr(R));

      I := seAdcNegPoint1.Value;
      iniF.WriteString('CALIBRATION','AdcNegPoint1',IntToStr(I));

      R := ssFlowNegPoint2.Value;
      iniF.WriteString('CALIBRATION','FlowNegPoint2',FloatToStr(R));

      I := seAdcNegPoint2.Value;
      iniF.WriteString('CALIBRATION','AdcNegPoint2',IntToStr(I));

      R := ssFlowZeroPoint.Value;
      iniF.WriteString('CALIBRATION','FlowZeroPoint',FloatToStr(R));

      I := seAdcZeroPoint.Value;
      iniF.WriteString('CALIBRATION','AdcZeroPoint',IntToStr(I));
    finally
     iniF.Free;
    end;

    iniF := TIniFile.Create(sExePath+'MagB2.ini');
    try
      iniF.WriteString('CALIBRATION', 'SavePath', ExtractFilePath(SaveDialog1.FileName) );
    finally
      iniF.Free;
    end;
  end;
end;

procedure TformMagB2Menu.pcMenuChanging(Sender: TObject;
  var AllowChange: Boolean);
begin
//pokud je aktivni online tak zastav
  if (pcMenu.ActivePage=tsOnline) or (pcMenu.ActivePage=tsCalibration) then
  begin
    if(MyCommunication is TMyCommPort) then
    begin
      OnlineTimer.enabled:=false;
      TMyCommPort(MyCommunication).WriteOnline(0);
      OnlinePocitadlo:=0;
    end
    else if MyCommunication is TMyModbus then
    begin
      OnlineTimer.enabled:=false;
    end;
  end;
  TabIndexPred:=TPageControl(sender).ActivePageIndex;
end;

procedure TformMagB2Menu.pcMenuChange(Sender: TObject);
var PassForm:TformMagB2Pass;
    i:Integer;
begin
  //Online
  if (pcMenu.ActivePage=tsOnline)then
  begin
    OnlinePocitadlo:=1;
    OnlineTimer.enabled:=true;

    MinChartCnt := GRAPH_ITEMS_COUNT;
    MaxChartCnt := GRAPH_ITEMS_COUNT;

    OnlineChart.SeriesList[0].Clear;
    OnlineChart2.SeriesList[0].Clear;
    for I := 0 to GRAPH_ITEMS_COUNT - 1 do begin
      OnlineChart.SeriesList[0].AddXY(I, 0);
      OnlineChart2.SeriesList[0].AddXY(I, 0);
    end;
    if(MyCommunication is TMyCommPort)then
    begin
      TMyCommPort(MyCommunication).FlushBuffers(True,False);
      TMyCommPort(MyCommunication).WriteOnline(1);
    end;
  end
  //Kalibrace
  else if pcMenu.ActivePage=tsCalibration then
  begin
    ssFlowPosPoint1.SetFocus;
    {if firmwareNO<107 then      //podle firmware zobraz kalibraci se zadavanim prutoku
    begin
      Panel6.Visible:=false;
      btnWriteAll.Visible:=false;
    end
    else}
    begin
      btnWriteAll.Visible:=true;
    end;

    if secure[3] then   //pokud bylo zadano heslo cti vsechno
    begin
      btnReadAll.Click;
      OnlinePocitadlo:=1;
      OnlineTimer.enabled:=true;
      
      MinChartCnt := GRAPH_ITEMS_COUNT;
      MaxChartCnt := GRAPH_ITEMS_COUNT;

      OnlineChart.SeriesList[0].Clear;
      OnlineChart2.SeriesList[0].Clear;
      for I := 0 to GRAPH_ITEMS_COUNT - 1 do begin
        OnlineChart.SeriesList[0].AddXY(I, 0);
        OnlineChart2.SeriesList[0].AddXY(I, 0);
      end;
      if(MyCommunication is TMyCommPort)then
      begin
        TMyCommPort(MyCommunication).FlushBuffers(True,False);
        TMyCommPort(MyCommunication).WriteOnline(1);
      end;
    end
    else
    begin               //jinak zobraz okno hesla
      PassForm:=TformMagB2Pass.CreateFrm(self,3,MyCommunication,Values,ProtokolType,LocalLanguage);
      SetCenter(PassForm);
      case PassForm.Showmodal of
        mrOk:
        begin
          Secure[3]:=true;
          pcMenuChange(Sender);
        end;
        mrCancel:
        begin
         pcMenu.ActivePageIndex:=TabIndexPred;
         pcMenuChange(Sender);
        end;
      end;
    end;
  end
  //Firmware update
  else if pcMenu.ActivePage=tsUpdateFirmware then
  begin
    if secure[3] then   //pokud bylo zadano heslo cti vsechno
    begin
      // OK - Continue
    end
    else
    begin               //jinak zobraz okno hesla
      PassForm:=TformMagB2Pass.CreateFrm(self,3,MyCommunication,Values,ProtokolType,LocalLanguage);
      SetCenter(PassForm);
      case PassForm.Showmodal of
        mrOk:
        begin
          Secure[3]:=true;
          pcMenuChange(Sender);
        end;
        mrCancel:
        begin
         pcMenu.ActivePageIndex:=TabIndexPred;
         pcMenuChange(Sender);
        end;
      end;
    end;
  end
  //Time, Date
  else if (pcMenu.ActivePage=tsDate)or(pcMenu.ActivePage=tsTime) then
  begin
    if (Secure[2]) then begin
    end else begin
      //zabrazeni dialogu hesla
      PassForm:=TformMagB2Pass.CreateFrm(self,2,MyCommunication,Values,ProtokolType,LocalLanguage);
      SetCenter(PassForm);
      case PassForm.Showmodal of
        mrCancel:
        begin
          pcMenu.ActivePageIndex:=TabIndexPred;
          pcMenuChange(Sender);
        end;
        mrOK:
        begin
          Secure[2]:=true;
          pcMenuChange(Sender);
        end
      end;
    end;
  end
  //GPRS, SMS
  else if (pcMenu.ActivePage=tsGPRS)or(pcMenu.ActivePage=tsGSM) then
  begin
    if Secure[1]then begin
      if (pcMenu.ActivePage=tsGPRS) then begin
        btnGPRS_Read.Click;
      end;
      if (pcMenu.ActivePage=tsGSM) then begin
        btnGSMReadNumbers.Click;
        btnGSMReadEvents.Click;
      end;
    end else begin
      //zabrazeni dialogu hesla
      PassForm:=TformMagB2Pass.CreateFrm(self,1,MyCommunication,Values,ProtokolType,LocalLanguage);
      SetCenter(PassForm);
      case PassForm.Showmodal of
        mrCancel:
        begin
          pcMenu.ActivePageIndex:=TabIndexPred;
          pcMenuChange(Sender);
        end;
        mrOK:
        begin
          Secure[1]:=true;
          pcMenuChange(Sender);
        end
      end;
    end;
  end;
end;

procedure TformMagB2Menu.TranslateForm;
begin
  // Page control
  tsEdit.Caption := GetTranslationText('MENU', tsEdit.Caption);
  tsTime.Caption := GetTranslationText('TIME', tsTime.Caption);
  tsDate.Caption := GetTranslationText('DATE', tsDate.Caption);
  tsOnline.Caption := GetTranslationText('REAL_TIME', tsOnline.Caption);
  tsCalibration.Caption := GetTranslationText('CALIB', tsCalibration.Caption);
  tsUpdateFirmware.Caption := GetTranslationText('FW_UPDATE', tsUpdateFirmware.Caption);
  btnMenuBackLoad.Caption := GetTranslationText('LOAD_BCK_FILE', btnMenuBackLoad.Caption);
  btnMenuBackSave.Caption := GetTranslationText('SAVE_BCK_FILE', btnMenuBackSave.Caption);
  // Date/Time
  btnReadTime.Caption := GetTranslationText('READ_TIME', btnReadTime.Caption);
  btnWriteTime.Caption := GetTranslationText('WRITE_TIME', btnWriteTime.Caption);
  btnReadDate.Caption := GetTranslationText('READ_DATE', btnReadDate.Caption);
  btnWriteDate.Caption := GetTranslationText('WRITE_DATE', btnWriteDate.Caption);
  btnGoToday.Caption := GetTranslationText('GO_TODAY', btnGoToday.Caption);
  lblNowInMemTime.Caption := GetTranslationText('NOW_IN_MEM', lblNowInMemTime.Caption);
  lblNowInMemDate.Caption := GetTranslationText('NOW_IN_MEM', lblNowInMemDate.Caption);
  lblTimeMem.Caption := GetTranslationText('UNKNOWN', lblTimeMem.Caption);
  lblDateMem.Caption := GetTranslationText('UNKNOWN', lblDateMem.Caption);
  // Online
  lblFlow.Caption := GetTranslationText('FLOW', lblFlow.Caption);
  lblTotalPlus.Caption := GetTranslationText('TOTAL_PLUS', lblTotalPlus.Caption);
  lblTotalMinus.Caption := GetTranslationText('TOTAL_MINUS', lblTotalMinus.Caption);
  lblTotal.Caption := GetTranslationText('TOTAL', lblTotal.Caption);
  lblAuxPlus.Caption := GetTranslationText('AUX_PLUS', lblAuxPlus.Caption);
  lblErrorCode.Caption := GetTranslationText('ERROR_CODE', lblErrorCode.Caption);
  OnlineChart.Title.Text.Text := GetTranslationText('ACTUAL_FLOW', OnlineChart.Title.Text.Text);
  OnlineChart.RightAxis.Title.Caption := GetTranslationText('FLOW_UNIT', OnlineChart.RightAxis.Title.Caption);
  // Calibration
  //lblCalibrationData.Caption := GetTranslationText('CALIB_DATA', lblCalibrationData.Caption);
  //lblUnit.Caption := GetTranslationText('UNIT', lblUnit.Caption);
  //btnWriteCalData1.Caption := GetTranslationText('CALIB_WRITE_DATA1', btnWriteCalData1.Caption);
  //btnWriteCalData2.Caption := GetTranslationText('CALIB_WRITE_DATA2', btnWriteCalData2.Caption);
  //btnWriteCalData3.Caption := GetTranslationText('CALIB_WRITE_DATA3', btnWriteCalData3.Caption);
  //btnAutomaticZeroConst.Caption := GetTranslationText('AUTOMATIC_ZERO_CONST', btnAutomaticZeroConst.Caption);
  //btnManualZeroConst.Caption := GetTranslationText('MANUAL_ZERO_CONST', btnManualZeroConst.Caption);
  //lblMeasurementData.Caption := GetTranslationText('MEAS_DATA', lblMeasurementData.Caption);
  //lblError.Caption := GetTranslationText('ERROR_PROC', lblError.Caption);
  //btnWriteMeasData1.Caption := GetTranslationText('MEAS_WRITE_DATA1', btnWriteMeasData1.Caption);
  //btnWriteMeasData2.Caption := GetTranslationText('MEAS_WRITE_DATA2', btnWriteMeasData2.Caption);
  //btnWriteMeasData3.Caption := GetTranslationText('MEAS_WRITE_DATA3', btnWriteMeasData3.Caption);
  //btnMeasurementCalculate.Caption := GetTranslationText('CALC_MEAS_POINT', btnMeasurementCalculate.Caption);
  btnReadAll.Caption := GetTranslationText('READ_ALL', btnReadAll.Caption);
  btnWriteAll.Caption := GetTranslationText('WRITE_ALL', btnWriteAll.Caption);
  btnOpenDataFile.Caption := GetTranslationText('OPEN_DATA', btnOpenDataFile.Caption);
  btnSaveDataFile.Caption := GetTranslationText('SAVE_DATA', btnSaveDataFile.Caption);
  lblFlow2.Caption := GetTranslationText('FLOW', lblFlow2.Caption);
  lblTotalPlus2.Caption := GetTranslationText('TOTAL_PLUS', lblTotalPlus2.Caption);
  lblTotalMinus2.Caption := GetTranslationText('TOTAL_MINUS', lblTotalMinus2.Caption);
  lblTotal2.Caption := GetTranslationText('TOTAL', lblTotal2.Caption);
  lblAuxPlus2.Caption := GetTranslationText('AUX_PLUS', lblAuxPlus2.Caption);
  lblErrorCode2.Caption := GetTranslationText('ERROR_CODE', lblErrorCode2.Caption);
  OnlineChart2.Title.Text.Text := GetTranslationText('ACTUAL_FLOW', OnlineChart2.Title.Text.Text);
  OnlineChart2.RightAxis.Title.Caption := GetTranslationText('FLOW_UNIT', OnlineChart2.RightAxis.Title.Caption);
  // FW Update
  lblUpdateFirmwareFile.Caption := GetTranslationText('FW_UPDATE_FILE', lblUpdateFirmwareFile.Caption);
  lblUpdateFirmwareComment.Caption := GetTranslationText('FW_UPDATE_COMMENT', lblUpdateFirmwareComment.Caption);
  btnUpdateFirmware.Caption := GetTranslationText('FW_UPDATE_BTN', btnUpdateFirmware.Caption);
end;

procedure TformMagB2Menu.btnUpdateFWFileSelectClick(Sender: TObject);
var OpenDlg: TOpenDialog;
begin
  inherited;
  OpenDlg := TOpenDialog.Create(nil);
  try
    OpenDlg.Filter := GetTranslationText('FW_UPDATE_FILTER', STR_FW_UPDATE_FILTER);
    OpenDlg.DefaultExt := 'MGB';
    OpenDlg.FileName := edtUpdateFirmwareFile.Text;
    if OpenDlg.Execute then begin
      edtUpdateFirmwareFile.Text := OpenDlg.FileName;
    end;
  finally
    OpenDlg.Free;
  end;
end;

{
procedure TformMagB2Menu.btnUpdateFirmwareClick(Sender: TObject);
var MyXModem: TXModem;
    MyStream: TMemoryStream;
    ZipMaster: TZipMaster;
begin
  inherited;
  if MessageDlg(Format(GetTranslationText('FW_UPDATE_CONF', STR_FW_UPDATE_CONF),
                       [edtUpdateFirmwareFile.Text]), mtConfirmation, [mbYes, mbNo], 0) = mrYes then
  begin
    ZipMaster := TZipMaster.Create(nil);
    MyXModem := TXModem.Create(nil);
    try
      lblUpdateFirmwareStatus.Caption := 'Extracting file ...';
      Application.ProcessMessages;
      ZipMaster.ZipFileName := edtUpdateFirmwareFile.Text;
      MyStream := ZipMaster.ExtractFileToStream(ExtractFileName(ChangeFileExt(edtUpdateFirmwareFile.Text, '.bin')));
      if Assigned(MyStream) then begin
        lblUpdateFirmwareStatus.Caption := 'Device reset ...';
        Application.ProcessMessages;
        progUpdateFirmware.Max:= (MyStream.Size div 127);
        progUpdateFirmware.Position:=0;
        with MyXModem do begin
          ComPort := MyCommunication.GetComPort;
          ComPortSpeed := br115200;
          ComPortDataBits := db8BITS;
          ComPortStopBits :=sb1BITS;
          ComPortParity := ptNONE;
          ComPortRtsControl := rcRTSDISABLE;
          ComPortHwHandshaking := hhNONE;
          Timeout := 1500;
          EnableDTROnOpen := false;
        end;
        // RESET
        MyCommunication.WriteReset;
        MyCommunication.Disconnect;
        lblUpdateFirmwareStatus.Caption := 'Connecting device ...';
        Application.ProcessMessages;
        if MyXModem.Connect then begin
          lblUpdateFirmwareStatus.Caption := 'Updating firmware ...';
          Application.ProcessMessages;
          if MyXModem.SendFromStream(MyStream, progUpdateFirmware) then begin
            MessageDlg(GetTranslationText('FW_UPDATE_DONE', STR_FW_UPDATE_DONE) + #13#10 +
                       GetTranslationText('APP_RESTART', STR_APP_RESTART), mtInformation, [mbOK], 0);
            progUpdateFirmware.Position:=0;
            MyXModem.Disconnect();
          end else begin
            MessageDlg(GetTranslationText('FW_UPDATE_FAIL', STR_FW_UPDATE_FAIL) + #13#10 +
                       GetTranslationText('APP_RESTART', STR_APP_RESTART), mtError, [mbOK], 0);
            MyXModem.Disconnect();
          end;
        end else begin
          MessageDlg(GetTranslationText('FW_UPDATE_SKIP', STR_FW_UPDATE_SKIP) + #13#10 +
                     GetTranslationText('APP_RESTART', STR_APP_RESTART), mtWarning, [mbOK], 0);
        end;
        ShellExecute(0, 'open', PChar(Application.ExeName), nil, nil, SW_SHOWNORMAL);
        Konec := True;
        Application.Terminate;
      end;
    finally
      FreeAndNil(MyXModem);
      FreeAndNil(ZipMaster);
      lblUpdateFirmwareStatus.Caption := '-';
    end;
  end;
end;
}

procedure TformMagB2Menu.edtUpdateFirmwareFileChange(Sender: TObject);
begin
  inherited;
  btnUpdateFirmware.Enabled := (edtUpdateFirmwareFile.Text <> '');
end;

procedure TformMagB2Menu.btnGPRS_ReadClick(Sender: TObject);
var FourByte:T4Byte;
    Str: String;
    ReadOK:Boolean;
    WaitFrm : TformWait;
begin
  // GPRS - Read all data
  if not Busy then
  try
    Busy:=true;
    WaitFrm:=TformWait.Create(Self);
    Screen.Cursor:=crHourGlass;
    WaitFrm.gWait.Progress:=0;
    WaitFrm.Show;
    SetCenter(WaitFrm);
    WaitFrm.caption:='Please wait...';
    Application.ProcessMessages;

    lblGPRS_IPaddress.Caption := '';
    lblGPRS_Status.Caption :='';
    shpGPRS_Status.Brush.Color := clWhite;

    try
      // Gateway
      repeat
        ReadOK:=MyCommunication.ReadMenuStr(GetAddress(ProtokolType,'GSM_GATEWAY0',Values),Str,64);
      until(ReadOK)or(MessageDlg('Reading error',mtError, [mbRetry, mbAbort], 0) = mrAbort);
      if not ReadOK then
        Raise Exception.Create('Reading error');  //ukonci cteni
      edtGPRS_Gateway.Text := Str;
      WaitFrm.gWait.Progress:=15;
      Application.ProcessMessages;
      // User
      repeat
        ReadOK:=MyCommunication.ReadMenuStr(GetAddress(ProtokolType,'GSM_USER0',Values),Str,12);
      until(ReadOK)or(MessageDlg('Reading error',mtError, [mbRetry, mbAbort], 0) = mrAbort);
      if not ReadOK then
        Raise Exception.Create('Reading error');  //ukonci cteni
      edtGPRS_User.Text := Str;
      WaitFrm.gWait.Progress:=30;
      Application.ProcessMessages;
      // Password
      repeat
        ReadOK:=MyCommunication.ReadMenuStr(GetAddress(ProtokolType,'GSM_PASSWORD0',Values),Str,12);
      until(ReadOK)or(MessageDlg('Reading error',mtError, [mbRetry, mbAbort], 0) = mrAbort);
      if not ReadOK then
        Raise Exception.Create('Reading error');  //ukonci cteni
      edtGPRS_Password.Text := Str;
      WaitFrm.gWait.Progress:=45;
      Application.ProcessMessages;
      // Port
      repeat
        ReadOK:=MyCommunication.ReadMenuValue(GetAddress(ProtokolType,'GSM_PORT',Values),FourByte);
      until(ReadOK)or(MessageDlg('Reading error',mtError, [mbRetry, mbAbort], 0) = mrAbort);
      if not ReadOK then
        Raise Exception.Create('Reading error');  //ukonci cteni
      seGPRS_Port.Value := Integer(FourByte);
      WaitFrm.gWait.Progress:=60;
      Application.ProcessMessages;
      // PIN
      repeat
        ReadOK:=MyCommunication.ReadMenuValue(GetAddress(ProtokolType,'GSM_PIN',Values),FourByte);
      until(ReadOK)or(MessageDlg('Reading error',mtError, [mbRetry, mbAbort], 0) = mrAbort);
      if not ReadOK then
        Raise Exception.Create('Reading error');  //ukonci cteni
      seGPRS_PIN.Value := Integer(FourByte);
      WaitFrm.gWait.Progress:=75;
      Application.ProcessMessages;
      // IP
      repeat
        ReadOK:=MyCommunication.ReadMenuValue(GSM_IP_VALUE_ADDRESS{GetAddress(ProtokolType,'GSM_IP',Values)},FourByte);
      until(ReadOK)or(MessageDlg('Reading error',mtError, [mbRetry, mbAbort], 0) = mrAbort);
      if not ReadOK then
        Raise Exception.Create('Reading error');  //ukonci cteni
      lblGPRS_IPaddress.Caption := Format('%d.%d.%d.%d', [FourByte[3], FourByte[2], FourByte[1], FourByte[0]]);
      WaitFrm.gWait.Progress:=90;
      Application.ProcessMessages;
      // Status
      repeat
        ReadOK:=MyCommunication.ReadMenuValue(GetAddress(ProtokolType,'GSM_PRESSENT',Values),FourByte);
      until(ReadOK)or(MessageDlg('Reading error',mtError, [mbRetry, mbAbort], 0) = mrAbort);
      if not ReadOK then
        Raise Exception.Create('Reading error');  //ukonci cteni
      if (Integer(FourByte) = 1) then begin
        shpGPRS_Status.Brush.Color := clGreen;
      end else begin
        shpGPRS_Status.Brush.Color := clLtGray;
      end;
      WaitFrm.gWait.Progress:=100;
      Application.ProcessMessages;
      lblGPRS_Status.Caption :='Reading successfully';
    except
      lblGPRS_Status.Caption := 'Reading failed';
      shpGPRS_Status.Brush.Color := clRed;
    end;
  finally
    FreeAndNil(WaitFrm);
    Screen.Cursor:=crDefault;
    Busy:=false;
  end;
end;

procedure TformMagB2Menu.btnGPRS_WriteClick(Sender: TObject);
var WriteOK:Boolean;
    WaitFrm : TformWait;
begin
  // GPRS - Read all data
  if not Busy then
  try
    Busy:=true;
    WaitFrm:=TformWait.Create(Self);
    Screen.Cursor:=crHourGlass;
    WaitFrm.gWait.Progress:=0;
    WaitFrm.Show;
    SetCenter(WaitFrm);
    WaitFrm.caption:='Please wait...';
    Application.ProcessMessages;

    lblGPRS_Status.Caption :='';
    try
      // Gateway
      repeat
        WriteOK:=MyCommunication.WriteMenuStr(GetAddress(ProtokolType,'GSM_GATEWAY0',Values),edtGPRS_Gateway.Text,64);
      until (WriteOK)or(MessageDlg('Write error',mtError, [mbRetry, mbAbort], 0) = mrAbort);
      if not WriteOK then
        Raise Exception.Create('Write error');  //ukonci zapis
      WaitFrm.gWait.Progress:=15;
      Application.ProcessMessages;
      // User
      repeat
        WriteOK:=MyCommunication.WriteMenuStr(GetAddress(ProtokolType,'GSM_USER0',Values),edtGPRS_User.Text,12);
      until (WriteOK)or(MessageDlg('Write error',mtError, [mbRetry, mbAbort], 0) = mrAbort);
      if not WriteOK then
        Raise Exception.Create('Write error');  //ukonci zapis
      WaitFrm.gWait.Progress:=30;
      Application.ProcessMessages;
      // Password
      repeat
        WriteOK:=MyCommunication.WriteMenuStr(GetAddress(ProtokolType,'GSM_PASSWORD0',Values),edtGPRS_Password.Text,12);
      until (WriteOK)or(MessageDlg('Write error',mtError, [mbRetry, mbAbort], 0) = mrAbort);
      if not WriteOK then
        Raise Exception.Create('Write error');  //ukonci zapis
      WaitFrm.gWait.Progress:=45;
      Application.ProcessMessages;
      // Port
      repeat
        WriteOK:=MyCommunication.WriteMenuValue(GetAddress(ProtokolType,'GSM_PORT',Values),T4Byte(seGPRS_Port.Value));
      until (WriteOK)or(MessageDlg('Write error',mtError, [mbRetry, mbAbort], 0) = mrAbort);
      if not WriteOK then
        Raise Exception.Create('Write error');  //ukonci zapis
      WaitFrm.gWait.Progress:=60;
      Application.ProcessMessages;
      // PIN
      repeat
        WriteOK:=MyCommunication.WriteMenuValue(GetAddress(ProtokolType,'GSM_PIN',Values),T4Byte(seGPRS_PIN.Value));
      until (WriteOK)or(MessageDlg('Write error',mtError, [mbRetry, mbAbort], 0) = mrAbort);
      if not WriteOK then
        Raise Exception.Create('Write error');  //ukonci zapis
      WaitFrm.gWait.Progress:=100;
      Application.ProcessMessages;
      lblGPRS_Status.Caption :='GPRS setting writed successfully';
    except
      lblGPRS_Status.Caption := 'GPRS setting writed failed';
    end;
  finally
    FreeAndNil(WaitFrm);
    Screen.Cursor:=crDefault;
    Busy:=false;
  end;
end;

procedure TformMagB2Menu.btnGSMReadNumbersClick(Sender: TObject);
var FourByte: T4Byte;
    NumH, NumL: Integer;
    ReadOK:Boolean;
    WaitFrm : TformWait;

  function GetNumber(ANumH, ANumL: Integer): String;
  begin
    if (ANumH > 0) then begin
      Result := Format('%d%.8d', [ANumH, ANumL]);
    end else begin
      Result := Format('%d', [ANumL]);
    end;
  end;

begin
  // GSM - Read Numbers
  if not Busy then begin
    try
      Busy := true;
      WaitFrm := TformWait.Create(Self);
      Screen.Cursor := crHourGlass;
      WaitFrm.gWait.Progress:=0;
      WaitFrm.Show;
      SetCenter(WaitFrm);
      WaitFrm.caption := 'Please wait...';
      Application.ProcessMessages;

      lblGSMNumbersStatus.Caption := '';
      try
        // Phone1
        repeat
          ReadOK:=MyCommunication.ReadMenuValue(GetAddress(ProtokolType,'GSM_PHONE_1_H',Values), FourByte);
        until(ReadOK)or(MessageDlg('Reading error',mtError, [mbRetry, mbAbort], 0) = mrAbort);
        if not ReadOK then
          Raise Exception.Create('Reading error');  //ukonci cteni
        NumH := Integer(FourByte);
        WaitFrm.gWait.Progress := 10;
        Application.ProcessMessages;
        repeat
          ReadOK:=MyCommunication.ReadMenuValue(GetAddress(ProtokolType,'GSM_PHONE_1_L',Values), FourByte);
        until(ReadOK)or(MessageDlg('Reading error',mtError, [mbRetry, mbAbort], 0) = mrAbort);
        if not ReadOK then
          Raise Exception.Create('Reading error');  //ukonci cteni
        NumL := Integer(FourByte);
        WaitFrm.gWait.Progress := 20;
        Application.ProcessMessages;
        edtGSMPhone1.Text := GetNumber(NumH, NumL);
        WaitFrm.gWait.Progress := 30;
        Application.ProcessMessages;
        // Phone2
        repeat
          ReadOK:=MyCommunication.ReadMenuValue(GetAddress(ProtokolType,'GSM_PHONE_2_H',Values), FourByte);
        until(ReadOK)or(MessageDlg('Reading error',mtError, [mbRetry, mbAbort], 0) = mrAbort);
        if not ReadOK then
          Raise Exception.Create('Reading error');  //ukonci cteni
        NumH := Integer(FourByte);
        WaitFrm.gWait.Progress := 40;
        Application.ProcessMessages;
        repeat
          ReadOK:=MyCommunication.ReadMenuValue(GetAddress(ProtokolType,'GSM_PHONE_2_L',Values), FourByte);
        until(ReadOK)or(MessageDlg('Reading error',mtError, [mbRetry, mbAbort], 0) = mrAbort);
        if not ReadOK then
          Raise Exception.Create('Reading error');  //ukonci cteni
        NumL := Integer(FourByte);
        WaitFrm.gWait.Progress := 50;
        Application.ProcessMessages;
        edtGSMPhone2.Text := GetNumber(NumH, NumL);
        WaitFrm.gWait.Progress := 60;
        Application.ProcessMessages;
        // Phone3
        repeat
          ReadOK:=MyCommunication.ReadMenuValue(GetAddress(ProtokolType,'GSM_PHONE_3_H',Values), FourByte);
        until(ReadOK)or(MessageDlg('Reading error',mtError, [mbRetry, mbAbort], 0) = mrAbort);
        if not ReadOK then
          Raise Exception.Create('Reading error');  //ukonci cteni
        NumH := Integer(FourByte);
        WaitFrm.gWait.Progress := 70;
        Application.ProcessMessages;
        repeat
          ReadOK:=MyCommunication.ReadMenuValue(GetAddress(ProtokolType,'GSM_PHONE_3_L',Values), FourByte);
        until(ReadOK)or(MessageDlg('Reading error',mtError, [mbRetry, mbAbort], 0) = mrAbort);
        if not ReadOK then
          Raise Exception.Create('Reading error');  //ukonci cteni
        NumL := Integer(FourByte);
        WaitFrm.gWait.Progress := 80;
        Application.ProcessMessages;
        edtGSMPhone3.Text := GetNumber(NumH, NumL);
        WaitFrm.gWait.Progress := 100;
        Application.ProcessMessages;
        lblGSMNumbersStatus.Caption :='Reading successfully';
      except
        lblGSMNumbersStatus.Caption := 'Reading failed';
      end;
    finally
      FreeAndNil(WaitFrm);
      Screen.Cursor := crDefault;
      Busy := false;
    end;
  end;
end;

procedure TformMagB2Menu.btnGSMWriteNumbersClick(Sender: TObject);
var WriteOK: Boolean;
    NumH, NumL: Integer;
    WaitFrm : TformWait;

  function ExtractNumber(ANum: String; var ANumH, ANumL: Integer): Boolean;
  begin
    Result := False;
    try
      if Length(ANum) <= 8 then begin
        ANumH := 0;
        ANumL := StrToInt(ANum);
      end else begin
        ANumH := StrToInt(Copy(ANum, 1, Length(ANum) - 8));
        ANumL := StrToInt(Copy(ANum, Length(ANum) - 7, 8));
      end;
      Result := True;
    except
    end;
  end;

begin
  // GSM Numbers - Write data
  if not Busy then begin
    try
      Busy := true;
      WaitFrm := TformWait.Create(Self);
      Screen.Cursor := crHourGlass;
      WaitFrm.gWait.Progress := 0;
      WaitFrm.Show;
      SetCenter(WaitFrm);
      WaitFrm.caption := 'Please wait...';
      Application.ProcessMessages;

      lblGSMNumbersStatus.Caption := '';
      try
        // Phone1
        if not ExtractNumber(edtGSMPhone1.Text, NumH, NumL) then begin
          edtGSMPhone1.Font.Color := clRed;
          raise Exception.Create('Incorrect format');
        end;
        WaitFrm.gWait.Progress:=10;
        Application.ProcessMessages;
        repeat
          WriteOK:=MyCommunication.WriteMenuValue(GetAddress(ProtokolType,'GSM_PHONE_1_H',Values),T4Byte(NumH));
        until (WriteOK)or(MessageDlg('Write error',mtError, [mbRetry, mbAbort], 0) = mrAbort);
        if not WriteOK then
          Raise Exception.Create('Write error');  //ukonci zapis
        WaitFrm.gWait.Progress:=20;
        Application.ProcessMessages;
        repeat
          WriteOK:=MyCommunication.WriteMenuValue(GetAddress(ProtokolType,'GSM_PHONE_1_L',Values),T4Byte(NumL));
        until (WriteOK)or(MessageDlg('Write error',mtError, [mbRetry, mbAbort], 0) = mrAbort);
        if not WriteOK then
          Raise Exception.Create('Write error');  //ukonci zapis
        WaitFrm.gWait.Progress:=30;
        Application.ProcessMessages;
        // Phone2
        if not ExtractNumber(edtGSMPhone2.Text, NumH, NumL) then begin
          edtGSMPhone2.Font.Color := clRed;
          raise Exception.Create('Incorrect format');
        end;
        WaitFrm.gWait.Progress:=40;
        Application.ProcessMessages;
        repeat
          WriteOK:=MyCommunication.WriteMenuValue(GetAddress(ProtokolType,'GSM_PHONE_2_H',Values),T4Byte(NumH));
        until (WriteOK)or(MessageDlg('Write error',mtError, [mbRetry, mbAbort], 0) = mrAbort);
        if not WriteOK then
          Raise Exception.Create('Write error');  //ukonci zapis
        WaitFrm.gWait.Progress:=50;
        Application.ProcessMessages;
        repeat
          WriteOK:=MyCommunication.WriteMenuValue(GetAddress(ProtokolType,'GSM_PHONE_2_L',Values),T4Byte(NumL));
        until (WriteOK)or(MessageDlg('Write error',mtError, [mbRetry, mbAbort], 0) = mrAbort);
        if not WriteOK then
          Raise Exception.Create('Write error');  //ukonci zapis
        WaitFrm.gWait.Progress:=60;
        Application.ProcessMessages;
        // Phone3
        if not ExtractNumber(edtGSMPhone3.Text, NumH, NumL) then begin
          edtGSMPhone3.Font.Color := clRed;
          raise Exception.Create('Incorrect format');
        end;
        WaitFrm.gWait.Progress:=70;
        Application.ProcessMessages;
        repeat
          WriteOK:=MyCommunication.WriteMenuValue(GetAddress(ProtokolType,'GSM_PHONE_3_H',Values),T4Byte(NumH));
        until (WriteOK)or(MessageDlg('Write error',mtError, [mbRetry, mbAbort], 0) = mrAbort);
        if not WriteOK then
          Raise Exception.Create('Write error');  //ukonci zapis
        WaitFrm.gWait.Progress:=80;
        Application.ProcessMessages;
        repeat
          WriteOK:=MyCommunication.WriteMenuValue(GetAddress(ProtokolType,'GSM_PHONE_3_L',Values),T4Byte(NumL));
        until (WriteOK)or(MessageDlg('Write error',mtError, [mbRetry, mbAbort], 0) = mrAbort);
        if not WriteOK then
          Raise Exception.Create('Write error');  //ukonci zapis
        WaitFrm.gWait.Progress:=100;
        Application.ProcessMessages;
        lblGSMNumbersStatus.Caption :='Writed successfully';
      except
        on E: Exception do begin
          lblGSMNumbersStatus.Caption := E.Message;
        end;
      end;
    finally
      FreeAndNil(WaitFrm);
      Screen.Cursor := crDefault;
      Busy := false;
    end;
  end;
end;

procedure TformMagB2Menu.edtGSMPhoneChange(Sender: TObject);
begin
  inherited;
  TEdit(Sender).Font.Color := clWindowText;
end;

procedure TformMagB2Menu.btnGSMReadEventsClick(Sender: TObject);
var FourByte: T4Byte;
    ReadOK:Boolean;
    WaitFrm : TformWait;
begin
  // GSM - Read Numbers
  if not Busy then begin
    try
      Busy := true;
      WaitFrm := TformWait.Create(Self);
      Screen.Cursor := crHourGlass;
      WaitFrm.gWait.Progress:=0;
      WaitFrm.Show;
      SetCenter(WaitFrm);
      WaitFrm.caption := 'Please wait...';
      Application.ProcessMessages;

      lblGSMEventsStatus.Caption := '';
      try
        // Set Event - EmptyPipe
        repeat
          ReadOK:=MyCommunication.ReadMenuValue(GetAddress(ProtokolType,'GSM_EMPTY_PIPE',Values), FourByte);
        until(ReadOK)or(MessageDlg('Reading error',mtError, [mbRetry, mbAbort], 0) = mrAbort);
        if not ReadOK then
          Raise Exception.Create('Reading error');  //ukonci cteni
        cmbGSMEventEmptyPipe.ItemIndex := Integer(FourByte);
        WaitFrm.gWait.Progress := 15;
        Application.ProcessMessages;
        // Set Event - ZeroFlow
        repeat
          ReadOK:=MyCommunication.ReadMenuValue(GetAddress(ProtokolType,'GSM_ZERO_FLOW',Values), FourByte);
        until(ReadOK)or(MessageDlg('Reading error',mtError, [mbRetry, mbAbort], 0) = mrAbort);
        if not ReadOK then
          Raise Exception.Create('Reading error');  //ukonci cteni
        cmbGSMEventZeroFlow.ItemIndex := Integer(FourByte);
        WaitFrm.gWait.Progress := 30;
        Application.ProcessMessages;
        // Set Event - Error
        repeat
          ReadOK:=MyCommunication.ReadMenuValue(GetAddress(ProtokolType,'GSM_ERROR_DETECT',Values), FourByte);
        until(ReadOK)or(MessageDlg('Reading error',mtError, [mbRetry, mbAbort], 0) = mrAbort);
        if not ReadOK then
          Raise Exception.Create('Reading error');  //ukonci cteni
        cmbGSMEventError.ItemIndex := Integer(FourByte);
        WaitFrm.gWait.Progress := 45;
        Application.ProcessMessages;
        // Set Event - EmptyPipe
        repeat
          ReadOK:=MyCommunication.ReadMenuValue(GetAddress(ProtokolType,'GSM_EMPTY_PIPE_SEND',Values), FourByte);
        until(ReadOK)or(MessageDlg('Reading error',mtError, [mbRetry, mbAbort], 0) = mrAbort);
        if not ReadOK then
          Raise Exception.Create('Reading error');  //ukonci cteni
        cmbGSMSendEventEmptyPipe.ItemIndex := Integer(FourByte);
        WaitFrm.gWait.Progress := 60;
        Application.ProcessMessages;
        // Set Event - ZeroFlow
        repeat
          ReadOK:=MyCommunication.ReadMenuValue(GetAddress(ProtokolType,'GSM_ZERO_FLOW_SEND',Values), FourByte);
        until(ReadOK)or(MessageDlg('Reading error',mtError, [mbRetry, mbAbort], 0) = mrAbort);
        if not ReadOK then
          Raise Exception.Create('Reading error');  //ukonci cteni
        cmbGSMSendEventZeroFlow.ItemIndex := Integer(FourByte);
        WaitFrm.gWait.Progress := 75;
        Application.ProcessMessages;
        // Interval
        repeat
          ReadOK:=MyCommunication.ReadMenuValue(GetAddress(ProtokolType,'GSM_DATA_INTERVAL',Values), FourByte);
        until(ReadOK)or(MessageDlg('Reading error',mtError, [mbRetry, mbAbort], 0) = mrAbort);
        if not ReadOK then
          Raise Exception.Create('Reading error');  //ukonci cteni
        seGSMInterval.Value := Integer(FourByte);
        WaitFrm.gWait.Progress := 100;
        Application.ProcessMessages;
        lblGSMEventsStatus.Caption :='Reading successfully';
      except
        lblGSMEventsStatus.Caption := 'Reading failed';
      end;
    finally
      FreeAndNil(WaitFrm);
      Screen.Cursor := crDefault;
      Busy := false;
    end;
  end;
end;

procedure TformMagB2Menu.btnGSMWriteEventsClick(Sender: TObject);
var WriteOK: Boolean;
    WaitFrm : TformWait;
begin
  // GSM Numbers - Write data
  if not Busy then begin
    try
      Busy := true;
      WaitFrm := TformWait.Create(Self);
      Screen.Cursor := crHourGlass;
      WaitFrm.gWait.Progress := 0;
      WaitFrm.Show;
      SetCenter(WaitFrm);
      WaitFrm.caption := 'Please wait...';
      Application.ProcessMessages;

      lblGSMNumbersStatus.Caption := '';
      try
        // Set Event - EmptyPipe
        repeat
          WriteOK:=MyCommunication.WriteMenuValue(GetAddress(ProtokolType,'GSM_EMPTY_PIPE',Values),T4Byte(cmbGSMEventEmptyPipe.ItemIndex));
        until (WriteOK)or(MessageDlg('Write error',mtError, [mbRetry, mbAbort], 0) = mrAbort);
        if not WriteOK then
          Raise Exception.Create('Write error');  //ukonci zapis
        WaitFrm.gWait.Progress:=15;
        Application.ProcessMessages;
        // Set Event - ZeroFlow
        repeat
          WriteOK:=MyCommunication.WriteMenuValue(GetAddress(ProtokolType,'GSM_ZERO_FLOW',Values),T4Byte(cmbGSMEventZeroFlow.ItemIndex));
        until (WriteOK)or(MessageDlg('Write error',mtError, [mbRetry, mbAbort], 0) = mrAbort);
        if not WriteOK then
          Raise Exception.Create('Write error');  //ukonci zapis
        WaitFrm.gWait.Progress:=30;
        Application.ProcessMessages;
        // Set Event - Error
        repeat
          WriteOK:=MyCommunication.WriteMenuValue(GetAddress(ProtokolType,'GSM_ERROR_DETECT',Values),T4Byte(cmbGSMEventError.ItemIndex));
        until (WriteOK)or(MessageDlg('Write error',mtError, [mbRetry, mbAbort], 0) = mrAbort);
        if not WriteOK then
          Raise Exception.Create('Write error');  //ukonci zapis
        WaitFrm.gWait.Progress:=45;
        Application.ProcessMessages;
        // Send Event - EmptyPipe
        repeat
          WriteOK:=MyCommunication.WriteMenuValue(GetAddress(ProtokolType,'GSM_EMPTY_PIPE_SEND',Values),T4Byte(cmbGSMSendEventEmptyPipe.ItemIndex));
        until (WriteOK)or(MessageDlg('Write error',mtError, [mbRetry, mbAbort], 0) = mrAbort);
        if not WriteOK then
          Raise Exception.Create('Write error');  //ukonci zapis
        WaitFrm.gWait.Progress:=60;
        Application.ProcessMessages;
        // Send Event - ZeroFlow
        repeat
          WriteOK:=MyCommunication.WriteMenuValue(GetAddress(ProtokolType,'GSM_ZERO_FLOW_SEND',Values),T4Byte(cmbGSMSendEventZeroFlow.ItemIndex));
        until (WriteOK)or(MessageDlg('Write error',mtError, [mbRetry, mbAbort], 0) = mrAbort);
        if not WriteOK then
          Raise Exception.Create('Write error');  //ukonci zapis
        WaitFrm.gWait.Progress:=75;
        Application.ProcessMessages;
        // Interval
        repeat
          WriteOK:=MyCommunication.WriteMenuValue(GetAddress(ProtokolType,'GSM_DATA_INTERVAL',Values),T4Byte(seGSMInterval.Value));
        until (WriteOK)or(MessageDlg('Write error',mtError, [mbRetry, mbAbort], 0) = mrAbort);
        if not WriteOK then
          Raise Exception.Create('Write error');  //ukonci zapis
        WaitFrm.gWait.Progress:=100;
        Application.ProcessMessages;
        lblGSMEventsStatus.Caption :='Writed successfully';
      except
        on E: Exception do begin
          lblGSMEventsStatus.Caption := E.Message;
        end;
      end;
    finally
      FreeAndNil(WaitFrm);
      Screen.Cursor := crDefault;
      Busy := false;
    end;
  end;
end;

procedure TformMagB2Menu.WMNeedReset(var Message: TMessage);
begin
  NeedReset := True;
  Close;
end;

procedure TformMagB2Menu.btnWriteSensorUnitNoClick(Sender: TObject);
var SensorUnitNo: Cardinal;
begin
  if not Busy then begin
  try
  Busy := true;
  StatusLabel.Caption:='';
  if seSensorUnitNo.Visible then begin
    SensorUnitNo := seSensorUnitNo.Value;
    if MyCommunication.WriteSensorUnitNo(SensorUnitNo) then begin
      StatusLabel.Caption := GetTranslationText('WRITE_SUCCESS', STR_WRITE_SUCC);
      StatusLabel.Font.Color:=clGreen;
    end else begin
      StatusLabel.Caption := GetTranslationText('WRITE_FAILED', STR_WRITE_FAIL);
      StatusLabel.Font.Color:=clRed;
    end;
  end;
  finally
  Busy := false;
  end;
  end;
end;

procedure TformMagB2Menu.btnWriteDiameterClick(Sender: TObject);
var Diameter: Cardinal;
begin
  if not Busy then begin
  try
  Busy := true;
  StatusLabel.Caption:='';
  if seDiameter.Visible then begin
    Diameter := seDiameter.Value;
    if MyCommunication.WriteDiameter(Diameter) then begin
      StatusLabel.Caption := GetTranslationText('WRITE_SUCCESS', STR_WRITE_SUCC);
      StatusLabel.Font.Color:=clGreen;
    end else begin
      StatusLabel.Caption := GetTranslationText('WRITE_FAILED', STR_WRITE_FAIL);
      StatusLabel.Font.Color:=clRed;
    end;
  end;
  finally
  Busy := false;
  end;
  end;
end;

procedure TformMagB2Menu.btnReadDiameterClick(Sender: TObject);
var Diameter: Cardinal;
begin
  if not Busy then
  try
    Busy:=true;
    StatusLabel.Caption:='';
    try
      if seDiameter.Visible then begin
        if MyCommunication.ReadDiameter(Diameter) then begin
          seDiameter.Value := Diameter;
        end else begin
          raise Exception.Create(GetTranslationText('READ_ERROR', STR_READ_ERROR));  //ukonci cteni
        end;
      end;
      StatusLabel.Caption:=GetTranslationText('READ_SUCCESS', STR_READ_SUCC);
      StatusLabel.Font.Color:=clGreen;
    except
      StatusLabel.Caption:=GetTranslationText('READ_FAILED', STR_READ_FAIL);
      StatusLabel.Font.Color:=clRed;
    end;
  finally
    Busy:=false;
  end;
end;

procedure TformMagB2Menu.btnWriteExcitationEnableClick(Sender: TObject);
var ExcitationEnable: Cardinal;
begin
  if not Busy then begin
  try
  Busy := true;
  StatusLabel.Caption:='';
  if cbExcitationEnable.Visible then begin
    ExcitationEnable := cbExcitationEnable.ItemIndex;
    if MyCommunication.WriteExcitationEnable(ExcitationEnable) then begin
      StatusLabel.Caption := GetTranslationText('WRITE_SUCCESS', STR_WRITE_SUCC);
      StatusLabel.Font.Color:=clGreen;
    end else begin
      StatusLabel.Caption := GetTranslationText('WRITE_FAILED', STR_WRITE_FAIL);
      StatusLabel.Font.Color:=clRed;
    end;
  end;
  finally
  Busy := false;
  end;
  end;
end;

procedure TformMagB2Menu.btnReadExcitationEnableClick(Sender: TObject);
var ExcitationEnable: Cardinal;
begin
  if not Busy then
  try
    Busy:=true;
    StatusLabel.Caption:='';
    try
      if cbExcitationEnable.Visible then begin
        if MyCommunication.ReadExcitationEnable(ExcitationEnable) then begin
          cbExcitationEnable.ItemIndex := ExcitationEnable;
        end else begin
          raise Exception.Create(GetTranslationText('READ_ERROR', STR_READ_ERROR));  //ukonci cteni
        end;
      end;
      StatusLabel.Caption:=GetTranslationText('READ_SUCCESS', STR_READ_SUCC);
      StatusLabel.Font.Color:=clGreen;
    except
      StatusLabel.Caption:=GetTranslationText('READ_FAILED', STR_READ_FAIL);
      StatusLabel.Font.Color:=clRed;
    end;
  finally
    Busy:=false;
  end;
end;

procedure TformMagB2Menu.btnWriteExcitationCurrentClick(Sender: TObject);
var ExcitationCurrent: Cardinal;
begin
  if not Busy then begin
  try
  Busy := true;
  StatusLabel.Caption:='';
  if cbExcitationCurrent.Visible then begin
    ExcitationCurrent := cbExcitationCurrent.ItemIndex;
    if MyCommunication.WriteExcitationCurrent(ExcitationCurrent) then begin
      StatusLabel.Caption := GetTranslationText('WRITE_SUCCESS', STR_WRITE_SUCC);
      StatusLabel.Font.Color:=clGreen;
    end else begin
      StatusLabel.Caption := GetTranslationText('WRITE_FAILED', STR_WRITE_FAIL);
      StatusLabel.Font.Color:=clRed;
    end;
  end;
  finally
  Busy := false;
  end;
  end;
end;

procedure TformMagB2Menu.btnReadExcitationCurrentClick(Sender: TObject);
var ExcitationCurrent: Cardinal;
begin
  if not Busy then
  try
    Busy:=true;
    StatusLabel.Caption:='';
    try
      if cbExcitationCurrent.Visible then begin
        if MyCommunication.ReadExcitationCurrent(ExcitationCurrent) then begin
          cbExcitationCurrent.ItemIndex := ExcitationCurrent;
        end else begin
          raise Exception.Create(GetTranslationText('READ_ERROR', STR_READ_ERROR));  //ukonci cteni
        end;
      end;
      StatusLabel.Caption:=GetTranslationText('READ_SUCCESS', STR_READ_SUCC);
      StatusLabel.Font.Color:=clGreen;
    except
      StatusLabel.Caption:=GetTranslationText('READ_FAILED', STR_READ_FAIL);
      StatusLabel.Font.Color:=clRed;
    end;
  finally
    Busy:=false;
  end;
end;

procedure TformMagB2Menu.btnWriteExcitationFrequencyClick(Sender: TObject);
var ExcitationFrequency: Cardinal;
begin
  if not Busy then begin
  try
  Busy := true;
  StatusLabel.Caption:='';
  if cbExcitationFrequency.Visible then begin
    ExcitationFrequency := cbExcitationFrequency.ItemIndex;
    if MyCommunication.WriteExcitationFrequency(ExcitationFrequency) then begin
      StatusLabel.Caption := GetTranslationText('WRITE_SUCCESS', STR_WRITE_SUCC);
      StatusLabel.Font.Color:=clGreen;
    end else begin
      StatusLabel.Caption := GetTranslationText('WRITE_FAILED', STR_WRITE_FAIL);
      StatusLabel.Font.Color:=clRed;
    end;
  end;
  finally
  Busy := false;
  end;
  end;
end;

procedure TformMagB2Menu.btnReadExcitationFrequencyClick(Sender: TObject);
var ExcitationFrequency: Cardinal;
begin
  if not Busy then
  try
    Busy:=true;
    StatusLabel.Caption:='';
    try
      if cbExcitationFrequency.Visible then begin
        if MyCommunication.ReadExcitationFrequency(ExcitationFrequency) then begin
          cbExcitationFrequency.ItemIndex := ExcitationFrequency;
        end else begin
          raise Exception.Create(GetTranslationText('READ_ERROR', STR_READ_ERROR));  //ukonci cteni
        end;
      end;
      StatusLabel.Caption:=GetTranslationText('READ_SUCCESS', STR_READ_SUCC);
      StatusLabel.Font.Color:=clGreen;
    except
      StatusLabel.Caption:=GetTranslationText('READ_FAILED', STR_READ_FAIL);
      StatusLabel.Font.Color:=clRed;
    end;
  finally
    Busy:=false;
  end;
end;

procedure TformMagB2Menu.btnWriteServiceModeClick(Sender: TObject);
var ServiceMode: Cardinal;
begin
  if not Busy then begin
  try
  Busy := true;
  StatusLabel.Caption:='';
  if cbServiceMode.Visible then begin
    ServiceMode := cbServiceMode.ItemIndex;
    if MyCommunication.WriteServiceMode(ServiceMode) then begin
      StatusLabel.Caption := GetTranslationText('WRITE_SUCCESS', STR_WRITE_SUCC);
      StatusLabel.Font.Color:=clGreen;
    end else begin
      StatusLabel.Caption := GetTranslationText('WRITE_FAILED', STR_WRITE_FAIL);
      StatusLabel.Font.Color:=clRed;
    end;
  end;
  finally
  Busy := false;
  end;
  end;
end;

procedure TformMagB2Menu.btnReadServiceModeClick(Sender: TObject);
var ServiceMode: Cardinal;
begin
  if not Busy then
  try
    Busy:=true;
    StatusLabel.Caption:='';
    try
      if cbServiceMode.Visible then begin
        if MyCommunication.ReadServiceMode(ServiceMode) then begin
          cbServiceMode.ItemIndex := ServiceMode;
        end else begin
          raise Exception.Create(GetTranslationText('READ_ERROR', STR_READ_ERROR));  //ukonci cteni
        end;
      end;
      StatusLabel.Caption:=GetTranslationText('READ_SUCCESS', STR_READ_SUCC);
      StatusLabel.Font.Color:=clGreen;
    except
      StatusLabel.Caption:=GetTranslationText('READ_FAILED', STR_READ_FAIL);
      StatusLabel.Font.Color:=clRed;
    end;
  finally
    Busy:=false;
  end;
end;

procedure TformMagB2Menu.btnWritePosPoint1Click(Sender: TObject);
var
  Flow: Single;
  Adc: Cardinal;
begin
  if not Busy then begin
  try
  Busy := true;
  CalDataPointsStatusLabel.Caption:='';
  if ssFlowPosPoint1.Visible then begin
    Flow := ssFlowPosPoint1.Value;
    Adc := seAdcPosPoint1.Value;
    if MyCommunication.WritePosPoint1(Flow, Adc) then begin
      CalDataPointsStatusLabel.Caption := GetTranslationText('WRITE_SUCCESS', STR_WRITE_SUCC);
      CalDataPointsStatusLabel.Font.Color:=clGreen;
    end else begin
      CalDataPointsStatusLabel.Caption := GetTranslationText('WRITE_FAILED', STR_WRITE_FAIL);
      CalDataPointsStatusLabel.Font.Color:=clRed;
    end;
  end;
  finally
  Busy := false;
  end;
  end;
end;

procedure TformMagB2Menu.btnReadPosPoint1Click(Sender: TObject);
var
  Flow: Single;
  Adc: Cardinal;
begin
  if not Busy then
  try
    Busy:=true;
    CalDataPointsStatusLabel.Caption:='';
    try
      if ssFlowPosPoint1.Visible then begin
        if MyCommunication.ReadPosPoint1(Flow, Adc) then begin
          ssFlowPosPoint1.Value := Flow;
          seAdcPosPoint1.Value := Adc;
        end else begin
          raise Exception.Create(GetTranslationText('READ_ERROR', STR_READ_ERROR));  //ukonci cteni
        end;
      end;
      CalDataPointsStatusLabel.Caption:=GetTranslationText('READ_SUCCESS', STR_READ_SUCC);
      CalDataPointsStatusLabel.Font.Color:=clGreen;
    except
      CalDataPointsStatusLabel.Caption:=GetTranslationText('READ_FAILED', STR_READ_FAIL);
      CalDataPointsStatusLabel.Font.Color:=clRed;
    end;
  finally
    Busy:=false;
  end;
end;

procedure TformMagB2Menu.btnWritePosPoint2Click(Sender: TObject);
var
  Flow: Single;
  Adc: Cardinal;
begin
  if not Busy then begin
  try
  Busy := true;
  CalDataPointsStatusLabel.Caption:='';
  if ssFlowPosPoint2.Visible then begin
    Flow := ssFlowPosPoint2.Value;
    Adc := seAdcPosPoint2.Value;
    if MyCommunication.WritePosPoint2(Flow, Adc) then begin
      CalDataPointsStatusLabel.Caption := GetTranslationText('WRITE_SUCCESS', STR_WRITE_SUCC);
      CalDataPointsStatusLabel.Font.Color:=clGreen;
    end else begin
      CalDataPointsStatusLabel.Caption := GetTranslationText('WRITE_FAILED', STR_WRITE_FAIL);
      CalDataPointsStatusLabel.Font.Color:=clRed;
    end;
  end;
  finally
  Busy := false;
  end;
  end;
end;

procedure TformMagB2Menu.btnReadPosPoint2Click(Sender: TObject);
var
  Flow: Single;
  Adc: Cardinal;
begin
  if not Busy then
  try
    Busy:=true;
    CalDataPointsStatusLabel.Caption:='';
    try
      if ssFlowPosPoint2.Visible then begin
        if MyCommunication.ReadPosPoint2(Flow, Adc) then begin
          ssFlowPosPoint2.Value := Flow;
          seAdcPosPoint2.Value := Adc;
        end else begin
          raise Exception.Create(GetTranslationText('READ_ERROR', STR_READ_ERROR));  //ukonci cteni
        end;
      end;
      CalDataPointsStatusLabel.Caption:=GetTranslationText('READ_SUCCESS', STR_READ_SUCC);
      CalDataPointsStatusLabel.Font.Color:=clGreen;
    except
      CalDataPointsStatusLabel.Caption:=GetTranslationText('READ_FAILED', STR_READ_FAIL);
      CalDataPointsStatusLabel.Font.Color:=clRed;
    end;
  finally
    Busy:=false;
  end;
end;

procedure TformMagB2Menu.btnWriteNegPoint1Click(Sender: TObject);
var
  Flow: Single;
  Adc: Cardinal;
begin
  if not Busy then begin
  try
  Busy := true;
  CalDataPointsStatusLabel.Caption:='';
  if ssFlowNegPoint1.Visible then begin
    Flow := ssFlowNegPoint1.Value;
    Adc := seAdcNegPoint1.Value;
    if MyCommunication.WriteNegPoint1(Flow, Adc) then begin
      CalDataPointsStatusLabel.Caption := GetTranslationText('WRITE_SUCCESS', STR_WRITE_SUCC);
      CalDataPointsStatusLabel.Font.Color:=clGreen;
    end else begin
      CalDataPointsStatusLabel.Caption := GetTranslationText('WRITE_FAILED', STR_WRITE_FAIL);
      CalDataPointsStatusLabel.Font.Color:=clRed;
    end;
  end;
  finally
  Busy := false;
  end;
  end;
end;

procedure TformMagB2Menu.btnReadNegPoint1Click(Sender: TObject);
var
  Flow: Single;
  Adc: Cardinal;
begin
  if not Busy then
  try
    Busy:=true;
    CalDataPointsStatusLabel.Caption:='';
    try
      if ssFlowNegPoint1.Visible then begin
        if MyCommunication.ReadNegPoint1(Flow, Adc) then begin
          ssFlowNegPoint1.Value := Flow;
          seAdcNegPoint1.Value := Adc;
        end else begin
          raise Exception.Create(GetTranslationText('READ_ERROR', STR_READ_ERROR));  //ukonci cteni
        end;
      end;
      CalDataPointsStatusLabel.Caption:=GetTranslationText('READ_SUCCESS', STR_READ_SUCC);
      CalDataPointsStatusLabel.Font.Color:=clGreen;
    except
      CalDataPointsStatusLabel.Caption:=GetTranslationText('READ_FAILED', STR_READ_FAIL);
      CalDataPointsStatusLabel.Font.Color:=clRed;
    end;
  finally
    Busy:=false;
  end;
end;

procedure TformMagB2Menu.btnWriteNegPoint2Click(Sender: TObject);
var
  Flow: Single;
  Adc: Cardinal;
begin
  if not Busy then begin
  try
  Busy := true;
  CalDataPointsStatusLabel.Caption:='';
  if ssFlowNegPoint2.Visible then begin
    Flow := ssFlowNegPoint2.Value;
    Adc := seAdcNegPoint2.Value;
    if MyCommunication.WriteNegPoint2(Flow, Adc) then begin
      CalDataPointsStatusLabel.Caption := GetTranslationText('WRITE_SUCCESS', STR_WRITE_SUCC);
      CalDataPointsStatusLabel.Font.Color:=clGreen;
    end else begin
      CalDataPointsStatusLabel.Caption := GetTranslationText('WRITE_FAILED', STR_WRITE_FAIL);
      CalDataPointsStatusLabel.Font.Color:=clRed;
    end;
  end;
  finally
  Busy := false;
  end;
  end;
end;

procedure TformMagB2Menu.btnReadNegPoint2Click(Sender: TObject);
var
  Flow: Single;
  Adc: Cardinal;
begin
  if not Busy then
  try
    Busy:=true;
    CalDataPointsStatusLabel.Caption:='';
    try
      if ssFlowNegPoint2.Visible then begin
        if MyCommunication.ReadNegPoint2(Flow, Adc) then begin
          ssFlowNegPoint2.Value := Flow;
          seAdcNegPoint2.Value := Adc;
        end else begin
          raise Exception.Create(GetTranslationText('READ_ERROR', STR_READ_ERROR));  //ukonci cteni
        end;
      end;
      CalDataPointsStatusLabel.Caption:=GetTranslationText('READ_SUCCESS', STR_READ_SUCC);
      CalDataPointsStatusLabel.Font.Color:=clGreen;
    except
      CalDataPointsStatusLabel.Caption:=GetTranslationText('READ_FAILED', STR_READ_FAIL);
      CalDataPointsStatusLabel.Font.Color:=clRed;
    end;
  finally
    Busy:=false;
  end;
end;

procedure TformMagB2Menu.btnWriteZeroPointClick(Sender: TObject);
var
  Flow: Single;
  Adc: Cardinal;
begin
  if not Busy then begin
  try
  Busy := true;
  CalDataPointsStatusLabel.Caption:='';
  if ssFlowZeroPoint.Visible then begin
    Flow := ssFlowZeroPoint.Value;
    Adc := seAdcZeroPoint.Value;
    if MyCommunication.WritePosPoint1(Flow, Adc) then begin
      CalDataPointsStatusLabel.Caption := GetTranslationText('WRITE_SUCCESS', STR_WRITE_SUCC);
      CalDataPointsStatusLabel.Font.Color:=clGreen;
    end else begin
      CalDataPointsStatusLabel.Caption := GetTranslationText('WRITE_FAILED', STR_WRITE_FAIL);
      CalDataPointsStatusLabel.Font.Color:=clRed;
    end;
  end;
  finally
  Busy := false;
  end;
  end;
end;

procedure TformMagB2Menu.btnReadZeroPointClick(Sender: TObject);
var
  Flow: Single;
  Adc: Cardinal;
begin
  if not Busy then
  try
    Busy:=true;
    CalDataPointsStatusLabel.Caption:='';
    try
      if ssFlowZeroPoint.Visible then begin
        if MyCommunication.ReadPosPoint1(Flow, Adc) then begin
          ssFlowZeroPoint.Value := Flow;
          seAdcZeroPoint.Value := Adc;
        end else begin
          raise Exception.Create(GetTranslationText('READ_ERROR', STR_READ_ERROR));  //ukonci cteni
        end;
      end;
      CalDataPointsStatusLabel.Caption:=GetTranslationText('READ_SUCCESS', STR_READ_SUCC);
      CalDataPointsStatusLabel.Font.Color:=clGreen;
    except
      CalDataPointsStatusLabel.Caption:=GetTranslationText('READ_FAILED', STR_READ_FAIL);
      CalDataPointsStatusLabel.Font.Color:=clRed;
    end;
  finally
    Busy:=false;
  end;
end;

procedure TformMagB2Menu.btnMeasureZeroClick(Sender: TObject);
var MeasureZero: Cardinal;
begin
  if not Busy then begin
  try
  Busy := true;
  CalDataPointsStatusLabel.Caption:='';
  if seMeasureZero.Visible then begin
    MeasureZero := seMeasureZero.Value;
    if MyCommunication.WriteMeasureZero(MeasureZero) then begin
      CalDataPointsStatusLabel.Caption := GetTranslationText('WRITE_SUCCESS', STR_WRITE_SUCC);
      CalDataPointsStatusLabel.Font.Color:=clGreen;
    end else begin
      CalDataPointsStatusLabel.Caption := GetTranslationText('WRITE_FAILED', STR_WRITE_FAIL);
      CalDataPointsStatusLabel.Font.Color:=clRed;
    end;
  end;
  finally
  Busy := false;
  end;
  end;
end;

procedure TformMagB2Menu.btnWritePosDoErrorCorrectionClick(Sender: TObject);
var
  Flow, LowerErr, UpperErr: Single;
  Flow1, Flow2: Single;
  Adc1, Adc2: Cardinal;
begin
  if not Busy then begin
  try
  Busy := true;
  CalTunningStatusLabel.Caption:='';
  if ssPosFlowCorrection.Visible then begin
    Flow := ssPosFlowCorrection.Value;
    LowerErr := ssPosErrLowerCorrection.Value;
    UpperErr := ssPosErrUpperCorrection.Value;
    if MyCommunication.WritePosCorrection(Flow, LowerErr, UpperErr) then begin
      if MyCommunication.WritePosDoErrorCorrection then begin
        CalTunningStatusLabel.Caption := GetTranslationText('WRITE_SUCCESS', STR_WRITE_SUCC);
        CalTunningStatusLabel.Font.Color:=clGreen;
        CalDataPointsStatusLabel.Caption:='';
        try
          if ssFlowPosPoint1.Visible and ssFlowPosPoint2.Visible then begin
            if MyCommunication.ReadPosPoint1(Flow1, Adc1) and MyCommunication.ReadPosPoint2(Flow2, Adc2) then begin
              ssFlowPosPoint1.Value := Flow1;
              seAdcPosPoint1.Value := Adc1;
              ssFlowPosPoint2.Value := Flow2;
              seAdcPosPoint2.Value := Adc2;
              CalTunningStatusLabel.Caption:='';
              try
                if ssPosFlowCorrection.Visible then begin
                  if MyCommunication.ReadPosCorrection(Flow, LowerErr, UpperErr) then begin
                    ssPosFlowCorrection.Value := Flow;
                    ssPosErrLowerCorrection.Value := LowerErr;
                    ssPosErrUpperCorrection.Value := UpperErr;
                  end else begin
                    raise Exception.Create(GetTranslationText('READ_ERROR', STR_READ_ERROR));  //ukonci cteni
                  end;
                end;
                CalTunningStatusLabel.Caption:=GetTranslationText('READ_SUCCESS', STR_READ_SUCC);
                CalTunningStatusLabel.Font.Color:=clGreen;
              except
                CalTunningStatusLabel.Caption:=GetTranslationText('READ_FAILED', STR_READ_FAIL);
                CalTunningStatusLabel.Font.Color:=clRed;
              end;
            end else begin
              raise Exception.Create(GetTranslationText('READ_ERROR', STR_READ_ERROR));  //ukonci cteni
            end;
          end;
          CalDataPointsStatusLabel.Caption:=GetTranslationText('READ_SUCCESS', STR_READ_SUCC);
          CalDataPointsStatusLabel.Font.Color:=clGreen;
        except
          CalDataPointsStatusLabel.Caption:=GetTranslationText('READ_FAILED', STR_READ_FAIL);
          CalDataPointsStatusLabel.Font.Color:=clRed;
        end;
      end else begin
        CalTunningStatusLabel.Caption := GetTranslationText('WRITE_FAILED', STR_WRITE_FAIL);
        CalTunningStatusLabel.Font.Color:=clRed;
      end;
    end else begin
      CalTunningStatusLabel.Caption := GetTranslationText('WRITE_FAILED', STR_WRITE_FAIL);
      CalTunningStatusLabel.Font.Color:=clRed;
    end;
  end;
  finally
  Busy := false;
  end;
  end;
end;

procedure TformMagB2Menu.btnWriteNegDoErrorCorrectionClick(Sender: TObject);
var
  Flow, LowerErr, UpperErr: Single;
  Flow1, Flow2: Single;
  Adc1, Adc2: Cardinal;
begin
  if not Busy then begin
  try
  Busy := true;
  CalTunningStatusLabel.Caption:='';
  if ssNegFlowCorrection.Visible then begin
    Flow := ssNegFlowCorrection.Value;
    LowerErr := ssNegErrLowerCorrection.Value;
    UpperErr := ssNegErrUpperCorrection.Value;
    if MyCommunication.WriteNegCorrection(Flow, LowerErr, UpperErr) then begin
      if MyCommunication.WriteNegDoErrorCorrection then begin
        CalTunningStatusLabel.Caption := GetTranslationText('WRITE_SUCCESS', STR_WRITE_SUCC);
        CalTunningStatusLabel.Font.Color:=clGreen;
        CalDataPointsStatusLabel.Caption:='';
        try
          if ssFlowNegPoint1.Visible and ssFlowNegPoint2.Visible then begin
            if MyCommunication.ReadNegPoint1(Flow1, Adc1) and MyCommunication.ReadNegPoint2(Flow2, Adc2) then begin
              ssFlowNegPoint1.Value := Flow1;
              seAdcNegPoint1.Value := Adc1;
              ssFlowNegPoint2.Value := Flow2;
              seAdcNegPoint2.Value := Adc2;
              CalTunningStatusLabel.Caption:='';
              try
                if ssNegFlowCorrection.Visible then begin
                  if MyCommunication.ReadNegCorrection(Flow, LowerErr, UpperErr) then begin
                    ssNegFlowCorrection.Value := Flow;
                    ssNegErrLowerCorrection.Value := LowerErr;
                    ssNegErrUpperCorrection.Value := UpperErr;
                  end else begin
                    raise Exception.Create(GetTranslationText('READ_ERROR', STR_READ_ERROR));  //ukonci cteni
                  end;
                end;
                CalTunningStatusLabel.Caption:=GetTranslationText('READ_SUCCESS', STR_READ_SUCC);
                CalTunningStatusLabel.Font.Color:=clGreen;
              except
                CalTunningStatusLabel.Caption:=GetTranslationText('READ_FAILED', STR_READ_FAIL);
                CalTunningStatusLabel.Font.Color:=clRed;
              end;
            end else begin
              raise Exception.Create(GetTranslationText('READ_ERROR', STR_READ_ERROR));  //ukonci cteni
            end;
          end;
          CalDataPointsStatusLabel.Caption:=GetTranslationText('READ_SUCCESS', STR_READ_SUCC);
          CalDataPointsStatusLabel.Font.Color:=clGreen;
        except
          CalDataPointsStatusLabel.Caption:=GetTranslationText('READ_FAILED', STR_READ_FAIL);
          CalDataPointsStatusLabel.Font.Color:=clRed;
        end;
      end else begin
        CalTunningStatusLabel.Caption := GetTranslationText('WRITE_FAILED', STR_WRITE_FAIL);
        CalTunningStatusLabel.Font.Color:=clRed;
      end;
    end else begin
      CalTunningStatusLabel.Caption := GetTranslationText('WRITE_FAILED', STR_WRITE_FAIL);
      CalTunningStatusLabel.Font.Color:=clRed;
    end;
  end;
  finally
  Busy := false;
  end;
  end;
end;

procedure TformMagB2Menu.btnSetAfterCalibrationClick(Sender: TObject);
begin
  if not Busy then begin
    try
      Busy := true;
      ServiceStatusLabel.Caption:='';
      if MyCommunication.WriteSetAfterCalibration(1) then begin
        ServiceStatusLabel.Caption := GetTranslationText('WRITE_SUCCESS', STR_WRITE_SUCC);
        ServiceStatusLabel.Font.Color:=clGreen;
      end else begin
        ServiceStatusLabel.Caption := GetTranslationText('WRITE_FAILED', STR_WRITE_FAIL);
        ServiceStatusLabel.Font.Color:=clRed;
      end;
    finally
      Busy := false;
    end;
  end;
end;

procedure TformMagB2Menu.btnService1Click(Sender: TObject);
begin
  if not Busy then begin
    try
      Busy := true;
      ServiceStatusLabel.Caption:='';
      if MyCommunication.WriteService1(1) then begin
        ServiceStatusLabel.Caption := GetTranslationText('WRITE_SUCCESS', STR_WRITE_SUCC);
        ServiceStatusLabel.Font.Color:=clGreen;
      end else begin
        ServiceStatusLabel.Caption := GetTranslationText('WRITE_FAILED', STR_WRITE_FAIL);
        ServiceStatusLabel.Font.Color:=clRed;
      end;
    finally
      Busy := false;
    end;
  end;
end;

procedure TformMagB2Menu.btnService2Click(Sender: TObject);
begin
  if not Busy then begin
    try
      Busy := true;
      ServiceStatusLabel.Caption:='';
      if MyCommunication.WriteService2(1) then begin
        ServiceStatusLabel.Caption := GetTranslationText('WRITE_SUCCESS', STR_WRITE_SUCC);
        ServiceStatusLabel.Font.Color:=clGreen;
      end else begin
        ServiceStatusLabel.Caption := GetTranslationText('WRITE_FAILED', STR_WRITE_FAIL);
        ServiceStatusLabel.Font.Color:=clRed;
      end;
    finally
      Busy := false;
    end;
  end;
end;

procedure TformMagB2Menu.btnService3Click(Sender: TObject);
begin
  if not Busy then begin
    try
      Busy := true;
      ServiceStatusLabel.Caption:='';
      if MyCommunication.WriteService3(1) then begin
        ServiceStatusLabel.Caption := GetTranslationText('WRITE_SUCCESS', STR_WRITE_SUCC);
        ServiceStatusLabel.Font.Color:=clGreen;
      end else begin
        ServiceStatusLabel.Caption := GetTranslationText('WRITE_FAILED', STR_WRITE_FAIL);
        ServiceStatusLabel.Font.Color:=clRed;
      end;
    finally
      Busy := false;
    end;
  end;
end;

end.
