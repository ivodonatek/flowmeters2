unit MagB2RunUnit;

interface

procedure MagB2Run;

implementation

uses sysutils, controls, Dialogs, Forms, MagB2TranslationUnit,
  MagB2GlobalUtilsClass, MagB2MyCommunicationClass,
  MagB2StatFrm, MagB2EnterFrm, MagB2MenuFrm;

var
    Demo : Boolean;
    ReadOK:Boolean;
    firmwareNO:integer;
    MyCommunication : TMyCommunication;

function GetDeviceFirmwareNumber(AProtokolType:TProtokolType;ASlaveID,AComNumber:Cardinal;
   BaudRate:TBaudRate;StopBits:TStopBits;Parity:TParity;Timeout:Integer;
   RtsControl:TRtsControl;EnableDTROnOpen:Boolean;ASwitchWait:Cardinal;
   AIPAddress: String; APort: Integer):integer;
var firmwareNO:integer;
begin
    MyCommunication.SetCommunicationParam(AComNumber,ASlaveID,BaudRate,db8BITS,
        StopBits,Parity,RtsControl,EnableDTROnOpen,Timeout,AIPAddress,APort);
    if not MyCommunication.Connect() then
        raise Exception.Create('Comunication port error');
    //cti verzi FW
    repeat
      ReadOK:=MyCommunication.ReadFirmNO(Cardinal(firmwareNO));
    until (ReadOK)or(MessageDlg(GetTranslationText('READ_ERROR', STR_READ_ERROR), mtError, [mbRetry, mbAbort], 0) = mrAbort);
    if not ReadOK then
        raise Exception.Create('No device was detected');
    Result:=firmwareNO;
end;

procedure MagB2Run;
var
  statForm: TformMagB2Stat;
  menuForm: TformMagB2Menu;
begin
    MyCommunication:= nil;
    statForm:= nil;
    menuForm:= nil;

    try
      case formMagB2Enter.ShowModal of
        //statistic
        mrOK:
        begin
              case formMagB2Enter.ProtokolType of
                ptMagX1:MyCommunication:=TMyCommPort.Create();
                ptModbus:MyCommunication:=TMyModbus.Create();
                ptTCPModbus:MyCommunication:=TMyTCPModbus.Create();
                ptDemo:MyCommunication:=TDemoCommunication.Create();
              else
                raise Exception.Create('Unsuported protocol');
              end;
              //cti verzi firmware
              firmwareNO := GetDeviceFirmwareNumber(formMagB2Enter.ProtokolType,formMagB2Enter.SlaveID,
                      formMagB2Enter.ComPortNumber,formMagB2Enter.BaudRate,formMagB2Enter.StopBits,
                      formMagB2Enter.Parity,formMagB2Enter.Timeout,formMagB2Enter.RtsControl ,true,
                      formMagB2Enter.rgConvertorRS485.ItemIndex, formMagB2Enter.edtTCPIPAddress.Text,
                      formMagB2Enter.seTCPPort.Value);

              statForm := TformMagB2Stat.Create(Application);
              statForm.Caption := 'MagB2 statistic';
              statForm.Init(MyCommunication, firmwareNO);
        end;
        //Service
        mrNo:
        begin
              case formMagB2Enter.ProtokolType of
                ptMagX1:MyCommunication:=TMyCommPort.Create();
                ptModbus:MyCommunication:=TMyModbus.Create();
                ptTCPModbus:MyCommunication:=TMyTCPModbus.Create();
                ptDemo:MyCommunication:=TDemoCommunication.Create();
              else
                raise Exception.Create('Unsuported protocol');
              end;
              //cti verzi firmware
              firmwareNO := GetDeviceFirmwareNumber(formMagB2Enter.ProtokolType,formMagB2Enter.SlaveID,
                      formMagB2Enter.ComPortNumber,formMagB2Enter.BaudRate,formMagB2Enter.StopBits,
                      formMagB2Enter.Parity,formMagB2Enter.Timeout,formMagB2Enter.RtsControl ,true,
                      formMagB2Enter.rgConvertorRS485.ItemIndex, formMagB2Enter.edtTCPIPAddress.Text,
                      formMagB2Enter.seTCPPort.Value);

              menuForm := TformMagB2Menu.Create(Application);
              menuForm.Caption := 'MagB2 service';
              menuForm.Init(MyCommunication, firmwareNO, formMagB2Enter.ProtokolType);
        end;
        //Close
        mrCancel:
        begin
        end;
      end;

    except
      on E: Exception do
      begin
        MessageDlg(E.Message,mtError, [mbOk], 0);
        if MyCommunication <> nil then begin
          MyCommunication.Disconnect;
          FreeAndNil(MyCommunication);
        end;
        if statForm <> nil then statForm.Close;
        if menuForm <> nil then menuForm.Close;        
      end;
    end;
end;

end.
