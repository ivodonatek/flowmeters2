unit MagB2TranslationUnit;

interface

uses INIFiles, FunctionsUnit;

const
  LANG_FILE_NAME = 'MagB2\MagB2.dic';

var
  GlobalLanguage: TLanguage = langEnglish;
  GlobalLanguageSection: String = 'EN';
  GlobalLanguageVersionName: String = 'English version';
  LanguageIniFile: TINIFile = nil;

function GetTranslationText(ACode: String; ADefValue: String = ''): String;
//function TranslationTextExist(ACode: String): Boolean;

implementation

uses Forms, SysUtils;

function GetTranslationText(ACode: String; ADefValue: String = ''): String;
begin
  Result := ADefValue;
  if Assigned(LanguageIniFile) then begin
    Result := LanguageIniFile.ReadString(GlobalLanguageSection, ACode, ADefValue);
  end;
end;

{
function TranslationTextExist(ACode: String): Boolean;
begin
  Result := False;
  if Assigned(LanguageIniFile) then begin
    Result := LanguageIniFile.ValueExists(GlobalLanguageSection, ACode);
  end;
end;
}

initialization
LanguageIniFile := TIniFile.Create(IncludeTrailingBackslash(ExtractFilePath(Application.ExeName)) + LANG_FILE_NAME);

finalization
FreeAndNil(LanguageIniFile);

end.
