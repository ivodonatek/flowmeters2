unit MagB2PassForm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, StdCtrls,
  Buttons, ExtCtrls, MagB2MyCommunicationClass, MagB2GlobalUtilsClass, WaitFrm,
  MagB2FunctionsUnit, MagX1Exception, FunctionsUnit;

const PassValueName : Array[1..4] of String =
  ('CM_PASSWORD_USER','CM_PASSWORD_SERVICE','CM_PASSWORD_FACTORY','CM_PASSWORD_AUTHORIZE');

type
  TformMagB2Pass= class(TForm)
    GroupBox1: TGroupBox;
    Panel1: TPanel;
    eValue: TEdit;
    btnCancel: TButton;
    btnConfirm: TButton;
    procedure btnCancelClick(Sender: TObject);
    procedure btnConfirmClick(Sender: TObject);
    constructor CreateFrm(AOwner: TComponent;Pass:integer;MyCommunication:TMyCommunication;Values:TStringList;
      ProtokolType:TProtokolType; Language: TLanguage);
  private
    { Private declarations }
    LocalLanguage: TLanguage;
    FValues : TStringList;
    FMyCommunication : TMyCommunication;
    FProtokolType:TProtokolType;
  public
    { Public declarations }
  end;

var
    PassIdx:integer;
implementation

uses MagB2TranslationUnit;

// uses MenuFrm;

{$R *.DFM}

const
  PassLabels: array[1..4] of string = ('Password (user)','Password (Service)','Password (Factory)','Password (Authorize)');

constructor TformMagB2Pass.CreateFrm(AOwner: TComponent;Pass:integer;MyCommunication:TMyCommunication;Values:TStringList;
   ProtokolType:TProtokolType; Language: TLanguage);
begin
  inherited Create(AOwner);
  LocalLanguage:= Language;
  FValues:=Values;
  FMyCommunication:= MyCommunication;
  FProtokolType:=ProtokolType;
  PassIdx:=Pass;
  GroupBox1.Caption:=GetTranslationText(PassValueName[PassIdx], PassLabels[PassIdx]);
end;


procedure TformMagB2Pass.btnCancelClick(Sender: TObject);
begin
  ModalResult:=mrCancel;
end;

procedure TformMagB2Pass.btnConfirmClick(Sender: TObject);
var FourByte : T4Byte;
    ReadOK:Boolean;
begin
  inherited;
  try
    Screen.Cursor:=crHourGlass;

    case FProtokolType of
      ptMagX1:
      begin
        repeat
          ReadOK:=FMyCommunication.ReadMenuValue(GetAddress(FProtokolType,PassValueName[PassIdx],FValues),FourByte);
        until (ReadOK)or(MessageDlg(GetTranslationText('READ_ERROR', STR_READ_ERROR), mtError, [mbRetry, mbAbort], 0) = mrAbort);
        if not ReadOK then
        begin
          exit;
        end;

        if IntToStr(Integer(FourByte)) = eValue.Text then
        begin
          Modalresult:=mrOk;
        end
        else
        begin
          ShowMessage(GetTranslationText('INCORRECT_PASSWORD', STR_INCORRECT_PASSWORD));
          eValue.SetFocus;
        end;

      end;
      ptTCPModbus,ptModbus,ptDemo:
      begin
        Integer(FourByte):=StrToIntDef(eValue.Text,0);
        repeat
          ReadOK:=FMyCommunication.WriteMenuValue(GetAddress(FProtokolType,PassValueName[PassIdx],FValues),FourByte);
        until (ReadOK)or(MessageDlg(GetTranslationText('READ_ERROR', STR_READ_ERROR), mtError, [mbRetry, mbAbort], 0) = mrAbort);
        if not ReadOK then
        begin
          raise EReadingError.Create(GetTranslationText('COMMUNICATION_ERROR', STR_COMMUNICATION_ERROR));
        end;

        repeat
            ReadOK:=FMyCommunication.ReadMenuValue(GetAddress(FProtokolType,PassValueName[PassIdx],FValues),FourByte);
        until (ReadOK)or(MessageDlg(GetTranslationText('READ_ERROR', STR_READ_ERROR), mtError, [mbRetry, mbAbort], 0) = mrAbort);
        if (ReadOK)and(Integer(FourByte)=1) then
          Modalresult:=mrOk
        else
        begin
          MessageDlg(GetTranslationText('INCORRECT_PASSWORD', STR_INCORRECT_PASSWORD), mtError, [mbOk], 0);
          eValue.SetFocus;
        end;
      end;
    else
    	Exception.create(GetTranslationText('UNKNOWN_COMM_PROTOCOL', STR_UNKNOWN_COMM_PROTOCOL));
    end;
  finally
    Screen.Cursor:=crDefault;
  end;
end;

end.
